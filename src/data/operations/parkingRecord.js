export const generateParkingRecordData = () =>{
    return [{
        id:'parking-record-1',
        carNumber:'云LM5190',
        carStatus:'车辆进场',
        timeRecord:'2021/03/25 16:22:14'
    },{
        id:'parking-record-2',
        carNumber:'云LV8156',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 16:21:22'
    },{
        id:'parking-record-3',
        carNumber:'云LS9226',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 16:18:12'
    },{
        id:'parking-record-4',
        carNumber:'川CD0022',
        carStatus:'车辆进场',
        timeRecord:'2021/03/25 16:15:14'
    },{
        id:'parking-record-5',
        carNumber:'云LL548T',
        carStatus:'车辆进场',
        timeRecord:'2021/03/25 16:14:45'
    },{
        id:'parking-record-6',
        carNumber:'云A8BB03',
        carStatus:'车辆进场',
        timeRecord:'2021/03/25 15:31:14'
    },{
        id:'parking-record-7',
        carNumber:'云LY2543',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 15:28:14'
    },{
        id:'parking-record-8',
        carNumber:'贵AV8163',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 15:12:14'
    },{
        id:'parking-record-9',
        carNumber:'云LW7653',
        carStatus:'车辆进场',
        timeRecord:'2021/03/25 14:54:23'
    },{
        id:'parking-record-10',
        carNumber:'云LW775',
        carStatus:'车辆进场',
        timeRecord:'2021/03/25 14:52:54'
    },{
        id:'parking-record-11',
        carNumber:'云AFE321',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 14:22:23'
    },{
        id:'parking-record-12',
        carNumber:'云LO4852',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 14:19:14'
    },{
        id:'parking-record-13',
        carNumber:'云AFE321',
        carStatus:'车辆进场',
        timeRecord:'2021/03/25 14:14:14'
    },{
        id:'parking-record-14',
        carNumber:'云LFE321',
        carStatus:'车辆进场',
        timeRecord:'2021/03/25 13:22:14'
    },{
        id:'parking-record-15',
        carNumber:'云LFE321',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 13:05:14'
    },
    {
        id:'parking-record-15',
        carNumber:'云A3E321',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 12:25:54'
    },
    {
        id:'parking-record-15',
        carNumber:'贵A3T32G',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 12:21:44'
    },
    {
        id:'parking-record-15',
        carNumber:'川A3T32G',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 12:15:43'
    },
    {
        id:'parking-record-15',
        carNumber:'川AGWE94',
        carStatus:'车辆出场',
        timeRecord:'2021/03/25 12:15:43'
    },
    {
        id:'parking-record-15',
        carNumber:'贵A8BB03',
        carStatus:'车辆出',
        timeRecord:'2021/03/25 12:12:34'
    },
    ]
}