export const generateBikePOI = () => {
    let src = [{
		"coord": "100.22261,25.625326",
		"bikeNumber": "BK1025",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.22261,25.625336",
		"bikeNumber": "BK1026",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.222603,25.625351",
		"bikeNumber": "BK1027",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.22261,25.625393",
		"bikeNumber": "BK1028",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.22261,25.625408",
		"bikeNumber": "BK1029",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:11",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.222618,25.625458",
		"bikeNumber": "BK1031",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:11",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.222572,25.625675",
		"bikeNumber": "BK1032",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:11",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.222603,25.625954",
		"bikeNumber": "BK1033",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:11",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.222557,25.626127",
		"bikeNumber": "BK1034",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 10:15",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.222588,25.62628",
		"bikeNumber": "BK1035",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 10:15",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.222267,25.627197",
		"bikeNumber": "BK1036",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 10:25",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.222084,25.627407",
		"bikeNumber": "BK1037",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 10:25",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221703,25.627663",
		"bikeNumber": "BK1038",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 10:25",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221306,25.62818",
		"bikeNumber": "BK1039",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 10:25",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221092,25.628498",
		"bikeNumber": "BK1040",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 11:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221107,25.628788",
		"bikeNumber": "BK1041",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221359,25.629082",
		"bikeNumber": "BK1042",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221451,25.629524",
		"bikeNumber": "BK1043",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221649,25.629663",
		"bikeNumber": "BK1044",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 11:01",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221725,25.629768",
		"bikeNumber": "BK1045",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 11:10",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221733,25.629808",
		"bikeNumber": "BK1046",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:10",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221748,25.629848",
		"bikeNumber": "BK1047",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:10",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221764,25.629898",
		"bikeNumber": "BK1048",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:10",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221741,25.630209",
		"bikeNumber": "BK1049",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:10",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221756,25.630192",
		"bikeNumber": "BK1050",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:10",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221786,25.630163",
		"bikeNumber": "BK1051",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:10",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.221245,25.630199",
		"bikeNumber": "BK1052",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 11:20",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.220535,25.63035",
		"bikeNumber": "BK1053",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:20",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.220146,25.630817",
		"bikeNumber": "BK1054",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 11:20",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.219986,25.631231",
		"bikeNumber": "BK1055",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 13:05",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.2201,25.631767",
		"bikeNumber": "BK1056",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 13:05",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.220062,25.632374",
		"bikeNumber": "BK1057",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 13:05",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.21991,25.632572",
		"bikeNumber": "BK1058",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 13:05",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.219467,25.632984",
		"bikeNumber": "BK1059",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 14:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.218826,25.633272",
		"bikeNumber": "BK1060",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 14:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.218674,25.63357",
		"bikeNumber": "BK1061",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 14:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.218613,25.634008",
		"bikeNumber": "BK1062",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 14:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.218269,25.634483",
		"bikeNumber": "BK1063",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 14:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.218109,25.634914",
		"bikeNumber": "BK1064",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 14:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.217636,25.635208",
		"bikeNumber": "BK1065",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 14:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.217331,25.635418",
		"bikeNumber": "BK1066",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 14:00",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.216965,25.635286",
		"bikeNumber": "BK1067",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 16:43",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.216637,25.635248",
		"bikeNumber": "BK1068",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 16:43",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.216301,25.635265",
		"bikeNumber": "BK1069",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 16:43",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.21611,25.635784",
		"bikeNumber": "BK1070",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 16:43",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.215958,25.636145",
		"bikeNumber": "BK1071",
		"bikeType": "自行车",
		"lastTime": "2020/10/27 17:08",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.215714,25.636473",
		"bikeNumber": "BK1072",
		"bikeType": "自行车",
		"lastTime": "2020/10/28 17:08",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.215446,25.636662",
		"bikeNumber": "BK1073",
		"bikeType": "自行车",
		"lastTime": "2020/10/29 17:08",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.215324,25.63703",
		"bikeNumber": "BK1074",
		"bikeType": "自行车",
		"lastTime": "2020/10/30 17:08",
		"isAbnormal": false,
		"abnormalReason": ""
	},
	{
		"coord": "100.215401,25.637644",
		"bikeNumber": "BK1075",
		"bikeType": "自行车",
		"lastTime": "2020/10/31 17:08",
		"isAbnormal": false,
		"abnormalReason": ""
	},

]
    return src.map((o,i)=>{
        return {
            id: 'operation_bike_'+i,
            coord: o.coord,
            coord_type: 0,
            cad_mapkey: '',
            coord_z: 1,
            always_show_label: true,
            sort_order: false,
            state: o.isAbnormal?'state_2':'state_1',
            marker: {
                size: '46,73',
                images: [{
                    define_state: 'state_1',
                    normal_url: 'http://localhost:3000/images/bike_marker.png',
                    activate_url: 'http://localhost:3000/images/bike_marker_click.png'
                },{
                    define_state:'state_2',
                    normal_url: 'http://localhost:3000/images/unormal_bike_marker.png',
                    activate_url: 'http://localhost:3000/images/bike_marker_click.png'
                }]
            },
            window: {
                url: 'http://localhost:3000/pages/operations/bike.html?id=operation_bike_'+i,
                size: '500,220',
                offset: '30,200'
            }
        }
    })
}