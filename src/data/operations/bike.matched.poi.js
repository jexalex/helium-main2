export const bikeMatchedPOI = () => {
    //CAMERA10 CAMERA11
    'operation_bike_14'
    let src = [{
        "coord": "100.221715,25.632453"
    }, {
        "coord": "100.222094,25.632541"
    }, {
        "coord": "100.220525,25.63066"
    }];
    return src.map((o, i) => {
        return {
            id: i < 2 ? 'bm-camera-' + i : 'bm-worker-' + i,
            coord: o.coord,
            coord_type: 0,
            cad_mapkey: "",
            coord_z: 1, //高度(米)
            always_show_label: true,
            show_label_range: "0,2000",
            sort_order: false,
            state: "state_1",
            marker: {
                size: "46,73",
                images: [{
                    define_state: "state_1",
                    normal_url: i < 2 ? "http://localhost:3000/images/camera_marker.png" : 'http://localhost:3000/images/worker_marker.png',
                    activate_url: i < 2 ? "http://localhost:3000/html/images/camera_marker_click.png" : 'http://localhost:3000/images/worker_marker_click.png',
                }],
            },
            label: {
                bg_image_url: "http://localhost:3000/html/images/label_bg.png",
                size: "113,26",
                offset: "11,63",
                text: i < 2 ? "摄像头" : "黄旭东",
                font_color: "fafafaff",
                text_boxsize: "82,14",
                text_offset: "25,7",
            },
            window: {
                url: i < 2 ? "http://localhost:3000/pages/operations/camera.html?id=" + i : 'http://localhost:3000/pages/operations/worker.html?name=黄旭东&number=0047&group=巡检一组&tel=135****1789&id=bm-worker-' + i,
                size: i < 2 ? "270,155" : "320,145",
                offset: "50,180"
            }
        }
    })
}