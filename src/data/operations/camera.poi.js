export const generateCameraPOI = () => {
    let src = [
        {
          "coord": "100.222618,25.625746"
        },
        {
          "coord": "100.22185495092482,25.630206365705874"
        },
        {
          "coord": "100.22020603203897,25.630769222947666"
        },
        {
          "coord": "100.21892703911239,25.63389553755106"
        },
        {
          "coord": "100.2160947669507,25.636115906106788"
        },
        {
          "coord": "100.21638431756286,25.638588188294992"
        },
        {
          "coord": "100.21538011610546,25.641715455825757"
        },
        {
          "coord": "100.21503694523422,25.642912187745303"
        },
        {
          "coord": "100.21434980154402,25.6434655253107"
        },
        {
          "coord": "100.21441314586602,25.644383603415772"
        },
        {
          "coord": "100.21359922439379,25.644957669192447"
        },
        {
          "coord": "100.21346672257366,25.646490216911204"
        }];
    return src.map((o,i) => {
        return {
            id: 'operation_camera_'+i,
            coord: o.coord,
            coord_type: 0,
            cad_mapkey: '',
            coord_z: 1,
            always_show_label: true,
            sort_order: false,
            state: 'state_1',
            marker: {
                size: '46,73',
                images: [{
                    define_state: 'state_1',
                    normal_url: 'http://localhost:3000/images/camera_marker.png',
                    activate_url: 'http://localhost:3000/images/camera_marker_click.png'
                }]
            },
            window: {
                url: 'http://localhost:3000/pages/operations/camera.html?id='+i,
                size: '270,155',
                offset: '30,200'
            }
        }
    })
}