export const generateShuttlePOI = () => {
    let src = [{
            "coord": "100.222816,25.625404",
            detail: {
                "carNumber": "DB0150",
                "carType": "接驳车",
                "carDriverName": "王生安",
                "carDriverTelNumber": 13950035537,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "8: 00",
                "endShiftTime": "17: 00",
                "shiftNumber": 15,
                shiftDrivers: [{
                    name: '王生安',
                    telNumber: '13950035537',
                    photoURL: 'http://localhost:3000/images/头像1.jfif'
                }, {
                    name: '张淮森',
                    telNumber: '19219249432',
                    photoURL: 'http://localhost:3000/images/头像2.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-1.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.222786,25.625429",
            detail: {
                "carNumber": "DB0151",
                "carType": "接驳车",
                "carDriverName": "李鑫灏",
                "carDriverTelNumber": 13966427550,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "08: 15",
                "endShiftTime": "17: 15",
                "shiftNumber": 14,
                shiftDrivers: [{
                    name: '李鑫灏',
                    telNumber: '13966427550',
                    photoURL: 'http://localhost:3000/images/头像3.jfif'
                }, {
                    name: '夏劲釜',
                    telNumber: '13947766628',
                    photoURL: 'http://localhost:3000/images/头像4.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-2.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.222755,25.625458",
            detail: {
                "carNumber": "DB0152",
                "carType": "接驳车",
                "carDriverName": "薛佛世",
                "carDriverTelNumber": 13947766628,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "08: 30",
                "endShiftTime": "17: 30",
                "shiftNumber": 13,
                shiftDrivers: [{
                    name: '薛佛世',
                    telNumber: '13947766628',
                    photoURL: 'http://localhost:3000/images/头像5.jfif'
                }, {
                    name: '王好隐',
                    telNumber: '14185910017',
                    photoURL: 'http://localhost:3000/images/头像6.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-3.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.222725,25.625492",
            detail: {
                "carNumber": "DB0153",
                "carType": "接驳车",
                "carDriverName": "蔡壮保",
                "carDriverTelNumber": 13916882701,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "08: 45",
                "endShiftTime": "17: 45",
                "shiftNumber": 12,
                shiftDrivers: [{
                    name: '蔡壮保',
                    telNumber: '13916882701',
                    photoURL: 'http://localhost:3000/images/头像7.jfif'
                }, {
                    name: '刘昼星',
                    telNumber: '18945938454',
                    photoURL: 'http://localhost:3000/images/头像8.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-4.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.222702,25.625523",
            detail: {
                "carNumber": "DB0154",
                "carType": "接驳车",
                "carDriverName": "钱勤堃",
                "carDriverTelNumber": 13961332155,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "09: 00",
                "endShiftTime": "18: 00",
                "shiftNumber": 11,
                shiftDrivers: [{
                    name: '钱勤堃',
                    telNumber: '13961332155',
                    photoURL: 'http://localhost:3000/images/头像9.jfif'
                }, {
                    name: '池荣弈',
                    telNumber: '16135452621',
                    photoURL: 'http://localhost:3000/images/头像10.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '道路监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-5.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.222656,25.625605",
            detail: {
                "carNumber": "DB0155",
                "carType": "接驳车",
                "carDriverName": "潘恩依",
                "carDriverTelNumber": 13937401011,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "09: 15",
                "endShiftTime": "18: 15",
                "shiftNumber": 10,
                shiftDrivers: [{
                    name: '潘恩依',
                    telNumber: '13937401011',
                    photoURL: 'http://localhost:3000/images/头像1.jfif'
                }, {
                    name: '邹包幼',
                    telNumber: '13335291016',
                    photoURL: 'http://localhost:3000/images/头像2.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-6.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.222565,25.626278",
            detail: {
                "carNumber": "DB0156",
                "carType": "接驳车",
                "carDriverName": "陈国柏",
                "carDriverTelNumber": 13964507501,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "9: 30",
                "endShiftTime": "18: 30",
                "shiftNumber": "9",
                shiftDrivers: [{
                    name: '陈国柏',
                    telNumber: '13964507501',
                    photoURL: 'http://localhost:3000/images/头像3.jfif'
                }, {
                    name: '王施峪',
                    telNumber: '18916988261',
                    photoURL: 'http://localhost:3000/images/头像4.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-1.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.221634,25.627796",
            detail: {
                "carNumber": "DB0157",
                "carType": "接驳车",
                "carDriverName": "魏皑虎",
                "carDriverTelNumber": 13901784558,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "09: 45",
                "endShiftTime": "18: 45",
                "shiftNumber": 8,
                shiftDrivers: [{
                    name: '魏皑虎',
                    telNumber: '13901784558',
                    photoURL: 'http://localhost:3000/images/头像5.jfif'
                }, {
                    name: '刘昼星',
                    telNumber: '13943250086',
                    photoURL: 'http://localhost:3000/images/头像6.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-2.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.221809,25.630108",
            detail: {
                "carNumber": "DB0158",
                "carType": "接驳车",
                "carDriverName": "周卓浩",
                "carDriverTelNumber": 13943250086,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "10: 00",
                "endShiftTime": "19: 00",
                "shiftNumber": "7",
                shiftDrivers: [{
                    name: '周卓浩',
                    telNumber: '13943250086',
                    photoURL: 'http://localhost:3000/images/头像7.jfif'
                }, {
                    name: '池荣弈',
                    telNumber: '13901784558',
                    photoURL: 'http://localhost:3000/images/头像8.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-3.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.220055,25.631456",
            detail: {
                "carNumber": "DB0159",
                "carType": "接驳车",
                "carDriverName": "汤辟邦",
                "carDriverTelNumber": 18916988261,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "10: 15",
                "endShiftTime": "19: 15",
                "shiftNumber": "6",
                shiftDrivers: [{
                    name: '汤辟邦',
                    telNumber: '18916988261',
                    photoURL: 'http://localhost:3000/images/头像9.jfif'
                }, {
                    name: '范千皋',
                    telNumber: '13964507501',
                    photoURL: 'http://localhost:3000/images/头像10.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-4.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.21714,25.63538",
            detail: {
                "carNumber": "DB0160",
                "carType": "接驳车",
                "carDriverName": "张顺谷",
                "carDriverTelNumber": 13335291016,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "10: 30",
                "endShiftTime": "19: 30",
                "shiftNumber": 5,
                shiftDrivers: [{
                    name: '张顺谷',
                    telNumber: '13335291016',
                    photoURL: 'http://localhost:3000/images/头像1.jfif'
                }, {
                    name: '潘佩焱',
                    telNumber: '13937401011',
                    photoURL: 'http://localhost:3000/images/头像2.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-5.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.215683,25.636492",
            detail: {
                "carNumber": "DB0161",
                "carType": "接驳车",
                "carDriverName": "张悌斯",
                "carDriverTelNumber": 16135452621,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "10: 45",
                "endShiftTime": "19: 45",
                "shiftNumber": 4,
                shiftDrivers: [{
                    name: '张悌斯',
                    telNumber: '16135452621',
                    photoURL: 'http://localhost:3000/images/头像3.jfif'
                }, {
                    name: '朱付流',
                    telNumber: '13961332155',
                    photoURL: 'http://localhost:3000/images/头像4.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-6.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.21595,25.638018",
            detail: {
                "carNumber": "DB0162",
                "carType": "接驳车",
                "carDriverName": "张灶冲",
                "carDriverTelNumber": 18945938454,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "11: 00",
                "endShiftTime": "20: 00",
                "shiftNumber": 3,
                shiftDrivers: [{
                    name: '张灶冲',
                    telNumber: '18945938454',
                    photoURL: 'http://localhost:3000/images/头像5.jfif'
                }, {
                    name: '陆丛枫',
                    telNumber: '13916882701',
                    photoURL: 'http://localhost:3000/images/头像6.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-1.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.215767,25.640356",
            detail: {
                "carNumber": "DB0163",
                "carType": "接驳车",
                "carDriverName": "易江维",
                "carDriverTelNumber": 14185910017,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "正常",
                "driverCurrentStatus": "正常",
                "chargerName": "宋文书",
                "beginShiftTime": "11: 15",
                "endShiftTime": "20: 15",
                "shiftNumber": 2,
                shiftDrivers: [{
                    name: '易江维',
                    telNumber: '14185910017',
                    photoURL: 'http://localhost:3000/images/头像7.jfif'
                }, {
                    name: '汤丞昱',
                    telNumber: '13947766628',
                    photoURL: 'http://localhost:3000/images/头像8.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-2.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        },
        {
            "coord": "100.214203,25.641403",
            detail: {
                "carNumber": "DB0164",
                "carType": "接驳车",
                "carDriverName": "孙来笙",
                "carDriverTelNumber": 19219249432,
                "designLoad": 14,
                "routeName": "阳南溪-才村-阳南溪",
                "currentStatus": "异常",
                "driverCurrentStatus": "异常",
                "chargerName": "宋文书",
                "beginShiftTime": "11: 30",
                "endShiftTime": "20: 30",
                "shiftNumber": 1,
                shiftDrivers: [{
                    name: '孙来笙',
                    telNumber: '19219249432',
                    photoURL: 'http://localhost:3000/images/头像9.jfif'
                }, {
                    name: '萧百徽',
                    telNumber: '13950035537',
                    photoURL: 'http://localhost:3000/images/头像10.jfif'
                }],
                monitors: [{
                    id: 'monitor_1',
                    name: '车内监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-outside.mp4'
                }, {
                    name: '车头监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-road-3.mp4',
                    id: 'monitor_2'
                }, {
                    id: 'monitor_3',
                    name: '驾驶员监控画面',
                    url: 'http://localhost:3000/media/operation-shuttle-driver.mp4'
                }]
            }
        }
    ];
    return src.map((o, i) => {
        let state = 0;
        if (o.detail.currentStatus === '正常') {
            state = 1;
        } else {
            state = 2;
        }
        return {
            id: 'operation_shuttle_' + i,
            coord: o.coord,
            coord_type: 0,
            cad_mapkey: '',
            coord_z: 1,
            always_show_label: true,
            sort_order: false,
            state: 'state_' + state,
            marker: {
                size: '46,73',
                images: [{
                    define_state: 'state_1',
                    normal_url: 'http://localhost:3000/images/shuttle_marker.png',
                    activate_url: 'http://localhost:3000/images/shuttle_marker_click.png'
                }, {
                    define_state: 'state_2',
                    normal_url: 'http://localhost:3000/images/unormal_shuttle_marker.png',
                    activate_url: 'http://localhost:3000/images/shuttle_marker_click.png'
                }]
            },
            detail: o.detail
        }
    })
}