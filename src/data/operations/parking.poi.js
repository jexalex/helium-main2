export default [{
    id:'park_廊道入口停车场_poi',
    coord:'100.224136,25.62377',
    coord_type:0,
    cad_mapkey:'',
    coord_z:1,
    always_show_label:true,
    sort_order:false,
    state:'state_1',
    marker:{
        size:'46,216',
        images:[{
            define_state:'state_1',
            normal_url:'http://localhost:3000/html/images/parking_marker.png',
            activate_url:'http://localhost:3000/html/images/parking_marker_click.png'
        }]
    },
    label: {
        bg_image_url: "http://localhost:3000/html/images/label_bg.png",
        size: "113,26", //label大小(宽,高 单位:像素)
        offset: "11,206", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
        text: "廊道入口停车场", //label中的文本内容
        font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
        text_boxsize: "100,14", //label中文本框的大小(宽,高 单位:像素)
        text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
      },
}]