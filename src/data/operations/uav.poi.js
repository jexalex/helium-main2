export default [{
    id: 'operation_uav_1',
    coord: "100.222588,25.625593",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/images/uav_marker.png',
            activate_url: 'http://localhost:3000/images/uav_marker_click.png'
        }]
    },
    detail:{
        carNumber:'WR0001',
        carType:'电动驱动',
        carShelterNumber:'200699',
        designLoad:14,
        producer:'深圳比亚迪（BYD）',
        carPurchaseDate:'2020/09/30',
        carLastMaintenanceDate:'2020/10/01',
        routeName:'阳南溪-龙龛',
        chargerName:'林敏丽',
        chargerTelNumber:'183****1996',
        currentStatus:'normal',
        abnormalReason:null,
        monitors:[{
            id:'monitor_1',
            name:'无人车监控画面1',
            url:'http://localhost:3000/media/operation-uav-1.mp4'
        },{
            name:'无人车监控画面2',
            url:'http://localhost:3000/media/operation-uav-outside.mp4',
            id:'monitor_2'
        }]
    }
}]