export const generateHomeListData = () => {
        return [{
            id:'opinion-1',
            listInfo: '有人在廊道乱丢垃圾',
            eventType: '投诉类',
            coord: '100.221542,25.630354',
            status: '处置中'
        }, {
            id:'opinion-2',
            listInfo: '共享单车故障不能使用了',
            eventType: '投诉类',
            coord: '100.215759,25.638014',
            status: '处置中'
        }, {
            id:'opinion-3',
            listInfo: '有人在洱海游泳',
            eventType: '投诉类',
            coord: '100.21538011610546,25.641715455825757',
            status: '处置中'
        }, {
            id:'opinion-4',
            listInfo: '有人在洱海钓鱼',
            eventType: '投诉类',
            coord: '100.21359922439379,25.644957669192447',
            status: '已受理'
        }, {
            id:'opinion-5',
            listInfo: '有人乱停自行车',
            eventType: '投诉类',
            coord: '100.21991,25.632572',
            status: '已受理'
        }, {
            id: 'security_event_1',
            listInfo: 'SJ1111',
            eventType:'安防类',
            status: '已完成',
            coord: '100.220131,25.628828',
        }, {
            id: 'security_event_2',
            listInfo: 'SJ1112',
            eventType: '安防类',
            status: '处置中',
            coord: '100.215698,25.638037',
        }, {
            id: 'security_event_3',
            listInfo: 'SJ1113',
            eventType: '安防类',
            status: '待受理',
            coord: '100.215698,25.638037',
        }, {
            id: 'security_event_4',
            listInfo: 'SJ1114',
            eventType: '安防类',
            status: '待处理',
            coord: '100.215698,25.638037',
        }, {
            id: 'security_event_5',
            listInfo: 'SJ1115',
            eventType: '安防类',
            status: '已完成',
            coord: '100.215698,25.638037',
        },{
            id: 'operation_lamp_7',
            eventType: '故障类',
            listInfo: "智慧杆故障维修派单",
            status: '已受理',
        }]
    }