export const generateOpinionData = () => {
    return[
        {
            id: 'security_event_1',
            eventType: 'SJ1111',
            eventTime: '11:13',
            eventStatus: '已完成',
            coord:'100.220131,25.628828',
        },{
            id: 'security_event_2',
            eventType: 'SJ1112',
            eventTime: '15:22',
            eventStatus: '处置中',
            coord:'100.215698,25.638037',
        },{
            id: 'security_event_3',
            eventType: 'SJ1113',
            eventTime: '15:22',
            eventStatus: '待受理',
            coord:'100.221542,25.630354',
        },{
            id: 'security_event_4',
            eventType: 'SJ1114',
            eventTime: '15:22',
            eventStatus: '待受理',
            coord:'100.215759,25.638014',
        },{
            id: 'security_event_5',
            eventType: 'SJ1115',
            eventTime: '15:22',
            eventStatus: '待受理',
            coord:'100.220055,25.628769',
        }
    ]
}