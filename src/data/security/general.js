export default {
    alarmCount:124,
    alarmData:{
        finished:8,
        archieved:27,
        sent:41,
        accepted:41,
        idle:42
    },
    securityOnDuty:166,
    securityOnDutyRate:88,
    cameraOnDuty:72,
    cameraOnDutyRate:78,
    securityPatrolFinished:245,
    securityPatrolFinishedRate:68
}