export const generateCameraPOI = () => {
    let src = [
            {
                "type": "Feature",
                "id": 0,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.2027971,25.70452454
                    ]
                },
                "properties": {
                    "FID": 0,
                    "Id": 1,
                    "x": 100.2027971,
                    "y": 25.70452454,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 1,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.2016149,25.70607035
                    ]
                },
                "properties": {
                    "FID": 1,
                    "Id": 2,
                    "x": 100.2016149,
                    "y": 25.70607035,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 2,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.20029825,25.70827362
                    ]
                },
                "properties": {
                    "FID": 2,
                    "Id": 3,
                    "x": 100.20029825,
                    "y": 25.70827362,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 3,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19961307,
                        25.70834024
                    ]
                },
                "properties": {
                    "FID": 3,
                    "Id": 4,
                    "x": 100.19961307,
                    "y": 25.70834024,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 4,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19895471,
                        25.71023013
                    ]
                },
                "properties": {
                    "FID": 4,
                    "Id": 5,
                    "x": 100.19895471,
                    "y": 25.71023013,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 5,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19825337,
                        25.71124701
                    ]
                },
                "properties": {
                    "FID": 5,
                    "Id": 6,
                    "x": 100.19825337,
                    "y": 25.71124701,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 6,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19582682,
                        25.71418092
                    ]
                },
                "properties": {
                    "FID": 6,
                    "Id": 7,
                    "x": 100.19582682,
                    "y": 25.71418092,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 7,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19547209,
                        25.71483802
                    ]
                },
                "properties": {
                    "FID": 7,
                    "Id": 8,
                    "x": 100.19547209,
                    "y": 25.71483802,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 8,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19444016,
                        25.71585668
                    ]
                },
                "properties": {
                    "FID": 8,
                    "Id": 9,
                    "x": 100.19444016,
                    "y": 25.71585668,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 9,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19210747,
                        25.71972623
                    ]
                },
                "properties": {
                    "FID": 9,
                    "Id": 10,
                    "x": 100.19210747,
                    "y": 25.71972623,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 10,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19296203,
                        25.72035394
                    ]
                },
                "properties": {
                    "FID": 10,
                    "Id": 11,
                    "x": 100.19296203,
                    "y": 25.72035394,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 11,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.1918387,
                        25.72069292
                    ]
                },
                "properties": {
                    "FID": 11,
                    "Id": 12,
                    "x": 100.1918387,
                    "y": 25.72069292,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 12,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.19248366,
                        25.72119696
                    ]
                },
                "properties": {
                    "FID": 12,
                    "Id": 13,
                    "x": 100.19248366,
                    "y": 25.72119696,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 13,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.1921988,
                        25.72131736
                    ]
                },
                "properties": {
                    "FID": 13,
                    "Id": 14,
                    "x": 100.1921988,
                    "y": 25.72131736,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 14,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.21629542,
                        25.63923886
                    ]
                },
                "properties": {
                    "FID": 14,
                    "Id": 15,
                    "x": 100.21629542,
                    "y": 25.63923886,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 15,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.20685411,
                        25.69656966
                    ]
                },
                "properties": {
                    "FID": 15,
                    "Id": 16,
                    "x": 100.20685411,
                    "y": 25.69656966,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 16,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.20624157,
                        25.69757963
                    ]
                },
                "properties": {
                    "FID": 16,
                    "Id": 17,
                    "x": 100.20624157,
                    "y": 25.69757963,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 17,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.2052851,
                        25.69971117
                    ]
                },
                "properties": {
                    "FID": 17,
                    "Id": 18,
                    "x": 100.2052851,
                    "y": 25.69971117,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 18,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.20426146,
                        25.70116786
                    ]
                },
                "properties": {
                    "FID": 18,
                    "Id": 19,
                    "x": 100.20426146,
                    "y": 25.70116786,
                    "name": "监控"
                }
            },
            {
                "type": "Feature",
                "id": 19,
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        100.20324853,
                        25.7026874
                    ]
                },
                "properties": {
                    "FID": 19,
                    "Id": 20,
                    "x": 100.20324853,
                    "y": 25.7026874,
                    "name": "监控"
                }
            }
    ]
    // return src.map((o,i) => {
    //     return {
    //         id: 'security_camera_'+i,
    //         coord: `${o.properties.x},${o.properties.y}`,
    //         coord_type: 0,
    //         cad_mapkey: '',
    //         coord_z: 1,
    //         always_show_label: true,
    //         sort_order: false,
    //         state: 'state_1',
    //         marker: {
    //             size: '46,73',
    //             images: [{
    //                 define_state: 'state_1',
    //                 normal_url: 'http://localhost:3000/images/camera_marker.png',
    //                 activate_url: 'http://localhost:3000/images/camera_marker_click.png'
    //             }]
    //         },
    //         window: {
    //             url:  'http://localhost:3000/pages/facilities/camera.html?id='+(i+2),
    //             size: '430,249',
    //             offset: '30,200'
    //         }
    //     }
    // })
    return src.map((o,i) => {
        return {
            id: 'security_camera_'+i,
            coord: `${o.properties.x},${o.properties.y}`,
            coord_type: 0,
            cad_mapkey: '',
            coord_z: 1,
            always_show_label: true,
            sort_order: false,
            state: 'state_1',
            marker: {
                size: '46,73',
                images: [{
                    define_state: 'state_1',
                    normal_url: 'http://localhost:3000/images/camera_marker.png',
                    activate_url: 'http://localhost:3000/images/camera_marker_click.png'
                }]
            },
            window: {
                url:  '',
                size: '',
                offset: ''
            }
        }
    })
}