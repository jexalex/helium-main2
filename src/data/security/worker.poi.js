export default [{
    id: 'xj1',
    coord: "100.222672,25.625349",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '孙荣海',
        workerNumber: 'XJ1001',
        workerTel: '13950035537',
        workerGroup: '巡检一组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像1.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    },
}, {
    id: 'security_worker_2',
    coord: "100.222801,25.62538",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '李胜哲',
        workerNumber: 'XJ1002',
        workerTel: '13966427550',
        workerGroup: '巡检一组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像2.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_3',
    coord: "100.222633,25.625591",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '孙艳艳',
        workerNumber: 'XJ1003',
        workerTel: '13947766628',
        workerGroup: '巡检一组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像3.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_4',
    coord: "100.222588,25.626005",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '李馨',
        workerNumber: 'XJ1004',
        workerTel: '13916882701',
        workerGroup: '巡检一组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像4.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_5',
    coord: "100.222542,25.626289",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '张志军',
        workerNumber: 'XJ1005',
        workerTel: '13961332155',
        workerGroup: '巡检一组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像5.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_6',
    coord: "100.222282,25.627132",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '张厚宝',
        workerNumber: 'XJ1006',
        workerTel: '13937401011',
        workerGroup: '巡检一组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像6.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_7',
    coord: "100.221115,25.628481",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '庞文涛',
        workerNumber: 'XJ1007',
        workerTel: '13964507501',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像7.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_8',
    coord: "100.220955,25.630266",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '宋爽',
        workerNumber: 'XJ1008',
        workerTel: '13901784558',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像8.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_9',
    coord: "100.219513,25.632971",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '胡婷',
        workerNumber: 'XJ1009',
        workerTel: '13943250086',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像9.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_10',
    coord: "100.218018,25.634968",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '王金国',
        workerNumber: 'XJ1010',
        workerTel: '18916988261',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像10.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_11',
    coord: "100.216103,25.635727",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '孙荣海',
        workerNumber: 'XJ1011',
        workerTel: '13950035537',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像1.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_12',
    coord: "100.215286,25.63698",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '李胜哲',
        workerNumber: 'XJ1012',
        workerTel: '13966427550',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像2.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_13',
    coord: "100.216232,25.638308",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '孙艳艳',
        workerNumber: 'XJ1013',
        workerTel: '13947766628',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像3.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_14',
    coord: "100.215355,25.640772",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '李馨',
        workerNumber: 'XJ1014',
        workerTel: '13916882701',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像4.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_15',
    coord: "100.214729,25.642408",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '张志军',
        workerNumber: 'XJ1015',
        workerTel: '13961332155',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像5.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_16',
    coord: "100.214287,25.643398",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '张厚宝',
        workerNumber: 'XJ1016',
        workerTel: '13937401011',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像6.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_17',
    coord: "100.213539,25.644354",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '庞文涛',
        workerNumber: 'XJ1017',
        workerTel: '13964507501',
        workerGroup: '巡检二组',
        checktime: '09:21',
        photoURL: 'http://localhost:3000/images/头像7.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}, {
    id: 'security_worker_18',
    coord: "100.21357,25.645584",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
        size: '46,73',
        images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
        }]
    },
    detail:{
        workerName: '宋爽',
        workerNumber: 'XJ1018',
        workerTel: '13901784558',
        workerGroup: '巡检二组',
        checktime: '',
        photoURL: 'http://localhost:3000/images/头像8.jfif',
        monitors:[{
            id:'monitor_1',
            name:'单兵监控画面',
            url:'http://localhost:3000/media/安防事件单兵.mp4'
        }]
    }
}]