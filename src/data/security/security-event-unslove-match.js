export default {
    'security_event_1': [{
        id: 'se1-worker-1',
        coord: '100.221138,25.627914',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "赵伟",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/worker.html?name=赵伟&number=XJ1027&tel=15952010165&group=巡检一组&id=security-worker-1",
            size: "270,160",
            offset: "50,180"
        },
    }],
    'security_event_2': [{
        id: 'se2-worker-1',
        coord: '100.215759,25.640388',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "黄明",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/worker.html?name=黄明&number=XJ1125&tel=18051035415&group=巡检一组&id=security-worker-2",
            size: "270,160",
            offset: "50,180"
        },
    },{
        id: 'se2-camera-1',
        coord: '100.21638431756286,25.638588188294992',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window:{
            url: ''
        }
    }],
    'security_event_9': [{
        id: 'se9-worker-1',
        coord: '100.21402,25.661585',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "宋文书",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/worker.html?name=宋文书&number=WK-077&tel=135****1789&group=巡检一组&id=security-worker-9",
            size: "270,200",
            offset: "50,180"
        },
    },{
        id: 'se9-camera-1',
        coord: '100.214561,25.657795',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/operations/camera.html?id=22",
            size: "270,155",
            offset: "50,180"
        },
    }],
    'security_event_10': [{
        id: 'se10-worker-1',
        coord: '100.21402,25.661585',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "宋文书",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/worker.html?name=宋文书&number=WK-077&tel=135****1789&group=巡检一组&id=security-worker-10",
            size: "270,200",
            offset: "50,180"
        },
    },{
        id: 'se10-camera-1',
        coord: '100.214622,25.659121',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/operations/camera.html?id=22",
            size: "270,155",
            offset: "50,180"
        },
    }],
    'security_event_6': [{
        id: 'se6-worker-1',
        coord: '100.215698,25.645681',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "宋文书",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/workerMove.html?name=宋文书&number=WK-077&tel=135****1789&group=巡检一组&id=security-worker-6",
            size: "320,200",
            offset: "50,180"
        },
    },{
        id: 'se6-camera-1',
        coord: '100.216736,25.644278',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/operations/camera.html?id=22",
            size: "270,155",
            offset: "50,180"
        },
    }],
    'security_event_0': [{
        id: 'se1-worker-1',
        coord: '100.215698,25.645681',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "宋文书",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/worker.html?name=宋文书&number=WK-077&tel=135****1789&group=巡检一组&id=security-worker-1",
            size: "270,200",
            offset: "50,180"
        },
    },{
        id: 'se1-camera-1',
        coord: '100.216278,25.642035',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/operations/camera.html?id=22",
            size: "270,155",
            offset: "50,180"
        },
    }],
    'security_event_5': [{
        id: 'se5-worker-1',
        coord: '100.221092,25.62859',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "李建明",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/worker.html?name=李建明&number=WK-077&tel=135****1789&group=巡检一组&id=security-worker-5",
            size: "270,200",
            offset: "50,180"
        },window:{
            url: ''
        }
    }],
    'security_event_4': [{
        id: 'se4-worker-1',
        coord: '100.215759,25.640388',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "朱自红",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/workerMove.html?name=朱自红&number=WK-077&tel=180****5415&group=巡检一组&id=security-worker-4",
            size: "270,200",
            offset: "50,180"
        },
    },{
        id: 'se4-camera-1',
        coord: '100.21638431756286,25.638588188294992',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
    }],
    'security_event_3': [{
        id: 'se3-worker-1',
        coord: '100.219948,25.631151',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "杨达",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/workerMove.html?name=杨达&number=WK-077&tel=180****5415&group=巡检一组&id=security-worker-3",
            size: "270,200",
            offset: "50,180"
        },
    },{
        id: 'se3-camera-1',
        coord: '100.22185495092482,25.630206365705874',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },window:{
            url: ''
        }
    }],
    'security_event': [{
        id: 'se2-worker-1',
        coord: '100.220528,25.630642',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "宋文书",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/security/worker.html?name=宋文书&number=WK-077&tel=135****1789&group=巡检一组&id=security-worker-2",
            size: "270,200",
            offset: "50,180"
        },
    },{
        id: 'se2-camera-1',
        coord: '100.221931,25.630594',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/operations/camera.html?id=22",
            size: "270,155",
            offset: "50,180"
        },
    }],
}