export default {
    '': [{
        id: 'wc15-worker-1',
        coord: '100.211647,25.660103',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/worker_marker.png",
                activate_url: "http://localhost:3000/html/images/worker_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "宋文书",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/html/worker.html",
            size: "270,300",
            offset: "50,180"
        },
    },{
        id: 'wc15-camera-1',
        coord: '100.214653,25.657846',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/operations/camera.html?id=22",
            size: "270,155",
            offset: "50,180"
        },
    },{
        id: 'wc15-camera-2',
        coord: '100.214554,25.658216',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/operations/camera.html?id=23",
            size: "270,155",
            offset: "50,180"
        },
    },{
        id: 'wc15-camera-3',
        coord: '100.214638,25.658911',
        coord_type: 0,
        cad_mapkey: "", 
        coord_z: 1, //高度(米)
        always_show_label: true,
        show_label_range: "0,2000", 
        sort_order: false,
        state: "state_1",
        marker: {
            size: "46,73",
            images: [{
                define_state: "state_1",
                normal_url: "http://localhost:3000/html/images/camera_marker.png",
                activate_url: "http://localhost:3000/html/images/camera_marker_click.png",
            }],
        },
        label: {
            bg_image_url: "http://localhost:3000/html/images/label_bg.png",
            size: "113,26", 
            offset: "11,63", 
            text: "摄像头",
            font_color: "fafafaff", 
            text_boxsize: "82,14",
            text_offset: "25,7",
        },
        window: {
            url: "http://localhost:3000/pages/operations/camera.html?id=24",
            size: "270,155",
            offset: "50,180"
        },
    }]
}