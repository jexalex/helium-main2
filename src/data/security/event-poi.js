export const generateEventPOI = () => {
    let src = [{
            coord: "100.220131,25.628828",
            detail:{
                eventNumber:'SJ1111',
                eventType:'公共卫生',
                eventLevel:'重要',
                eventPosition:'洱海廊道中段',
                eventSubmitTime: '2020-09-04 13:15:00',
                eventSubmitPeople:'摄像头智能识别',
                eventStatus:'已完成',
                eventSlover:'赵伟',
                sloverTel: '15952010165',
                eventGetTime:'2020-10-28 11:20:00',
                eventAskPeopleTime:'2020-10-28  11:20:00',
                goToEventPositionTime:'2020-10-28 11:13:00',
                eventSlovedTime:'2020-10-28 11:30:00',
                monitors:[{
                    id:'monitor_1',
                    name:'单兵监控画面',
                    url:'http://localhost:3000/media/安防事件单兵.mp4'
                }]
            }
        },
        {
            coord: "100.215698,25.638037",
            detail:{
                eventNumber:'SJ1112',
                eventType:'公共卫生',
                eventLevel:'重要',
                eventPosition:'洱海廊道中段',
                eventSubmitTime: '2020-10-28 15:20:00',
                eventSubmitPeople:'摄像头智能识别',
                eventStatus:'处置中',
                eventSlover:'黄明',
                sloverTel: '18051035415',
                eventGetTime:'2020-10-28 15:25:00',
                eventAskPeopleTime:'2020-10-28 15:30:00',
                goToEventPositionTime:'2020-10-28 15:20:00',
                eventSlovedTime:'2020-10-28 15:40:00',
                monitors:[{
                    id:'monitor_1',
                    name:'安防监控画面',
                    url:'http://localhost:3000/media/安防事件摄像头.mp4'
                }]
            }
        },{
            coord: "100.221542,25.630354",
            detail:{
                eventNumber:'SJ1113',
                eventType:'公共卫生',
                eventLevel:'重要',
                eventPosition:'洱海廊道中段',
                eventSubmitTime: '2020-11-28 10:20:00',
                eventSubmitPeople:'摄像头智能识别',
                eventStatus:'待受理',
                eventSlover:'杨达',
                sloverTel: '18051035415',
                eventGetTime:'2020-11-28 13:25:00',
                eventAskPeopleTime:'2020-11-28 15:30:00',
                goToEventPositionTime:'2020-10-28 15:20:00',
                eventSlovedTime:'2020-10-28 15:40:00',
                monitors:[{
                    id:'monitor_1',
                    name:'安防监控画面',
                    url:'http://localhost:3000/media/安防事件摄像头.mp4'
                }]
            }
        },{
            coord: "100.215759,25.638014",
            detail:{
                eventNumber:'SJ1114',
                eventType:'公共安全',
                eventLevel:'重要',
                eventPosition:'洱海廊道中段',
                eventSubmitTime: '2020-12-08 15:20:00',
                eventSubmitPeople:'摄像头智能识别',
                eventStatus:'待处理',
                eventSlover:'朱自红',
                sloverTel: '18051035415',
                eventGetTime:'2020-10-28 15:25:00',
                eventAskPeopleTime:'2020-10-28 15:30:00',
                goToEventPositionTime:'2020-10-28 15:20:00',
                eventSlovedTime:'2020-10-28 15:40:00',
                monitors:[{
                    id:'monitor_1',
                    name:'安防监控画面',
                    url:'http://localhost:3000/media/安防事件摄像头.mp4'
                }]
            }
        },{
            coord: "100.220055,25.628769",
            detail:{
                eventNumber:'SJ1115',
                eventType:'公共卫生',
                eventLevel:'重要',
                eventPosition:'洱海廊道中段',
                eventSubmitTime: '2020-10-28 15:20:00',
                eventSubmitPeople:'摄像头智能识别',
                eventStatus:'已完成',
                eventSlover:'李建明',
                sloverTel: '18051035415',
                eventGetTime:'2020-10-13 15:25:00',
                eventAskPeopleTime:'2020-10-13 15:30:00',
                goToEventPositionTime:'2020-10-13 15:20:00',
                eventSlovedTime:'2020-10-13 15:40:00',
                monitors:[{
                    id:'monitor_1',
                    name:'安防监控画面',
                    url:'http://localhost:3000/media/安防事件摄像头.mp4'
                }]
            }
        },
    ];
    return src.map((o, i) => {
        return {
            id: 'security_event_'+(i+1),
            coord: o.coord,
            coord_type: 0,
            cad_mapkey: '',
            coord_z: 1,
            always_show_label: true,
            sort_order: false,
            state: 'state_'+1,
            marker: {
                size: '46,73',
                images: [{
                    define_state: 'state_1',
                    normal_url: 'http://localhost:3000/images/security_event_marker.png',
                    activate_url: 'http://localhost:3000/images/security_event_marker.png'
                }]
            },
            detail:o.detail
        }
    })
}