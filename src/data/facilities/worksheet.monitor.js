export default [{
    deviceID: 'DG0007',
    deviceType: '智慧灯杆',
    poiId: 'operation_lamp_6',
    deviceChargerName: '洪学军',
    monitoredCount: 1,
    poiData: {
        id: "operation_lamp_6",
        facilityId: "DG0007",
        facilityType: "智慧灯杆",
        type: 'lamp',
        responsibilityPerson: "洪学军",
        responsibilityTelNumber: '16135452621',
        coord: "100.21539452454638,25.636926375155575",
        coord_type: 0,
        cad_mapkey: '',
        coord_z: 1,
        always_show_label: true,
        sort_order: false,
        isAbnormal: true,
        state: 'state_2',
        marker: {
            size: '46,73',
            images: [{
                define_state: 'state_1',
                normal_url: 'http://localhost:3000/images/lamp_marker.png',
                activate_url: 'http://localhost:3000/images/lamp_marker_click.png'
            }, {
                define_state: 'state_2',
                normal_url: 'http://localhost:3000/images/unormal_lamp_marker.png',
                activate_url: 'http://localhost:3000/images/lamp_marker_click.png'
            }]
        },
        window: {
            url: 'http://localhost:3000/pages/facilities/lamp.html?id=operation_lamp_6',
            size: '420,200',
            offset: '30,200'
        }
    }
}, {
    deviceID: 'LP0004',
    deviceType: '户外高亮屏',
    poiId: 'operation_touch_3',
    deviceChargerName: '洪学军',
    monitoredCount: 1,
    poiData: {
        id: "operation_touch_3",
        facilityId: "LP0004",
        facilityType: "户外高亮屏",
        type: 'touch',
        responsibilityPerson: "洪学军",
        coord: "100.21537144559775,25.640963465295393",
        coord_type: 0,
        cad_mapkey: '',
        coord_z: 1,
        always_show_label: true,
        sort_order: false,
        isAbnormal: true,
        state: 'state_2',
        marker: {
            size: '46,73',
            images: [{
                define_state: 'state_1',
                normal_url: 'http://localhost:3000/images/touch_marker.png',
                activate_url: 'http://localhost:3000/images/touch_marker_click.png'
            }, {
                define_state: 'state_2',
                normal_url: 'http://localhost:3000/images/unormal_touch_marker.png',
                activate_url: 'http://localhost:3000/images/touch_marker_click.png'
            }]
        },
        window: {
            url: 'http://localhost:3000/pages/facilities/touch.html?id=operation_touch_3',
            size: '420,200',
            offset: '30,200'
        }
    }
}, {
    deviceID: 'MJ0002',
    deviceType: '智慧井盖',
    poiId: 'operation_well_1',
    deviceChargerName: '洪学军',
    monitoredCount: 1,
    poiData: {
        id: "operation_well_1",
        facilityId: "MJ0002",
        facilityType: "智慧井盖",
        type: 'well',
        responsibilityPerson: "洪学军",
        coord: "100.22090345118602,25.629285911604278",
        coord_type: 0,
        cad_mapkey: '',
        coord_z: 1,
        always_show_label: true,
        isAbnormal: true,
        sort_order: false,
        state: 'state_2',
        marker: {
            size: '46,73',
            images: [{
                define_state: 'state_1',
                normal_url: 'http://localhost:3000/images/well_marker.png',
                activate_url: 'http://localhost:3000/images/well_marker_click.png'
            }, {
                define_state: 'state_2',
                normal_url: 'http://localhost:3000/images/unormal_well_marker.png',
                activate_url: 'http://localhost:3000/images/well_marker_click.png'
            }]
        },
        window: {
            url: 'http://localhost:3000/pages/facilities/toilet.html?id=operation_well_1',
            size: '420,200',
            offset: '30,200'
        }
    }
}]