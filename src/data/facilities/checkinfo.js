/*
 * @Author: your name
 * @Date: 2020-10-13 16:43:23
 * @LastEditTime: 2020-10-14 10:42:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \helium-main\src\data\facilities\checkinfo.js
 */

import {
  timeShift
} from "@/lib/extension";

const poiData = {
  lamp: [{
    "coord": "100.22286276655319,25.62529555634801"
  }, {
    "coord": "100.22261204828146,25.626208329142234"
  }, {
    "coord": "100.22112509993111,25.628547334004182"
  }, {
    "coord": "100.22030183616482,25.630595212539657"
  }, {
    "coord": "100.21807755065684,25.635098550811197"
  }, {
    "coord": "100.21741825452162,25.63554870346782"
  }, {
    "coord": "100.21539452454638,25.636926375155575"
  }, {
    "coord": "100.21532722441438,25.637132490241274"
  }, {
    "coord": "100.2161090376128,25.638247596132075"
  }, {
    "coord": "100.21626101112558,25.639689768196597"
  }, {
    "coord": "100.21539194602151,25.640917364569905"
  }],
  toilet: [{
      "coord": "100.215889,25.638157"
    },
    {
      "coord": "100.215935,25.639706"
    }
  ],
  touch: [{
    "coord": "100.22280197002401,25.62532415016893"
  }, {
    "coord": "100.22017458917026,25.630892872752792"
  }, {
    "coord": "100.21532521655584,25.63713162788648"
  }, {
    "coord": "100.21537144559775,25.640963465295393"
  }],
  carbinet: [{
      "coord": "100.222481,25.628698"
    },
    {
      "coord": "100.221718,25.62989"
    },
    {
      "coord": "100.220078,25.631771"
    },
    {
      "coord": "100.218353,25.634003"
    },
    {
      "coord": "100.218109,25.635914"
    }
  ],
  camera: [{
      "coord": "100.222618,25.625746"
    },
    {
      "coord": "100.22185495092482,25.630206365705874"
    },
    {
      "coord": "100.22020603203897,25.630769222947666"
    },
    {
      "coord": "100.21892703911239,25.63389553755106"
    },
    {
      "coord": "100.2160947669507,25.636115906106788"
    },
    {
      "coord": "100.21638431756286,25.638588188294992"
    },
    {
      "coord": "100.21538011610546,25.641715455825757"
    },
    {
      "coord": "100.21503694523422,25.642912187745303"
    },
    {
      "coord": "100.21434980154402,25.6434655253107"
    },
    {
      "coord": "100.21441314586602,25.644383603415772"
    },
    {
      "coord": "100.21359922439379,25.644957669192447"
    },
    {
      "coord": "100.21346672257366,25.646490216911204"
    }
  ],
  well: [{
    "coord": "100.22042961228296,25.629545556208242"
  }, {
    "coord": "100.22072863829574,25.629375743387616"
  }, {
    "coord": "100.22090345118602,25.629285911604278"
  }, {
    "coord": "100.22089729196861,25.629374498279134"
  }, {
    "coord": "100.22108018651902,25.6293516758989"
  }, {
    "coord": "100.22131785997074,25.629554997789608"
  }, {
    "coord": "100.22146885300229,25.629723187411585"
  }, {
    "coord": "100.22161021231175,25.62980705120591"
  }, {
    "coord": "100.22165483349973,25.629954892630476"
  }, {
    "coord": "100.22179501147725,25.630121314746052"
  }, {
    "coord": "100.2217071856406,25.630283211199284"
  }, {
    "coord": "100.22156875485832,25.63037940780209"
  }, {
    "coord": "100.22127937196224,25.630230717578605"
  }, {
    "coord": "100.22100888260071,25.63028535412926"
  }, {
    "coord": "100.2206132754964,25.63033943615374"
  }, {
    "coord": "100.22030686409148,25.630489838672112"
  }, {
    "coord": "100.22013041832724,25.630657366189677"
  }, {
    "coord": "100.22008579045057,25.630811656476595"
  }, {
    "coord": "100.22007649867594,25.630909733916507"
  }, {
    "coord": "100.21998590905278,25.631070521082908"
  }, {
    "coord": "100.21991347613802,25.63127146487671"
  }, {
    "coord": "100.21992270396106,25.631475960047787"
  }, {
    "coord": "100.21995272925014,25.631476817847098"
  }, {
    "coord": "100.21997359771842,25.63156524438877"
  }, {
    "coord": "100.21999494088244,25.63168510777119"
  }, {
    "coord": "100.22000276854959,25.631740393588316"
  }, {
    "coord": "100.22002801373567,25.631919149450894"
  }, {
    "coord": "100.2200434275469,25.632097401949753"
  }, {
    "coord": "100.22005198401708,25.63229883413891"
  }, {
    "coord": "100.21997685900647,25.63245035488254"
  }, {
    "coord": "100.2200027207258,25.632464200705563"
  }, {
    "coord": "100.21974490180894,25.632821699086737"
  }, {
    "coord": "100.21952794631119,25.63301026722746"
  }, {
    "coord": "100.21914071994121,25.633176988798137"
  }, {
    "coord": "100.21890177428273,25.633277074959224"
  }, {
    "coord": "100.21872668185559,25.633391704987"
  }, {
    "coord": "100.21861996690309,25.63356610878937"
  }, {
    "coord": "100.21855654486725,25.633745651128557"
  }, {
    "coord": "100.21862577677028,25.633908706121957"
  }, {
    "coord": "100.21849898467647,25.63419056582004"
  }, {
    "coord": "100.21829150005762,25.634455133985817"
  }, {
    "coord": "100.2181813903198,25.63463729769615"
  }, {
    "coord": "100.21814389116332,25.634764850884956"
  }, {
    "coord": "100.21809730593668,25.634947850097717"
  }, {
    "coord": "100.21792379973549,25.635078563649387"
  }, {
    "coord": "100.21759627707075,25.63527441942454"
  }, {
    "coord": "100.21737663170505,25.635466604836104"
  }, {
    "coord": "100.21722597205508,25.635490417543625"
  }, {
    "coord": "100.2170186677814,25.635400462858843"
  }, {
    "coord": "100.21685804383664,25.63534684740881"
  }, {
    "coord": "100.21667090155478,25.63533671417798"
  }, {
    "coord": "100.21644495646994,25.635257276699942"
  }, {
    "coord": "100.21628460703784,25.635312362639052"
  }, {
    "coord": "100.2161345467456,25.63543041810529"
  }, {
    "coord": "100.21605934745499,25.63560275421424"
  }, {
    "coord": "100.21602759086812,25.635787414319335"
  }, {
    "coord": "100.21602500849717,25.63596577027034"
  }, {
    "coord": "100.21598718531445,25.63609126739891"
  }, {
    "coord": "100.21582133593807,25.636304004679396"
  }, {
    "coord": "100.21566631768208,25.636541259960698"
  }, {
    "coord": "100.21549605396855,25.63665327058301"
  }, {
    "coord": "100.21533689357953,25.636840825497174"
  }, {
    "coord": "100.21526555734641,25.63703211964687"
  }]
}
export const checkDevice = [{
    id: "ci_lamp_01",
    type: 'lamp',
    tabName: "智慧灯杆",
    title: "智慧灯杆",
    totalNum: 12,
    default: true,
    waitCheckNum: 10,
    needPatrolNum: 1,
    resCheckManNum: 3,
    table: poiData.lamp.map((o, i) => {
      return {
        id: 'operation_lamp_' + i,
        facilityId: i + 1 < 10 ? "DG000" + (i + 1) : 'DG00' + (i + 1),
        lastFixTime: '暂无记录',
        responsibilityPerson: "洪学军",
        responsibilityTelNumber: '16135452621',
        type: 'lamp',
        runStatus: "待检查",
        poi: {
          id: 'operation_lamp_' + i,
          coord: o.coord,
          coord_type: 0,
          cad_mapkey: '',
          coord_z: 1,
          always_show_label: true,
          sort_order: false,
          isAbnormal: i === 6 ? true : false,
          state: i === 6 ? 'state_2' : 'state_1',
          marker: {
            size: '46,73',
            images: [{
              define_state: 'state_1',
              normal_url: 'http://localhost:3000/images/lamp_marker.png',
              activate_url: 'http://localhost:3000/images/lamp_marker_click.png'
            }, {
              define_state: 'state_2',
              normal_url: 'http://localhost:3000/images/unormal_lamp_marker.png',
              activate_url: 'http://localhost:3000/images/lamp_marker_click.png'
            }]
          },
          window: {
            url: 'http://localhost:3000/pages/facilities/lamp.repairlist.html?id=operation_lamp_' + i,
            size: '420,155',
            offset: '30,200'
          }
        }
      }
    }),
  },
  {
    id: "ci_toilet_01",
    type: 'toilet',
    tabName: "智慧厕所",
    title: "智慧厕所",
    totalNum: 15,
    waitCheckNum: 3,
    needPatrolNum: 2,
    resCheckManNum: 1,
    table: poiData.toilet.map((o, i) => {
      return {
        id: 'operation_toilet_' + i,
        facilityId: "CS100" + i,
        lastFixTime: '暂无记录',
        responsibilityPerson: "洪学军",
        responsibilityTelNumber: '16135452621',
        runStatus: "待检查",
        type: 'toilet',
        poi: {
          id: 'operation_toilet_' + i,
          coord: o.coord,
          coord_type: 0,
          cad_mapkey: '',
          coord_z: 1,
          always_show_label: true,
          sort_order: false,
          isAbnormal: i === 0 ? false : true,
          state: i === 0 ? 'state_1' : 'state_2',
          marker: {
            size: '46,73',
            images: [{
              define_state: 'state_1',
              normal_url: 'http://localhost:3000/images/toilet_marker.png',
              activate_url: 'http://localhost:3000/images/toilet_marker_click.png'
            }, {
              define_state: 'state_2',
              normal_url: 'http://localhost:3000/images/unormal_toilet_marker.png',
              activate_url: 'http://localhost:3000/images/toilet_marker_click.png'
            }]
          },
          window: {
            url: 'http://localhost:3000/pages/facilities/toilet.repairlist.html?id=operation_toilet_' + i,
            size: '420,155',
            offset: '30,200'
          }
        }
      }
    }),

  },
  {
    id: "ci_touch_01",
    type: 'touch',
    tabName: "户外高亮屏",
    title: "户外高亮屏",
    totalNum: 15,
    waitCheckNum: 3,
    needPatrolNum: 2,
    resCheckManNum: 1,
    table: poiData.touch.map((o, i) => {
      return {
        id: 'operation_touch_' + i,
        facilityId: i + 1 < 10 ? "LP000" + i : 'LP00' + i,
        lastFixTime: '暂无信息',
        responsibilityPerson: "洪学军",
        responsibilityTelNumber: '16135452621',
        runStatus: "待检查",
        type: 'touch',
        poi: {
          id: 'operation_touch_' + i,
          coord: o.coord,
          coord_type: 0,
          cad_mapkey: '',
          coord_z: 1,
          always_show_label: true,
          sort_order: false,
          isAbnormal: i === 3 ? true : false,
          state: i === 3 ? 'state_2' : 'state_1',
          marker: {
            size: '46,73',
            images: [{
              define_state: 'state_1',
              normal_url: 'http://localhost:3000/images/touch_marker.png',
              activate_url: 'http://localhost:3000/images/touch_marker_click.png'
            }, {
              define_state: 'state_2',
              normal_url: 'http://localhost:3000/images/unormal_touch_marker.png',
              activate_url: 'http://localhost:3000/images/touch_marker_click.png'
            }]
          },
          window: {
            url: 'http://localhost:3000/pages/facilities/touch.repairlist.html?id=operation_touch_' + i,
            size: '420,155',
            offset: '30,200'
          }
        }
      }
    })
  },
  {
    id: "ci_well_01",
    type: 'well',
    tabName: "智慧井盖",
    title: "智慧井盖",
    totalNum: 63,
    waitCheckNum: 1,
    needPatrolNum: 2,
    resCheckManNum: 1,
    table: poiData.well.map((o, i) => {
      return {
        id: 'operation_well_' + i,
        facilityId: i + 1 < 10 ? "MJ-000" + i : 'MJ-00' + i,
        lastFixTime: '暂无信息',
        responsibilityPerson: "洪学军",
        responsibilityTelNumber: '16135452621',
        runStatus: "待检查",
        type: 'well',
        poi: {
          id: 'operation_well_' + i,
          coord: o.coord,
          coord_type: 0,
          cad_mapkey: '',
          coord_z: 1,
          always_show_label: true,
          sort_order: false,
          isAbnormal: i === 3 ? true : false,
          state: i === 3 ? 'state_2' : 'state_1',
          marker: {
            size: '46,73',
            images: [{
              define_state: 'state_1',
              normal_url: 'http://localhost:3000/images/well_marker.png',
              activate_url: 'http://localhost:3000/images/well_marker_click.png'
            }, {
              define_state: 'state_2',
              normal_url: 'http://localhost:3000/images/unormal_well_marker.png',
              activate_url: 'http://localhost:3000/images/well_marker_click.png'
            }]
          },
          window: {
            url: 'http://localhost:3000/pages/facilities/well.repairlist.html?id=operation_well_' + i,
            size: '420,155',
            offset: '30,200'
          }
        }
      }
    })
  },
  {
    id: "ci_camera_01",
    type: 'camera',
    tabName: "摄像头",
    title: "摄像头",
    totalNum: 15,
    waitCheckNum: 3,
    needPatrolNum: 2,
    resCheckManNum: 3,
    table: poiData.camera.map((o, i) => {
      return {
        id: 'operation_camera_' + i,
        facilityId: "CMRA-0" + i,
        lastFixTime: timeShift(37 * i * 1706),
        responsibilityPerson: "洪学军",
        runStatus: "待检查",
        type: 'camera',
        poi: {
          id: 'operation_camera_' + i,
          coord: o.coord,
          coord_type: 0,
          cad_mapkey: '',
          coord_z: 1,
          always_show_label: true,
          sort_order: false,
          state: 'state_1',
          marker: {
            size: '46,73',
            images: [{
              define_state: 'state_1',
              normal_url: 'http://localhost:3000/images/camera_marker.png',
              activate_url: 'http://localhost:3000/images/camera_marker_click.png'
            }]
          },
          window: {
            url: 'http://localhost:3000/pages/facilities/camera.repairlist.html?id=' + i,
            size: '270,155',
            offset: '30,200'
          }
        }
      }
    })
  },
];