/*
 * @Author: your name
 * @Date: 2020-10-13 10:03:10
 * @LastEditTime: 2020-10-15 16:35:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \helium-main\src\data\facilities\streetlamp.js
 */
export const getStreetLampPOI = () => {
  let src = [{
    "coord": "100.22286276655319,25.62529555634801"
  }, {
    "coord": "100.22261204828146,25.626208329142234"
  }, {
    "coord": "100.22112509993111,25.628547334004182"
  }, {
    "coord": "100.22030183616482,25.630595212539657"
  }, {
    "coord": "100.21807755065684,25.635098550811197"
  }, {
    "coord": "100.21741825452162,25.63554870346782"
  }, {
    "coord": "100.21539452454638,25.636926375155575"
  }, {
    "coord": "100.21532722441438,25.637132490241274"
  }, {
    "coord": "100.2161090376128,25.638247596132075"
  }, {
    "coord": "100.21626101112558,25.639689768196597"
  }, {
    "coord": "100.21539194602151,25.640917364569905"
  }]
  return src.map((o, i) => {
    return {
      id: 'operation_lamp_' + i,
      coord: o.coord,
      coord_type: 0,
      cad_mapkey: '',
      coord_z: 1,
      always_show_label: true,
      sort_order: false,
      isAbnormal: i === 6 ? true : false,
      state: i === 6 ? 'state_2' : 'state_1',
      marker: {
        size: '46,73',
        images: [{
          define_state: 'state_1',
          normal_url: 'http://localhost:3000/images/lamp_marker.png',
          activate_url: 'http://localhost:3000/images/lamp_marker_click.png'
        }, {
          define_state: 'state_2',
          normal_url: 'http://localhost:3000/images/unormal_lamp_marker.png',
          activate_url: 'http://localhost:3000/images/lamp_marker_click.png'
        }]
      },
      window: {
        url: 'http://localhost:3000/pages/facilities/lamp.html?id=operation_lamp_' + i,
        size: '420,130',
        offset: '30,200'
      }
    }
  })
}