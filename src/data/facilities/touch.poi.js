/*
 * @Author: your name
 * @Date: 2020-10-13 15:20:17
 * @LastEditTime: 2020-10-13 15:22:11
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \helium-main\src\data\facilities\touch.poi.js
 */
export const generateTouchPOI = () => {
  let src = [{
    "coord": "100.22280197002401,25.62532415016893"
  }, {
    "coord": "100.22017458917026,25.630892872752792"
  }, {
    "coord": "100.21532521655584,25.63713162788648"
  }, {
    "coord": "100.21537144559775,25.640963465295393"
  }]
  return src.map((o, i) => {
    return {
      id: 'operation_touch_' + i,
      coord: o.coord,
      coord_type: 0,
      cad_mapkey: '',
      coord_z: 1,
      always_show_label: true,
      sort_order: false,
      isAbnormal: i === 3 ? true : false,
      state: i === 3 ? 'state_2' : 'state_1',
      marker: {
        size: '46,73',
        images: [{
          define_state: 'state_1',
          normal_url: 'http://localhost:3000/images/touch_marker.png',
          activate_url: 'http://localhost:3000/images/touch_marker_click.png'
        }, {
          define_state: 'state_2',
          normal_url: 'http://localhost:3000/images/unormal_touch_marker.png',
          activate_url: 'http://localhost:3000/images/touch_marker_click.png'
        }]
      },
      window: {
        url: 'http://localhost:3000/pages/facilities/touch.html?id=operation_touch_' + i,
        size: '420,160',
        offset: '30,200'
      }
    }
  })
}