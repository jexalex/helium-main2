/*
 * @Author: your name
 * @Date: 2020-10-13 14:54:43
 * @LastEditTime: 2020-10-13 15:10:38
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \helium-main\src\data\facilities\toilet.poi.js
 */
export const generateToiletPOI = () => {
  let src = [{
      "coord": "100.215889,25.638157"
    },
    {
      "coord": "100.215935,25.639706"
    }
  ]
  return src.map((o, i) => {
    return {
      id: 'operation_toilet_' + i,
      coord: o.coord,
      coord_type: 0,
      cad_mapkey: '',
      coord_z: 1,
      always_show_label: true,
      sort_order: false,
      isAbnormal:false,
      state: 'state_1',
      marker: {
        size: '46,73',
        images: [{
          define_state: 'state_1',
          normal_url: 'http://localhost:3000/images/toilet_marker.png',
          activate_url: 'http://localhost:3000/images/toilet_marker_click.png'
        }, {
          define_state: 'state_2',
          normal_url: 'http://localhost:3000/images/unormal_toilet_marker.png',
          activate_url: 'http://localhost:3000/images/toilet_marker_click.png'
        }]
      },
      window: {
        url: 'http://localhost:3000/pages/facilities/toilet.html?id=operation_toilet_' + i,
        size: '420,200',
        offset: '30,200'
      }
    }
  })
}