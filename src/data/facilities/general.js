const statByCategory = [{
    type:'lamp',
    typeName:'智慧杆',
    totalCount:11,
    normalCount:10,
    failedCount:1,
    worksheet:1,
    repairedCount:1
},{
    type:'camera',
    typeName:'摄像头',
    totalCount:12,
    normalCount:12,
    failedCount:0,
    worksheet:0,
    repairedCount:0
},{
    type:'well',
    typeName:'智慧井盖',
    totalCount:63,
    normalCount:62,
    failedCount:1,
    worksheet:1,
    repairedCount:2
}];

export const generateBaseInfo = () => {
    return {
        facilitiesTotalCount:statByCategory.reduce((total,current)=>{
            return total + current.totalCount;
        },0),
        facilitiesNormalCount:statByCategory.reduce((total,current)=>{
            return total + current.normalCount;
        },0),
        facilitiesFailedCount:statByCategory.reduce((total,current)=>{
            return total + current.failedCount;
        },0),
        facilitiesWorksheetCount:statByCategory.reduce((total,current)=>{
            return total + current.worksheet;
        },0),
        statData:statByCategory
    }
}