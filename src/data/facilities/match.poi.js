/*
 * @Author: your name
 * @Date: 2020-10-15 11:02:39
 * @LastEditTime: 2020-10-15 14:47:53
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \helium-main\src\data\facilities\match.poi.js
 */
export const matchListPOI = [{
    id: 'lm_worker_1',
    coord: "100.222672,25.625349",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=孙荣海&number=XJ1001&tel=13950035537&group=巡检一组&id=lm_worker_1',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_2',
    coord: "100.222801,25.62538",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=李胜哲&number=XJ1002&tel=13966427550&group=巡检一组&id=lm_worker_2',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_3',
    coord: "100.222633,25.625591",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=孙艳艳&number=XJ1003&tel=13947766628&group=巡检一组&id=lm_worker_3',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_4',
    coord: "100.222588,25.626005",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=李馨&number=XJ1004&tel=13916882701&group=巡检一组&id=lm_worker_4',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_5',
    coord: "100.222542,25.626289",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=张志军&number=XJ1005&tel=13961332155&group=巡检一组&id=lm_worker_5',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_6',
    coord: "100.222282,25.627132",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=张厚宝&number=XJ1006&tel=13937401011&group=巡检二组&id=lm_worker_6',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_7',
    coord: "100.221115,25.628481",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=庞文涛&number=XJ1007&tel=13964507501&group=巡检二组&id=lm_worker_7',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_8',
    coord: "100.220955,25.630266",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=宋爽&number=XJ1008&tel=13901784558&group=巡检二组&id=lm_worker_8',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_9',
    coord: "100.219513,25.632971",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=胡婷&number=XJ1009&tel=13943250086&group=巡检二组&id=lm_worker_9',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_10',
    coord: "100.218018,25.634968",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=王金国&number=XJ1010&tel=18916988261&group=巡检二组&id=lm_worker_10',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_11',
    coord: "100.216103,25.635727",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=孙荣海&number=XJ1011&tel=13950035537&group=巡检一组&id=lm_worker_11',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_12',
    coord: "100.215286,25.63698",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=李胜哲&number=XJ1012&tel=13966427550&group=巡检一组&id=lm_worker_12',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_13',
    coord: "100.216232,25.638308",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=孙艳艳&number=XJ1013&tel=13947766628&group=巡检一组&id=lm_worker_13',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_14',
    coord: "100.215355,25.640772",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=李馨&number=XJ1014&tel=13916882701&group=巡检一组&id=lm_worker_14',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_15',
    coord: "100.214729,25.642408",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=张志军&number=XJ1015&tel=13961332155&group=巡检一组&id=lm_worker_15',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_16',
    coord: "100.214287,25.643398",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=张厚宝&number=XJ1016&tel=13937401011&group=巡检二组&id=lm_worker_16',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_17',
    coord: "100.213539,25.644354",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=庞文涛&number=XJ1017&tel=13964507501&group=巡检二组&id=lm_worker_17',
      size: '290,185',
      offset: '30,200'
    }
  }, {
    id: 'lm_worker_18',
    coord: "100.21357,25.645584",
    coord_type: 0,
    cad_mapkey: '',
    coord_z: 1,
    always_show_label: true,
    type: 'worker',
    sort_order: false,
    state: 'state_1',
    marker: {
      size: '46,73',
      images: [{
        define_state: 'state_1',
        normal_url: 'http://localhost:3000/html/images/worker_marker.png',
        activate_url: 'http://localhost:3000/images/worker_marker_click.png'
      }]
    },
    window: {
      url: 'http://localhost:3000/pages/facilities/worker.html?name=宋爽&number=XJ1018&tel=13901784558&group=巡检二组&id=lm_worker_18',
      size: '290,185',
      offset: '30,200'
    }
  },
  {
    "id": "operation_camera_0",
    "coord": "100.222618,25.625746",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=0",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_1",
    "coord": "100.22185495092482,25.630206365705874",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=1",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_2",
    "coord": "100.22020603203897,25.630769222947666",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=2",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_3",
    "coord": "100.21892703911239,25.63389553755106",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=3",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_4",
    "coord": "100.2160947669507,25.636115906106788",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=4",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_5",
    "coord": "100.21638431756286,25.638588188294992",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=5",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_6",
    "coord": "100.21538011610546,25.641715455825757",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=6",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_7",
    "coord": "100.21503694523422,25.642912187745303",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=7",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_8",
    "coord": "100.21434980154402,25.6434655253107",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=8",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_9",
    "coord": "100.21441314586602,25.644383603415772",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=9",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_10",
    "coord": "100.21359922439379,25.644957669192447",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=10",
      "size": "270,155",
      "offset": "30,200"
    }
  }, {
    "id": "operation_camera_11",
    "coord": "100.21346672257366,25.646490216911204",
    "coord_type": 0,
    "cad_mapkey": "",
    "coord_z": 1,
    "always_show_label": true,
    "type": "camera",
    "sort_order": false,
    "state": "state_1",
    "marker": {
      "size": "46,73",
      "images": [{
        "define_state": "state_1",
        "normal_url": "http://localhost:3000/images/camera_marker.png",
        "activate_url": "http://localhost:3000/images/camera_marker_click.png"
      }]
    },
    "window": {
      "url": "http://localhost:3000/pages/operations/camera.html?id=11",
      "size": "270,155",
      "offset": "30,200"
    }
  }
];
// type = Worker or camera