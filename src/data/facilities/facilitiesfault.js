export const facilitiesFalutTable = () => {
  return [{
      id: "operation_lamp_6",
      facilityId: "DG0007",
      facilityType: "智慧灯杆",
      type: 'lamp',
      responsibilityPerson: "洪学军",
      responsibilityTelNumber: '16135452621',
      coord: "100.21539452454638,25.636926375155575",
      coord_type: 0,
      cad_mapkey: '',
      coord_z: 1,
      always_show_label: true,
      sort_order: false,
      isAbnormal: true,
      state: 'state_2',
      marker: {
        size: '46,73',
        images: [{
          define_state: 'state_1',
          normal_url: 'http://localhost:3000/images/lamp_marker.png',
          activate_url: 'http://localhost:3000/images/lamp_marker_click.png'
        }, {
          define_state: 'state_2',
          normal_url: 'http://localhost:3000/images/unormal_lamp_marker.png',
          activate_url: 'http://localhost:3000/images/lamp_marker_click.png'
        }]
      },
      window: {
        url: 'http://localhost:3000/pages/facilities/lamp.html?id=operation_lamp_6',
        size: '420,200',
        offset: '30,200'
      },
      checker: {
        id: 'lm_worker_12',
        coord: "100.215286,25.63698",
        coord_type: 0,
        cad_mapkey: '',
        coord_z: 1,
        always_show_label: true,
        type: 'worker',
        sort_order: false,
        state: 'state_1',
        marker: {
          size: '46,73',
          images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
          }]
        },
        window: {
          url: 'http://localhost:3000/pages/facilities/worker.html?name=李胜哲&number=XJ1012&matchTo=operation_lamp_5&tel=13966427550&group=巡检一组&id=lm_worker_12',
          size: '290,185',
          offset: '30,200'
        }
      },
      checkPath: [{
          "coord": "100.215286,25.636971"
        },
        {
          "coord": "100.215401,25.636925"
        }
      ]
    },
    {
      id: "operation_touch_3",
      facilityId: "LP0004",
      facilityType: "户外高亮屏",
      type: 'touch',
      responsibilityPerson: "洪学军",
      coord: "100.21537144559775,25.640963465295393",
      coord_type: 0,
      cad_mapkey: '',
      coord_z: 1,
      always_show_label: true,
      sort_order: false,
      isAbnormal: true,
      state: 'state_2',
      marker: {
        size: '46,73',
        images: [{
          define_state: 'state_1',
          normal_url: 'http://localhost:3000/images/touch_marker.png',
          activate_url: 'http://localhost:3000/images/touch_marker_click.png'
        }, {
          define_state: 'state_2',
          normal_url: 'http://localhost:3000/images/unormal_touch_marker.png',
          activate_url: 'http://localhost:3000/images/touch_marker_click.png'
        }]
      },
      window: {
        url: 'http://localhost:3000/pages/facilities/touch.html?id=operation_touch_3',
        size: '420,200',
        offset: '30,200'
      },
      checker: {
        id: 'lm_worker_14',
        coord: "100.215355,25.640772",
        coord_type: 0,
        cad_mapkey: '',
        coord_z: 1,
        always_show_label: true,
        type: 'worker',
        sort_order: false,
        state: 'state_1',
        marker: {
          size: '46,73',
          images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
          }]
        },
        window: {
          url: 'http://localhost:3000/pages/facilities/worker.html?name=李馨&number=XJ1014&tel=13916882701&group=巡检一组&id=lm_worker_14',
          size: '290,185',
          offset: '30,200'
        }
      },
      checkPath: [{
          "coord": "100.215355,25.640774"
        },
        {
          "coord": "100.21534,25.640844"
        },
        {
          "coord": "100.21534,25.6409"
        },
        {
          "coord": "100.215363,25.640959"
        }
      ]
    },
    {
      id: "operation_well_2",
      facilityId: "MJ0002",
      facilityType: "智慧井盖",
      type: 'well',
      responsibilityPerson: "洪学军",

      coord: "100.22090345118602,25.629285911604278",
      coord_type: 0,
      cad_mapkey: '',
      coord_z: 1,
      always_show_label: true,
      isAbnormal: true,
      sort_order: false,
      state: 'state_2',
      marker: {
        size: '46,73',
        images: [{
          define_state: 'state_1',
          normal_url: 'http://localhost:3000/images/well_marker.png',
          activate_url: 'http://localhost:3000/images/well_marker_click.png'
        }, {
          define_state: 'state_2',
          normal_url: 'http://localhost:3000/images/unormal_well_marker.png',
          activate_url: 'http://localhost:3000/images/well_marker_click.png'
        }]
      },
      window: {
        url: 'http://localhost:3000/pages/facilities/toilet.html?id=operation_well_1',
        size: '420,200',
        offset: '30,200'
      },
      checker: {
        id: 'lm_worker_7',
        coord: "100.221115,25.628481",
        coord_type: 0,
        cad_mapkey: '',
        coord_z: 1,
        always_show_label: true,
        type: 'worker',
        sort_order: false,
        state: 'state_1',
        marker: {
          size: '46,73',
          images: [{
            define_state: 'state_1',
            normal_url: 'http://localhost:3000/html/images/worker_marker.png',
            activate_url: 'http://localhost:3000/images/worker_marker_click.png'
          }]
        },
        window: {
          url: 'http://localhost:3000/pages/facilities/worker.html?name=庞文涛&number=XJ1007&tel=13964507501&group=巡检二组&id=lm_worker_7',
          size: '290,185',
          offset: '30,200'
        }
      },
      checkPath: [{
          "coord": "100.221085,25.628513"
        },
        {
          "coord": "100.221085,25.628588"
        },
        {
          "coord": "100.2211,25.628672"
        },
        {
          "coord": "100.221123,25.628777"
        },
        {
          "coord": "100.221115,25.628845"
        },
        {
          "coord": "100.221153,25.628944"
        },
        {
          "coord": "100.221207,25.629009"
        },
        {
          "coord": "100.221268,25.629082"
        },
        {
          "coord": "100.221329,25.629152"
        },
        {
          "coord": "100.221367,25.629232"
        },
        {
          "coord": "100.221321,25.629305"
        },
        {
          "coord": "100.22123,25.629326"
        },
        {
          "coord": "100.221153,25.629257"
        },
        {
          "coord": "100.221069,25.629187"
        },
        {
          "coord": "100.220985,25.629166"
        },
        {
          "coord": "100.220924,25.62919"
        },
        {
          "coord": "100.220924,25.629284"
        }
      ]
    },
  ];
};