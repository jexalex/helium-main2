let src = [{
            "type": "Feature",
            "id": 1,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.23079054614686,
                    25.602272694029637
                ]
            },
            "properties": {
                "OBJECTID": 1,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 2,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.24613841665467,
                    25.608929765396397
                ]
            },
            "properties": {
                "OBJECTID": 2,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 3,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.24916255520782,
                    25.60872518941602
                ]
            },
            "properties": {
                "OBJECTID": 3,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 4,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.25697675277496,
                    25.61047017815622
                ]
            },
            "properties": {
                "OBJECTID": 4,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 5,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.25732370672375,
                    25.610539744313826
                ]
            },
            "properties": {
                "OBJECTID": 5,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 6,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.25768627739984,
                    25.610583293983893
                ]
            },
            "properties": {
                "OBJECTID": 6,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
         {
             "type": "Feature",
             "id": 7,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26505816590168,
                     25.612250190993564
                 ]
             },
             "properties": {
                 "OBJECTID": 7,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 8,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2660848607278,
                     25.612623774767428
                 ]
             },
             "properties": {
                 "OBJECTID": 8,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 9,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26621222901122,
                     25.613735334118417
                 ]
             },
             "properties": {
                 "OBJECTID": 9,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 10,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26613705917799,
                     25.613813355701836
                 ]
             },
             "properties": {
                 "OBJECTID": 10,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 11,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26602779784463,
                     25.613849188289464
                 ]
             },
             "properties": {
                 "OBJECTID": 11,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 12,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26600778702982,
                     25.613854167835655
                 ]
             },
             "properties": {
                 "OBJECTID": 12,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 13,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26586508710415,
                     25.613885060447217
                 ]
             },
             "properties": {
                 "OBJECTID": 13,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 14,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2658414790011,
                     25.614332355353952
                 ]
             },
             "properties": {
                 "OBJECTID": 14,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 15,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26580315709009,
                     25.614372129670073
                 ]
             },
             "properties": {
                 "OBJECTID": 15,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 16,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26551438298247,
                     25.614455512112272
                 ]
             },
             "properties": {
                 "OBJECTID": 16,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 17,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26593042914607,
                     25.615158707308524
                 ]
             },
             "properties": {
                 "OBJECTID": 17,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 18,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26596691823875,
                     25.615161323436382
                 ]
             },
             "properties": {
                 "OBJECTID": 18,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 19,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26600307368295,
                     25.615162362153342
                 ]
             },
             "properties": {
                 "OBJECTID": 19,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 20,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26601279895158,
                     25.615163212912023
                 ]
             },
             "properties": {
                 "OBJECTID": 20,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 21,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26603148416569,
                     25.615166519719139
                 ]
             },
             "properties": {
                 "OBJECTID": 21,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 22,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26619771755071,
                     25.615235257601114
                 ]
             },
             "properties": {
                 "OBJECTID": 22,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 23,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2663220308371,
                     25.615402354335231
                 ]
             },
             "properties": {
                 "OBJECTID": 23,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 24,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26634454086792,
                     25.615503921968468
                 ]
             },
             "properties": {
                 "OBJECTID": 24,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 25,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26631667987095,
                     25.615559963221756
                 ]
             },
             "properties": {
                 "OBJECTID": 25,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 26,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26597753563482,
                     25.615899143430795
                 ]
             },
             "properties": {
                 "OBJECTID": 26,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 27,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26594225523098,
                     25.615911965065209
                 ]
             },
             "properties": {
                 "OBJECTID": 27,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 28,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26593548333597,
                     25.615932796061713
                 ]
             },
             "properties": {
                 "OBJECTID": 28,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 29,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26582991731686,
                     25.616086231194686
                 ]
             },
             "properties": {
                 "OBJECTID": 29,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 30,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26568709778138,
                     25.616292879213063
                 ]
             },
             "properties": {
                 "OBJECTID": 30,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 31,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26566857624385,
                     25.616313423325892
                 ]
             },
             "properties": {
                 "OBJECTID": 31,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 32,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26543529570239,
                     25.616547747080915
                 ]
             },
             "properties": {
                 "OBJECTID": 32,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 33,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26530244155447,
                     25.61682014813141
                 ]
             },
             "properties": {
                 "OBJECTID": 33,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 34,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26531784514248,
                     25.616874754066714
                 ]
             },
             "properties": {
                 "OBJECTID": 34,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 35,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26531570025941,
                     25.61706711095843
                 ]
             },
             "properties": {
                 "OBJECTID": 35,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 36,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26531412824448,
                     25.617075853268091
                 ]
             },
             "properties": {
                 "OBJECTID": 36,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 37,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26530397939518,
                     25.617113307333341
                 ]
             },
             "properties": {
                 "OBJECTID": 37,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 38,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26527159570765,
                     25.617169463699838
                 ]
             },
             "properties": {
                 "OBJECTID": 38,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 39,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26526250176312,
                     25.617178101688069
                 ]
             },
             "properties": {
                 "OBJECTID": 39,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 40,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26517667856092,
                     25.617244107429656
                 ]
             },
             "properties": {
                 "OBJECTID": 40,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 41,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26517420002938,
                     25.617245195609314
                 ]
             },
             "properties": {
                 "OBJECTID": 41,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 42,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26505992497562,
                     25.617289768707906
                 ]
             },
             "properties": {
                 "OBJECTID": 42,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 43,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26503080402847,
                     25.61729242080861
                 ]
             },
             "properties": {
                 "OBJECTID": 43,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 44,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26502104008898,
                     25.61729214921337
                 ]
             },
             "properties": {
                 "OBJECTID": 44,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 45,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26496474342821,
                     25.617278578443688
                 ]
             },
             "properties": {
                 "OBJECTID": 45,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 46,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26473165084508,
                     25.61734613011987
                 ]
             },
             "properties": {
                 "OBJECTID": 46,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 47,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26417988619284,
                     25.618211295914648
                 ]
             },
             "properties": {
                 "OBJECTID": 47,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 48,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26415393625524,
                     25.618271235728969
                 ]
             },
             "properties": {
                 "OBJECTID": 48,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 49,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26412739906027,
                     25.618310831979386
                 ]
             },
             "properties": {
                 "OBJECTID": 49,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 50,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26376629787643,
                     25.618867679599759
                 ]
             },
             "properties": {
                 "OBJECTID": 50,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 51,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26375996035392,
                     25.618877132373825
                 ]
             },
             "properties": {
                 "OBJECTID": 51,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 52,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26374810099412,
                     25.618891198669928
                 ]
             },
             "properties": {
                 "OBJECTID": 52,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 53,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26374143611844,
                     25.618897674688014
                 ]
             },
             "properties": {
                 "OBJECTID": 53,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 54,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26363305612068,
                     25.618937424722446
                 ]
             },
             "properties": {
                 "OBJECTID": 54,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 55,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26358198811829,
                     25.619019592180507
                 ]
             },
             "properties": {
                 "OBJECTID": 55,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 56,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26357834136741,
                     25.619040871039431
                 ]
             },
             "properties": {
                 "OBJECTID": 56,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 57,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26381135930683,
                     25.619194638022179
                 ]
             },
             "properties": {
                 "OBJECTID": 57,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 58,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2638434363256,
                     25.619248796095121
                 ]
             },
             "properties": {
                 "OBJECTID": 58,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 59,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26385244663317,
                     25.619290764757011
                 ]
             },
             "properties": {
                 "OBJECTID": 59,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 60,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26385278567756,
                     25.619299616783962
                 ]
             },
             "properties": {
                 "OBJECTID": 60,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 61,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26385154641179,
                     25.619317286663488
                 ]
             },
             "properties": {
                 "OBJECTID": 61,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 62,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2638477737558,
                     25.619334658867444
                 ]
             },
             "properties": {
                 "OBJECTID": 62,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 63,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26383751698791,
                     25.619359510732863
                 ]
             },
             "properties": {
                 "OBJECTID": 63,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 64,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26348266429358,
                     25.619863730926511
                 ]
             },
             "properties": {
                 "OBJECTID": 64,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 65,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2625100780777,
                     25.621127615646969
                 ]
             },
             "properties": {
                 "OBJECTID": 65,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 66,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26232862906306,
                     25.621279451785313
                 ]
             },
             "properties": {
                 "OBJECTID": 66,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 67,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26207624062607,
                     25.62159306966447
                 ]
             },
             "properties": {
                 "OBJECTID": 67,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 68,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26288286325359,
                     25.622179475302573
                 ]
             },
             "properties": {
                 "OBJECTID": 68,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 69,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26334989468194,
                     25.622179975325651
                 ]
             },
             "properties": {
                 "OBJECTID": 69,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 70,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26335757309357,
                     25.622180597656495
                 ]
             },
             "properties": {
                 "OBJECTID": 70,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 71,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26347675664704,
                     25.622195327652321
                 ]
             },
             "properties": {
                 "OBJECTID": 71,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 72,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2646267422304,
                     25.622152312179537
                 ]
             },
             "properties": {
                 "OBJECTID": 72,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 73,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26730026119816,
                     25.621547607136392
                 ]
             },
             "properties": {
                 "OBJECTID": 73,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 74,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26729217809162,
                     25.62161280168948
                 ]
             },
             "properties": {
                 "OBJECTID": 74,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 75,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26714290861838,
                     25.621977361665984
                 ]
             },
             "properties": {
                 "OBJECTID": 75,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 76,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26737435903948,
                     25.621979833002968
                 ]
             },
             "properties": {
                 "OBJECTID": 76,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 77,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26743210001149,
                     25.62197283627745
                 ]
             },
             "properties": {
                 "OBJECTID": 77,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 78,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26834826995673,
                     25.622308787920304
                 ]
             },
             "properties": {
                 "OBJECTID": 78,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 79,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26858923340643,
                     25.622431049853105
                 ]
             },
             "properties": {
                 "OBJECTID": 79,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 80,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26874221168379,
                     25.622538589884073
                 ]
             },
             "properties": {
                 "OBJECTID": 80,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 81,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26879592908892,
                     25.622570278395699
                 ]
             },
             "properties": {
                 "OBJECTID": 81,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 82,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26884756816082,
                     25.622473464578775
                 ]
             },
             "properties": {
                 "OBJECTID": 82,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 83,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2689990103961,
                     25.62164885461101
                 ]
             },
             "properties": {
                 "OBJECTID": 83,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 84,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26912633821001,
                     25.621473907694735
                 ]
             },
             "properties": {
                 "OBJECTID": 84,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 85,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26922636800253,
                     25.621390129550889
                 ]
             },
             "properties": {
                 "OBJECTID": 85,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 86,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26984020566101,
                     25.621064814189879
                 ]
             },
             "properties": {
                 "OBJECTID": 86,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 87,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26986684088206,
                     25.621053814581956
                 ]
             },
             "properties": {
                 "OBJECTID": 87,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 88,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26988498110705,
                     25.621054525046361
                 ]
             },
             "properties": {
                 "OBJECTID": 88,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 89,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27084436707395,
                     25.626672278702756
                 ]
             },
             "properties": {
                 "OBJECTID": 89,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 90,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27580558236178,
                     25.637390004045528
                 ]
             },
             "properties": {
                 "OBJECTID": 90,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 91,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2760957090498,
                     25.637907139403012
                 ]
             },
             "properties": {
                 "OBJECTID": 91,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 92,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27864532122464,
                     25.64061483710509
                 ]
             },
             "properties": {
                 "OBJECTID": 92,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 93,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28279012296588,
                     25.644354495163896
                 ]
             },
             "properties": {
                 "OBJECTID": 93,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 94,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28305580967793,
                     25.650130111315661
                 ]
             },
             "properties": {
                 "OBJECTID": 94,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 95,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28248882130214,
                     25.653858890275615
                 ]
             },
             "properties": {
                 "OBJECTID": 95,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 96,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28248893371739,
                     25.653859644806801
                 ]
             },
             "properties": {
                 "OBJECTID": 96,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 97,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28248991307908,
                     25.65386845816289
                 ]
             },
             "properties": {
                 "OBJECTID": 97,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 98,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28342959489993,
                     25.654120920344269
                 ]
             },
             "properties": {
                 "OBJECTID": 98,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 99,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28444950524056,
                     25.653779855156245
                 ]
             },
             "properties": {
                 "OBJECTID": 99,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 100,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28468762503394,
                     25.653801840882295
                 ]
             },
             "properties": {
                 "OBJECTID": 100,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 101,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.29024062660369,
                     25.660809122642945
                 ]
             },
             "properties": {
                 "OBJECTID": 101,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 102,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28389579895202,
                     25.664731514847574
                 ]
             },
             "properties": {
                 "OBJECTID": 102,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 103,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27966567583036,
                     25.664399347551921
                 ]
             },
             "properties": {
                 "OBJECTID": 103,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 104,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27563926533986,
                     25.66666154359541
                 ]
             },
             "properties": {
                 "OBJECTID": 104,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 105,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27043098470222,
                     25.67575815461521
                 ]
             },
             "properties": {
                 "OBJECTID": 105,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 106,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26876022960096,
                     25.680322771578687
                 ]
             },
             "properties": {
                 "OBJECTID": 106,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 107,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26901101005012,
                     25.681424139812236
                 ]
             },
             "properties": {
                 "OBJECTID": 107,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 108,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27085002111164,
                     25.681820410783359
                 ]
             },
             "properties": {
                 "OBJECTID": 108,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 109,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27301827127627,
                     25.685027625224791
                 ]
             },
             "properties": {
                 "OBJECTID": 109,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 110,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27352428202005,
                     25.691571248014725
                 ]
             },
             "properties": {
                 "OBJECTID": 110,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 111,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27333427685545,
                     25.694405732138875
                 ]
             },
             "properties": {
                 "OBJECTID": 111,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 112,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27281036510635,
                     25.694982381032105
                 ]
             },
             "properties": {
                 "OBJECTID": 112,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 113,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27265397120385,
                     25.695285403998241
                 ]
             },
             "properties": {
                 "OBJECTID": 113,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 114,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27163922027387,
                     25.697061746702275
                 ]
             },
             "properties": {
                 "OBJECTID": 114,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 115,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26731527358106,
                     25.699102434329916
                 ]
             },
             "properties": {
                 "OBJECTID": 115,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 116,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26696847161776,
                     25.699293471016915
                 ]
             },
             "properties": {
                 "OBJECTID": 116,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 117,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26635061129173,
                     25.699929209865218
                 ]
             },
             "properties": {
                 "OBJECTID": 117,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 118,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26606474019565,
                     25.700689211637837
                 ]
             },
             "properties": {
                 "OBJECTID": 118,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 119,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26521081772512,
                     25.701168972070434
                 ]
             },
             "properties": {
                 "OBJECTID": 119,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 120,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26473596129563,
                     25.701694490908096
                 ]
             },
             "properties": {
                 "OBJECTID": 120,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 121,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26467655118194,
                     25.701841205407334
                 ]
             },
             "properties": {
                 "OBJECTID": 121,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 122,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26447274502124,
                     25.701969392973183
                 ]
             },
             "properties": {
                 "OBJECTID": 122,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 123,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26447249321103,
                     25.70197876121091
                 ]
             },
             "properties": {
                 "OBJECTID": 123,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 124,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26446123369902,
                     25.702030580147209
                 ]
             },
             "properties": {
                 "OBJECTID": 124,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 125,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26445721732676,
                     25.702038654260548
                 ]
             },
             "properties": {
                 "OBJECTID": 125,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 126,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26442894264164,
                     25.70207454980067
                 ]
             },
             "properties": {
                 "OBJECTID": 126,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 127,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26442182091034,
                     25.7020806166272
                 ]
             },
             "properties": {
                 "OBJECTID": 127,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 128,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26425518732708,
                     25.702168525357195
                 ]
             },
             "properties": {
                 "OBJECTID": 128,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 129,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26423898424179,
                     25.702258419790269
                 ]
             },
             "properties": {
                 "OBJECTID": 129,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 130,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26422924818132,
                     25.702273772116939
                 ]
             },
             "properties": {
                 "OBJECTID": 130,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 131,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26417981604573,
                     25.702315881073105
                 ]
             },
             "properties": {
                 "OBJECTID": 131,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 132,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26245235509219,
                     25.702758379895386
                 ]
             },
             "properties": {
                 "OBJECTID": 132,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 133,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26241273635878,
                     25.702732763606207
                 ]
             },
             "properties": {
                 "OBJECTID": 133,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 134,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26191380238015,
                     25.702332941212319
                 ]
             },
             "properties": {
                 "OBJECTID": 134,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 135,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26189442109074,
                     25.702330664128908
                 ]
             },
             "properties": {
                 "OBJECTID": 135,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 136,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26179404326052,
                     25.702265763654111
                 ]
             },
             "properties": {
                 "OBJECTID": 136,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 137,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26178190421155,
                     25.702241616857179
                 ]
             },
             "properties": {
                 "OBJECTID": 137,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 138,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26177680865288,
                     25.702224524342398
                 ]
             },
             "properties": {
                 "OBJECTID": 138,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 139,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26177049631144,
                     25.702184546779449
                 ]
             },
             "properties": {
                 "OBJECTID": 139,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 140,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26162915616067,
                     25.702470541082675
                 ]
             },
             "properties": {
                 "OBJECTID": 140,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 141,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26155273986711,
                     25.70250771905603
                 ]
             },
             "properties": {
                 "OBJECTID": 141,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 142,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26153336936954,
                     25.702510063588591
                 ]
             },
             "properties": {
                 "OBJECTID": 142,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 143,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26122326514161,
                     25.702574553073191
                 ]
             },
             "properties": {
                 "OBJECTID": 143,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 144,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26122233164534,
                     25.702583264805867
                 ]
             },
             "properties": {
                 "OBJECTID": 144,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 145,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26017172114808,
                     25.702621488690795
                 ]
             },
             "properties": {
                 "OBJECTID": 145,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 146,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26014070982598,
                     25.702541931964618
                 ]
             },
             "properties": {
                 "OBJECTID": 146,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 147,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26013826097204,
                     25.702443461596545
                 ]
             },
             "properties": {
                 "OBJECTID": 147,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 148,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26005043497963,
                     25.702632889396398
                 ]
             },
             "properties": {
                 "OBJECTID": 148,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 149,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25998674589169,
                     25.702650768817989
                 ]
             },
             "properties": {
                 "OBJECTID": 149,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 150,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25935465739161,
                     25.702694061281932
                 ]
             },
             "properties": {
                 "OBJECTID": 150,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 151,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25931453144051,
                     25.702691894815132
                 ]
             },
             "properties": {
                 "OBJECTID": 151,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 152,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25926894480602,
                     25.702676441764424
                 ]
             },
             "properties": {
                 "OBJECTID": 152,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 153,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25916766225788,
                     25.702553591674985
                 ]
             },
             "properties": {
                 "OBJECTID": 153,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 154,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25914879358203,
                     25.702558206995718
                 ]
             },
             "properties": {
                 "OBJECTID": 154,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 155,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2590465361688,
                     25.702613353423544
                 ]
             },
             "properties": {
                 "OBJECTID": 155,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 156,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25903975438121,
                     25.702672666410479
                 ]
             },
             "properties": {
                 "OBJECTID": 156,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 157,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25894648479249,
                     25.702757493163801
                 ]
             },
             "properties": {
                 "OBJECTID": 157,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 158,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25891797538429,
                     25.702763567184945
                 ]
             },
             "properties": {
                 "OBJECTID": 158,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 159,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25891694386195,
                     25.702763687694073
                 ]
             },
             "properties": {
                 "OBJECTID": 159,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 160,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25848392839066,
                     25.70281309824594
                 ]
             },
             "properties": {
                 "OBJECTID": 160,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 161,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.258399419098,
                     25.702795402286029
                 ]
             },
             "properties": {
                 "OBJECTID": 161,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 162,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2583831485635,
                     25.702785593380497
                 ]
             },
             "properties": {
                 "OBJECTID": 162,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 163,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25834462880164,
                     25.702745953063356
                 ]
             },
             "properties": {
                 "OBJECTID": 163,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 164,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25832629612171,
                     25.702699149646094
                 ]
             },
             "properties": {
                 "OBJECTID": 164,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 165,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25802094481082,
                     25.702702496922768
                 ]
             },
             "properties": {
                 "OBJECTID": 165,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 166,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25799013943345,
                     25.702736637885607
                 ]
             },
             "properties": {
                 "OBJECTID": 166,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 167,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25793561263845,
                     25.702766473793758
                 ]
             },
             "properties": {
                 "OBJECTID": 167,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 168,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25714374878504,
                     25.703098650082666
                 ]
             },
             "properties": {
                 "OBJECTID": 168,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 169,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25713873236668,
                     25.703133692166205
                 ]
             },
             "properties": {
                 "OBJECTID": 169,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 170,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25712387556649,
                     25.703166361838157
                 ]
             },
             "properties": {
                 "OBJECTID": 170,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 171,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25710019192036,
                     25.703194435974467
                 ]
             },
             "properties": {
                 "OBJECTID": 171,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 172,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25702964909908,
                     25.703236457696391
                 ]
             },
             "properties": {
                 "OBJECTID": 172,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 173,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25711290923346,
                     25.703419321344654
                 ]
             },
             "properties": {
                 "OBJECTID": 173,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 174,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25711422404225,
                     25.703436986727581
                 ]
             },
             "properties": {
                 "OBJECTID": 174,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 175,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25703977006941,
                     25.703554336563457
                 ]
             },
             "properties": {
                 "OBJECTID": 175,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 176,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25680510637062,
                     25.703640021270019
                 ]
             },
             "properties": {
                 "OBJECTID": 176,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 177,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25675667607982,
                     25.703644427048744
                 ]
             },
             "properties": {
                 "OBJECTID": 177,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 178,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25670595431637,
                     25.70363476383335
                 ]
             },
             "properties": {
                 "OBJECTID": 178,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 179,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25670386878858,
                     25.703650295125044
                 ]
             },
             "properties": {
                 "OBJECTID": 179,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 180,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25664594525421,
                     25.7037489282705
                 ]
             },
             "properties": {
                 "OBJECTID": 180,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 181,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25644808810881,
                     25.703821077280963
                 ]
             },
             "properties": {
                 "OBJECTID": 181,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 182,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25633849492567,
                     25.703965850143902
                 ]
             },
             "properties": {
                 "OBJECTID": 182,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 183,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25631365295277,
                     25.703979904748849
                 ]
             },
             "properties": {
                 "OBJECTID": 183,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 184,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2563108767456,
                     25.703981117934291
                 ]
             },
             "properties": {
                 "OBJECTID": 184,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 185,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2562107938931,
                     25.704190647380528
                 ]
             },
             "properties": {
                 "OBJECTID": 185,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 186,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25617533182617,
                     25.704220853809431
                 ]
             },
             "properties": {
                 "OBJECTID": 186,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 187,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25614208119214,
                     25.704308047578365
                 ]
             },
             "properties": {
                 "OBJECTID": 187,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 188,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25615400710183,
                     25.704322074304343
                 ]
             },
             "properties": {
                 "OBJECTID": 188,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 189,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2563172322545,
                     25.704686377074665
                 ]
             },
             "properties": {
                 "OBJECTID": 189,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 190,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25634020093958,
                     25.704782360817376
                 ]
             },
             "properties": {
                 "OBJECTID": 190,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 191,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25647992410927,
                     25.704723218701645
                 ]
             },
             "properties": {
                 "OBJECTID": 191,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 192,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25659211903115,
                     25.704743235811748
                 ]
             },
             "properties": {
                 "OBJECTID": 192,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 193,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25662149088913,
                     25.704766483286619
                 ]
             },
             "properties": {
                 "OBJECTID": 193,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 194,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25690868668659,
                     25.705061314328248
                 ]
             },
             "properties": {
                 "OBJECTID": 194,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 195,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25697054385557,
                     25.705086577183863
                 ]
             },
             "properties": {
                 "OBJECTID": 195,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 196,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25698767773918,
                     25.705100457320327
                 ]
             },
             "properties": {
                 "OBJECTID": 196,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 197,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25732137927827,
                     25.705196177561618
                 ]
             },
             "properties": {
                 "OBJECTID": 197,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 198,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25742244059325,
                     25.705586424873673
                 ]
             },
             "properties": {
                 "OBJECTID": 198,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 199,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25746608559143,
                     25.70562497161518
                 ]
             },
             "properties": {
                 "OBJECTID": 199,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 200,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25747801329976,
                     25.705638998341158
                 ]
             },
             "properties": {
                 "OBJECTID": 200,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 201,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25750425911434,
                     25.705707661579368
                 ]
             },
             "properties": {
                 "OBJECTID": 201,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 202,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25752364400103,
                     25.705708613961406
                 ]
             },
             "properties": {
                 "OBJECTID": 202,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 203,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25770149302951,
                     25.705856755684806
                 ]
             },
             "properties": {
                 "OBJECTID": 203,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 204,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25786450594228,
                     25.705912017225842
                 ]
             },
             "properties": {
                 "OBJECTID": 204,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 205,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25786452662669,
                     25.705912046004187
                 ]
             },
             "properties": {
                 "OBJECTID": 205,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 206,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25809071871174,
                     25.70625938576211
                 ]
             },
             "properties": {
                 "OBJECTID": 206,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 207,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2584603850388,
                     25.706924313906029
                 ]
             },
             "properties": {
                 "OBJECTID": 207,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 208,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2584285130655,
                     25.706978582595582
                 ]
             },
             "properties": {
                 "OBJECTID": 208,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 209,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25833007597231,
                     25.70705538919492
                 ]
             },
             "properties": {
                 "OBJECTID": 209,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 210,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25832908581873,
                     25.707057202228157
                 ]
             },
             "properties": {
                 "OBJECTID": 210,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 211,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25836747158161,
                     25.70706979903207
                 ]
             },
             "properties": {
                 "OBJECTID": 211,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 212,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25839994430203,
                     25.707089373675728
                 ]
             },
             "properties": {
                 "OBJECTID": 212,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 213,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2584609309273,
                     25.707145272836101
                 ]
             },
             "properties": {
                 "OBJECTID": 213,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 214,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2588080539486,
                     25.706869646816813
                 ]
             },
             "properties": {
                 "OBJECTID": 214,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 215,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25881740509925,
                     25.706867060366619
                 ]
             },
             "properties": {
                 "OBJECTID": 215,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 216,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25882692082581,
                     25.706865032395399
                 ]
             },
             "properties": {
                 "OBJECTID": 216,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 217,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25893911934492,
                     25.706885047706862
                 ]
             },
             "properties": {
                 "OBJECTID": 217,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 218,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25903907719174,
                     25.706984107130893
                 ]
             },
             "properties": {
                 "OBJECTID": 218,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 219,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25905408687669,
                     25.707003441655559
                 ]
             },
             "properties": {
                 "OBJECTID": 219,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 220,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25918225016079,
                     25.707153304681299
                 ]
             },
             "properties": {
                 "OBJECTID": 220,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 221,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25919114805311,
                     25.707149633648726
                 ]
             },
             "properties": {
                 "OBJECTID": 221,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 222,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25923852973443,
                     25.707139538758781
                 ]
             },
             "properties": {
                 "OBJECTID": 222,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 223,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2592963408535,
                     25.707146330438832
                 ]
             },
             "properties": {
                 "OBJECTID": 223,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 224,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25931441093138,
                     25.70715307355556
                 ]
             },
             "properties": {
                 "OBJECTID": 224,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 225,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25933931315882,
                     25.707167041825585
                 ]
             },
             "properties": {
                 "OBJECTID": 225,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 226,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25938588005437,
                     25.707195944237469
                 ]
             },
             "properties": {
                 "OBJECTID": 226,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 227,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25944263626872,
                     25.707233246317287
                 ]
             },
             "properties": {
                 "OBJECTID": 227,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 228,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2594854655818,
                     25.707274217631095
                 ]
             },
             "properties": {
                 "OBJECTID": 228,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 229,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25950212732135,
                     25.707274800391758
                 ]
             },
             "properties": {
                 "OBJECTID": 229,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 230,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25959547874839,
                     25.707311690582117
                 ]
             },
             "properties": {
                 "OBJECTID": 230,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 231,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25990347676412,
                     25.70757643480465
                 ]
             },
             "properties": {
                 "OBJECTID": 231,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 232,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26170293204473,
                     25.709202088905329
                 ]
             },
             "properties": {
                 "OBJECTID": 232,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 233,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26042991740684,
                     25.711466401053599
                 ]
             },
             "properties": {
                 "OBJECTID": 233,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 234,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25972320316333,
                     25.712241440390187
                 ]
             },
             "properties": {
                 "OBJECTID": 234,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 235,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25756384909243,
                     25.713417219429175
                 ]
             },
             "properties": {
                 "OBJECTID": 235,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 236,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25466515247484,
                     25.714989110259296
                 ]
             },
             "properties": {
                 "OBJECTID": 236,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 237,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25340647213102,
                     25.716502698041154
                 ]
             },
             "properties": {
                 "OBJECTID": 237,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 238,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25242390883636,
                     25.717188394229083
                 ]
             },
             "properties": {
                 "OBJECTID": 238,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 239,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25065136127137,
                     25.717713583914815
                 ]
             },
             "properties": {
                 "OBJECTID": 239,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 240,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24694466516422,
                     25.718181134252006
                 ]
             },
             "properties": {
                 "OBJECTID": 240,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 241,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24667916371249,
                     25.71808371159409
                 ]
             },
             "properties": {
                 "OBJECTID": 241,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 242,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24668287071796,
                     25.718031005926207
                 ]
             },
             "properties": {
                 "OBJECTID": 242,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 243,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24670287973419,
                     25.717990806230773
                 ]
             },
             "properties": {
                 "OBJECTID": 243,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 244,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24686970936966,
                     25.717893369183685
                 ]
             },
             "properties": {
                 "OBJECTID": 244,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 245,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24687502436296,
                     25.717888304201949
                 ]
             },
             "properties": {
                 "OBJECTID": 245,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 246,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24674370265978,
                     25.717593554099267
                 ]
             },
             "properties": {
                 "OBJECTID": 246,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 247,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24675382902603,
                     25.717498639650557
                 ]
             },
             "properties": {
                 "OBJECTID": 247,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 248,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24679678334496,
                     25.717451050225861
                 ]
             },
             "properties": {
                 "OBJECTID": 248,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 249,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24680101285651,
                     25.717442601095229
                 ]
             },
             "properties": {
                 "OBJECTID": 249,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 250,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24678269276711,
                     25.717377686231259
                 ]
             },
             "properties": {
                 "OBJECTID": 250,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 251,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24673001587757,
                     25.717332238092354
                 ]
             },
             "properties": {
                 "OBJECTID": 251,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 252,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24672483758127,
                     25.717324724256684
                 ]
             },
             "properties": {
                 "OBJECTID": 252,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 253,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2467098449834,
                     25.71729210494675
                 ]
             },
             "properties": {
                 "OBJECTID": 253,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 254,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24662846623079,
                     25.716991586592485
                 ]
             },
             "properties": {
                 "OBJECTID": 254,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 255,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24664225283777,
                     25.716577205973351
                 ]
             },
             "properties": {
                 "OBJECTID": 255,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 256,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2466697568039,
                     25.716509248702891
                 ]
             },
             "properties": {
                 "OBJECTID": 256,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 257,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24666992137981,
                     25.716492586963341
                 ]
             },
             "properties": {
                 "OBJECTID": 257,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 258,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24668118358983,
                     25.716442225828132
                 ]
             },
             "properties": {
                 "OBJECTID": 258,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 259,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24671348543905,
                     25.716398260671269
                 ]
             },
             "properties": {
                 "OBJECTID": 259,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 260,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24670050192668,
                     25.716277902603167
                 ]
             },
             "properties": {
                 "OBJECTID": 260,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 261,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24669645138016,
                     25.716269842879001
                 ]
             },
             "properties": {
                 "OBJECTID": 261,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 262,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24668628724243,
                     25.716235728895811
                 ]
             },
             "properties": {
                 "OBJECTID": 262,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 263,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24592413609008,
                     25.715894053068155
                 ]
             },
             "properties": {
                 "OBJECTID": 263,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 264,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24517688490539,
                     25.716815411202333
                 ]
             },
             "properties": {
                 "OBJECTID": 264,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 265,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23971572480093,
                     25.713767723901412
                 ]
             },
             "properties": {
                 "OBJECTID": 265,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 266,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23806983055829,
                     25.709365280783118
                 ]
             },
             "properties": {
                 "OBJECTID": 266,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 267,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23713852862028,
                     25.70771639539538
                 ]
             },
             "properties": {
                 "OBJECTID": 267,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 268,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23699635659665,
                     25.707467748138129
                 ]
             },
             "properties": {
                 "OBJECTID": 268,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 269,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23706272386568,
                     25.707267871115391
                 ]
             },
             "properties": {
                 "OBJECTID": 269,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 270,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23659756032919,
                     25.706768446106935
                 ]
             },
             "properties": {
                 "OBJECTID": 270,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 271,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23521255941216,
                     25.709141218292643
                 ]
             },
             "properties": {
                 "OBJECTID": 271,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 272,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23443768914808,
                     25.710591467722111
                 ]
             },
             "properties": {
                 "OBJECTID": 272,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 273,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23410776566089,
                     25.711213235602372
                 ]
             },
             "properties": {
                 "OBJECTID": 273,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 274,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23367173925942,
                     25.721375615192528
                 ]
             },
             "properties": {
                 "OBJECTID": 274,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 275,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23366860782005,
                     25.722257271855653
                 ]
             },
             "properties": {
                 "OBJECTID": 275,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 276,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23307226377403,
                     25.725942865370996
                 ]
             },
             "properties": {
                 "OBJECTID": 276,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 277,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23282421906254,
                     25.726375741447384
                 ]
             },
             "properties": {
                 "OBJECTID": 277,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 278,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23200887121249,
                     25.728290196635328
                 ]
             },
             "properties": {
                 "OBJECTID": 278,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 279,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23221088142708,
                     25.730576493610329
                 ]
             },
             "properties": {
                 "OBJECTID": 279,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 280,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23235074758901,
                     25.730880385321598
                 ]
             },
             "properties": {
                 "OBJECTID": 280,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 281,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2306630096902,
                     25.736471480374576
                 ]
             },
             "properties": {
                 "OBJECTID": 281,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 282,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23026558578823,
                     25.739582364834348
                 ]
             },
             "properties": {
                 "OBJECTID": 282,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 283,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22625230180233,
                     25.744143873740825
                 ]
             },
             "properties": {
                 "OBJECTID": 283,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 284,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22389404197651,
                     25.756901509696945
                 ]
             },
             "properties": {
                 "OBJECTID": 284,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 285,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22368148721091,
                     25.757326968165103
                 ]
             },
             "properties": {
                 "OBJECTID": 285,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 286,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22283854726123,
                     25.758183878183502
                 ]
             },
             "properties": {
                 "OBJECTID": 286,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 287,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22190491517978,
                     25.75911504882049
                 ]
             },
             "properties": {
                 "OBJECTID": 287,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 288,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21991349691041,
                     25.762901290861919
                 ]
             },
             "properties": {
                 "OBJECTID": 288,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 289,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21954996395971,
                     25.764137037594253
                 ]
             },
             "properties": {
                 "OBJECTID": 289,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 290,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21930683044593,
                     25.765035930663714
                 ]
             },
             "properties": {
                 "OBJECTID": 290,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 291,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21672580315334,
                     25.772916356104986
                 ]
             },
             "properties": {
                 "OBJECTID": 291,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 292,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21698936386809,
                     25.773967348814153
                 ]
             },
             "properties": {
                 "OBJECTID": 292,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 293,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21705464835338,
                     25.774235907061495
                 ]
             },
             "properties": {
                 "OBJECTID": 293,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 294,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21711536428268,
                     25.774587943079155
                 ]
             },
             "properties": {
                 "OBJECTID": 294,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 295,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21707181011601,
                     25.77491284295337
                 ]
             },
             "properties": {
                 "OBJECTID": 295,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 296,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21648101398767,
                     25.777111683557393
                 ]
             },
             "properties": {
                 "OBJECTID": 296,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 297,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21614594188179,
                     25.779334366088165
                 ]
             },
             "properties": {
                 "OBJECTID": 297,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 298,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21544172505565,
                     25.780938457144146
                 ]
             },
             "properties": {
                 "OBJECTID": 298,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 299,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21801329777554,
                     25.785902689657462
                 ]
             },
             "properties": {
                 "OBJECTID": 299,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 300,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21847139174139,
                     25.794829629184107
                 ]
             },
             "properties": {
                 "OBJECTID": 300,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 301,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21879766398109,
                     25.795761339414355
                 ]
             },
             "properties": {
                 "OBJECTID": 301,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 302,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21950393036218,
                     25.797008866366127
                 ]
             },
             "properties": {
                 "OBJECTID": 302,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 303,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21992076073457,
                     25.797947754984932
                 ]
             },
             "properties": {
                 "OBJECTID": 303,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 304,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22061093104969,
                     25.803115934852542
                 ]
             },
             "properties": {
                 "OBJECTID": 304,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 305,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22197478890547,
                     25.80505227234454
                 ]
             },
             "properties": {
                 "OBJECTID": 305,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 306,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22202630656886,
                     25.806218386369494
                 ]
             },
             "properties": {
                 "OBJECTID": 306,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 307,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22203636998256,
                     25.806819360125417
                 ]
             },
             "properties": {
                 "OBJECTID": 307,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 308,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22248697259579,
                     25.809584287085215
                 ]
             },
             "properties": {
                 "OBJECTID": 308,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 309,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22792972717525,
                     25.816437264068554
                 ]
             },
             "properties": {
                 "OBJECTID": 309,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 310,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22809688056662,
                     25.816754299371496
                 ]
             },
             "properties": {
                 "OBJECTID": 310,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 311,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22829342020526,
                     25.817220114017118
                 ]
             },
             "properties": {
                 "OBJECTID": 311,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 312,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22822627232466,
                     25.819764859960344
                 ]
             },
             "properties": {
                 "OBJECTID": 312,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 313,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22799871686738,
                     25.820413570629853
                 ]
             },
             "properties": {
                 "OBJECTID": 313,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 314,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22779091501877,
                     25.820880431186993
                 ]
             },
             "properties": {
                 "OBJECTID": 314,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 315,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22365511189395,
                     25.827198640704751
                 ]
             },
             "properties": {
                 "OBJECTID": 315,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 316,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22346610407749,
                     25.827813598019191
                 ]
             },
             "properties": {
                 "OBJECTID": 316,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 317,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22330082757122,
                     25.828185286921496
                 ]
             },
             "properties": {
                 "OBJECTID": 317,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 318,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22139445998641,
                     25.830438668709633
                 ]
             },
             "properties": {
                 "OBJECTID": 318,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 319,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22004817128612,
                     25.831229385927486
                 ]
             },
             "properties": {
                 "OBJECTID": 319,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 320,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21094634779575,
                     25.83932939073884
                 ]
             },
             "properties": {
                 "OBJECTID": 320,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 321,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2106395260933,
                     25.839580300690386
                 ]
             },
             "properties": {
                 "OBJECTID": 321,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 322,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2105935185761,
                     25.83972636677845
                 ]
             },
             "properties": {
                 "OBJECTID": 322,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 323,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21084547893577,
                     25.840173459337734
                 ]
             },
             "properties": {
                 "OBJECTID": 323,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 324,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21345971729215,
                     25.840163626150456
                 ]
             },
             "properties": {
                 "OBJECTID": 324,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 325,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21542560560727,
                     25.840443433117969
                 ]
             },
             "properties": {
                 "OBJECTID": 325,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 326,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21797219516071,
                     25.849294182980202
                 ]
             },
             "properties": {
                 "OBJECTID": 326,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 327,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2184622006701,
                     25.849709333419071
                 ]
             },
             "properties": {
                 "OBJECTID": 327,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 328,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21924462703799,
                     25.850663036569927
                 ]
             },
             "properties": {
                 "OBJECTID": 328,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 329,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22062967292112,
                     25.852404938836912
                 ]
             },
             "properties": {
                 "OBJECTID": 329,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 330,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22067391416988,
                     25.853632163593545
                 ]
             },
             "properties": {
                 "OBJECTID": 330,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 331,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2199076558137,
                     25.854369015018108
                 ]
             },
             "properties": {
                 "OBJECTID": 331,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 332,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21774148398237,
                     25.853593555698069
                 ]
             },
             "properties": {
                 "OBJECTID": 332,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 333,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21709969539467,
                     25.853476569188388
                 ]
             },
             "properties": {
                 "OBJECTID": 333,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 334,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21660142511575,
                     25.853431558119951
                 ]
             },
             "properties": {
                 "OBJECTID": 334,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 335,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21386090485692,
                     25.857415611441354
                 ]
             },
             "properties": {
                 "OBJECTID": 335,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 336,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21512642004831,
                     25.861188008419902
                 ]
             },
             "properties": {
                 "OBJECTID": 336,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 337,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21618237341784,
                     25.864306155804513
                 ]
             },
             "properties": {
                 "OBJECTID": 337,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 338,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21433792055865,
                     25.86723181499633
                 ]
             },
             "properties": {
                 "OBJECTID": 338,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 339,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21317199539135,
                     25.866933054816684
                 ]
             },
             "properties": {
                 "OBJECTID": 339,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 340,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21271457771564,
                     25.86723181499633
                 ]
             },
             "properties": {
                 "OBJECTID": 340,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 341,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21267086976491,
                     25.867423390377212
                 ]
             },
             "properties": {
                 "OBJECTID": 341,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 342,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2126186704154,
                     25.867770007080196
                 ]
             },
             "properties": {
                 "OBJECTID": 342,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 343,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21241046926781,
                     25.868752002003362
                 ]
             },
             "properties": {
                 "OBJECTID": 343,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 344,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21139391159983,
                     25.873283218146071
                 ]
             },
             "properties": {
                 "OBJECTID": 344,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 345,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20897829031588,
                     25.876203786275767
                 ]
             },
             "properties": {
                 "OBJECTID": 345,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 346,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21262486134833,
                     25.883978130386481
                 ]
             },
             "properties": {
                 "OBJECTID": 346,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 347,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21565240833206,
                     25.888656859626394
                 ]
             },
             "properties": {
                 "OBJECTID": 347,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 348,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21588143238228,
                     25.888726447367731
                 ]
             },
             "properties": {
                 "OBJECTID": 348,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 349,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21127412010463,
                     25.899421424359275
                 ]
             },
             "properties": {
                 "OBJECTID": 349,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 350,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20996062608208,
                     25.900262875031729
                 ]
             },
             "properties": {
                 "OBJECTID": 350,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 351,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20933871880686,
                     25.901863433550659
                 ]
             },
             "properties": {
                 "OBJECTID": 351,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 352,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20249036793621,
                     25.904141162684539
                 ]
             },
             "properties": {
                 "OBJECTID": 352,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 353,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19991140818502,
                     25.904176599570462
                 ]
             },
             "properties": {
                 "OBJECTID": 353,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 354,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19837372576688,
                     25.902889275819462
                 ]
             },
             "properties": {
                 "OBJECTID": 354,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 355,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19847843383269,
                     25.90226453298186
                 ]
             },
             "properties": {
                 "OBJECTID": 355,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 356,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19723965098916,
                     25.902306897345511
                 ]
             },
             "properties": {
                 "OBJECTID": 356,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 357,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19695391029478,
                     25.902567234890057
                 ]
             },
             "properties": {
                 "OBJECTID": 357,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 358,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19526303286273,
                     25.903550764056547
                 ]
             },
             "properties": {
                 "OBJECTID": 358,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 359,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19491246813595,
                     25.904239112345238
                 ]
             },
             "properties": {
                 "OBJECTID": 359,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 360,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19501927521941,
                     25.90461498399435
                 ]
             },
             "properties": {
                 "OBJECTID": 360,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 361,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19504624408893,
                     25.904989485976046
                 ]
             },
             "properties": {
                 "OBJECTID": 361,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 362,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1944426739903,
                     25.905434998426472
                 ]
             },
             "properties": {
                 "OBJECTID": 362,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 363,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19408995718584,
                     25.904754911310135
                 ]
             },
             "properties": {
                 "OBJECTID": 363,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 364,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19409499788588,
                     25.904260267997017
                 ]
             },
             "properties": {
                 "OBJECTID": 364,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 365,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19375823955119,
                     25.903912638657403
                 ]
             },
             "properties": {
                 "OBJECTID": 365,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 366,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19324614219579,
                     25.9039322195963
                 ]
             },
             "properties": {
                 "OBJECTID": 366,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 367,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.193020721729,
                     25.904319455978168
                 ]
             },
             "properties": {
                 "OBJECTID": 367,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 368,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19308808005093,
                     25.904656615410545
                 ]
             },
             "properties": {
                 "OBJECTID": 368,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 369,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19300116507179,
                     25.905075615847068
                 ]
             },
             "properties": {
                 "OBJECTID": 369,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 370,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19324051513775,
                     25.905587944328204
                 ]
             },
             "properties": {
                 "OBJECTID": 370,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 371,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19331223517156,
                     25.90571957180083
                 ]
             },
             "properties": {
                 "OBJECTID": 371,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 372,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19335874630917,
                     25.905833103115242
                 ]
             },
             "properties": {
                 "OBJECTID": 372,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 373,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19337991455149,
                     25.906084680363733
                 ]
             },
             "properties": {
                 "OBJECTID": 373,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 374,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19321569744653,
                     25.906634991711542
                 ]
             },
             "properties": {
                 "OBJECTID": 374,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 375,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1930307203915,
                     25.90682727395955
                 ]
             },
             "properties": {
                 "OBJECTID": 375,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 376,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1930750263914,
                     25.907428759429763
                 ]
             },
             "properties": {
                 "OBJECTID": 376,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 377,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19254446865239,
                     25.9091189102096
                 ]
             },
             "properties": {
                 "OBJECTID": 377,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 378,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19160367706809,
                     25.909690906909873
                 ]
             },
             "properties": {
                 "OBJECTID": 378,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 379,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1915227848487,
                     25.909388651964775
                 ]
             },
             "properties": {
                 "OBJECTID": 379,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 380,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19099383419814,
                     25.908786358903399
                 ]
             },
             "properties": {
                 "OBJECTID": 380,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 381,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1896808330041,
                     25.908688624180684
                 ]
             },
             "properties": {
                 "OBJECTID": 381,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 382,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1896219373026,
                     25.908749000166267
                 ]
             },
             "properties": {
                 "OBJECTID": 382,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 383,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18958446884824,
                     25.908808144080638
                 ]
             },
             "properties": {
                 "OBJECTID": 383,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 384,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18939375681646,
                     25.909019619659773
                 ]
             },
             "properties": {
                 "OBJECTID": 384,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 385,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18889609627786,
                     25.909047966290643
                 ]
             },
             "properties": {
                 "OBJECTID": 385,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 386,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18892031412128,
                     25.909267783981079
                 ]
             },
             "properties": {
                 "OBJECTID": 386,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 387,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18886409030557,
                     25.909456168567374
                 ]
             },
             "properties": {
                 "OBJECTID": 387,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 388,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18895296760542,
                     25.909503488195469
                 ]
             },
             "properties": {
                 "OBJECTID": 388,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 389,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18908176940818,
                     25.909535016627785
                 ]
             },
             "properties": {
                 "OBJECTID": 389,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 390,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18914229288259,
                     25.909579221004321
                 ]
             },
             "properties": {
                 "OBJECTID": 390,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 391,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1894754242515,
                     25.9098242871612
                 ]
             },
             "properties": {
                 "OBJECTID": 391,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 392,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19123621767739,
                     25.909737840728837
                 ]
             },
             "properties": {
                 "OBJECTID": 392,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 393,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19179548627272,
                     25.910448401372662
                 ]
             },
             "properties": {
                 "OBJECTID": 393,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 394,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19191189811568,
                     25.911036853867756
                 ]
             },
             "properties": {
                 "OBJECTID": 394,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 395,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19198706615026,
                     25.911564569749146
                 ]
             },
             "properties": {
                 "OBJECTID": 395,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 396,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19210260744944,
                     25.913243178629841
                 ]
             },
             "properties": {
                 "OBJECTID": 396,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 397,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19139794006293,
                     25.915278962254376
                 ]
             },
             "properties": {
                 "OBJECTID": 397,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 398,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19130202376942,
                     25.915597678390213
                 ]
             },
             "properties": {
                 "OBJECTID": 398,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 399,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19110525930034,
                     25.915850320435993
                 ]
             },
             "properties": {
                 "OBJECTID": 399,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 400,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19069032739674,
                     25.916346981827814
                 ]
             },
             "properties": {
                 "OBJECTID": 400,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 401,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19051941933577,
                     25.91663361644828
                 ]
             },
             "properties": {
                 "OBJECTID": 401,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 402,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18888148319394,
                     25.917743357370114
                 ]
             },
             "properties": {
                 "OBJECTID": 402,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 403,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18908368226619,
                     25.918010115174752
                 ]
             },
             "properties": {
                 "OBJECTID": 403,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 404,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18892298240979,
                     25.919453966827177
                 ]
             },
             "properties": {
                 "OBJECTID": 404,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 405,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18716167097438,
                     25.923063551441771
                 ]
             },
             "properties": {
                 "OBJECTID": 405,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 406,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18697251476982,
                     25.923663106966842
                 ]
             },
             "properties": {
                 "OBJECTID": 406,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 407,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18663906683952,
                     25.924073967639572
                 ]
             },
             "properties": {
                 "OBJECTID": 407,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 408,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18645992008931,
                     25.925540048734319
                 ]
             },
             "properties": {
                 "OBJECTID": 408,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 409,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18671712169788,
                     25.928342848843442
                 ]
             },
             "properties": {
                 "OBJECTID": 409,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 410,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18262435023365,
                     25.933305808816101
                 ]
             },
             "properties": {
                 "OBJECTID": 410,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 411,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18009800082211,
                     25.934048958192932
                 ]
             },
             "properties": {
                 "OBJECTID": 411,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 412,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17823203617957,
                     25.934439646172791
                 ]
             },
             "properties": {
                 "OBJECTID": 412,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 413,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17318895200862,
                     25.934857409142182
                 ]
             },
             "properties": {
                 "OBJECTID": 413,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 414,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17054307460324,
                     25.933754271942121
                 ]
             },
             "properties": {
                 "OBJECTID": 414,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 415,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1683903278514,
                     25.931437564392127
                 ]
             },
             "properties": {
                 "OBJECTID": 415,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 416,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16647578902649,
                     25.933534018979856
                 ]
             },
             "properties": {
                 "OBJECTID": 416,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 417,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16444038761387,
                     25.937236541333391
                 ]
             },
             "properties": {
                 "OBJECTID": 417,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 418,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16437710681811,
                     25.93747141637283
                 ]
             },
             "properties": {
                 "OBJECTID": 418,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 419,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16436702541796,
                     25.937921839121657
                 ]
             },
             "properties": {
                 "OBJECTID": 419,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 420,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16672689503025,
                     25.945997321219465
                 ]
             },
             "properties": {
                 "OBJECTID": 420,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 421,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16444928011026,
                     25.948109411420262
                 ]
             },
             "properties": {
                 "OBJECTID": 421,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 422,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16393122294767,
                     25.948581147202219
                 ]
             },
             "properties": {
                 "OBJECTID": 422,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 423,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1578064746891,
                     25.9522550180547
                 ]
             },
             "properties": {
                 "OBJECTID": 423,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 424,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15669855489443,
                     25.952713650714486
                 ]
             },
             "properties": {
                 "OBJECTID": 424,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 425,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15300788200818,
                     25.957125354151799
                 ]
             },
             "properties": {
                 "OBJECTID": 425,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 426,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15431333159506,
                     25.964859608284371
                 ]
             },
             "properties": {
                 "OBJECTID": 426,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 427,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14786958841086,
                     25.964848300208928
                 ]
             },
             "properties": {
                 "OBJECTID": 427,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 428,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13873028117757,
                     25.960178941904701
                 ]
             },
             "properties": {
                 "OBJECTID": 428,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 429,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1383922026381,
                     25.959845743086646
                 ]
             },
             "properties": {
                 "OBJECTID": 429,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 430,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13833130864305,
                     25.958791044271322
                 ]
             },
             "properties": {
                 "OBJECTID": 430,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 431,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14005783879827,
                     25.957328545176267
                 ]
             },
             "properties": {
                 "OBJECTID": 431,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 432,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14069642490017,
                     25.9542792040221
                 ]
             },
             "properties": {
                 "OBJECTID": 432,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 433,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13926944992522,
                     25.953190690697852
                 ]
             },
             "properties": {
                 "OBJECTID": 433,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 434,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13803502429698,
                     25.950973416689862
                 ]
             },
             "properties": {
                 "OBJECTID": 434,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 435,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1356346420252,
                     25.952296501082685
                 ]
             },
             "properties": {
                 "OBJECTID": 435,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 436,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13644078441473,
                     25.950718487668098
                 ]
             },
             "properties": {
                 "OBJECTID": 436,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 437,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13733175625561,
                     25.950444680977284
                 ]
             },
             "properties": {
                 "OBJECTID": 437,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 438,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13809102957742,
                     25.949540881206758
                 ]
             },
             "properties": {
                 "OBJECTID": 438,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 439,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13935201668227,
                     25.947332142664322
                 ]
             },
             "properties": {
                 "OBJECTID": 439,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 440,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14061677104718,
                     25.945370602571302
                 ]
             },
             "properties": {
                 "OBJECTID": 440,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 441,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14113599193252,
                     25.94310620498743
                 ]
             },
             "properties": {
                 "OBJECTID": 441,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 442,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14076488938821,
                     25.941933171578683
                 ]
             },
             "properties": {
                 "OBJECTID": 442,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 443,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1381609024038,
                     25.940235121153989
                 ]
             },
             "properties": {
                 "OBJECTID": 443,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 444,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13408085904445,
                     25.931479301028958
                 ]
             },
             "properties": {
                 "OBJECTID": 444,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 445,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13409263027074,
                     25.931365668990509
                 ]
             },
             "properties": {
                 "OBJECTID": 445,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 446,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13437695363359,
                     25.928833463193826
                 ]
             },
             "properties": {
                 "OBJECTID": 446,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 447,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13439307038396,
                     25.927246436773714
                 ]
             },
             "properties": {
                 "OBJECTID": 447,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 448,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13396625124159,
                     25.926833441110489
                 ]
             },
             "properties": {
                 "OBJECTID": 448,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 449,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13299226298261,
                     25.926812021957289
                 ]
             },
             "properties": {
                 "OBJECTID": 449,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 450,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13186462375342,
                     25.928948563825202
                 ]
             },
             "properties": {
                 "OBJECTID": 450,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 451,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13172635478753,
                     25.929296780422135
                 ]
             },
             "properties": {
                 "OBJECTID": 451,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 452,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1300756923394,
                     25.932084094205607
                 ]
             },
             "properties": {
                 "OBJECTID": 452,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 453,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12775190712489,
                     25.934426728310939
                 ]
             },
             "properties": {
                 "OBJECTID": 453,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 454,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12514941661237,
                     25.937723061070471
                 ]
             },
             "properties": {
                 "OBJECTID": 454,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 455,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12258899728448,
                     25.937723538610442
                 ]
             },
             "properties": {
                 "OBJECTID": 455,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 456,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1195952162463,
                     25.939390940139845
                 ]
             },
             "properties": {
                 "OBJECTID": 456,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 457,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11922004337038,
                     25.940962630421097
                 ]
             },
             "properties": {
                 "OBJECTID": 457,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 458,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11871471341334,
                     25.942802912224693
                 ]
             },
             "properties": {
                 "OBJECTID": 458,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 459,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11594990156675,
                     25.940579786328101
                 ]
             },
             "properties": {
                 "OBJECTID": 459,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 460,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1146185344179,
                     25.93943772287264
                 ]
             },
             "properties": {
                 "OBJECTID": 460,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 461,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11359762313185,
                     25.939230207907826
                 ]
             },
             "properties": {
                 "OBJECTID": 461,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 462,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11120481315169,
                     25.940885663742449
                 ]
             },
             "properties": {
                 "OBJECTID": 462,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 463,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11087887905705,
                     25.941320119028319
                 ]
             },
             "properties": {
                 "OBJECTID": 463,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 464,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1092481986455,
                     25.940002726444732
                 ]
             },
             "properties": {
                 "OBJECTID": 464,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 465,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10807037994414,
                     25.936400502781055
                 ]
             },
             "properties": {
                 "OBJECTID": 465,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 466,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1020153633184,
                     25.935632854972312
                 ]
             },
             "properties": {
                 "OBJECTID": 466,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 467,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09827628981895,
                     25.943069374152344
                 ]
             },
             "properties": {
                 "OBJECTID": 467,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 468,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09819976920403,
                     25.942870643765275
                 ]
             },
             "properties": {
                 "OBJECTID": 468,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 469,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09765859316963,
                     25.94070574177664
                 ]
             },
             "properties": {
                 "OBJECTID": 469,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 470,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09647944257227,
                     25.938136513589257
                 ]
             },
             "properties": {
                 "OBJECTID": 470,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 471,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.0957831011093,
                     25.937383165001108
                 ]
             },
             "properties": {
                 "OBJECTID": 471,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 472,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09614186405582,
                     25.935276797186475
                 ]
             },
             "properties": {
                 "OBJECTID": 472,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 473,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09740833522653,
                     25.932980622957416
                 ]
             },
             "properties": {
                 "OBJECTID": 473,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 474,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10083624239871,
                     25.929249140635363
                 ]
             },
             "properties": {
                 "OBJECTID": 474,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 475,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10566258366134,
                     25.922988269193297
                 ]
             },
             "properties": {
                 "OBJECTID": 475,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 476,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10590046963102,
                     25.92313153389324
                 ]
             },
             "properties": {
                 "OBJECTID": 476,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 477,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10615590137382,
                     25.92273471703362
                 ]
             },
             "properties": {
                 "OBJECTID": 477,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 478,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10727401318525,
                     25.921760476964494
                 ]
             },
             "properties": {
                 "OBJECTID": 478,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 479,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10727990284528,
                     25.921756162916608
                 ]
             },
             "properties": {
                 "OBJECTID": 479,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 480,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10731372724678,
                     25.921738469654713
                 ]
             },
             "properties": {
                 "OBJECTID": 480,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 481,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10733226227416,
                     25.921732765254944
                 ]
             },
             "properties": {
                 "OBJECTID": 481,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 482,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10735146190052,
                     25.921729295670502
                 ]
             },
             "properties": {
                 "OBJECTID": 482,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 483,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10744558944248,
                     25.921746062630746
                 ]
             },
             "properties": {
                 "OBJECTID": 483,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 484,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10754588183704,
                     25.921794958770363
                 ]
             },
             "properties": {
                 "OBJECTID": 484,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 485,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1077729912306,
                     25.921475937764342
                 ]
             },
             "properties": {
                 "OBJECTID": 485,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 486,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10813327133349,
                     25.921227685309475
                 ]
             },
             "properties": {
                 "OBJECTID": 486,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 487,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10814280864378,
                     25.921225666331509
                 ]
             },
             "properties": {
                 "OBJECTID": 487,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 488,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10839458644108,
                     25.921385924620779
                 ]
             },
             "properties": {
                 "OBJECTID": 488,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 489,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10919639140042,
                     25.920947615739919
                 ]
             },
             "properties": {
                 "OBJECTID": 489,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 490,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10921596784272,
                     25.920947601350747
                 ]
             },
             "properties": {
                 "OBJECTID": 490,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 491,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10933514600026,
                     25.921013778862857
                 ]
             },
             "properties": {
                 "OBJECTID": 491,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 492,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10969221732205,
                     25.920769329640905
                 ]
             },
             "properties": {
                 "OBJECTID": 492,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 493,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10973565457687,
                     25.920682438044139
                 ]
             },
             "properties": {
                 "OBJECTID": 493,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 494,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10977526881368,
                     25.920656721930243
                 ]
             },
             "properties": {
                 "OBJECTID": 494,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 495,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11086168042226,
                     25.920322855815186
                 ]
             },
             "properties": {
                 "OBJECTID": 495,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 496,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.110871051358,
                     25.92032027835819
                 ]
             },
             "properties": {
                 "OBJECTID": 496,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 497,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11087440223196,
                     25.920319500444634
                 ]
             },
             "properties": {
                 "OBJECTID": 497,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 498,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11309702810541,
                     25.919275261644088
                 ]
             },
             "properties": {
                 "OBJECTID": 498,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 499,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11317448941122,
                     25.919220615239226
                 ]
             },
             "properties": {
                 "OBJECTID": 499,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 500,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11319339585862,
                     25.919216016106304
                 ]
             },
             "properties": {
                 "OBJECTID": 500,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 501,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11320305907395,
                     25.919214566399148
                 ]
             },
             "properties": {
                 "OBJECTID": 501,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 502,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11324213281824,
                     25.919214534922901
                 ]
             },
             "properties": {
                 "OBJECTID": 502,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 503,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11327071417219,
                     25.919220537897559
                 ]
             },
             "properties": {
                 "OBJECTID": 503,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 504,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11385409089365,
                     25.919066138691392
                 ]
             },
             "properties": {
                 "OBJECTID": 504,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 505,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11387223021933,
                     25.919069352868405
                 ]
             },
             "properties": {
                 "OBJECTID": 505,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 506,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11398020102553,
                     25.918910879733346
                 ]
             },
             "properties": {
                 "OBJECTID": 506,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 507,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11398070284724,
                     25.918851875213932
                 ]
             },
             "properties": {
                 "OBJECTID": 507,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 508,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11394743962268,
                     25.918627899058322
                 ]
             },
             "properties": {
                 "OBJECTID": 508,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 509,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11394338907621,
                     25.918619833938237
                 ]
             },
             "properties": {
                 "OBJECTID": 509,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 510,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11393225816727,
                     25.918559192652651
                 ]
             },
             "properties": {
                 "OBJECTID": 510,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 511,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11393320875067,
                     25.91855037659866
                 ]
             },
             "properties": {
                 "OBJECTID": 511,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 512,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11410163917935,
                     25.918087209557143
                 ]
             },
             "properties": {
                 "OBJECTID": 512,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 513,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11410524905807,
                     25.91807846724754
                 ]
             },
             "properties": {
                 "OBJECTID": 513,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 514,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11419534403996,
                     25.918002020377003
                 ]
             },
             "properties": {
                 "OBJECTID": 514,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 515,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11421425138667,
                     25.917997423042664
                 ]
             },
             "properties": {
                 "OBJECTID": 515,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 516,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11427265336033,
                     25.917997376277924
                 ]
             },
             "properties": {
                 "OBJECTID": 516,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 517,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11430649844618,
                     25.918007186082832
                 ]
             },
             "properties": {
                 "OBJECTID": 517,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 518,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11444463071518,
                     25.917702798845141
                 ]
             },
             "properties": {
                 "OBJECTID": 518,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 519,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11445406820076,
                     25.91765085040646
                 ]
             },
             "properties": {
                 "OBJECTID": 519,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 520,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11445735162556,
                     25.917599828269488
                 ]
             },
             "properties": {
                 "OBJECTID": 520,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 521,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11448511279775,
                     25.91750075985226
                 ]
             },
             "properties": {
                 "OBJECTID": 521,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 522,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11449542442438,
                     25.917475917879358
                 ]
             },
             "properties": {
                 "OBJECTID": 522,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 523,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1145051991557,
                     25.917460577243901
                 ]
             },
             "properties": {
                 "OBJECTID": 523,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 524,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11453639933541,
                     25.917429927449234
                 ]
             },
             "properties": {
                 "OBJECTID": 524,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 525,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11453768986257,
                     25.917424315679682
                 ]
             },
             "properties": {
                 "OBJECTID": 525,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 526,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1145321806157,
                     25.917411947303549
                 ]
             },
             "properties": {
                 "OBJECTID": 526,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 527,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11452545818338,
                     25.917350776317335
                 ]
             },
             "properties": {
                 "OBJECTID": 527,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 528,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1145299089282,
                     25.917331285310638
                 ]
             },
             "properties": {
                 "OBJECTID": 528,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 529,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11465676100079,
                     25.917067302813848
                 ]
             },
             "properties": {
                 "OBJECTID": 529,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 530,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11467431306914,
                     25.917059458926929
                 ]
             },
             "properties": {
                 "OBJECTID": 530,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 531,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11469284629788,
                     25.917053751829258
                 ]
             },
             "properties": {
                 "OBJECTID": 531,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 532,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11470824628861,
                     25.916869694780644
                 ]
             },
             "properties": {
                 "OBJECTID": 532,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 533,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11473905436395,
                     25.916757593388297
                 ]
             },
             "properties": {
                 "OBJECTID": 533,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 534,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11481450028901,
                     25.91645664695676
                 ]
             },
             "properties": {
                 "OBJECTID": 534,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 535,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11476096994289,
                     25.91638433786693
                 ]
             },
             "properties": {
                 "OBJECTID": 535,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 536,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11488299895132,
                     25.916087019300562
                 ]
             },
             "properties": {
                 "OBJECTID": 536,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 537,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11487006400233,
                     25.915999114167846
                 ]
             },
             "properties": {
                 "OBJECTID": 537,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 538,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11485747169502,
                     25.915965664783641
                 ]
             },
             "properties": {
                 "OBJECTID": 538,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 539,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11485488074823,
                     25.915930407762175
                 ]
             },
             "properties": {
                 "OBJECTID": 539,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 540,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11498632565844,
                     25.915590234701597
                 ]
             },
             "properties": {
                 "OBJECTID": 540,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 541,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11500015813084,
                     25.915577704447571
                 ]
             },
             "properties": {
                 "OBJECTID": 541,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 542,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11507927598785,
                     25.915545208344724
                 ]
             },
             "properties": {
                 "OBJECTID": 542,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 543,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11509881106133,
                     25.915544033830145
                 ]
             },
             "properties": {
                 "OBJECTID": 543,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 544,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11510860108115,
                     25.915544317116598
                 ]
             },
             "properties": {
                 "OBJECTID": 544,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 545,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1152885608185,
                     25.915076319816365
                 ]
             },
             "properties": {
                 "OBJECTID": 545,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 546,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11528510202589,
                     25.915068032563681
                 ]
             },
             "properties": {
                 "OBJECTID": 546,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 547,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11528845919509,
                     25.914972723312587
                 ]
             },
             "properties": {
                 "OBJECTID": 547,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 548,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11547701824986,
                     25.914704461841495
                 ]
             },
             "properties": {
                 "OBJECTID": 548,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 549,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11555449394479,
                     25.914704398888944
                 ]
             },
             "properties": {
                 "OBJECTID": 549,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 550,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11554063809001,
                     25.914608089591695
                 ]
             },
             "properties": {
                 "OBJECTID": 550,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 551,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11563317832855,
                     25.91434208541898
                 ]
             },
             "properties": {
                 "OBJECTID": 551,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 552,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11574319509242,
                     25.914275477131639
                 ]
             },
             "properties": {
                 "OBJECTID": 552,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 553,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11571129524015,
                     25.914060547256497
                 ]
             },
             "properties": {
                 "OBJECTID": 553,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 554,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11570498469735,
                     25.914043785692229
                 ]
             },
             "properties": {
                 "OBJECTID": 554,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 555,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11561966691414,
                     25.913932579125287
                 ]
             },
             "properties": {
                 "OBJECTID": 555,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 556,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11561693837109,
                     25.913933096235439
                 ]
             },
             "properties": {
                 "OBJECTID": 556,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 557,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11558774188086,
                     25.913935721356495
                 ]
             },
             "properties": {
                 "OBJECTID": 557,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 558,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11555853999471,
                     25.913933143899499
                 ]
             },
             "properties": {
                 "OBJECTID": 558,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 559,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1154493820834,
                     25.913852275062482
                 ]
             },
             "properties": {
                 "OBJECTID": 559,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 560,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11544236827075,
                     25.913503419946949
                 ]
             },
             "properties": {
                 "OBJECTID": 560,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 561,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11536618220362,
                     25.913375351091645
                 ]
             },
             "properties": {
                 "OBJECTID": 561,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 562,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11533760174899,
                     25.913369348116987
                 ]
             },
             "properties": {
                 "OBJECTID": 562,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 563,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11530254437696,
                     25.913353746678126
                 ]
             },
             "properties": {
                 "OBJECTID": 563,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 564,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11519581463517,
                     25.913043379848148
                 ]
             },
             "properties": {
                 "OBJECTID": 564,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 565,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11525018944474,
                     25.912753024732353
                 ]
             },
             "properties": {
                 "OBJECTID": 565,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 566,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11517881205259,
                     25.912746591881785
                 ]
             },
             "properties": {
                 "OBJECTID": 566,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 567,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11513821305817,
                     25.912742024225111
                 ]
             },
             "properties": {
                 "OBJECTID": 567,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 568,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11512883762583,
                     25.91273946115723
                 ]
             },
             "properties": {
                 "OBJECTID": 568,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 569,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11508581495843,
                     25.912718708401712
                 ]
             },
             "properties": {
                 "OBJECTID": 569,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 570,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11503513726183,
                     25.912654871824941
                 ]
             },
             "properties": {
                 "OBJECTID": 570,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 571,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11530667856044,
                     25.910764370978995
                 ]
             },
             "properties": {
                 "OBJECTID": 571,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 572,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11532159921251,
                     25.910731714796839
                 ]
             },
             "properties": {
                 "OBJECTID": 572,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 573,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11536802131718,
                     25.91068681344575
                 ]
             },
             "properties": {
                 "OBJECTID": 573,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 574,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11545114835189,
                     25.910663938290156
                 ]
             },
             "properties": {
                 "OBJECTID": 574,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 575,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11551281576391,
                     25.910675909165946
                 ]
             },
             "properties": {
                 "OBJECTID": 575,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 576,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1160024237729,
                     25.910010571830469
                 ]
             },
             "properties": {
                 "OBJECTID": 576,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 577,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11601150872423,
                     25.909978832057504
                 ]
             },
             "properties": {
                 "OBJECTID": 577,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 578,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11606756796397,
                     25.909915646589866
                 ]
             },
             "properties": {
                 "OBJECTID": 578,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 579,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11670462162107,
                     25.909810506849453
                 ]
             },
             "properties": {
                 "OBJECTID": 579,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 580,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11671293495402,
                     25.90980582318025
                 ]
             },
             "properties": {
                 "OBJECTID": 580,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 581,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11674901755316,
                     25.909792273094979
                 ]
             },
             "properties": {
                 "OBJECTID": 581,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 582,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1170480718111,
                     25.909719523337401
                 ]
             },
             "properties": {
                 "OBJECTID": 582,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 583,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11565439783226,
                     25.908504012250432
                 ]
             },
             "properties": {
                 "OBJECTID": 583,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 584,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11551738611854,
                     25.908258928107102
                 ]
             },
             "properties": {
                 "OBJECTID": 584,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 585,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11550560410041,
                     25.908259111568782
                 ]
             },
             "properties": {
                 "OBJECTID": 585,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 586,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11531328318154,
                     25.908323341149298
                 ]
             },
             "properties": {
                 "OBJECTID": 586,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 587,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11524535199146,
                     25.908325405093422
                 ]
             },
             "properties": {
                 "OBJECTID": 587,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 588,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11513620487204,
                     25.908244535357085
                 ]
             },
             "properties": {
                 "OBJECTID": 588,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 589,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1150579800418,
                     25.907272596653058
                 ]
             },
             "properties": {
                 "OBJECTID": 589,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 590,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11505925707911,
                     25.907265876019437
                 ]
             },
             "properties": {
                 "OBJECTID": 590,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 591,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11515923201301,
                     25.907020220806601
                 ]
             },
             "properties": {
                 "OBJECTID": 591,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 592,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11549829261219,
                     25.906601314798877
                 ]
             },
             "properties": {
                 "OBJECTID": 592,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 593,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11562574992848,
                     25.906524772600221
                 ]
             },
             "properties": {
                 "OBJECTID": 593,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 594,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12079690565275,
                     25.902628393285795
                 ]
             },
             "properties": {
                 "OBJECTID": 594,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 595,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11999503684154,
                     25.900919958389409
                 ]
             },
             "properties": {
                 "OBJECTID": 595,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 596,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12081757656995,
                     25.899384979333377
                 ]
             },
             "properties": {
                 "OBJECTID": 596,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 597,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12081827624252,
                     25.899384229298789
                 ]
             },
             "properties": {
                 "OBJECTID": 597,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 598,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12375264716701,
                     25.895430611733389
                 ]
             },
             "properties": {
                 "OBJECTID": 598,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 599,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1237390638068,
                     25.895065136247069
                 ]
             },
             "properties": {
                 "OBJECTID": 599,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 600,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12392227459338,
                     25.89377663438421
                 ]
             },
             "properties": {
                 "OBJECTID": 600,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 601,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12407714774224,
                     25.892968405567501
                 ]
             },
             "properties": {
                 "OBJECTID": 601,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 602,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12407912085479,
                     25.892960857557568
                 ]
             },
             "properties": {
                 "OBJECTID": 602,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 603,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12495524578804,
                     25.89186649774075
                 ]
             },
             "properties": {
                 "OBJECTID": 603,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 604,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12497556327168,
                     25.89181919430041
                 ]
             },
             "properties": {
                 "OBJECTID": 604,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 605,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12559675828385,
                     25.890444577855021
                 ]
             },
             "properties": {
                 "OBJECTID": 605,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 606,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12662841377033,
                     25.888795210430658
                 ]
             },
             "properties": {
                 "OBJECTID": 606,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 607,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12721063126565,
                     25.885520766270417
                 ]
             },
             "properties": {
                 "OBJECTID": 607,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 608,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1272102490538,
                     25.885520164623927
                 ]
             },
             "properties": {
                 "OBJECTID": 608,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 609,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1266261960422,
                     25.88391833985952
                 ]
             },
             "properties": {
                 "OBJECTID": 609,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 610,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12646924905664,
                     25.881913875125292
                 ]
             },
             "properties": {
                 "OBJECTID": 610,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 611,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12647221232282,
                     25.88189890590985
                 ]
             },
             "properties": {
                 "OBJECTID": 611,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 612,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12653516576535,
                     25.881659605306595
                 ]
             },
             "properties": {
                 "OBJECTID": 612,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 613,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12677337818889,
                     25.881038663003949
                 ]
             },
             "properties": {
                 "OBJECTID": 613,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 614,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12687257970583,
                     25.880839162797201
                 ]
             },
             "properties": {
                 "OBJECTID": 614,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 615,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12709100074915,
                     25.880538534725645
                 ]
             },
             "properties": {
                 "OBJECTID": 615,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 616,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12710290057845,
                     25.880524478322059
                 ]
             },
             "properties": {
                 "OBJECTID": 616,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 617,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1271322436582,
                     25.880501156203422
                 ]
             },
             "properties": {
                 "OBJECTID": 617,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 618,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12716987219193,
                     25.88048471659647
                 ]
             },
             "properties": {
                 "OBJECTID": 618,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 619,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12723241464425,
                     25.880466606049083
                 ]
             },
             "properties": {
                 "OBJECTID": 619,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 620,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12746171928296,
                     25.880097086311537
                 ]
             },
             "properties": {
                 "OBJECTID": 620,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 621,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12761185929992,
                     25.879944056772842
                 ]
             },
             "properties": {
                 "OBJECTID": 621,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 622,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12772415584516,
                     25.879923750980367
                 ]
             },
             "properties": {
                 "OBJECTID": 622,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 623,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.127897199796,
                     25.879973728105142
                 ]
             },
             "properties": {
                 "OBJECTID": 623,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 624,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12797076433935,
                     25.879900690564455
                 ]
             },
             "properties": {
                 "OBJECTID": 624,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 625,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12807191828443,
                     25.879888691809754
                 ]
             },
             "properties": {
                 "OBJECTID": 625,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 626,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12874675156138,
                     25.876497828611946
                 ]
             },
             "properties": {
                 "OBJECTID": 626,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 627,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12875718909305,
                     25.876477396015048
                 ]
             },
             "properties": {
                 "OBJECTID": 627,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 628,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1290819576663,
                     25.875965128687767
                 ]
             },
             "properties": {
                 "OBJECTID": 628,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 629,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1292378686328,
                     25.87579780352587
                 ]
             },
             "properties": {
                 "OBJECTID": 629,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 630,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13018128713674,
                     25.874966747217684
                 ]
             },
             "properties": {
                 "OBJECTID": 630,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 631,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13070005206583,
                     25.874533359427062
                 ]
             },
             "properties": {
                 "OBJECTID": 631,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 632,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13090601300217,
                     25.874491091290906
                 ]
             },
             "properties": {
                 "OBJECTID": 632,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 633,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13325379212887,
                     25.874578128577809
                 ]
             },
             "properties": {
                 "OBJECTID": 633,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 634,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13327565374846,
                     25.874727219085969
                 ]
             },
             "properties": {
                 "OBJECTID": 634,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 635,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13329908918166,
                     25.875140979172897
                 ]
             },
             "properties": {
                 "OBJECTID": 635,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 636,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13353994291407,
                     25.875277147322549
                 ]
             },
             "properties": {
                 "OBJECTID": 636,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 637,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13355207386917,
                     25.875301304911318
                 ]
             },
             "properties": {
                 "OBJECTID": 637,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 638,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13355715863599,
                     25.875318401922755
                 ]
             },
             "properties": {
                 "OBJECTID": 638,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 639,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13356006164759,
                     25.875344806018006
                 ]
             },
             "properties": {
                 "OBJECTID": 639,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 640,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1335588016974,
                     25.875362474098949
                 ]
             },
             "properties": {
                 "OBJECTID": 640,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 641,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13365762459978,
                     25.875712296885069
                 ]
             },
             "properties": {
                 "OBJECTID": 641,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 642,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13391358784185,
                     25.876091195652236
                 ]
             },
             "properties": {
                 "OBJECTID": 642,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 643,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13397354564268,
                     25.876120118748531
                 ]
             },
             "properties": {
                 "OBJECTID": 643,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 644,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13686908923717,
                     25.875250947373388
                 ]
             },
             "properties": {
                 "OBJECTID": 644,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 645,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13758749376791,
                     25.874858925698902
                 ]
             },
             "properties": {
                 "OBJECTID": 645,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 646,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13690682658881,
                     25.874494482634304
                 ]
             },
             "properties": {
                 "OBJECTID": 646,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 647,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13689702397852,
                     25.874479159085979
                 ]
             },
             "properties": {
                 "OBJECTID": 647,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 648,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13689297433137,
                     25.874471094865214
                 ]
             },
             "properties": {
                 "OBJECTID": 648,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 649,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13688282818003,
                     25.874436974586786
                 ]
             },
             "properties": {
                 "OBJECTID": 649,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 650,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13688278771053,
                     25.874401641122915
                 ]
             },
             "properties": {
                 "OBJECTID": 650,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 651,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13580138712439,
                     25.873880616197539
                 ]
             },
             "properties": {
                 "OBJECTID": 651,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 652,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13579516831243,
                     25.873873774155413
                 ]
             },
             "properties": {
                 "OBJECTID": 652,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 653,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1357894567181,
                     25.873866581377683
                 ]
             },
             "properties": {
                 "OBJECTID": 653,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 654,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13545460044952,
                     25.873325791152411
                 ]
             },
             "properties": {
                 "OBJECTID": 654,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 655,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13545425600915,
                     25.873069011325867
                 ]
             },
             "properties": {
                 "OBJECTID": 655,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 656,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13546088671063,
                     25.87304316391095
                 ]
             },
             "properties": {
                 "OBJECTID": 656,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 657,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13547812761357,
                     25.873011458312192
                 ]
             },
             "properties": {
                 "OBJECTID": 657,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 658,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13549002564423,
                     25.872997400109966
                 ]
             },
             "properties": {
                 "OBJECTID": 658,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 659,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13540803535261,
                     25.872920251768278
                 ]
             },
             "properties": {
                 "OBJECTID": 659,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 660,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13523091657362,
                     25.87286434631261
                 ]
             },
             "properties": {
                 "OBJECTID": 660,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 661,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13515532765632,
                     25.872786527975961
                 ]
             },
             "properties": {
                 "OBJECTID": 661,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 662,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1351476699291,
                     25.872751878896167
                 ]
             },
             "properties": {
                 "OBJECTID": 662,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 663,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13514765014401,
                     25.872734173943002
                 ]
             },
             "properties": {
                 "OBJECTID": 663,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 664,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13515523232815,
                     25.872699512272732
                 ]
             },
             "properties": {
                 "OBJECTID": 664,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 665,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13515990700415,
                     25.872688592704435
                 ]
             },
             "properties": {
                 "OBJECTID": 665,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 666,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13555605836547,
                     25.871940861281757
                 ]
             },
             "properties": {
                 "OBJECTID": 666,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 667,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13557231900739,
                     25.871931011007405
                 ]
             },
             "properties": {
                 "OBJECTID": 667,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 668,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13560838811668,
                     25.871917457324855
                 ]
             },
             "properties": {
                 "OBJECTID": 668,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 669,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13569554321481,
                     25.871663245062734
                 ]
             },
             "properties": {
                 "OBJECTID": 669,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 670,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1358445680724,
                     25.871596590909974
                 ]
             },
             "properties": {
                 "OBJECTID": 670,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 671,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13609328727546,
                     25.871360716723757
                 ]
             },
             "properties": {
                 "OBJECTID": 671,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 672,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13613405174516,
                     25.871292421308226
                 ]
             },
             "properties": {
                 "OBJECTID": 672,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 673,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13614787702295,
                     25.871279889255561
                 ]
             },
             "properties": {
                 "OBJECTID": 673,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 674,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13642146427924,
                     25.871143542140828
                 ]
             },
             "properties": {
                 "OBJECTID": 674,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 675,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13639844433283,
                     25.871104674341325
                 ]
             },
             "properties": {
                 "OBJECTID": 675,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 676,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13639559168325,
                     25.871096200929003
                 ]
             },
             "properties": {
                 "OBJECTID": 676,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 677,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1363917551754,
                     25.871078838617564
                 ]
             },
             "properties": {
                 "OBJECTID": 677,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 678,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1362950636663,
                     25.87092269472663
                 ]
             },
             "properties": {
                 "OBJECTID": 678,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 679,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13628766044724,
                     25.870920601104899
                 ]
             },
             "properties": {
                 "OBJECTID": 679,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 680,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13606588493269,
                     25.870889915337386
                 ]
             },
             "properties": {
                 "OBJECTID": 680,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 681,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13603204164548,
                     25.870872277833428
                 ]
             },
             "properties": {
                 "OBJECTID": 681,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 682,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13598888407978,
                     25.870824799924605
                 ]
             },
             "properties": {
                 "OBJECTID": 682,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 683,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13597677830575,
                     25.870626040759248
                 ]
             },
             "properties": {
                 "OBJECTID": 683,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 684,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13597705080031,
                     25.870620348050693
                 ]
             },
             "properties": {
                 "OBJECTID": 684,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 685,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13597958508984,
                     25.870602792385
                 ]
             },
             "properties": {
                 "OBJECTID": 685,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 686,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1360018720888,
                     25.870553979882288
                 ]
             },
             "properties": {
                 "OBJECTID": 686,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 687,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13601376922014,
                     25.87053992168012
                 ]
             },
             "properties": {
                 "OBJECTID": 687,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 688,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13613598708622,
                     25.870493993303171
                 ]
             },
             "properties": {
                 "OBJECTID": 688,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 689,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13629458522701,
                     25.870167807398445
                 ]
             },
             "properties": {
                 "OBJECTID": 689,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 690,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13631084496956,
                     25.870157956224773
                 ]
             },
             "properties": {
                 "OBJECTID": 690,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 691,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13635488926678,
                     25.870067098617767
                 ]
             },
             "properties": {
                 "OBJECTID": 691,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 692,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13636277991844,
                     25.870023584021226
                 ]
             },
             "properties": {
                 "OBJECTID": 692,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 693,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1363919170534,
                     25.869977821119619
                 ]
             },
             "properties": {
                 "OBJECTID": 693,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 694,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13656726326866,
                     25.869744452444593
                 ]
             },
             "properties": {
                 "OBJECTID": 694,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 695,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13657811898508,
                     25.869729720650128
                 ]
             },
             "properties": {
                 "OBJECTID": 695,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 696,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1366675700524,
                     25.869679283971834
                 ]
             },
             "properties": {
                 "OBJECTID": 696,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 697,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13677795194099,
                     25.869563498956381
                 ]
             },
             "properties": {
                 "OBJECTID": 697,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 698,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13681344188689,
                     25.869521567166657
                 ]
             },
             "properties": {
                 "OBJECTID": 698,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 699,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13688286505226,
                     25.869490515375048
                 ]
             },
             "properties": {
                 "OBJECTID": 699,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 700,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13694022830896,
                     25.869483007834617
                 ]
             },
             "properties": {
                 "OBJECTID": 700,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 701,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13694658921378,
                     25.869480428578981
                 ]
             },
             "properties": {
                 "OBJECTID": 701,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 702,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13695488995626,
                     25.869101653018902
                 ]
             },
             "properties": {
                 "OBJECTID": 702,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 703,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13701299335503,
                     25.869003069336259
                 ]
             },
             "properties": {
                 "OBJECTID": 703,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 704,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13702925399696,
                     25.86899321726321
                 ]
             },
             "properties": {
                 "OBJECTID": 704,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 705,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13702801383181,
                     25.868776779225016
                 ]
             },
             "properties": {
                 "OBJECTID": 705,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 706,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13690814409563,
                     25.868772378842266
                 ]
             },
             "properties": {
                 "OBJECTID": 706,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 707,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13662664460401,
                     25.868752159384712
                 ]
             },
             "properties": {
                 "OBJECTID": 707,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 708,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13623032866678,
                     25.868678483325368
                 ]
             },
             "properties": {
                 "OBJECTID": 708,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 709,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13617201932328,
                     25.86884358086661
                 ]
             },
             "properties": {
                 "OBJECTID": 709,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 710,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13563541993875,
                     25.868931403261683
                 ]
             },
             "properties": {
                 "OBJECTID": 710,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 711,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13562161984197,
                     25.86895481621184
                 ]
             },
             "properties": {
                 "OBJECTID": 711,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 712,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13557207529112,
                     25.868996882899864
                 ]
             },
             "properties": {
                 "OBJECTID": 712,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 713,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13549729126703,
                     25.869015084278772
                 ]
             },
             "properties": {
                 "OBJECTID": 713,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 714,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13494978940133,
                     25.868856963677956
                 ]
             },
             "properties": {
                 "OBJECTID": 714,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 715,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13456945891346,
                     25.868637747634011
                 ]
             },
             "properties": {
                 "OBJECTID": 715,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 716,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13442785436206,
                     25.868509453948207
                 ]
             },
             "properties": {
                 "OBJECTID": 716,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 717,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13432328389251,
                     25.868237992689274
                 ]
             },
             "properties": {
                 "OBJECTID": 717,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 718,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13432581908137,
                     25.868220436124261
                 ]
             },
             "properties": {
                 "OBJECTID": 718,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 719,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13433430508417,
                     25.868195036571706
                 ]
             },
             "properties": {
                 "OBJECTID": 719,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 720,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13506397991881,
                     25.867872718651086
                 ]
             },
             "properties": {
                 "OBJECTID": 720,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 721,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13521443200051,
                     25.868005656435969
                 ]
             },
             "properties": {
                 "OBJECTID": 721,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 722,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13614648217447,
                     25.867363081840779
                 ]
             },
             "properties": {
                 "OBJECTID": 722,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 723,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13622327258599,
                     25.867372157798911
                 ]
             },
             "properties": {
                 "OBJECTID": 723,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 724,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13628476013361,
                     25.867414754187621
                 ]
             },
             "properties": {
                 "OBJECTID": 724,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 725,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13630098480269,
                     25.867429718007145
                 ]
             },
             "properties": {
                 "OBJECTID": 725,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 726,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13669541396223,
                     25.867259898125837
                 ]
             },
             "properties": {
                 "OBJECTID": 726,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 727,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13673413067568,
                     25.867255251328857
                 ]
             },
             "properties": {
                 "OBJECTID": 727,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 728,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13685859324954,
                     25.867315291867271
                 ]
             },
             "properties": {
                 "OBJECTID": 728,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 729,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1368724437084,
                     25.867338679636418
                 ]
             },
             "properties": {
                 "OBJECTID": 729,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 730,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13728267215765,
                     25.867480634923425
                 ]
             },
             "properties": {
                 "OBJECTID": 730,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 731,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13728822007533,
                     25.867479873197624
                 ]
             },
             "properties": {
                 "OBJECTID": 731,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 732,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13751491398216,
                     25.867484582047894
                 ]
             },
             "properties": {
                 "OBJECTID": 732,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 733,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1376661637625,
                     25.867525940070152
                 ]
             },
             "properties": {
                 "OBJECTID": 733,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 734,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13767238167515,
                     25.867532782112278
                 ]
             },
             "properties": {
                 "OBJECTID": 734,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 735,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13769920575379,
                     25.867583497580426
                 ]
             },
             "properties": {
                 "OBJECTID": 735,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 736,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13772376983616,
                     25.867683840337008
                 ]
             },
             "properties": {
                 "OBJECTID": 736,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 737,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13770873856748,
                     25.867752571024369
                 ]
             },
             "properties": {
                 "OBJECTID": 737,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 738,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13770011766638,
                     25.867820930291771
                 ]
             },
             "properties": {
                 "OBJECTID": 738,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 739,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13773347262173,
                     25.867853083752891
                 ]
             },
             "properties": {
                 "OBJECTID": 739,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 740,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13798218283154,
                     25.867963757921132
                 ]
             },
             "properties": {
                 "OBJECTID": 740,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 741,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13799748929273,
                     25.867964847000167
                 ]
             },
             "properties": {
                 "OBJECTID": 741,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 742,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13806906723374,
                     25.867991592837768
                 ]
             },
             "properties": {
                 "OBJECTID": 742,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 743,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13832188914392,
                     25.868083791333959
                 ]
             },
             "properties": {
                 "OBJECTID": 743,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 744,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1383316908549,
                     25.868099115781604
                 ]
             },
             "properties": {
                 "OBJECTID": 744,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 745,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13834428316221,
                     25.86813256156853
                 ]
             },
             "properties": {
                 "OBJECTID": 745,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 746,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13900489725825,
                     25.868446191138844
                 ]
             },
             "properties": {
                 "OBJECTID": 746,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 747,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13908594775836,
                     25.868519368074431
                 ]
             },
             "properties": {
                 "OBJECTID": 747,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 748,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13940594632743,
                     25.869038984661472
                 ]
             },
             "properties": {
                 "OBJECTID": 748,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 749,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14073503369491,
                     25.86898868108284
                 ]
             },
             "properties": {
                 "OBJECTID": 749,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 750,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14076422478922,
                     25.868991254043181
                 ]
             },
             "properties": {
                 "OBJECTID": 750,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 751,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14081817871602,
                     25.869011413246199
                 ]
             },
             "properties": {
                 "OBJECTID": 751,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 752,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14084087040987,
                     25.869028225172485
                 ]
             },
             "properties": {
                 "OBJECTID": 752,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 753,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14085378827178,
                     25.869041525246246
                 ]
             },
             "properties": {
                 "OBJECTID": 753,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 754,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14089773184492,
                     25.869566908286231
                 ]
             },
             "properties": {
                 "OBJECTID": 754,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 755,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14105339909514,
                     25.869478430285369
                 ]
             },
             "properties": {
                 "OBJECTID": 755,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 756,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14106231227595,
                     25.869474766447354
                 ]
             },
             "properties": {
                 "OBJECTID": 756,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 757,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14125364663852,
                     25.869436512884818
                 ]
             },
             "properties": {
                 "OBJECTID": 757,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 758,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14130004266292,
                     25.869391603439794
                 ]
             },
             "properties": {
                 "OBJECTID": 758,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 759,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14133505416947,
                     25.869375942645661
                 ]
             },
             "properties": {
                 "OBJECTID": 759,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 760,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14177066508421,
                     25.869394787939143
                 ]
             },
             "properties": {
                 "OBJECTID": 760,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 761,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14183415722061,
                     25.869460148866835
                 ]
             },
             "properties": {
                 "OBJECTID": 761,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 762,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14186585382618,
                     25.869692284571329
                 ]
             },
             "properties": {
                 "OBJECTID": 762,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 763,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14205471775108,
                     25.869680207575584
                 ]
             },
             "properties": {
                 "OBJECTID": 763,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 764,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14209190022109,
                     25.869528126821649
                 ]
             },
             "properties": {
                 "OBJECTID": 764,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 765,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14214437836046,
                     25.869489080956384
                 ]
             },
             "properties": {
                 "OBJECTID": 765,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 766,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14257537215576,
                     25.869477916772496
                 ]
             },
             "properties": {
                 "OBJECTID": 766,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 767,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14320883032332,
                     25.869150370193495
                 ]
             },
             "properties": {
                 "OBJECTID": 767,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 768,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14325983177588,
                     25.869097644740577
                 ]
             },
             "properties": {
                 "OBJECTID": 768,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 769,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14327676780863,
                     25.869088775626551
                 ]
             },
             "properties": {
                 "OBJECTID": 769,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 770,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14329789378286,
                     25.868760929573284
                 ]
             },
             "properties": {
                 "OBJECTID": 770,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 771,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14330040738793,
                     25.868725669853859
                 ]
             },
             "properties": {
                 "OBJECTID": 771,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 772,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1436036542853,
                     25.86857095858295
                 ]
             },
             "properties": {
                 "OBJECTID": 772,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 773,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14370632538669,
                     25.868592514433033
                 ]
             },
             "properties": {
                 "OBJECTID": 773,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 774,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14373571702976,
                     25.868615782592371
                 ]
             },
             "properties": {
                 "OBJECTID": 774,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 775,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14374193584172,
                     25.868622623735121
                 ]
             },
             "properties": {
                 "OBJECTID": 775,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 776,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14381098758707,
                     25.868709887651221
                 ]
             },
             "properties": {
                 "OBJECTID": 776,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 777,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14393926508507,
                     25.868585919704458
                 ]
             },
             "properties": {
                 "OBJECTID": 777,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 778,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14394594974584,
                     25.868579449082347
                 ]
             },
             "properties": {
                 "OBJECTID": 778,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 779,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1439686009702,
                     25.868562593089223
                 ]
             },
             "properties": {
                 "OBJECTID": 779,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 780,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14407266871876,
                     25.868524160561606
                 ]
             },
             "properties": {
                 "OBJECTID": 780,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 781,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14406187055891,
                     25.868481717057648
                 ]
             },
             "properties": {
                 "OBJECTID": 781,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 782,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14406311522066,
                     25.868416464947927
                 ]
             },
             "properties": {
                 "OBJECTID": 782,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 783,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14400636620093,
                     25.868043773301565
                 ]
             },
             "properties": {
                 "OBJECTID": 783,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 784,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1440101631386,
                     25.868026404694888
                 ]
             },
             "properties": {
                 "OBJECTID": 784,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 785,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14403023061072,
                     25.867986220287889
                 ]
             },
             "properties": {
                 "OBJECTID": 785,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 786,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14407146542584,
                     25.867948836369749
                 ]
             },
             "properties": {
                 "OBJECTID": 786,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 787,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14414694282721,
                     25.86792612668944
                 ]
             },
             "properties": {
                 "OBJECTID": 787,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 788,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14404692832312,
                     25.867729810082722
                 ]
             },
             "properties": {
                 "OBJECTID": 788,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 789,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14400427437783,
                     25.867386701635041
                 ]
             },
             "properties": {
                 "OBJECTID": 789,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 790,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1440118529647,
                     25.867352038166075
                 ]
             },
             "properties": {
                 "OBJECTID": 790,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 791,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14406237058199,
                     25.867288116153702
                 ]
             },
             "properties": {
                 "OBJECTID": 791,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 792,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14453148034363,
                     25.867313068743158
                 ]
             },
             "properties": {
                 "OBJECTID": 792,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 793,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.144538272923,
                     25.867339617629284
                 ]
             },
             "properties": {
                 "OBJECTID": 793,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 794,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14466195938201,
                     25.867382229306486
                 ]
             },
             "properties": {
                 "OBJECTID": 794,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 795,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14474328237662,
                     25.867409832198064
                 ]
             },
             "properties": {
                 "OBJECTID": 795,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 796,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14481055616227,
                     25.867517461261855
                 ]
             },
             "properties": {
                 "OBJECTID": 796,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 797,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14591566377692,
                     25.867298888233165
                 ]
             },
             "properties": {
                 "OBJECTID": 797,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 798,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14668164783984,
                     25.867255554400344
                 ]
             },
             "properties": {
                 "OBJECTID": 798,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 799,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14668423968595,
                     25.866919232236853
                 ]
             },
             "properties": {
                 "OBJECTID": 799,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 800,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14670129712721,
                     25.866897646709106
                 ]
             },
             "properties": {
                 "OBJECTID": 800,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 801,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1467151197071,
                     25.866885114656384
                 ]
             },
             "properties": {
                 "OBJECTID": 801,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 802,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14673894094943,
                     25.86686963642461
                 ]
             },
             "properties": {
                 "OBJECTID": 802,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 803,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14741247200806,
                     25.866849380994211
                 ]
             },
             "properties": {
                 "OBJECTID": 803,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 804,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14743540202227,
                     25.866810469127927
                 ]
             },
             "properties": {
                 "OBJECTID": 804,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 805,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14747093873291,
                     25.866780288779353
                 ]
             },
             "properties": {
                 "OBJECTID": 805,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 806,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1475744102309,
                     25.866758636701775
                 ]
             },
             "properties": {
                 "OBJECTID": 806,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 807,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14843532493035,
                     25.866292577440561
                 ]
             },
             "properties": {
                 "OBJECTID": 807,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 808,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14844075413754,
                     25.866273640416239
                 ]
             },
             "properties": {
                 "OBJECTID": 808,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 809,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1496569118371,
                     25.866331349012626
                 ]
             },
             "properties": {
                 "OBJECTID": 809,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 810,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14967956126281,
                     25.866314493918821
                 ]
             },
             "properties": {
                 "OBJECTID": 810,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 811,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14972393471186,
                     25.866296251171093
                 ]
             },
             "properties": {
                 "OBJECTID": 811,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 812,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14997916590585,
                     25.866306503442445
                 ]
             },
             "properties": {
                 "OBJECTID": 812,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 813,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15001000635675,
                     25.866255859020725
                 ]
             },
             "properties": {
                 "OBJECTID": 813,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 814,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15013221522963,
                     25.86620991895262
                 ]
             },
             "properties": {
                 "OBJECTID": 814,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 815,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15015161900209,
                     25.866212211324523
                 ]
             },
             "properties": {
                 "OBJECTID": 815,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 816,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15244899202742,
                     25.866933394760395
                 ]
             },
             "properties": {
                 "OBJECTID": 816,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 817,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.153422929025,
                     25.867314188399121
                 ]
             },
             "properties": {
                 "OBJECTID": 817,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 818,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15345962226382,
                     25.867318749760557
                 ]
             },
             "properties": {
                 "OBJECTID": 818,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 819,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15418232825192,
                     25.867297564431112
                 ]
             },
             "properties": {
                 "OBJECTID": 819,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 820,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15484394059547,
                     25.867240788431673
                 ]
             },
             "properties": {
                 "OBJECTID": 820,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 821,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15599291174357,
                     25.866714625679492
                 ]
             },
             "properties": {
                 "OBJECTID": 821,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 822,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15601429312517,
                     25.866696465669406
                 ]
             },
             "properties": {
                 "OBJECTID": 822,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 823,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15603055286778,
                     25.866686611797775
                 ]
             },
             "properties": {
                 "OBJECTID": 823,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 824,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1560480941443,
                     25.866678763414257
                 ]
             },
             "properties": {
                 "OBJECTID": 824,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 825,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15605725284001,
                     25.866675634672845
                 ]
             },
             "properties": {
                 "OBJECTID": 825,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 826,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15608580631499,
                     25.866669578638209
                 ]
             },
             "properties": {
                 "OBJECTID": 826,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 827,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1565887665584,
                     25.866253954256649
                 ]
             },
             "properties": {
                 "OBJECTID": 827,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 828,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15665446113451,
                     25.865923021730111
                 ]
             },
             "properties": {
                 "OBJECTID": 828,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 829,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1567857927302,
                     25.865696421352823
                 ]
             },
             "properties": {
                 "OBJECTID": 829,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 830,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15719130873197,
                     25.86525690468244
                 ]
             },
             "properties": {
                 "OBJECTID": 830,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 831,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15837977900367,
                     25.863394933929101
                 ]
             },
             "properties": {
                 "OBJECTID": 831,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 832,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15842894044329,
                     25.862972268755243
                 ]
             },
             "properties": {
                 "OBJECTID": 832,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 833,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15842435659886,
                     25.862967246041649
                 ]
             },
             "properties": {
                 "OBJECTID": 833,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 834,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15840292755314,
                     25.862949130098343
                 ]
             },
             "properties": {
                 "OBJECTID": 834,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 835,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15836090133456,
                     25.862902065877677
                 ]
             },
             "properties": {
                 "OBJECTID": 835,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 836,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15840489526977,
                     25.862311444217823
                 ]
             },
             "properties": {
                 "OBJECTID": 836,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 837,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15841134430815,
                     25.862286792901216
                 ]
             },
             "properties": {
                 "OBJECTID": 837,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 838,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15842341500866,
                     25.862262611030701
                 ]
             },
             "properties": {
                 "OBJECTID": 838,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 839,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1584781081782,
                     25.862213010721916
                 ]
             },
             "properties": {
                 "OBJECTID": 839,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 840,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15856105624783,
                     25.862194991905426
                 ]
             },
             "properties": {
                 "OBJECTID": 840,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 841,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15862965113757,
                     25.862176316583771
                 ]
             },
             "properties": {
                 "OBJECTID": 841,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 842,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1586175165852,
                     25.862152161692961
                 ]
             },
             "properties": {
                 "OBJECTID": 842,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 843,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15860985256273,
                     25.862117514411807
                 ]
             },
             "properties": {
                 "OBJECTID": 843,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 844,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15861014394306,
                     25.862096167204413
                 ]
             },
             "properties": {
                 "OBJECTID": 844,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 845,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15854901072834,
                     25.862017668080966
                 ]
             },
             "properties": {
                 "OBJECTID": 845,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 846,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15852093659203,
                     25.86200999596457
                 ]
             },
             "properties": {
                 "OBJECTID": 846,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 847,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15846566605768,
                     25.861974248812601
                 ]
             },
             "properties": {
                 "OBJECTID": 847,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 848,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15845373385281,
                     25.861960214892065
                 ]
             },
             "properties": {
                 "OBJECTID": 848,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 849,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15844855465713,
                     25.861952699257699
                 ]
             },
             "properties": {
                 "OBJECTID": 849,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 850,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15832178262423,
                     25.861261966866323
                 ]
             },
             "properties": {
                 "OBJECTID": 850,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 851,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15648244241083,
                     25.857494070095299
                 ]
             },
             "properties": {
                 "OBJECTID": 851,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 852,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15636170213088,
                     25.856208470344654
                 ]
             },
             "properties": {
                 "OBJECTID": 852,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 853,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15614411296053,
                     25.855629446341936
                 ]
             },
             "properties": {
                 "OBJECTID": 853,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 854,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1558760790179,
                     25.855500810014405
                 ]
             },
             "properties": {
                 "OBJECTID": 854,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 855,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15562531655519,
                     25.855243983423122
                 ]
             },
             "properties": {
                 "OBJECTID": 855,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 856,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1555298211444,
                     25.85508228621859
                 ]
             },
             "properties": {
                 "OBJECTID": 856,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 857,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15548297815695,
                     25.854796739777782
                 ]
             },
             "properties": {
                 "OBJECTID": 857,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 858,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1547659351998,
                     25.853817602900108
                 ]
             },
             "properties": {
                 "OBJECTID": 858,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 859,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15439460422766,
                     25.854113187573603
                 ]
             },
             "properties": {
                 "OBJECTID": 859,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 860,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1543697190873,
                     25.854127225091418
                 ]
             },
             "properties": {
                 "OBJECTID": 860,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 861,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1543422861676,
                     25.854136599624439
                 ]
             },
             "properties": {
                 "OBJECTID": 861,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 862,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1541739402752,
                     25.854073709134411
                 ]
             },
             "properties": {
                 "OBJECTID": 862,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 863,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15370082223524,
                     25.853269942753684
                 ]
             },
             "properties": {
                 "OBJECTID": 863,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 864,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15369791472705,
                     25.853243538658432
                 ]
             },
             "properties": {
                 "OBJECTID": 864,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 865,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15370075568541,
                     25.853217130066525
                 ]
             },
             "properties": {
                 "OBJECTID": 865,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 866,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15361550265339,
                     25.852917920225877
                 ]
             },
             "properties": {
                 "OBJECTID": 866,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 867,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15363085498001,
                     25.852858039766772
                 ]
             },
             "properties": {
                 "OBJECTID": 867,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 868,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15336381209033,
                     25.851452090941279
                 ]
             },
             "properties": {
                 "OBJECTID": 868,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 869,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15335780192106,
                     25.851430407387454
                 ]
             },
             "properties": {
                 "OBJECTID": 869,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 870,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15280541673667,
                     25.850452581721299
                 ]
             },
             "properties": {
                 "OBJECTID": 870,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 871,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15274815959992,
                     25.850442338443202
                 ]
             },
             "properties": {
                 "OBJECTID": 871,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 872,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15266365480386,
                     25.850360881449546
                 ]
             },
             "properties": {
                 "OBJECTID": 872,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 873,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15263163534178,
                     25.850126564889081
                 ]
             },
             "properties": {
                 "OBJECTID": 873,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 874,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1526309599509,
                     25.850119104113389
                 ]
             },
             "properties": {
                 "OBJECTID": 874,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 875,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15264040822831,
                     25.850032431951206
                 ]
             },
             "properties": {
                 "OBJECTID": 875,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 876,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15266609556392,
                     25.849994958100865
                 ]
             },
             "properties": {
                 "OBJECTID": 876,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 877,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15269621026198,
                     25.849646677652117
                 ]
             },
             "properties": {
                 "OBJECTID": 877,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 878,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15281518966935,
                     25.849531470900729
                 ]
             },
             "properties": {
                 "OBJECTID": 878,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 879,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15324416178868,
                     25.848703996592235
                 ]
             },
             "properties": {
                 "OBJECTID": 879,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 880,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15324708098808,
                     25.848335413948234
                 ]
             },
             "properties": {
                 "OBJECTID": 880,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 881,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15325495635119,
                     25.84829352802393
                 ]
             },
             "properties": {
                 "OBJECTID": 881,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 882,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15325839356007,
                     25.848285235375386
                 ]
             },
             "properties": {
                 "OBJECTID": 882,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 883,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15338472132788,
                     25.848201968945773
                 ]
             },
             "properties": {
                 "OBJECTID": 883,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 884,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15374178185783,
                     25.848171288574179
                 ]
             },
             "properties": {
                 "OBJECTID": 884,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 885,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1535422465775,
                     25.846716460696143
                 ]
             },
             "properties": {
                 "OBJECTID": 885,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 886,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15349297631985,
                     25.846668925230745
                 ]
             },
             "properties": {
                 "OBJECTID": 886,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 887,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15334845526706,
                     25.846464956292778
                 ]
             },
             "properties": {
                 "OBJECTID": 887,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 888,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15334746241552,
                     25.846438437084259
                 ]
             },
             "properties": {
                 "OBJECTID": 888,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 889,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15338128501833,
                     25.84574259564431
                 ]
             },
             "properties": {
                 "OBJECTID": 889,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 890,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15346171498624,
                     25.84564752561289
                 ]
             },
             "properties": {
                 "OBJECTID": 890,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 891,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15349867622297,
                     25.845636130303205
                 ]
             },
             "properties": {
                 "OBJECTID": 891,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 892,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15356133918453,
                     25.845126890093979
                 ]
             },
             "properties": {
                 "OBJECTID": 892,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 893,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15351871131952,
                     25.845048891892873
                 ]
             },
             "properties": {
                 "OBJECTID": 893,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 894,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15327233934704,
                     25.843719008625385
                 ]
             },
             "properties": {
                 "OBJECTID": 894,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 895,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15324140896394,
                     25.843718275677929
                 ]
             },
             "properties": {
                 "OBJECTID": 895,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 896,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15322221473349,
                     25.843714842066333
                 ]
             },
             "properties": {
                 "OBJECTID": 896,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 897,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15314841816502,
                     25.84367342109158
                 ]
             },
             "properties": {
                 "OBJECTID": 897,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 898,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15288445365468,
                     25.842793690776261
                 ]
             },
             "properties": {
                 "OBJECTID": 898,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 899,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15278635290792,
                     25.842790964031792
                 ]
             },
             "properties": {
                 "OBJECTID": 899,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 900,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1527419443853,
                     25.842772810317001
                 ]
             },
             "properties": {
                 "OBJECTID": 900,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 901,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15270062863118,
                     25.842735508237183
                 ]
             },
             "properties": {
                 "OBJECTID": 901,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 902,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15269545123414,
                     25.842727991703498
                 ]
             },
             "properties": {
                 "OBJECTID": 902,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 903,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15218103362787,
                     25.842430745982199
                 ]
             },
             "properties": {
                 "OBJECTID": 903,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 904,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15200359109292,
                     25.841566183632494
                 ]
             },
             "properties": {
                 "OBJECTID": 904,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 905,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15202930540818,
                     25.841539591578965
                 ]
             },
             "properties": {
                 "OBJECTID": 905,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 906,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15224448079823,
                     25.841430707061534
                 ]
             },
             "properties": {
                 "OBJECTID": 906,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 907,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15190448220613,
                     25.840885550627718
                 ]
             },
             "properties": {
                 "OBJECTID": 907,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 908,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15185390703226,
                     25.840859195995165
                 ]
             },
             "properties": {
                 "OBJECTID": 908,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 909,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1518142793056,
                     25.840809270131786
                 ]
             },
             "properties": {
                 "OBJECTID": 909,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 910,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15159011429239,
                     25.840080535088646
                 ]
             },
             "properties": {
                 "OBJECTID": 910,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 911,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15160200782645,
                     25.840066476886477
                 ]
             },
             "properties": {
                 "OBJECTID": 911,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 912,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15167527379492,
                     25.840014962820305
                 ]
             },
             "properties": {
                 "OBJECTID": 912,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 913,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15148908715173,
                     25.839578545213726
                 ]
             },
             "properties": {
                 "OBJECTID": 913,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 914,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15138062081905,
                     25.839383582986045
                 ]
             },
             "properties": {
                 "OBJECTID": 914,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 915,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15103764996763,
                     25.839475000870664
                 ]
             },
             "properties": {
                 "OBJECTID": 915,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 916,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15096102143406,
                     25.839438034237958
                 ]
             },
             "properties": {
                 "OBJECTID": 916,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 917,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15065663869296,
                     25.838924687724216
                 ]
             },
             "properties": {
                 "OBJECTID": 917,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 918,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15046702203551,
                     25.838360384724012
                 ]
             },
             "properties": {
                 "OBJECTID": 918,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 919,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15048425304587,
                     25.838328678225992
                 ]
             },
             "properties": {
                 "OBJECTID": 919,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 920,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15060679017131,
                     25.838249215029293
                 ]
             },
             "properties": {
                 "OBJECTID": 920,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 921,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15010330202585,
                     25.837813604114615
                 ]
             },
             "properties": {
                 "OBJECTID": 921,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 922,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15007081221825,
                     25.837794005189266
                 ]
             },
             "properties": {
                 "OBJECTID": 922,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 923,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14932552425358,
                     25.836914266780013
                 ]
             },
             "properties": {
                 "OBJECTID": 923,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 924,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14854278312299,
                     25.836333232792526
                 ]
             },
             "properties": {
                 "OBJECTID": 924,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 925,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1484222029224,
                     25.83618183012743
                 ]
             },
             "properties": {
                 "OBJECTID": 925,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 926,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1479691901261,
                     25.835919711925214
                 ]
             },
             "properties": {
                 "OBJECTID": 926,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 927,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14781480440973,
                     25.835844364026229
                 ]
             },
             "properties": {
                 "OBJECTID": 927,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 928,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14768591267477,
                     25.835491936803464
                 ]
             },
             "properties": {
                 "OBJECTID": 928,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 929,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14727046815761,
                     25.834725737802614
                 ]
             },
             "properties": {
                 "OBJECTID": 929,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 930,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14716486616572,
                     25.834581124119609
                 ]
             },
             "properties": {
                 "OBJECTID": 930,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 931,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14716229230601,
                     25.834563572950572
                 ]
             },
             "properties": {
                 "OBJECTID": 931,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 932,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14715197708216,
                     25.8339990577104
                 ]
             },
             "properties": {
                 "OBJECTID": 932,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 933,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14725983367447,
                     25.833894619441196
                 ]
             },
             "properties": {
                 "OBJECTID": 933,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 934,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14735756839713,
                     25.833877832695862
                 ]
             },
             "properties": {
                 "OBJECTID": 934,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 935,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14718212685375,
                     25.833439421292326
                 ]
             },
             "properties": {
                 "OBJECTID": 935,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 936,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14634557838582,
                     25.831424412007038
                 ]
             },
             "properties": {
                 "OBJECTID": 936,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 937,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1463379188599,
                     25.831389762927245
                 ]
             },
             "properties": {
                 "OBJECTID": 937,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 938,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14635790359443,
                     25.8312062661563
                 ]
             },
             "properties": {
                 "OBJECTID": 938,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 939,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14636134170263,
                     25.831197975306338
                 ]
             },
             "properties": {
                 "OBJECTID": 939,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 940,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14636537066536,
                     25.831189902991696
                 ]
             },
             "properties": {
                 "OBJECTID": 940,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 941,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14638082821273,
                     25.831167355189336
                 ]
             },
             "properties": {
                 "OBJECTID": 941,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 942,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14658987292438,
                     25.831075072156921
                 ]
             },
             "properties": {
                 "OBJECTID": 942,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 943,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14670657075169,
                     25.831076509273544
                 ]
             },
             "properties": {
                 "OBJECTID": 943,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 944,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14685150279468,
                     25.830967248839499
                 ]
             },
             "properties": {
                 "OBJECTID": 944,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 945,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14685954723041,
                     25.830689120006923
                 ]
             },
             "properties": {
                 "OBJECTID": 945,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 946,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14692647837433,
                     25.830434823208577
                 ]
             },
             "properties": {
                 "OBJECTID": 946,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 947,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14688005716897,
                     25.830412760140803
                 ]
             },
             "properties": {
                 "OBJECTID": 947,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 948,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14684153291046,
                     25.830373091944693
                 ]
             },
             "properties": {
                 "OBJECTID": 948,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 949,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14683691039517,
                     25.830365284030677
                 ]
             },
             "properties": {
                 "OBJECTID": 949,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 950,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14683286164728,
                     25.830357220709232
                 ]
             },
             "properties": {
                 "OBJECTID": 950,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 951,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14682940375405,
                     25.830348934355868
                 ]
             },
             "properties": {
                 "OBJECTID": 951,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 952,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14682432078581,
                     25.830331838243808
                 ]
             },
             "properties": {
                 "OBJECTID": 952,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 953,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14682271639532,
                     25.830323101330123
                 ]
             },
             "properties": {
                 "OBJECTID": 953,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 954,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14682141507632,
                     25.830305434148499
                 ]
             },
             "properties": {
                 "OBJECTID": 954,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 955,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14690372102996,
                     25.830005354663456
                 ]
             },
             "properties": {
                 "OBJECTID": 955,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 956,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14690655119642,
                     25.829996875855215
                 ]
             },
             "properties": {
                 "OBJECTID": 956,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 957,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14690687225442,
                     25.829996028693813
                 ]
             },
             "properties": {
                 "OBJECTID": 957,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 958,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.147009527168,
                     25.829547100618242
                 ]
             },
             "properties": {
                 "OBJECTID": 958,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 959,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14698727074602,
                     25.829182373367871
                 ]
             },
             "properties": {
                 "OBJECTID": 959,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 960,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14703096340827,
                     25.829086620751013
                 ]
             },
             "properties": {
                 "OBJECTID": 960,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 961,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14703809862942,
                     25.829080560219722
                 ]
             },
             "properties": {
                 "OBJECTID": 961,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 962,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14705360743807,
                     25.829069765657209
                 ]
             },
             "properties": {
                 "OBJECTID": 962,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 963,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14710749571441,
                     25.829049503032195
                 ]
             },
             "properties": {
                 "OBJECTID": 963,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 964,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14752514087263,
                     25.828803443124457
                 ]
             },
             "properties": {
                 "OBJECTID": 964,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 965,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1475072686456,
                     25.828786032249639
                 ]
             },
             "properties": {
                 "OBJECTID": 965,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 966,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14746465067316,
                     25.828708032249949
                 ]
             },
             "properties": {
                 "OBJECTID": 966,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 967,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14746157679042,
                     25.828632711330613
                 ]
             },
             "properties": {
                 "OBJECTID": 967,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 968,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14763417917408,
                     25.828263236559167
                 ]
             },
             "properties": {
                 "OBJECTID": 968,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 969,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14843177800424,
                     25.827868950391803
                 ]
             },
             "properties": {
                 "OBJECTID": 969,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 970,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14860248731497,
                     25.827686995958175
                 ]
             },
             "properties": {
                 "OBJECTID": 970,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 971,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14864370504301,
                     25.827649612040034
                 ]
             },
             "properties": {
                 "OBJECTID": 971,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 972,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14868806500226,
                     25.827631369292305
                 ]
             },
             "properties": {
                 "OBJECTID": 972,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 973,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1488015684377,
                     25.827644779083357
                 ]
             },
             "properties": {
                 "OBJECTID": 973,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 974,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14882603629258,
                     25.827654178797388
                 ]
             },
             "properties": {
                 "OBJECTID": 974,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 975,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14892642671327,
                     25.827455445712303
                 ]
             },
             "properties": {
                 "OBJECTID": 975,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 976,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14893398641442,
                     25.827449819553578
                 ]
             },
             "properties": {
                 "OBJECTID": 976,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 977,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14904698622951,
                     25.826966359310063
                 ]
             },
             "properties": {
                 "OBJECTID": 977,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 978,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14902440155493,
                     25.826892766887738
                 ]
             },
             "properties": {
                 "OBJECTID": 978,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 979,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14901472934633,
                     25.826486866875484
                 ]
             },
             "properties": {
                 "OBJECTID": 979,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 980,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14897969535673,
                     25.826471274429821
                 ]
             },
             "properties": {
                 "OBJECTID": 980,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 981,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1489503127068,
                     25.826448006270539
                 ]
             },
             "properties": {
                 "OBJECTID": 981,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 982,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14894409569354,
                     25.826441165127733
                 ]
             },
             "properties": {
                 "OBJECTID": 982,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 983,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14883259055165,
                     25.825543855764408
                 ]
             },
             "properties": {
                 "OBJECTID": 983,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 984,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1488582751893,
                     25.825506383712707
                 ]
             },
             "properties": {
                 "OBJECTID": 984,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 985,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14887502596173,
                     25.825488366694799
                 ]
             },
             "properties": {
                 "OBJECTID": 985,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 986,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14776753514371,
                     25.824554001665888
                 ]
             },
             "properties": {
                 "OBJECTID": 986,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 987,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14775237077538,
                     25.824503004709925
                 ]
             },
             "properties": {
                 "OBJECTID": 987,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 988,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14773603009377,
                     25.824357352310017
                 ]
             },
             "properties": {
                 "OBJECTID": 988,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 989,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14772588394243,
                     25.824323231132269
                 ]
             },
             "properties": {
                 "OBJECTID": 989,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 990,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14780344867029,
                     25.82398154181476
                 ]
             },
             "properties": {
                 "OBJECTID": 990,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 991,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14804731782954,
                     25.823564175446393
                 ]
             },
             "properties": {
                 "OBJECTID": 991,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 992,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14878513512593,
                     25.823623903020746
                 ]
             },
             "properties": {
                 "OBJECTID": 992,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 993,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14879979407527,
                     25.823346518826838
                 ]
             },
             "properties": {
                 "OBJECTID": 993,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 994,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14884238326943,
                     25.823207957581246
                 ]
             },
             "properties": {
                 "OBJECTID": 994,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 995,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14864647855222,
                     25.822762290447372
                 ]
             },
             "properties": {
                 "OBJECTID": 995,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 996,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14864467271354,
                     25.822717505108812
                 ]
             },
             "properties": {
                 "OBJECTID": 996,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 997,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14876363143651,
                     25.822602302854023
                 ]
             },
             "properties": {
                 "OBJECTID": 997,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 998,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14853378540596,
                     25.822600984447945
                 ]
             },
             "properties": {
                 "OBJECTID": 998,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 999,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14838935338599,
                     25.822521366567855
                 ]
             },
             "properties": {
                 "OBJECTID": 999,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1000,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14814260909418,
                     25.822492893132562
                 ]
             },
             "properties": {
                 "OBJECTID": 1000,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1001,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14807451602616,
                     25.822390440566437
                 ]
             },
             "properties": {
                 "OBJECTID": 1001,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1002,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14806943215865,
                     25.82237334535364
                 ]
             },
             "properties": {
                 "OBJECTID": 1002,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1003,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14824983256381,
                     25.821631579134134
                 ]
             },
             "properties": {
                 "OBJECTID": 1003,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1004,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14828716791851,
                     25.821388534653181
                 ]
             },
             "properties": {
                 "OBJECTID": 1004,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1005,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14815801717879,
                     25.821359954198556
                 ]
             },
             "properties": {
                 "OBJECTID": 1005,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1006,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14812193188169,
                     25.821346467065837
                 ]
             },
             "properties": {
                 "OBJECTID": 1006,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1007,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14799434236505,
                     25.821256146354131
                 ]
             },
             "properties": {
                 "OBJECTID": 1007,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1008,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14796754436674,
                     25.821183605239241
                 ]
             },
             "properties": {
                 "OBJECTID": 1008,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1009,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14790603523539,
                     25.821033831246382
                 ]
             },
             "properties": {
                 "OBJECTID": 1009,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1010,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14791743324304,
                     25.820920121866209
                 ]
             },
             "properties": {
                 "OBJECTID": 1010,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1011,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14796320064124,
                     25.820853345405624
                 ]
             },
             "properties": {
                 "OBJECTID": 1011,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1012,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1477318131727,
                     25.820342940574278
                 ]
             },
             "properties": {
                 "OBJECTID": 1012,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1013,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14761169342495,
                     25.820225692361873
                 ]
             },
             "properties": {
                 "OBJECTID": 1013,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1014,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14710823495716,
                     25.819729351128728
                 ]
             },
             "properties": {
                 "OBJECTID": 1014,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1015,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14710753618391,
                     25.819728800743633
                 ]
             },
             "properties": {
                 "OBJECTID": 1015,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1016,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14708747230907,
                     25.819709453628434
                 ]
             },
             "properties": {
                 "OBJECTID": 1016,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1017,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14660210280488,
                     25.819087736110191
                 ]
             },
             "properties": {
                 "OBJECTID": 1017,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1018,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14636491201117,
                     25.818659083250168
                 ]
             },
             "properties": {
                 "OBJECTID": 1018,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1019,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14638486796736,
                     25.818591385883792
                 ]
             },
             "properties": {
                 "OBJECTID": 1019,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1020,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14645985973482,
                     25.818006661977904
                 ]
             },
             "properties": {
                 "OBJECTID": 1020,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1021,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14633131873541,
                     25.81793345266675
                 ]
             },
             "properties": {
                 "OBJECTID": 1021,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1022,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1463288977605,
                     25.817931740357608
                 ]
             },
             "properties": {
                 "OBJECTID": 1022,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1023,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14558868287151,
                     25.817341803081774
                 ]
             },
             "properties": {
                 "OBJECTID": 1023,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1024,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14551588634913,
                     25.817215374589921
                 ]
             },
             "properties": {
                 "OBJECTID": 1024,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1025,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14502001006548,
                     25.816650886329398
                 ]
             },
             "properties": {
                 "OBJECTID": 1025,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1026,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14350231597916,
                     25.814727818334177
                 ]
             },
             "properties": {
                 "OBJECTID": 1026,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1027,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14344096332985,
                     25.814631850779335
                 ]
             },
             "properties": {
                 "OBJECTID": 1027,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1028,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14329308870515,
                     25.814259751786153
                 ]
             },
             "properties": {
                 "OBJECTID": 1028,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1029,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14326314667693,
                     25.814143018885261
                 ]
             },
             "properties": {
                 "OBJECTID": 1029,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1030,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14320118338799,
                     25.813359256124841
                 ]
             },
             "properties": {
                 "OBJECTID": 1030,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1031,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14306945698991,
                     25.81331815620797
                 ]
             },
             "properties": {
                 "OBJECTID": 1031,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1032,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14279714137501,
                     25.81270468007699
                 ]
             },
             "properties": {
                 "OBJECTID": 1032,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1033,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14278822369761,
                     25.81270103332605
                 ]
             },
             "properties": {
                 "OBJECTID": 1033,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1034,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14277127327568,
                     25.812692195688328
                 ]
             },
             "properties": {
                 "OBJECTID": 1034,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1035,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14271095574605,
                     25.81261836224769
                 ]
             },
             "properties": {
                 "OBJECTID": 1035,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1036,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14250403163584,
                     25.812240851134447
                 ]
             },
             "properties": {
                 "OBJECTID": 1036,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1037,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14244614497369,
                     25.81224781728298
                 ]
             },
             "properties": {
                 "OBJECTID": 1037,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1038,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14229857701775,
                     25.812215930021182
                 ]
             },
             "properties": {
                 "OBJECTID": 1038,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1039,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14228994082816,
                     25.812211762562868
                 ]
             },
             "properties": {
                 "OBJECTID": 1039,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1040,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1422230168788,
                     25.812138116181188
                 ]
             },
             "properties": {
                 "OBJECTID": 1040,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1041,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14222334962795,
                     25.81193174605329
                 ]
             },
             "properties": {
                 "OBJECTID": 1041,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1042,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14219204872416,
                     25.811886003836037
                 ]
             },
             "properties": {
                 "OBJECTID": 1042,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1043,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14212128377028,
                     25.81162422108099
                 ]
             },
             "properties": {
                 "OBJECTID": 1043,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1044,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14202217128621,
                     25.810650130299337
                 ]
             },
             "properties": {
                 "OBJECTID": 1044,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1045,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14199053403598,
                     25.810100692193259
                 ]
             },
             "properties": {
                 "OBJECTID": 1045,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1046,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1420413538255,
                     25.809504058565494
                 ]
             },
             "properties": {
                 "OBJECTID": 1046,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1047,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14313275936939,
                     25.807515383228406
                 ]
             },
             "properties": {
                 "OBJECTID": 1047,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1048,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14281888158615,
                     25.807452755340421
                 ]
             },
             "properties": {
                 "OBJECTID": 1048,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1049,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14275946517722,
                     25.807395457734231
                 ]
             },
             "properties": {
                 "OBJECTID": 1049,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1050,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14275541732871,
                     25.807387394412785
                 ]
             },
             "properties": {
                 "OBJECTID": 1050,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1051,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14310880772535,
                     25.80604562570511
                 ]
             },
             "properties": {
                 "OBJECTID": 1051,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1052,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14305106765272,
                     25.806019388883726
                 ]
             },
             "properties": {
                 "OBJECTID": 1052,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1053,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14303411633148,
                     25.806010553044644
                 ]
             },
             "properties": {
                 "OBJECTID": 1053,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1054,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14297604890561,
                     25.805852872212313
                 ]
             },
             "properties": {
                 "OBJECTID": 1054,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1055,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14306965484076,
                     25.805613377355542
                 ]
             },
             "properties": {
                 "OBJECTID": 1055,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1056,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14307368380355,
                     25.805605305940162
                 ]
             },
             "properties": {
                 "OBJECTID": 1056,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1057,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14315964819929,
                     25.805536913397873
                 ]
             },
             "properties": {
                 "OBJECTID": 1057,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1058,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1432044407324,
                     25.805352793396764
                 ]
             },
             "properties": {
                 "OBJECTID": 1058,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1059,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14313046609817,
                     25.805208257954803
                 ]
             },
             "properties": {
                 "OBJECTID": 1059,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1060,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14318713058168,
                     25.804961953431473
                 ]
             },
             "properties": {
                 "OBJECTID": 1060,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1061,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14358878399526,
                     25.802922711913652
                 ]
             },
             "properties": {
                 "OBJECTID": 1061,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1062,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1437413395912,
                     25.802319996170922
                 ]
             },
             "properties": {
                 "OBJECTID": 1062,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1063,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14374880576281,
                     25.802303630308359
                 ]
             },
             "properties": {
                 "OBJECTID": 1063,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1064,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14375340939239,
                     25.802295814300464
                 ]
             },
             "properties": {
                 "OBJECTID": 1064,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1065,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14375856610502,
                     25.802288286974942
                 ]
             },
             "properties": {
                 "OBJECTID": 1065,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1066,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14377045604175,
                     25.80227422787334
                 ]
             },
             "properties": {
                 "OBJECTID": 1066,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1067,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14379977573913,
                     25.802250900358843
                 ]
             },
             "properties": {
                 "OBJECTID": 1067,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1068,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14380808007888,
                     25.802246214890943
                 ]
             },
             "properties": {
                 "OBJECTID": 1068,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1069,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14388282183484,
                     25.802228005418158
                 ]
             },
             "properties": {
                 "OBJECTID": 1069,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1070,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14390234161982,
                     25.802229143959892
                 ]
             },
             "properties": {
                 "OBJECTID": 1070,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1071,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14490618107544,
                     25.801568432737042
                 ]
             },
             "properties": {
                 "OBJECTID": 1071,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1072,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14474199364815,
                     25.801072987228622
                 ]
             },
             "properties": {
                 "OBJECTID": 1072,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1073,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14488456766873,
                     25.800540217157334
                 ]
             },
             "properties": {
                 "OBJECTID": 1073,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1074,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14498311627784,
                     25.800506526754873
                 ]
             },
             "properties": {
                 "OBJECTID": 1074,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1075,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14480892479213,
                     25.799809302157598
                 ]
             },
             "properties": {
                 "OBJECTID": 1075,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1076,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14477797912048,
                     25.799775244831721
                 ]
             },
             "properties": {
                 "OBJECTID": 1076,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1077,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14476076789521,
                     25.799733991130836
                 ]
             },
             "properties": {
                 "OBJECTID": 1077,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1078,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14414589331841,
                     25.799474943214136
                 ]
             },
             "properties": {
                 "OBJECTID": 1078,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1079,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14411735333323,
                     25.799480998349452
                 ]
             },
             "properties": {
                 "OBJECTID": 1079,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1080,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14404976478488,
                     25.799475041240214
                 ]
             },
             "properties": {
                 "OBJECTID": 1080,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1081,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14395613456804,
                     25.799390468995
                 ]
             },
             "properties": {
                 "OBJECTID": 1081,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1082,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14373196236022,
                     25.79810084028162
                 ]
             },
             "properties": {
                 "OBJECTID": 1082,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1083,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14373571163384,
                     25.798048134613794
                 ]
             },
             "properties": {
                 "OBJECTID": 1083,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1084,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14373854269962,
                     25.798039656704816
                 ]
             },
             "properties": {
                 "OBJECTID": 1084,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1085,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14374197900918,
                     25.798031363156952
                 ]
             },
             "properties": {
                 "OBJECTID": 1085,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1086,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14375060980285,
                     25.798015474834358
                 ]
             },
             "properties": {
                 "OBJECTID": 1086,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1087,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14387288342687,
                     25.797941078418091
                 ]
             },
             "properties": {
                 "OBJECTID": 1087,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1088,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14368565536876,
                     25.797628877870466
                 ]
             },
             "properties": {
                 "OBJECTID": 1088,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1089,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14368181796158,
                     25.797611517357666
                 ]
             },
             "properties": {
                 "OBJECTID": 1089,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1090,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14364201037057,
                     25.797229338763032
                 ]
             },
             "properties": {
                 "OBJECTID": 1090,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1091,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14341278397285,
                     25.796734809663803
                 ]
             },
             "properties": {
                 "OBJECTID": 1091,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1092,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14336888986242,
                     25.796641015770319
                 ]
             },
             "properties": {
                 "OBJECTID": 1092,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1093,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14329251943428,
                     25.796462424801121
                 ]
             },
             "properties": {
                 "OBJECTID": 1093,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1094,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14328868202711,
                     25.796445063389001
                 ]
             },
             "properties": {
                 "OBJECTID": 1094,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1095,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14329953234761,
                     25.79632645720028
                 ]
             },
             "properties": {
                 "OBJECTID": 1095,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1096,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14331383966203,
                     25.796282656619383
                 ]
             },
             "properties": {
                 "OBJECTID": 1096,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1097,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14338173577852,
                     25.796221056656464
                 ]
             },
             "properties": {
                 "OBJECTID": 1097,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1098,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14340915700706,
                     25.796211683022818
                 ]
             },
             "properties": {
                 "OBJECTID": 1098,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1099,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14341868262613,
                     25.796209662246156
                 ]
             },
             "properties": {
                 "OBJECTID": 1099,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1100,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14350239601885,
                     25.795536894711972
                 ]
             },
             "properties": {
                 "OBJECTID": 1100,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1101,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14337622833034,
                     25.794731577000732
                 ]
             },
             "properties": {
                 "OBJECTID": 1101,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1102,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14337001311566,
                     25.794724734958606
                 ]
             },
             "properties": {
                 "OBJECTID": 1102,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1103,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14334415221094,
                     25.794677393746781
                 ]
             },
             "properties": {
                 "OBJECTID": 1103,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1104,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14330264040461,
                     25.794351165573858
                 ]
             },
             "properties": {
                 "OBJECTID": 1104,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1105,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14329400511429,
                     25.794346999014863
                 ]
             },
             "properties": {
                 "OBJECTID": 1105,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1106,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14327773098256,
                     25.794337179317438
                 ]
             },
             "properties": {
                 "OBJECTID": 1106,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1107,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14326301177863,
                     25.794325519607071
                 ]
             },
             "properties": {
                 "OBJECTID": 1107,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1108,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14322725113681,
                     25.7939459058789
                 ]
             },
             "properties": {
                 "OBJECTID": 1108,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1109,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1432370132776,
                     25.793930563444803
                 ]
             },
             "properties": {
                 "OBJECTID": 1109,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1110,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14330405503819,
                     25.793880642078022
                 ]
             },
             "properties": {
                 "OBJECTID": 1110,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1111,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14332256668314,
                     25.793874932282336
                 ]
             },
             "properties": {
                 "OBJECTID": 1111,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1112,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14359122025866,
                     25.793542902582942
                 ]
             },
             "properties": {
                 "OBJECTID": 1112,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1113,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14363881867661,
                     25.79338043915601
                 ]
             },
             "properties": {
                 "OBJECTID": 1113,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1114,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14356113973486,
                     25.793388266855061
                 ]
             },
             "properties": {
                 "OBJECTID": 1114,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1115,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14346684401971,
                     25.793371384781608
                 ]
             },
             "properties": {
                 "OBJECTID": 1115,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1116,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14344300029427,
                     25.793355952415311
                 ]
             },
             "properties": {
                 "OBJECTID": 1116,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1117,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14342915523139,
                     25.793343446442975
                 ]
             },
             "properties": {
                 "OBJECTID": 1117,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1118,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14333800714337,
                     25.793031506698753
                 ]
             },
             "properties": {
                 "OBJECTID": 1118,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1119,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14333895502881,
                     25.793022690644705
                 ]
             },
             "properties": {
                 "OBJECTID": 1119,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1120,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14338850767354,
                     25.792938541980163
                 ]
             },
             "properties": {
                 "OBJECTID": 1120,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1121,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14324666030518,
                     25.792378667241735
                 ]
             },
             "properties": {
                 "OBJECTID": 1121,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1122,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14324373840788,
                     25.792370004971815
                 ]
             },
             "properties": {
                 "OBJECTID": 1122,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1123,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14323891174644,
                     25.792326124351234
                 ]
             },
             "properties": {
                 "OBJECTID": 1123,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1124,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14324144333801,
                     25.792308567786222
                 ]
             },
             "properties": {
                 "OBJECTID": 1124,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1125,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14324365297227,
                     25.792299938791189
                 ]
             },
             "properties": {
                 "OBJECTID": 1125,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1126,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14315475678666,
                     25.791897490376925
                 ]
             },
             "properties": {
                 "OBJECTID": 1126,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1127,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14315053626831,
                     25.791887566358128
                 ]
             },
             "properties": {
                 "OBJECTID": 1127,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1128,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14274067384309,
                     25.791127480948546
                 ]
             },
             "properties": {
                 "OBJECTID": 1128,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1129,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14274063427291,
                     25.790837411817165
                 ]
             },
             "properties": {
                 "OBJECTID": 1129,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1130,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14274158305767,
                     25.790828594863797
                 ]
             },
             "properties": {
                 "OBJECTID": 1130,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1131,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14279113480308,
                     25.790744447997952
                 ]
             },
             "properties": {
                 "OBJECTID": 1131,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1132,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14283235343044,
                     25.790682016162179
                 ]
             },
             "properties": {
                 "OBJECTID": 1132,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1133,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14282340427673,
                     25.790639178755157
                 ]
             },
             "properties": {
                 "OBJECTID": 1133,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1134,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14247507076794,
                     25.789990470783664
                 ]
             },
             "properties": {
                 "OBJECTID": 1134,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1135,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.142422535072,
                     25.789951524743117
                 ]
             },
             "properties": {
                 "OBJECTID": 1135,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1136,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14240298201207,
                     25.78992094329692
                 ]
             },
             "properties": {
                 "OBJECTID": 1136,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1137,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14239444384856,
                     25.789895559932177
                 ]
             },
             "properties": {
                 "OBJECTID": 1137,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1138,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14239284035733,
                     25.789886822119172
                 ]
             },
             "properties": {
                 "OBJECTID": 1138,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1139,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14239275941839,
                     25.789886093668315
                 ]
             },
             "properties": {
                 "OBJECTID": 1139,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1140,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14239189427059,
                     25.78987832532448
                 ]
             },
             "properties": {
                 "OBJECTID": 1140,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1141,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14243441061961,
                     25.789520039917932
                 ]
             },
             "properties": {
                 "OBJECTID": 1141,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1142,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14241371002475,
                     25.789396079165783
                 ]
             },
             "properties": {
                 "OBJECTID": 1142,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1143,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.142396346814,
                     25.789386364689051
                 ]
             },
             "properties": {
                 "OBJECTID": 1143,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1144,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14237782077987,
                     25.789380691765587
                 ]
             },
             "properties": {
                 "OBJECTID": 1144,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1145,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14231066120811,
                     25.789330905297106
                 ]
             },
             "properties": {
                 "OBJECTID": 1145,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1146,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1422141378722,
                     25.788586924946685
                 ]
             },
             "properties": {
                 "OBJECTID": 1146,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1147,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14222202042998,
                     25.788543407652185
                 ]
             },
             "properties": {
                 "OBJECTID": 1147,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1148,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14244382921942,
                     25.788090429030149
                 ]
             },
             "properties": {
                 "OBJECTID": 1148,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1149,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14235861935487,
                     25.787683715131379
                 ]
             },
             "properties": {
                 "OBJECTID": 1149,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1150,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14236709726379,
                     25.787658314679504
                 ]
             },
             "properties": {
                 "OBJECTID": 1150,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1151,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14269632737364,
                     25.787151642034701
                 ]
             },
             "properties": {
                 "OBJECTID": 1151,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1152,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14288159940634,
                     25.786823184442483
                 ]
             },
             "properties": {
                 "OBJECTID": 1152,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1153,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14317651678289,
                     25.78630979655992
                 ]
             },
             "properties": {
                 "OBJECTID": 1153,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1154,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14447056656343,
                     25.7837079013986
                 ]
             },
             "properties": {
                 "OBJECTID": 1154,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1155,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14583358085514,
                     25.781966244646526
                 ]
             },
             "properties": {
                 "OBJECTID": 1155,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1156,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.146173701755,
                     25.781152557844337
                 ]
             },
             "properties": {
                 "OBJECTID": 1156,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1157,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14705635306831,
                     25.778688416337388
                 ]
             },
             "properties": {
                 "OBJECTID": 1157,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1158,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14707772096011,
                     25.778670256327302
                 ]
             },
             "properties": {
                 "OBJECTID": 1158,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1159,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14711149769749,
                     25.778652554072153
                 ]
             },
             "properties": {
                 "OBJECTID": 1159,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1160,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14712064919865,
                     25.778649425330741
                 ]
             },
             "properties": {
                 "OBJECTID": 1160,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1161,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14713000664455,
                     25.778646844276466
                 ]
             },
             "properties": {
                 "OBJECTID": 1161,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1162,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14718820986803,
                     25.778643330625243
                 ]
             },
             "properties": {
                 "OBJECTID": 1162,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1163,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14732423862279,
                     25.778598872639918
                 ]
             },
             "properties": {
                 "OBJECTID": 1163,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1164,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14732291482073,
                     25.778581051674223
                 ]
             },
             "properties": {
                 "OBJECTID": 1164,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1165,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14734257130266,
                     25.7783832134146
                 ]
             },
             "properties": {
                 "OBJECTID": 1165,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1166,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14738694295312,
                     25.777955051584399
                 ]
             },
             "properties": {
                 "OBJECTID": 1166,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1167,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14747376710073,
                     25.777734137620428
                 ]
             },
             "properties": {
                 "OBJECTID": 1167,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1168,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1474938165864,
                     25.77769394961615
                 ]
             },
             "properties": {
                 "OBJECTID": 1168,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1169,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14755194156891,
                     25.777647692986704
                 ]
             },
             "properties": {
                 "OBJECTID": 1169,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1170,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14761804623589,
                     25.777633667160046
                 ]
             },
             "properties": {
                 "OBJECTID": 1170,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1171,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14772348455125,
                     25.777647605752463
                 ]
             },
             "properties": {
                 "OBJECTID": 1171,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1172,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14796335712327,
                     25.77725765072006
                 ]
             },
             "properties": {
                 "OBJECTID": 1172,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1173,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14798233191914,
                     25.777228114286117
                 ]
             },
             "properties": {
                 "OBJECTID": 1173,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1174,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14808135986692,
                     25.777176212612176
                 ]
             },
             "properties": {
                 "OBJECTID": 1174,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1175,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14813957118434,
                     25.77717960755291
                 ]
             },
             "properties": {
                 "OBJECTID": 1175,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1176,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14822380888171,
                     25.776984129114339
                 ]
             },
             "properties": {
                 "OBJECTID": 1176,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1177,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14822300129055,
                     25.77698207686143
                 ]
             },
             "properties": {
                 "OBJECTID": 1177,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1178,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14828048955303,
                     25.776427766228494
                 ]
             },
             "properties": {
                 "OBJECTID": 1178,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1179,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14828451761645,
                     25.776419693913795
                 ]
             },
             "properties": {
                 "OBJECTID": 1179,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1180,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14828911944738,
                     25.776411877006581
                 ]
             },
             "properties": {
                 "OBJECTID": 1180,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1181,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14841850221217,
                     25.776344066325635
                 ]
             },
             "properties": {
                 "OBJECTID": 1181,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1182,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14848771403683,
                     25.77629992400233
                 ]
             },
             "properties": {
                 "OBJECTID": 1182,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1183,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14846909537249,
                     25.776279432050217
                 ]
             },
             "properties": {
                 "OBJECTID": 1183,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1184,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1484467121461,
                     25.776230660916326
                 ]
             },
             "properties": {
                 "OBJECTID": 1184,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1185,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14848498729236,
                     25.775989943880859
                 ]
             },
             "properties": {
                 "OBJECTID": 1185,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1186,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14851293732227,
                     25.775933922412662
                 ]
             },
             "properties": {
                 "OBJECTID": 1186,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1187,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1486314742632,
                     25.775880844425501
                 ]
             },
             "properties": {
                 "OBJECTID": 1187,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1188,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14878051710724,
                     25.775899035012515
                 ]
             },
             "properties": {
                 "OBJECTID": 1188,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1189,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14881273082295,
                     25.775720255185661
                 ]
             },
             "properties": {
                 "OBJECTID": 1189,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1190,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14880293360858,
                     25.775704930738016
                 ]
             },
             "properties": {
                 "OBJECTID": 1190,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1191,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14879257881449,
                     25.775680107650885
                 ]
             },
             "properties": {
                 "OBJECTID": 1191,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1192,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14879034579786,
                     25.77567148405177
                 ]
             },
             "properties": {
                 "OBJECTID": 1192,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1193,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14878775035447,
                     25.775636227030304
                 ]
             },
             "properties": {
                 "OBJECTID": 1193,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1194,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14879160484873,
                     25.775613201687975
                 ]
             },
             "properties": {
                 "OBJECTID": 1194,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1195,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14881105538598,
                     25.775535109058069
                 ]
             },
             "properties": {
                 "OBJECTID": 1195,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1196,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14880577906354,
                     25.775526257930494
                 ]
             },
             "properties": {
                 "OBJECTID": 1196,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1197,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14867919498892,
                     25.774904841685156
                 ]
             },
             "properties": {
                 "OBJECTID": 1197,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1198,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14866097832157,
                     25.774729178908558
                 ]
             },
             "properties": {
                 "OBJECTID": 1198,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1199,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14869176661182,
                     25.774646747949134
                 ]
             },
             "properties": {
                 "OBJECTID": 1199,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1200,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14870464400417,
                     25.774633421795045
                 ]
             },
             "properties": {
                 "OBJECTID": 1200,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1201,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14871933173185,
                     25.77462173330639
                 ]
             },
             "properties": {
                 "OBJECTID": 1201,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1202,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1487531066706,
                     25.77460403105124
                 ]
             },
             "properties": {
                 "OBJECTID": 1202,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1203,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14870498394794,
                     25.773737840928675
                 ]
             },
             "properties": {
                 "OBJECTID": 1203,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1204,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1487357731375,
                     25.773655409069931
                 ]
             },
             "properties": {
                 "OBJECTID": 1204,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1205,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14881562124401,
                     25.773606982376407
                 ]
             },
             "properties": {
                 "OBJECTID": 1205,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1206,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14883479658863,
                     25.77360350739599
                 ]
             },
             "properties": {
                 "OBJECTID": 1206,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1207,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14884453085045,
                     25.77360262965766
                 ]
             },
             "properties": {
                 "OBJECTID": 1207,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1208,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1489101390917,
                     25.773601149373576
                 ]
             },
             "properties": {
                 "OBJECTID": 1208,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1209,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14891561686227,
                     25.773598532346455
                 ]
             },
             "properties": {
                 "OBJECTID": 1209,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1210,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14891783099318,
                     25.773591293703305
                 ]
             },
             "properties": {
                 "OBJECTID": 1210,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1211,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14875479739607,
                     25.773276461739385
                 ]
             },
             "properties": {
                 "OBJECTID": 1211,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1212,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14872151528579,
                     25.773269975828782
                 ]
             },
             "properties": {
                 "OBJECTID": 1212,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1213,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14869480811893,
                     25.773259052663207
                 ]
             },
             "properties": {
                 "OBJECTID": 1213,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1214,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14861991527692,
                     25.773141906074159
                 ]
             },
             "properties": {
                 "OBJECTID": 1214,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1215,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14869469390504,
                     25.772806718855065
                 ]
             },
             "properties": {
                 "OBJECTID": 1215,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1216,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.148717327143,
                     25.772789862861941
                 ]
             },
             "properties": {
                 "OBJECTID": 1216,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1217,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14875230717331,
                     25.772774199369849
                 ]
             },
             "properties": {
                 "OBJECTID": 1217,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1218,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14918767617036,
                     25.772595621890503
                 ]
             },
             "properties": {
                 "OBJECTID": 1218,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1219,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14917295966444,
                     25.772583963079455
                 ]
             },
             "properties": {
                 "OBJECTID": 1219,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1220,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14916626421177,
                     25.772577505947197
                 ]
             },
             "properties": {
                 "OBJECTID": 1220,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1221,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14916004989647,
                     25.772570664804391
                 ]
             },
             "properties": {
                 "OBJECTID": 1221,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1222,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14915434189942,
                     25.772563471127341
                 ]
             },
             "properties": {
                 "OBJECTID": 1222,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1223,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14913338679645,
                     25.772448314737971
                 ]
             },
             "properties": {
                 "OBJECTID": 1223,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1224,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14913349111777,
                     25.772447579092557
                 ]
             },
             "properties": {
                 "OBJECTID": 1224,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1225,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14915503527669,
                     25.772378705412962
                 ]
             },
             "properties": {
                 "OBJECTID": 1225,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1226,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14925033013867,
                     25.772307729118438
                 ]
             },
             "properties": {
                 "OBJECTID": 1226,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1227,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14926950368471,
                     25.772304255037398
                 ]
             },
             "properties": {
                 "OBJECTID": 1227,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1228,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14931437535819,
                     25.772305012266543
                 ]
             },
             "properties": {
                 "OBJECTID": 1228,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1229,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1493408091311,
                     25.772208442165947
                 ]
             },
             "properties": {
                 "OBJECTID": 1229,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1230,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14932528233595,
                     25.772197679079682
                 ]
             },
             "properties": {
                 "OBJECTID": 1230,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1231,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1493052247564,
                     25.772178332863803
                 ]
             },
             "properties": {
                 "OBJECTID": 1231,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1232,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14927713443228,
                     25.772122368952239
                 ]
             },
             "properties": {
                 "OBJECTID": 1232,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1233,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1493304939072,
                     25.771923110663067
                 ]
             },
             "properties": {
                 "OBJECTID": 1233,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1234,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14938514930526,
                     25.771873510354283
                 ]
             },
             "properties": {
                 "OBJECTID": 1234,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1235,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14942118603898,
                     25.771859951275758
                 ]
             },
             "properties": {
                 "OBJECTID": 1235,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1236,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14947938566519,
                     25.771856435825896
                 ]
             },
             "properties": {
                 "OBJECTID": 1236,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1237,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14966696625754,
                     25.771882527856405
                 ]
             },
             "properties": {
                 "OBJECTID": 1237,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1238,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14975700638081,
                     25.771637682033429
                 ]
             },
             "properties": {
                 "OBJECTID": 1238,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1239,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14976563537584,
                     25.771621793710835
                 ]
             },
             "properties": {
                 "OBJECTID": 1239,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1240,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14978267842793,
                     25.771600207283768
                 ]
             },
             "properties": {
                 "OBJECTID": 1240,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1241,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14985313401496,
                     25.771538780890069
                 ]
             },
             "properties": {
                 "OBJECTID": 1241,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1242,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14991492193616,
                     25.771487284810405
                 ]
             },
             "properties": {
                 "OBJECTID": 1242,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1243,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14992605014714,
                     25.771479530855743
                 ]
             },
             "properties": {
                 "OBJECTID": 1243,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1244,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14993108635059,
                     25.771474034199343
                 ]
             },
             "properties": {
                 "OBJECTID": 1244,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1245,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15023496816929,
                     25.770855663058455
                 ]
             },
             "properties": {
                 "OBJECTID": 1245,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1246,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15027629741326,
                     25.770832332845941
                 ]
             },
             "properties": {
                 "OBJECTID": 1246,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1247,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15032371506743,
                     25.770822270331564
                 ]
             },
             "properties": {
                 "OBJECTID": 1247,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1248,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15034326812736,
                     25.770822249647154
                 ]
             },
             "properties": {
                 "OBJECTID": 1248,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1249,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.150424515579,
                     25.770827177032629
                 ]
             },
             "properties": {
                 "OBJECTID": 1249,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1250,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15046693210331,
                     25.770448747711612
                 ]
             },
             "properties": {
                 "OBJECTID": 1250,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1251,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1504550115896,
                     25.770434713791076
                 ]
             },
             "properties": {
                 "OBJECTID": 1251,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1252,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15043486137984,
                     25.77039456715562
                 ]
             },
             "properties": {
                 "OBJECTID": 1252,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1253,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15042468645015,
                     25.770350869996719
                 ]
             },
             "properties": {
                 "OBJECTID": 1253,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1254,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14936814672268,
                     25.770293560699315
                 ]
             },
             "properties": {
                 "OBJECTID": 1254,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1255,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14930057616073,
                     25.770287605388717
                 ]
             },
             "properties": {
                 "OBJECTID": 1255,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1256,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14921042182357,
                     25.770211323094145
                 ]
             },
             "properties": {
                 "OBJECTID": 1256,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1257,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14913671788526,
                     25.770084230003249
                 ]
             },
             "properties": {
                 "OBJECTID": 1257,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1258,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14909710994374,
                     25.77003430324055
                 ]
             },
             "properties": {
                 "OBJECTID": 1258,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1259,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1490549865984,
                     25.769646671156977
                 ]
             },
             "properties": {
                 "OBJECTID": 1259,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1260,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14907883032384,
                     25.769589112747383
                 ]
             },
             "properties": {
                 "OBJECTID": 1260,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1261,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14912832900922,
                     25.76954704066344
                 ]
             },
             "properties": {
                 "OBJECTID": 1261,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1262,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14894646810507,
                     25.769167090588837
                 ]
             },
             "properties": {
                 "OBJECTID": 1262,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1263,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14893681388293,
                     25.769165657968813
                 ]
             },
             "properties": {
                 "OBJECTID": 1263,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1264,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14886737542912,
                     25.769134740176185
                 ]
             },
             "properties": {
                 "OBJECTID": 1264,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1265,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14857640068038,
                     25.768929174941547
                 ]
             },
             "properties": {
                 "OBJECTID": 1265,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1266,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14856687596063,
                     25.768931195718153
                 ]
             },
             "properties": {
                 "OBJECTID": 1266,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1267,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14845463787128,
                     25.768911098568367
                 ]
             },
             "properties": {
                 "OBJECTID": 1267,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1268,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14843196416388,
                     25.768894289340039
                 ]
             },
             "properties": {
                 "OBJECTID": 1268,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1269,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14842526781194,
                     25.768887831308405
                 ]
             },
             "properties": {
                 "OBJECTID": 1269,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1270,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14823619524429,
                     25.768523136433657
                 ]
             },
             "properties": {
                 "OBJECTID": 1270,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1271,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14814756885528,
                     25.768475973287593
                 ]
             },
             "properties": {
                 "OBJECTID": 1271,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1272,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14783112530625,
                     25.767868520815625
                 ]
             },
             "properties": {
                 "OBJECTID": 1272,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1273,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14772455384514,
                     25.767847500961466
                 ]
             },
             "properties": {
                 "OBJECTID": 1273,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1274,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1476631148609,
                     25.767770050447496
                 ]
             },
             "properties": {
                 "OBJECTID": 1274,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1275,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14752767426279,
                     25.767156702919578
                 ]
             },
             "properties": {
                 "OBJECTID": 1275,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1276,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14750689902422,
                     25.767137874713171
                 ]
             },
             "properties": {
                 "OBJECTID": 1276,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1277,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14720701199406,
                     25.766970757294644
                 ]
             },
             "properties": {
                 "OBJECTID": 1277,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1278,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14718560093479,
                     25.766952638653379
                 ]
             },
             "properties": {
                 "OBJECTID": 1278,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1279,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14689074471215,
                     25.766630354007702
                 ]
             },
             "properties": {
                 "OBJECTID": 1279,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1280,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14688728861756,
                     25.766622067654339
                 ]
             },
             "properties": {
                 "OBJECTID": 1280,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1281,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14675787977239,
                     25.766237000437286
                 ]
             },
             "properties": {
                 "OBJECTID": 1281,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1282,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14671835007186,
                     25.765940976894626
                 ]
             },
             "properties": {
                 "OBJECTID": 1282,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1283,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14673454506328,
                     25.765754583407329
                 ]
             },
             "properties": {
                 "OBJECTID": 1283,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1284,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14683313234326,
                     25.765257007405012
                 ]
             },
             "properties": {
                 "OBJECTID": 1284,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1285,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14683534287684,
                     25.765248379309298
                 ]
             },
             "properties": {
                 "OBJECTID": 1285,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1286,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14686838576745,
                     25.765137498296951
                 ]
             },
             "properties": {
                 "OBJECTID": 1286,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1287,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1468758492411,
                     25.765121134233027
                 ]
             },
             "properties": {
                 "OBJECTID": 1287,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1288,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14690417069096,
                     25.765085259377258
                 ]
             },
             "properties": {
                 "OBJECTID": 1288,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1289,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14752863743672,
                     25.763708858676921
                 ]
             },
             "properties": {
                 "OBJECTID": 1289,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1290,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14753432474936,
                     25.763701654208035
                 ]
             },
             "properties": {
                 "OBJECTID": 1290,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1291,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14755433196689,
                     25.763682265724015
                 ]
             },
             "properties": {
                 "OBJECTID": 1291,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1292,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14760481001406,
                     25.763655807669466
                 ]
             },
             "properties": {
                 "OBJECTID": 1292,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1293,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14822001284335,
                     25.763646557242851
                 ]
             },
             "properties": {
                 "OBJECTID": 1293,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1294,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14791301217582,
                     25.763515654623802
                 ]
             },
             "properties": {
                 "OBJECTID": 1294,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1295,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14784600369018,
                     25.763354989840934
                 ]
             },
             "properties": {
                 "OBJECTID": 1295,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1296,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14785346716383,
                     25.76333862487769
                 ]
             },
             "properties": {
                 "OBJECTID": 1296,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1297,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14876010519475,
                     25.761023570281566
                 ]
             },
             "properties": {
                 "OBJECTID": 1297,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1298,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14902389883395,
                     25.760633986669177
                 ]
             },
             "properties": {
                 "OBJECTID": 1298,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1299,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14906509228024,
                     25.760596600053077
                 ]
             },
             "properties": {
                 "OBJECTID": 1299,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1300,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14909091901075,
                     25.760584065302396
                 ]
             },
             "properties": {
                 "OBJECTID": 1300,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1301,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14910006961259,
                     25.760580935661721
                 ]
             },
             "properties": {
                 "OBJECTID": 1301,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1302,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1491383330677,
                     25.760574002788076
                 ]
             },
             "properties": {
                 "OBJECTID": 1302,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1303,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14918949819685,
                     25.760569840725623
                 ]
             },
             "properties": {
                 "OBJECTID": 1303,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1304,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14938610708322,
                     25.760245850965305
                 ]
             },
             "properties": {
                 "OBJECTID": 1304,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1305,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15005358930176,
                     25.759361615046544
                 ]
             },
             "properties": {
                 "OBJECTID": 1305,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1306,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15006646309683,
                     25.759348289791774
                 ]
             },
             "properties": {
                 "OBJECTID": 1306,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1307,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15008909363684,
                     25.759331432899387
                 ]
             },
             "properties": {
                 "OBJECTID": 1307,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1308,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15012407096918,
                     25.759315767608655
                 ]
             },
             "properties": {
                 "OBJECTID": 1308,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1309,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15018188478621,
                     25.759308814949918
                 ]
             },
             "properties": {
                 "OBJECTID": 1309,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1310,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15019162084667,
                     25.759309673802477
                 ]
             },
             "properties": {
                 "OBJECTID": 1310,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1311,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15100497040311,
                     25.759465897733037
                 ]
             },
             "properties": {
                 "OBJECTID": 1311,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1312,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15101005067334,
                     25.759430747730846
                 ]
             },
             "properties": {
                 "OBJECTID": 1312,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1313,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15108473487265,
                     25.759150095400628
                 ]
             },
             "properties": {
                 "OBJECTID": 1313,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1314,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15089414604807,
                     25.759085453930595
                 ]
             },
             "properties": {
                 "OBJECTID": 1314,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1315,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15085262884583,
                     25.759065164325932
                 ]
             },
             "properties": {
                 "OBJECTID": 1315,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1316,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15080950275643,
                     25.759017690014446
                 ]
             },
             "properties": {
                 "OBJECTID": 1316,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1317,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15086627875581,
                     25.75872395344777
                 ]
             },
             "properties": {
                 "OBJECTID": 1317,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1318,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1509133123995,
                     25.758668316889384
                 ]
             },
             "properties": {
                 "OBJECTID": 1318,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1319,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15092125611113,
                     25.758663150284235
                 ]
             },
             "properties": {
                 "OBJECTID": 1319,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1320,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15103343394588,
                     25.75864282290803
                 ]
             },
             "properties": {
                 "OBJECTID": 1320,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1321,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15115176674072,
                     25.758514988775744
                 ]
             },
             "properties": {
                 "OBJECTID": 1321,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1322,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15113300058755,
                     25.758283541951926
                 ]
             },
             "properties": {
                 "OBJECTID": 1322,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1323,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15121888044706,
                     25.758076768028445
                 ]
             },
             "properties": {
                 "OBJECTID": 1323,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1324,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15125028837019,
                     25.758034829943483
                 ]
             },
             "properties": {
                 "OBJECTID": 1324,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1325,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1515612703364,
                     25.757925563214201
                 ]
             },
             "properties": {
                 "OBJECTID": 1325,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1326,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15160830128212,
                     25.757869927555191
                 ]
             },
             "properties": {
                 "OBJECTID": 1326,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1327,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15163316843604,
                     25.75785589003732
                 ]
             },
             "properties": {
                 "OBJECTID": 1327,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1328,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15168547030828,
                     25.757840697790016
                 ]
             },
             "properties": {
                 "OBJECTID": 1328,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1329,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15187463190881,
                     25.757612544283518
                 ]
             },
             "properties": {
                 "OBJECTID": 1329,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1330,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15218579823608,
                     25.757482571563457
                 ]
             },
             "properties": {
                 "OBJECTID": 1330,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1331,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15218923184761,
                     25.757474278914913
                 ]
             },
             "properties": {
                 "OBJECTID": 1331,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1332,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15221526092563,
                     25.757436431845917
                 ]
             },
             "properties": {
                 "OBJECTID": 1332,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1333,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15222918782683,
                     25.757334384874014
                 ]
             },
             "properties": {
                 "OBJECTID": 1333,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1334,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15223580863574,
                     25.757308535660457
                 ]
             },
             "properties": {
                 "OBJECTID": 1334,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1335,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15229421780396,
                     25.75723943804968
                 ]
             },
             "properties": {
                 "OBJECTID": 1335,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1336,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15236307529574,
                     25.757042096215798
                 ]
             },
             "properties": {
                 "OBJECTID": 1336,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1337,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15237513610373,
                     25.75701791434534
                 ]
             },
             "properties": {
                 "OBJECTID": 1337,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1338,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15244893896744,
                     25.756722916929107
                 ]
             },
             "properties": {
                 "OBJECTID": 1338,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1339,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15249167385173,
                     25.756405279979731
                 ]
             },
             "properties": {
                 "OBJECTID": 1339,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1340,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15240856120619,
                     25.756273864747129
                 ]
             },
             "properties": {
                 "OBJECTID": 1340,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1341,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15234424439143,
                     25.755909910014395
                 ]
             },
             "properties": {
                 "OBJECTID": 1341,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1342,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15231326004897,
                     25.7558884324053
                 ]
             },
             "properties": {
                 "OBJECTID": 1342,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1343,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15226645573239,
                     25.755741100071759
                 ]
             },
             "properties": {
                 "OBJECTID": 1343,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1344,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15224891085859,
                     25.755733286761824
                 ]
             },
             "properties": {
                 "OBJECTID": 1344,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1345,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1521854690842,
                     25.755667930330787
                 ]
             },
             "properties": {
                 "OBJECTID": 1345,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1346,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15217916393732,
                     25.755651169665782
                 ]
             },
             "properties": {
                 "OBJECTID": 1346,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1347,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15217104485788,
                     25.755429773665185
                 ]
             },
             "properties": {
                 "OBJECTID": 1347,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1348,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15217102956944,
                     25.755427980417039
                 ]
             },
             "properties": {
                 "OBJECTID": 1348,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1349,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15217638683083,
                     25.755221835119642
                 ]
             },
             "properties": {
                 "OBJECTID": 1349,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1350,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15218142573229,
                     25.755204728215688
                 ]
             },
             "properties": {
                 "OBJECTID": 1350,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1351,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15219348743955,
                     25.755180545445853
                 ]
             },
             "properties": {
                 "OBJECTID": 1351,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1352,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15229762713386,
                     25.755114698884313
                 ]
             },
             "properties": {
                 "OBJECTID": 1352,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1353,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15231214219165,
                     25.75511217898395
                 ]
             },
             "properties": {
                 "OBJECTID": 1353,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1354,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15231244796115,
                     25.755111444237798
                 ]
             },
             "properties": {
                 "OBJECTID": 1354,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1355,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15229312422838,
                     25.755100603809865
                 ]
             },
             "properties": {
                 "OBJECTID": 1355,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1356,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15224204273613,
                     25.755047978181722
                 ]
             },
             "properties": {
                 "OBJECTID": 1356,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1357,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15224779030336,
                     25.754578543764808
                 ]
             },
             "properties": {
                 "OBJECTID": 1357,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1358,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15224793059758,
                     25.754576248694946
                 ]
             },
             "properties": {
                 "OBJECTID": 1358,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1359,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15224887938234,
                     25.754567432640954
                 ]
             },
             "properties": {
                 "OBJECTID": 1359,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1360,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1522526709241,
                     25.754550063134957
                 ]
             },
             "properties": {
                 "OBJECTID": 1360,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1361,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15235842130414,
                     25.754138697043288
                 ]
             },
             "properties": {
                 "OBJECTID": 1361,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1362,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15235845817637,
                     25.75413815385275
                 ]
             },
             "properties": {
                 "OBJECTID": 1362,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1363,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15185173786745,
                     25.753834244155087
                 ]
             },
             "properties": {
                 "OBJECTID": 1363,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1364,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15182790133662,
                     25.75381881178879
                 ]
             },
             "properties": {
                 "OBJECTID": 1364,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1365,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1517882978917,
                     25.753768885026091
                 ]
             },
             "properties": {
                 "OBJECTID": 1365,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1366,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1517819918455,
                     25.753752126159725
                 ]
             },
             "properties": {
                 "OBJECTID": 1366,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1367,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15177718496915,
                     25.753725949592933
                 ]
             },
             "properties": {
                 "OBJECTID": 1367,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1368,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15326960900529,
                     25.750693272876276
                 ]
             },
             "properties": {
                 "OBJECTID": 1368,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1369,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15386694410427,
                     25.750280711585617
                 ]
             },
             "properties": {
                 "OBJECTID": 1369,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1370,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15403198229023,
                     25.750091648910541
                 ]
             },
             "properties": {
                 "OBJECTID": 1370,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1371,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15405048763995,
                     25.750085939114854
                 ]
             },
             "properties": {
                 "OBJECTID": 1371,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1372,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15407939095121,
                     25.750081586396163
                 ]
             },
             "properties": {
                 "OBJECTID": 1372,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1373,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1540989422125,
                     25.750081564812433
                 ]
             },
             "properties": {
                 "OBJECTID": 1373,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1374,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1541463769538,
                     25.750091529300676
                 ]
             },
             "properties": {
                 "OBJECTID": 1374,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1375,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15417697908435,
                     25.750092890874271
                 ]
             },
             "properties": {
                 "OBJECTID": 1375,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1376,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15432158197547,
                     25.749962626773879
                 ]
             },
             "properties": {
                 "OBJECTID": 1376,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1377,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15440266664979,
                     25.749892452674601
                 ]
             },
             "properties": {
                 "OBJECTID": 1377,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1378,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1544097982736,
                     25.74988639124399
                 ]
             },
             "properties": {
                 "OBJECTID": 1378,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1379,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15449852808467,
                     25.74985299581914
                 ]
             },
             "properties": {
                 "OBJECTID": 1379,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1380,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15450285562235,
                     25.749624042815356
                 ]
             },
             "properties": {
                 "OBJECTID": 1380,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1381,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15449600009038,
                     25.74962015144888
                 ]
             },
             "properties": {
                 "OBJECTID": 1381,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1382,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15449575727342,
                     25.74962001475194
                 ]
             },
             "properties": {
                 "OBJECTID": 1382,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1383,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15448780097131,
                     25.749614863435227
                 ]
             },
             "properties": {
                 "OBJECTID": 1383,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1384,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15443717363672,
                     25.749551039448988
                 ]
             },
             "properties": {
                 "OBJECTID": 1384,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1385,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15443432368517,
                     25.749542566036666
                 ]
             },
             "properties": {
                 "OBJECTID": 1385,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1386,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15444452199716,
                     25.749447656084499
                 ]
             },
             "properties": {
                 "OBJECTID": 1386,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1387,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1548000428877,
                     25.748964170659974
                 ]
             },
             "properties": {
                 "OBJECTID": 1387,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1388,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15482266983042,
                     25.748947312868268
                 ]
             },
             "properties": {
                 "OBJECTID": 1388,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1389,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1548309705729,
                     25.748942627400368
                 ]
             },
             "properties": {
                 "OBJECTID": 1389,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1390,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15483959057468,
                     25.748938442854922
                 ]
             },
             "properties": {
                 "OBJECTID": 1390,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1391,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15490567815459,
                     25.748924414330304
                 ]
             },
             "properties": {
                 "OBJECTID": 1391,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1392,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15493483957124,
                     25.748926982794046
                 ]
             },
             "properties": {
                 "OBJECTID": 1392,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1393,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15497180080803,
                     25.748938301661326
                 ]
             },
             "properties": {
                 "OBJECTID": 1393,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1394,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15498356304107,
                     25.748944147254633
                 ]
             },
             "properties": {
                 "OBJECTID": 1394,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1395,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15508063226542,
                     25.748992324835967
                 ]
             },
             "properties": {
                 "OBJECTID": 1395,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1396,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15546500070928,
                     25.748755525247361
                 ]
             },
             "properties": {
                 "OBJECTID": 1396,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1397,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15557513798228,
                     25.74876705275733
                 ]
             },
             "properties": {
                 "OBJECTID": 1397,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1398,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.155850603023,
                     25.748816564932554
                 ]
             },
             "properties": {
                 "OBJECTID": 1398,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1399,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15643756174416,
                     25.748154356338546
                 ]
             },
             "properties": {
                 "OBJECTID": 1399,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1400,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15704921944598,
                     25.74769069466987
                 ]
             },
             "properties": {
                 "OBJECTID": 1400,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1401,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15685289024873,
                     25.746773786381198
                 ]
             },
             "properties": {
                 "OBJECTID": 1401,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1402,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15685724656475,
                     25.746768870686878
                 ]
             },
             "properties": {
                 "OBJECTID": 1402,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1403,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1570989582504,
                     25.746508952226407
                 ]
             },
             "properties": {
                 "OBJECTID": 1403,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1404,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15857138586085,
                     25.744988229223452
                 ]
             },
             "properties": {
                 "OBJECTID": 1404,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1405,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1585832677037,
                     25.744974168323267
                 ]
             },
             "properties": {
                 "OBJECTID": 1405,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1406,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15874462676317,
                     25.744935376966112
                 ]
             },
             "properties": {
                 "OBJECTID": 1406,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1407,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15973719501562,
                     25.743114053770626
                 ]
             },
             "properties": {
                 "OBJECTID": 1407,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1408,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15974193264412,
                     25.74308786821058
                 ]
             },
             "properties": {
                 "OBJECTID": 1408,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1409,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15974476101195,
                     25.7430793876037
                 ]
             },
             "properties": {
                 "OBJECTID": 1409,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1410,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15981145833217,
                     25.743005600927802
                 ]
             },
             "properties": {
                 "OBJECTID": 1410,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1411,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1604107080878,
                     25.742868519966294
                 ]
             },
             "properties": {
                 "OBJECTID": 1411,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1412,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16041838560005,
                     25.742859009635652
                 ]
             },
             "properties": {
                 "OBJECTID": 1412,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1413,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16067735617509,
                     25.742565927775388
                 ]
             },
             "properties": {
                 "OBJECTID": 1413,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1414,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16068823707252,
                     25.742554906583734
                 ]
             },
             "properties": {
                 "OBJECTID": 1414,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1415,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16087093614487,
                     25.742422217911098
                 ]
             },
             "properties": {
                 "OBJECTID": 1415,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1416,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16270450091207,
                     25.741657466818708
                 ]
             },
             "properties": {
                 "OBJECTID": 1416,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1417,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1645191448427,
                     25.740014263347632
                 ]
             },
             "properties": {
                 "OBJECTID": 1417,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1418,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16501344641347,
                     25.73988242723226
                 ]
             },
             "properties": {
                 "OBJECTID": 1418,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1419,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16609399624099,
                     25.738201629401715
                 ]
             },
             "properties": {
                 "OBJECTID": 1419,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1420,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16611624366976,
                     25.738152809704445
                 ]
             },
             "properties": {
                 "OBJECTID": 1420,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1421,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16624524422269,
                     25.738002603137659
                 ]
             },
             "properties": {
                 "OBJECTID": 1421,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1422,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16704667506406,
                     25.73797312426035
                 ]
             },
             "properties": {
                 "OBJECTID": 1422,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1423,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16730809898962,
                     25.738217531214161
                 ]
             },
             "properties": {
                 "OBJECTID": 1423,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1424,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16733875418021,
                     25.738259749887618
                 ]
             },
             "properties": {
                 "OBJECTID": 1424,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1425,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16734221117417,
                     25.738268036240925
                 ]
             },
             "properties": {
                 "OBJECTID": 1425,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1426,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16735581701738,
                     25.738332870165891
                 ]
             },
             "properties": {
                 "OBJECTID": 1426,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1427,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16766589426567,
                     25.738665688570734
                 ]
             },
             "properties": {
                 "OBJECTID": 1427,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1428,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16918593108591,
                     25.738180201255318
                 ]
             },
             "properties": {
                 "OBJECTID": 1428,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1429,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17072270339008,
                     25.737606941007584
                 ]
             },
             "properties": {
                 "OBJECTID": 1429,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1430,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17073109676278,
                     25.737528725170591
                 ]
             },
             "properties": {
                 "OBJECTID": 1430,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1431,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17073512212824,
                     25.737520651956572
                 ]
             },
             "properties": {
                 "OBJECTID": 1431,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1432,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17078605163505,
                     25.737467913913122
                 ]
             },
             "properties": {
                 "OBJECTID": 1432,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1433,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17073298443972,
                     25.737297140750513
                 ]
             },
             "properties": {
                 "OBJECTID": 1433,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1434,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17076703367172,
                     25.737181445667261
                 ]
             },
             "properties": {
                 "OBJECTID": 1434,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1435,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17083815115978,
                     25.737117692727395
                 ]
             },
             "properties": {
                 "OBJECTID": 1435,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1436,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17068111334277,
                     25.736764463309385
                 ]
             },
             "properties": {
                 "OBJECTID": 1436,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1437,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17027403701724,
                     25.735970395217635
                 ]
             },
             "properties": {
                 "OBJECTID": 1437,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1438,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17024451317383,
                     25.735912773855489
                 ]
             },
             "properties": {
                 "OBJECTID": 1438,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1439,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1703526269722,
                     25.735712152194139
                 ]
             },
             "properties": {
                 "OBJECTID": 1439,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1440,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1708649509568,
                     25.735420431907471
                 ]
             },
             "properties": {
                 "OBJECTID": 1440,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1441,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1708747247888,
                     25.735420710697269
                 ]
             },
             "properties": {
                 "OBJECTID": 1441,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1442,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17088445905063,
                     25.735421567751189
                 ]
             },
             "properties": {
                 "OBJECTID": 1442,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1443,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17091299903575,
                     25.73542756083333
                 ]
             },
             "properties": {
                 "OBJECTID": 1443,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1444,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17166296257574,
                     25.735353400039457
                 ]
             },
             "properties": {
                 "OBJECTID": 1444,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1445,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1716990262891,
                     25.735366879977562
                 ]
             },
             "properties": {
                 "OBJECTID": 1445,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1446,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17172286102129,
                     25.73538230784726
                 ]
             },
             "properties": {
                 "OBJECTID": 1446,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1447,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17174291500356,
                     25.7354016522645
                 ]
             },
             "properties": {
                 "OBJECTID": 1447,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1448,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17324177728625,
                     25.734800491449562
                 ]
             },
             "properties": {
                 "OBJECTID": 1448,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1449,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17357118726056,
                     25.734407818665943
                 ]
             },
             "properties": {
                 "OBJECTID": 1449,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1450,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17357864443898,
                     25.73439145190406
                 ]
             },
             "properties": {
                 "OBJECTID": 1450,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1451,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17359408310051,
                     25.734368899605101
                 ]
             },
             "properties": {
                 "OBJECTID": 1451,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1452,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17490463633993,
                     25.733088273105352
                 ]
             },
             "properties": {
                 "OBJECTID": 1452,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1453,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17521309930515,
                     25.732739731853201
                 ]
             },
             "properties": {
                 "OBJECTID": 1453,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1454,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1753358396773,
                     25.732728634219143
                 ]
             },
             "properties": {
                 "OBJECTID": 1454,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1455,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17536600473738,
                     25.732741416283375
                 ]
             },
             "properties": {
                 "OBJECTID": 1455,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1456,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17598100701792,
                     25.733082355566296
                 ]
             },
             "properties": {
                 "OBJECTID": 1456,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1457,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17675822541185,
                     25.733086916028412
                 ]
             },
             "properties": {
                 "OBJECTID": 1457,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1458,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17681123505054,
                     25.733070994430875
                 ]
             },
             "properties": {
                 "OBJECTID": 1458,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1459,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17693646924272,
                     25.733109330731054
                 ]
             },
             "properties": {
                 "OBJECTID": 1459,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1460,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17694937721205,
                     25.733122627207536
                 ]
             },
             "properties": {
                 "OBJECTID": 1460,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1461,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17696892757402,
                     25.733153204157077
                 ]
             },
             "properties": {
                 "OBJECTID": 1461,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1462,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17723587873286,
                     25.733481743587674
                 ]
             },
             "properties": {
                 "OBJECTID": 1462,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1463,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18410148219772,
                     25.732580321624596
                 ]
             },
             "properties": {
                 "OBJECTID": 1463,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1464,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18627048509489,
                     25.731918747951909
                 ]
             },
             "properties": {
                 "OBJECTID": 1464,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1465,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18657721326781,
                     25.731375331706261
                 ]
             },
             "properties": {
                 "OBJECTID": 1465,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1466,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18702008171147,
                     25.730610850410471
                 ]
             },
             "properties": {
                 "OBJECTID": 1466,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1467,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18711921937654,
                     25.730412881749146
                 ]
             },
             "properties": {
                 "OBJECTID": 1467,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1468,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18716353526901,
                     25.730394626410941
                 ]
             },
             "properties": {
                 "OBJECTID": 1468,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1469,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18720220431834,
                     25.730389963426148
                 ]
             },
             "properties": {
                 "OBJECTID": 1469,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1470,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18723843620495,
                     25.730393954617341
                 ]
             },
             "properties": {
                 "OBJECTID": 1470,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1471,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18736544925616,
                     25.729385750751874
                 ]
             },
             "properties": {
                 "OBJECTID": 1471,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1472,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1873277541726,
                     25.729376654109331
                 ]
             },
             "properties": {
                 "OBJECTID": 1472,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1473,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18728637456661,
                     25.729353420124312
                 ]
             },
             "properties": {
                 "OBJECTID": 1473,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1474,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18723814122728,
                     25.729225310799507
                 ]
             },
             "properties": {
                 "OBJECTID": 1474,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1475,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18749244881752,
                     25.728690431818052
                 ]
             },
             "properties": {
                 "OBJECTID": 1475,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1476,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18750000042473,
                     25.728684802961311
                 ]
             },
             "properties": {
                 "OBJECTID": 1476,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1477,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18753375557844,
                     25.728667091712964
                 ]
             },
             "properties": {
                 "OBJECTID": 1477,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1478,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18758878689306,
                     25.728651240262593
                 ]
             },
             "properties": {
                 "OBJECTID": 1478,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1479,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18760759711296,
                     25.728557051566725
                 ]
             },
             "properties": {
                 "OBJECTID": 1479,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1480,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18761385459578,
                     25.728540277411923
                 ]
             },
             "properties": {
                 "OBJECTID": 1480,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1481,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18765330605527,
                     25.728490260717024
                 ]
             },
             "properties": {
                 "OBJECTID": 1481,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1482,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18769461191687,
                     25.728466920611936
                 ]
             },
             "properties": {
                 "OBJECTID": 1482,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1483,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18773227822214,
                     25.728457726842691
                 ]
             },
             "properties": {
                 "OBJECTID": 1483,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1484,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18776492900832,
                     25.728457053250452
                 ]
             },
             "properties": {
                 "OBJECTID": 1484,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1485,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18868781509065,
                     25.728527642836525
                 ]
             },
             "properties": {
                 "OBJECTID": 1485,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1486,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18873489729771,
                     25.728411922572207
                 ]
             },
             "properties": {
                 "OBJECTID": 1486,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1487,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18891857303379,
                     25.72799319642894
                 ]
             },
             "properties": {
                 "OBJECTID": 1487,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1488,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18894019813172,
                     25.727963787698684
                 ]
             },
             "properties": {
                 "OBJECTID": 1488,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1489,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18895400002714,
                     25.727951250250044
                 ]
             },
             "properties": {
                 "OBJECTID": 1489,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1490,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18909210261847,
                     25.72763051693488
                 ]
             },
             "properties": {
                 "OBJECTID": 1490,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1491,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18909553443143,
                     25.727622221588319
                 ]
             },
             "properties": {
                 "OBJECTID": 1491,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1492,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18913498589097,
                     25.72757220489342
                 ]
             },
             "properties": {
                 "OBJECTID": 1492,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1493,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18927486913998,
                     25.726631267618984
                 ]
             },
             "properties": {
                 "OBJECTID": 1493,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1494,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18920684442037,
                     25.726569826836112
                 ]
             },
             "properties": {
                 "OBJECTID": 1494,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1495,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18919933687994,
                     25.726553479859263
                 ]
             },
             "properties": {
                 "OBJECTID": 1495,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1496,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18912482624978,
                     25.726369429105944
                 ]
             },
             "properties": {
                 "OBJECTID": 1496,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1497,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18910500429257,
                     25.726292923779454
                 ]
             },
             "properties": {
                 "OBJECTID": 1497,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1498,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1892750544003,
                     25.725629344618596
                 ]
             },
             "properties": {
                 "OBJECTID": 1498,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1499,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18941073331877,
                     25.725269773181537
                 ]
             },
             "properties": {
                 "OBJECTID": 1499,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1500,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1894345185882,
                     25.725254285956566
                 ]
             },
             "properties": {
                 "OBJECTID": 1500,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1501,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1894431367914,
                     25.725250099612481
                 ]
             },
             "properties": {
                 "OBJECTID": 1501,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1502,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18953836240559,
                     25.725238618867195
                 ]
             },
             "properties": {
                 "OBJECTID": 1502,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1503,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18992714472199,
                     25.725418382552391
                 ]
             },
             "properties": {
                 "OBJECTID": 1503,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1504,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18989035075913,
                     25.725640542077429
                 ]
             },
             "properties": {
                 "OBJECTID": 1504,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1505,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18999248406595,
                     25.725688668397368
                 ]
             },
             "properties": {
                 "OBJECTID": 1505,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1506,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19036929640595,
                     25.725609619788202
                 ]
             },
             "properties": {
                 "OBJECTID": 1506,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1507,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19044355072936,
                     25.725432122394579
                 ]
             },
             "properties": {
                 "OBJECTID": 1507,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1508,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19049301524052,
                     25.725390036820784
                 ]
             },
             "properties": {
                 "OBJECTID": 1508,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1509,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19079801491654,
                     25.724412240832294
                 ]
             },
             "properties": {
                 "OBJECTID": 1509,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1510,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19073792761333,
                     25.724384062374611
                 ]
             },
             "properties": {
                 "OBJECTID": 1510,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1511,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19072038543749,
                     25.724376253561331
                 ]
             },
             "properties": {
                 "OBJECTID": 1511,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1512,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19066561312758,
                     25.724326781855552
                 ]
             },
             "properties": {
                 "OBJECTID": 1512,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1513,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19064679211584,
                     25.7242767966369
                 ]
             },
             "properties": {
                 "OBJECTID": 1513,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1514,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19065052160437,
                     25.724224092767713
                 ]
             },
             "properties": {
                 "OBJECTID": 1514,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1515,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19116155416248,
                     25.722416980659204
                 ]
             },
             "properties": {
                 "OBJECTID": 1515,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1516,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19114604985037,
                     25.722394468829748
                 ]
             },
             "properties": {
                 "OBJECTID": 1516,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1517,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19113345844244,
                     25.722361025740781
                 ]
             },
             "properties": {
                 "OBJECTID": 1517,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1518,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19113185135393,
                     25.722352288827096
                 ]
             },
             "properties": {
                 "OBJECTID": 1518,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1519,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19113337300683,
                     25.722308213053623
                 ]
             },
             "properties": {
                 "OBJECTID": 1519,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1520,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19124713634631,
                     25.722066564320528
                 ]
             },
             "properties": {
                 "OBJECTID": 1520,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1521,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19132386380534,
                     25.722001278036601
                 ]
             },
             "properties": {
                 "OBJECTID": 1521,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1522,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19152823114302,
                     25.721730786246837
                 ]
             },
             "properties": {
                 "OBJECTID": 1522,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1523,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19154372016664,
                     25.721719987187726
                 ]
             },
             "properties": {
                 "OBJECTID": 1523,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1524,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19178959391468,
                     25.721711019148245
                 ]
             },
             "properties": {
                 "OBJECTID": 1524,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1525,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19183076038132,
                     25.721673619941669
                 ]
             },
             "properties": {
                 "OBJECTID": 1525,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1526,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19186571972722,
                     25.721657945657739
                 ]
             },
             "properties": {
                 "OBJECTID": 1526,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1527,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19190396699452,
                     25.7216510019922
                 ]
             },
             "properties": {
                 "OBJECTID": 1527,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1528,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19192351196057,
                     25.721650976811191
                 ]
             },
             "properties": {
                 "OBJECTID": 1528,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1529,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19194289504861,
                     25.721653262887799
                 ]
             },
             "properties": {
                 "OBJECTID": 1529,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1530,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19160655040201,
                     25.721202339216632
                 ]
             },
             "properties": {
                 "OBJECTID": 1530,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1531,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19156932656324,
                     25.72111314175811
                 ]
             },
             "properties": {
                 "OBJECTID": 1531,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1532,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19179710505239,
                     25.720196667842004
                 ]
             },
             "properties": {
                 "OBJECTID": 1532,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1533,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19192280239548,
                     25.719663057803928
                 ]
             },
             "properties": {
                 "OBJECTID": 1533,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1534,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19193409338374,
                     25.719611245162866
                 ]
             },
             "properties": {
                 "OBJECTID": 1534,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1535,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19193811605129,
                     25.719603172848224
                 ]
             },
             "properties": {
                 "OBJECTID": 1535,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1536,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19196641321946,
                     25.719567290797897
                 ]
             },
             "properties": {
                 "OBJECTID": 1536,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1537,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19269331094762,
                     25.718862134180199
                 ]
             },
             "properties": {
                 "OBJECTID": 1537,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1538,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19343460682171,
                     25.71734516494746
                 ]
             },
             "properties": {
                 "OBJECTID": 1538,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1539,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19352716774466,
                     25.717143398449139
                 ]
             },
             "properties": {
                 "OBJECTID": 1539,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1540,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1946434422411,
                     25.71524654928885
                 ]
             },
             "properties": {
                 "OBJECTID": 1540,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1541,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19471041565322,
                     25.715196607237658
                 ]
             },
             "properties": {
                 "OBJECTID": 1541,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1542,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19471956085908,
                     25.715193475798344
                 ]
             },
             "properties": {
                 "OBJECTID": 1542,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1543,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19510516497144,
                     25.715282283850343
                 ]
             },
             "properties": {
                 "OBJECTID": 1543,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1544,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19516724966888,
                     25.715179062363916
                 ]
             },
             "properties": {
                 "OBJECTID": 1544,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1545,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19532125497199,
                     25.714816182320988
                 ]
             },
             "properties": {
                 "OBJECTID": 1545,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1546,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19553426839184,
                     25.714433813969379
                 ]
             },
             "properties": {
                 "OBJECTID": 1546,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1547,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19562061140215,
                     25.71429754509569
                 ]
             },
             "properties": {
                 "OBJECTID": 1547,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1548,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19573331713895,
                     25.714153357691373
                 ]
             },
             "properties": {
                 "OBJECTID": 1548,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1549,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1957408660482,
                     25.714147728834689
                 ]
             },
             "properties": {
                 "OBJECTID": 1549,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1550,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19590585657011,
                     25.714005832003579
                 ]
             },
             "properties": {
                 "OBJECTID": 1550,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1551,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19610297717071,
                     25.713785359606732
                 ]
             },
             "properties": {
                 "OBJECTID": 1551,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1552,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19640317626562,
                     25.71345910085688
                 ]
             },
             "properties": {
                 "OBJECTID": 1552,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1553,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19640905693251,
                     25.713454041271063
                 ]
             },
             "properties": {
                 "OBJECTID": 1553,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1554,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19642967838701,
                     25.713437234740638
                 ]
             },
             "properties": {
                 "OBJECTID": 1554,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1555,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1966273511714,
                     25.71322517730016
                 ]
             },
             "properties": {
                 "OBJECTID": 1555,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1556,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19680114695541,
                     25.712887303806099
                 ]
             },
             "properties": {
                 "OBJECTID": 1556,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1557,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19680516872364,
                     25.71287923059208
                 ]
             },
             "properties": {
                 "OBJECTID": 1557,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1558,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19781925685328,
                     25.711557889085384
                 ]
             },
             "properties": {
                 "OBJECTID": 1558,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1559,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19799027643018,
                     25.711325685032364
                 ]
             },
             "properties": {
                 "OBJECTID": 1559,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1560,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19818193185074,
                     25.711080617076846
                 ]
             },
             "properties": {
                 "OBJECTID": 1560,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1561,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19848067674189,
                     25.710696772038432
                 ]
             },
             "properties": {
                 "OBJECTID": 1561,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1562,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19910997374637,
                     25.709620387870643
                 ]
             },
             "properties": {
                 "OBJECTID": 1562,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1563,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19948602885722,
                     25.70890258678503
                 ]
             },
             "properties": {
                 "OBJECTID": 1563,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1564,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19949117927456,
                     25.708895057660811
                 ]
             },
             "properties": {
                 "OBJECTID": 1564,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1565,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19950305482217,
                     25.708880995861307
                 ]
             },
             "properties": {
                 "OBJECTID": 1565,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1566,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19953234124461,
                     25.708857657554915
                 ]
             },
             "properties": {
                 "OBJECTID": 1566,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1567,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19954063659117,
                     25.708852970288376
                 ]
             },
             "properties": {
                 "OBJECTID": 1567,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1568,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19964631862274,
                     25.70883611159735
                 ]
             },
             "properties": {
                 "OBJECTID": 1568,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1569,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19964866405468,
                     25.7088336150793
                 ]
             },
             "properties": {
                 "OBJECTID": 1569,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1570,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19978240313543,
                     25.708548487722567
                 ]
             },
             "properties": {
                 "OBJECTID": 1570,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1571,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19978881979824,
                     25.708537880219069
                 ]
             },
             "properties": {
                 "OBJECTID": 1571,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1572,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19988246530357,
                     25.708398279357141
                 ]
             },
             "properties": {
                 "OBJECTID": 1572,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1573,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1999754363174,
                     25.70826165075465
                 ]
             },
             "properties": {
                 "OBJECTID": 1573,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1574,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20030056911605,
                     25.707902393180973
                 ]
             },
             "properties": {
                 "OBJECTID": 1574,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1575,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20051214092263,
                     25.707723962291084
                 ]
             },
             "properties": {
                 "OBJECTID": 1575,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1576,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20077690493025,
                     25.707377137844674
                 ]
             },
             "properties": {
                 "OBJECTID": 1576,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1577,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20087780706518,
                     25.707273764372758
                 ]
             },
             "properties": {
                 "OBJECTID": 1577,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1578,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20122975944588,
                     25.70665036692094
                 ]
             },
             "properties": {
                 "OBJECTID": 1578,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1579,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2013647800606,
                     25.706327163168112
                 ]
             },
             "properties": {
                 "OBJECTID": 1579,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1580,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20283417695578,
                     25.703935112215731
                 ]
             },
             "properties": {
                 "OBJECTID": 1580,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1581,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2031234726702,
                     25.703392022423941
                 ]
             },
             "properties": {
                 "OBJECTID": 1581,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1582,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20446037424693,
                     25.70173651532798
                 ]
             },
             "properties": {
                 "OBJECTID": 1582,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1583,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20446831076401,
                     25.701731346024872
                 ]
             },
             "properties": {
                 "OBJECTID": 1583,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1584,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20450326561325,
                     25.701715668143606
                 ]
             },
             "properties": {
                 "OBJECTID": 1584,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1585,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20472645486205,
                     25.701197295318991
                 ]
             },
             "properties": {
                 "OBJECTID": 1585,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1586,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20554001935648,
                     25.699097469172898
                 ]
             },
             "properties": {
                 "OBJECTID": 1586,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1587,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20563582323473,
                     25.699057980841189
                 ]
             },
             "properties": {
                 "OBJECTID": 1587,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1588,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20653393209534,
                     25.696426066309982
                 ]
             },
             "properties": {
                 "OBJECTID": 1588,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1589,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2066228750457,
                     25.69607525786688
                 ]
             },
             "properties": {
                 "OBJECTID": 1589,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1590,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20662630416069,
                     25.696066964319016
                 ]
             },
             "properties": {
                 "OBJECTID": 1590,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1591,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20663032502955,
                     25.696058891104997
                 ]
             },
             "properties": {
                 "OBJECTID": 1591,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1592,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20667328834162,
                     25.696011311572818
                 ]
             },
             "properties": {
                 "OBJECTID": 1592,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1593,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20684064228186,
                     25.69596201613416
                 ]
             },
             "properties": {
                 "OBJECTID": 1593,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1594,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20685951005839,
                     25.695957407108722
                 ]
             },
             "properties": {
                 "OBJECTID": 1594,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1595,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20686634760392,
                     25.695956314432408
                 ]
             },
             "properties": {
                 "OBJECTID": 1595,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1596,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20722449361557,
                     25.695906961437174
                 ]
             },
             "properties": {
                 "OBJECTID": 1596,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1597,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20723422068278,
                     25.695906077403606
                 ]
             },
             "properties": {
                 "OBJECTID": 1597,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1598,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20724586960125,
                     25.695905783325315
                 ]
             },
             "properties": {
                 "OBJECTID": 1598,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1599,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20732181375075,
                     25.695885544981991
                 ]
             },
             "properties": {
                 "OBJECTID": 1599,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1600,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.207318899048,
                     25.695859140886682
                 ]
             },
             "properties": {
                 "OBJECTID": 1600,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1601,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20731982804767,
                     25.695829223140208
                 ]
             },
             "properties": {
                 "OBJECTID": 1601,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1602,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2073538494007,
                     25.695540061424765
                 ]
             },
             "properties": {
                 "OBJECTID": 1602,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1603,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20742049276157,
                     25.695466254963719
                 ]
             },
             "properties": {
                 "OBJECTID": 1603,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1604,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20754034720932,
                     25.695373352298418
                 ]
             },
             "properties": {
                 "OBJECTID": 1604,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1605,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2075425532463,
                     25.695364722404065
                 ]
             },
             "properties": {
                 "OBJECTID": 1605,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1606,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20762225116601,
                     25.695129040672725
                 ]
             },
             "properties": {
                 "OBJECTID": 1606,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1607,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20759555658975,
                     25.695118127399724
                 ]
             },
             "properties": {
                 "OBJECTID": 1607,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1608,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20755286307423,
                     25.695087758193552
                 ]
             },
             "properties": {
                 "OBJECTID": 1608,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1609,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20730146299218,
                     25.694955347411394
                 ]
             },
             "properties": {
                 "OBJECTID": 1609,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1610,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20730200888067,
                     25.694943566292579
                 ]
             },
             "properties": {
                 "OBJECTID": 1610,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1611,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20734921699284,
                     25.694638706910837
                 ]
             },
             "properties": {
                 "OBJECTID": 1611,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1612,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2074158621524,
                     25.694564899550528
                 ]
             },
             "properties": {
                 "OBJECTID": 1612,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1613,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20752772612371,
                     25.694357532074491
                 ]
             },
             "properties": {
                 "OBJECTID": 1613,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1614,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20750857685937,
                     25.694276852095072
                 ]
             },
             "properties": {
                 "OBJECTID": 1614,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1615,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20749713568432,
                     25.694109401927392
                 ]
             },
             "properties": {
                 "OBJECTID": 1615,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1616,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20737108490772,
                     25.694007352257529
                 ]
             },
             "properties": {
                 "OBJECTID": 1616,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1617,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20736537870931,
                     25.694000160379176
                 ]
             },
             "properties": {
                 "OBJECTID": 1617,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1618,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20735558059562,
                     25.693984840428072
                 ]
             },
             "properties": {
                 "OBJECTID": 1618,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1619,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20731892602765,
                     25.693765311420009
                 ]
             },
             "properties": {
                 "OBJECTID": 1619,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1620,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20731923089784,
                     25.693756459393114
                 ]
             },
             "properties": {
                 "OBJECTID": 1620,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1621,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20732677980709,
                     25.693721791427606
                 ]
             },
             "properties": {
                 "OBJECTID": 1621,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1622,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20739378739341,
                     25.693642149265827
                 ]
             },
             "properties": {
                 "OBJECTID": 1622,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1623,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20738723313434,
                     25.693523841651995
                 ]
             },
             "properties": {
                 "OBJECTID": 1623,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1624,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20740945808006,
                     25.693475018357447
                 ]
             },
             "properties": {
                 "OBJECTID": 1624,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1625,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20745082959218,
                     25.69338578222812
                 ]
             },
             "properties": {
                 "OBJECTID": 1625,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1626,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20743127923021,
                     25.693355207976538
                 ]
             },
             "properties": {
                 "OBJECTID": 1626,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1627,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20742782133692,
                     25.693346923421814
                 ]
             },
             "properties": {
                 "OBJECTID": 1627,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1628,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20737841438239,
                     25.693268274111631
                 ]
             },
             "properties": {
                 "OBJECTID": 1628,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1629,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20732208714469,
                     25.693254682657539
                 ]
             },
             "properties": {
                 "OBJECTID": 1629,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1630,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20729719391045,
                     25.693240705394317
                 ]
             },
             "properties": {
                 "OBJECTID": 1630,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1631,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20714094120154,
                     25.693125727070708
                 ]
             },
             "properties": {
                 "OBJECTID": 1631,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1632,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20707381220672,
                     25.693075965783237
                 ]
             },
             "properties": {
                 "OBJECTID": 1632,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1633,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20705996804315,
                     25.693052584309385
                 ]
             },
             "properties": {
                 "OBJECTID": 1633,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1634,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20705142268508,
                     25.6930272036426
                 ]
             },
             "properties": {
                 "OBJECTID": 1634,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1635,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20705044422266,
                     25.69302231402861
                 ]
             },
             "properties": {
                 "OBJECTID": 1635,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1636,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20704621740907,
                     25.692998760784235
                 ]
             },
             "properties": {
                 "OBJECTID": 1636,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1637,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20704424159851,
                     25.692995930617769
                 ]
             },
             "properties": {
                 "OBJECTID": 1637,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1638,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20693220136008,
                     25.692961286034574
                 ]
             },
             "properties": {
                 "OBJECTID": 1638,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1639,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20691836079379,
                     25.692948784558837
                 ]
             },
             "properties": {
                 "OBJECTID": 1639,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1640,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20688913912255,
                     25.692903087307684
                 ]
             },
             "properties": {
                 "OBJECTID": 1640,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1641,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20683893716728,
                     25.692852796319528
                 ]
             },
             "properties": {
                 "OBJECTID": 1641,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1642,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20681627874831,
                     25.692853042733759
                 ]
             },
             "properties": {
                 "OBJECTID": 1642,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1643,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20680654808376,
                     25.692852188377856
                 ]
             },
             "properties": {
                 "OBJECTID": 1643,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1644,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20676886289277,
                     25.692843095332648
                 ]
             },
             "properties": {
                 "OBJECTID": 1644,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1645,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20645698969838,
                     25.692847214227584
                 ]
             },
             "properties": {
                 "OBJECTID": 1645,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1646,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20644722036297,
                     25.692846937236425
                 ]
             },
             "properties": {
                 "OBJECTID": 1646,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1647,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20641697436395,
                     25.692843250915359
                 ]
             },
             "properties": {
                 "OBJECTID": 1647,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1648,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20635697159702,
                     25.692822140229623
                 ]
             },
             "properties": {
                 "OBJECTID": 1648,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1649,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20632139801415,
                     25.692792042618748
                 ]
             },
             "properties": {
                 "OBJECTID": 1649,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1650,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20629330319343,
                     25.692736088599645
                 ]
             },
             "properties": {
                 "OBJECTID": 1650,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1651,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2062372646381,
                     25.69207169105573
                 ]
             },
             "properties": {
                 "OBJECTID": 1651,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1652,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20624730107215,
                     25.692037545596293
                 ]
             },
             "properties": {
                 "OBJECTID": 1652,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1653,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20628462293706,
                     25.691467663202161
                 ]
             },
             "properties": {
                 "OBJECTID": 1653,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1654,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20617982403968,
                     25.691150428249728
                 ]
             },
             "properties": {
                 "OBJECTID": 1654,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1655,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20608501571098,
                     25.691163257078699
                 ]
             },
             "properties": {
                 "OBJECTID": 1655,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1656,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20605574457699,
                     25.691162429702445
                 ]
             },
             "properties": {
                 "OBJECTID": 1656,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1657,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20598425656823,
                     25.691135714441714
                 ]
             },
             "properties": {
                 "OBJECTID": 1657,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1658,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20593839923777,
                     25.690442747235068
                 ]
             },
             "properties": {
                 "OBJECTID": 1658,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1659,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20603748474218,
                     25.689830677643727
                 ]
             },
             "properties": {
                 "OBJECTID": 1659,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1660,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20607370223962,
                     25.689754717306414
                 ]
             },
             "properties": {
                 "OBJECTID": 1660,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1661,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20616749163651,
                     25.689205023792852
                 ]
             },
             "properties": {
                 "OBJECTID": 1661,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1662,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20616235111169,
                     25.688737989666549
                 ]
             },
             "properties": {
                 "OBJECTID": 1662,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1663,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20637536363216,
                     25.688271501428687
                 ]
             },
             "properties": {
                 "OBJECTID": 1663,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1664,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20648969624256,
                     25.687998346746383
                 ]
             },
             "properties": {
                 "OBJECTID": 1664,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1665,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20649448333381,
                     25.687804085989853
                 ]
             },
             "properties": {
                 "OBJECTID": 1665,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1666,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20639823069388,
                     25.687012302176072
                 ]
             },
             "properties": {
                 "OBJECTID": 1666,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1667,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20637237158775,
                     25.686964970856764
                 ]
             },
             "properties": {
                 "OBJECTID": 1667,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1668,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20633568914081,
                     25.686826878157945
                 ]
             },
             "properties": {
                 "OBJECTID": 1668,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1669,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20621192534014,
                     25.686825997721655
                 ]
             },
             "properties": {
                 "OBJECTID": 1669,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1670,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20613745068289,
                     25.68681658002123
                 ]
             },
             "properties": {
                 "OBJECTID": 1670,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1671,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20611444242763,
                     25.686811338772316
                 ]
             },
             "properties": {
                 "OBJECTID": 1671,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1672,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20552857818137,
                     25.686680476622769
                 ]
             },
             "properties": {
                 "OBJECTID": 1672,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1673,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20560674185771,
                     25.686158786199087
                 ]
             },
             "properties": {
                 "OBJECTID": 1673,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1674,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20569403635074,
                     25.686046280111782
                 ]
             },
             "properties": {
                 "OBJECTID": 1674,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1675,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20603448100655,
                     25.685952119294939
                 ]
             },
             "properties": {
                 "OBJECTID": 1675,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1676,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2060442116711,
                     25.685944582976163
                 ]
             },
             "properties": {
                 "OBJECTID": 1676,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1677,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20614734232606,
                     25.685686541400798
                 ]
             },
             "properties": {
                 "OBJECTID": 1677,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1678,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.206190874909,
                     25.685626381252519
                 ]
             },
             "properties": {
                 "OBJECTID": 1678,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1679,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20632298891485,
                     25.685343058835144
                 ]
             },
             "properties": {
                 "OBJECTID": 1679,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1680,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20646388480048,
                     25.684802128315596
                 ]
             },
             "properties": {
                 "OBJECTID": 1680,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1681,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20654020576592,
                     25.684541593819574
                 ]
             },
             "properties": {
                 "OBJECTID": 1681,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1682,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20658316368213,
                     25.684494016086035
                 ]
             },
             "properties": {
                 "OBJECTID": 1682,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1683,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20664491742906,
                     25.684468560775485
                 ]
             },
             "properties": {
                 "OBJECTID": 1683,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1684,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20671935071755,
                     25.684372920573878
                 ]
             },
             "properties": {
                 "OBJECTID": 1684,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1685,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20701204856726,
                     25.683834457794035
                 ]
             },
             "properties": {
                 "OBJECTID": 1685,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1686,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20702584506677,
                     25.683821920345395
                 ]
             },
             "properties": {
                 "OBJECTID": 1686,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1687,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20707627634914,
                     25.683795441606378
                 ]
             },
             "properties": {
                 "OBJECTID": 1687,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1688,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20717230865517,
                     25.683795310305356
                 ]
             },
             "properties": {
                 "OBJECTID": 1688,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1689,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20723731165265,
                     25.683609367378438
                 ]
             },
             "properties": {
                 "OBJECTID": 1689,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1690,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20735823359564,
                     25.683417848654869
                 ]
             },
             "properties": {
                 "OBJECTID": 1690,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1691,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20738210520005,
                     25.683389499325983
                 ]
             },
             "properties": {
                 "OBJECTID": 1691,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1692,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20742161961209,
                     25.683363758031078
                 ]
             },
             "properties": {
                 "OBJECTID": 1692,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1693,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20751681195139,
                     25.683352267393275
                 ]
             },
             "properties": {
                 "OBJECTID": 1693,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1694,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20764927219619,
                     25.683389428279554
                 ]
             },
             "properties": {
                 "OBJECTID": 1694,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1695,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20775862166312,
                     25.683136201674415
                 ]
             },
             "properties": {
                 "OBJECTID": 1695,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1696,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20790349435083,
                     25.682871241614578
                 ]
             },
             "properties": {
                 "OBJECTID": 1696,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1697,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20785989521806,
                     25.68285928782592
                 ]
             },
             "properties": {
                 "OBJECTID": 1697,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1698,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20784235843814,
                     25.682851481710543
                 ]
             },
             "properties": {
                 "OBJECTID": 1698,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1699,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20781138398826,
                     25.682830013094701
                 ]
             },
             "properties": {
                 "OBJECTID": 1699,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1700,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20778759781945,
                     25.68280201180346
                 ]
             },
             "properties": {
                 "OBJECTID": 1700,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1701,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20777038479548,
                     25.682760765297132
                 ]
             },
             "properties": {
                 "OBJECTID": 1701,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1702,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20776747099205,
                     25.682734362101201
                 ]
             },
             "properties": {
                 "OBJECTID": 1702,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1703,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20776777586224,
                     25.682725509174986
                 ]
             },
             "properties": {
                 "OBJECTID": 1703,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1704,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20783580238043,
                     25.682452056816999
                 ]
             },
             "properties": {
                 "OBJECTID": 1704,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1705,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20785741668647,
                     25.68242264628816
                 ]
             },
             "properties": {
                 "OBJECTID": 1705,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1706,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2078712122867,
                     25.68241010704088
                 ]
             },
             "properties": {
                 "OBJECTID": 1706,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1707,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20793862906453,
                     25.68233747689311
                 ]
             },
             "properties": {
                 "OBJECTID": 1707,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1708,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2079710298392,
                     25.682156160978082
                 ]
             },
             "properties": {
                 "OBJECTID": 1708,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1709,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20797562537484,
                     25.682148342272228
                 ]
             },
             "properties": {
                 "OBJECTID": 1709,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1710,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20801398775541,
                     25.682108581445902
                 ]
             },
             "properties": {
                 "OBJECTID": 1710,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1711,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20814006820962,
                     25.68207780304823
                 ]
             },
             "properties": {
                 "OBJECTID": 1711,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1712,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2081825485858,
                     25.681804660956402
                 ]
             },
             "properties": {
                 "OBJECTID": 1712,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1713,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20820297938405,
                     25.681465389016523
                 ]
             },
             "properties": {
                 "OBJECTID": 1713,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1714,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20832988541594,
                     25.681200171750561
                 ]
             },
             "properties": {
                 "OBJECTID": 1714,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1715,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20834375476051,
                     25.681191997812505
                 ]
             },
             "properties": {
                 "OBJECTID": 1715,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1716,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20824738790668,
                     25.680826424300108
                 ]
             },
             "properties": {
                 "OBJECTID": 1716,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1717,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20821308236793,
                     25.680763630936895
                 ]
             },
             "properties": {
                 "OBJECTID": 1717,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1718,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20836932878154,
                     25.680370576840687
                 ]
             },
             "properties": {
                 "OBJECTID": 1718,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1719,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20835348812307,
                     25.680302094366198
                 ]
             },
             "properties": {
                 "OBJECTID": 1719,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1720,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2083471793789,
                     25.680285337298528
                 ]
             },
             "properties": {
                 "OBJECTID": 1720,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1721,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20841860083783,
                     25.679969030446443
                 ]
             },
             "properties": {
                 "OBJECTID": 1721,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1722,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20844082128696,
                     25.679920206252575
                 ]
             },
             "properties": {
                 "OBJECTID": 1722,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1723,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20850777491393,
                     25.679870257906146
                 ]
             },
             "properties": {
                 "OBJECTID": 1723,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1724,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.208584418736,
                     25.679861006580268
                 ]
             },
             "properties": {
                 "OBJECTID": 1724,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1725,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2087167665656,
                     25.679904258574709
                 ]
             },
             "properties": {
                 "OBJECTID": 1725,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1726,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20872629128536,
                     25.679904363795401
                 ]
             },
             "properties": {
                 "OBJECTID": 1726,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1727,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2087330595831,
                     25.679900546173315
                 ]
             },
             "properties": {
                 "OBJECTID": 1727,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1728,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20902016814631,
                     25.679444972107831
                 ]
             },
             "properties": {
                 "OBJECTID": 1728,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1729,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20935344700405,
                     25.678843330155587
                 ]
             },
             "properties": {
                 "OBJECTID": 1729,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1730,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20914673243584,
                     25.678260796098925
                 ]
             },
             "properties": {
                 "OBJECTID": 1730,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1731,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2090377328052,
                     25.678179991113723
                 ]
             },
             "properties": {
                 "OBJECTID": 1731,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1732,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20902758215726,
                     25.678145871734671
                 ]
             },
             "properties": {
                 "OBJECTID": 1732,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1733,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20902387515179,
                     25.678102126911654
                 ]
             },
             "properties": {
                 "OBJECTID": 1733,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1734,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20887000204903,
                     25.677918042883448
                 ]
             },
             "properties": {
                 "OBJECTID": 1734,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1735,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2088697313531,
                     25.677918084252212
                 ]
             },
             "properties": {
                 "OBJECTID": 1735,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1736,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20881156680042,
                     25.677914710895266
                 ]
             },
             "properties": {
                 "OBJECTID": 1736,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1737,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20870354833016,
                     25.677810522637571
                 ]
             },
             "properties": {
                 "OBJECTID": 1737,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1738,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20868987413843,
                     25.67741075690094
                 ]
             },
             "properties": {
                 "OBJECTID": 1738,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1739,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2085985749643,
                     25.677153199160841
                 ]
             },
             "properties": {
                 "OBJECTID": 1739,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1740,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20858473889467,
                     25.677140697685104
                 ]
             },
             "properties": {
                 "OBJECTID": 1740,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1741,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20855043425519,
                     25.67707790612053
                 ]
             },
             "properties": {
                 "OBJECTID": 1741,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1742,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20850126382231,
                     25.676628588638494
                 ]
             },
             "properties": {
                 "OBJECTID": 1742,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1743,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20851585891984,
                     25.676555755243953
                 ]
             },
             "properties": {
                 "OBJECTID": 1743,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1744,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20856675065505,
                     25.676503006408666
                 ]
             },
             "properties": {
                 "OBJECTID": 1744,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1745,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20858365611087,
                     25.676494128301442
                 ]
             },
             "properties": {
                 "OBJECTID": 1745,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1746,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20863938709812,
                     25.676374548146953
                 ]
             },
             "properties": {
                 "OBJECTID": 1746,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1747,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20890780505124,
                     25.675707073122965
                 ]
             },
             "properties": {
                 "OBJECTID": 1747,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1748,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20895151569994,
                     25.675634422290784
                 ]
             },
             "properties": {
                 "OBJECTID": 1748,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1749,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20894581130023,
                     25.675627228613735
                 ]
             },
             "properties": {
                 "OBJECTID": 1749,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1750,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20893601588449,
                     25.675611909562008
                 ]
             },
             "properties": {
                 "OBJECTID": 1750,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1751,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20892181738805,
                     25.675569729559413
                 ]
             },
             "properties": {
                 "OBJECTID": 1751,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1752,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20892553968196,
                     25.675517023891587
                 ]
             },
             "properties": {
                 "OBJECTID": 1752,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1753,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20891555271066,
                     25.675381119243298
                 ]
             },
             "properties": {
                 "OBJECTID": 1753,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1754,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20888175438949,
                     25.675363497927151
                 ]
             },
             "properties": {
                 "OBJECTID": 1754,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1755,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20884325980865,
                     25.675323844120157
                 ]
             },
             "properties": {
                 "OBJECTID": 1755,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1756,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20883459124343,
                     25.675307975582655
                 ]
             },
             "properties": {
                 "OBJECTID": 1756,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1757,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20882346932768,
                     25.675265044646153
                 ]
             },
             "properties": {
                 "OBJECTID": 1757,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1758,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20882595865112,
                     25.675229782228712
                 ]
             },
             "properties": {
                 "OBJECTID": 1758,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1759,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20887578918638,
                     25.674905951648384
                 ]
             },
             "properties": {
                 "OBJECTID": 1759,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1760,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20887508411789,
                     25.674893020296679
                 ]
             },
             "properties": {
                 "OBJECTID": 1760,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1761,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20889038608249,
                     25.674833131743696
                 ]
             },
             "properties": {
                 "OBJECTID": 1761,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1762,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20913983103844,
                     25.67438989907464
                 ]
             },
             "properties": {
                 "OBJECTID": 1762,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1763,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20915673559495,
                     25.674381021866736
                 ]
             },
             "properties": {
                 "OBJECTID": 1763,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1764,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20916563258794,
                     25.674377355330762
                 ]
             },
             "properties": {
                 "OBJECTID": 1764,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1765,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20950664021933,
                     25.673586228022032
                 ]
             },
             "properties": {
                 "OBJECTID": 1765,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1766,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20952632458028,
                     25.673483586598309
                 ]
             },
             "properties": {
                 "OBJECTID": 1766,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1767,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20969392403543,
                     25.672300791948032
                 ]
             },
             "properties": {
                 "OBJECTID": 1767,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1768,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20970128678499,
                     25.67023185211832
                 ]
             },
             "properties": {
                 "OBJECTID": 1768,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1769,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20969141312827,
                     25.670106356223414
                 ]
             },
             "properties": {
                 "OBJECTID": 1769,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1770,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2096829532058,
                     25.670073250380256
                 ]
             },
             "properties": {
                 "OBJECTID": 1770,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1771,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20969017026522,
                     25.669998730756845
                 ]
             },
             "properties": {
                 "OBJECTID": 1771,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1772,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2097130427228,
                     25.669959808998044
                 ]
             },
             "properties": {
                 "OBJECTID": 1772,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1773,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20971923275647,
                     25.669952952566746
                 ]
             },
             "properties": {
                 "OBJECTID": 1773,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1774,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21015713784169,
                     25.669888711295016
                 ]
             },
             "properties": {
                 "OBJECTID": 1774,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1775,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21014918513686,
                     25.669883562676318
                 ]
             },
             "properties": {
                 "OBJECTID": 1775,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1776,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21014162093911,
                     25.669877955403365
                 ]
             },
             "properties": {
                 "OBJECTID": 1776,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1777,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21013447582544,
                     25.669871910160566
                 ]
             },
             "properties": {
                 "OBJECTID": 1777,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1778,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21010607163794,
                     25.669836104552644
                 ]
             },
             "properties": {
                 "OBJECTID": 1778,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1779,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21006730995845,
                     25.669685150649229
                 ]
             },
             "properties": {
                 "OBJECTID": 1779,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1780,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2100710313531,
                     25.669632444082083
                 ]
             },
             "properties": {
                 "OBJECTID": 1780,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1781,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21002591236601,
                     25.669627197437251
                 ]
             },
             "properties": {
                 "OBJECTID": 1781,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1782,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21000653737184,
                     25.669624913159282
                 ]
             },
             "properties": {
                 "OBJECTID": 1782,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1783,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2099695959202,
                     25.669613605983159
                 ]
             },
             "properties": {
                 "OBJECTID": 1783,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1784,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20992999787114,
                     25.669587976204184
                 ]
             },
             "properties": {
                 "OBJECTID": 1784,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1785,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20965042832466,
                     25.669161576145882
                 ]
             },
             "properties": {
                 "OBJECTID": 1785,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1786,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20962046920937,
                     25.669167249069346
                 ]
             },
             "properties": {
                 "OBJECTID": 1786,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1787,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20951795459001,
                     25.669145739084627
                 ]
             },
             "properties": {
                 "OBJECTID": 1787,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1788,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20951000188512,
                     25.66914058866729
                 ]
             },
             "properties": {
                 "OBJECTID": 1788,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1789,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20948238910103,
                     25.669115642373072
                 ]
             },
             "properties": {
                 "OBJECTID": 1789,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1790,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20945938444311,
                     25.669076782667446
                 ]
             },
             "properties": {
                 "OBJECTID": 1790,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1791,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2094542987769,
                     25.669059688353968
                 ]
             },
             "properties": {
                 "OBJECTID": 1791,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1792,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20944510320902,
                     25.66901183183063
                 ]
             },
             "properties": {
                 "OBJECTID": 1792,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1793,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20949351101677,
                     25.668711517622512
                 ]
             },
             "properties": {
                 "OBJECTID": 1793,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1794,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20950392336744,
                     25.668673061712468
                 ]
             },
             "properties": {
                 "OBJECTID": 1794,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1795,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20959649058562,
                     25.667971443927058
                 ]
             },
             "properties": {
                 "OBJECTID": 1795,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1796,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20962261409244,
                     25.667895087888041
                 ]
             },
             "properties": {
                 "OBJECTID": 1796,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1797,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20964630223511,
                     25.667867020946289
                 ]
             },
             "properties": {
                 "OBJECTID": 1797,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1798,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20965342576505,
                     25.667860955918457
                 ]
             },
             "properties": {
                 "OBJECTID": 1798,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1799,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2096681098954,
                     25.667831360129242
                 ]
             },
             "properties": {
                 "OBJECTID": 1799,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1800,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20966911263952,
                     25.667802678051203
                 ]
             },
             "properties": {
                 "OBJECTID": 1800,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1801,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20967163433852,
                     25.667785118788231
                 ]
             },
             "properties": {
                 "OBJECTID": 1801,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1802,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20967383767754,
                     25.667776489793198
                 ]
             },
             "properties": {
                 "OBJECTID": 1802,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1803,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20970572224132,
                     25.667722230996219
                 ]
             },
             "properties": {
                 "OBJECTID": 1803,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1804,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20974891488055,
                     25.667551618812183
                 ]
             },
             "properties": {
                 "OBJECTID": 1804,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1805,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20959458042563,
                     25.66728885759477
                 ]
             },
             "properties": {
                 "OBJECTID": 1805,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1806,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20951099024006,
                     25.667140927212074
                 ]
             },
             "properties": {
                 "OBJECTID": 1806,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1807,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20963010004914,
                     25.666846813829466
                 ]
             },
             "properties": {
                 "OBJECTID": 1807,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1808,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20964813685208,
                     25.666840013156161
                 ]
             },
             "properties": {
                 "OBJECTID": 1808,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1809,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20966700103133,
                     25.666835404130666
                 ]
             },
             "properties": {
                 "OBJECTID": 1809,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1810,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2099875859584,
                     25.666789474854454
                 ]
             },
             "properties": {
                 "OBJECTID": 1810,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1811,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21000211000944,
                     25.666742723597849
                 ]
             },
             "properties": {
                 "OBJECTID": 1811,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1812,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20998722263226,
                     25.666735443585878
                 ]
             },
             "properties": {
                 "OBJECTID": 1812,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1813,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20995476879762,
                     25.666715858150383
                 ]
             },
             "properties": {
                 "OBJECTID": 1813,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1814,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20994093272793,
                     25.666703356674645
                 ]
             },
             "properties": {
                 "OBJECTID": 1814,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1815,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20991171735193,
                     25.666657660322812
                 ]
             },
             "properties": {
                 "OBJECTID": 1815,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1816,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20990663078646,
                     25.666640564210695
                 ]
             },
             "properties": {
                 "OBJECTID": 1816,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1817,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21009103857062,
                     25.666390835069024
                 ]
             },
             "properties": {
                 "OBJECTID": 1817,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1818,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21017607216805,
                     25.666408323285566
                 ]
             },
             "properties": {
                 "OBJECTID": 1818,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1819,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21019010069267,
                     25.666351730747806
                 ]
             },
             "properties": {
                 "OBJECTID": 1819,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1820,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21019290028221,
                     25.66634484373958
                 ]
             },
             "properties": {
                 "OBJECTID": 1820,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1821,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21030306003826,
                     25.666134313347925
                 ]
             },
             "properties": {
                 "OBJECTID": 1821,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1822,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21034434341675,
                     25.666110968746239
                 ]
             },
             "properties": {
                 "OBJECTID": 1822,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1823,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21041921287645,
                     25.665943908884344
                 ]
             },
             "properties": {
                 "OBJECTID": 1823,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1824,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21043125479861,
                     25.66591972341655
                 ]
             },
             "properties": {
                 "OBJECTID": 1824,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1825,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21064050905233,
                     25.665355099358408
                 ]
             },
             "properties": {
                 "OBJECTID": 1825,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1826,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21062424930972,
                     25.665345285056901
                 ]
             },
             "properties": {
                 "OBJECTID": 1826,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1827,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21056687795914,
                     25.665220310768802
                 ]
             },
             "properties": {
                 "OBJECTID": 1827,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1828,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21093500464679,
                     25.664940486714158
                 ]
             },
             "properties": {
                 "OBJECTID": 1828,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1829,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2109338346288,
                     25.66491498194091
                 ]
             },
             "properties": {
                 "OBJECTID": 1829,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1830,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.210933715019,
                     25.664844915760284
                 ]
             },
             "properties": {
                 "OBJECTID": 1830,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1831,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21104178025399,
                     25.664564336275134
                 ]
             },
             "properties": {
                 "OBJECTID": 1831,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1832,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21116018229662,
                     25.664511212422553
                 ]
             },
             "properties": {
                 "OBJECTID": 1832,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1833,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21119085097706,
                     25.664495063296613
                 ]
             },
             "properties": {
                 "OBJECTID": 1833,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1834,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21124968912193,
                     25.663881902827598
                 ]
             },
             "properties": {
                 "OBJECTID": 1834,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1835,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.211249842906,
                     25.663881488240122
                 ]
             },
             "properties": {
                 "OBJECTID": 1835,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1836,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21134862893615,
                     25.663437743856832
                 ]
             },
             "properties": {
                 "OBJECTID": 1836,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1837,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2111801328569,
                     25.663400154893282
                 ]
             },
             "properties": {
                 "OBJECTID": 1837,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1838,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21112192873409,
                     25.663354037658735
                 ]
             },
             "properties": {
                 "OBJECTID": 1838,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1839,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21109954370905,
                     25.663305276417418
                 ]
             },
             "properties": {
                 "OBJECTID": 1839,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1840,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21108458258749,
                     25.663003099713308
                 ]
             },
             "properties": {
                 "OBJECTID": 1840,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1841,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21135574886875,
                     25.662601330287202
                 ]
             },
             "properties": {
                 "OBJECTID": 1841,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1842,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21137782272842,
                     25.662557819287997
                 ]
             },
             "properties": {
                 "OBJECTID": 1842,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1843,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21138539861732,
                     25.66253438655275
                 ]
             },
             "properties": {
                 "OBJECTID": 1843,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1844,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21130985376681,
                     25.662246473096332
                 ]
             },
             "properties": {
                 "OBJECTID": 1844,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1845,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2112909760977,
                     25.66216115171585
                 ]
             },
             "properties": {
                 "OBJECTID": 1845,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1846,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21130962264101,
                     25.662111114336483
                 ]
             },
             "properties": {
                 "OBJECTID": 1846,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1847,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21151196110816,
                     25.661770659788147
                 ]
             },
             "properties": {
                 "OBJECTID": 1847,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1848,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2116327769312,
                     25.661556434981549
                 ]
             },
             "properties": {
                 "OBJECTID": 1848,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1849,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21184237832318,
                     25.661189130274181
                 ]
             },
             "properties": {
                 "OBJECTID": 1849,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1850,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21180985883797,
                     25.660201353609636
                 ]
             },
             "properties": {
                 "OBJECTID": 1850,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1851,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.211884514259,
                     25.659884839913502
                 ]
             },
             "properties": {
                 "OBJECTID": 1851,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1852,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21190637497926,
                     25.659770072930598
                 ]
             },
             "properties": {
                 "OBJECTID": 1852,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1853,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21198392082135,
                     25.65954171617733
                 ]
             },
             "properties": {
                 "OBJECTID": 1853,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1854,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21208413497493,
                     25.659326583954737
                 ]
             },
             "properties": {
                 "OBJECTID": 1854,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1855,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21208834739934,
                     25.659319371391916
                 ]
             },
             "properties": {
                 "OBJECTID": 1855,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1856,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21242790892092,
                     25.658621268157049
                 ]
             },
             "properties": {
                 "OBJECTID": 1856,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1857,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21242603383445,
                     25.657501281259158
                 ]
             },
             "properties": {
                 "OBJECTID": 1857,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1858,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21245057183648,
                     25.657285844166438
                 ]
             },
             "properties": {
                 "OBJECTID": 1858,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1859,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21248401672403,
                     25.656766778863812
                 ]
             },
             "properties": {
                 "OBJECTID": 1859,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1860,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21248036637587,
                     25.656583962879608
                 ]
             },
             "properties": {
                 "OBJECTID": 1860,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1861,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2128980996676,
                     25.654584108969516
                 ]
             },
             "properties": {
                 "OBJECTID": 1861,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1862,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21304573417336,
                     25.65382079139647
                 ]
             },
             "properties": {
                 "OBJECTID": 1862,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1863,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21290564138229,
                     25.65357178441036
                 ]
             },
             "properties": {
                 "OBJECTID": 1863,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1864,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21275871194507,
                     25.653519702872018
                 ]
             },
             "properties": {
                 "OBJECTID": 1864,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1865,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21274177861028,
                     25.653510875126813
                 ]
             },
             "properties": {
                 "OBJECTID": 1865,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1866,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21263070604232,
                     25.653386033938375
                 ]
             },
             "properties": {
                 "OBJECTID": 1866,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1867,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21262608622499,
                     25.653378227822998
                 ]
             },
             "properties": {
                 "OBJECTID": 1867,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1868,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21235524819622,
                     25.652955193027765
                 ]
             },
             "properties": {
                 "OBJECTID": 1868,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1869,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.212236177058,
                     25.652559066847516
                 ]
             },
             "properties": {
                 "OBJECTID": 1869,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1870,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21223608622648,
                     25.652506254160357
                 ]
             },
             "properties": {
                 "OBJECTID": 1870,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1871,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21227030273235,
                     25.652355198633586
                 ]
             },
             "properties": {
                 "OBJECTID": 1871,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1872,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21229317069333,
                     25.652316275975409
                 ]
             },
             "properties": {
                 "OBJECTID": 1872,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1873,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2123286300623,
                     25.652286081237719
                 ]
             },
             "properties": {
                 "OBJECTID": 1873,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1874,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21241427340004,
                     25.651913474127582
                 ]
             },
             "properties": {
                 "OBJECTID": 1874,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1875,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21240406339689,
                     25.651844021284603
                 ]
             },
             "properties": {
                 "OBJECTID": 1875,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1876,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21212813520532,
                     25.651319348709023
                 ]
             },
             "properties": {
                 "OBJECTID": 1876,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1877,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21207833524704,
                     25.651199982593198
                 ]
             },
             "properties": {
                 "OBJECTID": 1877,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1878,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21207890991383,
                     25.651196360123947
                 ]
             },
             "properties": {
                 "OBJECTID": 1878,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1879,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21211649168282,
                     25.650980964400048
                 ]
             },
             "properties": {
                 "OBJECTID": 1879,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1880,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21212494710869,
                     25.650955560350894
                 ]
             },
             "properties": {
                 "OBJECTID": 1880,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1881,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21213870763529,
                     25.65093213930686
                 ]
             },
             "properties": {
                 "OBJECTID": 1881,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1882,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21245789501592,
                     25.650703717802401
                 ]
             },
             "properties": {
                 "OBJECTID": 1882,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1883,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2125900755716,
                     25.65073181711972
                 ]
             },
             "properties": {
                 "OBJECTID": 1883,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1884,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21321555315529,
                     25.64968166527666
                 ]
             },
             "properties": {
                 "OBJECTID": 1884,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1885,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21309206634584,
                     25.649305326879357
                 ]
             },
             "properties": {
                 "OBJECTID": 1885,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1886,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21306718120553,
                     25.649291349616135
                 ]
             },
             "properties": {
                 "OBJECTID": 1886,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1887,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21305961700779,
                     25.649285741443862
                 ]
             },
             "properties": {
                 "OBJECTID": 1887,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1888,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21301359779937,
                     25.649149004922663
                 ]
             },
             "properties": {
                 "OBJECTID": 1888,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1889,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21311674194413,
                     25.648854330363122
                 ]
             },
             "properties": {
                 "OBJECTID": 1889,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1890,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21314935226087,
                     25.648678143281757
                 ]
             },
             "properties": {
                 "OBJECTID": 1890,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1891,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21330201217819,
                     25.64845445131192
                 ]
             },
             "properties": {
                 "OBJECTID": 1891,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1892,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21335946266913,
                     25.648408966300792
                 ]
             },
             "properties": {
                 "OBJECTID": 1892,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1893,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21341572155836,
                     25.64839521566671
                 ]
             },
             "properties": {
                 "OBJECTID": 1893,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1894,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21344498190047,
                     25.648396043942341
                 ]
             },
             "properties": {
                 "OBJECTID": 1894,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1895,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21358946158443,
                     25.648411692145942
                 ]
             },
             "properties": {
                 "OBJECTID": 1895,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1896,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21376840688652,
                     25.647318669620972
                 ]
             },
             "properties": {
                 "OBJECTID": 1896,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1897,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21377299702624,
                     25.647007425951983
                 ]
             },
             "properties": {
                 "OBJECTID": 1897,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1898,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21378920730615,
                     25.646785949911703
                 ]
             },
             "properties": {
                 "OBJECTID": 1898,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1899,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21380523142636,
                     25.646319512035916
                 ]
             },
             "properties": {
                 "OBJECTID": 1899,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1900,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21402714903377,
                     25.645079331645888
                 ]
             },
             "properties": {
                 "OBJECTID": 1900,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1901,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21394156595056,
                     25.644913759262636
                 ]
             },
             "properties": {
                 "OBJECTID": 1901,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1902,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21441039692235,
                     25.64356283815448
                 ]
             },
             "properties": {
                 "OBJECTID": 1902,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1903,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21440634997316,
                     25.643554774833035
                 ]
             },
             "properties": {
                 "OBJECTID": 1903,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1904,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21470120799444,
                     25.642876982786561
                 ]
             },
             "properties": {
                 "OBJECTID": 1904,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1905,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21472686565238,
                     25.642850379941137
                 ]
             },
             "properties": {
                 "OBJECTID": 1905,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1906,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21475063473406,
                     25.642834890018207
                 ]
             },
             "properties": {
                 "OBJECTID": 1906,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1907,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21481550193391,
                     25.642816953039983
                 ]
             },
             "properties": {
                 "OBJECTID": 1907,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1908,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21482526767204,
                     25.642816649069175
                 ]
             },
             "properties": {
                 "OBJECTID": 1908,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1909,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21571530871461,
                     25.641467151587847
                 ]
             },
             "properties": {
                 "OBJECTID": 1909,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1910,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21578462396127,
                     25.6412640918644
                 ]
             },
             "properties": {
                 "OBJECTID": 1910,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1911,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21630742324902,
                     25.640292100999716
                 ]
             },
             "properties": {
                 "OBJECTID": 1911,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1912,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21659398412515,
                     25.63985481824534
                 ]
             },
             "properties": {
                 "OBJECTID": 1912,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1913,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21692003872886,
                     25.638594793415052
                 ]
             },
             "properties": {
                 "OBJECTID": 1913,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1914,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21599067932647,
                     25.637999229980551
                 ]
             },
             "properties": {
                 "OBJECTID": 1914,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1915,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21595464079411,
                     25.637985759035587
                 ]
             },
             "properties": {
                 "OBJECTID": 1915,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1916,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21592019316245,
                     25.637961034873911
                 ]
             },
             "properties": {
                 "OBJECTID": 1916,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1917,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2157816292189,
                     25.637827135713792
                 ]
             },
             "properties": {
                 "OBJECTID": 1917,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1918,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21550451841887,
                     25.637700664054478
                 ]
             },
             "properties": {
                 "OBJECTID": 1918,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1919,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21547602070183,
                     25.63770672998163
                 ]
             },
             "properties": {
                 "OBJECTID": 1919,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1920,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2154467657557,
                     25.637707640095584
                 ]
             },
             "properties": {
                 "OBJECTID": 1920,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1921,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21543703868844,
                     25.637706784840304
                 ]
             },
             "properties": {
                 "OBJECTID": 1921,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1922,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21535801885756,
                     25.637674465004579
                 ]
             },
             "properties": {
                 "OBJECTID": 1922,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1923,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21533797476781,
                     25.637655126882635
                 ]
             },
             "properties": {
                 "OBJECTID": 1923,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1924,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21515805909723,
                     25.637187395781723
                 ]
             },
             "properties": {
                 "OBJECTID": 1924,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1925,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21519862841404,
                     25.637026757079241
                 ]
             },
             "properties": {
                 "OBJECTID": 1925,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1926,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21523327389656,
                     25.636910841662086
                 ]
             },
             "properties": {
                 "OBJECTID": 1926,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1927,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21536726928412,
                     25.636654407175229
                 ]
             },
             "properties": {
                 "OBJECTID": 1927,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1928,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21548255157853,
                     25.636614885568576
                 ]
             },
             "properties": {
                 "OBJECTID": 1928,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1929,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21569257385323,
                     25.636549319595474
                 ]
             },
             "properties": {
                 "OBJECTID": 1929,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1930,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2156739668801,
                     25.636528835737238
                 ]
             },
             "properties": {
                 "OBJECTID": 1930,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1931,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.215653821167,
                     25.63648869629634
                 ]
             },
             "properties": {
                 "OBJECTID": 1931,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1932,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21589618576041,
                     25.636074801311111
                 ]
             },
             "properties": {
                 "OBJECTID": 1932,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1933,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21595132589306,
                     25.636057744769175
                 ]
             },
             "properties": {
                 "OBJECTID": 1933,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1934,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21597081510112,
                     25.636056560362022
                 ]
             },
             "properties": {
                 "OBJECTID": 1934,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1935,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21599030700713,
                     25.636057689011182
                 ]
             },
             "properties": {
                 "OBJECTID": 1935,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1936,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21585353541241,
                     25.635942582084567
                 ]
             },
             "properties": {
                 "OBJECTID": 1936,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1937,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2163905736661,
                     25.635166067859814
                 ]
             },
             "properties": {
                 "OBJECTID": 1937,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1938,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21640009298994,
                     25.635168066153369
                 ]
             },
             "properties": {
                 "OBJECTID": 1938,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1939,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21641859923903,
                     25.635173731882276
                 ]
             },
             "properties": {
                 "OBJECTID": 1939,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1940,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21645787892805,
                     25.635195347986951
                 ]
             },
             "properties": {
                 "OBJECTID": 1940,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1941,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21658630571352,
                     25.63529345682764
                 ]
             },
             "properties": {
                 "OBJECTID": 1941,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1942,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21682288676686,
                     25.635349768776848
                 ]
             },
             "properties": {
                 "OBJECTID": 1942,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1943,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21687150681464,
                     25.635348256117197
                 ]
             },
             "properties": {
                 "OBJECTID": 1943,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1944,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21690917401924,
                     25.635357348263085
                 ]
             },
             "properties": {
                 "OBJECTID": 1944,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1945,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2171260851008,
                     25.6356264200233
                 ]
             },
             "properties": {
                 "OBJECTID": 1945,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1946,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21736076139007,
                     25.635462700243465
                 ]
             },
             "properties": {
                 "OBJECTID": 1946,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1947,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21737562088828,
                     25.635430033269472
                 ]
             },
             "properties": {
                 "OBJECTID": 1947,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1948,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21792061274613,
                     25.6350006816362
                 ]
             },
             "properties": {
                 "OBJECTID": 1948,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1949,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21793947242878,
                     25.634996070812065
                 ]
             },
             "properties": {
                 "OBJECTID": 1949,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1950,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21802639909907,
                     25.634822944123641
                 ]
             },
             "properties": {
                 "OBJECTID": 1950,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1951,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21808145829272,
                     25.634787057576716
                 ]
             },
             "properties": {
                 "OBJECTID": 1951,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1952,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21809994295808,
                     25.634781339687152
                 ]
             },
             "properties": {
                 "OBJECTID": 1952,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1953,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2180854108131,
                     25.634689675388245
                 ]
             },
             "properties": {
                 "OBJECTID": 1953,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1954,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21808752242129,
                     25.634628230108774
                 ]
             },
             "properties": {
                 "OBJECTID": 1954,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1955,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21810238102012,
                     25.634595563134781
                 ]
             },
             "properties": {
                 "OBJECTID": 1955,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1956,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21828888692266,
                     25.634336983764854
                 ]
             },
             "properties": {
                 "OBJECTID": 1956,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1957,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21835231610658,
                     25.63431523815774
                 ]
             },
             "properties": {
                 "OBJECTID": 1957,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1958,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21841045817615,
                     25.634318607917464
                 ]
             },
             "properties": {
                 "OBJECTID": 1958,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1959,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21849555562545,
                     25.634363884285904
                 ]
             },
             "properties": {
                 "OBJECTID": 1959,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1960,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21840965688023,
                     25.634268322325283
                 ]
             },
             "properties": {
                 "OBJECTID": 1960,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1961,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21839256886204,
                     25.634246778166357
                 ]
             },
             "properties": {
                 "OBJECTID": 1961,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1962,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21838390209547,
                     25.634230911427494
                 ]
             },
             "properties": {
                 "OBJECTID": 1962,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1963,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21837759425063,
                     25.634214154359768
                 ]
             },
             "properties": {
                 "OBJECTID": 1963,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1964,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21841554384235,
                     25.634083778743445
                 ]
             },
             "properties": {
                 "OBJECTID": 1964,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1965,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21849985978076,
                     25.633940839598097
                 ]
             },
             "properties": {
                 "OBJECTID": 1965,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1966,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2185098890202,
                     25.633906693239339
                 ]
             },
             "properties": {
                 "OBJECTID": 1966,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1967,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21856477734264,
                     25.633845866693434
                 ]
             },
             "properties": {
                 "OBJECTID": 1967,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1968,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21866040585309,
                     25.633776422843709
                 ]
             },
             "properties": {
                 "OBJECTID": 1968,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1969,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21853562312054,
                     25.63354207210898
                 ]
             },
             "properties": {
                 "OBJECTID": 1969,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1970,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21854960667906,
                     25.633464516374374
                 ]
             },
             "properties": {
                 "OBJECTID": 1970,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1971,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21858500129684,
                     25.63342256390024
                 ]
             },
             "properties": {
                 "OBJECTID": 1971,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1972,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21872585941094,
                     25.633316550018833
                 ]
             },
             "properties": {
                 "OBJECTID": 1972,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1973,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21890019209025,
                     25.63327700862709
                 ]
             },
             "properties": {
                 "OBJECTID": 1973,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1974,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21896034504397,
                     25.63324139907138
                 ]
             },
             "properties": {
                 "OBJECTID": 1974,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1975,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21907792060961,
                     25.633150510887447
                 ]
             },
             "properties": {
                 "OBJECTID": 1975,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1976,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21914227429659,
                     25.633123172396552
                 ]
             },
             "properties": {
                 "OBJECTID": 1976,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1977,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21928294355308,
                     25.633107389294651
                 ]
             },
             "properties": {
                 "OBJECTID": 1977,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1978,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21928913088874,
                     25.633100532863352
                 ]
             },
             "properties": {
                 "OBJECTID": 1978,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1979,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21943947325315,
                     25.632979307848814
                 ]
             },
             "properties": {
                 "OBJECTID": 1979,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1980,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21953359090259,
                     25.632962194649622
                 ]
             },
             "properties": {
                 "OBJECTID": 1980,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1981,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21957091276749,
                     25.632965002333037
                 ]
             },
             "properties": {
                 "OBJECTID": 1981,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1982,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21957418180313,
                     25.632961688331307
                 ]
             },
             "properties": {
                 "OBJECTID": 1982,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1983,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21959418722201,
                     25.632921491333832
                 ]
             },
             "properties": {
                 "OBJECTID": 1983,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1984,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21978134783097,
                     25.632615363907973
                 ]
             },
             "properties": {
                 "OBJECTID": 1984,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1985,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21984911624378,
                     25.632553733368127
                 ]
             },
             "properties": {
                 "OBJECTID": 1985,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1986,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21994622773627,
                     25.63247764083053
                 ]
             },
             "properties": {
                 "OBJECTID": 1986,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1987,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21998069875031,
                     25.632078339144073
                 ]
             },
             "properties": {
                 "OBJECTID": 1987,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1988,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2199831008395,
                     25.632073684253157
                 ]
             },
             "properties": {
                 "OBJECTID": 1988,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1989,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21999725436984,
                     25.63191553667275
                 ]
             },
             "properties": {
                 "OBJECTID": 1989,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1990,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21999372183285,
                     25.631783891213672
                 ]
             },
             "properties": {
                 "OBJECTID": 1990,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1991,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21998460630459,
                     25.631780197697992
                 ]
             },
             "properties": {
                 "OBJECTID": 1991,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1992,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21993618680563,
                     25.631775945703396
                 ]
             },
             "properties": {
                 "OBJECTID": 1992,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1993,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21975662097134,
                     25.631451577328505
                 ]
             },
             "properties": {
                 "OBJECTID": 1993,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1994,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21972595588824,
                     25.631378073039684
                 ]
             },
             "properties": {
                 "OBJECTID": 1994,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1995,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21981309210059,
                     25.631144361722988
                 ]
             },
             "properties": {
                 "OBJECTID": 1995,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1996,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2198668823508,
                     25.631124073017588
                 ]
             },
             "properties": {
                 "OBJECTID": 1996,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1997,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21987652038518,
                     25.631122616115874
                 ]
             },
             "properties": {
                 "OBJECTID": 1997,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1998,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22004539777697,
                     25.631128164033612
                 ]
             },
             "properties": {
                 "OBJECTID": 1998,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1999,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22003522464598,
                     25.630970314128717
                 ]
             },
             "properties": {
                 "OBJECTID": 1999,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2000,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22010568472962,
                     25.630804829879025
                 ]
             },
             "properties": {
                 "OBJECTID": 2000,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2001,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22009456281387,
                     25.630761898942524
                 ]
             },
             "properties": {
                 "OBJECTID": 2001,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2002,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22009925277837,
                     25.63071800573141
                 ]
             },
             "properties": {
                 "OBJECTID": 2002,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2003,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22013112385235,
                     25.630663745135791
                 ]
             },
             "properties": {
                 "OBJECTID": 2003,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2004,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22025173193197,
                     25.630569048322968
                 ]
             },
             "properties": {
                 "OBJECTID": 2004,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2005,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22025722319233,
                     25.630565024756152
                 ]
             },
             "properties": {
                 "OBJECTID": 2005,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2006,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22064193517724,
                     25.630283395762149
                 ]
             },
             "properties": {
                 "OBJECTID": 2006,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2007,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22100772812422,
                     25.630191371734441
                 ]
             },
             "properties": {
                 "OBJECTID": 2007,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2008,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22106485126193,
                     25.630180984564845
                 ]
             },
             "properties": {
                 "OBJECTID": 2008,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2009,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22113953905853,
                     25.63019900967663
                 ]
             },
             "properties": {
                 "OBJECTID": 2009,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2010,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2211868128212,
                     25.630194531952156
                 ]
             },
             "properties": {
                 "OBJECTID": 2010,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2011,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22136643711144,
                     25.630173277374922
                 ]
             },
             "properties": {
                 "OBJECTID": 2011,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2012,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22141029974557,
                     25.630208035272688
                 ]
             },
             "properties": {
                 "OBJECTID": 2012,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2013,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22147039604198,
                     25.630294407061285
                 ]
             },
             "properties": {
                 "OBJECTID": 2013,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2014,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22157756465293,
                     25.630031581991943
                 ]
             },
             "properties": {
                 "OBJECTID": 2014,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2015,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22152230311184,
                     25.629821440107435
                 ]
             },
             "properties": {
                 "OBJECTID": 2015,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2016,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22134029471891,
                     25.629727085037018
                 ]
             },
             "properties": {
                 "OBJECTID": 2016,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2017,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22130962783712,
                     25.629653581647517
                 ]
             },
             "properties": {
                 "OBJECTID": 2017,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2018,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22130929328932,
                     25.629644728721303
                 ]
             },
             "properties": {
                 "OBJECTID": 2018,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2019,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22304836359115,
                     25.626267304092835
                 ]
             },
             "properties": {
                 "OBJECTID": 2019,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2020,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22327980861633,
                     25.625537024014477
                 ]
             },
             "properties": {
                 "OBJECTID": 2020,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2021,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22330807880485,
                     25.625501136568232
                 ]
             },
             "properties": {
                 "OBJECTID": 2021,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2022,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2248923856751,
                     25.625314335688074
                 ]
             },
             "properties": {
                 "OBJECTID": 2022,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2023,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22491512143574,
                     25.625275743980467
                 ]
             },
             "properties": {
                 "OBJECTID": 2023,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2024,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2250807252953,
                     25.624797962056334
                 ]
             },
             "properties": {
                 "OBJECTID": 2024,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2025,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22507451457727,
                     25.624791125410184
                 ]
             },
             "properties": {
                 "OBJECTID": 2025,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2026,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22505901835905,
                     25.624768613580727
                 ]
             },
             "properties": {
                 "OBJECTID": 2026,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2027,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22504384679615,
                     25.624717622020682
                 ]
             },
             "properties": {
                 "OBJECTID": 2027,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2028,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22543331709397,
                     25.620902783336817
                 ]
             },
             "properties": {
                 "OBJECTID": 2028,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2029,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22720518657019,
                     25.618842116370615
                 ]
             },
             "properties": {
                 "OBJECTID": 2029,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2030,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22692537960262,
                     25.618376058008721
                 ]
             },
             "properties": {
                 "OBJECTID": 2030,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2031,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22738897022487,
                     25.617620989916816
                 ]
             },
             "properties": {
                 "OBJECTID": 2031,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2032,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22745084627957,
                     25.617607010854897
                 ]
             },
             "properties": {
                 "OBJECTID": 2032,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2033,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22783064616743,
                     25.616918047429976
                 ]
             },
             "properties": {
                 "OBJECTID": 2033,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2034,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22862265301308,
                     25.616271076049372
                 ]
             },
             "properties": {
                 "OBJECTID": 2034,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2035,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22897599034974,
                     25.613573136890807
                 ]
             },
             "properties": {
                 "OBJECTID": 2035,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2036,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23013565443694,
                     25.612494005294138
                 ]
             },
             "properties": {
                 "OBJECTID": 2036,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2037,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23021064890236,
                     25.611870726552866
                 ]
             },
             "properties": {
                 "OBJECTID": 2037,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2038,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23153113515383,
                     25.60833863202231
                 ]
             },
             "properties": {
                 "OBJECTID": 2038,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2039,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23152867910534,
                     25.606665051251184
                 ]
             },
             "properties": {
                 "OBJECTID": 2039,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         }
    ]

export const generateGreenLine = () => {

    let coordGroup = src.map(o => {
        return {
            coord: `${o.geometry.coordinates[0]},${o.geometry.coordinates[1]}`,
            coord_z: 40
        }
    });
    return {
        id: 'path-tripleline-green',
        coord_type: 0,
        cad_mapkey: '',
        type: 'arrow',
        color: '48ff00',
        width: 10,
        points: coordGroup
    };
}