export default {
    id: "long_kan_1",
    coord_type: 0,                         //坐标类型(0:经纬度坐标, 1:cad坐标)
    cad_mapkey: "",                        //CAD基准点的Key值, 项目中约定
    coord: "100.209429,25.679282",          //POI点的坐标 lng,lat
    coord_z: 1,                            //高度(米)
    always_show_label: true,               //是否永远显示title, true:显示title(默认), false:不显示title
    show_label_range: "0,2000",            //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    sort_order: false,                     //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    state: "state_1",                      //与marker之中images中的define_state对应
    marker: {
        size: "46,73",                   //marker大小(宽,高 单位:像素)
        images: [
            {
                "define_state": "state_1",   //marker图片组
                "normal_url": "http://localhost:3000/images/timemachine_marker.png",        //normal 图片url地址
                "activate_url": "http://localhost:3000/images/timemachine_marker_click.png"       //hover, active 图片url地址
                //本地图片地址: "file:///D:/xxx/markerNormal.png",    D: 云渲染所在盘符
            }
        ]
    },
    label: {
        bg_image_url: "http://localhost:3000/html/images/label_bg.png",
        //本地图片地址: "file:///D:/xxx/LabelBg.png" 
        size: "113,26",                    //label大小(宽,高 单位:像素)
        offset: "11,63",                  //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
        text: "变迁地点",                  //label中的文本内容
        font_color: "fafafaff",            //label中文本颜色(HEXA颜色值)
        text_boxsize: "82,14",            //label中文本框的大小(宽,高 单位:像素)
        text_offset: "25,7"                 //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    }
}