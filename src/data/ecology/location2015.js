 export default[
    {
      id: "商业用地",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.233734,25.603064",
          coord_z: 50,
        },
        {
          coord: "100.201637,25.599413",
          coord_z: 50,
        },
        {
          coord: "100.204262,25.575348",
          coord_z: 50,
        },
        {
          coord: "100.238441,25.577936",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.236458,25.603456",
          coord_z: 50,
        },
        {
          coord: "100.240532,25.578842",
          coord_z: 50,
        },
        {
          coord: "100.290909,25.596857",
          coord_z: 50,
        },
        {
          coord: "100.288994,25.64543",
          coord_z: 50,
        },
        {
          coord: "100.270332,25.614948",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地2",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.202156,25.601004",
          coord_z: 50,
        },
        {
          coord: "100.231316,25.610168",
          coord_z: 50,
        },
        {
          coord: "100.209755,25.655148",
          coord_z: 50,
        },
        {
          coord: "100.20636,25.691399",
          coord_z: 50,
        },
        {
          coord: "100.162888,25.671453",
          coord_z: 50,
        },
        {
          coord: "100.171524,25.669916",
          coord_z: 50,
        },
        {
          coord: "100.175926,25.662575",
          coord_z: 50,
        },
        {
          coord: "100.175644,25.653032",
          coord_z: 50,
        },
        {
          coord: "100.187271,25.648743",
          coord_z: 50,
        },
        {
          coord: "100.19812,25.644558",
          coord_z: 50,
        },
        {
          coord: "100.204079,25.630665",
          coord_z: 50,
        },
        {
          coord: "100.205872,25.615442",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地3",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.155838,25.672094",
          coord_z: 50,
        },
        {
          coord: "100.207108,25.6954",
          coord_z: 50,
        },
        {
          coord: "100.192513,25.720337",
          coord_z: 50,
        },
        {
          coord: "100.187859,25.73118",
          coord_z: 50,
        },
        {
          coord: "100.168694,25.737574",
          coord_z: 50,
        },
        {
          coord: "100.156807,25.74568",
          coord_z: 50,
        },
        {
          coord: "100.14743,25.764875",
          coord_z: 50,
        },
        {
          coord: "100.117386,25.757753",
          coord_z: 50,
        },
        {
          coord: "100.123619,25.738949",
          coord_z: 50,
        },
        {
          coord: "100.126526,25.726667",
          coord_z: 50,
        },
        {
          coord: "100.122749,25.720243",
          coord_z: 50,
        },
        {
          coord: "100.127052,25.711584",
          coord_z: 50,
        },
        {
          coord: "100.138084,25.706823",
          coord_z: 50,
        },
        {
          coord: "100.14669,25.690435",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地4",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.116432,25.759979",
          coord_z: 50,
        },
        {
          coord: "100.147858,25.769415",
          coord_z: 50,
        },
        {
          coord: "100.142639,25.788795",
          coord_z: 50,
        },
        {
          coord: "100.14135,25.812429",
          coord_z: 50,
        },
        {
          coord: "100.152641,25.851912",
          coord_z: 50,
        },
        {
          coord: "100.108795,25.85162",
          coord_z: 50,
        },
        {
          coord: "100.10537,25.813009",
          coord_z: 50,
        },
      ],
    },
    {
      id: "商业用地2",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.109756,25.854364",
          coord_z: 50,
        },
        {
          coord: "100.153908,25.85355",
          coord_z: 50,
        },
        {
          coord: "100.160454,25.863209",
          coord_z: 50,
        },
        {
          coord: "100.14238,25.869806",
          coord_z: 50,
        },
        {
          coord: "100.13073,25.875574",
          coord_z: 50,
        },
        {
          coord: "100.124237,25.8964",
          coord_z: 50,
        },
        {
          coord: "100.113953,25.920628",
          coord_z: 50,
        },
        {
          coord: "100.097946,25.916107",
          coord_z: 50,
        },
        {
          coord: "100.104149,25.910151",
          coord_z: 50,
        },
        {
          coord: "100.100395,25.904932",
          coord_z: 50,
        },
        {
          coord: "100.103897,25.895548",
          coord_z: 50,
        },
        {
          coord: "100.111275,25.893482",
          coord_z: 50,
        },
        {
          coord: "100.10392,25.887785",
          coord_z: 50,
        },
      ],
    },
    {
      id: "商业用地3",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.097038,25.917048",
          coord_z: 50,
        },
        {
          coord: "100.111404,25.920855",
          coord_z: 50,
        },
        {
          coord: "100.097008,25.936319",
          coord_z: 50,
        },
        {
          coord: "100.100365,25.949436",
          coord_z: 50,
        },
        {
          coord: "100.094978,25.959919",
          coord_z: 50,
        },
        {
          coord: "100.087532,25.954866",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地5",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.101974,25.946505",
          coord_z: 50,
        },
        {
          coord: "100.105553,25.935995",
          coord_z: 50,
        },
        {
          coord: "100.120216,25.94305",
          coord_z: 50,
        },
        {
          coord: "100.134186,25.928427",
          coord_z: 50,
        },
        {
          coord: "100.141418,25.945061",
          coord_z: 50,
        },
        {
          coord: "100.147682,25.966791",
          coord_z: 50,
        },
        {
          coord: "100.128914,25.983088",
          coord_z: 50,
        },
        {
          coord: "100.121262,25.985529",
          coord_z: 50,
        },
        {
          coord: "100.113724,25.992863",
          coord_z: 50,
        },
        {
          coord: "100.094223,25.974007",
          coord_z: 50,
        },
      ],
    },
    {
      id: "商业用地4",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.17205,25.934729",
          coord_z: 50,
        },
        {
          coord: "100.18679,25.932762",
          coord_z: 50,
        },
        {
          coord: "100.204536,25.947912",
          coord_z: 50,
        },
        {
          coord: "100.167305,25.985086",
          coord_z: 50,
        },
        {
          coord: "100.144981,25.972387",
          coord_z: 50,
        },
        {
          coord: "100.15834,25.965876",
          coord_z: 50,
        },
        {
          coord: "100.159187,25.954544",
          coord_z: 50,
        },
        {
          coord: "100.167915,25.947741",
          coord_z: 50,
        },
      ],
    },
    {
      id: "商业用地5",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.190948,25.910013",
          coord_z: 50,
        },
        {
          coord: "100.195969,25.909323",
          coord_z: 50,
        },
        {
          coord: "100.195831,25.903921",
          coord_z: 50,
        },
        {
          coord: "100.199677,25.902706",
          coord_z: 50,
        },
        {
          coord: "100.201614,25.905088",
          coord_z: 50,
        },
        {
          coord: "100.191589,25.932068",
          coord_z: 50,
        },
        {
          coord: "100.188377,25.92836",
          coord_z: 50,
        },
        {
          coord: "100.191109,25.918013",
          coord_z: 50,
        },
        {
          coord: "100.19416,25.914957",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地6",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.202354,25.903847",
          coord_z: 50,
        },
        {
          coord: "100.214523,25.898811",
          coord_z: 50,
        },
        {
          coord: "100.220238,25.892538",
          coord_z: 50,
        },
        {
          coord: "100.215401,25.883781",
          coord_z: 50,
        },
        {
          coord: "100.222755,25.882145",
          coord_z: 50,
        },
        {
          coord: "100.22596,25.891476",
          coord_z: 50,
        },
        {
          coord: "100.225426,25.900349",
          coord_z: 50,
        },
        {
          coord: "100.223465,25.904135",
          coord_z: 50,
        },
      ],
    },
    {
      id: "商业用地6",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.217247,25.882437",
          coord_z: 50,
        },
        {
          coord: "100.250015,25.87678",
          coord_z: 50,
        },
        {
          coord: "100.262405,25.814945",
          coord_z: 50,
        },
        {
          coord: "100.222443,25.800335",
          coord_z: 50,
        },
        {
          coord: "100.231339,25.819502",
          coord_z: 50,
        },
        {
          coord: "100.220512,25.833391",
          coord_z: 50,
        },
        {
          coord: "100.210785,25.839973",
          coord_z: 50,
        },
        {
          coord: "100.21875,25.846474",
          coord_z: 50,
        },
        {
          coord: "100.221886,25.853724",
          coord_z: 50,
        },
        {
          coord: "100.216003,25.858374",
          coord_z: 50,
        },
        {
          coord: "100.217728,25.867094",
          coord_z: 50,
        },
        {
          coord: "100.209274,25.877937",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地7",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.223434,25.796511",
          coord_z: 50,
        },
        {
          coord: "100.268654,25.801695",
          coord_z: 50,
        },
        {
          coord: "100.276184,25.715532",
          coord_z: 50,
        },
        {
          coord: "100.26194,25.712305",
          coord_z: 50,
        },
        {
          coord: "100.249237,25.719303",
          coord_z: 50,
        },
        {
          coord: "100.240341,25.710443",
          coord_z: 50,
        },
        {
          coord: "100.235565,25.71324",
          coord_z: 50,
        },
        {
          coord: "100.228546,25.745953",
          coord_z: 50,
        },
        {
          coord: "100.220161,25.770964",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地8",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.290962,25.646263",
          coord_z: 50,
        },
        {
          coord: "100.289658,25.652571",
          coord_z: 50,
        },
        {
          coord: "100.291595,25.66227",
          coord_z: 50,
        },
        {
          coord: "100.278679,25.664347",
          coord_z: 50,
        },
        {
          coord: "100.271767,25.681625",
          coord_z: 50,
        },
        {
          coord: "100.321693,25.694216",
          coord_z: 50,
        },
        {
          coord: "100.333626,25.673935",
          coord_z: 50,
        },
        {
          coord: "100.336655,25.658916",
          coord_z: 50,
        },
        {
          coord: "100.328751,25.63018",
          coord_z: 50,
        },
        {
          coord: "100.33773,25.609287",
          coord_z: 50,
        },
        {
          coord: "100.307976,25.600409",
          coord_z: 50,
        },
        {
          coord: "100.296265,25.612204",
          coord_z: 50,
        },
      ],
    },
    ,
    {
      id: "居住用地9",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.275826,25.683558",
          coord_z: 50,
        },
        {
          coord: "100.320671,25.695765",
          coord_z: 50,
        },
        {
          coord: "100.30751,25.725859",
          coord_z: 50,
        },
        {
          coord: "100.263596,25.710579",
          coord_z: 50,
        },
        {
          coord: "100.258301,25.704685",
          coord_z: 50,
        },
        {
          coord: "100.274185,25.697905",
          coord_z: 50,
        },
      ],
    }]