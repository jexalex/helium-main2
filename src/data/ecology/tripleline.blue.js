const src = [{
            "type": "Feature",
            "id": 1,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.23077202011268,
                    25.602260890427829
                ]
            },
            "properties": {
                "OBJECTID": 1,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 2,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.23079054614686,
                    25.602272694029637
                ]
            },
            "properties": {
                "OBJECTID": 2,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 3,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.23149380069839,
                    25.603158109858953
                ]
            },
            "properties": {
                "OBJECTID": 3,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 4,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.23162811804326,
                    25.60337664421678
                ]
            },
            "properties": {
                "OBJECTID": 4,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 5,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.23166873412487,
                    25.603411939009845
                ]
            },
            "properties": {
                "OBJECTID": 5,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
        {
            "type": "Feature",
            "id": 6,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    100.23318558464706,
                    25.604574376606251
                ]
            },
            "properties": {
                "OBJECTID": 6,
                "Id": 0,
                "ORIG_FID": 0
            }
        },
         {
             "type": "Feature",
             "id": 7,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23565052655061,
                     25.605113606507757
                 ]
             },
             "properties": {
                 "OBJECTID": 7,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 8,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23638287686833,
                     25.605255545607008
                 ]
             },
             "properties": {
                 "OBJECTID": 8,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 9,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23829615034714,
                     25.605918979976991
                 ]
             },
             "properties": {
                 "OBJECTID": 9,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 10,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2398160082023,
                     25.606747137770242
                 ]
             },
             "properties": {
                 "OBJECTID": 10,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 11,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24045319585838,
                     25.606980739369703
                 ]
             },
             "properties": {
                 "OBJECTID": 11,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 12,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24068259222793,
                     25.607176420155724
                 ]
             },
             "properties": {
                 "OBJECTID": 12,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 13,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24097793228583,
                     25.607389706070137
                 ]
             },
             "properties": {
                 "OBJECTID": 13,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 14,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24149301628972,
                     25.607736005312461
                 ]
             },
             "properties": {
                 "OBJECTID": 14,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 15,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2417943800067,
                     25.60786902403629
                 ]
             },
             "properties": {
                 "OBJECTID": 15,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 16,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24247770917901,
                     25.607953241948621
                 ]
             },
             "properties": {
                 "OBJECTID": 16,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 17,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24313890063979,
                     25.608066702216604
                 ]
             },
             "properties": {
                 "OBJECTID": 17,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 18,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24400924203297,
                     25.608361231985327
                 ]
             },
             "properties": {
                 "OBJECTID": 18,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 19,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24413148957666,
                     25.608486849288738
                 ]
             },
             "properties": {
                 "OBJECTID": 19,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 20,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24512110895205,
                     25.608636806743277
                 ]
             },
             "properties": {
                 "OBJECTID": 20,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 21,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24522412539312,
                     25.608716590997915
                 ]
             },
             "properties": {
                 "OBJECTID": 21,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 22,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24536271991366,
                     25.608769788594941
                 ]
             },
             "properties": {
                 "OBJECTID": 22,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 23,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24581194026888,
                     25.60888729671143
                 ]
             },
             "properties": {
                 "OBJECTID": 23,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 24,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24797035544759,
                     25.608938148876518
                 ]
             },
             "properties": {
                 "OBJECTID": 24,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 25,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24898768473395,
                     25.608696429996314
                 ]
             },
             "properties": {
                 "OBJECTID": 25,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 26,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24933181391214,
                     25.608753042319108
                 ]
             },
             "properties": {
                 "OBJECTID": 26,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 27,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24951469374815,
                     25.608772018014292
                 ]
             },
             "properties": {
                 "OBJECTID": 27,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 28,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24986037965283,
                     25.608826643734687
                 ]
             },
             "properties": {
                 "OBJECTID": 28,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 29,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25002803756394,
                     25.608891506437942
                 ]
             },
             "properties": {
                 "OBJECTID": 29,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 30,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25031747357258,
                     25.609037915167676
                 ]
             },
             "properties": {
                 "OBJECTID": 30,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 31,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25059905580184,
                     25.609142190659611
                 ]
             },
             "properties": {
                 "OBJECTID": 31,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 32,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2511298581565,
                     25.609352015083459
                 ]
             },
             "properties": {
                 "OBJECTID": 32,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 33,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25138873879933,
                     25.609456540586905
                 ]
             },
             "properties": {
                 "OBJECTID": 33,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 34,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25169534376516,
                     25.609530141103164
                 ]
             },
             "properties": {
                 "OBJECTID": 34,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 35,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25215519590563,
                     25.609615401329734
                 ]
             },
             "properties": {
                 "OBJECTID": 35,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 36,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25291352224133,
                     25.609708863373385
                 ]
             },
             "properties": {
                 "OBJECTID": 36,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 37,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25339688176075,
                     25.609639206384315
                 ]
             },
             "properties": {
                 "OBJECTID": 37,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 38,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25365966456195,
                     25.60961504879549
                 ]
             },
             "properties": {
                 "OBJECTID": 38,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 39,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25479546603799,
                     25.609847092769144
                 ]
             },
             "properties": {
                 "OBJECTID": 39,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 40,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25563290213677,
                     25.610103877092286
                 ]
             },
             "properties": {
                 "OBJECTID": 40,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 41,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25632591700753,
                     25.610316859035891
                 ]
             },
             "properties": {
                 "OBJECTID": 41,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 42,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25671831909523,
                     25.610409268872729
                 ]
             },
             "properties": {
                 "OBJECTID": 42,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 43,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25697675277496,
                     25.61047017815622
                 ]
             },
             "properties": {
                 "OBJECTID": 43,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 44,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25899604373251,
                     25.610785338372693
                 ]
             },
             "properties": {
                 "OBJECTID": 44,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 45,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25939344784939,
                     25.610829128161811
                 ]
             },
             "properties": {
                 "OBJECTID": 45,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 46,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26066147844455,
                     25.610868770277591
                 ]
             },
             "properties": {
                 "OBJECTID": 46,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 47,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26425635734506,
                     25.612012206098484
                 ]
             },
             "properties": {
                 "OBJECTID": 47,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 48,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26589618476117,
                     25.612557506423855
                 ]
             },
             "properties": {
                 "OBJECTID": 48,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 49,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26607015231576,
                     25.612962515207983
                 ]
             },
             "properties": {
                 "OBJECTID": 49,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 50,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26608701910072,
                     25.613180694333607
                 ]
             },
             "properties": {
                 "OBJECTID": 50,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 51,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26597837560161,
                     25.613721462975207
                 ]
             },
             "properties": {
                 "OBJECTID": 51,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 52,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2657264718992,
                     25.613757884618735
                 ]
             },
             "properties": {
                 "OBJECTID": 52,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 53,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2657150487106,
                     25.613981723178028
                 ]
             },
             "properties": {
                 "OBJECTID": 53,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 54,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26538725122072,
                     25.614689730646603
                 ]
             },
             "properties": {
                 "OBJECTID": 54,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 55,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26543077121318,
                     25.615031693357992
                 ]
             },
             "properties": {
                 "OBJECTID": 55,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 56,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26546104688987,
                     25.615212440901473
                 ]
             },
             "properties": {
                 "OBJECTID": 56,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 57,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26595884772269,
                     25.615296524814823
                 ]
             },
             "properties": {
                 "OBJECTID": 57,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 58,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26599360022453,
                     25.615297445720614
                 ]
             },
             "properties": {
                 "OBJECTID": 58,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 59,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26619806019238,
                     25.615477773280645
                 ]
             },
             "properties": {
                 "OBJECTID": 59,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 60,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26615025043378,
                     25.615531723610218
                 ]
             },
             "properties": {
                 "OBJECTID": 60,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 61,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26591124570746,
                     25.615777860859623
                 ]
             },
             "properties": {
                 "OBJECTID": 61,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 62,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26548590415121,
                     25.61574595561143
                 ]
             },
             "properties": {
                 "OBJECTID": 62,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 63,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26578750618853,
                     25.61595072584538
                 ]
             },
             "properties": {
                 "OBJECTID": 63,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 64,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26572552221512,
                     25.615985712170925
                 ]
             },
             "properties": {
                 "OBJECTID": 64,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 65,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26526554416961,
                     25.616349994256893
                 ]
             },
             "properties": {
                 "OBJECTID": 65,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 66,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2651614809177,
                     25.616573753675823
                 ]
             },
             "properties": {
                 "OBJECTID": 66,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 67,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26511332761805,
                     25.616685973778715
                 ]
             },
             "properties": {
                 "OBJECTID": 67,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 68,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26506144662852,
                     25.616836934876687
                 ]
             },
             "properties": {
                 "OBJECTID": 68,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 69,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26516867279605,
                     25.616880249823737
                 ]
             },
             "properties": {
                 "OBJECTID": 69,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 70,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26517247243174,
                     25.616964991141458
                 ]
             },
             "properties": {
                 "OBJECTID": 70,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 71,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26489389663607,
                     25.617069122741896
                 ]
             },
             "properties": {
                 "OBJECTID": 71,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 72,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26476517757095,
                     25.617023102634107
                 ]
             },
             "properties": {
                 "OBJECTID": 72,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 73,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26469590459237,
                     25.617116933399814
                 ]
             },
             "properties": {
                 "OBJECTID": 73,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 74,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26460735734372,
                     25.617271147345605
                 ]
             },
             "properties": {
                 "OBJECTID": 74,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 75,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26396563260789,
                     25.618267773339142
                 ]
             },
             "properties": {
                 "OBJECTID": 75,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 76,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26363566325534,
                     25.61880214870024
                 ]
             },
             "properties": {
                 "OBJECTID": 76,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 77,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26337612970468,
                     25.619110242044087
                 ]
             },
             "properties": {
                 "OBJECTID": 77,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 78,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26358126416409,
                     25.619206267155562
                 ]
             },
             "properties": {
                 "OBJECTID": 78,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 79,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26370348922472,
                     25.619299879385949
                 ]
             },
             "properties": {
                 "OBJECTID": 79,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 80,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2633569201858,
                     25.619790758137015
                 ]
             },
             "properties": {
                 "OBJECTID": 80,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 81,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26295548530749,
                     25.620355079123669
                 ]
             },
             "properties": {
                 "OBJECTID": 81,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 82,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26265728360681,
                     25.620762567338659
                 ]
             },
             "properties": {
                 "OBJECTID": 82,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 83,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26251764407408,
                     25.620945751145541
                 ]
             },
             "properties": {
                 "OBJECTID": 83,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 84,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26241453230489,
                     25.621023606354413
                 ]
             },
             "properties": {
                 "OBJECTID": 84,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 85,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26210966213125,
                     25.621391730344158
                 ]
             },
             "properties": {
                 "OBJECTID": 85,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 86,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26203219363089,
                     25.62142796672731
                 ]
             },
             "properties": {
                 "OBJECTID": 86,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 87,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26192819962677,
                     25.621465265209849
                 ]
             },
             "properties": {
                 "OBJECTID": 87,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 88,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26179237142088,
                     25.621505313819227
                 ]
             },
             "properties": {
                 "OBJECTID": 88,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 89,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26174879926771,
                     25.621549353619798
                 ]
             },
             "properties": {
                 "OBJECTID": 89,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 90,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26183559373771,
                     25.621627206130711
                 ]
             },
             "properties": {
                 "OBJECTID": 90,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 91,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26191628091169,
                     25.621684420099939
                 ]
             },
             "properties": {
                 "OBJECTID": 91,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 92,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26198558086992,
                     25.621700614192036
                 ]
             },
             "properties": {
                 "OBJECTID": 92,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 93,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26249415108424,
                     25.622212921988819
                 ]
             },
             "properties": {
                 "OBJECTID": 93,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 94,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26261613782458,
                     25.622340473733914
                 ]
             },
             "properties": {
                 "OBJECTID": 94,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 95,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26265481316915,
                     25.622362229233545
                 ]
             },
             "properties": {
                 "OBJECTID": 95,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 96,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26279818578769,
                     25.622329231309038
                 ]
             },
             "properties": {
                 "OBJECTID": 96,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 97,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26289506075852,
                     25.622314379904765
                 ]
             },
             "properties": {
                 "OBJECTID": 97,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 98,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2631044211322,
                     25.622335993311481
                 ]
             },
             "properties": {
                 "OBJECTID": 98,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 99,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26334041762624,
                     25.622315057993603
                 ]
             },
             "properties": {
                 "OBJECTID": 99,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 100,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26386072669123,
                     25.622443181707524
                 ]
             },
             "properties": {
                 "OBJECTID": 100,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 101,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26405172290873,
                     25.622503804107339
                 ]
             },
             "properties": {
                 "OBJECTID": 101,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 102,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26410886493221,
                     25.622512490658949
                 ]
             },
             "properties": {
                 "OBJECTID": 102,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 103,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26422260938597,
                     25.622436383732179
                 ]
             },
             "properties": {
                 "OBJECTID": 103,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 104,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26467978964064,
                     25.622278836898829
                 ]
             },
             "properties": {
                 "OBJECTID": 104,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 105,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26535669315695,
                     25.622052716759526
                 ]
             },
             "properties": {
                 "OBJECTID": 105,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 106,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26583583755394,
                     25.621894550293291
                 ]
             },
             "properties": {
                 "OBJECTID": 106,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 107,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26635541996671,
                     25.621721863373352
                 ]
             },
             "properties": {
                 "OBJECTID": 107,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 108,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26679584765031,
                     25.621575609326953
                 ]
             },
             "properties": {
                 "OBJECTID": 108,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 109,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26715227595685,
                     25.621565536920059
                 ]
             },
             "properties": {
                 "OBJECTID": 109,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 110,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26737659565345,
                     25.62221339773015
                 ]
             },
             "properties": {
                 "OBJECTID": 110,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 111,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26742262655301,
                     25.622107919844666
                 ]
             },
             "properties": {
                 "OBJECTID": 111,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 112,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26773618957355,
                     25.622167429783133
                 ]
             },
             "properties": {
                 "OBJECTID": 112,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 113,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26788615062543,
                     25.622199833255763
                 ]
             },
             "properties": {
                 "OBJECTID": 113,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 114,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26804444119807,
                     25.622287592698285
                 ]
             },
             "properties": {
                 "OBJECTID": 114,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 115,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26849101304987,
                     25.622532991604317
                 ]
             },
             "properties": {
                 "OBJECTID": 115,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 116,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26867360060623,
                     25.62265880585926
                 ]
             },
             "properties": {
                 "OBJECTID": 116,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 117,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26877514575642,
                     25.622720735873315
                 ]
             },
             "properties": {
                 "OBJECTID": 117,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 118,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26884859968317,
                     25.622754584556503
                 ]
             },
             "properties": {
                 "OBJECTID": 118,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 119,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26890261026733,
                     25.622727380963909
                 ]
             },
             "properties": {
                 "OBJECTID": 119,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 120,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26897780887884,
                     25.622614927037318
                 ]
             },
             "properties": {
                 "OBJECTID": 120,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 121,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26906651710613,
                     25.622075903080486
                 ]
             },
             "properties": {
                 "OBJECTID": 121,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 122,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26908787330677,
                     25.621876435249362
                 ]
             },
             "properties": {
                 "OBJECTID": 122,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 123,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26923282423553,
                     25.621568781674
                 ]
             },
             "properties": {
                 "OBJECTID": 123,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 124,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26929275325801,
                     25.621511370753183
                 ]
             },
             "properties": {
                 "OBJECTID": 124,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 125,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27013336126566,
                     25.622345046786506
                 ]
             },
             "properties": {
                 "OBJECTID": 125,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 126,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27055996816807,
                     25.624946298033251
                 ]
             },
             "properties": {
                 "OBJECTID": 126,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 127,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27084436707395,
                     25.626672278702756
                 ]
             },
             "properties": {
                 "OBJECTID": 127,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 128,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27091831742649,
                     25.62710049629095
                 ]
             },
             "properties": {
                 "OBJECTID": 128,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 129,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27108590429111,
                     25.628009239634764
                 ]
             },
             "properties": {
                 "OBJECTID": 129,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 130,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27130088093099,
                     25.628794155326034
                 ]
             },
             "properties": {
                 "OBJECTID": 130,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 131,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27145671095849,
                     25.629224162565095
                 ]
             },
             "properties": {
                 "OBJECTID": 131,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 132,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27200974815139,
                     25.630404942733946
                 ]
             },
             "properties": {
                 "OBJECTID": 132,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 133,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27266564979993,
                     25.631615664031699
                 ]
             },
             "properties": {
                 "OBJECTID": 133,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 134,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27284591980339,
                     25.631944306884293
                 ]
             },
             "properties": {
                 "OBJECTID": 134,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 135,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27349639404343,
                     25.633150711436258
                 ]
             },
             "properties": {
                 "OBJECTID": 135,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 136,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27389214610571,
                     25.63388071542272
                 ]
             },
             "properties": {
                 "OBJECTID": 136,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 137,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27507263579355,
                     25.636063549892981
                 ]
             },
             "properties": {
                 "OBJECTID": 137,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 138,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27543225309608,
                     25.63672649683042
                 ]
             },
             "properties": {
                 "OBJECTID": 138,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 139,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27566958508334,
                     25.637187476720726
                 ]
             },
             "properties": {
                 "OBJECTID": 139,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 140,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27593141280454,
                     25.637628811820264
                 ]
             },
             "properties": {
                 "OBJECTID": 140,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 141,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27600420033366,
                     25.637756665737584
                 ]
             },
             "properties": {
                 "OBJECTID": 141,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 142,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27607656698007,
                     25.637825731872056
                 ]
             },
             "properties": {
                 "OBJECTID": 142,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 143,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2761120830063,
                     25.637976772110392
                 ]
             },
             "properties": {
                 "OBJECTID": 143,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 144,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27741034341267,
                     25.639508904812146
                 ]
             },
             "properties": {
                 "OBJECTID": 144,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 145,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27798783137337,
                     25.640064346792201
                 ]
             },
             "properties": {
                 "OBJECTID": 145,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 146,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27814952228266,
                     25.64022279564557
                 ]
             },
             "properties": {
                 "OBJECTID": 146,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 147,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2785383081964,
                     25.640547687425908
                 ]
             },
             "properties": {
                 "OBJECTID": 147,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 148,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27864532122464,
                     25.64061483710509
                 ]
             },
             "properties": {
                 "OBJECTID": 148,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 149,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27945717970516,
                     25.641201590780838
                 ]
             },
             "properties": {
                 "OBJECTID": 149,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 150,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27986441161335,
                     25.641475250882195
                 ]
             },
             "properties": {
                 "OBJECTID": 150,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 151,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28017941534779,
                     25.641688849760669
                 ]
             },
             "properties": {
                 "OBJECTID": 151,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 152,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28118671629397,
                     25.64239364575019
                 ]
             },
             "properties": {
                 "OBJECTID": 152,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 153,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28178374922072,
                     25.642910719953818
                 ]
             },
             "properties": {
                 "OBJECTID": 153,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 154,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28238197824578,
                     25.643625463344506
                 ]
             },
             "properties": {
                 "OBJECTID": 154,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 155,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28309165665468,
                     25.648397838089807
                 ]
             },
             "properties": {
                 "OBJECTID": 155,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 156,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28305580967793,
                     25.650130111315661
                 ]
             },
             "properties": {
                 "OBJECTID": 156,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 157,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28304418773911,
                     25.650698746350088
                 ]
             },
             "properties": {
                 "OBJECTID": 157,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 158,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28303254511593,
                     25.651028963016302
                 ]
             },
             "properties": {
                 "OBJECTID": 158,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 159,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28247677128604,
                     25.652305750310177
                 ]
             },
             "properties": {
                 "OBJECTID": 159,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 160,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28243842329465,
                     25.652721931372071
                 ]
             },
             "properties": {
                 "OBJECTID": 160,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 161,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28247385658329,
                     25.652974476291092
                 ]
             },
             "properties": {
                 "OBJECTID": 161,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 162,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28242647220401,
                     25.653230783074207
                 ]
             },
             "properties": {
                 "OBJECTID": 162,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 163,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28242434710597,
                     25.653308982723445
                 ]
             },
             "properties": {
                 "OBJECTID": 163,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 164,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28234091430181,
                     25.653877591677542
                 ]
             },
             "properties": {
                 "OBJECTID": 164,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 165,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28238776898041,
                     25.654435698348834
                 ]
             },
             "properties": {
                 "OBJECTID": 165,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 166,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28281659810756,
                     25.654407134981341
                 ]
             },
             "properties": {
                 "OBJECTID": 166,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 167,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28368359132475,
                     25.654143848559841
                 ]
             },
             "properties": {
                 "OBJECTID": 167,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 168,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28389106941739,
                     25.654058993927492
                 ]
             },
             "properties": {
                 "OBJECTID": 168,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 169,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28419583526966,
                     25.653983941006118
                 ]
             },
             "properties": {
                 "OBJECTID": 169,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 170,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28444005156717,
                     25.653914936924878
                 ]
             },
             "properties": {
                 "OBJECTID": 170,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 171,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28538777062914,
                     25.653860360667181
                 ]
             },
             "properties": {
                 "OBJECTID": 171,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 172,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28605045766255,
                     25.653992156312995
                 ]
             },
             "properties": {
                 "OBJECTID": 172,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 173,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28676578111595,
                     25.654165230840761
                 ]
             },
             "properties": {
                 "OBJECTID": 173,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 174,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.287368073278,
                     25.654422547562547
                 ]
             },
             "properties": {
                 "OBJECTID": 174,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 175,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2883474223957,
                     25.65523405620678
                 ]
             },
             "properties": {
                 "OBJECTID": 175,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 176,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28868306287313,
                     25.655776389668688
                 ]
             },
             "properties": {
                 "OBJECTID": 176,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 177,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28923034080765,
                     25.657194389409597
                 ]
             },
             "properties": {
                 "OBJECTID": 177,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 178,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.29032811624961,
                     25.660605081759172
                 ]
             },
             "properties": {
                 "OBJECTID": 178,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 179,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.29005791044426,
                     25.660989449303713
                 ]
             },
             "properties": {
                 "OBJECTID": 179,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 180,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28951748894104,
                     25.661189206716585
                 ]
             },
             "properties": {
                 "OBJECTID": 180,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 181,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28899743438416,
                     25.661533825125957
                 ]
             },
             "properties": {
                 "OBJECTID": 181,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 182,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28794281291056,
                     25.66248369896357
                 ]
             },
             "properties": {
                 "OBJECTID": 182,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 183,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2876628827359,
                     25.662739080344352
                 ]
             },
             "properties": {
                 "OBJECTID": 183,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 184,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28719386830244,
                     25.663100543955011
                 ]
             },
             "properties": {
                 "OBJECTID": 184,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 185,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28660896812943,
                     25.663393385696224
                 ]
             },
             "properties": {
                 "OBJECTID": 185,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 186,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28595586067451,
                     25.663715902367073
                 ]
             },
             "properties": {
                 "OBJECTID": 186,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 187,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28506049924272,
                     25.664166837729454
                 ]
             },
             "properties": {
                 "OBJECTID": 187,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 188,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28456921579675,
                     25.664420169555228
                 ]
             },
             "properties": {
                 "OBJECTID": 188,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 189,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28389579895202,
                     25.664731514847574
                 ]
             },
             "properties": {
                 "OBJECTID": 189,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 190,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28343431094476,
                     25.66486835389145
                 ]
             },
             "properties": {
                 "OBJECTID": 190,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 191,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28287897058806,
                     25.664917619652499
                 ]
             },
             "properties": {
                 "OBJECTID": 191,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 192,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28257599258802,
                     25.664900955214989
                 ]
             },
             "properties": {
                 "OBJECTID": 192,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 193,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28233058109151,
                     25.664875011572576
                 ]
             },
             "properties": {
                 "OBJECTID": 193,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 194,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28182557668907,
                     25.664763232137489
                 ]
             },
             "properties": {
                 "OBJECTID": 194,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 195,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.28004606657277,
                     25.664520401695256
                 ]
             },
             "properties": {
                 "OBJECTID": 195,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 196,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2790940487514,
                     25.664056590739108
                 ]
             },
             "properties": {
                 "OBJECTID": 196,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 197,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27869471828666,
                     25.663659819744964
                 ]
             },
             "properties": {
                 "OBJECTID": 197,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 198,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27856311059912,
                     25.663561444704953
                 ]
             },
             "properties": {
                 "OBJECTID": 198,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 199,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27835956164449,
                     25.6634287461398
                 ]
             },
             "properties": {
                 "OBJECTID": 199,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 200,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27732986937804,
                     25.66324181575726
                 ]
             },
             "properties": {
                 "OBJECTID": 200,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 201,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27649886433119,
                     25.663711019947641
                 ]
             },
             "properties": {
                 "OBJECTID": 201,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 202,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27630894909879,
                     25.664068429414556
                 ]
             },
             "properties": {
                 "OBJECTID": 202,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 203,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27585526271025,
                     25.665920127212132
                 ]
             },
             "properties": {
                 "OBJECTID": 203,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 204,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27553045186892,
                     25.666892431040935
                 ]
             },
             "properties": {
                 "OBJECTID": 204,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 205,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27549501588231,
                     25.666965402931055
                 ]
             },
             "properties": {
                 "OBJECTID": 205,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 206,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2753841834334,
                     25.6671538999326
                 ]
             },
             "properties": {
                 "OBJECTID": 206,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 207,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27279854441736,
                     25.670145125996839
                 ]
             },
             "properties": {
                 "OBJECTID": 207,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 208,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27262640608382,
                     25.670430336990535
                 ]
             },
             "properties": {
                 "OBJECTID": 208,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 209,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27235829030292,
                     25.671251337079582
                 ]
             },
             "properties": {
                 "OBJECTID": 209,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 210,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27225790437882,
                     25.671938560316335
                 ]
             },
             "properties": {
                 "OBJECTID": 210,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 211,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27209191560945,
                     25.672844998697769
                 ]
             },
             "properties": {
                 "OBJECTID": 211,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 212,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27173685786977,
                     25.673710408208819
                 ]
             },
             "properties": {
                 "OBJECTID": 212,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 213,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27141224757725,
                     25.674361229587134
                 ]
             },
             "properties": {
                 "OBJECTID": 213,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 214,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27107978260597,
                     25.675063827633608
                 ]
             },
             "properties": {
                 "OBJECTID": 214,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 215,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27012664422932,
                     25.676178625443242
                 ]
             },
             "properties": {
                 "OBJECTID": 215,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 216,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26967642832454,
                     25.677497823865508
                 ]
             },
             "properties": {
                 "OBJECTID": 216,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 217,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26949620058923,
                     25.677736684700278
                 ]
             },
             "properties": {
                 "OBJECTID": 217,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 218,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26919538096212,
                     25.678331121284259
                 ]
             },
             "properties": {
                 "OBJECTID": 218,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 219,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26903341486025,
                     25.678842976722024
                 ]
             },
             "properties": {
                 "OBJECTID": 219,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 220,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26884958084355,
                     25.679748989724146
                 ]
             },
             "properties": {
                 "OBJECTID": 220,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 221,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26875371940866,
                     25.680389766574535
                 ]
             },
             "properties": {
                 "OBJECTID": 221,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 222,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26876655003633,
                     25.680721393377667
                 ]
             },
             "properties": {
                 "OBJECTID": 222,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 223,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26901101005012,
                     25.681424139812236
                 ]
             },
             "properties": {
                 "OBJECTID": 223,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 224,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2694370449837,
                     25.681794847554215
                 ]
             },
             "properties": {
                 "OBJECTID": 224,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 225,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26992684994423,
                     25.681931313379437
                 ]
             },
             "properties": {
                 "OBJECTID": 225,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 226,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27014660468211,
                     25.68192731319499
                 ]
             },
             "properties": {
                 "OBJECTID": 226,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 227,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27182297425094,
                     25.682244429436935
                 ]
             },
             "properties": {
                 "OBJECTID": 227,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 228,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27191258899484,
                     25.682487859727019
                 ]
             },
             "properties": {
                 "OBJECTID": 228,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 229,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27198148875476,
                     25.682973348841017
                 ]
             },
             "properties": {
                 "OBJECTID": 229,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 230,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27204781105769,
                     25.683477548350254
                 ]
             },
             "properties": {
                 "OBJECTID": 230,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 231,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27230723399174,
                     25.684111994872694
                 ]
             },
             "properties": {
                 "OBJECTID": 231,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 232,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27301827127627,
                     25.685027625224791
                 ]
             },
             "properties": {
                 "OBJECTID": 232,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 233,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27320476368897,
                     25.685543676899215
                 ]
             },
             "properties": {
                 "OBJECTID": 233,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 234,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27374200788739,
                     25.687753162778279
                 ]
             },
             "properties": {
                 "OBJECTID": 234,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 235,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27386745791694,
                     25.689214016114022
                 ]
             },
             "properties": {
                 "OBJECTID": 235,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 236,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27352575421025,
                     25.691309901430884
                 ]
             },
             "properties": {
                 "OBJECTID": 236,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 237,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27352428202005,
                     25.691571248014725
                 ]
             },
             "properties": {
                 "OBJECTID": 237,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 238,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27361154323819,
                     25.692020113137744
                 ]
             },
             "properties": {
                 "OBJECTID": 238,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 239,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27394140826942,
                     25.69307293416864
                 ]
             },
             "properties": {
                 "OBJECTID": 239,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 240,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.273873054398,
                     25.693678802035151
                 ]
             },
             "properties": {
                 "OBJECTID": 240,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 241,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27333427685545,
                     25.694405732138875
                 ]
             },
             "properties": {
                 "OBJECTID": 241,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 242,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27292913587098,
                     25.694843448366498
                 ]
             },
             "properties": {
                 "OBJECTID": 242,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 243,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27265397120385,
                     25.695285403998241
                 ]
             },
             "properties": {
                 "OBJECTID": 243,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 244,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27254909136752,
                     25.695935100324675
                 ]
             },
             "properties": {
                 "OBJECTID": 244,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 245,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27163922027387,
                     25.697061746702275
                 ]
             },
             "properties": {
                 "OBJECTID": 245,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 246,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.27101312305723,
                     25.69760559462253
                 ]
             },
             "properties": {
                 "OBJECTID": 246,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 247,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26853797384848,
                     25.698668745266446
                 ]
             },
             "properties": {
                 "OBJECTID": 247,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 248,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26770836365012,
                     25.698948354383106
                 ]
             },
             "properties": {
                 "OBJECTID": 248,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 249,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26731527358106,
                     25.699102434329916
                 ]
             },
             "properties": {
                 "OBJECTID": 249,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 250,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26648410485762,
                     25.699751904926529
                 ]
             },
             "properties": {
                 "OBJECTID": 250,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 251,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26635061129173,
                     25.699929209865218
                 ]
             },
             "properties": {
                 "OBJECTID": 251,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 252,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26604635625438,
                     25.700732964554732
                 ]
             },
             "properties": {
                 "OBJECTID": 252,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 253,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.265833307761,
                     25.700728780009229
                 ]
             },
             "properties": {
                 "OBJECTID": 253,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 254,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26544021229603,
                     25.700916988328402
                 ]
             },
             "properties": {
                 "OBJECTID": 254,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 255,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2651533726301,
                     25.701044022064025
                 ]
             },
             "properties": {
                 "OBJECTID": 255,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 256,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26470545359388,
                     25.701199748669524
                 ]
             },
             "properties": {
                 "OBJECTID": 256,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 257,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26435996733869,
                     25.701322600557603
                 ]
             },
             "properties": {
                 "OBJECTID": 257,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 258,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26431957968492,
                     25.701336962730693
                 ]
             },
             "properties": {
                 "OBJECTID": 258,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 259,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26428816996315,
                     25.701919462613148
                 ]
             },
             "properties": {
                 "OBJECTID": 259,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 260,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26406242124381,
                     25.702111118033713
                 ]
             },
             "properties": {
                 "OBJECTID": 260,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 261,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26410486204981,
                     25.70219879294001
                 ]
             },
             "properties": {
                 "OBJECTID": 261,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 262,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26379950264499,
                     25.702353462842098
                 ]
             },
             "properties": {
                 "OBJECTID": 262,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 263,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26380802641933,
                     25.702377603343791
                 ]
             },
             "properties": {
                 "OBJECTID": 263,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 264,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2634715162975,
                     25.702494078139239
                 ]
             },
             "properties": {
                 "OBJECTID": 264,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 265,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26251817197613,
                     25.702636867097794
                 ]
             },
             "properties": {
                 "OBJECTID": 265,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 266,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26191759302259,
                     25.702048613352872
                 ]
             },
             "properties": {
                 "OBJECTID": 266,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 267,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26156917677616,
                     25.702044941420979
                 ]
             },
             "properties": {
                 "OBJECTID": 267,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 268,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26152330865381,
                     25.702375016893541
                 ]
             },
             "properties": {
                 "OBJECTID": 268,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 269,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26109530870161,
                     25.702349748642064
                 ]
             },
             "properties": {
                 "OBJECTID": 269,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 270,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26043593297197,
                     25.702569912571448
                 ]
             },
             "properties": {
                 "OBJECTID": 270,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 271,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26028386480857,
                     25.702289407730007
                 ]
             },
             "properties": {
                 "OBJECTID": 271,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 272,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25934339608091,
                     25.702559090129967
                 ]
             },
             "properties": {
                 "OBJECTID": 272,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 273,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25929536059249,
                     25.702315724591074
                 ]
             },
             "properties": {
                 "OBJECTID": 273,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 274,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25910729526555,
                     25.702335576225892
                 ]
             },
             "properties": {
                 "OBJECTID": 274,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 275,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.259119365966,
                     25.70242550303459
                 ]
             },
             "properties": {
                 "OBJECTID": 275,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 276,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25887536280783,
                     25.702456238264858
                 ]
             },
             "properties": {
                 "OBJECTID": 276,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 277,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2584738712722,
                     25.702678049752251
                 ]
             },
             "properties": {
                 "OBJECTID": 277,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 278,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25785778261064,
                     25.702561774606238
                 ]
             },
             "properties": {
                 "OBJECTID": 278,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 279,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25787817563736,
                     25.70264152108939
                 ]
             },
             "properties": {
                 "OBJECTID": 279,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 280,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25697483631973,
                     25.703017585193436
                 ]
             },
             "properties": {
                 "OBJECTID": 280,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 281,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25655476928722,
                     25.703641700304274
                 ]
             },
             "properties": {
                 "OBJECTID": 281,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 282,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25637550202788,
                     25.703700990808102
                 ]
             },
             "properties": {
                 "OBJECTID": 282,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 283,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25603236120457,
                     25.703950167766095
                 ]
             },
             "properties": {
                 "OBJECTID": 283,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 284,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25602994112893,
                     25.70439748695452
                 ]
             },
             "properties": {
                 "OBJECTID": 284,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 285,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25610923435374,
                     25.704614400734044
                 ]
             },
             "properties": {
                 "OBJECTID": 285,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 286,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25618405255204,
                     25.704747720730779
                 ]
             },
             "properties": {
                 "OBJECTID": 286,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 287,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25626986496235,
                     25.704983439334285
                 ]
             },
             "properties": {
                 "OBJECTID": 287,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 288,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25650935082592,
                     25.704855922662773
                 ]
             },
             "properties": {
                 "OBJECTID": 288,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 289,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25710490166995,
                     25.705406868032583
                 ]
             },
             "properties": {
                 "OBJECTID": 289,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 290,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25721112689206,
                     25.70559049160795
                 ]
             },
             "properties": {
                 "OBJECTID": 290,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 291,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25751415885139,
                     25.705843695730039
                 ]
             },
             "properties": {
                 "OBJECTID": 291,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 292,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25774043907006,
                     25.705987431674657
                 ]
             },
             "properties": {
                 "OBJECTID": 292,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 293,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25779861441458,
                     25.706420859035461
                 ]
             },
             "properties": {
                 "OBJECTID": 293,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 294,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25803765601307,
                     25.706520444562898
                 ]
             },
             "properties": {
                 "OBJECTID": 294,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 295,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25798186926789,
                     25.707142982262894
                 ]
             },
             "properties": {
                 "OBJECTID": 295,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 296,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25811176644493,
                     25.707283545399378
                 ]
             },
             "properties": {
                 "OBJECTID": 296,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 297,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25825195726208,
                     25.707185530987488
                 ]
             },
             "properties": {
                 "OBJECTID": 297,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 298,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25897051377825,
                     25.70719391356829
                 ]
             },
             "properties": {
                 "OBJECTID": 298,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 299,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25915322274312,
                     25.707337482239041
                 ]
             },
             "properties": {
                 "OBJECTID": 299,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 300,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25931083162965,
                     25.707332775187467
                 ]
             },
             "properties": {
                 "OBJECTID": 300,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 301,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25932897185464,
                     25.707321094792746
                 ]
             },
             "properties": {
                 "OBJECTID": 301,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 302,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25944427573279,
                     25.707443540187285
                 ]
             },
             "properties": {
                 "OBJECTID": 302,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 303,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25949264397042,
                     25.707409882160391
                 ]
             },
             "properties": {
                 "OBJECTID": 303,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 304,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2600335807852,
                     25.707867736007245
                 ]
             },
             "properties": {
                 "OBJECTID": 304,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 305,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26017316635858,
                     25.70793079467046
                 ]
             },
             "properties": {
                 "OBJECTID": 305,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 306,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26022474877317,
                     25.70798552021563
                 ]
             },
             "properties": {
                 "OBJECTID": 306,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 307,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26042961703325,
                     25.707835717444482
                 ]
             },
             "properties": {
                 "OBJECTID": 307,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 308,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26121656339376,
                     25.708673301032093
                 ]
             },
             "properties": {
                 "OBJECTID": 308,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 309,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26042991740684,
                     25.711466401053599
                 ]
             },
             "properties": {
                 "OBJECTID": 309,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 310,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26020228730579,
                     25.711777849767998
                 ]
             },
             "properties": {
                 "OBJECTID": 310,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 311,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.26001156268353,
                     25.711989641908474
                 ]
             },
             "properties": {
                 "OBJECTID": 311,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 312,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25872121990841,
                     25.712813661021698
                 ]
             },
             "properties": {
                 "OBJECTID": 312,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 313,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25767728238071,
                     25.713357390231408
                 ]
             },
             "properties": {
                 "OBJECTID": 313,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 314,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.257301004238,
                     25.713555856217852
                 ]
             },
             "properties": {
                 "OBJECTID": 314,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 315,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25415156314421,
                     25.715419190343425
                 ]
             },
             "properties": {
                 "OBJECTID": 315,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 316,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25398693414974,
                     25.715608697283585
                 ]
             },
             "properties": {
                 "OBJECTID": 316,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 317,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2537232349394,
                     25.71601761272268
                 ]
             },
             "properties": {
                 "OBJECTID": 317,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 318,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25340647213102,
                     25.716502698041154
                 ]
             },
             "properties": {
                 "OBJECTID": 318,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 319,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25293432086227,
                     25.716943784927764
                 ]
             },
             "properties": {
                 "OBJECTID": 319,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 320,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2512027796846,
                     25.717552064776044
                 ]
             },
             "properties": {
                 "OBJECTID": 320,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 321,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2506663502719,
                     25.717709193424582
                 ]
             },
             "properties": {
                 "OBJECTID": 321,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 322,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.25004362821096,
                     25.717874592238672
                 ]
             },
             "properties": {
                 "OBJECTID": 322,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 323,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24808893735462,
                     25.718143742239988
                 ]
             },
             "properties": {
                 "OBJECTID": 323,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 324,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24694466516422,
                     25.718181134252006
                 ]
             },
             "properties": {
                 "OBJECTID": 324,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 325,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24682726766434,
                     25.718065799796875
                 ]
             },
             "properties": {
                 "OBJECTID": 325,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 326,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24697824495013,
                     25.717524132732592
                 ]
             },
             "properties": {
                 "OBJECTID": 326,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 327,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24690200492364,
                     25.717253982685179
                 ]
             },
             "properties": {
                 "OBJECTID": 327,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 328,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24685410073624,
                     25.717256834435432
                 ]
             },
             "properties": {
                 "OBJECTID": 328,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 329,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24683071026914,
                     25.717002962117078
                 ]
             },
             "properties": {
                 "OBJECTID": 329,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 330,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24677771412024,
                     25.716985189714762
                 ]
             },
             "properties": {
                 "OBJECTID": 330,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 331,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2467876858031,
                     25.716627792838437
                 ]
             },
             "properties": {
                 "OBJECTID": 331,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 332,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24679042513804,
                     25.716594626740687
                 ]
             },
             "properties": {
                 "OBJECTID": 332,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 333,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24681846869743,
                     25.716581216949635
                 ]
             },
             "properties": {
                 "OBJECTID": 333,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 334,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24681933114726,
                     25.716493794752864
                 ]
             },
             "properties": {
                 "OBJECTID": 334,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 335,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2468717787097,
                     25.716225428960456
                 ]
             },
             "properties": {
                 "OBJECTID": 335,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 336,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2467917444435,
                     25.715957775431036
                 ]
             },
             "properties": {
                 "OBJECTID": 336,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 337,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24571646644182,
                     25.715881175675747
                 ]
             },
             "properties": {
                 "OBJECTID": 337,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 338,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24564689578767,
                     25.716448220708855
                 ]
             },
             "properties": {
                 "OBJECTID": 338,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 339,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24517688490539,
                     25.716815411202333
                 ]
             },
             "properties": {
                 "OBJECTID": 339,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 340,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24411789272665,
                     25.716760112789018
                 ]
             },
             "properties": {
                 "OBJECTID": 340,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 341,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24335682885464,
                     25.716574822769928
                 ]
             },
             "properties": {
                 "OBJECTID": 341,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 342,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24308618597877,
                     25.716469743284051
                 ]
             },
             "properties": {
                 "OBJECTID": 342,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 343,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24239442026635,
                     25.716016002036895
                 ]
             },
             "properties": {
                 "OBJECTID": 343,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 344,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.24060206962986,
                     25.714728004693711
                 ]
             },
             "properties": {
                 "OBJECTID": 344,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 345,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23971572480093,
                     25.713767723901412
                 ]
             },
             "properties": {
                 "OBJECTID": 345,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 346,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23944094234565,
                     25.713005318240903
                 ]
             },
             "properties": {
                 "OBJECTID": 346,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 347,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23902075570339,
                     25.711698238183203
                 ]
             },
             "properties": {
                 "OBJECTID": 347,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 348,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.237884394849,
                     25.709050950640915
                 ]
             },
             "properties": {
                 "OBJECTID": 348,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 349,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23723772204329,
                     25.707944842980169
                 ]
             },
             "properties": {
                 "OBJECTID": 349,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 350,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23718067894526,
                     25.707845031722911
                 ]
             },
             "properties": {
                 "OBJECTID": 350,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 351,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23713852862028,
                     25.70771639539538
                 ]
             },
             "properties": {
                 "OBJECTID": 351,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 352,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23699635659665,
                     25.707467748138129
                 ]
             },
             "properties": {
                 "OBJECTID": 352,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 353,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23706272386568,
                     25.707267871115391
                 ]
             },
             "properties": {
                 "OBJECTID": 353,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 354,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23693366575611,
                     25.707156560227133
                 ]
             },
             "properties": {
                 "OBJECTID": 354,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 355,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23689055315657,
                     25.706854396113556
                 ]
             },
             "properties": {
                 "OBJECTID": 355,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 356,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2368516062167,
                     25.706792942740151
                 ]
             },
             "properties": {
                 "OBJECTID": 356,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 357,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23659756032919,
                     25.706768446106935
                 ]
             },
             "properties": {
                 "OBJECTID": 357,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 358,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23656673336814,
                     25.706723092396828
                 ]
             },
             "properties": {
                 "OBJECTID": 358,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 359,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23647771667333,
                     25.706769932686313
                 ]
             },
             "properties": {
                 "OBJECTID": 359,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 360,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23615289773812,
                     25.70698398032647
                 ]
             },
             "properties": {
                 "OBJECTID": 360,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 361,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23568437163652,
                     25.707657308138323
                 ]
             },
             "properties": {
                 "OBJECTID": 361,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 362,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23566304331484,
                     25.707722079110738
                 ]
             },
             "properties": {
                 "OBJECTID": 362,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 363,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23543874070538,
                     25.70806870300828
                 ]
             },
             "properties": {
                 "OBJECTID": 363,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 364,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2352527905839,
                     25.708351066748378
                 ]
             },
             "properties": {
                 "OBJECTID": 364,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 365,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23521255941216,
                     25.709141218292643
                 ]
             },
             "properties": {
                 "OBJECTID": 365,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 366,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2348275209734,
                     25.709863614913957
                 ]
             },
             "properties": {
                 "OBJECTID": 366,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 367,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23475253640049,
                     25.710003165413809
                 ]
             },
             "properties": {
                 "OBJECTID": 367,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 368,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23443768914808,
                     25.710591467722111
                 ]
             },
             "properties": {
                 "OBJECTID": 368,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 369,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23430521721207,
                     25.710838393676966
                 ]
             },
             "properties": {
                 "OBJECTID": 369,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 370,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23400656944767,
                     25.711633047227394
                 ]
             },
             "properties": {
                 "OBJECTID": 370,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 371,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23428754193657,
                     25.713051150390356
                 ]
             },
             "properties": {
                 "OBJECTID": 371,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 372,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2347246987859,
                     25.713564280167475
                 ]
             },
             "properties": {
                 "OBJECTID": 372,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 373,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23542649913367,
                     25.714796197586509
                 ]
             },
             "properties": {
                 "OBJECTID": 373,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 374,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23491908274951,
                     25.715743056896599
                 ]
             },
             "properties": {
                 "OBJECTID": 374,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 375,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23444219115424,
                     25.716751778771652
                 ]
             },
             "properties": {
                 "OBJECTID": 375,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 376,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23307226377403,
                     25.725942865370996
                 ]
             },
             "properties": {
                 "OBJECTID": 376,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 377,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23211509373664,
                     25.729265745218015
                 ]
             },
             "properties": {
                 "OBJECTID": 377,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 378,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2319874142878,
                     25.730125178734056
                 ]
             },
             "properties": {
                 "OBJECTID": 378,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 379,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23202219556794,
                     25.73030736699144
                 ]
             },
             "properties": {
                 "OBJECTID": 379,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 380,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23235074758901,
                     25.730880385321598
                 ]
             },
             "properties": {
                 "OBJECTID": 380,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 381,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23234023991023,
                     25.731120691366868
                 ]
             },
             "properties": {
                 "OBJECTID": 381,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 382,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23196537100512,
                     25.731987790704011
                 ]
             },
             "properties": {
                 "OBJECTID": 382,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 383,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23191576530041,
                     25.732254145612387
                 ]
             },
             "properties": {
                 "OBJECTID": 383,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 384,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23199211864141,
                     25.732573983202826
                 ]
             },
             "properties": {
                 "OBJECTID": 384,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 385,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23223889440953,
                     25.73325915238803
                 ]
             },
             "properties": {
                 "OBJECTID": 385,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 386,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23225072768901,
                     25.733582866056452
                 ]
             },
             "properties": {
                 "OBJECTID": 386,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 387,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23196138880712,
                     25.734079486079452
                 ]
             },
             "properties": {
                 "OBJECTID": 387,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 388,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23140923564779,
                     25.734819268394574
                 ]
             },
             "properties": {
                 "OBJECTID": 388,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 389,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23101770500313,
                     25.735590616913498
                 ]
             },
             "properties": {
                 "OBJECTID": 389,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 390,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2306630096902,
                     25.736471480374576
                 ]
             },
             "properties": {
                 "OBJECTID": 390,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 391,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23038609044579,
                     25.737848998932179
                 ]
             },
             "properties": {
                 "OBJECTID": 391,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 392,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23026558578823,
                     25.739582364834348
                 ]
             },
             "properties": {
                 "OBJECTID": 392,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 393,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23018123567556,
                     25.739846284378586
                 ]
             },
             "properties": {
                 "OBJECTID": 393,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 394,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.230089175675,
                     25.740018297706342
                 ]
             },
             "properties": {
                 "OBJECTID": 394,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 395,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22994587770017,
                     25.740236842856064
                 ]
             },
             "properties": {
                 "OBJECTID": 395,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 396,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22985893664065,
                     25.740314842855753
                 ]
             },
             "properties": {
                 "OBJECTID": 396,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 397,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22922375896934,
                     25.740756470234942
                 ]
             },
             "properties": {
                 "OBJECTID": 397,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 398,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2283999817738,
                     25.741192835680863
                 ]
             },
             "properties": {
                 "OBJECTID": 398,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 399,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22776262414584,
                     25.742132978853931
                 ]
             },
             "properties": {
                 "OBJECTID": 399,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 400,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22740710595326,
                     25.742776002210803
                 ]
             },
             "properties": {
                 "OBJECTID": 400,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 401,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22636968401378,
                     25.743660990862111
                 ]
             },
             "properties": {
                 "OBJECTID": 401,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 402,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22626214488213,
                     25.743792767622153
                 ]
             },
             "properties": {
                 "OBJECTID": 402,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 403,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22622019240799,
                     25.743895418039187
                 ]
             },
             "properties": {
                 "OBJECTID": 403,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 404,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22625230180233,
                     25.744143873740825
                 ]
             },
             "properties": {
                 "OBJECTID": 404,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 405,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22646945300289,
                     25.744913515346525
                 ]
             },
             "properties": {
                 "OBJECTID": 405,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 406,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22658118567324,
                     25.745687488087128
                 ]
             },
             "properties": {
                 "OBJECTID": 406,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 407,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22644903839245,
                     25.746059221955591
                 ]
             },
             "properties": {
                 "OBJECTID": 407,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 408,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22511071318894,
                     25.748547825921491
                 ]
             },
             "properties": {
                 "OBJECTID": 408,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 409,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22476614783949,
                     25.749285050564708
                 ]
             },
             "properties": {
                 "OBJECTID": 409,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 410,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22485568973838,
                     25.751666548872208
                 ]
             },
             "properties": {
                 "OBJECTID": 410,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 411,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22495169956136,
                     25.751973854409925
                 ]
             },
             "properties": {
                 "OBJECTID": 411,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 412,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22483249082683,
                     25.753458849146625
                 ]
             },
             "properties": {
                 "OBJECTID": 412,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 413,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22450863686419,
                     25.755196956274574
                 ]
             },
             "properties": {
                 "OBJECTID": 413,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 414,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22421617823409,
                     25.756256712877075
                 ]
             },
             "properties": {
                 "OBJECTID": 414,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 415,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22368148721091,
                     25.757326968165103
                 ]
             },
             "properties": {
                 "OBJECTID": 415,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 416,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22352688206001,
                     25.757509374957749
                 ]
             },
             "properties": {
                 "OBJECTID": 416,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 417,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22336027635572,
                     25.757705940676658
                 ]
             },
             "properties": {
                 "OBJECTID": 417,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 418,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22309299424637,
                     25.757955857776665
                 ]
             },
             "properties": {
                 "OBJECTID": 418,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 419,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22283854726123,
                     25.758183878183502
                 ]
             },
             "properties": {
                 "OBJECTID": 419,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 420,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22231877239346,
                     25.758671198317188
                 ]
             },
             "properties": {
                 "OBJECTID": 420,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 421,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22163286756285,
                     25.759495578058534
                 ]
             },
             "properties": {
                 "OBJECTID": 421,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 422,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22137445906407,
                     25.759935172070584
                 ]
             },
             "properties": {
                 "OBJECTID": 422,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 423,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2197466007251,
                     25.763284000056615
                 ]
             },
             "properties": {
                 "OBJECTID": 423,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 424,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21802274065703,
                     25.768166264761362
                 ]
             },
             "properties": {
                 "OBJECTID": 424,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 425,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21787799837097,
                     25.768775660668268
                 ]
             },
             "properties": {
                 "OBJECTID": 425,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 426,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2174609332755,
                     25.770643316036285
                 ]
             },
             "properties": {
                 "OBJECTID": 426,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 427,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21731084092261,
                     25.771045654733257
                 ]
             },
             "properties": {
                 "OBJECTID": 427,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 428,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21708370185144,
                     25.771636843865281
                 ]
             },
             "properties": {
                 "OBJECTID": 428,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 429,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21684816401159,
                     25.772241759349754
                 ]
             },
             "properties": {
                 "OBJECTID": 429,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 430,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21673631173144,
                     25.772646737556954
                 ]
             },
             "properties": {
                 "OBJECTID": 430,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 431,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21672580315334,
                     25.772916356104986
                 ]
             },
             "properties": {
                 "OBJECTID": 431,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 432,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21674785542922,
                     25.773024023839696
                 ]
             },
             "properties": {
                 "OBJECTID": 432,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 433,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21683200229512,
                     25.773331993976399
                 ]
             },
             "properties": {
                 "OBJECTID": 433,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 434,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21698936386809,
                     25.773967348814153
                 ]
             },
             "properties": {
                 "OBJECTID": 434,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 435,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21711536428268,
                     25.774587943079155
                 ]
             },
             "properties": {
                 "OBJECTID": 435,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 436,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21710672899241,
                     25.774768493671104
                 ]
             },
             "properties": {
                 "OBJECTID": 436,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 437,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21693716182062,
                     25.775215453130727
                 ]
             },
             "properties": {
                 "OBJECTID": 437,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 438,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21656347822199,
                     25.776075071907087
                 ]
             },
             "properties": {
                 "OBJECTID": 438,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 439,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21652260493431,
                     25.776260657803164
                 ]
             },
             "properties": {
                 "OBJECTID": 439,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 440,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21650074061677,
                     25.776459710147549
                 ]
             },
             "properties": {
                 "OBJECTID": 440,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 441,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21644371100854,
                     25.778276984595209
                 ]
             },
             "properties": {
                 "OBJECTID": 441,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 442,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21640072341472,
                     25.778546227226684
                 ]
             },
             "properties": {
                 "OBJECTID": 442,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 443,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21590284344154,
                     25.780013132100407
                 ]
             },
             "properties": {
                 "OBJECTID": 443,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 444,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21564630103603,
                     25.780542892140829
                 ]
             },
             "properties": {
                 "OBJECTID": 444,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 445,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21556678388004,
                     25.780686055217302
                 ]
             },
             "properties": {
                 "OBJECTID": 445,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 446,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21553171391753,
                     25.780740635971597
                 ]
             },
             "properties": {
                 "OBJECTID": 446,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 447,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21547692991646,
                     25.780850927928043
                 ]
             },
             "properties": {
                 "OBJECTID": 447,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 448,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21544172505565,
                     25.780938457144146
                 ]
             },
             "properties": {
                 "OBJECTID": 448,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 449,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21547447206927,
                     25.781511307301059
                 ]
             },
             "properties": {
                 "OBJECTID": 449,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 450,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21571539594885,
                     25.782057606773208
                 ]
             },
             "properties": {
                 "OBJECTID": 450,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 451,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21583585743895,
                     25.782240084612283
                 ]
             },
             "properties": {
                 "OBJECTID": 451,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 452,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2164870043718,
                     25.782963123349532
                 ]
             },
             "properties": {
                 "OBJECTID": 452,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 453,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21671595917422,
                     25.783193549442785
                 ]
             },
             "properties": {
                 "OBJECTID": 453,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 454,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2168152182478,
                     25.783311827379009
                 ]
             },
             "properties": {
                 "OBJECTID": 454,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 455,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21707465826893,
                     25.783649893327947
                 ]
             },
             "properties": {
                 "OBJECTID": 455,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 456,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21751926869933,
                     25.784506433724971
                 ]
             },
             "properties": {
                 "OBJECTID": 456,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 457,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21770404880289,
                     25.784847514201488
                 ]
             },
             "properties": {
                 "OBJECTID": 457,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 458,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21772502459027,
                     25.784887868580313
                 ]
             },
             "properties": {
                 "OBJECTID": 458,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 459,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21795903987783,
                     25.785533893874174
                 ]
             },
             "properties": {
                 "OBJECTID": 459,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 460,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21799883307972,
                     25.785767827323411
                 ]
             },
             "properties": {
                 "OBJECTID": 460,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 461,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21801329777554,
                     25.785902689657462
                 ]
             },
             "properties": {
                 "OBJECTID": 461,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 462,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21826761615762,
                     25.788153287144553
                 ]
             },
             "properties": {
                 "OBJECTID": 462,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 463,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21843294212664,
                     25.789337187960882
                 ]
             },
             "properties": {
                 "OBJECTID": 463,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 464,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21846845815287,
                     25.794373810502975
                 ]
             },
             "properties": {
                 "OBJECTID": 464,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 465,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21847139174139,
                     25.794829629184107
                 ]
             },
             "properties": {
                 "OBJECTID": 465,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 466,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21867190908142,
                     25.79551929318086
                 ]
             },
             "properties": {
                 "OBJECTID": 466,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 467,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21879766398109,
                     25.795761339414355
                 ]
             },
             "properties": {
                 "OBJECTID": 467,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 468,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21914771519494,
                     25.796390846860163
                 ]
             },
             "properties": {
                 "OBJECTID": 468,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 469,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21994111778844,
                     25.798079214284371
                 ]
             },
             "properties": {
                 "OBJECTID": 469,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 470,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22004640591695,
                     25.799575658290053
                 ]
             },
             "properties": {
                 "OBJECTID": 470,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 471,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2200596403402,
                     25.799756940030818
                 ]
             },
             "properties": {
                 "OBJECTID": 471,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 472,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22008180503133,
                     25.800048396816123
                 ]
             },
             "properties": {
                 "OBJECTID": 472,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 473,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22017114907931,
                     25.800447495255753
                 ]
             },
             "properties": {
                 "OBJECTID": 473,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 474,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2203510971255,
                     25.800817743444156
                 ]
             },
             "properties": {
                 "OBJECTID": 474,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 475,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2205858489578,
                     25.801469137690617
                 ]
             },
             "properties": {
                 "OBJECTID": 475,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 476,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22057365594952,
                     25.801658573584348
                 ]
             },
             "properties": {
                 "OBJECTID": 476,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 477,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2205551640896,
                     25.801945855716724
                 ]
             },
             "properties": {
                 "OBJECTID": 477,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 478,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22053229612862,
                     25.802164500691219
                 ]
             },
             "properties": {
                 "OBJECTID": 478,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 479,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22051618657281,
                     25.802440201354273
                 ]
             },
             "properties": {
                 "OBJECTID": 479,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 480,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22053801401819,
                     25.802763533710163
                 ]
             },
             "properties": {
                 "OBJECTID": 480,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 481,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22071644221012,
                     25.80337220656213
                 ]
             },
             "properties": {
                 "OBJECTID": 481,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 482,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22079393589149,
                     25.803497632309927
                 ]
             },
             "properties": {
                 "OBJECTID": 482,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 483,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2211343598629,
                     25.803890786230852
                 ]
             },
             "properties": {
                 "OBJECTID": 483,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 484,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22134255921179,
                     25.804101842725913
                 ]
             },
             "properties": {
                 "OBJECTID": 484,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 485,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22190122706002,
                     25.804808610928717
                 ]
             },
             "properties": {
                 "OBJECTID": 485,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 486,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22200577324787,
                     25.805178162142511
                 ]
             },
             "properties": {
                 "OBJECTID": 486,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 487,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22202630656886,
                     25.806218386369494
                 ]
             },
             "properties": {
                 "OBJECTID": 487,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 488,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22203636998256,
                     25.806819360125417
                 ]
             },
             "properties": {
                 "OBJECTID": 488,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 489,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22209231320977,
                     25.807470696815244
                 ]
             },
             "properties": {
                 "OBJECTID": 489,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 490,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22212200162915,
                     25.807749195269196
                 ]
             },
             "properties": {
                 "OBJECTID": 490,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 491,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22221110915541,
                     25.808535869135085
                 ]
             },
             "properties": {
                 "OBJECTID": 491,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 492,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22248697259579,
                     25.809584287085215
                 ]
             },
             "properties": {
                 "OBJECTID": 492,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 493,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22304240468327,
                     25.809854045028203
                 ]
             },
             "properties": {
                 "OBJECTID": 493,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 494,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22332351296978,
                     25.810057855685557
                 ]
             },
             "properties": {
                 "OBJECTID": 494,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 495,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22556830803103,
                     25.812302577901676
                 ]
             },
             "properties": {
                 "OBJECTID": 495,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 496,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22601112521329,
                     25.812672997860602
                 ]
             },
             "properties": {
                 "OBJECTID": 496,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 497,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22692928535827,
                     25.813445017273807
                 ]
             },
             "properties": {
                 "OBJECTID": 497,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 498,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22703541795022,
                     25.813617092654738
                 ]
             },
             "properties": {
                 "OBJECTID": 498,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 499,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22774514492255,
                     25.816078413887737
                 ]
             },
             "properties": {
                 "OBJECTID": 499,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 500,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22792972717525,
                     25.816437264068554
                 ]
             },
             "properties": {
                 "OBJECTID": 500,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 501,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22801472030318,
                     25.816598467545305
                 ]
             },
             "properties": {
                 "OBJECTID": 501,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 502,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22809688056662,
                     25.816754299371496
                 ]
             },
             "properties": {
                 "OBJECTID": 502,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 503,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22822060299848,
                     25.817015139637022
                 ]
             },
             "properties": {
                 "OBJECTID": 503,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 504,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22829342020526,
                     25.817220114017118
                 ]
             },
             "properties": {
                 "OBJECTID": 504,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 505,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2283013621182,
                     25.818080219326703
                 ]
             },
             "properties": {
                 "OBJECTID": 505,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 506,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22828341165018,
                     25.818731530835521
                 ]
             },
             "properties": {
                 "OBJECTID": 506,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 507,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22822627232466,
                     25.819764859960344
                 ]
             },
             "properties": {
                 "OBJECTID": 507,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 508,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2276223730741,
                     25.821273904367217
                 ]
             },
             "properties": {
                 "OBJECTID": 508,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 509,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22743792931703,
                     25.821882830828031
                 ]
             },
             "properties": {
                 "OBJECTID": 509,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 510,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22722737284505,
                     25.822566412709818
                 ]
             },
             "properties": {
                 "OBJECTID": 510,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 511,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22660515260577,
                     25.823716024175212
                 ]
             },
             "properties": {
                 "OBJECTID": 511,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 512,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22543103461459,
                     25.824806966568303
                 ]
             },
             "properties": {
                 "OBJECTID": 512,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 513,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22517232933961,
                     25.825045241045075
                 ]
             },
             "properties": {
                 "OBJECTID": 513,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 514,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22501025981569,
                     25.825215805565051
                 ]
             },
             "properties": {
                 "OBJECTID": 514,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 515,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22466677725004,
                     25.825610800398181
                 ]
             },
             "properties": {
                 "OBJECTID": 515,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 516,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22431902919988,
                     25.826040901166721
                 ]
             },
             "properties": {
                 "OBJECTID": 516,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 517,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22414544205856,
                     25.82625431208686
                 ]
             },
             "properties": {
                 "OBJECTID": 517,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 518,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22392759478271,
                     25.826580821747598
                 ]
             },
             "properties": {
                 "OBJECTID": 518,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 519,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22346610407749,
                     25.827813598019191
                 ]
             },
             "properties": {
                 "OBJECTID": 519,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 520,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22330082757122,
                     25.828185286921496
                 ]
             },
             "properties": {
                 "OBJECTID": 520,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 521,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22298001601501,
                     25.828714820332721
                 ]
             },
             "properties": {
                 "OBJECTID": 521,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 522,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22139445998641,
                     25.830438668709633
                 ]
             },
             "properties": {
                 "OBJECTID": 522,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 523,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21911123779347,
                     25.831452917817955
                 ]
             },
             "properties": {
                 "OBJECTID": 523,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 524,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21871414214399,
                     25.831576864180988
                 ]
             },
             "properties": {
                 "OBJECTID": 524,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 525,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21817245709326,
                     25.832233230779025
                 ]
             },
             "properties": {
                 "OBJECTID": 525,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 526,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2181235546584,
                     25.832584212791232
                 ]
             },
             "properties": {
                 "OBJECTID": 526,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 527,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21755735588732,
                     25.834824067876468
                 ]
             },
             "properties": {
                 "OBJECTID": 527,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 528,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21712051829735,
                     25.83579353614283
                 ]
             },
             "properties": {
                 "OBJECTID": 528,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 529,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21700244360795,
                     25.83594492801609
                 ]
             },
             "properties": {
                 "OBJECTID": 529,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 530,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21679601322546,
                     25.836086293347819
                 ]
             },
             "properties": {
                 "OBJECTID": 530,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 531,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21660942008873,
                     25.836176552006293
                 ]
             },
             "properties": {
                 "OBJECTID": 531,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 532,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21617642440253,
                     25.836200195182926
                 ]
             },
             "properties": {
                 "OBJECTID": 532,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 533,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21501997719025,
                     25.836113980775622
                 ]
             },
             "properties": {
                 "OBJECTID": 533,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 534,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2120283707128,
                     25.836470255298082
                 ]
             },
             "properties": {
                 "OBJECTID": 534,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 535,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21169419972762,
                     25.836772769247943
                 ]
             },
             "properties": {
                 "OBJECTID": 535,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 536,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21136557666011,
                     25.836976941432738
                 ]
             },
             "properties": {
                 "OBJECTID": 536,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 537,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2112300506264,
                     25.83718167929112
                 ]
             },
             "properties": {
                 "OBJECTID": 537,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 538,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21115387355246,
                     25.837235383206405
                 ]
             },
             "properties": {
                 "OBJECTID": 538,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 539,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2111084137224,
                     25.837341807178689
                 ]
             },
             "properties": {
                 "OBJECTID": 539,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 540,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21108325428884,
                     25.83756093508913
                 ]
             },
             "properties": {
                 "OBJECTID": 540,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 541,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21125753930403,
                     25.838128361434769
                 ]
             },
             "properties": {
                 "OBJECTID": 541,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 542,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21154288789404,
                     25.839177077959846
                 ]
             },
             "properties": {
                 "OBJECTID": 542,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 543,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21117165494798,
                     25.83926496690475
                 ]
             },
             "properties": {
                 "OBJECTID": 543,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 544,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21094634779575,
                     25.83932939073884
                 ]
             },
             "properties": {
                 "OBJECTID": 544,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 545,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21075050693042,
                     25.839436812958581
                 ]
             },
             "properties": {
                 "OBJECTID": 545,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 546,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2105935185761,
                     25.83972636677845
                 ]
             },
             "properties": {
                 "OBJECTID": 546,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 547,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21063429923356,
                     25.839960641970094
                 ]
             },
             "properties": {
                 "OBJECTID": 547,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 548,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21069952796091,
                     25.840069370005494
                 ]
             },
             "properties": {
                 "OBJECTID": 548,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 549,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21099750842831,
                     25.840233070899558
                 ]
             },
             "properties": {
                 "OBJECTID": 549,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 550,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21542560560727,
                     25.840443433117969
                 ]
             },
             "properties": {
                 "OBJECTID": 550,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 551,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2158587829565,
                     25.841236749376549
                 ]
             },
             "properties": {
                 "OBJECTID": 551,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 552,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21589860673538,
                     25.84187804243777
                 ]
             },
             "properties": {
                 "OBJECTID": 552,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 553,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21643291015073,
                     25.844149444841037
                 ]
             },
             "properties": {
                 "OBJECTID": 553,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 554,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21628309388973,
                     25.84448219309877
                 ]
             },
             "properties": {
                 "OBJECTID": 554,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 555,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21603567600573,
                     25.845013931647657
                 ]
             },
             "properties": {
                 "OBJECTID": 555,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 556,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21590495774768,
                     25.845616683363289
                 ]
             },
             "properties": {
                 "OBJECTID": 556,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 557,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21600821160973,
                     25.845744988740307
                 ]
             },
             "properties": {
                 "OBJECTID": 557,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 558,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21631891568546,
                     25.845813454127665
                 ]
             },
             "properties": {
                 "OBJECTID": 558,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 559,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21668924031627,
                     25.845797612569811
                 ]
             },
             "properties": {
                 "OBJECTID": 559,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 560,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21658840383185,
                     25.847487439593692
                 ]
             },
             "properties": {
                 "OBJECTID": 560,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 561,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21643768015485,
                     25.847587937033722
                 ]
             },
             "properties": {
                 "OBJECTID": 561,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 562,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2163061210307,
                     25.847696388077907
                 ]
             },
             "properties": {
                 "OBJECTID": 562,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 563,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21620334470867,
                     25.84782125534673
                 ]
             },
             "properties": {
                 "OBJECTID": 563,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 564,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21615711326024,
                     25.848051775868839
                 ]
             },
             "properties": {
                 "OBJECTID": 564,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 565,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21617743793848,
                     25.848206826184139
                 ]
             },
             "properties": {
                 "OBJECTID": 565,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 566,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21624593570141,
                     25.848304213768529
                 ]
             },
             "properties": {
                 "OBJECTID": 566,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 567,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21797219516071,
                     25.849294182980202
                 ]
             },
             "properties": {
                 "OBJECTID": 567,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 568,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2184622006701,
                     25.849709333419071
                 ]
             },
             "properties": {
                 "OBJECTID": 568,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 569,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21884579030103,
                     25.850165105335407
                 ]
             },
             "properties": {
                 "OBJECTID": 569,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 570,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21924462703799,
                     25.850663036569927
                 ]
             },
             "properties": {
                 "OBJECTID": 570,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 571,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21969504888744,
                     25.851222538089701
                 ]
             },
             "properties": {
                 "OBJECTID": 571,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 572,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2209230857319,
                     25.85328499200881
                 ]
             },
             "properties": {
                 "OBJECTID": 572,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 573,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21973044620319,
                     25.854400771878147
                 ]
             },
             "properties": {
                 "OBJECTID": 573,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 574,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21955036056073,
                     25.85434184380108
                 ]
             },
             "properties": {
                 "OBJECTID": 574,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 575,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21906714583218,
                     25.854123554958164
                 ]
             },
             "properties": {
                 "OBJECTID": 575,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 576,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21709969539467,
                     25.853476569188388
                 ]
             },
             "properties": {
                 "OBJECTID": 576,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 577,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21677441240928,
                     25.853441128705185
                 ]
             },
             "properties": {
                 "OBJECTID": 577,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 578,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21671967337431,
                     25.853438516174606
                 ]
             },
             "properties": {
                 "OBJECTID": 578,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 579,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21660142511575,
                     25.853431558119951
                 ]
             },
             "properties": {
                 "OBJECTID": 579,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 580,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21649333919629,
                     25.853452298284992
                 ]
             },
             "properties": {
                 "OBJECTID": 580,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 581,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21633029210938,
                     25.853529791067047
                 ]
             },
             "properties": {
                 "OBJECTID": 581,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 582,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21531012276404,
                     25.854745100705884
                 ]
             },
             "properties": {
                 "OBJECTID": 582,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 583,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2149067696318,
                     25.855157235717911
                 ]
             },
             "properties": {
                 "OBJECTID": 583,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 584,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2141540730521,
                     25.855764412997303
                 ]
             },
             "properties": {
                 "OBJECTID": 584,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 585,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21407396234355,
                     25.855861587442348
                 ]
             },
             "properties": {
                 "OBJECTID": 585,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 586,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21398343838501,
                     25.856019663976383
                 ]
             },
             "properties": {
                 "OBJECTID": 586,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 587,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21391956313738,
                     25.856273417584191
                 ]
             },
             "properties": {
                 "OBJECTID": 587,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 588,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21390858601251,
                     25.856698633235396
                 ]
             },
             "properties": {
                 "OBJECTID": 588,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 589,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21377812855786,
                     25.857668587135663
                 ]
             },
             "properties": {
                 "OBJECTID": 589,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 590,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21362676006697,
                     25.857997357691943
                 ]
             },
             "properties": {
                 "OBJECTID": 590,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 591,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21360953355321,
                     25.859308392068669
                 ]
             },
             "properties": {
                 "OBJECTID": 591,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 592,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21379414728216,
                     25.859619853373601
                 ]
             },
             "properties": {
                 "OBJECTID": 592,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 593,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21425150020661,
                     25.860272753984475
                 ]
             },
             "properties": {
                 "OBJECTID": 593,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 594,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21564436029905,
                     25.861562781097518
                 ]
             },
             "properties": {
                 "OBJECTID": 594,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 595,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21607576598387,
                     25.862000285984436
                 ]
             },
             "properties": {
                 "OBJECTID": 595,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 596,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21627795066695,
                     25.862505527807912
                 ]
             },
             "properties": {
                 "OBJECTID": 596,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 597,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21632298601708,
                     25.863104786556733
                 ]
             },
             "properties": {
                 "OBJECTID": 597,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 598,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21618237341784,
                     25.864306155804513
                 ]
             },
             "properties": {
                 "OBJECTID": 598,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 599,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21594483188852,
                     25.86492015624026
                 ]
             },
             "properties": {
                 "OBJECTID": 599,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 600,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21389467529906,
                     25.867324725755566
                 ]
             },
             "properties": {
                 "OBJECTID": 600,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 601,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21330417414839,
                     25.866993893953122
                 ]
             },
             "properties": {
                 "OBJECTID": 601,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 602,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21300180678799,
                     25.866951881224395
                 ]
             },
             "properties": {
                 "OBJECTID": 602,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 603,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21277024125368,
                     25.867111438941777
                 ]
             },
             "properties": {
                 "OBJECTID": 603,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 604,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21270884633623,
                     25.867244210352055
                 ]
             },
             "properties": {
                 "OBJECTID": 604,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 605,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21267086976491,
                     25.867423390377212
                 ]
             },
             "properties": {
                 "OBJECTID": 605,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 606,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21254498086626,
                     25.868247696374112
                 ]
             },
             "properties": {
                 "OBJECTID": 606,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 607,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21241046926781,
                     25.868752002003362
                 ]
             },
             "properties": {
                 "OBJECTID": 607,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 608,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21164515430047,
                     25.869836686913629
                 ]
             },
             "properties": {
                 "OBJECTID": 608,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 609,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21145105002597,
                     25.87015271317722
                 ]
             },
             "properties": {
                 "OBJECTID": 609,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 610,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2113745384043,
                     25.870964905306209
                 ]
             },
             "properties": {
                 "OBJECTID": 610,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 611,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21144233109874,
                     25.871195406043228
                 ]
             },
             "properties": {
                 "OBJECTID": 611,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 612,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21157335692493,
                     25.871550229059892
                 ]
             },
             "properties": {
                 "OBJECTID": 612,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 613,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21034581830492,
                     25.874870416336705
                 ]
             },
             "properties": {
                 "OBJECTID": 613,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 614,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20965855909526,
                     25.875538180942328
                 ]
             },
             "properties": {
                 "OBJECTID": 614,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 615,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20854939373959,
                     25.877547046059362
                 ]
             },
             "properties": {
                 "OBJECTID": 615,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 616,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20841639570017,
                     25.878433827059496
                 ]
             },
             "properties": {
                 "OBJECTID": 616,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 617,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20849545600055,
                     25.878631888351038
                 ]
             },
             "properties": {
                 "OBJECTID": 617,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 618,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20859927014027,
                     25.878832783406267
                 ]
             },
             "properties": {
                 "OBJECTID": 618,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 619,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20903237104716,
                     25.879144571165057
                 ]
             },
             "properties": {
                 "OBJECTID": 619,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 620,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20941454784315,
                     25.879352893721091
                 ]
             },
             "properties": {
                 "OBJECTID": 620,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 621,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21019214305301,
                     25.879852435641453
                 ]
             },
             "properties": {
                 "OBJECTID": 621,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 622,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21170961410746,
                     25.880951173358028
                 ]
             },
             "properties": {
                 "OBJECTID": 622,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 623,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21217454471952,
                     25.881187323636141
                 ]
             },
             "properties": {
                 "OBJECTID": 623,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 624,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21262883365381,
                     25.881428476842757
                 ]
             },
             "properties": {
                 "OBJECTID": 624,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 625,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21300763079756,
                     25.881823961806447
                 ]
             },
             "properties": {
                 "OBJECTID": 625,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 626,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21311533720313,
                     25.882285689932701
                 ]
             },
             "properties": {
                 "OBJECTID": 626,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 627,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21317566372596,
                     25.883269048228044
                 ]
             },
             "properties": {
                 "OBJECTID": 627,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 628,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21279429272249,
                     25.88364664028029
                 ]
             },
             "properties": {
                 "OBJECTID": 628,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 629,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21265294717585,
                     25.883812736968309
                 ]
             },
             "properties": {
                 "OBJECTID": 629,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 630,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21262486134833,
                     25.883978130386481
                 ]
             },
             "properties": {
                 "OBJECTID": 630,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 631,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21275051822192,
                     25.88436466889442
                 ]
             },
             "properties": {
                 "OBJECTID": 631,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 632,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.212862641198,
                     25.884553377236614
                 ]
             },
             "properties": {
                 "OBJECTID": 632,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 633,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21304709934424,
                     25.884685761938385
                 ]
             },
             "properties": {
                 "OBJECTID": 633,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 634,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21334197535197,
                     25.884806978859046
                 ]
             },
             "properties": {
                 "OBJECTID": 634,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 635,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21461907740854,
                     25.885640339230349
                 ]
             },
             "properties": {
                 "OBJECTID": 635,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 636,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21483797329387,
                     25.886115129109953
                 ]
             },
             "properties": {
                 "OBJECTID": 636,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 637,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21522615306458,
                     25.887026547936898
                 ]
             },
             "properties": {
                 "OBJECTID": 637,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 638,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21525515979783,
                     25.887501332420584
                 ]
             },
             "properties": {
                 "OBJECTID": 638,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 639,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21525184669542,
                     25.888015026971971
                 ]
             },
             "properties": {
                 "OBJECTID": 639,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 640,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21531932013056,
                     25.888331857229502
                 ]
             },
             "properties": {
                 "OBJECTID": 640,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 641,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21541030364267,
                     25.888492902425583
                 ]
             },
             "properties": {
                 "OBJECTID": 641,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 642,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21551901279224,
                     25.888584779863777
                 ]
             },
             "properties": {
                 "OBJECTID": 642,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 643,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21565240833206,
                     25.888656859626394
                 ]
             },
             "properties": {
                 "OBJECTID": 643,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 644,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21588143238228,
                     25.888726447367731
                 ]
             },
             "properties": {
                 "OBJECTID": 644,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 645,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21663208390362,
                     25.888936216033585
                 ]
             },
             "properties": {
                 "OBJECTID": 645,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 646,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21687982014765,
                     25.889156120058942
                 ]
             },
             "properties": {
                 "OBJECTID": 646,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 647,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21728856921214,
                     25.889424333865975
                 ]
             },
             "properties": {
                 "OBJECTID": 647,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 648,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21736639024675,
                     25.889489784725811
                 ]
             },
             "properties": {
                 "OBJECTID": 648,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 649,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21750892289856,
                     25.889824718336115
                 ]
             },
             "properties": {
                 "OBJECTID": 649,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 650,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2175273149337,
                     25.890326271140566
                 ]
             },
             "properties": {
                 "OBJECTID": 650,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 651,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21757289257499,
                     25.890845512710314
                 ]
             },
             "properties": {
                 "OBJECTID": 651,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 652,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21736238466639,
                     25.891794444957725
                 ]
             },
             "properties": {
                 "OBJECTID": 652,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 653,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2169396574393,
                     25.892717482126159
                 ]
             },
             "properties": {
                 "OBJECTID": 653,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 654,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21643143076597,
                     25.893550745370646
                 ]
             },
             "properties": {
                 "OBJECTID": 654,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 655,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21607383963607,
                     25.893916516733896
                 ]
             },
             "properties": {
                 "OBJECTID": 655,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 656,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21560030700863,
                     25.894185674829089
                 ]
             },
             "properties": {
                 "OBJECTID": 656,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 657,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21494828593472,
                     25.894461734321624
                 ]
             },
             "properties": {
                 "OBJECTID": 657,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 658,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21366757130141,
                     25.896355241601157
                 ]
             },
             "properties": {
                 "OBJECTID": 658,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 659,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21355703562875,
                     25.896745874722399
                 ]
             },
             "properties": {
                 "OBJECTID": 659,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 660,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21353301203891,
                     25.896923542987167
                 ]
             },
             "properties": {
                 "OBJECTID": 660,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 661,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21353694297562,
                     25.897547673386441
                 ]
             },
             "properties": {
                 "OBJECTID": 661,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 662,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21340387658768,
                     25.897886750173427
                 ]
             },
             "properties": {
                 "OBJECTID": 662,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 663,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21080955102002,
                     25.899639236561541
                 ]
             },
             "properties": {
                 "OBJECTID": 663,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 664,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21018982280214,
                     25.899979201878693
                 ]
             },
             "properties": {
                 "OBJECTID": 664,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 665,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2100088774078,
                     25.900177843232939
                 ]
             },
             "properties": {
                 "OBJECTID": 665,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 666,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20982435630907,
                     25.900773253782688
                 ]
             },
             "properties": {
                 "OBJECTID": 666,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 667,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20912807959724,
                     25.902007862872608
                 ]
             },
             "properties": {
                 "OBJECTID": 667,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 668,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20864544133411,
                     25.902299806191138
                 ]
             },
             "properties": {
                 "OBJECTID": 668,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 669,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20819219561338,
                     25.90252711613357
                 ]
             },
             "properties": {
                 "OBJECTID": 669,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 670,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2076397753554,
                     25.902746626255862
                 ]
             },
             "properties": {
                 "OBJECTID": 670,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 671,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20717878557258,
                     25.902930061872894
                 ]
             },
             "properties": {
                 "OBJECTID": 671,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 672,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20571440419644,
                     25.903482164670208
                 ]
             },
             "properties": {
                 "OBJECTID": 672,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 673,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20462118112266,
                     25.903775374234158
                 ]
             },
             "properties": {
                 "OBJECTID": 673,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 674,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20305415742348,
                     25.904105077387442
                 ]
             },
             "properties": {
                 "OBJECTID": 674,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 675,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20249036793621,
                     25.904141162684539
                 ]
             },
             "properties": {
                 "OBJECTID": 675,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 676,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20154767968171,
                     25.904113852072669
                 ]
             },
             "properties": {
                 "OBJECTID": 676,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 677,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20152259938851,
                     25.904014914057086
                 ]
             },
             "properties": {
                 "OBJECTID": 677,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 678,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2014505070353,
                     25.904029266337602
                 ]
             },
             "properties": {
                 "OBJECTID": 678,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 679,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20126247498331,
                     25.903852643085088
                 ]
             },
             "properties": {
                 "OBJECTID": 679,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 680,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20111744761221,
                     25.903897929345987
                 ]
             },
             "properties": {
                 "OBJECTID": 680,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 681,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20082899186457,
                     25.904039917008618
                 ]
             },
             "properties": {
                 "OBJECTID": 681,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 682,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20033218388323,
                     25.9040295379329
                 ]
             },
             "properties": {
                 "OBJECTID": 682,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 683,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20009632588477,
                     25.904037515818743
                 ]
             },
             "properties": {
                 "OBJECTID": 683,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 684,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1999857335548,
                     25.904155310818965
                 ]
             },
             "properties": {
                 "OBJECTID": 684,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 685,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19991140818502,
                     25.904176599570462
                 ]
             },
             "properties": {
                 "OBJECTID": 685,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 686,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19951197070094,
                     25.904246387860553
                 ]
             },
             "properties": {
                 "OBJECTID": 686,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 687,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19937871185812,
                     25.904285799749914
                 ]
             },
             "properties": {
                 "OBJECTID": 687,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 688,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19929015561627,
                     25.904363021836048
                 ]
             },
             "properties": {
                 "OBJECTID": 688,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 689,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19902756796796,
                     25.904324824930768
                 ]
             },
             "properties": {
                 "OBJECTID": 689,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 690,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19788973582268,
                     25.903398340660942
                 ]
             },
             "properties": {
                 "OBJECTID": 690,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 691,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19800096487268,
                     25.903308868909221
                 ]
             },
             "properties": {
                 "OBJECTID": 691,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 692,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19811933633838,
                     25.903342276924604
                 ]
             },
             "properties": {
                 "OBJECTID": 692,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 693,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19813438919078,
                     25.903181470948198
                 ]
             },
             "properties": {
                 "OBJECTID": 693,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 694,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19837372576688,
                     25.902889275819462
                 ]
             },
             "properties": {
                 "OBJECTID": 694,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 695,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19839900301162,
                     25.902826491449446
                 ]
             },
             "properties": {
                 "OBJECTID": 695,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 696,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19842512741775,
                     25.902756781400399
                 ]
             },
             "properties": {
                 "OBJECTID": 696,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 697,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19836188619217,
                     25.902734921579395
                 ]
             },
             "properties": {
                 "OBJECTID": 697,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 698,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19841582033388,
                     25.902419837805326
                 ]
             },
             "properties": {
                 "OBJECTID": 698,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 699,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19847843383269,
                     25.90226453298186
                 ]
             },
             "properties": {
                 "OBJECTID": 699,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 700,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1984537447446,
                     25.902255618002414
                 ]
             },
             "properties": {
                 "OBJECTID": 700,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 701,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19833845795353,
                     25.902023968831145
                 ]
             },
             "properties": {
                 "OBJECTID": 701,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 702,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19825827529922,
                     25.901996917223983
                 ]
             },
             "properties": {
                 "OBJECTID": 702,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 703,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19810873333142,
                     25.902019195229741
                 ]
             },
             "properties": {
                 "OBJECTID": 703,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 704,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19804250455798,
                     25.902042769158527
                 ]
             },
             "properties": {
                 "OBJECTID": 704,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 705,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19797645834694,
                     25.901996261618251
                 ]
             },
             "properties": {
                 "OBJECTID": 705,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 706,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19786515195528,
                     25.901997927162654
                 ]
             },
             "properties": {
                 "OBJECTID": 706,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 707,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19776754943291,
                     25.902034649179768
                 ]
             },
             "properties": {
                 "OBJECTID": 707,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 708,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19768228920634,
                     25.902119827568015
                 ]
             },
             "properties": {
                 "OBJECTID": 708,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 709,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19752889903953,
                     25.902018017117825
                 ]
             },
             "properties": {
                 "OBJECTID": 709,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 710,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19723965098916,
                     25.902306897345511
                 ]
             },
             "properties": {
                 "OBJECTID": 710,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 711,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19736147765013,
                     25.902419966408388
                 ]
             },
             "properties": {
                 "OBJECTID": 711,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 712,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1971522539734,
                     25.902548421072822
                 ]
             },
             "properties": {
                 "OBJECTID": 712,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 713,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19709633682652,
                     25.902485848043511
                 ]
             },
             "properties": {
                 "OBJECTID": 713,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 714,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19695391029478,
                     25.902567234890057
                 ]
             },
             "properties": {
                 "OBJECTID": 714,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 715,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19695791857316,
                     25.90259271178428
                 ]
             },
             "properties": {
                 "OBJECTID": 715,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 716,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19657808541035,
                     25.902739528806251
                 ]
             },
             "properties": {
                 "OBJECTID": 716,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 717,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19660228976392,
                     25.902779740192898
                 ]
             },
             "properties": {
                 "OBJECTID": 717,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 718,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19601995086009,
                     25.902958011902797
                 ]
             },
             "properties": {
                 "OBJECTID": 718,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 719,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19600500502702,
                     25.902922415836883
                 ]
             },
             "properties": {
                 "OBJECTID": 719,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 720,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19568033807718,
                     25.903023005007753
                 ]
             },
             "properties": {
                 "OBJECTID": 720,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 721,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19568033807718,
                     25.903033938965166
                 ]
             },
             "properties": {
                 "OBJECTID": 721,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 722,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19571135929186,
                     25.903146481025317
                 ]
             },
             "properties": {
                 "OBJECTID": 722,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 723,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19544930044492,
                     25.903123763251131
                 ]
             },
             "properties": {
                 "OBJECTID": 723,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 724,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19520779920066,
                     25.903343912791399
                 ]
             },
             "properties": {
                 "OBJECTID": 724,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 725,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19536924459504,
                     25.903465142302537
                 ]
             },
             "properties": {
                 "OBJECTID": 725,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 726,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19507422109848,
                     25.903492264056808
                 ]
             },
             "properties": {
                 "OBJECTID": 726,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 727,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19513409975895,
                     25.904006616012566
                 ]
             },
             "properties": {
                 "OBJECTID": 727,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 728,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19497733983241,
                     25.90430720901054
                 ]
             },
             "properties": {
                 "OBJECTID": 728,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 729,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19508150830501,
                     25.904539440942472
                 ]
             },
             "properties": {
                 "OBJECTID": 729,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 730,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19509832023135,
                     25.904617043441874
                 ]
             },
             "properties": {
                 "OBJECTID": 730,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 731,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1949151939811,
                     25.905138678107562
                 ]
             },
             "properties": {
                 "OBJECTID": 731,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 732,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19508433397488,
                     25.905270037582227
                 ]
             },
             "properties": {
                 "OBJECTID": 732,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 733,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19520855283253,
                     25.905494743087957
                 ]
             },
             "properties": {
                 "OBJECTID": 733,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 734,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19529364578517,
                     25.905468410039134
                 ]
             },
             "properties": {
                 "OBJECTID": 734,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 735,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19531809205637,
                     25.905534360922047
                 ]
             },
             "properties": {
                 "OBJECTID": 735,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 736,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19462379385305,
                     25.905681386586764
                 ]
             },
             "properties": {
                 "OBJECTID": 736,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 737,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19464930132426,
                     25.905606336363405
                 ]
             },
             "properties": {
                 "OBJECTID": 737,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 738,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19432994487113,
                     25.905294367840838
                 ]
             },
             "properties": {
                 "OBJECTID": 738,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 739,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19411273701326,
                     25.905087875405172
                 ]
             },
             "properties": {
                 "OBJECTID": 739,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 740,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19405431615382,
                     25.905060251829184
                 ]
             },
             "properties": {
                 "OBJECTID": 740,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 741,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19403701229834,
                     25.90490473836303
                 ]
             },
             "properties": {
                 "OBJECTID": 741,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 742,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19410205396667,
                     25.904887949819056
                 ]
             },
             "properties": {
                 "OBJECTID": 742,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 743,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19409777499237,
                     25.904837251438039
                 ]
             },
             "properties": {
                 "OBJECTID": 743,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 744,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19410411970944,
                     25.90481034821903
                 ]
             },
             "properties": {
                 "OBJECTID": 744,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 745,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19409926157175,
                     25.904789481249622
                 ]
             },
             "properties": {
                 "OBJECTID": 745,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 746,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19406140191222,
                     25.904569704928008
                 ]
             },
             "properties": {
                 "OBJECTID": 746,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 747,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19402234615438,
                     25.904576566755225
                 ]
             },
             "properties": {
                 "OBJECTID": 747,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 748,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19398046023014,
                     25.90444658594123
                 ]
             },
             "properties": {
                 "OBJECTID": 748,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 749,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19394125518482,
                     25.903990010930329
                 ]
             },
             "properties": {
                 "OBJECTID": 749,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 750,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19383223127249,
                     25.903863361205197
                 ]
             },
             "properties": {
                 "OBJECTID": 750,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 751,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19342502904192,
                     25.903891688950353
                 ]
             },
             "properties": {
                 "OBJECTID": 751,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 752,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19324614219579,
                     25.9039322195963
                 ]
             },
             "properties": {
                 "OBJECTID": 752,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 753,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19318854961193,
                     25.903988584605543
                 ]
             },
             "properties": {
                 "OBJECTID": 753,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 754,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.193020721729,
                     25.904319455978168
                 ]
             },
             "properties": {
                 "OBJECTID": 754,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 755,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19302184678088,
                     25.904520726050691
                 ]
             },
             "properties": {
                 "OBJECTID": 755,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 756,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19313635655766,
                     25.904838388181133
                 ]
             },
             "properties": {
                 "OBJECTID": 756,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 757,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19302584966329,
                     25.90504100094148
                 ]
             },
             "properties": {
                 "OBJECTID": 757,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 758,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19309516041341,
                     25.905290549319432
                 ]
             },
             "properties": {
                 "OBJECTID": 758,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 759,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19324051513775,
                     25.905587944328204
                 ]
             },
             "properties": {
                 "OBJECTID": 759,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 760,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19331223517156,
                     25.90571957180083
                 ]
             },
             "properties": {
                 "OBJECTID": 760,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 761,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19337610412396,
                     25.905951595989393
                 ]
             },
             "properties": {
                 "OBJECTID": 761,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 762,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19337991455149,
                     25.906084680363733
                 ]
             },
             "properties": {
                 "OBJECTID": 762,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 763,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19332564136533,
                     25.906385616902753
                 ]
             },
             "properties": {
                 "OBJECTID": 763,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 764,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19323543756548,
                     25.906611478936668
                 ]
             },
             "properties": {
                 "OBJECTID": 764,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 765,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19321569744653,
                     25.906634991711542
                 ]
             },
             "properties": {
                 "OBJECTID": 765,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 766,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19311261715364,
                     25.906720010919798
                 ]
             },
             "properties": {
                 "OBJECTID": 766,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 767,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19306275694072,
                     25.906778106224692
                 ]
             },
             "properties": {
                 "OBJECTID": 767,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 768,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1930307203915,
                     25.90682727395955
                 ]
             },
             "properties": {
                 "OBJECTID": 768,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 769,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19301231936311,
                     25.906905451125738
                 ]
             },
             "properties": {
                 "OBJECTID": 769,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 770,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19301086426003,
                     25.906947382016142
                 ]
             },
             "properties": {
                 "OBJECTID": 770,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 771,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1930750263914,
                     25.907428759429763
                 ]
             },
             "properties": {
                 "OBJECTID": 771,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 772,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19309654626863,
                     25.90750423683113
                 ]
             },
             "properties": {
                 "OBJECTID": 772,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 773,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19316817457167,
                     25.90772384767746
                 ]
             },
             "properties": {
                 "OBJECTID": 773,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 774,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19314268059031,
                     25.907734930023025
                 ]
             },
             "properties": {
                 "OBJECTID": 774,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 775,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19313185994741,
                     25.907784432305732
                 ]
             },
             "properties": {
                 "OBJECTID": 775,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 776,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19284334934116,
                     25.908301035264628
                 ]
             },
             "properties": {
                 "OBJECTID": 776,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 777,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1932716991297,
                     25.90854205717028
                 ]
             },
             "properties": {
                 "OBJECTID": 777,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 778,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19254446865239,
                     25.9091189102096
                 ]
             },
             "properties": {
                 "OBJECTID": 778,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 779,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19173944861672,
                     25.909734218259644
                 ]
             },
             "properties": {
                 "OBJECTID": 779,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 780,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19172011319273,
                     25.909754104068725
                 ]
             },
             "properties": {
                 "OBJECTID": 780,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 781,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.191633827739,
                     25.909708985081636
                 ]
             },
             "properties": {
                 "OBJECTID": 781,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 782,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19160367706809,
                     25.909690906909873
                 ]
             },
             "properties": {
                 "OBJECTID": 782,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 783,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19159009820453,
                     25.90943385189081
                 ]
             },
             "properties": {
                 "OBJECTID": 783,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 784,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19111627419676,
                     25.90914716421031
                 ]
             },
             "properties": {
                 "OBJECTID": 784,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 785,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19117083966256,
                     25.908988426674625
                 ]
             },
             "properties": {
                 "OBJECTID": 785,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 786,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19122176467278,
                     25.908789701683418
                 ]
             },
             "properties": {
                 "OBJECTID": 786,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 787,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19099383419814,
                     25.908786358903399
                 ]
             },
             "properties": {
                 "OBJECTID": 787,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 788,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19069837632907,
                     25.908754594848745
                 ]
             },
             "properties": {
                 "OBJECTID": 788,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 789,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19069965156774,
                     25.908713270101373
                 ]
             },
             "properties": {
                 "OBJECTID": 789,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 790,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18999983962095,
                     25.908636214389844
                 ]
             },
             "properties": {
                 "OBJECTID": 790,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 791,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18999101457371,
                     25.908676843961246
                 ]
             },
             "properties": {
                 "OBJECTID": 791,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 792,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18980629292616,
                     25.908648152890009
                 ]
             },
             "properties": {
                 "OBJECTID": 792,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 793,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18978131425632,
                     25.908657966292139
                 ]
             },
             "properties": {
                 "OBJECTID": 793,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 794,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1897453827433,
                     25.908678878227704
                 ]
             },
             "properties": {
                 "OBJECTID": 794,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 795,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18971173640756,
                     25.90869332044042
                 ]
             },
             "properties": {
                 "OBJECTID": 795,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 796,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1896219373026,
                     25.908749000166267
                 ]
             },
             "properties": {
                 "OBJECTID": 796,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 797,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18961428317272,
                     25.908753970719204
                 ]
             },
             "properties": {
                 "OBJECTID": 797,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 798,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18960067912809,
                     25.908775994216796
                 ]
             },
             "properties": {
                 "OBJECTID": 798,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 799,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18955464463119,
                     25.908839112235285
                 ]
             },
             "properties": {
                 "OBJECTID": 799,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 800,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18953171821431,
                     25.908841821892622
                 ]
             },
             "properties": {
                 "OBJECTID": 800,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 801,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18944198565919,
                     25.909029577852777
                 ]
             },
             "properties": {
                 "OBJECTID": 801,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 802,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18939375681646,
                     25.909019619659773
                 ]
             },
             "properties": {
                 "OBJECTID": 802,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 803,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18936622137409,
                     25.909010258616604
                 ]
             },
             "properties": {
                 "OBJECTID": 803,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 804,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18935871473298,
                     25.909011290139006
                 ]
             },
             "properties": {
                 "OBJECTID": 804,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 805,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18934150170901,
                     25.909011601304428
                 ]
             },
             "properties": {
                 "OBJECTID": 805,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 806,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18932524286572,
                     25.909011046422677
                 ]
             },
             "properties": {
                 "OBJECTID": 806,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 807,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18924907208708,
                     25.909019789631657
                 ]
             },
             "properties": {
                 "OBJECTID": 807,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 808,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1889928750212,
                     25.909054381154817
                 ]
             },
             "properties": {
                 "OBJECTID": 808,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 809,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18892430171519,
                     25.90905653682978
                 ]
             },
             "properties": {
                 "OBJECTID": 809,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 810,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18889609627786,
                     25.909047966290643
                 ]
             },
             "properties": {
                 "OBJECTID": 810,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 811,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18883851988187,
                     25.909018774297067
                 ]
             },
             "properties": {
                 "OBJECTID": 811,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 812,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18876553809918,
                     25.909080070289065
                 ]
             },
             "properties": {
                 "OBJECTID": 812,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 813,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18875783270789,
                     25.909120474130646
                 ]
             },
             "properties": {
                 "OBJECTID": 813,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 814,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18892714087491,
                     25.909282702834503
                 ]
             },
             "properties": {
                 "OBJECTID": 814,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 815,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18891747406224,
                     25.909313449755928
                 ]
             },
             "properties": {
                 "OBJECTID": 815,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 816,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18881065888485,
                     25.909429867894119
                 ]
             },
             "properties": {
                 "OBJECTID": 816,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 817,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18881734174698,
                     25.90943385189081
                 ]
             },
             "properties": {
                 "OBJECTID": 817,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 818,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18894131598898,
                     25.909482042062621
                 ]
             },
             "properties": {
                 "OBJECTID": 818,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 819,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18895296760542,
                     25.909503488195469
                 ]
             },
             "properties": {
                 "OBJECTID": 819,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 820,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18896899442359,
                     25.909509400338607
                 ]
             },
             "properties": {
                 "OBJECTID": 820,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 821,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18898155255664,
                     25.909513343865797
                 ]
             },
             "properties": {
                 "OBJECTID": 821,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 822,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18914229288259,
                     25.909579221004321
                 ]
             },
             "properties": {
                 "OBJECTID": 822,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 823,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18916716273441,
                     25.909611396049115
                 ]
             },
             "properties": {
                 "OBJECTID": 823,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 824,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18913736639638,
                     25.909680448693791
                 ]
             },
             "properties": {
                 "OBJECTID": 824,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 825,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18915318816914,
                     25.90976949596552
                 ]
             },
             "properties": {
                 "OBJECTID": 825,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 826,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18933031594139,
                     25.909899928239156
                 ]
             },
             "properties": {
                 "OBJECTID": 826,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 827,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18940366734546,
                     25.909832193101295
                 ]
             },
             "properties": {
                 "OBJECTID": 827,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 828,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18969859011787,
                     25.909930527671747
                 ]
             },
             "properties": {
                 "OBJECTID": 828,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 829,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19002698655623,
                     25.909644714132298
                 ]
             },
             "properties": {
                 "OBJECTID": 829,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 830,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1900526316237,
                     25.909618632893626
                 ]
             },
             "properties": {
                 "OBJECTID": 830,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 831,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19029745406431,
                     25.909527143063258
                 ]
             },
             "properties": {
                 "OBJECTID": 831,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 832,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19053662876246,
                     25.90951699241532
                 ]
             },
             "properties": {
                 "OBJECTID": 832,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 833,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1905552923929,
                     25.909442167022462
                 ]
             },
             "properties": {
                 "OBJECTID": 833,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 834,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1906220023036,
                     25.90945254519886
                 ]
             },
             "properties": {
                 "OBJECTID": 834,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 835,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19102749582231,
                     25.909698301135734
                 ]
             },
             "properties": {
                 "OBJECTID": 835,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 836,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19123621767739,
                     25.909737840728837
                 ]
             },
             "properties": {
                 "OBJECTID": 836,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 837,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19143627636311,
                     25.909830603999239
                 ]
             },
             "properties": {
                 "OBJECTID": 837,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 838,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19148572468652,
                     25.909854886593791
                 ]
             },
             "properties": {
                 "OBJECTID": 838,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 839,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19157802030946,
                     25.909898729442887
                 ]
             },
             "properties": {
                 "OBJECTID": 839,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 840,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19155620365592,
                     25.910098272817095
                 ]
             },
             "properties": {
                 "OBJECTID": 840,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 841,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.191900929984,
                     25.910438512427447
                 ]
             },
             "properties": {
                 "OBJECTID": 841,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 842,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19190688799256,
                     25.910749193120807
                 ]
             },
             "properties": {
                 "OBJECTID": 842,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 843,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1919075256119,
                     25.910862452839979
                 ]
             },
             "properties": {
                 "OBJECTID": 843,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 844,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19191189811568,
                     25.911036853867756
                 ]
             },
             "properties": {
                 "OBJECTID": 844,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 845,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19195567891154,
                     25.911223525245532
                 ]
             },
             "properties": {
                 "OBJECTID": 845,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 846,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19200232674604,
                     25.91122427258216
                 ]
             },
             "properties": {
                 "OBJECTID": 846,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 847,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19199052044621,
                     25.911503045329312
                 ]
             },
             "properties": {
                 "OBJECTID": 847,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 848,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19198706615026,
                     25.911564569749146
                 ]
             },
             "properties": {
                 "OBJECTID": 848,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 849,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19173097430507,
                     25.911796697359762
                 ]
             },
             "properties": {
                 "OBJECTID": 849,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 850,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19190181671553,
                     25.912129430329003
                 ]
             },
             "properties": {
                 "OBJECTID": 850,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 851,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19196987381071,
                     25.912522821670962
                 ]
             },
             "properties": {
                 "OBJECTID": 851,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 852,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19172278777648,
                     25.912558565225652
                 ]
             },
             "properties": {
                 "OBJECTID": 852,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 853,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19180492106034,
                     25.913058984884287
                 ]
             },
             "properties": {
                 "OBJECTID": 853,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 854,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1921101923316,
                     25.913016196939964
                 ]
             },
             "properties": {
                 "OBJECTID": 854,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 855,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19204599332801,
                     25.913570871798356
                 ]
             },
             "properties": {
                 "OBJECTID": 855,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 856,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19203791291943,
                     25.913738233832476
                 ]
             },
             "properties": {
                 "OBJECTID": 856,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 857,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19180242634098,
                     25.913901643346207
                 ]
             },
             "properties": {
                 "OBJECTID": 857,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 858,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19193226865934,
                     25.914135473373449
                 ]
             },
             "properties": {
                 "OBJECTID": 858,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 859,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19225850042949,
                     25.914279930574367
                 ]
             },
             "properties": {
                 "OBJECTID": 859,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 860,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19224734164158,
                     25.91432663956283
                 ]
             },
             "properties": {
                 "OBJECTID": 860,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 861,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19228631466177,
                     25.914398451327543
                 ]
             },
             "properties": {
                 "OBJECTID": 861,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 862,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1919254194226,
                     25.915367065237945
                 ]
             },
             "properties": {
                 "OBJECTID": 862,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 863,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19159070524688,
                     25.91515309134212
                 ]
             },
             "properties": {
                 "OBJECTID": 863,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 864,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19158089813999,
                     25.915168654110175
                 ]
             },
             "properties": {
                 "OBJECTID": 864,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 865,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19147594905587,
                     25.915126585623455
                 ]
             },
             "properties": {
                 "OBJECTID": 865,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 866,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19130202376942,
                     25.915597678390213
                 ]
             },
             "properties": {
                 "OBJECTID": 866,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 867,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19110525930034,
                     25.915850320435993
                 ]
             },
             "properties": {
                 "OBJECTID": 867,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 868,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19102011238834,
                     25.915833152378127
                 ]
             },
             "properties": {
                 "OBJECTID": 868,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 869,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19078875369809,
                     25.915899972006173
                 ]
             },
             "properties": {
                 "OBJECTID": 869,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 870,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19050593400169,
                     25.916091784808089
                 ]
             },
             "properties": {
                 "OBJECTID": 870,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 871,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19060647910578,
                     25.916140362587726
                 ]
             },
             "properties": {
                 "OBJECTID": 871,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 872,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19052713012303,
                     25.916250608678695
                 ]
             },
             "properties": {
                 "OBJECTID": 872,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 873,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19041217967839,
                     25.916553593873289
                 ]
             },
             "properties": {
                 "OBJECTID": 873,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 874,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1892792569322,
                     25.916589142275143
                 ]
             },
             "properties": {
                 "OBJECTID": 874,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 875,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18916256270217,
                     25.916810821562194
                 ]
             },
             "properties": {
                 "OBJECTID": 875,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 876,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18885758730789,
                     25.916911561819177
                 ]
             },
             "properties": {
                 "OBJECTID": 876,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 877,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18881714569477,
                     25.917157911308607
                 ]
             },
             "properties": {
                 "OBJECTID": 877,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 878,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18875041509966,
                     25.917414013046312
                 ]
             },
             "properties": {
                 "OBJECTID": 878,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 879,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18872298128065,
                     25.917508993145589
                 ]
             },
             "properties": {
                 "OBJECTID": 879,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 880,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18871322093844,
                     25.917540437041566
                 ]
             },
             "properties": {
                 "OBJECTID": 880,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 881,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18883188198583,
                     25.917571712764357
                 ]
             },
             "properties": {
                 "OBJECTID": 881,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 882,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18881145658349,
                     25.917631305440466
                 ]
             },
             "properties": {
                 "OBJECTID": 882,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 883,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18878955629305,
                     25.917717756369427
                 ]
             },
             "properties": {
                 "OBJECTID": 883,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 884,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18908368226619,
                     25.918010115174752
                 ]
             },
             "properties": {
                 "OBJECTID": 884,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 885,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18907022391176,
                     25.918236488023581
                 ]
             },
             "properties": {
                 "OBJECTID": 885,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 886,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18931307503846,
                     25.918806716656718
                 ]
             },
             "properties": {
                 "OBJECTID": 886,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 887,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18945334679455,
                     25.918955498697358
                 ]
             },
             "properties": {
                 "OBJECTID": 887,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 888,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1893286243166,
                     25.919484030263845
                 ]
             },
             "properties": {
                 "OBJECTID": 888,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 889,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1889583716316,
                     25.919373892091471
                 ]
             },
             "properties": {
                 "OBJECTID": 889,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 890,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18887617899253,
                     25.919582253318367
                 ]
             },
             "properties": {
                 "OBJECTID": 890,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 891,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18912048342361,
                     25.91972150794129
                 ]
             },
             "properties": {
                 "OBJECTID": 891,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 892,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18909458204939,
                     25.919773031000659
                 ]
             },
             "properties": {
                 "OBJECTID": 892,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 893,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1889811550563,
                     25.919896696775197
                 ]
             },
             "properties": {
                 "OBJECTID": 893,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 894,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18886128172284,
                     25.920363334300475
                 ]
             },
             "properties": {
                 "OBJECTID": 894,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 895,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18884260280396,
                     25.920595434032123
                 ]
             },
             "properties": {
                 "OBJECTID": 895,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 896,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18783526408623,
                     25.921247767170769
                 ]
             },
             "properties": {
                 "OBJECTID": 896,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 897,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.187735344011,
                     25.921239366603515
                 ]
             },
             "properties": {
                 "OBJECTID": 897,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 898,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18737418437115,
                     25.92166952043209
                 ]
             },
             "properties": {
                 "OBJECTID": 898,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 899,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18733124803873,
                     25.921732065582432
                 ]
             },
             "properties": {
                 "OBJECTID": 899,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 900,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18728001276241,
                     25.921806701218316
                 ]
             },
             "properties": {
                 "OBJECTID": 900,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 901,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18708212863737,
                     25.922219529607673
                 ]
             },
             "properties": {
                 "OBJECTID": 901,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 902,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18714752823593,
                     25.922241059377427
                 ]
             },
             "properties": {
                 "OBJECTID": 902,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 903,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18717707815966,
                     25.922796308003285
                 ]
             },
             "properties": {
                 "OBJECTID": 903,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 904,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18716167097438,
                     25.923063551441771
                 ]
             },
             "properties": {
                 "OBJECTID": 904,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 905,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18722782510412,
                     25.923087095692949
                 ]
             },
             "properties": {
                 "OBJECTID": 905,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 906,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18717884982414,
                     25.923313053054983
                 ]
             },
             "properties": {
                 "OBJECTID": 906,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 907,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18714333379785,
                     25.923718056443192
                 ]
             },
             "properties": {
                 "OBJECTID": 907,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 908,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18697251476982,
                     25.923663106966842
                 ]
             },
             "properties": {
                 "OBJECTID": 908,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 909,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1869120425568,
                     25.923799106943306
                 ]
             },
             "properties": {
                 "OBJECTID": 909,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 910,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18674376501281,
                     25.923998911120918
                 ]
             },
             "properties": {
                 "OBJECTID": 910,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 911,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18663906683952,
                     25.924073967639572
                 ]
             },
             "properties": {
                 "OBJECTID": 911,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 912,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18652296706136,
                     25.92419648138258
                 ]
             },
             "properties": {
                 "OBJECTID": 912,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 913,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18582845032279,
                     25.92449217307535
                 ]
             },
             "properties": {
                 "OBJECTID": 913,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 914,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18648128977975,
                     25.925091126953987
                 ]
             },
             "properties": {
                 "OBJECTID": 914,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 915,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18645829231639,
                     25.925290618167537
                 ]
             },
             "properties": {
                 "OBJECTID": 915,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 916,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18646250384154,
                     25.926137554704439
                 ]
             },
             "properties": {
                 "OBJECTID": 916,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 917,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18646328715101,
                     25.92631857833976
                 ]
             },
             "properties": {
                 "OBJECTID": 917,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 918,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18650646000521,
                     25.927184230667763
                 ]
             },
             "properties": {
                 "OBJECTID": 918,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 919,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18671712169788,
                     25.928342848843442
                 ]
             },
             "properties": {
                 "OBJECTID": 919,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 920,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18644656515693,
                     25.929310326910127
                 ]
             },
             "properties": {
                 "OBJECTID": 920,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 921,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18445431591402,
                     25.932062222693446
                 ]
             },
             "properties": {
                 "OBJECTID": 921,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 922,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18422209837121,
                     25.932258231732078
                 ]
             },
             "properties": {
                 "OBJECTID": 922,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 923,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18322077072207,
                     25.933060821799529
                 ]
             },
             "properties": {
                 "OBJECTID": 923,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 924,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18096636820337,
                     25.933844416386762
                 ]
             },
             "properties": {
                 "OBJECTID": 924,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 925,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18009800082211,
                     25.934048958192932
                 ]
             },
             "properties": {
                 "OBJECTID": 925,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 926,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1792437689848,
                     25.93425758561915
                 ]
             },
             "properties": {
                 "OBJECTID": 926,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 927,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1788973267503,
                     25.934320103789787
                 ]
             },
             "properties": {
                 "OBJECTID": 927,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 928,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17482800241044,
                     25.934843526307759
                 ]
             },
             "properties": {
                 "OBJECTID": 928,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 929,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17318895200862,
                     25.934857409142182
                 ]
             },
             "properties": {
                 "OBJECTID": 929,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 930,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17183182377971,
                     25.934801687148195
                 ]
             },
             "properties": {
                 "OBJECTID": 930,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 931,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17116915113547,
                     25.934623803945442
                 ]
             },
             "properties": {
                 "OBJECTID": 931,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 932,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17092755905963,
                     25.934496423071494
                 ]
             },
             "properties": {
                 "OBJECTID": 932,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 933,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17054307460324,
                     25.933754271942121
                 ]
             },
             "properties": {
                 "OBJECTID": 933,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 934,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17035545623935,
                     25.933068082026409
                 ]
             },
             "properties": {
                 "OBJECTID": 934,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 935,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17017842739256,
                     25.932669875714225
                 ]
             },
             "properties": {
                 "OBJECTID": 935,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 936,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16915818430283,
                     25.93169989303567
                 ]
             },
             "properties": {
                 "OBJECTID": 936,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 937,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16818180204859,
                     25.931550604676715
                 ]
             },
             "properties": {
                 "OBJECTID": 937,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 938,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1668812123981,
                     25.932992438250494
                 ]
             },
             "properties": {
                 "OBJECTID": 938,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 939,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16664170225283,
                     25.933228665870274
                 ]
             },
             "properties": {
                 "OBJECTID": 939,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 940,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16650874018626,
                     25.933421619012563
                 ]
             },
             "properties": {
                 "OBJECTID": 940,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 941,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16647578902649,
                     25.933534018979856
                 ]
             },
             "properties": {
                 "OBJECTID": 941,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 942,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16645298401806,
                     25.933703560071308
                 ]
             },
             "properties": {
                 "OBJECTID": 942,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 943,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16572710971838,
                     25.93608145029873
                 ]
             },
             "properties": {
                 "OBJECTID": 943,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 944,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16472645386284,
                     25.936857328702786
                 ]
             },
             "properties": {
                 "OBJECTID": 944,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 945,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16444038761387,
                     25.937236541333391
                 ]
             },
             "properties": {
                 "OBJECTID": 945,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 946,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16437710681811,
                     25.93747141637283
                 ]
             },
             "properties": {
                 "OBJECTID": 946,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 947,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16436702541796,
                     25.937921839121657
                 ]
             },
             "properties": {
                 "OBJECTID": 947,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 948,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16477147482374,
                     25.938819396697852
                 ]
             },
             "properties": {
                 "OBJECTID": 948,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 949,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1653434571349,
                     25.939776530762344
                 ]
             },
             "properties": {
                 "OBJECTID": 949,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 950,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16544851863426,
                     25.939914868076755
                 ]
             },
             "properties": {
                 "OBJECTID": 950,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 951,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16721737628097,
                     25.941075730960279
                 ]
             },
             "properties": {
                 "OBJECTID": 951,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 952,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16750220955936,
                     25.943689421629529
                 ]
             },
             "properties": {
                 "OBJECTID": 952,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 953,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.167426838278,
                     25.944663023179999
                 ]
             },
             "properties": {
                 "OBJECTID": 953,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 954,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16709794271594,
                     25.945509866187422
                 ]
             },
             "properties": {
                 "OBJECTID": 954,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 955,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16618456516562,
                     25.946512136326078
                 ]
             },
             "properties": {
                 "OBJECTID": 955,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 956,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16568563478427,
                     25.946969769839086
                 ]
             },
             "properties": {
                 "OBJECTID": 956,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 957,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16393122294767,
                     25.948581147202219
                 ]
             },
             "properties": {
                 "OBJECTID": 957,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 958,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16118066805348,
                     25.948753414138821
                 ]
             },
             "properties": {
                 "OBJECTID": 958,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 959,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16058578810367,
                     25.948601228164193
                 ]
             },
             "properties": {
                 "OBJECTID": 959,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 960,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16042316279879,
                     25.948596250416699
                 ]
             },
             "properties": {
                 "OBJECTID": 960,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 961,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16027883420088,
                     25.9486188027156
                 ]
             },
             "properties": {
                 "OBJECTID": 961,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 962,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16012779755988,
                     25.948675563426548
                 ]
             },
             "properties": {
                 "OBJECTID": 962,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 963,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15995141712426,
                     25.948755958320874
                 ]
             },
             "properties": {
                 "OBJECTID": 963,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 964,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15962813782835,
                     25.948931836035456
                 ]
             },
             "properties": {
                 "OBJECTID": 964,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 965,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15932078732453,
                     25.949135475821606
                 ]
             },
             "properties": {
                 "OBJECTID": 965,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 966,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15919909466254,
                     25.949283900831404
                 ]
             },
             "properties": {
                 "OBJECTID": 966,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 967,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15911962876794,
                     25.949606325771356
                 ]
             },
             "properties": {
                 "OBJECTID": 967,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 968,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1586692752669,
                     25.951732847393032
                 ]
             },
             "properties": {
                 "OBJECTID": 968,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 969,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15855239487718,
                     25.951902303948202
                 ]
             },
             "properties": {
                 "OBJECTID": 969,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 970,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1578064746891,
                     25.9522550180547
                 ]
             },
             "properties": {
                 "OBJECTID": 970,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 971,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15675235593653,
                     25.952691423070746
                 ]
             },
             "properties": {
                 "OBJECTID": 971,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 972,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1551949172117,
                     25.953364090780212
                 ]
             },
             "properties": {
                 "OBJECTID": 972,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 973,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15462098786833,
                     25.954242865116157
                 ]
             },
             "properties": {
                 "OBJECTID": 973,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 974,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15440067285283,
                     25.954709939712018
                 ]
             },
             "properties": {
                 "OBJECTID": 974,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 975,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15360340677188,
                     25.95554343767958
                 ]
             },
             "properties": {
                 "OBJECTID": 975,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 976,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15314087375236,
                     25.955821766161648
                 ]
             },
             "properties": {
                 "OBJECTID": 976,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 977,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15284961481791,
                     25.956019761802622
                 ]
             },
             "properties": {
                 "OBJECTID": 977,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 978,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15257932357696,
                     25.956358959098736
                 ]
             },
             "properties": {
                 "OBJECTID": 978,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 979,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15255003895322,
                     25.95663172797191
                 ]
             },
             "properties": {
                 "OBJECTID": 979,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 980,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.152619265167,
                     25.956809200184466
                 ]
             },
             "properties": {
                 "OBJECTID": 980,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 981,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15281184688928,
                     25.957011031433979
                 ]
             },
             "properties": {
                 "OBJECTID": 981,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 982,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1536203706836,
                     25.957357158006459
                 ]
             },
             "properties": {
                 "OBJECTID": 982,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 983,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15387424839793,
                     25.957851807614873
                 ]
             },
             "properties": {
                 "OBJECTID": 983,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 984,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15394896137553,
                     25.95824215834898
                 ]
             },
             "properties": {
                 "OBJECTID": 984,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 985,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15484797675276,
                     25.960542892140836
                 ]
             },
             "properties": {
                 "OBJECTID": 985,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 986,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15502329598837,
                     25.961069386742849
                 ]
             },
             "properties": {
                 "OBJECTID": 986,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 987,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15510429522709,
                     25.961460379592893
                 ]
             },
             "properties": {
                 "OBJECTID": 987,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 988,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15514154604557,
                     25.962285021036962
                 ]
             },
             "properties": {
                 "OBJECTID": 988,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 989,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1550663986954,
                     25.962811825905078
                 ]
             },
             "properties": {
                 "OBJECTID": 989,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 990,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15502077159141,
                     25.962959827334203
                 ]
             },
             "properties": {
                 "OBJECTID": 990,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 991,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15457244786023,
                     25.964350414839828
                 ]
             },
             "properties": {
                 "OBJECTID": 991,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 992,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15431333159506,
                     25.964859608284371
                 ]
             },
             "properties": {
                 "OBJECTID": 992,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 993,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15411344917641,
                     25.965026383960492
                 ]
             },
             "properties": {
                 "OBJECTID": 993,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 994,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15282723339016,
                     25.965508203840614
                 ]
             },
             "properties": {
                 "OBJECTID": 994,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 995,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15215549198246,
                     25.965741665145799
                 ]
             },
             "properties": {
                 "OBJECTID": 995,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 996,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15115272901528,
                     25.965857701971402
                 ]
             },
             "properties": {
                 "OBJECTID": 996,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 997,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15006788312638,
                     25.965800539263512
                 ]
             },
             "properties": {
                 "OBJECTID": 997,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 998,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14887307443291,
                     25.965721926825495
                 ]
             },
             "properties": {
                 "OBJECTID": 998,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 999,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14875077203055,
                     25.965702829721863
                 ]
             },
             "properties": {
                 "OBJECTID": 999,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1000,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14821413757244,
                     25.965408272973491
                 ]
             },
             "properties": {
                 "OBJECTID": 1000,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1001,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14797947477302,
                     25.965119654448529
                 ]
             },
             "properties": {
                 "OBJECTID": 1001,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1002,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14762031972202,
                     25.964320987223857
                 ]
             },
             "properties": {
                 "OBJECTID": 1002,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1003,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14745968461682,
                     25.964161112945078
                 ]
             },
             "properties": {
                 "OBJECTID": 1003,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1004,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14616262840264,
                     25.963744110802111
                 ]
             },
             "properties": {
                 "OBJECTID": 1004,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1005,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14494677377468,
                     25.963447287762165
                 ]
             },
             "properties": {
                 "OBJECTID": 1005,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1006,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14437353600994,
                     25.963301359270361
                 ]
             },
             "properties": {
                 "OBJECTID": 1006,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1007,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14279465924619,
                     25.962881752690805
                 ]
             },
             "properties": {
                 "OBJECTID": 1007,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1008,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14168528794573,
                     25.962273444064238
                 ]
             },
             "properties": {
                 "OBJECTID": 1008,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1009,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14120632341314,
                     25.961862185891221
                 ]
             },
             "properties": {
                 "OBJECTID": 1009,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1010,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14006401893937,
                     25.960916758301778
                 ]
             },
             "properties": {
                 "OBJECTID": 1010,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1011,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13919017108958,
                     25.960447888659189
                 ]
             },
             "properties": {
                 "OBJECTID": 1011,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1012,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1383922026381,
                     25.959845743086646
                 ]
             },
             "properties": {
                 "OBJECTID": 1012,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1013,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13824014616586,
                     25.959512294257024
                 ]
             },
             "properties": {
                 "OBJECTID": 1013,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1014,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13833130864305,
                     25.958791044271322
                 ]
             },
             "properties": {
                 "OBJECTID": 1014,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1015,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13848176432202,
                     25.958575663835859
                 ]
             },
             "properties": {
                 "OBJECTID": 1015,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1016,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1390263730687,
                     25.958122901950389
                 ]
             },
             "properties": {
                 "OBJECTID": 1016,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1017,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14032189593883,
                     25.957034736663843
                 ]
             },
             "properties": {
                 "OBJECTID": 1017,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1018,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14133727189761,
                     25.955494638666664
                 ]
             },
             "properties": {
                 "OBJECTID": 1018,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1019,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14126018381052,
                     25.954813040689316
                 ]
             },
             "properties": {
                 "OBJECTID": 1019,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1020,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14069642490017,
                     25.9542792040221
                 ]
             },
             "properties": {
                 "OBJECTID": 1020,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1021,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13963490382781,
                     25.953470125346087
                 ]
             },
             "properties": {
                 "OBJECTID": 1021,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1022,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13926944992522,
                     25.953190690697852
                 ]
             },
             "properties": {
                 "OBJECTID": 1022,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1023,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13884398606115,
                     25.952865473362976
                 ]
             },
             "properties": {
                 "OBJECTID": 1023,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1024,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13846472486722,
                     25.952537135380567
                 ]
             },
             "properties": {
                 "OBJECTID": 1024,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1025,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13819861007784,
                     25.952125798966506
                 ]
             },
             "properties": {
                 "OBJECTID": 1025,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1026,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13809738148905,
                     25.951602842297405
                 ]
             },
             "properties": {
                 "OBJECTID": 1026,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1027,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13803502429698,
                     25.950973416689862
                 ]
             },
             "properties": {
                 "OBJECTID": 1027,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1028,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13614776190974,
                     25.952458939328665
                 ]
             },
             "properties": {
                 "OBJECTID": 1028,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1029,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1359370075869,
                     25.952383305445267
                 ]
             },
             "properties": {
                 "OBJECTID": 1029,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1030,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1356346420252,
                     25.952296501082685
                 ]
             },
             "properties": {
                 "OBJECTID": 1030,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1031,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13799390459508,
                     25.950331388882546
                 ]
             },
             "properties": {
                 "OBJECTID": 1031,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1032,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13800933606205,
                     25.949967389183712
                 ]
             },
             "properties": {
                 "OBJECTID": 1032,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1033,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13809102957742,
                     25.949540881206758
                 ]
             },
             "properties": {
                 "OBJECTID": 1033,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1034,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13819324562184,
                     25.949211173556876
                 ]
             },
             "properties": {
                 "OBJECTID": 1034,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1035,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13871100870614,
                     25.948331993626653
                 ]
             },
             "properties": {
                 "OBJECTID": 1035,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1036,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14022436266424,
                     25.945982459922618
                 ]
             },
             "properties": {
                 "OBJECTID": 1036,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1037,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14103261216536,
                     25.944687282392238
                 ]
             },
             "properties": {
                 "OBJECTID": 1037,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1038,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14122894765785,
                     25.944096526733347
                 ]
             },
             "properties": {
                 "OBJECTID": 1038,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1039,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14124331612618,
                     25.943969478608608
                 ]
             },
             "properties": {
                 "OBJECTID": 1039,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1040,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14057654976801,
                     25.941716535689636
                 ]
             },
             "properties": {
                 "OBJECTID": 1040,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1041,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13763207316168,
                     25.939693558411022
                 ]
             },
             "properties": {
                 "OBJECTID": 1041,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1042,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13740681637154,
                     25.939350883436589
                 ]
             },
             "properties": {
                 "OBJECTID": 1042,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1043,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13685941972648,
                     25.938347775129785
                 ]
             },
             "properties": {
                 "OBJECTID": 1043,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1044,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1361142073049,
                     25.936984027890617
                 ]
             },
             "properties": {
                 "OBJECTID": 1044,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1045,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13576984700092,
                     25.936350744191543
                 ]
             },
             "properties": {
                 "OBJECTID": 1045,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1046,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13489306826057,
                     25.935152337310569
                 ]
             },
             "properties": {
                 "OBJECTID": 1046,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1047,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1342940523287,
                     25.934506530551971
                 ]
             },
             "properties": {
                 "OBJECTID": 1047,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1048,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1340878126025,
                     25.934249786698331
                 ]
             },
             "properties": {
                 "OBJECTID": 1048,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1049,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13400367113258,
                     25.932412109331437
                 ]
             },
             "properties": {
                 "OBJECTID": 1049,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1050,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13404455521209,
                     25.931829765931013
                 ]
             },
             "properties": {
                 "OBJECTID": 1050,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1051,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13423668097812,
                     25.930218444325817
                 ]
             },
             "properties": {
                 "OBJECTID": 1051,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1052,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13437695363359,
                     25.928833463193826
                 ]
             },
             "properties": {
                 "OBJECTID": 1052,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1053,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13441732420023,
                     25.927342121941422
                 ]
             },
             "properties": {
                 "OBJECTID": 1053,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1054,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13439307038396,
                     25.927246436773714
                 ]
             },
             "properties": {
                 "OBJECTID": 1054,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1055,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13434533796703,
                     25.927145760368603
                 ]
             },
             "properties": {
                 "OBJECTID": 1055,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1056,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13421386427848,
                     25.926982044186047
                 ]
             },
             "properties": {
                 "OBJECTID": 1056,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1057,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13346673360292,
                     25.926805205995493
                 ]
             },
             "properties": {
                 "OBJECTID": 1057,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1058,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13276738930364,
                     25.926824843591703
                 ]
             },
             "properties": {
                 "OBJECTID": 1058,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1059,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1324599030022,
                     25.926906512825383
                 ]
             },
             "properties": {
                 "OBJECTID": 1059,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1060,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13219543487151,
                     25.927172815573044
                 ]
             },
             "properties": {
                 "OBJECTID": 1060,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1061,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13195849768658,
                     25.928631566293802
                 ]
             },
             "properties": {
                 "OBJECTID": 1061,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1062,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13119075185176,
                     25.930647016246837
                 ]
             },
             "properties": {
                 "OBJECTID": 1062,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1063,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13073653126594,
                     25.93140451970288
                 ]
             },
             "properties": {
                 "OBJECTID": 1063,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1064,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1300756923394,
                     25.932084094205607
                 ]
             },
             "properties": {
                 "OBJECTID": 1064,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1065,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12949641832512,
                     25.932620690892236
                 ]
             },
             "properties": {
                 "OBJECTID": 1065,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1066,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12775190712489,
                     25.934426728310939
                 ]
             },
             "properties": {
                 "OBJECTID": 1066,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1067,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12664091974273,
                     25.937217323720574
                 ]
             },
             "properties": {
                 "OBJECTID": 1067,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1068,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12105569276605,
                     25.937811196429607
                 ]
             },
             "properties": {
                 "OBJECTID": 1068,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1069,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1200302237159,
                     25.938406221170226
                 ]
             },
             "properties": {
                 "OBJECTID": 1069,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1070,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11922004337038,
                     25.940962630421097
                 ]
             },
             "properties": {
                 "OBJECTID": 1070,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1071,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11979632713883,
                     25.942106069839326
                 ]
             },
             "properties": {
                 "OBJECTID": 1071,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1072,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12055672641185,
                     25.943062017698026
                 ]
             },
             "properties": {
                 "OBJECTID": 1072,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1073,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1202500351111,
                     25.94346764161844
                 ]
             },
             "properties": {
                 "OBJECTID": 1073,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1074,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12008942608622,
                     25.943576301305313
                 ]
             },
             "properties": {
                 "OBJECTID": 1074,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1075,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11987626158026,
                     25.943319221105241
                 ]
             },
             "properties": {
                 "OBJECTID": 1075,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1076,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11974936634027,
                     25.943223299415763
                 ]
             },
             "properties": {
                 "OBJECTID": 1076,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1077,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11939898867246,
                     25.943097138921871
                 ]
             },
             "properties": {
                 "OBJECTID": 1077,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1078,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11926515696149,
                     25.943054985898925
                 ]
             },
             "properties": {
                 "OBJECTID": 1078,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1079,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11871471341334,
                     25.942802912224693
                 ]
             },
             "properties": {
                 "OBJECTID": 1079,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1080,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11848667681875,
                     25.942847129191705
                 ]
             },
             "properties": {
                 "OBJECTID": 1080,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1081,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11792927971254,
                     25.943117386258393
                 ]
             },
             "properties": {
                 "OBJECTID": 1081,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1082,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11715477187528,
                     25.942063806199769
                 ]
             },
             "properties": {
                 "OBJECTID": 1082,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1083,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1172785221861,
                     25.941782602585079
                 ]
             },
             "properties": {
                 "OBJECTID": 1083,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1084,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11649343112703,
                     25.941067131642853
                 ]
             },
             "properties": {
                 "OBJECTID": 1084,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1085,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11497252825961,
                     25.939670354103612
                 ]
             },
             "properties": {
                 "OBJECTID": 1085,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1086,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11431266060089,
                     25.939307154801384
                 ]
             },
             "properties": {
                 "OBJECTID": 1086,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1087,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11259214331284,
                     25.939225741874566
                 ]
             },
             "properties": {
                 "OBJECTID": 1087,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1088,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11092268413461,
                     25.939225483769121
                 ]
             },
             "properties": {
                 "OBJECTID": 1088,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1089,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11085351367876,
                     25.941219906673382
                 ]
             },
             "properties": {
                 "OBJECTID": 1089,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1090,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11087887905705,
                     25.941320119028319
                 ]
             },
             "properties": {
                 "OBJECTID": 1090,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1091,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11079075898641,
                     25.941463554599409
                 ]
             },
             "properties": {
                 "OBJECTID": 1091,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1092,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11066945033491,
                     25.941551757407751
                 ]
             },
             "properties": {
                 "OBJECTID": 1092,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1093,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11034578343123,
                     25.941330367702335
                 ]
             },
             "properties": {
                 "OBJECTID": 1093,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1094,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10977181361835,
                     25.940822268732745
                 ]
             },
             "properties": {
                 "OBJECTID": 1094,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1095,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10969602864884,
                     25.940720720884599
                 ]
             },
             "properties": {
                 "OBJECTID": 1095,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1096,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10953897194605,
                     25.940655402225104
                 ]
             },
             "properties": {
                 "OBJECTID": 1096,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1097,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1092481986455,
                     25.940002726444732
                 ]
             },
             "properties": {
                 "OBJECTID": 1097,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1098,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10891458344133,
                     25.939592794771727
                 ]
             },
             "properties": {
                 "OBJECTID": 1098,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1099,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10866869260616,
                     25.939299794749786
                 ]
             },
             "properties": {
                 "OBJECTID": 1099,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1100,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10852974105478,
                     25.938941497652024
                 ]
             },
             "properties": {
                 "OBJECTID": 1100,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1101,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10847298933703,
                     25.93873820230624
                 ]
             },
             "properties": {
                 "OBJECTID": 1101,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1102,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10855877386837,
                     25.938730475331226
                 ]
             },
             "properties": {
                 "OBJECTID": 1102,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1103,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10853842041183,
                     25.938311717711656
                 ]
             },
             "properties": {
                 "OBJECTID": 1103,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1104,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10867020166853,
                     25.938307615903796
                 ]
             },
             "properties": {
                 "OBJECTID": 1104,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1105,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10859759040653,
                     25.937954311842077
                 ]
             },
             "properties": {
                 "OBJECTID": 1105,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1106,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10858194220293,
                     25.936876554409537
                 ]
             },
             "properties": {
                 "OBJECTID": 1106,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1107,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10821089901384,
                     25.93688893267813
                 ]
             },
             "properties": {
                 "OBJECTID": 1107,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1108,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10788056993243,
                     25.936696772737946
                 ]
             },
             "properties": {
                 "OBJECTID": 1108,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1109,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10801526858984,
                     25.935458803780818
                 ]
             },
             "properties": {
                 "OBJECTID": 1109,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1110,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10768502404466,
                     25.935137081211394
                 ]
             },
             "properties": {
                 "OBJECTID": 1110,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1111,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10729199782747,
                     25.934776572680732
                 ]
             },
             "properties": {
                 "OBJECTID": 1111,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1112,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10706284697289,
                     25.934636696626285
                 ]
             },
             "properties": {
                 "OBJECTID": 1112,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1113,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10692048519229,
                     25.934595754090822
                 ]
             },
             "properties": {
                 "OBJECTID": 1113,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1114,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10625645906839,
                     25.934442236220264
                 ]
             },
             "properties": {
                 "OBJECTID": 1114,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1115,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10568238223624,
                     25.934373046878648
                 ]
             },
             "properties": {
                 "OBJECTID": 1115,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1116,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10556692277532,
                     25.9343579112886
                 ]
             },
             "properties": {
                 "OBJECTID": 1116,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1117,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10545451111687,
                     25.934609535301888
                 ]
             },
             "properties": {
                 "OBJECTID": 1117,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1118,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10525586616535,
                     25.934878178984832
                 ]
             },
             "properties": {
                 "OBJECTID": 1118,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1119,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10480547219487,
                     25.934676453855332
                 ]
             },
             "properties": {
                 "OBJECTID": 1119,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1120,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10395662729655,
                     25.934111338767309
                 ]
             },
             "properties": {
                 "OBJECTID": 1120,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1121,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10388565190135,
                     25.93382755319908
                 ]
             },
             "properties": {
                 "OBJECTID": 1121,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1122,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10387603185347,
                     25.933514253679903
                 ]
             },
             "properties": {
                 "OBJECTID": 1122,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1123,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10392030008182,
                     25.933089681943329
                 ]
             },
             "properties": {
                 "OBJECTID": 1123,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1124,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10406099991525,
                     25.932859218078477
                 ]
             },
             "properties": {
                 "OBJECTID": 1124,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1125,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10392597660262,
                     25.932754879634047
                 ]
             },
             "properties": {
                 "OBJECTID": 1125,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1126,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10379761276965,
                     25.932709325375129
                 ]
             },
             "properties": {
                 "OBJECTID": 1126,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1127,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10230900905361,
                     25.934194198703381
                 ]
             },
             "properties": {
                 "OBJECTID": 1127,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1128,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10219582038087,
                     25.934764010950403
                 ]
             },
             "properties": {
                 "OBJECTID": 1128,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1129,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1021225202382,
                     25.935217546252829
                 ]
             },
             "properties": {
                 "OBJECTID": 1129,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1130,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10127866118131,
                     25.937921071999938
                 ]
             },
             "properties": {
                 "OBJECTID": 1130,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1131,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10126134023869,
                     25.938079889575306
                 ]
             },
             "properties": {
                 "OBJECTID": 1131,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1132,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10142284678699,
                     25.93913094074037
                 ]
             },
             "properties": {
                 "OBJECTID": 1132,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1133,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10129424103644,
                     25.939513790229284
                 ]
             },
             "properties": {
                 "OBJECTID": 1133,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1134,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10033623912619,
                     25.94158109958812
                 ]
             },
             "properties": {
                 "OBJECTID": 1134,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1135,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09959717716805,
                     25.943124700444628
                 ]
             },
             "properties": {
                 "OBJECTID": 1135,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1136,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09946752640531,
                     25.943291418564172
                 ]
             },
             "properties": {
                 "OBJECTID": 1136,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1137,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09938981778595,
                     25.943502634239223
                 ]
             },
             "properties": {
                 "OBJECTID": 1137,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1138,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09872137059483,
                     25.94346173936782
                 ]
             },
             "properties": {
                 "OBJECTID": 1138,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1139,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09827628981895,
                     25.943069374152344
                 ]
             },
             "properties": {
                 "OBJECTID": 1139,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1140,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09808924342389,
                     25.942423938813761
                 ]
             },
             "properties": {
                 "OBJECTID": 1140,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1141,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09755466121868,
                     25.940286725152305
                 ]
             },
             "properties": {
                 "OBJECTID": 1141,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1142,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09733104569125,
                     25.939406487619351
                 ]
             },
             "properties": {
                 "OBJECTID": 1142,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1143,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09684460599385,
                     25.938513870019165
                 ]
             },
             "properties": {
                 "OBJECTID": 1143,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1144,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09647944257227,
                     25.938136513589257
                 ]
             },
             "properties": {
                 "OBJECTID": 1144,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1145,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09740833522653,
                     25.932980622957416
                 ]
             },
             "properties": {
                 "OBJECTID": 1145,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1146,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09786854709586,
                     25.932510511351097
                 ]
             },
             "properties": {
                 "OBJECTID": 1146,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1147,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09862030118603,
                     25.93171001490532
                 ]
             },
             "properties": {
                 "OBJECTID": 1147,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1148,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09901104042723,
                     25.931311379616545
                 ]
             },
             "properties": {
                 "OBJECTID": 1148,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1149,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.09978089337358,
                     25.930488306590121
                 ]
             },
             "properties": {
                 "OBJECTID": 1149,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1150,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10031324885739,
                     25.929889610816929
                 ]
             },
             "properties": {
                 "OBJECTID": 1150,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1151,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10105898828175,
                     25.928981229000556
                 ]
             },
             "properties": {
                 "OBJECTID": 1151,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1152,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10242248281139,
                     25.927881195360953
                 ]
             },
             "properties": {
                 "OBJECTID": 1152,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1153,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10322896244674,
                     25.927302350323259
                 ]
             },
             "properties": {
                 "OBJECTID": 1153,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1154,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10361441907025,
                     25.926866909380408
                 ]
             },
             "properties": {
                 "OBJECTID": 1154,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1155,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10395033384088,
                     25.926306603866749
                 ]
             },
             "properties": {
                 "OBJECTID": 1155,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1156,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10490402979718,
                     25.924486013618662
                 ]
             },
             "properties": {
                 "OBJECTID": 1156,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1157,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10527964783756,
                     25.923758296608128
                 ]
             },
             "properties": {
                 "OBJECTID": 1157,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1158,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1055878491,
                     25.923105545284727
                 ]
             },
             "properties": {
                 "OBJECTID": 1158,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1159,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1065698656069,
                     25.922509146380037
                 ]
             },
             "properties": {
                 "OBJECTID": 1159,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1160,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10737112377842,
                     25.921863479016395
                 ]
             },
             "properties": {
                 "OBJECTID": 1160,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1161,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10835800471818,
                     25.921565934720149
                 ]
             },
             "properties": {
                 "OBJECTID": 1161,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1162,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10861357225861,
                     25.921430388901399
                 ]
             },
             "properties": {
                 "OBJECTID": 1162,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1163,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1089083322538,
                     25.921258532055674
                 ]
             },
             "properties": {
                 "OBJECTID": 1163,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1164,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10920630642596,
                     25.921082674126183
                 ]
             },
             "properties": {
                 "OBJECTID": 1164,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1165,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10926776699392,
                     25.921176666769782
                 ]
             },
             "properties": {
                 "OBJECTID": 1165,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1166,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10934444858748,
                     25.921255835888189
                 ]
             },
             "properties": {
                 "OBJECTID": 1166,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1167,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.10993143968426,
                     25.920938335635753
                 ]
             },
             "properties": {
                 "OBJECTID": 1167,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1168,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11155658836594,
                     25.92040178841188
                 ]
             },
             "properties": {
                 "OBJECTID": 1168,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1169,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11210835571609,
                     25.920259003050603
                 ]
             },
             "properties": {
                 "OBJECTID": 1169,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1170,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11237745625465,
                     25.920182926700761
                 ]
             },
             "properties": {
                 "OBJECTID": 1170,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1171,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11275655377199,
                     25.920000838268095
                 ]
             },
             "properties": {
                 "OBJECTID": 1171,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1172,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11393564141679,
                     25.919243360892438
                 ]
             },
             "properties": {
                 "OBJECTID": 1172,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1173,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11415037973632,
                     25.918916930372063
                 ]
             },
             "properties": {
                 "OBJECTID": 1173,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1174,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11412911796447,
                     25.91886942368501
                 ]
             },
             "properties": {
                 "OBJECTID": 1174,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1175,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11419385116534,
                     25.918703245158667
                 ]
             },
             "properties": {
                 "OBJECTID": 1175,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1176,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11424358187588,
                     25.918130153983498
                 ]
             },
             "properties": {
                 "OBJECTID": 1176,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1177,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11438698507135,
                     25.918183215782847
                 ]
             },
             "properties": {
                 "OBJECTID": 1177,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1178,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11449421393689,
                     25.917975617181014
                 ]
             },
             "properties": {
                 "OBJECTID": 1178,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1179,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1145278971448,
                     25.917815276154101
                 ]
             },
             "properties": {
                 "OBJECTID": 1179,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1180,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11462972288348,
                     25.917535676030695
                 ]
             },
             "properties": {
                 "OBJECTID": 1180,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1181,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11479153520122,
                     25.917410019157103
                 ]
             },
             "properties": {
                 "OBJECTID": 1181,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1182,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11473171319807,
                     25.917184464691331
                 ]
             },
             "properties": {
                 "OBJECTID": 1182,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1183,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11483368013029,
                     25.917209741036743
                 ]
             },
             "properties": {
                 "OBJECTID": 1183,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1184,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11495025385119,
                     25.917043716294529
                 ]
             },
             "properties": {
                 "OBJECTID": 1184,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1185,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11499824437357,
                     25.916883574917108
                 ]
             },
             "properties": {
                 "OBJECTID": 1185,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1186,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11491791512975,
                     25.916773467321718
                 ]
             },
             "properties": {
                 "OBJECTID": 1186,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1187,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11500121123697,
                     25.916517091290757
                 ]
             },
             "properties": {
                 "OBJECTID": 1187,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1188,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11501196083339,
                     25.916383942165226
                 ]
             },
             "properties": {
                 "OBJECTID": 1188,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1189,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11490550898213,
                     25.916349187864796
                 ]
             },
             "properties": {
                 "OBJECTID": 1189,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1190,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11500424195225,
                     25.915939140179205
                 ]
             },
             "properties": {
                 "OBJECTID": 1190,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1191,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11524767673893,
                     25.915734101947294
                 ]
             },
             "properties": {
                 "OBJECTID": 1191,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1192,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11541870081243,
                     25.915383411315418
                 ]
             },
             "properties": {
                 "OBJECTID": 1192,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1193,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11534128177482,
                     25.915335818293443
                 ]
             },
             "properties": {
                 "OBJECTID": 1193,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1194,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11547375730811,
                     25.915046334620683
                 ]
             },
             "properties": {
                 "OBJECTID": 1194,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1195,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11551588515005,
                     25.914835173804306
                 ]
             },
             "properties": {
                 "OBJECTID": 1195,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1196,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11569030956019,
                     25.914607969082567
                 ]
             },
             "properties": {
                 "OBJECTID": 1196,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1197,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11576286326556,
                     25.914409657779515
                 ]
             },
             "properties": {
                 "OBJECTID": 1197,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1198,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11591004631163,
                     25.914403340941476
                 ]
             },
             "properties": {
                 "OBJECTID": 1198,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1199,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1158799891702,
                     25.914012886785372
                 ]
             },
             "properties": {
                 "OBJECTID": 1199,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1200,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11578045130682,
                     25.913762121624643
                 ]
             },
             "properties": {
                 "OBJECTID": 1200,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1201,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11558760788188,
                     25.913800365294662
                 ]
             },
             "properties": {
                 "OBJECTID": 1201,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1202,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11551731956871,
                     25.913620580025793
                 ]
             },
             "properties": {
                 "OBJECTID": 1202,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1203,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11571055509808,
                     25.913562483821636
                 ]
             },
             "properties": {
                 "OBJECTID": 1203,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1204,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11564990931595,
                     25.913380316248663
                 ]
             },
             "properties": {
                 "OBJECTID": 1204,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1205,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11561460373105,
                     25.91319837710347
                 ]
             },
             "properties": {
                 "OBJECTID": 1205,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1206,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11538558507675,
                     25.913241137168768
                 ]
             },
             "properties": {
                 "OBJECTID": 1206,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1207,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11534548430672,
                     25.91304325933902
                 ]
             },
             "properties": {
                 "OBJECTID": 1207,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1208,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11540308768241,
                     25.912608527061934
                 ]
             },
             "properties": {
                 "OBJECTID": 1208,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1209,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11517682095359,
                     25.912611250209068
                 ]
             },
             "properties": {
                 "OBJECTID": 1209,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1210,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11519143313819,
                     25.912535750324651
                 ]
             },
             "properties": {
                 "OBJECTID": 1210,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1211,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11524458127246,
                     25.912126986870987
                 ]
             },
             "properties": {
                 "OBJECTID": 1211,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1212,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11727698524476,
                     25.909686492137951
                 ]
             },
             "properties": {
                 "OBJECTID": 1212,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1213,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11726572123609,
                     25.909518023937721
                 ]
             },
             "properties": {
                 "OBJECTID": 1213,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1214,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11710489457528,
                     25.908639955569527
                 ]
             },
             "properties": {
                 "OBJECTID": 1214,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1215,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11578408186995,
                     25.908436451581053
                 ]
             },
             "properties": {
                 "OBJECTID": 1215,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1216,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11559477637786,
                     25.908084573844064
                 ]
             },
             "properties": {
                 "OBJECTID": 1216,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1217,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11519310407857,
                     25.907596703325225
                 ]
             },
             "properties": {
                 "OBJECTID": 1217,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1218,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11518304606079,
                     25.907434632002719
                 ]
             },
             "properties": {
                 "OBJECTID": 1218,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1219,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11538198598993,
                     25.906920836727295
                 ]
             },
             "properties": {
                 "OBJECTID": 1219,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1220,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11546663377817,
                     25.90682054793001
                 ]
             },
             "properties": {
                 "OBJECTID": 1220,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1221,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11622543585526,
                     25.906456650753853
                 ]
             },
             "properties": {
                 "OBJECTID": 1221,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1222,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11679851983587,
                     25.906303515095203
                 ]
             },
             "properties": {
                 "OBJECTID": 1222,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1223,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11733477657873,
                     25.906160511198664
                 ]
             },
             "properties": {
                 "OBJECTID": 1223,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1224,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11734900385352,
                     25.906155535249809
                 ]
             },
             "properties": {
                 "OBJECTID": 1224,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1225,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1184322534458,
                     25.905663203194308
                 ]
             },
             "properties": {
                 "OBJECTID": 1225,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1226,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.11948190976238,
                     25.904922875890065
                 ]
             },
             "properties": {
                 "OBJECTID": 1226,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1227,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12068848428618,
                     25.904104233823375
                 ]
             },
             "properties": {
                 "OBJECTID": 1227,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1228,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12106704041162,
                     25.903459827309234
                 ]
             },
             "properties": {
                 "OBJECTID": 1228,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1229,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12111227181396,
                     25.903256219898708
                 ]
             },
             "properties": {
                 "OBJECTID": 1229,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1230,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12110773923087,
                     25.903115501179514
                 ]
             },
             "properties": {
                 "OBJECTID": 1230,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1231,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12107302090322,
                     25.902924148830436
                 ]
             },
             "properties": {
                 "OBJECTID": 1231,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1232,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12093269698642,
                     25.90257136187887
                 ]
             },
             "properties": {
                 "OBJECTID": 1232,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1233,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1207734774141,
                     25.902283388167859
                 ]
             },
             "properties": {
                 "OBJECTID": 1233,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1234,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12049951693922,
                     25.901845039716875
                 ]
             },
             "properties": {
                 "OBJECTID": 1234,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1235,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12018088174239,
                     25.901211277578454
                 ]
             },
             "properties": {
                 "OBJECTID": 1235,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1236,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12014437556252,
                     25.900928689007856
                 ]
             },
             "properties": {
                 "OBJECTID": 1236,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1237,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12020892170443,
                     25.900450300041371
                 ]
             },
             "properties": {
                 "OBJECTID": 1237,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1238,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12049723625853,
                     25.899942871966005
                 ]
             },
             "properties": {
                 "OBJECTID": 1238,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1239,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12093241999526,
                     25.899471762112171
                 ]
             },
             "properties": {
                 "OBJECTID": 1239,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1240,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12195231864467,
                     25.898613693767004
                 ]
             },
             "properties": {
                 "OBJECTID": 1240,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1241,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12241031998036,
                     25.898333697042517
                 ]
             },
             "properties": {
                 "OBJECTID": 1241,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1242,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12287590889616,
                     25.898012067103252
                 ]
             },
             "properties": {
                 "OBJECTID": 1242,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1243,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12338038809452,
                     25.897704471084523
                 ]
             },
             "properties": {
                 "OBJECTID": 1243,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1244,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12396254623457,
                     25.89597915771202
                 ]
             },
             "properties": {
                 "OBJECTID": 1244,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1245,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12388839623259,
                     25.895073864167557
                 ]
             },
             "properties": {
                 "OBJECTID": 1245,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1246,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12393996605664,
                     25.894710377981596
                 ]
             },
             "properties": {
                 "OBJECTID": 1246,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1247,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12407093792353,
                     25.893792098226811
                 ]
             },
             "properties": {
                 "OBJECTID": 1247,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1248,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12412665632019,
                     25.89342098578993
                 ]
             },
             "properties": {
                 "OBJECTID": 1248,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1249,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1247488936466,
                     25.89285417637916
                 ]
             },
             "properties": {
                 "OBJECTID": 1249,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1250,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12489010969085,
                     25.892732483717225
                 ]
             },
             "properties": {
                 "OBJECTID": 1250,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1251,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12502726079953,
                     25.892094026218388
                 ]
             },
             "properties": {
                 "OBJECTID": 1251,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1252,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1257455969818,
                     25.890932246925729
                 ]
             },
             "properties": {
                 "OBJECTID": 1252,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1253,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12573505782672,
                     25.890496258295741
                 ]
             },
             "properties": {
                 "OBJECTID": 1253,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1254,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12699277679525,
                     25.888348160139515
                 ]
             },
             "properties": {
                 "OBJECTID": 1254,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1255,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12772233921464,
                     25.886088112576374
                 ]
             },
             "properties": {
                 "OBJECTID": 1255,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1256,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12760037495741,
                     25.885855134207134
                 ]
             },
             "properties": {
                 "OBJECTID": 1256,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1257,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12688045237104,
                     25.884647133358555
                 ]
             },
             "properties": {
                 "OBJECTID": 1257,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1258,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12665014858555,
                     25.882788607907969
                 ]
             },
             "properties": {
                 "OBJECTID": 1258,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1259,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12658522113111,
                     25.882172408629799
                 ]
             },
             "properties": {
                 "OBJECTID": 1259,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1260,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12667973088492,
                     25.881694515189793
                 ]
             },
             "properties": {
                 "OBJECTID": 1260,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1261,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12690111969096,
                     25.881196136992173
                 ]
             },
             "properties": {
                 "OBJECTID": 1261,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1262,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12691794061055,
                     25.881073571987827
                 ]
             },
             "properties": {
                 "OBJECTID": 1262,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1263,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1276951041458,
                     25.880056528685884
                 ]
             },
             "properties": {
                 "OBJECTID": 1263,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1264,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12797700563436,
                     25.880150854978012
                 ]
             },
             "properties": {
                 "OBJECTID": 1264,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1265,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12818716910266,
                     25.880030265784171
                 ]
             },
             "properties": {
                 "OBJECTID": 1265,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1266,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12841097798429,
                     25.87926729534945
                 ]
             },
             "properties": {
                 "OBJECTID": 1266,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1267,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12848718743379,
                     25.878696877858658
                 ]
             },
             "properties": {
                 "OBJECTID": 1267,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1268,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12867899663843,
                     25.87772283923772
                 ]
             },
             "properties": {
                 "OBJECTID": 1268,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1269,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12870387818151,
                     25.877000052310621
                 ]
             },
             "properties": {
                 "OBJECTID": 1269,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1270,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1288220625882,
                     25.87670610620188
                 ]
             },
             "properties": {
                 "OBJECTID": 1270,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1271,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12888732009378,
                     25.876544194059363
                 ]
             },
             "properties": {
                 "OBJECTID": 1271,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1272,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.12913791798059,
                     25.876145701762766
                 ]
             },
             "properties": {
                 "OBJECTID": 1272,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1273,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13075744140286,
                     25.87465835889617
                 ]
             },
             "properties": {
                 "OBJECTID": 1273,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1274,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1312838784483,
                     25.874622940896074
                 ]
             },
             "properties": {
                 "OBJECTID": 1274,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1275,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13192655556617,
                     25.874623449013029
                 ]
             },
             "properties": {
                 "OBJECTID": 1275,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1276,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13284860977558,
                     25.874624691876079
                 ]
             },
             "properties": {
                 "OBJECTID": 1276,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1277,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13314140025545,
                     25.874667474424484
                 ]
             },
             "properties": {
                 "OBJECTID": 1277,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1278,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13298401440079,
                     25.874928320985248
                 ]
             },
             "properties": {
                 "OBJECTID": 1278,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1279,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13356666087276,
                     25.875819761372952
                 ]
             },
             "properties": {
                 "OBJECTID": 1279,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1280,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.134287546633,
                     25.876794388150529
                 ]
             },
             "properties": {
                 "OBJECTID": 1280,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1281,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13462697235695,
                     25.876885103664677
                 ]
             },
             "properties": {
                 "OBJECTID": 1281,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1282,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13536682571851,
                     25.876062616996251
                 ]
             },
             "properties": {
                 "OBJECTID": 1282,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1283,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13653776730416,
                     25.875473777792649
                 ]
             },
             "properties": {
                 "OBJECTID": 1283,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1284,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13761035543359,
                     25.875192361937991
                 ]
             },
             "properties": {
                 "OBJECTID": 1284,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1285,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13778648226042,
                     25.874805157032426
                 ]
             },
             "properties": {
                 "OBJECTID": 1285,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1286,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13758484706312,
                     25.874702119007623
                 ]
             },
             "properties": {
                 "OBJECTID": 1286,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1287,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13703114527118,
                     25.874419171607542
                 ]
             },
             "properties": {
                 "OBJECTID": 1287,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1288,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13707034042392,
                     25.874326598993378
                 ]
             },
             "properties": {
                 "OBJECTID": 1288,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1289,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13591377629979,
                     25.873791268552225
                 ]
             },
             "properties": {
                 "OBJECTID": 1289,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1290,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13564101821845,
                     25.87307734534329
                 ]
             },
             "properties": {
                 "OBJECTID": 1290,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1291,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13571126336416,
                     25.872935274943018
                 ]
             },
             "properties": {
                 "OBJECTID": 1291,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1292,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13548257296242,
                     25.872802704980927
                 ]
             },
             "properties": {
                 "OBJECTID": 1292,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1293,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13553248893328,
                     25.872256341656907
                 ]
             },
             "properties": {
                 "OBJECTID": 1293,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1294,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13559167421647,
                     25.872150888053113
                 ]
             },
             "properties": {
                 "OBJECTID": 1294,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1295,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13564725591618,
                     25.872048162093051
                 ]
             },
             "properties": {
                 "OBJECTID": 1295,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1296,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13574612828126,
                     25.872063812994611
                 ]
             },
             "properties": {
                 "OBJECTID": 1296,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1297,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13615284397861,
                     25.871607370184051
                 ]
             },
             "properties": {
                 "OBJECTID": 1297,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1298,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13623018477529,
                     25.871415320860422
                 ]
             },
             "properties": {
                 "OBJECTID": 1298,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1299,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13650621009356,
                     25.871446890661502
                 ]
             },
             "properties": {
                 "OBJECTID": 1299,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1300,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13662889560709,
                     25.871089143049574
                 ]
             },
             "properties": {
                 "OBJECTID": 1300,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1301,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13654006777,
                     25.87106103563832
                 ]
             },
             "properties": {
                 "OBJECTID": 1301,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1302,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13651203410313,
                     25.870859002940676
                 ]
             },
             "properties": {
                 "OBJECTID": 1302,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1303,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13635450795425,
                     25.870791953086211
                 ]
             },
             "properties": {
                 "OBJECTID": 1303,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1304,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13612300087584,
                     25.870764813345488
                 ]
             },
             "properties": {
                 "OBJECTID": 1304,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1305,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13626139125029,
                     25.870639416376036
                 ]
             },
             "properties": {
                 "OBJECTID": 1305,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1306,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13636327274691,
                     25.870646188270996
                 ]
             },
             "properties": {
                 "OBJECTID": 1306,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1307,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13638578097908,
                     25.87027510731042
                 ]
             },
             "properties": {
                 "OBJECTID": 1307,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1308,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13651689943543,
                     25.87027643830703
                 ]
             },
             "properties": {
                 "OBJECTID": 1308,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1309,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13650450138169,
                     25.870066962820147
                 ]
             },
             "properties": {
                 "OBJECTID": 1309,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1310,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13709627777104,
                     25.869599888224286
                 ]
             },
             "properties": {
                 "OBJECTID": 1310,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1311,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13710419000643,
                     25.869110368348856
                 ]
             },
             "properties": {
                 "OBJECTID": 1311,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1312,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1367170318656,
                     25.868630124980314
                 ]
             },
             "properties": {
                 "OBJECTID": 1312,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1313,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13649041709914,
                     25.868585050959382
                 ]
             },
             "properties": {
                 "OBJECTID": 1313,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1314,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13610895976075,
                     25.868515788772697
                 ]
             },
             "properties": {
                 "OBJECTID": 1314,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1315,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13557105006396,
                     25.86863768468146
                 ]
             },
             "properties": {
                 "OBJECTID": 1315,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1316,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13500690444516,
                     25.868731862585435
                 ]
             },
             "properties": {
                 "OBJECTID": 1316,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1317,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13479543606059,
                     25.868614839203531
                 ]
             },
             "properties": {
                 "OBJECTID": 1317,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1318,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13455734594481,
                     25.868441661253712
                 ]
             },
             "properties": {
                 "OBJECTID": 1318,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1319,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13447258214399,
                     25.868246710717187
                 ]
             },
             "properties": {
                 "OBJECTID": 1319,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1320,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13516843437583,
                     25.868189366346257
                 ]
             },
             "properties": {
                 "OBJECTID": 1320,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1321,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13562071692263,
                     25.867962519554681
                 ]
             },
             "properties": {
                 "OBJECTID": 1321,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1322,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13686015177467,
                     25.867683344810587
                 ]
             },
             "properties": {
                 "OBJECTID": 1322,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1323,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13755378178166,
                     25.86761528771541
                 ]
             },
             "properties": {
                 "OBJECTID": 1323,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1324,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1375361577675,
                     25.867726330605706
                 ]
             },
             "properties": {
                 "OBJECTID": 1324,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1325,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13754948572023,
                     25.867931110732229
                 ]
             },
             "properties": {
                 "OBJECTID": 1325,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1326,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13760966835156,
                     25.868047263570418
                 ]
             },
             "properties": {
                 "OBJECTID": 1326,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1327,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13775272261012,
                     25.868053816030852
                 ]
             },
             "properties": {
                 "OBJECTID": 1327,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1328,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1379781116006,
                     25.868099060023667
                 ]
             },
             "properties": {
                 "OBJECTID": 1328,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1329,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13806709142318,
                     25.868170296222218
                 ]
             },
             "properties": {
                 "OBJECTID": 1329,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1330,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13819757855549,
                     25.868159104159361
                 ]
             },
             "properties": {
                 "OBJECTID": 1330,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1331,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13894778491238,
                     25.868571293130742
                 ]
             },
             "properties": {
                 "OBJECTID": 1331,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1332,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.13935810779049,
                     25.869224135285663
                 ]
             },
             "properties": {
                 "OBJECTID": 1332,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1333,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1401762408409,
                     25.869144380708633
                 ]
             },
             "properties": {
                 "OBJECTID": 1333,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1334,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14111970880759,
                     25.869599763218559
                 ]
             },
             "properties": {
                 "OBJECTID": 1334,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1335,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1413832920054,
                     25.869504067258958
                 ]
             },
             "properties": {
                 "OBJECTID": 1335,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1336,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14262635112527,
                     25.869613675730591
                 ]
             },
             "properties": {
                 "OBJECTID": 1336,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1337,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14336954187092,
                     25.869372602563601
                 ]
             },
             "properties": {
                 "OBJECTID": 1337,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1338,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14352159834317,
                     25.869177371438582
                 ]
             },
             "properties": {
                 "OBJECTID": 1338,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1339,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14391077815992,
                     25.868923208639217
                 ]
             },
             "properties": {
                 "OBJECTID": 1339,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1340,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14407550787843,
                     25.8687916513137
                 ]
             },
             "properties": {
                 "OBJECTID": 1340,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1341,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14420766685038,
                     25.868451356844673
                 ]
             },
             "properties": {
                 "OBJECTID": 1341,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1342,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14425026683642,
                     25.868430498868463
                 ]
             },
             "properties": {
                 "OBJECTID": 1342,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1343,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14439127783527,
                     25.868050164783313
                 ]
             },
             "properties": {
                 "OBJECTID": 1343,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1344,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14419139631593,
                     25.86769463939612
                 ]
             },
             "properties": {
                 "OBJECTID": 1344,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1345,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14415357262931,
                     25.867395409770381
                 ]
             },
             "properties": {
                 "OBJECTID": 1345,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1346,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1443898632017,
                     25.867356712842081
                 ]
             },
             "properties": {
                 "OBJECTID": 1346,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1347,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14441385441592,
                     25.86752719012776
                 ]
             },
             "properties": {
                 "OBJECTID": 1347,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1348,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14471334934166,
                     25.867678251050506
                 ]
             },
             "properties": {
                 "OBJECTID": 1348,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1349,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14591126159553,
                     25.86743432703264
                 ]
             },
             "properties": {
                 "OBJECTID": 1349,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1350,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14684102389418,
                     25.867400431584713
                 ]
             },
             "properties": {
                 "OBJECTID": 1350,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1351,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14681388505284,
                     25.866986781215019
                 ]
             },
             "properties": {
                 "OBJECTID": 1351,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1352,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14755419167267,
                     25.866892748101918
                 ]
             },
             "properties": {
                 "OBJECTID": 1352,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1353,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14795157420582,
                     25.866941781837795
                 ]
             },
             "properties": {
                 "OBJECTID": 1353,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1354,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14844282167894,
                     25.867005509596652
                 ]
             },
             "properties": {
                 "OBJECTID": 1354,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1355,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14858247470153,
                     25.866317006624627
                 ]
             },
             "properties": {
                 "OBJECTID": 1355,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1356,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1489352913307,
                     25.866402319011854
                 ]
             },
             "properties": {
                 "OBJECTID": 1356,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1357,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14976281600121,
                     25.86642695234201
                 ]
             },
             "properties": {
                 "OBJECTID": 1357,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1358,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15007528634544,
                     25.866489299641501
                 ]
             },
             "properties": {
                 "OBJECTID": 1358,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1359,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15246541274865,
                     25.867140128214373
                 ]
             },
             "properties": {
                 "OBJECTID": 1359,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1360,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15274944742913,
                     25.867312484183856
                 ]
             },
             "properties": {
                 "OBJECTID": 1360,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1361,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15422450825452,
                     25.867427423836602
                 ]
             },
             "properties": {
                 "OBJECTID": 1361,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1362,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15442422969448,
                     25.86737432696367
                 ]
             },
             "properties": {
                 "OBJECTID": 1362,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1363,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15580886818481,
                     25.867449574138561
                 ]
             },
             "properties": {
                 "OBJECTID": 1363,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1364,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15586516034892,
                     25.867111506390984
                 ]
             },
             "properties": {
                 "OBJECTID": 1364,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1365,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15676291487671,
                     25.866071195829079
                 ]
             },
             "properties": {
                 "OBJECTID": 1365,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1366,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15679618259782,
                     25.865966379844622
                 ]
             },
             "properties": {
                 "OBJECTID": 1366,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1367,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1569608349746,
                     25.865723069164346
                 ]
             },
             "properties": {
                 "OBJECTID": 1367,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1368,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15730735994674,
                     25.865342328585655
                 ]
             },
             "properties": {
                 "OBJECTID": 1368,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1369,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15767819089587,
                     25.864925332737982
                 ]
             },
             "properties": {
                 "OBJECTID": 1369,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1370,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15804737068964,
                     25.864514283207654
                 ]
             },
             "properties": {
                 "OBJECTID": 1370,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1371,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15842058124491,
                     25.864097426754881
                 ]
             },
             "properties": {
                 "OBJECTID": 1371,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1372,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15849652539441,
                     25.863609420438479
                 ]
             },
             "properties": {
                 "OBJECTID": 1372,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1373,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15854279821161,
                     25.863321123870833
                 ]
             },
             "properties": {
                 "OBJECTID": 1373,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1374,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15860551333384,
                     25.862881207901466
                 ]
             },
             "properties": {
                 "OBJECTID": 1374,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1375,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15851528795025,
                     25.862859767164593
                 ]
             },
             "properties": {
                 "OBJECTID": 1375,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1376,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15852580282359,
                     25.862506883985532
                 ]
             },
             "properties": {
                 "OBJECTID": 1376,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1377,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1585530612748,
                     25.862330149217087
                 ]
             },
             "properties": {
                 "OBJECTID": 1377,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1378,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15885995132578,
                     25.862345011413197
                 ]
             },
             "properties": {
                 "OBJECTID": 1378,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1379,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15887677494328,
                     25.862133001636778
                 ]
             },
             "properties": {
                 "OBJECTID": 1379,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1380,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15875912203592,
                     25.862108503204922
                 ]
             },
             "properties": {
                 "OBJECTID": 1380,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1381,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15877978755719,
                     25.861904220403517
                 ]
             },
             "properties": {
                 "OBJECTID": 1381,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1382,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15857802465615,
                     25.861884885878851
                 ]
             },
             "properties": {
                 "OBJECTID": 1382,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1383,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15859296599263,
                     25.861699685791905
                 ]
             },
             "properties": {
                 "OBJECTID": 1383,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1384,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15856306443396,
                     25.861399680950569
                 ]
             },
             "properties": {
                 "OBJECTID": 1384,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1385,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15846623982515,
                     25.861226781790606
                 ]
             },
             "properties": {
                 "OBJECTID": 1385,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1386,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15871494823631,
                     25.861141369578604
                 ]
             },
             "properties": {
                 "OBJECTID": 1386,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1387,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15854610681743,
                     25.860734893100869
                 ]
             },
             "properties": {
                 "OBJECTID": 1387,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1388,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15842782078738,
                     25.860501551405548
                 ]
             },
             "properties": {
                 "OBJECTID": 1388,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1389,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15810221134802,
                     25.860168289634885
                 ]
             },
             "properties": {
                 "OBJECTID": 1389,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1390,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15801041574815,
                     25.859995459722711
                 ]
             },
             "properties": {
                 "OBJECTID": 1390,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1391,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15777189485715,
                     25.859514461822982
                 ]
             },
             "properties": {
                 "OBJECTID": 1391,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1392,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15708220657865,
                     25.8584385587925
                 ]
             },
             "properties": {
                 "OBJECTID": 1392,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1393,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15697022389679,
                     25.858276935332356
                 ]
             },
             "properties": {
                 "OBJECTID": 1393,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1394,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15667110938426,
                     25.857676530847243
                 ]
             },
             "properties": {
                 "OBJECTID": 1394,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1395,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15663161925386,
                     25.857483961715502
                 ]
             },
             "properties": {
                 "OBJECTID": 1395,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1396,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15649796021273,
                     25.856120244153999
                 ]
             },
             "properties": {
                 "OBJECTID": 1396,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1397,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1563385688699,
                     25.855620026842814
                 ]
             },
             "properties": {
                 "OBJECTID": 1397,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1398,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15624440805306,
                     25.855528257323272
                 ]
             },
             "properties": {
                 "OBJECTID": 1398,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1399,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15617064475953,
                     25.855478613847026
                 ]
             },
             "properties": {
                 "OBJECTID": 1399,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1400,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15581898915508,
                     25.855270980171667
                 ]
             },
             "properties": {
                 "OBJECTID": 1400,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1401,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15563866339369,
                     25.854934183166051
                 ]
             },
             "properties": {
                 "OBJECTID": 1401,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1402,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15563242659528,
                     25.854790788963783
                 ]
             },
             "properties": {
                 "OBJECTID": 1402,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1403,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15559906714327,
                     25.854169466247924
                 ]
             },
             "properties": {
                 "OBJECTID": 1403,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1404,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1550690912656,
                     25.853395578043546
                 ]
             },
             "properties": {
                 "OBJECTID": 1404,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1405,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15430340397899,
                     25.854005899352842
                 ]
             },
             "properties": {
                 "OBJECTID": 1405,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1406,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15384750525823,
                     25.853243384874361
                 ]
             },
             "properties": {
                 "OBJECTID": 1406,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1407,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15390249790204,
                     25.853177060772794
                 ]
             },
             "properties": {
                 "OBJECTID": 1407,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1408,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15394960529011,
                     25.852616328980446
                 ]
             },
             "properties": {
                 "OBJECTID": 1408,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1409,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15350540674916,
                     25.851408438748479
                 ]
             },
             "properties": {
                 "OBJECTID": 1409,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1410,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15304493677445,
                     25.85030882419295
                 ]
             },
             "properties": {
                 "OBJECTID": 1410,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1411,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15280524856342,
                     25.850317229256802
                 ]
             },
             "properties": {
                 "OBJECTID": 1411,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1412,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15278032834954,
                     25.85011179622245
                 ]
             },
             "properties": {
                 "OBJECTID": 1412,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1413,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1535576276824,
                     25.849587203686553
                 ]
             },
             "properties": {
                 "OBJECTID": 1413,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1414,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1533924438063,
                     25.848686178324499
                 ]
             },
             "properties": {
                 "OBJECTID": 1414,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1415,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15390342870035,
                     25.848300188403016
                 ]
             },
             "properties": {
                 "OBJECTID": 1415,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1416,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15369039189812,
                     25.846590137424926
                 ]
             },
             "properties": {
                 "OBJECTID": 1416,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1417,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15351862138732,
                     25.846288174759536
                 ]
             },
             "properties": {
                 "OBJECTID": 1417,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1418,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15370742595701,
                     25.845485844596112
                 ]
             },
             "properties": {
                 "OBJECTID": 1418,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1419,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15366698794122,
                     25.845031072725874
                 ]
             },
             "properties": {
                 "OBJECTID": 1419,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1420,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15360883058315,
                     25.84423814587376
                 ]
             },
             "properties": {
                 "OBJECTID": 1420,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1421,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15353674632382,
                     25.843564678667065
                 ]
             },
             "properties": {
                 "OBJECTID": 1421,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1422,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15320389464409,
                     25.843391124800632
                 ]
             },
             "properties": {
                 "OBJECTID": 1422,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1423,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15299418803141,
                     25.842632964839481
                 ]
             },
             "properties": {
                 "OBJECTID": 1423,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1424,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15282490414609,
                     25.842660183720568
                 ]
             },
             "properties": {
                 "OBJECTID": 1424,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1425,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15232835786748,
                     25.840987623679951
                 ]
             },
             "properties": {
                 "OBJECTID": 1425,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1426,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15211150614118,
                     25.840701287634374
                 ]
             },
             "properties": {
                 "OBJECTID": 1426,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1427,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15171457236966,
                     25.840155607795111
                 ]
             },
             "properties": {
                 "OBJECTID": 1427,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1428,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15187838927631,
                     25.840115241725073
                 ]
             },
             "properties": {
                 "OBJECTID": 1428,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1429,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15162803600509,
                     25.839528423298134
                 ]
             },
             "properties": {
                 "OBJECTID": 1429,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1430,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15148180534112,
                     25.839206637776158
                 ]
             },
             "properties": {
                 "OBJECTID": 1430,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1431,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1510666683921,
                     25.839342218668492
                 ]
             },
             "properties": {
                 "OBJECTID": 1431,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1432,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15060870842524,
                     25.8384037509324
                 ]
             },
             "properties": {
                 "OBJECTID": 1432,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1433,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15041775537514,
                     25.837772178643206
                 ]
             },
             "properties": {
                 "OBJECTID": 1433,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1434,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1503249561319,
                     25.837678202187305
                 ]
             },
             "properties": {
                 "OBJECTID": 1434,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1435,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14990380901361,
                     25.837256742105012
                 ]
             },
             "properties": {
                 "OBJECTID": 1435,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1436,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1490148984189,
                     25.836546129300473
                 ]
             },
             "properties": {
                 "OBJECTID": 1436,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1437,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14829207192162,
                     25.835784513244732
                 ]
             },
             "properties": {
                 "OBJECTID": 1437,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1438,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14817692632414,
                     25.83574374877503
                 ]
             },
             "properties": {
                 "OBJECTID": 1438,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1439,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14782759636654,
                     25.835535306609188
                 ]
             },
             "properties": {
                 "OBJECTID": 1439,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1440,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14758282878461,
                     25.835045552910003
                 ]
             },
             "properties": {
                 "OBJECTID": 1440,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1441,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14729869967533,
                     25.834025320612113
                 ]
             },
             "properties": {
                 "OBJECTID": 1441,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1442,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14760696389033,
                     25.834034364194622
                 ]
             },
             "properties": {
                 "OBJECTID": 1442,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1443,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1475488083309,
                     25.833819969416083
                 ]
             },
             "properties": {
                 "OBJECTID": 1443,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1444,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14748701411446,
                     25.833810029209531
                 ]
             },
             "properties": {
                 "OBJECTID": 1444,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1445,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14732564426311,
                     25.833401320614485
                 ]
             },
             "properties": {
                 "OBJECTID": 1445,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1446,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14717984077708,
                     25.832951521995199
                 ]
             },
             "properties": {
                 "OBJECTID": 1446,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1447,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14665173189195,
                     25.831726239774184
                 ]
             },
             "properties": {
                 "OBJECTID": 1447,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1448,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14668720924737,
                     25.831210724095683
                 ]
             },
             "properties": {
                 "OBJECTID": 1448,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1449,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14682104725358,
                     25.831251336579953
                 ]
             },
             "properties": {
                 "OBJECTID": 1449,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1450,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14700800461577,
                     25.831204302936271
                 ]
             },
             "properties": {
                 "OBJECTID": 1450,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1451,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14700426703337,
                     25.830723280754853
                 ]
             },
             "properties": {
                 "OBJECTID": 1451,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1452,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14711270998362,
                     25.830347010706021
                 ]
             },
             "properties": {
                 "OBJECTID": 1452,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1453,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14697097682915,
                     25.830305289357682
                 ]
             },
             "properties": {
                 "OBJECTID": 1453,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1454,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14704822679431,
                     25.830040246560202
                 ]
             },
             "properties": {
                 "OBJECTID": 1454,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1455,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1475862803826,
                     25.829197143833198
                 ]
             },
             "properties": {
                 "OBJECTID": 1455,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1456,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14767712000315,
                     25.828821234412487
                 ]
             },
             "properties": {
                 "OBJECTID": 1456,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1457,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14778158255405,
                     25.828716898666016
                 ]
             },
             "properties": {
                 "OBJECTID": 1457,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1458,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14761082737783,
                     25.828641416767994
                 ]
             },
             "properties": {
                 "OBJECTID": 1458,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1459,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14762211027221,
                     25.828486449190336
                 ]
             },
             "properties": {
                 "OBJECTID": 1459,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1460,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14855622169233,
                     25.827944024896908
                 ]
             },
             "properties": {
                 "OBJECTID": 1460,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1461,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14872693100313,
                     25.827762072261862
                 ]
             },
             "properties": {
                 "OBJECTID": 1461,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1462,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14886649679147,
                     25.827845534743687
                 ]
             },
             "properties": {
                 "OBJECTID": 1462,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1463,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1489374622941,
                     25.827837117089359
                 ]
             },
             "properties": {
                 "OBJECTID": 1463,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1464,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14897433719597,
                     25.827671700288761
                 ]
             },
             "properties": {
                 "OBJECTID": 1464,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1465,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14902515968345,
                     25.827557112270938
                 ]
             },
             "properties": {
                 "OBJECTID": 1465,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1466,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14909676190609,
                     25.827518518764691
                 ]
             },
             "properties": {
                 "OBJECTID": 1466,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1467,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14916440261516,
                     25.827205525914337
                 ]
             },
             "properties": {
                 "OBJECTID": 1467,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1468,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14924848023321,
                     25.826449937114944
                 ]
             },
             "properties": {
                 "OBJECTID": 1468,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1469,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14920798106351,
                     25.826367526839931
                 ]
             },
             "properties": {
                 "OBJECTID": 1469,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1470,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14906264702358,
                     25.826358649632027
                 ]
             },
             "properties": {
                 "OBJECTID": 1470,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1471,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1490685043081,
                     25.826287042912725
                 ]
             },
             "properties": {
                 "OBJECTID": 1471,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1472,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14884022309786,
                     25.82620650772418
                 ]
             },
             "properties": {
                 "OBJECTID": 1472,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1473,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14897082264542,
                     25.82559551821862
                 ]
             },
             "properties": {
                 "OBJECTID": 1473,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1474,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14902574334343,
                     25.82554944684955
                 ]
             },
             "properties": {
                 "OBJECTID": 1474,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1475,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14902991260044,
                     25.825313524099897
                 ]
             },
             "properties": {
                 "OBJECTID": 1475,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1476,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14877661944547,
                     25.824748418005129
                 ]
             },
             "properties": {
                 "OBJECTID": 1476,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1477,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14841841497793,
                     25.824675182613646
                 ]
             },
             "properties": {
                 "OBJECTID": 1477,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1478,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14790159438314,
                     25.824494006992893
                 ]
             },
             "properties": {
                 "OBJECTID": 1478,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1479,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.147980563852,
                     25.823845832319364
                 ]
             },
             "properties": {
                 "OBJECTID": 1479,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1480,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14805233334857,
                     25.823699453267182
                 ]
             },
             "properties": {
                 "OBJECTID": 1480,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1481,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14836647283454,
                     25.823689915057571
                 ]
             },
             "properties": {
                 "OBJECTID": 1481,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1482,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14871119916256,
                     25.823854128565245
                 ]
             },
             "properties": {
                 "OBJECTID": 1482,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1483,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14894371438095,
                     25.823841840228795
                 ]
             },
             "properties": {
                 "OBJECTID": 1483,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1484,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14899090180876,
                     25.823303920639489
                 ]
             },
             "properties": {
                 "OBJECTID": 1484,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1485,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14886461271175,
                     25.822976282329591
                 ]
             },
             "properties": {
                 "OBJECTID": 1485,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1486,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1488187670725,
                     25.822848590290278
                 ]
             },
             "properties": {
                 "OBJECTID": 1486,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1487,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14879296732164,
                     25.822735026600242
                 ]
             },
             "properties": {
                 "OBJECTID": 1487,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1488,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14892489876513,
                     25.822460861979266
                 ]
             },
             "properties": {
                 "OBJECTID": 1488,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1489,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14824403823184,
                     25.822035632838208
                 ]
             },
             "properties": {
                 "OBJECTID": 1489,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1490,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14839659922376,
                     25.821661225285368
                 ]
             },
             "properties": {
                 "OBJECTID": 1490,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1491,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.148418649701,
                     25.821487943014233
                 ]
             },
             "properties": {
                 "OBJECTID": 1491,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1492,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14846241970497,
                     25.821277751666969
                 ]
             },
             "properties": {
                 "OBJECTID": 1492,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1493,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14819656661831,
                     25.821229174786652
                 ]
             },
             "properties": {
                 "OBJECTID": 1493,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1494,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14810993762359,
                     25.821141613194925
                 ]
             },
             "properties": {
                 "OBJECTID": 1494,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1495,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14805267958758,
                     25.821007279662297
                 ]
             },
             "properties": {
                 "OBJECTID": 1495,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1496,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14810088055128,
                     25.820641975946501
                 ]
             },
             "properties": {
                 "OBJECTID": 1496,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1497,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14795746026863,
                     25.820356966400936
                 ]
             },
             "properties": {
                 "OBJECTID": 1497,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1498,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14771974427083,
                     25.820132115205013
                 ]
             },
             "properties": {
                 "OBJECTID": 1498,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1499,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14673153413304,
                     25.819019934422499
                 ]
             },
             "properties": {
                 "OBJECTID": 1499,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1500,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14666763999963,
                     25.818532037823331
                 ]
             },
             "properties": {
                 "OBJECTID": 1500,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1501,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1466970334414,
                     25.8179685604008
                 ]
             },
             "properties": {
                 "OBJECTID": 1501,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1502,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14615879639138,
                     25.817635531554572
                 ]
             },
             "properties": {
                 "OBJECTID": 1502,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1503,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14591702714915,
                     25.81746336983872
                 ]
             },
             "properties": {
                 "OBJECTID": 1503,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1504,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14576089764739,
                     25.817332064323352
                 ]
             },
             "properties": {
                 "OBJECTID": 1504,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1505,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.145713387363,
                     25.817267094600766
                 ]
             },
             "properties": {
                 "OBJECTID": 1505,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1506,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14471709681663,
                     25.816117433672673
                 ]
             },
             "properties": {
                 "OBJECTID": 1506,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1507,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14433714134606,
                     25.81571701412895
                 ]
             },
             "properties": {
                 "OBJECTID": 1507,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1508,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14394040992204,
                     25.815145421224258
                 ]
             },
             "properties": {
                 "OBJECTID": 1508,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1509,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14365635006055,
                     25.814697396967347
                 ]
             },
             "properties": {
                 "OBJECTID": 1509,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1510,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14357906232391,
                     25.814579922125745
                 ]
             },
             "properties": {
                 "OBJECTID": 1510,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1511,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14342008466923,
                     25.814095344024963
                 ]
             },
             "properties": {
                 "OBJECTID": 1511,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1512,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14348102542903,
                     25.81379481757682
                 ]
             },
             "properties": {
                 "OBJECTID": 1512,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1513,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.143449927772,
                     25.813465195362539
                 ]
             },
             "properties": {
                 "OBJECTID": 1513,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1514,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14338772706202,
                     25.813211555968564
                 ]
             },
             "properties": {
                 "OBJECTID": 1514,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1515,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14319058038103,
                     25.813224241805358
                 ]
             },
             "properties": {
                 "OBJECTID": 1515,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1516,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14310921242026,
                     25.81308278204483
                 ]
             },
             "properties": {
                 "OBJECTID": 1516,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1517,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14304111935223,
                     25.812859225872614
                 ]
             },
             "properties": {
                 "OBJECTID": 1517,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1518,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14296900631467,
                     25.812724553295538
                 ]
             },
             "properties": {
                 "OBJECTID": 1518,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1519,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14291444984207,
                     25.812577746166085
                 ]
             },
             "properties": {
                 "OBJECTID": 1519,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1520,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14290777237585,
                     25.812560918951306
                 ]
             },
             "properties": {
                 "OBJECTID": 1520,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1521,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14285375369781,
                     25.812579457575964
                 ]
             },
             "properties": {
                 "OBJECTID": 1521,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1522,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14268055416431,
                     25.812055433411558
                 ]
             },
             "properties": {
                 "OBJECTID": 1522,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1523,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14266404980611,
                     25.812042572207019
                 ]
             },
             "properties": {
                 "OBJECTID": 1523,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1524,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14245576142429,
                     25.812112740910322
                 ]
             },
             "properties": {
                 "OBJECTID": 1524,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1525,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14236457016887,
                     25.812094465787027
                 ]
             },
             "properties": {
                 "OBJECTID": 1525,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1526,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14239174318448,
                     25.811832483382432
                 ]
             },
             "properties": {
                 "OBJECTID": 1526,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1527,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14218060664973,
                     25.811348923314199
                 ]
             },
             "properties": {
                 "OBJECTID": 1527,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1528,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14217165120078,
                     25.810654000082081
                 ]
             },
             "properties": {
                 "OBJECTID": 1528,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1529,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14217957242937,
                     25.81040334733666
                 ]
             },
             "properties": {
                 "OBJECTID": 1529,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1530,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14213963083932,
                     25.810111094651347
                 ]
             },
             "properties": {
                 "OBJECTID": 1530,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1531,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14218253299754,
                     25.809607412252319
                 ]
             },
             "properties": {
                 "OBJECTID": 1531,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1532,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14219044972953,
                     25.809514462822278
                 ]
             },
             "properties": {
                 "OBJECTID": 1532,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1533,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14246066902473,
                     25.809022811553575
                 ]
             },
             "properties": {
                 "OBJECTID": 1533,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1534,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.142645917675,
                     25.808629876167913
                 ]
             },
             "properties": {
                 "OBJECTID": 1534,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1535,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14275225081576,
                     25.808438279203301
                 ]
             },
             "properties": {
                 "OBJECTID": 1535,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1536,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14291518728612,
                     25.80826186099614
                 ]
             },
             "properties": {
                 "OBJECTID": 1536,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1537,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14332822881477,
                     25.807398047781703
                 ]
             },
             "properties": {
                 "OBJECTID": 1537,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1538,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14289248390105,
                     25.807315817371148
                 ]
             },
             "properties": {
                 "OBJECTID": 1538,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1539,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1431170572065,
                     25.80589792644821
                 ]
             },
             "properties": {
                 "OBJECTID": 1539,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1540,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1432078698474,
                     25.80566503891049
                 ]
             },
             "properties": {
                 "OBJECTID": 1540,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1541,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14328061510838,
                     25.805683376986337
                 ]
             },
             "properties": {
                 "OBJECTID": 1541,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1542,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14333352312366,
                     25.804989552725715
                 ]
             },
             "properties": {
                 "OBJECTID": 1542,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1543,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14370550610431,
                     25.803139199414545
                 ]
             },
             "properties": {
                 "OBJECTID": 1543,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1544,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1438106629318,
                     25.802677592696796
                 ]
             },
             "properties": {
                 "OBJECTID": 1544,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1545,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14388298731006,
                     25.80236336327863
                 ]
             },
             "properties": {
                 "OBJECTID": 1545,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1546,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1446697843831,
                     25.802517818242791
                 ]
             },
             "properties": {
                 "OBJECTID": 1546,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1547,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14504236091631,
                     25.801624961422874
                 ]
             },
             "properties": {
                 "OBJECTID": 1547,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1548,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14504417934546,
                     25.801620554744829
                 ]
             },
             "properties": {
                 "OBJECTID": 1548,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1549,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14526506093387,
                     25.80114133930141
                 ]
             },
             "properties": {
                 "OBJECTID": 1549,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1550,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14498328355177,
                     25.800641883716025
                 ]
             },
             "properties": {
                 "OBJECTID": 1550,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1551,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14540200879571,
                     25.800737194765759
                 ]
             },
             "properties": {
                 "OBJECTID": 1551,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1552,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14551787834745,
                     25.799990798836347
                 ]
             },
             "properties": {
                 "OBJECTID": 1552,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1553,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14545286995406,
                     25.799623622732042
                 ]
             },
             "properties": {
                 "OBJECTID": 1553,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1554,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14472430218484,
                     25.799104737293817
                 ]
             },
             "properties": {
                 "OBJECTID": 1554,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1555,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14409767167029,
                     25.799346817701519
                 ]
             },
             "properties": {
                 "OBJECTID": 1555,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1556,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14406828002717,
                     25.798567758395393
                 ]
             },
             "properties": {
                 "OBJECTID": 1556,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1557,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14388018502257,
                     25.798083023812524
                 ]
             },
             "properties": {
                 "OBJECTID": 1557,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1558,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14395481076599,
                     25.797808480576975
                 ]
             },
             "properties": {
                 "OBJECTID": 1558,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1559,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14383004242256,
                     25.797593700888626
                 ]
             },
             "properties": {
                 "OBJECTID": 1559,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1560,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14377697612662,
                     25.797100965037544
                 ]
             },
             "properties": {
                 "OBJECTID": 1560,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1561,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14377579531674,
                     25.797092266794721
                 ]
             },
             "properties": {
                 "OBJECTID": 1561,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1562,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14367733394187,
                     25.797095257040553
                 ]
             },
             "properties": {
                 "OBJECTID": 1562,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1563,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14360040413538,
                     25.796667087116418
                 ]
             },
             "properties": {
                 "OBJECTID": 1563,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1564,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14351839585737,
                     25.796638992295698
                 ]
             },
             "properties": {
                 "OBJECTID": 1564,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1565,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14351495325258,
                     25.796430444009843
                 ]
             },
             "properties": {
                 "OBJECTID": 1565,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1566,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14343690379013,
                     25.796427246919961
                 ]
             },
             "properties": {
                 "OBJECTID": 1566,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1567,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1436109244047,
                     25.796349789211433
                 ]
             },
             "properties": {
                 "OBJECTID": 1567,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1568,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14361411969594,
                     25.795757235807855
                 ]
             },
             "properties": {
                 "OBJECTID": 1568,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1569,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14363436343518,
                     25.795264153717767
                 ]
             },
             "properties": {
                 "OBJECTID": 1569,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1570,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1435774210612,
                     25.794654354015279
                 ]
             },
             "properties": {
                 "OBJECTID": 1570,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1571,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14348853476815,
                     25.794642216764942
                 ]
             },
             "properties": {
                 "OBJECTID": 1571,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1572,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14345420494766,
                     25.794240616411344
                 ]
             },
             "properties": {
                 "OBJECTID": 1572,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1573,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14336862276377,
                     25.794229702239022
                 ]
             },
             "properties": {
                 "OBJECTID": 1573,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1574,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14385269634494,
                     25.79365472698413
                 ]
             },
             "properties": {
                 "OBJECTID": 1574,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1575,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14376737766241,
                     25.793243306933107
                 ]
             },
             "properties": {
                 "OBJECTID": 1575,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1576,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14376192057625,
                     25.793220849063005
                 ]
             },
             "properties": {
                 "OBJECTID": 1576,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1577,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14339982923877,
                     25.792365798842582
                 ]
             },
             "properties": {
                 "OBJECTID": 1577,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1578,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14338811736781,
                     25.792334827989976
                 ]
             },
             "properties": {
                 "OBJECTID": 1578,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1579,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14348471085077,
                     25.792308940105556
                 ]
             },
             "properties": {
                 "OBJECTID": 1579,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1580,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14329206527663,
                     25.791843915964023
                 ]
             },
             "properties": {
                 "OBJECTID": 1580,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1581,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14293075724873,
                     25.791308963238123
                 ]
             },
             "properties": {
                 "OBJECTID": 1581,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1582,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1429740011493,
                     25.790827330417017
                 ]
             },
             "properties": {
                 "OBJECTID": 1582,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1583,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14289890506046,
                     25.790291770648764
                 ]
             },
             "properties": {
                 "OBJECTID": 1583,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1584,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14276230613558,
                     25.789830604598762
                 ]
             },
             "properties": {
                 "OBJECTID": 1584,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1585,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14254105402648,
                     25.789869007448772
                 ]
             },
             "properties": {
                 "OBJECTID": 1585,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1586,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14258996995119,
                     25.789534373312733
                 ]
             },
             "properties": {
                 "OBJECTID": 1586,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1587,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.142434886361,
                     25.789255581679811
                 ]
             },
             "properties": {
                 "OBJECTID": 1587,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1588,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14238620066271,
                     25.788987449711101
                 ]
             },
             "properties": {
                 "OBJECTID": 1588,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1589,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14236365106171,
                     25.788586778357171
                 ]
             },
             "properties": {
                 "OBJECTID": 1589,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1590,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14258489417756,
                     25.788159112952769
                 ]
             },
             "properties": {
                 "OBJECTID": 1590,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1591,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14266833957225,
                     25.788024436778358
                 ]
             },
             "properties": {
                 "OBJECTID": 1591,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1592,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14245617421312,
                     25.78794949537297
                 ]
             },
             "properties": {
                 "OBJECTID": 1592,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1593,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14248615041555,
                     25.787835425364619
                 ]
             },
             "properties": {
                 "OBJECTID": 1593,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1594,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14250528978738,
                     25.787709978033092
                 ]
             },
             "properties": {
                 "OBJECTID": 1594,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1595,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14262887642155,
                     25.787554532016088
                 ]
             },
             "properties": {
                 "OBJECTID": 1595,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1596,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14282892971141,
                     25.787214179990485
                 ]
             },
             "properties": {
                 "OBJECTID": 1596,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1597,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14301475302847,
                     25.786884743036524
                 ]
             },
             "properties": {
                 "OBJECTID": 1597,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1598,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14361143791757,
                     25.785838677712832
                 ]
             },
             "properties": {
                 "OBJECTID": 1598,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1599,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14411393321149,
                     25.784947550289246
                 ]
             },
             "properties": {
                 "OBJECTID": 1599,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1600,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14420009635739,
                     25.784655082666006
                 ]
             },
             "properties": {
                 "OBJECTID": 1600,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1601,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14420475484565,
                     25.784637399296571
                 ]
             },
             "properties": {
                 "OBJECTID": 1601,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1602,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14435716205344,
                     25.784117664898361
                 ]
             },
             "properties": {
                 "OBJECTID": 1602,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1603,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14500376471204,
                     25.783006815112458
                 ]
             },
             "properties": {
                 "OBJECTID": 1603,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1604,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14524597732003,
                     25.782554386875518
                 ]
             },
             "properties": {
                 "OBJECTID": 1604,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1605,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14594823002687,
                     25.782053825123967
                 ]
             },
             "properties": {
                 "OBJECTID": 1605,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1606,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14608421381547,
                     25.781881013198245
                 ]
             },
             "properties": {
                 "OBJECTID": 1606,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1607,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1462356380643,
                     25.781575583646315
                 ]
             },
             "properties": {
                 "OBJECTID": 1607,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1608,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.146320225598,
                     25.781179442177574
                 ]
             },
             "properties": {
                 "OBJECTID": 1608,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1609,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1464463321326,
                     25.780616059183842
                 ]
             },
             "properties": {
                 "OBJECTID": 1609,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1610,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14647067408242,
                     25.780507312262728
                 ]
             },
             "properties": {
                 "OBJECTID": 1610,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1611,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14716886365221,
                     25.778777549044719
                 ]
             },
             "properties": {
                 "OBJECTID": 1611,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1612,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14747657208619,
                     25.778608993610192
                 ]
             },
             "properties": {
                 "OBJECTID": 1612,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1613,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14749602262339,
                     25.778346640684958
                 ]
             },
             "properties": {
                 "OBJECTID": 1613,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1614,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14761821530846,
                     25.777769024121199
                 ]
             },
             "properties": {
                 "OBJECTID": 1614,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1615,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14781168825886,
                     25.777818812388318
                 ]
             },
             "properties": {
                 "OBJECTID": 1615,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1616,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14802326006549,
                     25.777476845180331
                 ]
             },
             "properties": {
                 "OBJECTID": 1616,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1617,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14810103973127,
                     25.777310392360789
                 ]
             },
             "properties": {
                 "OBJECTID": 1617,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1618,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14824959514277,
                     25.777355619266473
                 ]
             },
             "properties": {
                 "OBJECTID": 1618,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1619,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14841867218405,
                     25.776479423286787
                 ]
             },
             "properties": {
                 "OBJECTID": 1619,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1620,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14877173882473,
                     25.776254188080372
                 ]
             },
             "properties": {
                 "OBJECTID": 1620,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1621,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14859330433757,
                     25.776204103936323
                 ]
             },
             "properties": {
                 "OBJECTID": 1621,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1622,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14863164423508,
                     25.776016199588014
                 ]
             },
             "properties": {
                 "OBJECTID": 1622,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1623,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14889480295284,
                     25.776054455848509
                 ]
             },
             "properties": {
                 "OBJECTID": 1623,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1624,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14891806301824,
                     25.775436894996744
                 ]
             },
             "properties": {
                 "OBJECTID": 1624,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1625,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14895668979943,
                     25.775278141273247
                 ]
             },
             "properties": {
                 "OBJECTID": 1625,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1626,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14887667891566,
                     25.774959462009633
                 ]
             },
             "properties": {
                 "OBJECTID": 1626,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1627,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14899848219426,
                     25.774549070783053
                 ]
             },
             "properties": {
                 "OBJECTID": 1627,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1628,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14890371433501,
                     25.774153843025488
                 ]
             },
             "properties": {
                 "OBJECTID": 1628,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1629,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14907628344378,
                     25.773663102770001
                 ]
             },
             "properties": {
                 "OBJECTID": 1629,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1630,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14904514261929,
                     25.773389324857533
                 ]
             },
             "properties": {
                 "OBJECTID": 1630,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1631,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14901171391955,
                     25.773336734302916
                 ]
             },
             "properties": {
                 "OBJECTID": 1631,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1632,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14876940778203,
                     25.773141753189407
                 ]
             },
             "properties": {
                 "OBJECTID": 1632,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1633,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14880052072749,
                     25.77290232308377
                 ]
             },
             "properties": {
                 "OBJECTID": 1633,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1634,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14929879460374,
                     25.772753569821418
                 ]
             },
             "properties": {
                 "OBJECTID": 1634,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1635,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14937636113024,
                     25.772517478898578
                 ]
             },
             "properties": {
                 "OBJECTID": 1635,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1636,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14977203225357,
                     25.772038916362931
                 ]
             },
             "properties": {
                 "OBJECTID": 1636,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1637,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15004359063926,
                     25.771563165108034
                 ]
             },
             "properties": {
                 "OBJECTID": 1637,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1638,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15007717402244,
                     25.771581542754063
                 ]
             },
             "properties": {
                 "OBJECTID": 1638,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1639,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15045546484788,
                     25.771180068305569
                 ]
             },
             "properties": {
                 "OBJECTID": 1639,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1640,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15051644967446,
                     25.771080230967925
                 ]
             },
             "properties": {
                 "OBJECTID": 1640,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1641,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15057490111087,
                     25.770971958888822
                 ]
             },
             "properties": {
                 "OBJECTID": 1641,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1642,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1506274269143,
                     25.77037531536854
                 ]
             },
             "properties": {
                 "OBJECTID": 1642,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1643,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1495122828656,
                     25.770127356092644
                 ]
             },
             "properties": {
                 "OBJECTID": 1643,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1644,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1492351549785,
                     25.769982361996483
                 ]
             },
             "properties": {
                 "OBJECTID": 1644,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1645,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14932865389432,
                     25.769386951446677
                 ]
             },
             "properties": {
                 "OBJECTID": 1645,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1646,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14907568089802,
                     25.768995665417663
                 ]
             },
             "properties": {
                 "OBJECTID": 1646,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1647,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14896581162299,
                     25.769032870370722
                 ]
             },
             "properties": {
                 "OBJECTID": 1647,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1648,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14881986064813,
                     25.768862357112141
                 ]
             },
             "properties": {
                 "OBJECTID": 1648,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1649,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14866885098672,
                     25.768729725996081
                 ]
             },
             "properties": {
                 "OBJECTID": 1649,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1650,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14834136825959,
                     25.768405139985248
                 ]
             },
             "properties": {
                 "OBJECTID": 1650,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1651,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14824600594847,
                     25.76837410528077
                 ]
             },
             "properties": {
                 "OBJECTID": 1651,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1652,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14806605700301,
                     25.767697333964861
                 ]
             },
             "properties": {
                 "OBJECTID": 1652,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1653,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14780746504255,
                     25.767734869868377
                 ]
             },
             "properties": {
                 "OBJECTID": 1653,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1654,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1472978803929,
                     25.766863278417588
                 ]
             },
             "properties": {
                 "OBJECTID": 1654,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1655,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14690446027271,
                     25.766210444356545
                 ]
             },
             "properties": {
                 "OBJECTID": 1655,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1656,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14687768745534,
                     25.765825330374753
                 ]
             },
             "properties": {
                 "OBJECTID": 1656,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1657,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14688277132291,
                     25.765772101301479
                 ]
             },
             "properties": {
                 "OBJECTID": 1657,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1658,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14695875414327,
                     25.765541479156013
                 ]
             },
             "properties": {
                 "OBJECTID": 1658,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1659,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14700999031885,
                     25.76518086360602
                 ]
             },
             "properties": {
                 "OBJECTID": 1659,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1660,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1475624654355,
                     25.764357464125737
                 ]
             },
             "properties": {
                 "OBJECTID": 1660,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1661,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14759983406515,
                     25.763969968739104
                 ]
             },
             "properties": {
                 "OBJECTID": 1661,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1662,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14839500832301,
                     25.763537917341068
                 ]
             },
             "properties": {
                 "OBJECTID": 1662,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1663,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14838673456023,
                     25.762441998099803
                 ]
             },
             "properties": {
                 "OBJECTID": 1663,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1664,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14840852873067,
                     25.762370919282546
                 ]
             },
             "properties": {
                 "OBJECTID": 1664,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1665,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14854981402277,
                     25.761975143837844
                 ]
             },
             "properties": {
                 "OBJECTID": 1665,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1666,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14874302886773,
                     25.761270977373783
                 ]
             },
             "properties": {
                 "OBJECTID": 1666,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1667,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14887482811088,
                     25.761134942323793
                 ]
             },
             "properties": {
                 "OBJECTID": 1667,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1668,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14900239514446,
                     25.760900266034525
                 ]
             },
             "properties": {
                 "OBJECTID": 1668,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1669,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14914827687153,
                     25.760709061174282
                 ]
             },
             "properties": {
                 "OBJECTID": 1669,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1670,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.14925570538657,
                     25.760741800094024
                 ]
             },
             "properties": {
                 "OBJECTID": 1670,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1671,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1495171077284,
                     25.760311038323835
                 ]
             },
             "properties": {
                 "OBJECTID": 1671,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1672,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15017228002677,
                     25.759443893121215
                 ]
             },
             "properties": {
                 "OBJECTID": 1672,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1673,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15115247720513,
                     25.759715470392678
                 ]
             },
             "properties": {
                 "OBJECTID": 1673,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1674,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15115444492176,
                     25.759465742150326
                 ]
             },
             "properties": {
                 "OBJECTID": 1674,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1675,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15123700988016,
                     25.759178700136943
                 ]
             },
             "properties": {
                 "OBJECTID": 1675,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1676,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15126710209512,
                     25.759060162296691
                 ]
             },
             "properties": {
                 "OBJECTID": 1676,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1677,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15094348735209,
                     25.758957682750918
                 ]
             },
             "properties": {
                 "OBJECTID": 1677,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1678,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15100444160174,
                     25.758775608707424
                 ]
             },
             "properties": {
                 "OBJECTID": 1678,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1679,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1513057891309,
                     25.75858401713873
                 ]
             },
             "properties": {
                 "OBJECTID": 1679,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1680,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15129864581593,
                     25.758250916346753
                 ]
             },
             "properties": {
                 "OBJECTID": 1680,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1681,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15169943228301,
                     25.757977219373231
                 ]
             },
             "properties": {
                 "OBJECTID": 1681,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1682,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15232739379422,
                     25.757525934174566
                 ]
             },
             "properties": {
                 "OBJECTID": 1682,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1683,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15235433568409,
                     25.757520969017548
                 ]
             },
             "properties": {
                 "OBJECTID": 1683,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1684,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1523774050932,
                     25.757351897372189
                 ]
             },
             "properties": {
                 "OBJECTID": 1684,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1685,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15245782606786,
                     25.757352193249176
                 ]
             },
             "properties": {
                 "OBJECTID": 1685,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1686,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15250466995462,
                     25.757085457028268
                 ]
             },
             "properties": {
                 "OBJECTID": 1686,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1687,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15257847281833,
                     25.756790459612091
                 ]
             },
             "properties": {
                 "OBJECTID": 1687,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1688,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1527714790206,
                     25.756577079268823
                 ]
             },
             "properties": {
                 "OBJECTID": 1688,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1689,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15255762923124,
                     25.75628381214824
                 ]
             },
             "properties": {
                 "OBJECTID": 1689,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1690,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1525953800728,
                     25.755819856401331
                 ]
             },
             "properties": {
                 "OBJECTID": 1690,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1691,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15241882966529,
                     25.755792608741956
                 ]
             },
             "properties": {
                 "OBJECTID": 1691,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1692,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15241824420667,
                     25.75561326773817
                 ]
             },
             "properties": {
                 "OBJECTID": 1692,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1693,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15232302039112,
                     25.755248089028157
                 ]
             },
             "properties": {
                 "OBJECTID": 1693,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1694,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15245691505464,
                     25.75517177975388
                 ]
             },
             "properties": {
                 "OBJECTID": 1694,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1695,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15247681615222,
                     25.754981763797389
                 ]
             },
             "properties": {
                 "OBJECTID": 1695,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1696,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15247876408375,
                     25.754579911633641
                 ]
             },
             "properties": {
                 "OBJECTID": 1696,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1697,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15250761883158,
                     25.754146850296934
                 ]
             },
             "properties": {
                 "OBJECTID": 1697,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1698,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15253963469644,
                     25.752688295628388
                 ]
             },
             "properties": {
                 "OBJECTID": 1698,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1699,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15289803701484,
                     25.751986384663951
                 ]
             },
             "properties": {
                 "OBJECTID": 1699,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1700,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15296930918635,
                     25.75181361320773
                 ]
             },
             "properties": {
                 "OBJECTID": 1700,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1701,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15316953964259,
                     25.751243094992901
                 ]
             },
             "properties": {
                 "OBJECTID": 1701,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1702,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15338209710615,
                     25.750782402885648
                 ]
             },
             "properties": {
                 "OBJECTID": 1702,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1703,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1539833532492,
                     25.750367140031528
                 ]
             },
             "properties": {
                 "OBJECTID": 1703,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1704,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15441094041262,
                     25.750130202846663
                 ]
             },
             "properties": {
                 "OBJECTID": 1704,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1705,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15450847728448,
                     25.749988053306083
                 ]
             },
             "properties": {
                 "OBJECTID": 1705,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1706,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15470873112309,
                     25.750085327575846
                 ]
             },
             "properties": {
                 "OBJECTID": 1706,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1707,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15495192129418,
                     25.749706272326648
                 ]
             },
             "properties": {
                 "OBJECTID": 1707,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1708,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15457864868574,
                     25.749507380061573
                 ]
             },
             "properties": {
                 "OBJECTID": 1708,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1709,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15536008220209,
                     25.748865061773245
                 ]
             },
             "properties": {
                 "OBJECTID": 1709,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1710,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1554305027156,
                     25.748888174349815
                 ]
             },
             "properties": {
                 "OBJECTID": 1710,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1711,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15655411567997,
                     25.74823908776375
                 ]
             },
             "properties": {
                 "OBJECTID": 1711,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1712,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15723075209758,
                     25.747756757068714
                 ]
             },
             "properties": {
                 "OBJECTID": 1712,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1713,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15697157108121,
                     25.746856059059951
                 ]
             },
             "properties": {
                 "OBJECTID": 1713,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1714,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15721144455256,
                     25.746598078638442
                 ]
             },
             "properties": {
                 "OBJECTID": 1714,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1715,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15743414906677,
                     25.746400975124971
                 ]
             },
             "properties": {
                 "OBJECTID": 1715,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1716,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1583717327693,
                     25.74557000065505
                 ]
             },
             "properties": {
                 "OBJECTID": 1716,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1717,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15885702583114,
                     25.74511383303701
                 ]
             },
             "properties": {
                 "OBJECTID": 1717,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1718,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15972097034654,
                     25.743738720165879
                 ]
             },
             "properties": {
                 "OBJECTID": 1718,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1719,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15979306000179,
                     25.743614446449612
                 ]
             },
             "properties": {
                 "OBJECTID": 1719,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1720,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.15988634218098,
                     25.743122743020251
                 ]
             },
             "properties": {
                 "OBJECTID": 1720,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1721,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16092811683916,
                     25.742547275836159
                 ]
             },
             "properties": {
                 "OBJECTID": 1721,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1722,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16099526022316,
                     25.742522093919547
                 ]
             },
             "properties": {
                 "OBJECTID": 1722,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1723,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16200145320454,
                     25.742449011412816
                 ]
             },
             "properties": {
                 "OBJECTID": 1723,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1724,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16224201375798,
                     25.742360066663821
                 ]
             },
             "properties": {
                 "OBJECTID": 1724,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1725,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16256179918776,
                     25.742149861826704
                 ]
             },
             "properties": {
                 "OBJECTID": 1725,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1726,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16262917369744,
                     25.742056843148816
                 ]
             },
             "properties": {
                 "OBJECTID": 1726,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1727,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16283494937346,
                     25.741723523821634
                 ]
             },
             "properties": {
                 "OBJECTID": 1727,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1728,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16331706692915,
                     25.741417010586645
                 ]
             },
             "properties": {
                 "OBJECTID": 1728,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1729,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16336669151968,
                     25.741362295833312
                 ]
             },
             "properties": {
                 "OBJECTID": 1729,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1730,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16354968287163,
                     25.741069764358201
                 ]
             },
             "properties": {
                 "OBJECTID": 1730,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1731,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16366466119524,
                     25.740861460687938
                 ]
             },
             "properties": {
                 "OBJECTID": 1731,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1732,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1638643529576,
                     25.740704141383048
                 ]
             },
             "properties": {
                 "OBJECTID": 1732,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1733,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16419709941664,
                     25.74043309740955
                 ]
             },
             "properties": {
                 "OBJECTID": 1733,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1734,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16460233033331,
                     25.740126714576263
                 ]
             },
             "properties": {
                 "OBJECTID": 1734,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1735,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16580340820082,
                     25.739745598080958
                 ]
             },
             "properties": {
                 "OBJECTID": 1735,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1736,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16588860907211,
                     25.739510294964191
                 ]
             },
             "properties": {
                 "OBJECTID": 1736,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1737,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16634339533147,
                     25.7389207479942
                 ]
             },
             "properties": {
                 "OBJECTID": 1737,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1738,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16654203578639,
                     25.738531400004149
                 ]
             },
             "properties": {
                 "OBJECTID": 1738,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1739,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16632842881398,
                     25.73811505436629
                 ]
             },
             "properties": {
                 "OBJECTID": 1739,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1740,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16720075411155,
                     25.738311707319497
                 ]
             },
             "properties": {
                 "OBJECTID": 1740,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1741,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16761363915816,
                     25.738794957121627
                 ]
             },
             "properties": {
                 "OBJECTID": 1741,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1742,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16808686511678,
                     25.738723991618997
                 ]
             },
             "properties": {
                 "OBJECTID": 1742,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1743,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16894219682496,
                     25.738398917276243
                 ]
             },
             "properties": {
                 "OBJECTID": 1743,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1744,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.16911971220503,
                     25.738333772185911
                 ]
             },
             "properties": {
                 "OBJECTID": 1744,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1745,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1697289246502,
                     25.738254615658036
                 ]
             },
             "properties": {
                 "OBJECTID": 1745,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1746,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1701429707216,
                     25.738203456824124
                 ]
             },
             "properties": {
                 "OBJECTID": 1746,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1747,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17094964820774,
                     25.737764543598871
                 ]
             },
             "properties": {
                 "OBJECTID": 1747,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1748,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17086924162226,
                     25.73758036334317
                 ]
             },
             "properties": {
                 "OBJECTID": 1748,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1749,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17103685007061,
                     25.737145354074869
                 ]
             },
             "properties": {
                 "OBJECTID": 1749,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1750,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17082256590874,
                     25.736720788633534
                 ]
             },
             "properties": {
                 "OBJECTID": 1750,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1751,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17239073444489,
                     25.735352119404865
                 ]
             },
             "properties": {
                 "OBJECTID": 1751,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1752,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17371276303362,
                     25.734451159693322
                 ]
             },
             "properties": {
                 "OBJECTID": 1752,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1753,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17410300315112,
                     25.734088973027724
                 ]
             },
             "properties": {
                 "OBJECTID": 1753,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1754,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17505124381989,
                     25.733114505430137
                 ]
             },
             "properties": {
                 "OBJECTID": 1754,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1755,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17622861555822,
                     25.733509007434805
                 ]
             },
             "properties": {
                 "OBJECTID": 1755,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1756,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17703012104329,
                     25.733642025259314
                 ]
             },
             "properties": {
                 "OBJECTID": 1756,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1757,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17758292621113,
                     25.733552758553003
                 ]
             },
             "properties": {
                 "OBJECTID": 1757,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1758,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.17813909394408,
                     25.733537104054165
                 ]
             },
             "properties": {
                 "OBJECTID": 1758,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1759,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18117011321021,
                     25.732829109176123
                 ]
             },
             "properties": {
                 "OBJECTID": 1759,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1760,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18229138344066,
                     25.732801723021169
                 ]
             },
             "properties": {
                 "OBJECTID": 1760,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1761,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18322920006761,
                     25.732763577377227
                 ]
             },
             "properties": {
                 "OBJECTID": 1761,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1762,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18347271039733,
                     25.732709254728377
                 ]
             },
             "properties": {
                 "OBJECTID": 1762,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1763,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18366564645248,
                     25.732716452902025
                 ]
             },
             "properties": {
                 "OBJECTID": 1763,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1764,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18550848233065,
                     25.732526584434368
                 ]
             },
             "properties": {
                 "OBJECTID": 1764,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1765,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1860126422697,
                     25.732229154352069
                 ]
             },
             "properties": {
                 "OBJECTID": 1765,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1766,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18634257205213,
                     25.732038605097614
                 ]
             },
             "properties": {
                 "OBJECTID": 1766,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1767,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18638552007576,
                     25.732010373579953
                 ]
             },
             "properties": {
                 "OBJECTID": 1767,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1768,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1871971033637,
                     25.730542186272942
                 ]
             },
             "properties": {
                 "OBJECTID": 1768,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1769,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18720241565899,
                     25.730525317689285
                 ]
             },
             "properties": {
                 "OBJECTID": 1769,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1770,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18793919873508,
                     25.729528582877833
                 ]
             },
             "properties": {
                 "OBJECTID": 1770,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1771,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18795244934614,
                     25.72931841221498
                 ]
             },
             "properties": {
                 "OBJECTID": 1771,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1772,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18738474421065,
                     25.729251528735119
                 ]
             },
             "properties": {
                 "OBJECTID": 1772,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1773,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18753876030559,
                     25.728909670345047
                 ]
             },
             "properties": {
                 "OBJECTID": 1773,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1774,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18895304584646,
                     25.728306637141657
                 ]
             },
             "properties": {
                 "OBJECTID": 1774,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1775,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18905268713189,
                     25.728052887131071
                 ]
             },
             "properties": {
                 "OBJECTID": 1775,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1776,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18909045775848,
                     25.728058470122335
                 ]
             },
             "properties": {
                 "OBJECTID": 1776,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1777,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18923367299567,
                     25.727673841774447
                 ]
             },
             "properties": {
                 "OBJECTID": 1777,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1778,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18939440702633,
                     25.727642623608233
                 ]
             },
             "properties": {
                 "OBJECTID": 1778,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1779,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18943427397261,
                     25.727558101725037
                 ]
             },
             "properties": {
                 "OBJECTID": 1779,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1780,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18934076696286,
                     25.72650978809628
                 ]
             },
             "properties": {
                 "OBJECTID": 1780,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1781,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18933389704176,
                     25.726320643582881
                 ]
             },
             "properties": {
                 "OBJECTID": 1781,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1782,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18925412807562,
                     25.726301584250791
                 ]
             },
             "properties": {
                 "OBJECTID": 1782,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1783,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18942051974125,
                     25.725660312773243
                 ]
             },
             "properties": {
                 "OBJECTID": 1783,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1784,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18950942042346,
                     25.725371409163245
                 ]
             },
             "properties": {
                 "OBJECTID": 1784,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1785,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18969390195207,
                     25.725732489662732
                 ]
             },
             "properties": {
                 "OBJECTID": 1785,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1786,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.18983241283564,
                     25.725768538986927
                 ]
             },
             "properties": {
                 "OBJECTID": 1786,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1787,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19138124594781,
                     25.722126255022715
                 ]
             },
             "properties": {
                 "OBJECTID": 1787,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1788,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19148443685731,
                     25.722109140024827
                 ]
             },
             "properties": {
                 "OBJECTID": 1788,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1789,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19191395486519,
                     25.721786054083168
                 ]
             },
             "properties": {
                 "OBJECTID": 1789,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1790,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19211824036455,
                     25.721897097872841
                 ]
             },
             "properties": {
                 "OBJECTID": 1790,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1791,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19193866733571,
                     25.720239989983611
                 ]
             },
             "properties": {
                 "OBJECTID": 1791,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1792,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19208555270615,
                     25.719857041569242
                 ]
             },
             "properties": {
                 "OBJECTID": 1792,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1793,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19207222295478,
                     25.719662861751715
                 ]
             },
             "properties": {
                 "OBJECTID": 1793,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1794,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19335887221422,
                     25.718507439766597
                 ]
             },
             "properties": {
                 "OBJECTID": 1794,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1795,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1935135484116,
                     25.717714996749748
                 ]
             },
             "properties": {
                 "OBJECTID": 1795,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1796,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19366514712891,
                     25.717195340592525
                 ]
             },
             "properties": {
                 "OBJECTID": 1796,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1797,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19518231691046,
                     25.715453019241522
                 ]
             },
             "properties": {
                 "OBJECTID": 1797,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1798,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19566385260475,
                     25.714501194774357
                 ]
             },
             "properties": {
                 "OBJECTID": 1798,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1799,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19583199884772,
                     25.714254990975121
                 ]
             },
             "properties": {
                 "OBJECTID": 1799,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1800,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19587605303747,
                     25.714275249103537
                 ]
             },
             "properties": {
                 "OBJECTID": 1800,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1801,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19602452930866,
                     25.714088072306765
                 ]
             },
             "properties": {
                 "OBJECTID": 1801,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1802,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1965089842023,
                     25.713554669112739
                 ]
             },
             "properties": {
                 "OBJECTID": 1802,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1803,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19671054385657,
                     25.71333760784438
                 ]
             },
             "properties": {
                 "OBJECTID": 1803,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1804,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19682311559438,
                     25.713200097006904
                 ]
             },
             "properties": {
                 "OBJECTID": 1804,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1805,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19693927113053,
                     25.712938915898292
                 ]
             },
             "properties": {
                 "OBJECTID": 1805,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1806,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1969676177614,
                     25.712938788194549
                 ]
             },
             "properties": {
                 "OBJECTID": 1806,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1807,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19738015926697,
                     25.712333964440973
                 ]
             },
             "properties": {
                 "OBJECTID": 1807,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1808,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19756599697325,
                     25.712081042706018
                 ]
             },
             "properties": {
                 "OBJECTID": 1808,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1809,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19757458100219,
                     25.712034651178271
                 ]
             },
             "properties": {
                 "OBJECTID": 1809,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1810,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19765389491135,
                     25.711995350804841
                 ]
             },
             "properties": {
                 "OBJECTID": 1810,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1811,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19771595172983,
                     25.711910751579978
                 ]
             },
             "properties": {
                 "OBJECTID": 1811,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1812,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19811456633425,
                     25.711400809899487
                 ]
             },
             "properties": {
                 "OBJECTID": 1812,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1813,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19860727070903,
                     25.710769012779792
                 ]
             },
             "properties": {
                 "OBJECTID": 1813,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1814,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19871924619633,
                     25.710587729240387
                 ]
             },
             "properties": {
                 "OBJECTID": 1814,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1815,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.1989500931723,
                     25.71021399707837
                 ]
             },
             "properties": {
                 "OBJECTID": 1815,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1816,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19923923060605,
                     25.709688292980388
                 ]
             },
             "properties": {
                 "OBJECTID": 1816,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1817,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19931787272174,
                     25.709563214370917
                 ]
             },
             "properties": {
                 "OBJECTID": 1817,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1818,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19938420401786,
                     25.70935792163084
                 ]
             },
             "properties": {
                 "OBJECTID": 1818,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1819,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.19991650374374,
                     25.708608175726795
                 ]
             },
             "properties": {
                 "OBJECTID": 1819,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1820,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2000108408277,
                     25.708467539745186
                 ]
             },
             "properties": {
                 "OBJECTID": 1820,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1821,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20008278299412,
                     25.708355799880337
                 ]
             },
             "properties": {
                 "OBJECTID": 1821,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1822,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20024800464176,
                     25.708201188434145
                 ]
             },
             "properties": {
                 "OBJECTID": 1822,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1823,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20045711590325,
                     25.707960235776284
                 ]
             },
             "properties": {
                 "OBJECTID": 1823,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1824,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20055184868892,
                     25.707922207943568
                 ]
             },
             "properties": {
                 "OBJECTID": 1824,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1825,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20068423339075,
                     25.707728631571115
                 ]
             },
             "properties": {
                 "OBJECTID": 1825,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1826,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20104199808981,
                     25.707312115961429
                 ]
             },
             "properties": {
                 "OBJECTID": 1826,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1827,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20111924985355,
                     25.707257760936955
                 ]
             },
             "properties": {
                 "OBJECTID": 1827,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1828,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20114822600988,
                     25.707221469695128
                 ]
             },
             "properties": {
                 "OBJECTID": 1828,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1829,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20180764490692,
                     25.70599396435
                 ]
             },
             "properties": {
                 "OBJECTID": 1829,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1830,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20210595182829,
                     25.705444755571079
                 ]
             },
             "properties": {
                 "OBJECTID": 1830,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1831,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20213308887099,
                     25.70540008444641
                 ]
             },
             "properties": {
                 "OBJECTID": 1831,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1832,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20435589191095,
                     25.702383347313685
                 ]
             },
             "properties": {
                 "OBJECTID": 1832,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1833,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20455150165054,
                     25.701843775669772
                 ]
             },
             "properties": {
                 "OBJECTID": 1833,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1834,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20469735280068,
                     25.70185214925732
                 ]
             },
             "properties": {
                 "OBJECTID": 1834,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1835,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20473878097005,
                     25.701704377155352
                 ]
             },
             "properties": {
                 "OBJECTID": 1835,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1836,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20505413813805,
                     25.700522971058319
                 ]
             },
             "properties": {
                 "OBJECTID": 1836,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1837,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20614722091756,
                     25.69803883132704
                 ]
             },
             "properties": {
                 "OBJECTID": 1837,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1838,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20628279191737,
                     25.697522522446491
                 ]
             },
             "properties": {
                 "OBJECTID": 1838,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1839,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20649484126398,
                     25.697018626908175
                 ]
             },
             "properties": {
                 "OBJECTID": 1839,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1840,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20654626359925,
                     25.696950641758747
                 ]
             },
             "properties": {
                 "OBJECTID": 1840,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1841,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20659777316877,
                     25.696851552657108
                 ]
             },
             "properties": {
                 "OBJECTID": 1841,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1842,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20758595093093,
                     25.696044551414957
                 ]
             },
             "properties": {
                 "OBJECTID": 1842,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1843,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20746856782023,
                     25.69584189099055
                 ]
             },
             "properties": {
                 "OBJECTID": 1843,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1844,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20765492533462,
                     25.695597609941785
                 ]
             },
             "properties": {
                 "OBJECTID": 1844,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1845,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20772465067216,
                     25.695401341898446
                 ]
             },
             "properties": {
                 "OBJECTID": 1845,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1846,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20781935917614,
                     25.695249238661461
                 ]
             },
             "properties": {
                 "OBJECTID": 1846,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1847,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20767005462938,
                     25.695000799147635
                 ]
             },
             "properties": {
                 "OBJECTID": 1847,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1848,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20766888191343,
                     25.694945596961759
                 ]
             },
             "properties": {
                 "OBJECTID": 1848,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1849,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20797204427447,
                     25.694668963701758
                 ]
             },
             "properties": {
                 "OBJECTID": 1849,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1850,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20791185624722,
                     25.694439714821044
                 ]
             },
             "properties": {
                 "OBJECTID": 1850,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1851,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20777222031177,
                     25.694438347851531
                 ]
             },
             "properties": {
                 "OBJECTID": 1851,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1852,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20769555670466,
                     25.693906567933823
                 ]
             },
             "properties": {
                 "OBJECTID": 1852,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1853,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20754725310331,
                     25.693755210234826
                 ]
             },
             "properties": {
                 "OBJECTID": 1853,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1854,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20755589019222,
                     25.693746083015355
                 ]
             },
             "properties": {
                 "OBJECTID": 1854,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1855,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2075385350754,
                     25.693594597612616
                 ]
             },
             "properties": {
                 "OBJECTID": 1855,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1856,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20771452970183,
                     25.693510798784359
                 ]
             },
             "properties": {
                 "OBJECTID": 1856,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1857,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20766962565278,
                     25.693293563946895
                 ]
             },
             "properties": {
                 "OBJECTID": 1857,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1858,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20734540027007,
                     25.692978257140908
                 ]
             },
             "properties": {
                 "OBJECTID": 1858,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1859,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20717012240328,
                     25.692845785204895
                 ]
             },
             "properties": {
                 "OBJECTID": 1859,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1860,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20702583787215,
                     25.692817914315356
                 ]
             },
             "properties": {
                 "OBJECTID": 1860,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1861,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20681969257481,
                     25.692688430826479
                 ]
             },
             "properties": {
                 "OBJECTID": 1861,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1862,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20645676306924,
                     25.692711856367112
                 ]
             },
             "properties": {
                 "OBJECTID": 1862,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1863,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20643978027169,
                     25.692709478559664
                 ]
             },
             "properties": {
                 "OBJECTID": 1863,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1864,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20640736780587,
                     25.692416839165844
                 ]
             },
             "properties": {
                 "OBJECTID": 1864,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1865,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20643603909201,
                     25.691458026067096
                 ]
             },
             "properties": {
                 "OBJECTID": 1865,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1866,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20608282136516,
                     25.690775359695124
                 ]
             },
             "properties": {
                 "OBJECTID": 1866,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1867,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20608777303238,
                     25.690444583650674
                 ]
             },
             "properties": {
                 "OBJECTID": 1867,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1868,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20615069859593,
                     25.690173518093445
                 ]
             },
             "properties": {
                 "OBJECTID": 1868,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1869,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20618616785742,
                     25.689843808644923
                 ]
             },
             "properties": {
                 "OBJECTID": 1869,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1870,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20632320025555,
                     25.689751516619253
                 ]
             },
             "properties": {
                 "OBJECTID": 1870,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1871,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20632082334737,
                     25.689320708084267
                 ]
             },
             "properties": {
                 "OBJECTID": 1871,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1872,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20628081790551,
                     25.688873959066029
                 ]
             },
             "properties": {
                 "OBJECTID": 1872,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1873,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20645854822351,
                     25.688383932872227
                 ]
             },
             "properties": {
                 "OBJECTID": 1873,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1874,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20661036817398,
                     25.688079524950183
                 ]
             },
             "properties": {
                 "OBJECTID": 1874,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1875,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20661403291138,
                     25.688073377184651
                 ]
             },
             "properties": {
                 "OBJECTID": 1875,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1876,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20665764373535,
                     25.688012077595374
                 ]
             },
             "properties": {
                 "OBJECTID": 1876,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1877,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20670996988923,
                     25.687803749643365
                 ]
             },
             "properties": {
                 "OBJECTID": 1877,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1878,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20663729657406,
                     25.68764701759585
                 ]
             },
             "properties": {
                 "OBJECTID": 1878,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1879,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20644254478765,
                     25.686681035101742
                 ]
             },
             "properties": {
                 "OBJECTID": 1879,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1880,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20616224409235,
                     25.686683097247169
                 ]
             },
             "properties": {
                 "OBJECTID": 1880,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1881,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20614778209455,
                     25.686643930872776
                 ]
             },
             "properties": {
                 "OBJECTID": 1881,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1882,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20598726030386,
                     25.686663167371307
                 ]
             },
             "properties": {
                 "OBJECTID": 1882,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1883,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20596161163911,
                     25.686673636379282
                 ]
             },
             "properties": {
                 "OBJECTID": 1883,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1884,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20593998474254,
                     25.686661913716421
                 ]
             },
             "properties": {
                 "OBJECTID": 1884,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1885,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20583297621096,
                     25.686624815782693
                 ]
             },
             "properties": {
                 "OBJECTID": 1885,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1886,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20577721824407,
                     25.686158713353962
                 ]
             },
             "properties": {
                 "OBJECTID": 1886,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1887,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20624361115381,
                     25.685857311865504
                 ]
             },
             "properties": {
                 "OBJECTID": 1887,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1888,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20633421425265,
                     25.685710979578118
                 ]
             },
             "properties": {
                 "OBJECTID": 1888,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1889,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20645803291194,
                     25.685540570640853
                 ]
             },
             "properties": {
                 "OBJECTID": 1889,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1890,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20648511239807,
                     25.685391421676798
                 ]
             },
             "properties": {
                 "OBJECTID": 1890,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1891,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20693293160957,
                     25.684266281663611
                 ]
             },
             "properties": {
                 "OBJECTID": 1891,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1892,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20691477969336,
                     25.684244277051789
                 ]
             },
             "properties": {
                 "OBJECTID": 1892,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1893,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20729691691923,
                     25.683983317176398
                 ]
             },
             "properties": {
                 "OBJECTID": 1893,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1894,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20748789425096,
                     25.683485064883882
                 ]
             },
             "properties": {
                 "OBJECTID": 1894,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1895,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20792098436601,
                     25.68314942980237
                 ]
             },
             "properties": {
                 "OBJECTID": 1895,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1896,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20804738677754,
                     25.682943151405368
                 ]
             },
             "properties": {
                 "OBJECTID": 1896,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1897,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20808275081833,
                     25.682775696741089
                 ]
             },
             "properties": {
                 "OBJECTID": 1897,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1898,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20791684928321,
                     25.682734154357775
                 ]
             },
             "properties": {
                 "OBJECTID": 1898,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1899,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20794406906361,
                     25.682599237165107
                 ]
             },
             "properties": {
                 "OBJECTID": 1899,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1900,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20813866436794,
                     25.682554582228249
                 ]
             },
             "properties": {
                 "OBJECTID": 1900,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1901,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20817195816937,
                     25.68252382721289
                 ]
             },
             "properties": {
                 "OBJECTID": 1901,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1902,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20822072210871,
                     25.682367109554491
                 ]
             },
             "properties": {
                 "OBJECTID": 1902,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1903,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20810510526644,
                     25.682215841787695
                 ]
             },
             "properties": {
                 "OBJECTID": 1903,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1904,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20818444795395,
                     25.682235313009357
                 ]
             },
             "properties": {
                 "OBJECTID": 1904,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1905,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20826806691781,
                     25.682224753169862
                 ]
             },
             "properties": {
                 "OBJECTID": 1905,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1906,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2083313692973,
                     25.681817740696204
                 ]
             },
             "properties": {
                 "OBJECTID": 1906,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1907,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20838082121799,
                     25.681463458172118
                 ]
             },
             "properties": {
                 "OBJECTID": 1907,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1908,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20840477196271,
                     25.68131729225928
                 ]
             },
             "properties": {
                 "OBJECTID": 1908,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1909,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20853746872928,
                     25.68098881937857
                 ]
             },
             "properties": {
                 "OBJECTID": 1909,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1910,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2083595441577,
                     25.680737019997537
                 ]
             },
             "properties": {
                 "OBJECTID": 1910,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1911,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20855168161489,
                     25.680080315254372
                 ]
             },
             "properties": {
                 "OBJECTID": 1911,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1912,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20856514896252,
                     25.679995233093564
                 ]
             },
             "properties": {
                 "OBJECTID": 1912,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1913,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20880346031151,
                     25.680081253247295
                 ]
             },
             "properties": {
                 "OBJECTID": 1913,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1914,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20881911481035,
                     25.680044925133245
                 ]
             },
             "properties": {
                 "OBJECTID": 1914,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1915,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20929028941538,
                     25.679220890731585
                 ]
             },
             "properties": {
                 "OBJECTID": 1915,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1916,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20935650110169,
                     25.679102334904883
                 ]
             },
             "properties": {
                 "OBJECTID": 1916,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1917,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20953903379944,
                     25.67867059197431
                 ]
             },
             "properties": {
                 "OBJECTID": 1917,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1918,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20958176418713,
                     25.678354937130678
                 ]
             },
             "properties": {
                 "OBJECTID": 1918,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1919,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20915022989925,
                     25.677753993052363
                 ]
             },
             "properties": {
                 "OBJECTID": 1919,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1920,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20914458935135,
                     25.677732339176146
                 ]
             },
             "properties": {
                 "OBJECTID": 1920,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1921,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20885000562333,
                     25.677783910798894
                 ]
             },
             "properties": {
                 "OBJECTID": 1921,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1922,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20890542724374,
                     25.677415514314589
                 ]
             },
             "properties": {
                 "OBJECTID": 1922,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1923,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20886899300967,
                     25.677324726854692
                 ]
             },
             "properties": {
                 "OBJECTID": 1923,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1924,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2087375094286,
                     25.677052215187643
                 ]
             },
             "properties": {
                 "OBJECTID": 1924,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1925,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20869689064904,
                     25.677051295181172
                 ]
             },
             "properties": {
                 "OBJECTID": 1925,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1926,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20887110461786,
                     25.676229272562921
                 ]
             },
             "properties": {
                 "OBJECTID": 1926,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1927,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20914877659482,
                     25.675661377670508
                 ]
             },
             "properties": {
                 "OBJECTID": 1927,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1928,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20916887014732,
                     25.675608360837259
                 ]
             },
             "properties": {
                 "OBJECTID": 1928,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1929,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20906987997103,
                     25.67555185643306
                 ]
             },
             "properties": {
                 "OBJECTID": 1929,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1930,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2091461442792,
                     25.67529776917678
                 ]
             },
             "properties": {
                 "OBJECTID": 1930,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1931,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20897250227921,
                     25.675255984875832
                 ]
             },
             "properties": {
                 "OBJECTID": 1931,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1932,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20903880659569,
                     25.675026178415465
                 ]
             },
             "properties": {
                 "OBJECTID": 1932,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1933,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20902445161721,
                     25.67489281165399
                 ]
             },
             "properties": {
                 "OBJECTID": 1933,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1934,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2091068762814,
                     25.674757805428442
                 ]
             },
             "properties": {
                 "OBJECTID": 1934,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1935,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20917741190812,
                     25.674642272223139
                 ]
             },
             "properties": {
                 "OBJECTID": 1935,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1936,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20958126866066,
                     25.674138411758349
                 ]
             },
             "properties": {
                 "OBJECTID": 1936,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1937,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20979061284658,
                     25.673109703350178
                 ]
             },
             "properties": {
                 "OBJECTID": 1937,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1938,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20982488690908,
                     25.672853947851479
                 ]
             },
             "properties": {
                 "OBJECTID": 1938,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1939,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20984334639337,
                     25.672300845008067
                 ]
             },
             "properties": {
                 "OBJECTID": 1939,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1940,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20983341787797,
                     25.6719678143632
                 ]
             },
             "properties": {
                 "OBJECTID": 1940,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1941,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2098610072797,
                     25.670667017868652
                 ]
             },
             "properties": {
                 "OBJECTID": 1941,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1942,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21037108026121,
                     25.669759421160393
                 ]
             },
             "properties": {
                 "OBJECTID": 1942,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1943,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2102399276306,
                     25.669776049625057
                 ]
             },
             "properties": {
                 "OBJECTID": 1943,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1944,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21021536444755,
                     25.669667274824917
                 ]
             },
             "properties": {
                 "OBJECTID": 1944,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1945,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21026781830523,
                     25.669656239244091
                 ]
             },
             "properties": {
                 "OBJECTID": 1945,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1946,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2100354496763,
                     25.669492115668675
                 ]
             },
             "properties": {
                 "OBJECTID": 1946,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1947,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2099588157468,
                     25.669219619290061
                 ]
             },
             "properties": {
                 "OBJECTID": 1947,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1948,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20961754281541,
                     25.668786932071328
                 ]
             },
             "properties": {
                 "OBJECTID": 1948,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1949,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2096245431382,
                     25.668786369995019
                 ]
             },
             "properties": {
                 "OBJECTID": 1949,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1950,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20965127818403,
                     25.668706470627171
                 ]
             },
             "properties": {
                 "OBJECTID": 1950,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1951,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20968485257396,
                     25.668606129669172
                 ]
             },
             "properties": {
                 "OBJECTID": 1951,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1952,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20963894937807,
                     25.668390376914431
                 ]
             },
             "properties": {
                 "OBJECTID": 1952,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1953,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20971243568044,
                     25.668211415424537
                 ]
             },
             "properties": {
                 "OBJECTID": 1953,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1954,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20978067983458,
                     25.667997108779616
                 ]
             },
             "properties": {
                 "OBJECTID": 1954,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1955,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20974875839852,
                     25.667970564390089
                 ]
             },
             "properties": {
                 "OBJECTID": 1955,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1956,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20981695488859,
                     25.667849508448114
                 ]
             },
             "properties": {
                 "OBJECTID": 1956,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1957,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20984493279747,
                     25.667791485988346
                 ]
             },
             "properties": {
                 "OBJECTID": 1957,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1958,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20988699319025,
                     25.667603226407834
                 ]
             },
             "properties": {
                 "OBJECTID": 1958,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1959,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20999802259075,
                     25.667623169773492
                 ]
             },
             "properties": {
                 "OBJECTID": 1959,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1960,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21003810087774,
                     25.667364146138482
                 ]
             },
             "properties": {
                 "OBJECTID": 1960,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1961,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20973935059061,
                     25.667324364627802
                 ]
             },
             "properties": {
                 "OBJECTID": 1961,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1962,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20968434175904,
                     25.66716157114962
                 ]
             },
             "properties": {
                 "OBJECTID": 1962,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1963,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.20969636479543,
                     25.66696811978295
                 ]
             },
             "properties": {
                 "OBJECTID": 1963,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1964,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21016350684044,
                     25.666855995907554
                 ]
             },
             "properties": {
                 "OBJECTID": 1964,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1965,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21020450423453,
                     25.666655766350573
                 ]
             },
             "properties": {
                 "OBJECTID": 1965,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1966,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21005307458978,
                     25.666613952372074
                 ]
             },
             "properties": {
                 "OBJECTID": 1966,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1967,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21034294497099,
                     25.666404247558035
                 ]
             },
             "properties": {
                 "OBJECTID": 1967,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1968,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21040171296875,
                     25.666235943034394
                 ]
             },
             "properties": {
                 "OBJECTID": 1968,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1969,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21048247478643,
                     25.666257756989921
                 ]
             },
             "properties": {
                 "OBJECTID": 1969,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1970,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21056071940171,
                     25.665987221133378
                 ]
             },
             "properties": {
                 "OBJECTID": 1970,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1971,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21066772703404,
                     25.666016306107679
                 ]
             },
             "properties": {
                 "OBJECTID": 1971,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1972,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21081199627668,
                     25.665664622624263
                 ]
             },
             "properties": {
                 "OBJECTID": 1972,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1973,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21083425359797,
                     25.665660740250985
                 ]
             },
             "properties": {
                 "OBJECTID": 1973,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1974,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21097571605651,
                     25.665300515006834
                 ]
             },
             "properties": {
                 "OBJECTID": 1974,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1975,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21071498730686,
                     25.66523777110632
                 ]
             },
             "properties": {
                 "OBJECTID": 1975,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1976,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21106881027742,
                     25.66512067487929
                 ]
             },
             "properties": {
                 "OBJECTID": 1976,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1977,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21116041342242,
                     25.664646570283026
                 ]
             },
             "properties": {
                 "OBJECTID": 1977,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1978,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21136976390352,
                     25.664560726396473
                 ]
             },
             "properties": {
                 "OBJECTID": 1978,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1979,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21133647819596,
                     25.664547440711885
                 ]
             },
             "properties": {
                 "OBJECTID": 1979,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1980,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21139119204992,
                     25.663925213278048
                 ]
             },
             "properties": {
                 "OBJECTID": 1980,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1981,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21141846758826,
                     25.663852789075008
                 ]
             },
             "properties": {
                 "OBJECTID": 1981,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1982,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2114447116042,
                     25.663727902021094
                 ]
             },
             "properties": {
                 "OBJECTID": 1982,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1983,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21148802205465,
                     25.663524248745148
                 ]
             },
             "properties": {
                 "OBJECTID": 1983,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1984,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21152688535756,
                     25.663325514760743
                 ]
             },
             "properties": {
                 "OBJECTID": 1984,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1985,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21124598301577,
                     25.663278662780101
                 ]
             },
             "properties": {
                 "OBJECTID": 1985,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1986,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21126669889907,
                     25.662968093602672
                 ]
             },
             "properties": {
                 "OBJECTID": 1986,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1987,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21132684106095,
                     25.66273412777781
                 ]
             },
             "properties": {
                 "OBJECTID": 1987,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1988,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21153670055833,
                     25.662724695688212
                 ]
             },
             "properties": {
                 "OBJECTID": 1988,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1989,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21150640869388,
                     25.662711948697506
                 ]
             },
             "properties": {
                 "OBJECTID": 1989,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1990,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21152435106802,
                     25.662584019237158
                 ]
             },
             "properties": {
                 "OBJECTID": 1990,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1991,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21166317041906,
                     25.662264796783006
                 ]
             },
             "properties": {
                 "OBJECTID": 1991,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1992,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2114390818482,
                     25.662178612053367
                 ]
             },
             "properties": {
                 "OBJECTID": 1992,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1993,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21154289328996,
                     25.662021472612935
                 ]
             },
             "properties": {
                 "OBJECTID": 1993,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1994,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21164613276289,
                     25.661830119364595
                 ]
             },
             "properties": {
                 "OBJECTID": 1994,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1995,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21176223523901,
                     25.661623931799056
                 ]
             },
             "properties": {
                 "OBJECTID": 1995,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1996,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21196496401194,
                     25.661306683356827
                 ]
             },
             "properties": {
                 "OBJECTID": 1996,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1997,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2119881970977,
                     25.660521652552291
                 ]
             },
             "properties": {
                 "OBJECTID": 1997,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1998,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21196814401469,
                     25.660297144897356
                 ]
             },
             "properties": {
                 "OBJECTID": 1998,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 1999,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21196791738555,
                     25.660101172731004
                 ]
             },
             "properties": {
                 "OBJECTID": 1999,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2000,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21205354813281,
                     25.659793101870207
                 ]
             },
             "properties": {
                 "OBJECTID": 2000,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2001,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21212644268127,
                     25.659582174877528
                 ]
             },
             "properties": {
                 "OBJECTID": 2001,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2002,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21216005034609,
                     25.659502888847271
                 ]
             },
             "properties": {
                 "OBJECTID": 2002,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2003,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2122181852211,
                     25.659386262965711
                 ]
             },
             "properties": {
                 "OBJECTID": 2003,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2004,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21226036522376,
                     25.659319010763795
                 ]
             },
             "properties": {
                 "OBJECTID": 2004,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2005,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21237607559544,
                     25.659193939348881
                 ]
             },
             "properties": {
                 "OBJECTID": 2005,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2006,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21244121978651,
                     25.659140677900041
                 ]
             },
             "properties": {
                 "OBJECTID": 2006,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2007,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2124932383723,
                     25.659074378080163
                 ]
             },
             "properties": {
                 "OBJECTID": 2007,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2008,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21255092628428,
                     25.65892813662424
                 ]
             },
             "properties": {
                 "OBJECTID": 2008,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2009,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21257778633583,
                     25.658672649123503
                 ]
             },
             "properties": {
                 "OBJECTID": 2009,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2010,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21257719907851,
                     25.658614709401377
                 ]
             },
             "properties": {
                 "OBJECTID": 2010,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2011,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2125586379708,
                     25.658423536017438
                 ]
             },
             "properties": {
                 "OBJECTID": 2011,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2012,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21255598407146,
                     25.658277610223593
                 ]
             },
             "properties": {
                 "OBJECTID": 2012,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2013,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21255083994936,
                     25.658068773255309
                 ]
             },
             "properties": {
                 "OBJECTID": 2013,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2014,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21257461892361,
                     25.657514932967842
                 ]
             },
             "properties": {
                 "OBJECTID": 2014,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2015,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21263678276136,
                     25.657111774988493
                 ]
             },
             "properties": {
                 "OBJECTID": 2015,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2016,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2126312897023,
                     25.656744302107882
                 ]
             },
             "properties": {
                 "OBJECTID": 2016,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2017,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21262846493175,
                     25.656601422317863
                 ]
             },
             "properties": {
                 "OBJECTID": 2017,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2018,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21309853876659,
                     25.6554016880375
                 ]
             },
             "properties": {
                 "OBJECTID": 2018,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2019,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21302265667032,
                     25.654794808433735
                 ]
             },
             "properties": {
                 "OBJECTID": 2019,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2020,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21311825010724,
                     25.654291565803192
                 ]
             },
             "properties": {
                 "OBJECTID": 2020,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2021,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21319549917303,
                     25.653822999232091
                 ]
             },
             "properties": {
                 "OBJECTID": 2021,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2022,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21318286909417,
                     25.653634470754298
                 ]
             },
             "properties": {
                 "OBJECTID": 2022,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2023,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21292490396121,
                     25.653437556098368
                 ]
             },
             "properties": {
                 "OBJECTID": 2023,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2024,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21282455580865,
                     25.653398210758894
                 ]
             },
             "properties": {
                 "OBJECTID": 2024,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2025,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21275992333187,
                     25.653318171996091
                 ]
             },
             "properties": {
                 "OBJECTID": 2025,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2026,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21269073488958,
                     25.653181979564749
                 ]
             },
             "properties": {
                 "OBJECTID": 2026,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2027,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21263242374744,
                     25.652969708984926
                 ]
             },
             "properties": {
                 "OBJECTID": 2027,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2028,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21238260197555,
                     25.652532454109519
                 ]
             },
             "properties": {
                 "OBJECTID": 2028,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2029,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21241179217054,
                     25.65239850908398
                 ]
             },
             "properties": {
                 "OBJECTID": 2029,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2030,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21248205170536,
                     25.65239733636804
                 ]
             },
             "properties": {
                 "OBJECTID": 2030,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2031,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2125093470288,
                     25.652281250079682
                 ]
             },
             "properties": {
                 "OBJECTID": 2031,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2032,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21255215475821,
                     25.651861478924161
                 ]
             },
             "properties": {
                 "OBJECTID": 2032,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2033,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21273671992373,
                     25.651450063369793
                 ]
             },
             "properties": {
                 "OBJECTID": 2033,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2034,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21251590038855,
                     25.651324043170121
                 ]
             },
             "properties": {
                 "OBJECTID": 2034,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2035,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21222642660837,
                     25.651217442031395
                 ]
             },
             "properties": {
                 "OBJECTID": 2035,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2036,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21226300743189,
                     25.651007165248529
                 ]
             },
             "properties": {
                 "OBJECTID": 2036,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2037,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21253314219081,
                     25.650856953285825
                 ]
             },
             "properties": {
                 "OBJECTID": 2037,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2038,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21293118572572,
                     25.65105418000644
                 ]
             },
             "properties": {
                 "OBJECTID": 2038,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2039,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21333203424604,
                     25.650241361050007
                 ]
             },
             "properties": {
                 "OBJECTID": 2039,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2040,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21342523368764,
                     25.649270356741624
                 ]
             },
             "properties": {
                 "OBJECTID": 2040,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2041,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21331187684171,
                     25.649240308593392
                 ]
             },
             "properties": {
                 "OBJECTID": 2041,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2042,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21382598238324,
                     25.648193641623322
                 ]
             },
             "properties": {
                 "OBJECTID": 2042,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2043,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21388847087627,
                     25.647727128204451
                 ]
             },
             "properties": {
                 "OBJECTID": 2043,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2044,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21392231776082,
                     25.647009233589301
                 ]
             },
             "properties": {
                 "OBJECTID": 2044,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2045,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21393385516336,
                     25.646854557391976
                 ]
             },
             "properties": {
                 "OBJECTID": 2045,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2046,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21393839134373,
                     25.646792457406036
                 ]
             },
             "properties": {
                 "OBJECTID": 2046,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2047,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2139535593094,
                     25.646335183621886
                 ]
             },
             "properties": {
                 "OBJECTID": 2047,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2048,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21396402472004,
                     25.646253805768595
                 ]
             },
             "properties": {
                 "OBJECTID": 2048,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2049,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21400840266568,
                     25.645878010561773
                 ]
             },
             "properties": {
                 "OBJECTID": 2049,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2050,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21402426940455,
                     25.645795011230859
                 ]
             },
             "properties": {
                 "OBJECTID": 2050,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2051,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21406108405182,
                     25.645653502906896
                 ]
             },
             "properties": {
                 "OBJECTID": 2051,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2052,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21420211573513,
                     25.644612400941583
                 ]
             },
             "properties": {
                 "OBJECTID": 2052,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2053,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21428914582748,
                     25.644289573105368
                 ]
             },
             "properties": {
                 "OBJECTID": 2053,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2054,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21446996531671,
                     25.643910212985986
                 ]
             },
             "properties": {
                 "OBJECTID": 2054,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2055,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21465187928084,
                     25.643582421791393
                 ]
             },
             "properties": {
                 "OBJECTID": 2055,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2056,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21454422233808,
                     25.643502779629614
                 ]
             },
             "properties": {
                 "OBJECTID": 2056,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2057,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21482550239506,
                     25.642952007828967
                 ]
             },
             "properties": {
                 "OBJECTID": 2057,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2058,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21539721760757,
                     25.642433679970395
                 ]
             },
             "properties": {
                 "OBJECTID": 2058,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2059,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21592808921002,
                     25.641301646653744
                 ]
             },
             "properties": {
                 "OBJECTID": 2059,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2060,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21665973355994,
                     25.64001088209659
                 ]
             },
             "properties": {
                 "OBJECTID": 2060,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2061,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21726160843656,
                     25.639393973253334
                 ]
             },
             "properties": {
                 "OBJECTID": 2061,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2062,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21737276374211,
                     25.639273023431315
                 ]
             },
             "properties": {
                 "OBJECTID": 2062,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2063,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21746126242738,
                     25.639113927965468
                 ]
             },
             "properties": {
                 "OBJECTID": 2063,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2064,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21745634133714,
                     25.639055249899911
                 ]
             },
             "properties": {
                 "OBJECTID": 2064,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2065,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21742901094018,
                     25.638972602203864
                 ]
             },
             "properties": {
                 "OBJECTID": 2065,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2066,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21740612499275,
                     25.638884190752833
                 ]
             },
             "properties": {
                 "OBJECTID": 2066,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2067,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21734717802991,
                     25.638792336697009
                 ]
             },
             "properties": {
                 "OBJECTID": 2067,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2068,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21702266126687,
                     25.638496374308261
                 ]
             },
             "properties": {
                 "OBJECTID": 2068,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2069,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21661289776705,
                     25.638148884363545
                 ]
             },
             "properties": {
                 "OBJECTID": 2069,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2070,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21643286068803,
                     25.638004891212802
                 ]
             },
             "properties": {
                 "OBJECTID": 2070,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2071,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21615495488732,
                     25.637788818299384
                 ]
             },
             "properties": {
                 "OBJECTID": 2071,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2072,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21602909926361,
                     25.637868428984859
                 ]
             },
             "properties": {
                 "OBJECTID": 2072,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2073,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21574821760623,
                     25.637620330314064
                 ]
             },
             "properties": {
                 "OBJECTID": 2073,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2074,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21562493404355,
                     25.63749991019273
                 ]
             },
             "properties": {
                 "OBJECTID": 2074,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2075,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2153160699807,
                     25.637162321783762
                 ]
             },
             "properties": {
                 "OBJECTID": 2075,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2076,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21547302056354,
                     25.636749969135792
                 ]
             },
             "properties": {
                 "OBJECTID": 2076,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2077,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2157979923835,
                     25.636453456361949
                 ]
             },
             "properties": {
                 "OBJECTID": 2077,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2078,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21597105072345,
                     25.636191918222494
                 ]
             },
             "properties": {
                 "OBJECTID": 2078,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2079,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2161346472962,
                     25.63629035441636
                 ]
             },
             "properties": {
                 "OBJECTID": 2079,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2080,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21629896242723,
                     25.636076324762655
                 ]
             },
             "properties": {
                 "OBJECTID": 2080,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2081,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21599139878407,
                     25.635890585981826
                 ]
             },
             "properties": {
                 "OBJECTID": 2081,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2082,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21648802959896,
                     25.635395366203227
                 ]
             },
             "properties": {
                 "OBJECTID": 2082,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2083,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21646270918671,
                     25.635421367402216
                 ]
             },
             "properties": {
                 "OBJECTID": 2083,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2084,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21671923540447,
                     25.635564900100121
                 ]
             },
             "properties": {
                 "OBJECTID": 2084,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2085,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21703752885895,
                     25.635744046850334
                 ]
             },
             "properties": {
                 "OBJECTID": 2085,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2086,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21711369334236,
                     25.635833246107495
                 ]
             },
             "properties": {
                 "OBJECTID": 2086,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2087,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21730520577069,
                     25.63569819311715
                 ]
             },
             "properties": {
                 "OBJECTID": 2087,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2088,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21751692416677,
                     25.635542215600822
                 ]
             },
             "properties": {
                 "OBJECTID": 2088,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2089,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21776524676881,
                     25.635235057551938
                 ]
             },
             "properties": {
                 "OBJECTID": 2089,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2090,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21796883619288,
                     25.635128787363669
                 ]
             },
             "properties": {
                 "OBJECTID": 2090,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2091,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21815042730043,
                     25.635018666278484
                 ]
             },
             "properties": {
                 "OBJECTID": 2091,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2092,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21809076807455,
                     25.634967302399161
                 ]
             },
             "properties": {
                 "OBJECTID": 2092,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2093,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21809846896923,
                     25.634959921663096
                 ]
             },
             "properties": {
                 "OBJECTID": 2093,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2094,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21832440564685,
                     25.634895317065286
                 ]
             },
             "properties": {
                 "OBJECTID": 2094,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2095,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21823181324766,
                     25.63466305455637
                 ]
             },
             "properties": {
                 "OBJECTID": 2095,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2096,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2182654937576,
                     25.634612666441456
                 ]
             },
             "properties": {
                 "OBJECTID": 2096,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2097,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21837204273567,
                     25.634449409812476
                 ]
             },
             "properties": {
                 "OBJECTID": 2097,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2098,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21852176186985,
                     25.634178911727417
                 ]
             },
             "properties": {
                 "OBJECTID": 2098,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2099,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21864793225632,
                     25.633958292741056
                 ]
             },
             "properties": {
                 "OBJECTID": 2099,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2100,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21877102336407,
                     25.633885848752982
                 ]
             },
             "properties": {
                 "OBJECTID": 2100,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2101,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21884172356675,
                     25.633935081239031
                 ]
             },
             "properties": {
                 "OBJECTID": 2101,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2102,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21878478479005,
                     25.633681181041709
                 ]
             },
             "properties": {
                 "OBJECTID": 2102,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2103,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21876051838325,
                     25.633648327908077
                 ]
             },
             "properties": {
                 "OBJECTID": 2103,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2104,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21879072930875,
                     25.633619047780883
                 ]
             },
             "properties": {
                 "OBJECTID": 2104,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2105,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21887129687292,
                     25.633409807916337
                 ]
             },
             "properties": {
                 "OBJECTID": 2105,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2106,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21911129714869,
                     25.63336012486991
                 ]
             },
             "properties": {
                 "OBJECTID": 2106,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2107,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21928077169031,
                     25.633361706777407
                 ]
             },
             "properties": {
                 "OBJECTID": 2107,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2108,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21930936203751,
                     25.633339168867622
                 ]
             },
             "properties": {
                 "OBJECTID": 2108,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2109,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21941777441083,
                     25.633253702696265
                 ]
             },
             "properties": {
                 "OBJECTID": 2109,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2110,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21957487697904,
                     25.633103643618313
                 ]
             },
             "properties": {
                 "OBJECTID": 2110,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2111,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21964557358439,
                     25.633151048682009
                 ]
             },
             "properties": {
                 "OBJECTID": 2111,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2112,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21975078706924,
                     25.633016564962531
                 ]
             },
             "properties": {
                 "OBJECTID": 2112,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2113,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22001162913341,
                     25.632697187824988
                 ]
             },
             "properties": {
                 "OBJECTID": 2113,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2114,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22008817582872,
                     25.632713702975082
                 ]
             },
             "properties": {
                 "OBJECTID": 2114,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2115,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22009533892879,
                     25.632484724790288
                 ]
             },
             "properties": {
                 "OBJECTID": 2115,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2116,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22010204337465,
                     25.632368785091387
                 ]
             },
             "properties": {
                 "OBJECTID": 2116,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2117,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22009535691524,
                     25.632177709733583
                 ]
             },
             "properties": {
                 "OBJECTID": 2117,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2118,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22011712320671,
                     25.632133356069573
                 ]
             },
             "properties": {
                 "OBJECTID": 2118,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2119,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22015691371064,
                     25.632093817375846
                 ]
             },
             "properties": {
                 "OBJECTID": 2119,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2120,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22015250073736,
                     25.631847705307393
                 ]
             },
             "properties": {
                 "OBJECTID": 2120,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2121,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.21999201402031,
                     25.631256919071632
                 ]
             },
             "properties": {
                 "OBJECTID": 2121,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2122,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22024354180604,
                     25.630752829279686
                 ]
             },
             "properties": {
                 "OBJECTID": 2122,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2123,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2204168519562,
                     25.630642943816838
                 ]
             },
             "properties": {
                 "OBJECTID": 2123,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2124,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22072508919155,
                     25.630395820910451
                 ]
             },
             "properties": {
                 "OBJECTID": 2124,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2125,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22078565043745,
                     25.630415831725315
                 ]
             },
             "properties": {
                 "OBJECTID": 2125,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2126,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22097235598949,
                     25.630354849596699
                 ]
             },
             "properties": {
                 "OBJECTID": 2126,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2127,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22117216376438,
                     25.630398410957923
                 ]
             },
             "properties": {
                 "OBJECTID": 2127,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2128,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22129199033316,
                     25.630290610123609
                 ]
             },
             "properties": {
                 "OBJECTID": 2128,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2129,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22143785227513,
                     25.630508398943505
                 ]
             },
             "properties": {
                 "OBJECTID": 2129,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2130,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22180403462858,
                     25.630198968307809
                 ]
             },
             "properties": {
                 "OBJECTID": 2130,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2131,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22167040616438,
                     25.629838642339507
                 ]
             },
             "properties": {
                 "OBJECTID": 2131,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2132,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22168832875343,
                     25.62968295710283
                 ]
             },
             "properties": {
                 "OBJECTID": 2132,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2133,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22157492334412,
                     25.629654322688907
                 ]
             },
             "properties": {
                 "OBJECTID": 2133,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2134,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22163215709844,
                     25.628956186179096
                 ]
             },
             "properties": {
                 "OBJECTID": 2134,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2135,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22166340494226,
                     25.62885166067565
                 ]
             },
             "properties": {
                 "OBJECTID": 2135,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2136,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22181152867915,
                     25.628521167917597
                 ]
             },
             "properties": {
                 "OBJECTID": 2136,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2137,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22228318262285,
                     25.62805231716078
                 ]
             },
             "properties": {
                 "OBJECTID": 2137,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2138,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22270166504984,
                     25.627642345018273
                 ]
             },
             "properties": {
                 "OBJECTID": 2138,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2139,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22280649722211,
                     25.627473625007838
                 ]
             },
             "properties": {
                 "OBJECTID": 2139,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2140,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22286264999138,
                     25.627346974383386
                 ]
             },
             "properties": {
                 "OBJECTID": 2140,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2141,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22304950123356,
                     25.62676871570369
                 ]
             },
             "properties": {
                 "OBJECTID": 2141,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2142,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22319221105175,
                     25.626303576448947
                 ]
             },
             "properties": {
                 "OBJECTID": 2142,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2143,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22479555207315,
                     25.62599203060779
                 ]
             },
             "properties": {
                 "OBJECTID": 2143,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2144,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22503372222855,
                     25.625357968995104
                 ]
             },
             "properties": {
                 "OBJECTID": 2144,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2145,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22538918736115,
                     25.624096837998707
                 ]
             },
             "properties": {
                 "OBJECTID": 2145,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2146,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22553242058478,
                     25.623405387049047
                 ]
             },
             "properties": {
                 "OBJECTID": 2146,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2147,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22579540483412,
                     25.622606797165986
                 ]
             },
             "properties": {
                 "OBJECTID": 2147,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2148,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22604137930625,
                     25.622665675780354
                 ]
             },
             "properties": {
                 "OBJECTID": 2148,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2149,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22615989916005,
                     25.622402503572744
                 ]
             },
             "properties": {
                 "OBJECTID": 2149,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2150,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22603069805831,
                     25.621501704839829
                 ]
             },
             "properties": {
                 "OBJECTID": 2150,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2151,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22543331709397,
                     25.620902783336817
                 ]
             },
             "properties": {
                 "OBJECTID": 2151,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2152,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22639573737342,
                     25.619253073270784
                 ]
             },
             "properties": {
                 "OBJECTID": 2152,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2153,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22688328143835,
                     25.619409860176972
                 ]
             },
             "properties": {
                 "OBJECTID": 2153,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2154,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22705274428876,
                     25.619175638944625
                 ]
             },
             "properties": {
                 "OBJECTID": 2154,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2155,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22717893356099,
                     25.618949127600217
                 ]
             },
             "properties": {
                 "OBJECTID": 2155,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2156,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22720181591114,
                     25.618899840255494
                 ]
             },
             "properties": {
                 "OBJECTID": 2156,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2157,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22726769215035,
                     25.61874784763512
                 ]
             },
             "properties": {
                 "OBJECTID": 2157,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2158,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22692537960262,
                     25.618376058008721
                 ]
             },
             "properties": {
                 "OBJECTID": 2158,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2159,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2270648977269,
                     25.618149052037154
                 ]
             },
             "properties": {
                 "OBJECTID": 2159,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2160,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2271916292903,
                     25.61794284918318
                 ]
             },
             "properties": {
                 "OBJECTID": 2160,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2161,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.2272917589076,
                     25.617779928001312
                 ]
             },
             "properties": {
                 "OBJECTID": 2161,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2162,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22738897022487,
                     25.617620989916816
                 ]
             },
             "properties": {
                 "OBJECTID": 2162,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2163,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22745084627957,
                     25.617607010854897
                 ]
             },
             "properties": {
                 "OBJECTID": 2163,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2164,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22751869743001,
                     25.617484178751909
                 ]
             },
             "properties": {
                 "OBJECTID": 2164,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2165,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22754001855708,
                     25.617445579849687
                 ]
             },
             "properties": {
                 "OBJECTID": 2165,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2166,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22772780509416,
                     25.617105621727092
                 ]
             },
             "properties": {
                 "OBJECTID": 2166,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2167,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22783064616743,
                     25.616918047429976
                 ]
             },
             "properties": {
                 "OBJECTID": 2167,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2168,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22792641497216,
                     25.616728279686413
                 ]
             },
             "properties": {
                 "OBJECTID": 2168,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2169,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22811101790921,
                     25.616350490682635
                 ]
             },
             "properties": {
                 "OBJECTID": 2169,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2170,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22883104121962,
                     25.615182617583798
                 ]
             },
             "properties": {
                 "OBJECTID": 2170,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2171,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22897599034974,
                     25.613573136890807
                 ]
             },
             "properties": {
                 "OBJECTID": 2171,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2172,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22917406063448,
                     25.613565041193738
                 ]
             },
             "properties": {
                 "OBJECTID": 2172,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2173,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22984658265375,
                     25.612677578507487
                 ]
             },
             "properties": {
                 "OBJECTID": 2173,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2174,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23007753125313,
                     25.612731648446868
                 ]
             },
             "properties": {
                 "OBJECTID": 2174,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2175,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23021064890236,
                     25.611870726552866
                 ]
             },
             "properties": {
                 "OBJECTID": 2175,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2176,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23023223532948,
                     25.61166777834535
                 ]
             },
             "properties": {
                 "OBJECTID": 2176,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2177,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23050728398402,
                     25.61164470803692
                 ]
             },
             "properties": {
                 "OBJECTID": 2177,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2178,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23053059171349,
                     25.609399689943814
                 ]
             },
             "properties": {
                 "OBJECTID": 2178,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2179,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23098798600682,
                     25.609381298807932
                 ]
             },
             "properties": {
                 "OBJECTID": 2179,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2180,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23129718281876,
                     25.609458686369351
                 ]
             },
             "properties": {
                 "OBJECTID": 2180,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2181,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23153113515383,
                     25.60833863202231
                 ]
             },
             "properties": {
                 "OBJECTID": 2181,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2182,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23152954065586,
                     25.60752438943905
                 ]
             },
             "properties": {
                 "OBJECTID": 2182,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2183,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23149113061123,
                     25.606199483919113
                 ]
             },
             "properties": {
                 "OBJECTID": 2183,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2184,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23129564228014,
                     25.605750093592007
                 ]
             },
             "properties": {
                 "OBJECTID": 2184,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2185,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23106744470687,
                     25.605382717838211
                 ]
             },
             "properties": {
                 "OBJECTID": 2185,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2186,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23069585472996,
                     25.604879772883237
                 ]
             },
             "properties": {
                 "OBJECTID": 2186,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2187,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.23055365033076,
                     25.604578153758837
                 ]
             },
             "properties": {
                 "OBJECTID": 2187,
                 "Id": 0,
                 "ORIG_FID": 0
             }
         },
         {
             "type": "Feature",
             "id": 2188,
             "geometry": {
                 "type": "Point",
                 "coordinates": [
                     100.22943085844742,
                     25.603746591132335
                 ]
             },
             "properties": {
                 "OBJECTID": 2188,
                 "Id": 0,
                 "ORIG_FID": 0
             }
        }
    ]

export const generateBlueLine = () => {

    let coordGroup = src.map(o => {
        return {
            coord: `${o.geometry.coordinates[0]},${o.geometry.coordinates[1]}`,
            coord_z: 45
        }
    });
    return {
        id: 'path-tripleline-blue',
        coord_type: 0,
        cad_mapkey: '',
        type: 'arrow',
        color: '2f61fd',
        width: 10,
        points: coordGroup
    };
}