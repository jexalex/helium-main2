export default [
    {
      id: "water_check_1",
      coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
      cad_mapkey: "", //CAD基准点的Key值, 项目中约定
      coord: "100.208206,25.676388", //POI点的坐标 lng,lat
      coord_z: 1, //高度(米)
      always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
      show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
      sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
      //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
      state: "state_1", //与marker之中images中的define_state对应
      marker: {
        size: "46,73", //marker大小(宽,高 单位:像素)
        images: [
          {
            define_state: "state_1", //marker图片组
            normal_url:
              "http://localhost:3000/html/images/water_marker.png",
            activate_url:
              "http://localhost:3000/html/images/water_marker_click.png",
          },
        ],
      },
      label: {
        bg_image_url: "http://localhost:3000/html/images/label_bg.png",
        size: "113,26", //label大小(宽,高 单位:像素)
        offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
        text: "大庄村北沟", //label中的文本内容
        font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
        text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
        text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
      },
      detail:{
        position:'大庄村北沟',
        listData: [
          {
            name: "化学需氧量（COD）",
            startPosition: '11 mg/L',
            endPosition: '6 mg/L',
            
        },{
            name: "氨含量（NH3）",
            startPosition: '3 mg/L',
            endPosition: '1 mg/L',
            
        },{
            name: "总磷含量（TP）",
            startPosition: '0.3 mg/L',
            endPosition: '0.2 mg/L',
            
        },{
            name: "总氮含量（TN）",
            startPosition: '0.1 mg/L',
            endPosition: '0 mg/L',
            
        },{
            name: "溶解氧含量（DO）",
            startPosition: '7.4 mg/L',
            endPosition: '6.3 mg/L',
            
        },{
            name: "CHLA",
            startPosition: '0.2 mg/L',
            endPosition: '0 mg/L',
            
        },{
            name: "温度",
            startPosition: '19℃',
            endPosition: '19℃',
            
        },{
            name: "传导性",
            startPosition: '64%',
            endPosition: '41.2%',
            
        },
      ]
    }
    },
    // {
    //   id: "water_check_2",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.225616,25.627785", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_3",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.224121,25.63007", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_4",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.221321,25.634272", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_5",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.218887,25.637653", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_6",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.218826,25.642191", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_7",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.217224,25.643978", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_8",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.216446,25.649548", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_9",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.215721,25.651863", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_10",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.215706,25.655294", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_11",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.214592,25.660248", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_12",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.217171,25.646988", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_13",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.220085,25.639687", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_14",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.222588,25.63336", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/water_marker_click.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_normal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "520,350", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
    // {
    //   id: "water_check_15",
    //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    //   coord: "100.215134,25.65803", //POI点的坐标 lng,lat
    //   coord_z: 1, //高度(米)
    //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    //   state: "state_1", //与marker之中images中的define_state对应
    //   marker: {
    //     size: "46,73", //marker大小(宽,高 单位:像素)
    //     images: [
    //       {
    //         define_state: "state_1", //marker图片组
    //         normal_url:
    //           "http://localhost:3000/html/images/unormal_water_marker.png",
    //         activate_url:
    //           "http://localhost:3000/html/images/unormal_water_marker.png",
    //       },
    //     ],
    //   },
    //   label: {
    //     bg_image_url:
    //       "http://localhost:3000/html/images/unormal_label_bg.png",
    //     size: "113,26", //label大小(宽,高 单位:像素)
    //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //     text: "南岸入水口", //label中的文本内容
    //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
    //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
    //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    //   },
    //   window: {
    //     url: "http://localhost:3000/html/water_unormal.html",
    //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
    //     size: "580,380", //window大小(宽,高 单位:像素)
    //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    //   },
    // },
  ]