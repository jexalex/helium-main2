export const generateWaterParamsListData = () => {
    return[
        {
            name: "化学需氧量（COD）",
            startPosition: '11 mg/L',
            endPosition: '6 mg/L',
            
        },{
            name: "氨含量（NH3）",
            startPosition: '3 mg/L',
            endPosition: '1 mg/L',
            
        },{
            name: "总磷含量（TP）",
            startPosition: '0.3 mg/L',
            endPosition: '0.2 mg/L',
            
        },{
            name: "总氮含量（TN）",
            startPosition: '0.1 mg/L',
            endPosition: '0 mg/L',
            
        },{
            name: "溶解氧含量（DO）",
            startPosition: '7.4 mg/L',
            endPosition: '6.3 mg/L',
            
        },{
            name: "TUB",
            startPosition: '19℃',
            endPosition: 304,
            
        },{
            name: "TUB",
            startPosition: '19℃',
            endPosition: '19℃',
            
        },{
            name: "传导性",
            startPosition: '64%',
            endPosition: '41.2%',
            
        },
    ]
}