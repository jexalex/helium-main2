export const generatePlantChartData = () => {
    return[
        { 
            name: "含油废水",
            value: 48,
            },
            {
              name: "固体废弃物",
              value: 22,
            },
            {
              name: "生活污染",
              value: 15,
            },
            {
              name: "工业污染",
              value: 5,
            },
            {
              name: "其他",
              value: 10,
            },
    ]
}