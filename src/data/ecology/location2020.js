export default[
    {
      id: "商业用地",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.202148,25.595318",
          coord_z: 50,
        },
        {
          coord: "100.234589,25.60018",
          coord_z: 50,
        },
        {
          coord: "100.238205,25.57839",
          coord_z: 50,
        },
        {
          coord: "100.205292,25.574078",
          coord_z: 50,
        },
      ],
    },
    {
      id: "居住用地",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.237129,25.601805",
          coord_z: 50,
        },
        {
          coord: "100.240326,25.580162",
          coord_z: 50,
        },
        {
          coord: "100.290092,25.597546",
          coord_z: 50,
        },
        {
          coord: "100.288643,25.64167",
          coord_z: 50,
        },
        {
          coord: "100.273705,25.620689",
          coord_z: 50,
        },
        {
            coord: "100.275757,25.608265",
            coord_z: 50
        }
      ],
    },
    {
      id: "居住用地2",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.29557,25.611561",
          coord_z: 50,
        },
        {
          coord: "100.307846,25.600643",
          coord_z: 50,
        },
        {
          coord: "100.337372,25.609343",
          coord_z: 50,
        },
        {
          coord: "100.321205,25.694321",
          coord_z: 50,
        },
        {
          coord: "100.275673,25.682133",
          coord_z: 50,
        },
        {
          coord: "100.279671,25.672594",
          coord_z: 50,
        },
        {
          coord: "100.28862,25.675846",
          coord_z: 50,
        },
        {
          coord: "100.298744,25.662733",
          coord_z: 50,
        },
        {
          coord: "100.293793,25.644943",
          coord_z: 50,
        }
      ],
    },
    {
      id: "居住用地3",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.275223,25.684052",
          coord_z: 50,
        },
        {
          coord: "100.262543,25.709705",
          coord_z: 50,
        },
        {
          coord: "100.306793,25.725126",
          coord_z: 50,
        },
        {
          coord: "100.319969,25.696018",
          coord_z: 50,
        }
      ],
    },
    {
      id: "居住用地4",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.26017,25.712444",
          coord_z: 50,
        },
        {
          coord: "100.30587,25.728472",
          coord_z: 50,
        },
        {
          coord: "100.263412,25.811155",
          coord_z: 50,
        },
        {
          coord: "100.231956,25.80088",
          coord_z: 50,
        },
        {
          coord: "100.224609,25.773418",
          coord_z: 50,
        }
      ],
    },
    {
      id: "商业用地2",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.23085,25.804022",
          coord_z: 50,
        },
        {
          coord: "100.265327,25.816978",
          coord_z: 50,
        },
        {
          coord: "100.245712,25.888372",
          coord_z: 50,
        },
        {
          coord: "100.219566,25.878918",
          coord_z: 50,
        },
        {
          coord: "100.237274,25.819349",
          coord_z: 50,
        }
      ],
    },
    {
      id: "居住用地5",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.219673,25.884197",
          coord_z: 50,
        },
        {
          coord: "100.248589,25.894178",
          coord_z: 50,
        },
        {
          coord: "100.233353,25.922237",
          coord_z: 50,
        },
        {
          coord: "100.212944,25.913445",
          coord_z: 50,
        },
        {
          coord: "100.226898,25.893217",
          coord_z: 50,
        }
      ],
    },
    {
      id: "商业用地3",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.212914,25.915331",
          coord_z: 50,
        },
        {
          coord: "100.229614,25.924843",
          coord_z: 50,
        },
        {
          coord: "100.209496,25.952032",
          coord_z: 50,
        },
        {
          coord: "100.191086,25.942007",
          coord_z: 50,
        },
        {
          coord: "100.201378,25.915159",
          coord_z: 50,
        }
      ],
    },
    {
      id: "商业用地4",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.190987,25.941025",
          coord_z: 50,
        },
        {
          coord: "100.209297,25.954132",
          coord_z: 50,
        },
        {
          coord: "100.178085,25.986446",
          coord_z: 50,
        },
        {
          coord: "100.160439,25.976233",
          coord_z: 50,
        },
        {
          coord: "100.153709,25.963333",
          coord_z: 50,
        }
      ],
    },
    {
      id: "商业用地5",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.097542,25.948914",
          coord_z: 50,
        },
        {
          coord: "100.094429,25.959969",
          coord_z: 50,
        },
        {
          coord: "100.076332,25.948111",
          coord_z: 50,
        },
        {
          coord: "100.107582,25.855055",
          coord_z: 50,
        },
        {
          coord: "100.145851,25.854235",
          coord_z: 50,
        },
        {
          coord: "100.147156,25.858932",
          coord_z: 50,
        },
        {
          coord: "100.121819,25.869095",
          coord_z: 50,
        },
        {
          coord: "100.113548,25.894184",
          coord_z: 50,
        },
        {
          coord: "100.087112,25.932016",
          coord_z: 50,
        },
        {
          coord: "00.090469,25.946548",
          coord_z: 50,
        }
      ],
    },
    {
      id: "居住用地7",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "ff7200",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.1446,25.851725",
          coord_z: 50,
        },
        {
          coord: "100.109055,25.851515",
          coord_z: 50,
        },
        {
          coord: "100.116127,25.760136",
          coord_z: 50,
        },
        {
          coord: "100.133911,25.787306",
          coord_z: 50,
        }
      ],
    },
    {
      id: "居住用地8",
      coord_type: 0,
      cad_mapkey: "",
      type: "flash",
      color: "e65aff",
      range_height: 50,
      fill_area: "dot",
      points: [
        {
          coord: "100.135902,25.762697",
          coord_z: 50,
        },
        {
          coord: "100.116524,25.757544",
          coord_z: 50,
        },
        {
          coord: "100.201569,25.600883",
          coord_z: 50,
        },
        {
          coord: "100.229477,25.610235",
          coord_z: 50,
        },
        {
          coord: "100.201981,25.692518",
          coord_z: 50,
        },
        {
          coord: "100.180061,25.725294",
          coord_z: 50,
        },
        {
          coord: "100.161095,25.729416",
          coord_z: 50,
        }
      ],
    },
    ]