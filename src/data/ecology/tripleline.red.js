const src = {
    "type": "FeatureCollection",
    "features": [{
        "type": "Feature",
        "id": 1,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23114256327875, 25.602735281907769]
        },
        "properties": {
            "OBJECTID": 1,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23162811804326, 25.60337664421678]
        },
        "properties": {
            "OBJECTID": 2,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 3,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23176479161191, 25.603495414082147]
        },
        "properties": {
            "OBJECTID": 3,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 4,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23318558464706, 25.604574376606251]
        },
        "properties": {
            "OBJECTID": 4,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 5,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2389155314267, 25.606308459268007]
        },
        "properties": {
            "OBJECTID": 5,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 6,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24045319585838, 25.606980739369703]
        },
        "properties": {
            "OBJECTID": 6,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 7,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24127612589257, 25.607609628081946]
        },
        "properties": {
            "OBJECTID": 7,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 8,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2417943800067, 25.60786902403629]
        },
        "properties": {
            "OBJECTID": 8,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 9,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24581194026888, 25.60888729671143]
        },
        "properties": {
            "OBJECTID": 9,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 10,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24916255520782, 25.60872518941602]
        },
        "properties": {
            "OBJECTID": 10,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 11,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24933181391214, 25.608753042319108]
        },
        "properties": {
            "OBJECTID": 11,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 12,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24972407660493, 25.608799411363805]
        },
        "properties": {
            "OBJECTID": 12,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 13,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24986037965283, 25.608826643734687]
        },
        "properties": {
            "OBJECTID": 13,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 14,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25031747357258, 25.609037915167676]
        },
        "properties": {
            "OBJECTID": 14,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 15,
        "geometry": {
            "type": "Point",
            "coordinates": [100.252004816669, 25.609580746854022]
        },
        "properties": {
            "OBJECTID": 15,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 16,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25230469560529, 25.609649852558675]
        },
        "properties": {
            "OBJECTID": 16,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 17,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2535401941247, 25.609626031316338]
        },
        "properties": {
            "OBJECTID": 17,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 18,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25365966456195, 25.60961504879549]
        },
        "properties": {
            "OBJECTID": 18,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 19,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25420775908088, 25.609670568442027]
        },
        "properties": {
            "OBJECTID": 19,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 20,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25939344784939, 25.610829128161811]
        },
        "properties": {
            "OBJECTID": 20,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 21,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26113692033266, 25.610960943592715]
        },
        "properties": {
            "OBJECTID": 21,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 22,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26316411812962, 25.611668513091445]
        },
        "properties": {
            "OBJECTID": 22,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 23,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26351225108959, 25.611795394841636]
        },
        "properties": {
            "OBJECTID": 23,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 24,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26589618476117, 25.612557506423855]
        },
        "properties": {
            "OBJECTID": 24,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 25,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26623594323428, 25.613171228969065]
        },
        "properties": {
            "OBJECTID": 25,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 26,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26623624720509, 25.613184591995378]
        },
        "properties": {
            "OBJECTID": 26,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 27,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26613705917799, 25.613813355701836]
        },
        "properties": {
            "OBJECTID": 27,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 28,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26612816758092, 25.613817026734466]
        },
        "properties": {
            "OBJECTID": 28,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 29,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26600778702982, 25.613854167835655]
        },
        "properties": {
            "OBJECTID": 29,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 30,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2658630240594, 25.613963795193058]
        },
        "properties": {
            "OBJECTID": 30,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 31,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26586400072313, 25.613972607649771]
        },
        "properties": {
            "OBJECTID": 31,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 32,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26585852295256, 25.614291053089687]
        },
        "properties": {
            "OBJECTID": 32,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 33,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26585632321081, 25.61429968298404]
        },
        "properties": {
            "OBJECTID": 33,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 34,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2658500819158, 25.61431646073612]
        },
        "properties": {
            "OBJECTID": 34,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 35,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26581781424079, 25.614360430389581]
        },
        "properties": {
            "OBJECTID": 35,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 36,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26580315709009, 25.614372129670073]
        },
        "properties": {
            "OBJECTID": 36,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 37,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26576030709253, 25.614392993042259]
        },
        "properties": {
            "OBJECTID": 37,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 38,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2657453693534, 25.614396845737872]
        },
        "properties": {
            "OBJECTID": 38,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 39,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26555349359893, 25.614436683905865]
        },
        "properties": {
            "OBJECTID": 39,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 40,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26553457096372, 25.614443332593794]
        },
        "properties": {
            "OBJECTID": 40,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 41,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26551127132819, 25.614474411365052]
        },
        "properties": {
            "OBJECTID": 41,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 42,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26553533718612, 25.614672558991458]
        },
        "properties": {
            "OBJECTID": 42,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 43,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26553555841934, 25.614674217341303]
        },
        "properties": {
            "OBJECTID": 43,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 44,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26596691823875, 25.615161323436382]
        },
        "properties": {
            "OBJECTID": 44,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 45,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26602244058324, 25.615164639236752]
        },
        "properties": {
            "OBJECTID": 45,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 46,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26612716573618, 25.615189608014077]
        },
        "properties": {
            "OBJECTID": 46,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 47,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26616215386036, 25.615205175278675]
        },
        "properties": {
            "OBJECTID": 47,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 48,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26618481317865, 25.615221969218567]
        },
        "properties": {
            "OBJECTID": 48,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 49,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26619150503399, 25.615228421854226]
        },
        "properties": {
            "OBJECTID": 49,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 50,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26632194630082, 25.615402238322702]
        },
        "properties": {
            "OBJECTID": 50,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 51,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2663220308371, 25.615402354335231]
        },
        "properties": {
            "OBJECTID": 51,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 52,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26633587679936, 25.615425731312484]
        },
        "properties": {
            "OBJECTID": 52,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 53,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26633952444956, 25.615521031570381]
        },
        "properties": {
            "OBJECTID": 53,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 54,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26631667987095, 25.615559963221756]
        },
        "properties": {
            "OBJECTID": 54,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 55,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26597753563482, 25.615899143430795]
        },
        "properties": {
            "OBJECTID": 55,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 56,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26596864583638, 25.615902813564048]
        },
        "properties": {
            "OBJECTID": 56,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 57,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26593525310949, 25.615919824240564]
        },
        "properties": {
            "OBJECTID": 57,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 58,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26593649777124, 25.615959315270231]
        },
        "properties": {
            "OBJECTID": 58,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 59,
        "geometry": {
            "type": "Point",
            "coordinates": [100.265925544928, 25.616002281280316]
        },
        "properties": {
            "OBJECTID": 59,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 60,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2659215312537, 25.616010356292975]
        },
        "properties": {
            "OBJECTID": 60,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 61,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26591179968989, 25.616025705921629]
        },
        "properties": {
            "OBJECTID": 61,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 62,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26589994212867, 25.616039774016429]
        },
        "properties": {
            "OBJECTID": 62,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 63,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26587862010228, 25.616057951113589]
        },
        "properties": {
            "OBJECTID": 63,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 64,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26587069077982, 25.616063124014033]
        },
        "properties": {
            "OBJECTID": 64,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 65,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26577641574903, 25.616154614743721]
        },
        "properties": {
            "OBJECTID": 65,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 66,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2656917391825, 25.616286135197015]
        },
        "properties": {
            "OBJECTID": 66,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 67,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26566145900921, 25.61631949105174]
        },
        "properties": {
            "OBJECTID": 67,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 68,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26560537189044, 25.616360544203815]
        },
        "properties": {
            "OBJECTID": 68,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 69,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26549870510121, 25.616525979890127]
        },
        "properties": {
            "OBJECTID": 69,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 70,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2654904187479, 25.616530672552528]
        },
        "properties": {
            "OBJECTID": 70,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 71,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26547292063884, 25.616538535325219]
        },
        "properties": {
            "OBJECTID": 71,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 72,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26536780697876, 25.616541839434376]
        },
        "properties": {
            "OBJECTID": 72,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 73,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2653370834397, 25.616535368812265]
        },
        "properties": {
            "OBJECTID": 73,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 74,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26530369341077, 25.616632758195294]
        },
        "properties": {
            "OBJECTID": 74,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 75,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26529987758732, 25.616650437967394]
        },
        "properties": {
            "OBJECTID": 75,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 76,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26528962171875, 25.616675289832813]
        },
        "properties": {
            "OBJECTID": 76,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 77,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26528072742371, 25.616790805950984]
        },
        "properties": {
            "OBJECTID": 77,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 78,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26530649210099, 25.616828207855576]
        },
        "properties": {
            "OBJECTID": 78,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 79,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26531762930523, 25.616871136094119]
        },
        "properties": {
            "OBJECTID": 79,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 80,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26532176618662, 25.616964727640095]
        },
        "properties": {
            "OBJECTID": 80,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 81,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26531570025941, 25.61706711095843]
        },
        "properties": {
            "OBJECTID": 81,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 82,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26531246539804, 25.617082596384762]
        },
        "properties": {
            "OBJECTID": 82,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 83,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26529720030561, 25.617131970963726]
        },
        "properties": {
            "OBJECTID": 83,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 84,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26528859829023, 25.617147865581558]
        },
        "properties": {
            "OBJECTID": 84,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 85,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26521372972991, 25.617220512816459]
        },
        "properties": {
            "OBJECTID": 85,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 86,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26518528687154, 25.617239916588915]
        },
        "properties": {
            "OBJECTID": 86,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 87,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26517667856092, 25.617244107429656]
        },
        "properties": {
            "OBJECTID": 87,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 88,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26517420002938, 25.617245195609314]
        },
        "properties": {
            "OBJECTID": 88,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 89,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26509432584254, 25.617279433698968]
        },
        "properties": {
            "OBJECTID": 89,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 90,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26502104008898, 25.61729214921337]
        },
        "properties": {
            "OBJECTID": 90,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 91,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26495611892983, 25.617274418179932]
        },
        "properties": {
            "OBJECTID": 91,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 92,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26469931302296, 25.617386140058386]
        },
        "properties": {
            "OBJECTID": 92,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 93,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26453049138911, 25.617646312127647]
        },
        "properties": {
            "OBJECTID": 93,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 94,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26414440074353, 25.618289232961786]
        },
        "properties": {
            "OBJECTID": 94,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 95,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26376629787643, 25.618867679599759]
        },
        "properties": {
            "OBJECTID": 95,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 96,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26371884874595, 25.618914548667533]
        },
        "properties": {
            "OBJECTID": 96,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 97,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26366507288486, 25.618934853560688]
        },
        "properties": {
            "OBJECTID": 97,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 98,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26365543664912, 25.618936313160361]
        },
        "properties": {
            "OBJECTID": 98,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 99,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26364571587715, 25.618937198992569]
        },
        "properties": {
            "OBJECTID": 99,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 100,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26357834136741, 25.619040871039431]
        },
        "properties": {
            "OBJECTID": 100,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 101,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26366906497543, 25.619096791783591]
        },
        "properties": {
            "OBJECTID": 101,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 102,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26378995814014, 25.619176537367366]
        },
        "properties": {
            "OBJECTID": 102,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 103,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26380466565286, 25.61918818538652]
        },
        "properties": {
            "OBJECTID": 103,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 104,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26382327712258, 25.619208663848781]
        },
        "properties": {
            "OBJECTID": 104,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 105,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26385248530403, 25.619308469710177]
        },
        "properties": {
            "OBJECTID": 105,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 106,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26384153156147, 25.619351434820885]
        },
        "properties": {
            "OBJECTID": 106,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 107,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2638292333325, 25.619372851276125]
        },
        "properties": {
            "OBJECTID": 107,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 108,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26307989302268, 25.620429910811765]
        },
        "properties": {
            "OBJECTID": 108,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 109,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26263626555129, 25.621027943784611]
        },
        "properties": {
            "OBJECTID": 109,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 110,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26263008271223, 25.621034802014549]
        },
        "properties": {
            "OBJECTID": 110,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 111,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26262341783655, 25.621041278032578]
        },
        "properties": {
            "OBJECTID": 111,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 112,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26250564711796, 25.621130834320581]
        },
        "properties": {
            "OBJECTID": 112,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 113,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26224369079375, 25.621451362590335]
        },
        "properties": {
            "OBJECTID": 113,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 114,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26222209987009, 25.621480782112428]
        },
        "properties": {
            "OBJECTID": 114,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 115,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26218456036923, 25.621508822074532]
        },
        "properties": {
            "OBJECTID": 115,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 116,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26205376476946, 25.621565794126127]
        },
        "properties": {
            "OBJECTID": 116,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 117,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26205135728435, 25.621579101394445]
        },
        "properties": {
            "OBJECTID": 117,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 118,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26232942676171, 25.621858221279922]
        },
        "properties": {
            "OBJECTID": 118,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 119,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26269287157879, 25.622210831964367]
        },
        "properties": {
            "OBJECTID": 119,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 120,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2627615411123, 25.622195958976363]
        },
        "properties": {
            "OBJECTID": 120,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 121,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26305029993148, 25.622186187842374]
        },
        "properties": {
            "OBJECTID": 121,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 122,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2631736347555, 25.622202756052388]
        },
        "properties": {
            "OBJECTID": 122,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 123,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26332064513173, 25.622180894432802]
        },
        "properties": {
            "OBJECTID": 123,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 124,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26345029769317, 25.622190322025801]
        },
        "properties": {
            "OBJECTID": 124,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 125,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26345234634874, 25.622190547755622]
        },
        "properties": {
            "OBJECTID": 125,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 126,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26390434021317, 25.622313728795575]
        },
        "properties": {
            "OBJECTID": 126,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 127,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26391005990138, 25.622315429413561]
        },
        "properties": {
            "OBJECTID": 127,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 128,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26414770755071, 25.622319292901125]
        },
        "properties": {
            "OBJECTID": 128,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 129,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26416520655908, 25.622311431027754]
        },
        "properties": {
            "OBJECTID": 129,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 130,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26707237928696, 25.621344678714024]
        },
        "properties": {
            "OBJECTID": 130,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 131,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26709152405465, 25.621341190243811]
        },
        "properties": {
            "OBJECTID": 131,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 132,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26714014320311, 25.621342547320751]
        },
        "properties": {
            "OBJECTID": 132,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 133,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26715901637556, 25.621347096091654]
        },
        "properties": {
            "OBJECTID": 133,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 134,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2671681669774, 25.621350199652056]
        },
        "properties": {
            "OBJECTID": 134,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 135,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26719400629844, 25.621362663356251]
        },
        "properties": {
            "OBJECTID": 135,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 136,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2672404526848, 25.621407445996908]
        },
        "properties": {
            "OBJECTID": 136,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 137,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26724507520009, 25.621415251212909]
        },
        "properties": {
            "OBJECTID": 137,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 138,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2672491248473, 25.621423311836395]
        },
        "properties": {
            "OBJECTID": 138,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 139,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26730127473411, 25.621574125445591]
        },
        "properties": {
            "OBJECTID": 139,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 140,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26729876382694, 25.621591684708505]
        },
        "properties": {
            "OBJECTID": 140,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 141,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26714304711402, 25.621976081031391]
        },
        "properties": {
            "OBJECTID": 141,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 142,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26714290861838, 25.621977361665984]
        },
        "properties": {
            "OBJECTID": 142,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 143,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2671430911808, 25.621978656689691]
        },
        "properties": {
            "OBJECTID": 143,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 144,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26731018521696, 25.622018871673617]
        },
        "properties": {
            "OBJECTID": 144,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 145,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26733150814266, 25.622000696375096]
        },
        "properties": {
            "OBJECTID": 145,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 146,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26733943836444, 25.621995522575332]
        },
        "properties": {
            "OBJECTID": 146,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 147,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26738370299557, 25.621977244754078]
        },
        "properties": {
            "OBJECTID": 147,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 148,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26744182528006, 25.621973687935395]
        },
        "properties": {
            "OBJECTID": 148,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 149,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26777080268045, 25.622035760042365]
        },
        "properties": {
            "OBJECTID": 149,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 150,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26792450940866, 25.622069018770276]
        },
        "properties": {
            "OBJECTID": 150,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 151,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26793386415659, 25.622071573744165]
        },
        "properties": {
            "OBJECTID": 151,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 152,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26808603124539, 25.62214759343675]
        },
        "properties": {
            "OBJECTID": 152,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 153,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26856540856681, 25.622415637271899]
        },
        "properties": {
            "OBJECTID": 153,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 154,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26858923340643, 25.622431049853105]
        },
        "properties": {
            "OBJECTID": 154,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 155,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26884807897574, 25.622470216227555]
        },
        "properties": {
            "OBJECTID": 155,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 156,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26891818292785, 25.622060341211807]
        },
        "properties": {
            "OBJECTID": 156,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 157,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2690685073058, 25.621541557396995]
        },
        "properties": {
            "OBJECTID": 157,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 158,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2691141973624, 25.621486594430849]
        },
        "properties": {
            "OBJECTID": 158,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 159,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26912038020146, 25.621479736200911]
        },
        "properties": {
            "OBJECTID": 159,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 160,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26986684088206, 25.621053814581956]
        },
        "properties": {
            "OBJECTID": 160,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 161,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26996972961939, 25.621462506989133]
        },
        "properties": {
            "OBJECTID": 161,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 162,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27027185865938, 25.623194108421444]
        },
        "properties": {
            "OBJECTID": 162,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 163,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27055996816807, 25.624946298033251]
        },
        "properties": {
            "OBJECTID": 163,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 164,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27108590429111, 25.628009239634764]
        },
        "properties": {
            "OBJECTID": 164,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 165,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27224447300409, 25.630846195995218]
        },
        "properties": {
            "OBJECTID": 165,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 166,
        "geometry": {
            "type": "Point",
            "coordinates": [100.273077678692, 25.63237849776948]
        },
        "properties": {
            "OBJECTID": 166,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 167,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27480157113564, 25.635591005620483]
        },
        "properties": {
            "OBJECTID": 167,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 168,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27507263579355, 25.636063549892981]
        },
        "properties": {
            "OBJECTID": 168,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 169,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27543225309608, 25.63672649683042]
        },
        "properties": {
            "OBJECTID": 169,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 170,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27566958508334, 25.637187476720726]
        },
        "properties": {
            "OBJECTID": 170,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 171,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27580558236178, 25.637390004045528]
        },
        "properties": {
            "OBJECTID": 171,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 172,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27600420033366, 25.637756665737584]
        },
        "properties": {
            "OBJECTID": 172,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 173,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27814952228266, 25.64022279564557]
        },
        "properties": {
            "OBJECTID": 173,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 174,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2785383081964, 25.640547687425908]
        },
        "properties": {
            "OBJECTID": 174,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 175,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27902454644573, 25.640902285612015]
        },
        "properties": {
            "OBJECTID": 175,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 176,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28118671629397, 25.64239364575019]
        },
        "properties": {
            "OBJECTID": 176,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 177,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28178374922072, 25.642910719953818]
        },
        "properties": {
            "OBJECTID": 177,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 178,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28238197824578, 25.643625463344506]
        },
        "properties": {
            "OBJECTID": 178,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 179,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28309350386218, 25.648925089021702]
        },
        "properties": {
            "OBJECTID": 179,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 180,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28304418773911, 25.650698746350088]
        },
        "properties": {
            "OBJECTID": 180,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 181,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28303254601525, 25.651028963016302]
        },
        "properties": {
            "OBJECTID": 181,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 182,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28262481138597, 25.652325461650776]
        },
        "properties": {
            "OBJECTID": 182,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 183,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28258855162039, 25.652718992387634]
        },
        "properties": {
            "OBJECTID": 183,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 184,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28262200190392, 25.652957399964066]
        },
        "properties": {
            "OBJECTID": 184,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 185,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28262114125272, 25.652996845128371]
        },
        "properties": {
            "OBJECTID": 185,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 186,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28257087724427, 25.653335113424816]
        },
        "properties": {
            "OBJECTID": 186,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 187,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28256867840184, 25.653343743319169]
        },
        "properties": {
            "OBJECTID": 187,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 188,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28248744533937, 25.653903720580274]
        },
        "properties": {
            "OBJECTID": 188,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 189,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28244141623844, 25.654299353932117]
        },
        "properties": {
            "OBJECTID": 189,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 190,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28256539227908, 25.654297129009365]
        },
        "properties": {
            "OBJECTID": 190,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 191,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28278947095737, 25.654273716958528]
        },
        "properties": {
            "OBJECTID": 191,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 192,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28384276862892, 25.653930913380975]
        },
        "properties": {
            "OBJECTID": 192,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 193,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2838519120362, 25.653928373695521]
        },
        "properties": {
            "OBJECTID": 193,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 194,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28443973680447, 25.653779582661684]
        },
        "properties": {
            "OBJECTID": 194,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 195,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28676578111595, 25.654165230840761]
        },
        "properties": {
            "OBJECTID": 195,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 196,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28868306287313, 25.655776389668688]
        },
        "properties": {
            "OBJECTID": 196,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 197,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28951071704603, 25.657977643153743]
        },
        "properties": {
            "OBJECTID": 197,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 198,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28999401181426, 25.659289128090847]
        },
        "properties": {
            "OBJECTID": 198,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 199,
        "geometry": {
            "type": "Point",
            "coordinates": [100.29010663391409, 25.661320571587169]
        },
        "properties": {
            "OBJECTID": 199,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 200,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28993491376531, 25.661425249975366]
        },
        "properties": {
            "OBJECTID": 200,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 201,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28927473314246, 25.661912596189438]
        },
        "properties": {
            "OBJECTID": 201,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 202,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28896875770209, 25.662171489422747]
        },
        "properties": {
            "OBJECTID": 202,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 203,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28894701569226, 25.662192281748446]
        },
        "properties": {
            "OBJECTID": 203,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 204,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2885518149144, 25.662582981419519]
        },
        "properties": {
            "OBJECTID": 204,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 205,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28742344183843, 25.663494670942384]
        },
        "properties": {
            "OBJECTID": 205,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 206,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28724472316549, 25.66360961329309]
        },
        "properties": {
            "OBJECTID": 206,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 207,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28720553430804, 25.663633045128961]
        },
        "properties": {
            "OBJECTID": 207,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 208,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28700186214627, 25.663749234839372]
        },
        "properties": {
            "OBJECTID": 208,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 209,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2866509718649, 25.663927177397397]
        },
        "properties": {
            "OBJECTID": 209,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 210,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28639599338044, 25.664050101231226]
        },
        "properties": {
            "OBJECTID": 210,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 211,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28630032080321, 25.664097010768501]
        },
        "properties": {
            "OBJECTID": 211,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 212,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2862232210249, 25.664133995387601]
        },
        "properties": {
            "OBJECTID": 212,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 213,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28619082025023, 25.664149699349196]
        },
        "properties": {
            "OBJECTID": 213,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 214,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28507292787339, 25.664690096570723]
        },
        "properties": {
            "OBJECTID": 214,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 215,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28395684403318, 25.66519551466132]
        },
        "properties": {
            "OBJECTID": 215,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 216,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28391599142992, 25.665209575561505]
        },
        "properties": {
            "OBJECTID": 216,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 217,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28369453877201, 25.665273246663048]
        },
        "properties": {
            "OBJECTID": 217,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 218,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28349205371535, 25.665317871022978]
        },
        "properties": {
            "OBJECTID": 218,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 219,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28333622818445, 25.665342581694858]
        },
        "properties": {
            "OBJECTID": 219,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 220,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28290492322367, 25.665371726024375]
        },
        "properties": {
            "OBJECTID": 220,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 221,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28275592174845, 25.665368458787384]
        },
        "properties": {
            "OBJECTID": 221,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 222,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28249330262389, 25.665349435428141]
        },
        "properties": {
            "OBJECTID": 222,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 223,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28217053594153, 25.665306839938751]
        },
        "properties": {
            "OBJECTID": 223,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 224,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28156308976486, 25.665213521786654]
        },
        "properties": {
            "OBJECTID": 224,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 225,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28131968195783, 25.665176865420051]
        },
        "properties": {
            "OBJECTID": 225,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 226,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28113625443467, 25.665148445944055]
        },
        "properties": {
            "OBJECTID": 226,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 227,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28104390125509, 25.665133653895055]
        },
        "properties": {
            "OBJECTID": 227,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 228,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28040651484883, 25.665034958696424]
        },
        "properties": {
            "OBJECTID": 228,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 229,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28021690088934, 25.665007107591975]
        },
        "properties": {
            "OBJECTID": 229,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 230,
        "geometry": {
            "type": "Point",
            "coordinates": [100.28017742874539, 25.665000752083074]
        },
        "properties": {
            "OBJECTID": 230,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 231,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2795095058591, 25.664873454846088]
        },
        "properties": {
            "OBJECTID": 231,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 232,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27922165895245, 25.664765326658539]
        },
        "properties": {
            "OBJECTID": 232,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 233,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27902003364767, 25.664657855875419]
        },
        "properties": {
            "OBJECTID": 233,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 234,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27869911057553, 25.664416767419937]
        },
        "properties": {
            "OBJECTID": 234,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 235,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27866740857411, 25.664383886407279]
        },
        "properties": {
            "OBJECTID": 235,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 236,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27843786291709, 25.664155776068242]
        },
        "properties": {
            "OBJECTID": 236,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 237,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27831838888261, 25.664039335447001]
        },
        "properties": {
            "OBJECTID": 237,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 238,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27825965955572, 25.663986931951399]
        },
        "properties": {
            "OBJECTID": 238,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 239,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27805369861937, 25.663840156298249]
        },
        "properties": {
            "OBJECTID": 239,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 240,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27780857940246, 25.66374193594163]
        },
        "properties": {
            "OBJECTID": 240,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 241,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27773093643361, 25.663727222133673]
        },
        "properties": {
            "OBJECTID": 241,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 242,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27762939488071, 25.663718709151169]
        },
        "properties": {
            "OBJECTID": 242,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 243,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27722779812444, 25.663801048379753]
        },
        "properties": {
            "OBJECTID": 243,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 244,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27705158316411, 25.663912295416139]
        },
        "properties": {
            "OBJECTID": 244,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 245,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27697612914511, 25.663985260111701]
        },
        "properties": {
            "OBJECTID": 245,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 246,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27689174036158, 25.664095741825065]
        },
        "properties": {
            "OBJECTID": 246,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 247,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27675662891534, 25.664442606740977]
        },
        "properties": {
            "OBJECTID": 247,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 248,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27652979291565, 25.665615134730729]
        },
        "properties": {
            "OBJECTID": 248,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 249,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27607628189497, 25.666974577814585]
        },
        "properties": {
            "OBJECTID": 249,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 250,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27440769595847, 25.669163204818119]
        },
        "properties": {
            "OBJECTID": 250,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 251,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27348189427408, 25.670116735299132]
        },
        "properties": {
            "OBJECTID": 251,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 252,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27294926989293, 25.671060402915316]
        },
        "properties": {
            "OBJECTID": 252,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 253,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27287378889429, 25.671445507903911]
        },
        "properties": {
            "OBJECTID": 253,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 254,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2727025741645, 25.672629166802665]
        },
        "properties": {
            "OBJECTID": 254,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 255,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27266995485462, 25.67279324900926]
        },
        "properties": {
            "OBJECTID": 255,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 256,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27259022455928, 25.673071655732372]
        },
        "properties": {
            "OBJECTID": 256,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 257,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27240373934114, 25.673531401752825]
        },
        "properties": {
            "OBJECTID": 257,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 258,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27219542667768, 25.674006027955897]
        },
        "properties": {
            "OBJECTID": 258,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 259,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27169934624789, 25.675092525000082]
        },
        "properties": {
            "OBJECTID": 259,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 260,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27166912183259, 25.675142252113289]
        },
        "properties": {
            "OBJECTID": 260,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 261,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27128163813717, 25.675632583177219]
        },
        "properties": {
            "OBJECTID": 261,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 262,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27099114632438, 25.675928145367664]
        },
        "properties": {
            "OBJECTID": 262,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 263,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27079187634411, 25.676166147349875]
        },
        "properties": {
            "OBJECTID": 263,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 264,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27065094178761, 25.676418474632953]
        },
        "properties": {
            "OBJECTID": 264,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 265,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27063893224101, 25.676448592928296]
        },
        "properties": {
            "OBJECTID": 265,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 266,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2703805965873, 25.677401501078464]
        },
        "properties": {
            "OBJECTID": 266,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 267,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26986690833121, 25.678228307190693]
        },
        "properties": {
            "OBJECTID": 267,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 268,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26973400202263, 25.678474540667594]
        },
        "properties": {
            "OBJECTID": 268,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 269,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26971680788444, 25.678510601682945]
        },
        "properties": {
            "OBJECTID": 269,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 270,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26960836133685, 25.678828154096095]
        },
        "properties": {
            "OBJECTID": 270,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 271,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26958241499653, 25.678924509258763]
        },
        "properties": {
            "OBJECTID": 271,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 272,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26956340242913, 25.679013822729758]
        },
        "properties": {
            "OBJECTID": 272,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 273,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26954676317263, 25.679095774350571]
        },
        "properties": {
            "OBJECTID": 273,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 274,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26946157758977, 25.679503655569306]
        },
        "properties": {
            "OBJECTID": 274,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 275,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26929895588216, 25.680476885699761]
        },
        "properties": {
            "OBJECTID": 275,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 276,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26930535905512, 25.680587138985345]
        },
        "properties": {
            "OBJECTID": 276,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 277,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26960899356027, 25.681296008903757]
        },
        "properties": {
            "OBJECTID": 277,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 278,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27000615216224, 25.681433885765273]
        },
        "properties": {
            "OBJECTID": 278,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 279,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27087436306147, 25.681319966843034]
        },
        "properties": {
            "OBJECTID": 279,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 280,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27121820805394, 25.681327769361133]
        },
        "properties": {
            "OBJECTID": 280,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 281,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27170818658362, 25.681467157982979]
        },
        "properties": {
            "OBJECTID": 281,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 282,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27207720809673, 25.681716181156901]
        },
        "properties": {
            "OBJECTID": 282,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 283,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27233708968498, 25.682053864893987]
        },
        "properties": {
            "OBJECTID": 283,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 284,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27251750178135, 25.682861290616131]
        },
        "properties": {
            "OBJECTID": 284,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 285,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27252936923509, 25.683017700706387]
        },
        "properties": {
            "OBJECTID": 285,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 286,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27257262842414, 25.683312288031743]
        },
        "properties": {
            "OBJECTID": 286,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 287,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27270262992249, 25.683692482721938]
        },
        "properties": {
            "OBJECTID": 287,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 288,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27280553934423, 25.683875367953931]
        },
        "properties": {
            "OBJECTID": 288,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 289,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27314785009327, 25.684324031628819]
        },
        "properties": {
            "OBJECTID": 289,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 290,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27343605672871, 25.684707895553004]
        },
        "properties": {
            "OBJECTID": 290,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 291,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27360509869641, 25.685037215595059]
        },
        "properties": {
            "OBJECTID": 291,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 292,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27363866139518, 25.68512348755894]
        },
        "properties": {
            "OBJECTID": 292,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 293,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27366416257109, 25.685196144686358]
        },
        "properties": {
            "OBJECTID": 293,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 294,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27369600756475, 25.685298331952481]
        },
        "properties": {
            "OBJECTID": 294,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 295,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27393347534968, 25.686246459306687]
        },
        "properties": {
            "OBJECTID": 295,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 296,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2740367480975, 25.686669665872444]
        },
        "properties": {
            "OBJECTID": 296,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 297,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27408178614559, 25.686852353253528]
        },
        "properties": {
            "OBJECTID": 297,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 298,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27411105368225, 25.686978029912211]
        },
        "properties": {
            "OBJECTID": 298,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 299,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27430229001874, 25.687759488609572]
        },
        "properties": {
            "OBJECTID": 299,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 300,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27436292860631, 25.688007394825433]
        },
        "properties": {
            "OBJECTID": 300,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 301,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27443426912629, 25.688340955170986]
        },
        "properties": {
            "OBJECTID": 301,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 302,
        "geometry": {
            "type": "Point",
            "coordinates": [100.274446639301, 25.68841589657643]
        },
        "properties": {
            "OBJECTID": 302,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 303,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27445585465404, 25.688489297443198]
        },
        "properties": {
            "OBJECTID": 303,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 304,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27445391841366, 25.689067366365919]
        },
        "properties": {
            "OBJECTID": 304,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 305,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27413416356086, 25.690695561051371]
        },
        "properties": {
            "OBJECTID": 305,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 306,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27408815514428, 25.690965659837389]
        },
        "properties": {
            "OBJECTID": 306,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 307,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27416572886534, 25.692008038839958]
        },
        "properties": {
            "OBJECTID": 307,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 308,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2742008078211, 25.692120733784861]
        },
        "properties": {
            "OBJECTID": 308,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 309,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27432669941771, 25.692455036970443]
        },
        "properties": {
            "OBJECTID": 309,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 310,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27437262239869, 25.692579462672143]
        },
        "properties": {
            "OBJECTID": 310,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 311,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27449572609692, 25.693025458957834]
        },
        "properties": {
            "OBJECTID": 311,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 312,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27447796628519, 25.693567618850579]
        },
        "properties": {
            "OBJECTID": 312,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 313,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27447195881393, 25.693602282319546]
        },
        "properties": {
            "OBJECTID": 313,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 314,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27434889018917, 25.693947273048252]
        },
        "properties": {
            "OBJECTID": 314,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 315,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2740203291749, 25.694415217288508]
        },
        "properties": {
            "OBJECTID": 315,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 316,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27390873320155, 25.694537038553563]
        },
        "properties": {
            "OBJECTID": 316,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 317,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27371846633423, 25.694696574687214]
        },
        "properties": {
            "OBJECTID": 317,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 318,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2731585646161, 25.695524828707903]
        },
        "properties": {
            "OBJECTID": 318,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 319,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27313461387138, 25.695625881928947]
        },
        "properties": {
            "OBJECTID": 319,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 320,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27303881359046, 25.696195544888553]
        },
        "properties": {
            "OBJECTID": 320,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 321,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27300682560457, 25.696323064258081]
        },
        "properties": {
            "OBJECTID": 321,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 322,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27287069342782, 25.696624592550961]
        },
        "properties": {
            "OBJECTID": 322,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 323,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27247948833775, 25.697085346711447]
        },
        "properties": {
            "OBJECTID": 323,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 324,
        "geometry": {
            "type": "Point",
            "coordinates": [100.272352864693, 25.69719186421321]
        },
        "properties": {
            "OBJECTID": 324,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 325,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27168416569174, 25.6977256658069]
        },
        "properties": {
            "OBJECTID": 325,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 326,
        "geometry": {
            "type": "Point",
            "coordinates": [100.27129363869051, 25.698034639586979]
        },
        "properties": {
            "OBJECTID": 326,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 327,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2703645391922, 25.6985782051201]
        },
        "properties": {
            "OBJECTID": 327,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 328,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26985204883306, 25.698770781446456]
        },
        "properties": {
            "OBJECTID": 328,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 329,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26944323142004, 25.698904655425565]
        },
        "properties": {
            "OBJECTID": 329,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 330,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26909093639767, 25.699019884660004]
        },
        "properties": {
            "OBJECTID": 330,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 331,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26819841232691, 25.699311674194462]
        },
        "properties": {
            "OBJECTID": 331,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 332,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26696254958205, 25.699985632431037]
        },
        "properties": {
            "OBJECTID": 332,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 333,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26643275266946, 25.701498512446449]
        },
        "properties": {
            "OBJECTID": 333,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 334,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26629242425605, 25.702112958046655]
        },
        "properties": {
            "OBJECTID": 334,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 335,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26611412376786, 25.702632016154666]
        },
        "properties": {
            "OBJECTID": 335,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 336,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26602193066765, 25.702792578414801]
        },
        "properties": {
            "OBJECTID": 336,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 337,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26533051299282, 25.703381786340401]
        },
        "properties": {
            "OBJECTID": 337,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 338,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26482077995507, 25.703541993368333]
        },
        "properties": {
            "OBJECTID": 338,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 339,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26316066113566, 25.703760649134722]
        },
        "properties": {
            "OBJECTID": 339,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 340,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26304055487776, 25.703771310597574]
        },
        "properties": {
            "OBJECTID": 340,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 341,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26211523882728, 25.703896835270768]
        },
        "properties": {
            "OBJECTID": 341,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 342,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26199155326765, 25.703923769966082]
        },
        "properties": {
            "OBJECTID": 342,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 343,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26193081665394, 25.703943724123633]
        },
        "properties": {
            "OBJECTID": 343,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 344,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26169297924764, 25.704029034712221]
        },
        "properties": {
            "OBJECTID": 344,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 345,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26145240790237, 25.704206516817351]
        },
        "properties": {
            "OBJECTID": 345,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 346,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26140344431354, 25.704262650700798]
        },
        "properties": {
            "OBJECTID": 346,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 347,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26123827662525, 25.704697281354527]
        },
        "properties": {
            "OBJECTID": 347,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 348,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26127521088233, 25.704959625286506]
        },
        "properties": {
            "OBJECTID": 348,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 349,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26145207695185, 25.705250260990795]
        },
        "properties": {
            "OBJECTID": 349,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 350,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26182285484094, 25.705527997718946]
        },
        "properties": {
            "OBJECTID": 350,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 351,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2627264963308, 25.706062805654028]
        },
        "properties": {
            "OBJECTID": 351,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 352,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26286058794585, 25.706197301064662]
        },
        "properties": {
            "OBJECTID": 352,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 353,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26298812440245, 25.706360276205828]
        },
        "properties": {
            "OBJECTID": 353,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 354,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26307453306333, 25.706507789303089]
        },
        "properties": {
            "OBJECTID": 354,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 355,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26319505031137, 25.706839697594035]
        },
        "properties": {
            "OBJECTID": 355,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 356,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26321412673065, 25.707327247054877]
        },
        "properties": {
            "OBJECTID": 356,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 357,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26318108294066, 25.707494519156796]
        },
        "properties": {
            "OBJECTID": 357,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 358,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26314693298463, 25.707617140818456]
        },
        "properties": {
            "OBJECTID": 358,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 359,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26271874777206, 25.708456569814871]
        },
        "properties": {
            "OBJECTID": 359,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 360,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26189229689203, 25.709932763786071]
        },
        "properties": {
            "OBJECTID": 360,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 361,
        "geometry": {
            "type": "Point",
            "coordinates": [100.26120137924033, 25.711170209337752]
        },
        "properties": {
            "OBJECTID": 361,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 362,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2606380052398, 25.712179336807026]
        },
        "properties": {
            "OBJECTID": 362,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 363,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25999581465516, 25.712675662751678]
        },
        "properties": {
            "OBJECTID": 363,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 364,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25952211385453, 25.7129171172312]
        },
        "properties": {
            "OBJECTID": 364,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 365,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25858914907002, 25.713417695170563]
        },
        "properties": {
            "OBJECTID": 365,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 366,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25691166074461, 25.71431125615959]
        },
        "properties": {
            "OBJECTID": 366,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 367,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25678238859643, 25.714379976954433]
        },
        "properties": {
            "OBJECTID": 367,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 368,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25555617287915, 25.715031718339162]
        },
        "properties": {
            "OBJECTID": 368,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 369,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25554196089286, 25.715039298724719]
        },
        "properties": {
            "OBJECTID": 369,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 370,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25498885265347, 25.715344514237984]
        },
        "properties": {
            "OBJECTID": 370,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 371,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25472868418149, 25.715529568634736]
        },
        "properties": {
            "OBJECTID": 371,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 372,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25449550616275, 25.715757864234092]
        },
        "properties": {
            "OBJECTID": 372,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 373,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25296445804338, 25.717472515244424]
        },
        "properties": {
            "OBJECTID": 373,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 374,
        "geometry": {
            "type": "Point",
            "coordinates": [100.25215109409777, 25.717755618227272]
        },
        "properties": {
            "OBJECTID": 374,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 375,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24998539081321, 25.718355197134713]
        },
        "properties": {
            "OBJECTID": 375,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 376,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24971261744338, 25.718405697664878]
        },
        "properties": {
            "OBJECTID": 376,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 377,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24708552867429, 25.718695477214567]
        },
        "properties": {
            "OBJECTID": 377,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 378,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24563825420211, 25.717905029793314]
        },
        "properties": {
            "OBJECTID": 378,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 379,
        "geometry": {
            "type": "Point",
            "coordinates": [100.244493482888, 25.717309219045262]
        },
        "properties": {
            "OBJECTID": 379,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 380,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24364213787442, 25.717136615762286]
        },
        "properties": {
            "OBJECTID": 380,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 381,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2418169547887, 25.71621858781765]
        },
        "properties": {
            "OBJECTID": 381,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 382,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24133923132052, 25.715876831950311]
        },
        "properties": {
            "OBJECTID": 382,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 383,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24113844328457, 25.715737609702956]
        },
        "properties": {
            "OBJECTID": 383,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 384,
        "geometry": {
            "type": "Point",
            "coordinates": [100.24112228876265, 25.715726408646901]
        },
        "properties": {
            "OBJECTID": 384,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 385,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23960372143466, 25.71453020420563]
        },
        "properties": {
            "OBJECTID": 385,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 386,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23933620010558, 25.71414373674412]
        },
        "properties": {
            "OBJECTID": 386,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 387,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23915173296615, 25.713744577150578]
        },
        "properties": {
            "OBJECTID": 387,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 388,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23912912131198, 25.713677512906941]
        },
        "properties": {
            "OBJECTID": 388,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 389,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23847089491591, 25.71126081873183]
        },
        "properties": {
            "OBJECTID": 389,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 390,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23806286171174, 25.710186755714233]
        },
        "properties": {
            "OBJECTID": 390,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 391,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23763449303749, 25.709425279952768]
        },
        "properties": {
            "OBJECTID": 391,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 392,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23728323763135, 25.708845026576739]
        },
        "properties": {
            "OBJECTID": 392,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 393,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23720756058054, 25.708715702267909]
        },
        "properties": {
            "OBJECTID": 393,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 394,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23718099011069, 25.708672330663603]
        },
        "properties": {
            "OBJECTID": 394,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 395,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23660529989473, 25.707775342358218]
        },
        "properties": {
            "OBJECTID": 395,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 396,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23658779279248, 25.707781637612527]
        },
        "properties": {
            "OBJECTID": 396,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 397,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23621962743391, 25.708154517217224]
        },
        "properties": {
            "OBJECTID": 397,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 398,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23618343331884, 25.70820036375585]
        },
        "properties": {
            "OBJECTID": 398,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 399,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23609912727295, 25.708324419836174]
        },
        "properties": {
            "OBJECTID": 399,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 400,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23594461115493, 25.70863707544072]
        },
        "properties": {
            "OBJECTID": 400,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 401,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23481831281492, 25.710729699815829]
        },
        "properties": {
            "OBJECTID": 401,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 402,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2345411480556, 25.711245351292007]
        },
        "properties": {
            "OBJECTID": 402,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 403,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23453872708063, 25.712380089769397]
        },
        "properties": {
            "OBJECTID": 403,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 404,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23477193028043, 25.713072154956023]
        },
        "properties": {
            "OBJECTID": 404,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 405,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2349742786401, 25.71326495521356]
        },
        "properties": {
            "OBJECTID": 405,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 406,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23504981090014, 25.713331836894781]
        },
        "properties": {
            "OBJECTID": 406,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 407,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23546338123015, 25.713704759666996]
        },
        "properties": {
            "OBJECTID": 407,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 408,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2356999829679, 25.713962555727392]
        },
        "properties": {
            "OBJECTID": 408,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 409,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23587782660047, 25.714728557776766]
        },
        "properties": {
            "OBJECTID": 409,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 410,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23575029733843, 25.715077826580455]
        },
        "properties": {
            "OBJECTID": 410,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 411,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23545726044426, 25.715658401913743]
        },
        "properties": {
            "OBJECTID": 411,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 412,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23467863820866, 25.71721020998325]
        },
        "properties": {
            "OBJECTID": 412,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 413,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23457331320787, 25.717511812020575]
        },
        "properties": {
            "OBJECTID": 413,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 414,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23442127022548, 25.718173837152904]
        },
        "properties": {
            "OBJECTID": 414,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 415,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2342614409128, 25.71885173531939]
        },
        "properties": {
            "OBJECTID": 415,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 416,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23411366341492, 25.719491513022945]
        },
        "properties": {
            "OBJECTID": 416,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 417,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23406457032371, 25.720008523374702]
        },
        "properties": {
            "OBJECTID": 417,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 418,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23406508473596, 25.720375575372543]
        },
        "properties": {
            "OBJECTID": 418,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 419,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23409748371199, 25.721929307091898]
        },
        "properties": {
            "OBJECTID": 419,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 420,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23402877101103, 25.724747634895493]
        },
        "properties": {
            "OBJECTID": 420,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 421,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23353681127486, 25.725897792249384]
        },
        "properties": {
            "OBJECTID": 421,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 422,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23319601138689, 25.726641569352978]
        },
        "properties": {
            "OBJECTID": 422,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 423,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23270949164976, 25.727074365389683]
        },
        "properties": {
            "OBJECTID": 423,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 424,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23237526400726, 25.727398126722221]
        },
        "properties": {
            "OBJECTID": 424,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 425,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2323597498027, 25.727440331006505]
        },
        "properties": {
            "OBJECTID": 425,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 426,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2323422094255, 25.727594919969647]
        },
        "properties": {
            "OBJECTID": 426,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 427,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23254978194694, 25.729172892015413]
        },
        "properties": {
            "OBJECTID": 427,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 428,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2324333970837, 25.730151520776133]
        },
        "properties": {
            "OBJECTID": 428,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 429,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23234071205428, 25.732191770433928]
        },
        "properties": {
            "OBJECTID": 429,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 430,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23233730812035, 25.732231161638822]
        },
        "properties": {
            "OBJECTID": 430,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 431,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2323836996481, 25.732451439782153]
        },
        "properties": {
            "OBJECTID": 431,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 432,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23242856862362, 25.732566974786096]
        },
        "properties": {
            "OBJECTID": 432,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 433,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23243867790273, 25.732603212068625]
        },
        "properties": {
            "OBJECTID": 433,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 434,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23260110625608, 25.733048186724488]
        },
        "properties": {
            "OBJECTID": 434,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 435,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23267897315617, 25.733456897118174]
        },
        "properties": {
            "OBJECTID": 435,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 436,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23256040294029, 25.733928194930286]
        },
        "properties": {
            "OBJECTID": 436,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 437,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2318105940837, 25.734929919180502]
        },
        "properties": {
            "OBJECTID": 437,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 438,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23121391189255, 25.7361966889261]
        },
        "properties": {
            "OBJECTID": 438,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 439,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23081513990684, 25.737430636114993]
        },
        "properties": {
            "OBJECTID": 439,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 440,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23051114567289, 25.740104034573562]
        },
        "properties": {
            "OBJECTID": 440,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 441,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23021066598949, 25.740522523295851]
        },
        "properties": {
            "OBJECTID": 441,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 442,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22860811727088, 25.741505322212902]
        },
        "properties": {
            "OBJECTID": 442,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 443,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22840438935117, 25.741663313311335]
        },
        "properties": {
            "OBJECTID": 443,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 444,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22799839580938, 25.742603753260653]
        },
        "properties": {
            "OBJECTID": 444,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 445,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22683943858925, 25.744717565663166]
        },
        "properties": {
            "OBJECTID": 445,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 446,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22702443542943, 25.745469725347618]
        },
        "properties": {
            "OBJECTID": 446,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 447,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2260545282939, 25.747864335770544]
        },
        "properties": {
            "OBJECTID": 447,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 448,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22568582693941, 25.748443175412262]
        },
        "properties": {
            "OBJECTID": 448,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 449,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22541311832083, 25.748817216041743]
        },
        "properties": {
            "OBJECTID": 449,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 450,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22525053438471, 25.749130277240567]
        },
        "properties": {
            "OBJECTID": 450,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 451,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22520069485626, 25.749289906004435]
        },
        "properties": {
            "OBJECTID": 451,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 452,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22512551602978, 25.750142253762078]
        },
        "properties": {
            "OBJECTID": 452,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 453,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22510862586245, 25.75044607982278]
        },
        "properties": {
            "OBJECTID": 453,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 454,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22509530240632, 25.750797976445483]
        },
        "properties": {
            "OBJECTID": 454,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 455,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22510044472978, 25.750897883930236]
        },
        "properties": {
            "OBJECTID": 455,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 456,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22513090926412, 25.751129233627296]
        },
        "properties": {
            "OBJECTID": 456,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 457,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22511629887816, 25.754173022367127]
        },
        "properties": {
            "OBJECTID": 457,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 458,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22501749126428, 25.754712130860185]
        },
        "properties": {
            "OBJECTID": 458,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 459,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22491422211374, 25.755275026421373]
        },
        "properties": {
            "OBJECTID": 459,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 460,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22483583540554, 25.755649946587823]
        },
        "properties": {
            "OBJECTID": 460,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 461,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22473699271808, 25.756046004419602]
        },
        "properties": {
            "OBJECTID": 461,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 462,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22435862634961, 25.75687558404104]
        },
        "properties": {
            "OBJECTID": 462,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 463,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22393522373164, 25.75767403003249]
        },
        "properties": {
            "OBJECTID": 463,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 464,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22272768603392, 25.758827852123716]
        },
        "properties": {
            "OBJECTID": 464,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 465,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22203630882865, 25.759624721603643]
        },
        "properties": {
            "OBJECTID": 465,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 466,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22174849070035, 25.760083150117339]
        },
        "properties": {
            "OBJECTID": 466,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 467,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22056103126664, 25.76249986407754]
        },
        "properties": {
            "OBJECTID": 467,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 468,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22005452409707, 25.763757482321978]
        },
        "properties": {
            "OBJECTID": 468,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 469,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21969476290303, 25.765189884705478]
        },
        "properties": {
            "OBJECTID": 469,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 470,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21960448625811, 25.765509477680325]
        },
        "properties": {
            "OBJECTID": 470,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 471,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21950015680687, 25.765870104022156]
        },
        "properties": {
            "OBJECTID": 471,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 472,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21896702700673, 25.767187485813849]
        },
        "properties": {
            "OBJECTID": 472,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 473,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21860569739511, 25.767802337008277]
        },
        "properties": {
            "OBJECTID": 473,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 474,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21792424510789, 25.770514060075868]
        },
        "properties": {
            "OBJECTID": 474,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 475,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21783251605785, 25.770803577023514]
        },
        "properties": {
            "OBJECTID": 475,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 476,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21738143500528, 25.771984722317143]
        },
        "properties": {
            "OBJECTID": 476,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 477,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21714765623938, 25.772941552410771]
        },
        "properties": {
            "OBJECTID": 477,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 478,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21743034103747, 25.774063804700859]
        },
        "properties": {
            "OBJECTID": 478,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 479,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21752181198212, 25.774530415246545]
        },
        "properties": {
            "OBJECTID": 479,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 480,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21752465383975, 25.774617330225681]
        },
        "properties": {
            "OBJECTID": 480,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 481,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21752306473769, 25.774738226088289]
        },
        "properties": {
            "OBJECTID": 481,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 482,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21750896156937, 25.774863390133419]
        },
        "properties": {
            "OBJECTID": 482,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 483,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21699110225762, 25.776049883695237]
        },
        "properties": {
            "OBJECTID": 483,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 484,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21691602865184, 25.776412538008287]
        },
        "properties": {
            "OBJECTID": 484,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 485,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21689905214959, 25.776671215404292]
        },
        "properties": {
            "OBJECTID": 485,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 486,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21689187376103, 25.776954589083061]
        },
        "properties": {
            "OBJECTID": 486,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 487,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21689086382236, 25.777101262213534]
        },
        "properties": {
            "OBJECTID": 487,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 488,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21687478214557, 25.777840686598438]
        },
        "properties": {
            "OBJECTID": 488,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 489,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2168475551706, 25.778289728887955]
        },
        "properties": {
            "OBJECTID": 489,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 490,
        "geometry": {
            "type": "Point",
            "coordinates": [100.216737782123, 25.778848724988734]
        },
        "properties": {
            "OBJECTID": 490,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 491,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21673514171346, 25.778858175064784]
        },
        "properties": {
            "OBJECTID": 491,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 492,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21670110776995, 25.778968604617432]
        },
        "properties": {
            "OBJECTID": 492,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 493,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21661899606988, 25.779205439279622]
        },
        "properties": {
            "OBJECTID": 493,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 494,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21657618564251, 25.77932668767653]
        },
        "properties": {
            "OBJECTID": 494,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 495,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21654001580913, 25.779431170911835]
        },
        "properties": {
            "OBJECTID": 495,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 496,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21623318331484, 25.780267410013039]
        },
        "properties": {
            "OBJECTID": 496,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 497,
        "geometry": {
            "type": "Point",
            "coordinates": [100.216047522775, 25.780644951703209]
        },
        "properties": {
            "OBJECTID": 497,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 498,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21585068186351, 25.78101788796522]
        },
        "properties": {
            "OBJECTID": 498,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 499,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21582504758794, 25.781188665624484]
        },
        "properties": {
            "OBJECTID": 499,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 500,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21588597395856, 25.781455664447435]
        },
        "properties": {
            "OBJECTID": 500,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 501,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21590084514793, 25.781495657298819]
        },
        "properties": {
            "OBJECTID": 501,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 502,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21597073506143, 25.781671117727967]
        },
        "properties": {
            "OBJECTID": 502,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 503,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21600687611652, 25.781755028072155]
        },
        "properties": {
            "OBJECTID": 503,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 504,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21609969784282, 25.781927811219589]
        },
        "properties": {
            "OBJECTID": 504,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 505,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21697566899201, 25.782905505584665]
        },
        "properties": {
            "OBJECTID": 505,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 506,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21722838388285, 25.783177913829718]
        },
        "properties": {
            "OBJECTID": 506,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 507,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21732377137499, 25.78330412018903]
        },
        "properties": {
            "OBJECTID": 507,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 508,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21734602959566, 25.783336261958993]
        },
        "properties": {
            "OBJECTID": 508,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 509,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21757068833665, 25.783724340106289]
        },
        "properties": {
            "OBJECTID": 509,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 510,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21770324390963, 25.783979058686668]
        },
        "properties": {
            "OBJECTID": 510,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 511,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21785127861369, 25.78426619692749]
        },
        "properties": {
            "OBJECTID": 511,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 512,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21792546998449, 25.784410130723018]
        },
        "properties": {
            "OBJECTID": 512,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 513,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21815290043605, 25.784869452263479]
        },
        "properties": {
            "OBJECTID": 513,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 514,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21868741878939, 25.788229897691679]
        },
        "properties": {
            "OBJECTID": 514,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 515,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2187059664073, 25.788386548800247]
        },
        "properties": {
            "OBJECTID": 515,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 516,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21872266322038, 25.788530811747592]
        },
        "properties": {
            "OBJECTID": 516,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 517,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21881883312273, 25.789209746832398]
        },
        "properties": {
            "OBJECTID": 517,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 518,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21892215893052, 25.789700880091573]
        },
        "properties": {
            "OBJECTID": 518,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 519,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21909687921766, 25.790489620599544]
        },
        "properties": {
            "OBJECTID": 519,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 520,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2191896397901, 25.790894912670126]
        },
        "properties": {
            "OBJECTID": 520,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 521,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21927796040961, 25.79129652471488]
        },
        "properties": {
            "OBJECTID": 521,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 522,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21906077683343, 25.793319939063963]
        },
        "properties": {
            "OBJECTID": 522,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 523,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21901826767896, 25.793534943582813]
        },
        "properties": {
            "OBJECTID": 523,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 524,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21899624328205, 25.793647759936221]
        },
        "properties": {
            "OBJECTID": 524,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 525,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21895701485442, 25.793862213170655]
        },
        "properties": {
            "OBJECTID": 525,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 526,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21893715512567, 25.793979137627161]
        },
        "properties": {
            "OBJECTID": 526,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 527,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21920453166388, 25.795671686898856]
        },
        "properties": {
            "OBJECTID": 527,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 528,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21950071438653, 25.796203370589126]
        },
        "properties": {
            "OBJECTID": 528,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 529,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21983213704357, 25.796779306319991]
        },
        "properties": {
            "OBJECTID": 529,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 530,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21985537912252, 25.796822380248727]
        },
        "properties": {
            "OBJECTID": 530,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 531,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22006043983748, 25.797202429248784]
        },
        "properties": {
            "OBJECTID": 531,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 532,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22010674413099, 25.797295468611026]
        },
        "properties": {
            "OBJECTID": 532,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 533,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22013795690123, 25.79736254184786]
        },
        "properties": {
            "OBJECTID": 533,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 534,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22031646873012, 25.797875728282293]
        },
        "properties": {
            "OBJECTID": 534,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 535,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22039918477464, 25.798546003795082]
        },
        "properties": {
            "OBJECTID": 535,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 536,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22044420663491, 25.799455496274163]
        },
        "properties": {
            "OBJECTID": 536,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 537,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2204533932096, 25.799631546658588]
        },
        "properties": {
            "OBJECTID": 537,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 538,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22046591267178, 25.799789065612913]
        },
        "properties": {
            "OBJECTID": 538,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 539,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22053117197606, 25.800230413302984]
        },
        "properties": {
            "OBJECTID": 539,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 540,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22055780899575, 25.800305993227084]
        },
        "properties": {
            "OBJECTID": 540,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 541,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22058593079612, 25.800371954901834]
        },
        "properties": {
            "OBJECTID": 541,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 542,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22080092002648, 25.800827014555125]
        },
        "properties": {
            "OBJECTID": 542,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 543,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22098577477374, 25.801748870913684]
        },
        "properties": {
            "OBJECTID": 543,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 544,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22095349540757, 25.802026902619502]
        },
        "properties": {
            "OBJECTID": 544,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 545,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22093543342356, 25.802205392864664]
        },
        "properties": {
            "OBJECTID": 545,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 546,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22092537270782, 25.802375755936453]
        },
        "properties": {
            "OBJECTID": 546,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 547,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22092882520519, 25.802556117670747]
        },
        "properties": {
            "OBJECTID": 547,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 548,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22100815800013, 25.803035565139282]
        },
        "properties": {
            "OBJECTID": 548,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 549,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2210463791871, 25.803130117161231]
        },
        "properties": {
            "OBJECTID": 549,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 550,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22138676268901, 25.803595985766208]
        },
        "properties": {
            "OBJECTID": 550,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 551,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22225888114252, 25.80456065435078]
        },
        "properties": {
            "OBJECTID": 551,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 552,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22244189227956, 25.806009147404268]
        },
        "properties": {
            "OBJECTID": 552,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 553,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22244955360406, 25.806347098240053]
        },
        "properties": {
            "OBJECTID": 553,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 554,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22245259690987, 25.806463656672463]
        },
        "properties": {
            "OBJECTID": 554,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 555,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2224669204121, 25.806838932970436]
        },
        "properties": {
            "OBJECTID": 555,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 556,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22267214390439, 25.808852147208881]
        },
        "properties": {
            "OBJECTID": 556,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 557,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2227352718154, 25.809225365858026]
        },
        "properties": {
            "OBJECTID": 557,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 558,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22275642566859, 25.809280677761137]
        },
        "properties": {
            "OBJECTID": 558,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 559,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2229191220199, 25.809362993607294]
        },
        "properties": {
            "OBJECTID": 559,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 560,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22345920807606, 25.80964348495894]
        },
        "properties": {
            "OBJECTID": 560,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 561,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22350300416036, 25.809678473982444]
        },
        "properties": {
            "OBJECTID": 561,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 562,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2236819116909, 25.809852291350239]
        },
        "properties": {
            "OBJECTID": 562,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 563,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22439645003618, 25.810578580237348]
        },
        "properties": {
            "OBJECTID": 563,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 564,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22653217262172, 25.812558864000437]
        },
        "properties": {
            "OBJECTID": 564,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 565,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22699485043205, 25.812919088345325]
        },
        "properties": {
            "OBJECTID": 565,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 566,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22751444543536, 25.813646503183634]
        },
        "properties": {
            "OBJECTID": 566,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 567,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22779466788967, 25.814973192059199]
        },
        "properties": {
            "OBJECTID": 567,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 568,
        "geometry": {
            "type": "Point",
            "coordinates": [100.227860295916, 25.815255707784729]
        },
        "properties": {
            "OBJECTID": 568,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 569,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22792026271003, 25.815486509794653]
        },
        "properties": {
            "OBJECTID": 569,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 570,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22795088822295, 25.815562804679757]
        },
        "properties": {
            "OBJECTID": 570,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 571,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22805830864411, 25.815775248828743]
        },
        "properties": {
            "OBJECTID": 571,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 572,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22823461713392, 25.816124787429032]
        },
        "properties": {
            "OBJECTID": 572,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 573,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22873016786303, 25.817448846687]
        },
        "properties": {
            "OBJECTID": 573,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 574,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22872918580333, 25.817518265355716]
        },
        "properties": {
            "OBJECTID": 574,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 575,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22872828827997, 25.817605752303677]
        },
        "properties": {
            "OBJECTID": 575,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 576,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22872200831409, 25.817855271004021]
        },
        "properties": {
            "OBJECTID": 576,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 577,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22867025233035, 25.819211839854574]
        },
        "properties": {
            "OBJECTID": 577,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 578,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22842748394129, 25.820430269241569]
        },
        "properties": {
            "OBJECTID": 578,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 579,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22835483041115, 25.820593522273271]
        },
        "properties": {
            "OBJECTID": 579,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 580,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22807029930493, 25.821246460655686]
        },
        "properties": {
            "OBJECTID": 580,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 581,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22794463883406, 25.821593418201758]
        },
        "properties": {
            "OBJECTID": 581,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 582,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2278430208388, 25.821952048947935]
        },
        "properties": {
            "OBJECTID": 582,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 583,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22772954618165, 25.822337661154165]
        },
        "properties": {
            "OBJECTID": 583,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 584,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22746666985097, 25.823067792844427]
        },
        "properties": {
            "OBJECTID": 584,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 585,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22726560572318, 25.823448217761097]
        },
        "properties": {
            "OBJECTID": 585,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 586,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22668159408039, 25.824214092106729]
        },
        "properties": {
            "OBJECTID": 586,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 587,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22657740312474, 25.824332015710013]
        },
        "properties": {
            "OBJECTID": 587,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 588,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22570549960921, 25.825079808286546]
        },
        "properties": {
            "OBJECTID": 588,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 589,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22527351655958, 25.825512073723303]
        },
        "properties": {
            "OBJECTID": 589,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 590,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22489436688159, 25.825946888737974]
        },
        "properties": {
            "OBJECTID": 590,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 591,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2244706396084, 25.826491487592136]
        },
        "properties": {
            "OBJECTID": 591,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 592,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22405883015097, 25.827266790430087]
        },
        "properties": {
            "OBJECTID": 592,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 593,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22398381679977, 25.827515390023279]
        },
        "properties": {
            "OBJECTID": 593,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 594,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22390763972584, 25.827772657282367]
        },
        "properties": {
            "OBJECTID": 594,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 595,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22302332466677, 25.829334589020107]
        },
        "properties": {
            "OBJECTID": 595,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 596,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22287613442614, 25.829531627782444]
        },
        "properties": {
            "OBJECTID": 596,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 597,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22271057373405, 25.829734522030606]
        },
        "properties": {
            "OBJECTID": 597,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 598,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22230125539869, 25.830121710748415]
        },
        "properties": {
            "OBJECTID": 598,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 599,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22190544218245, 25.830491974225254]
        },
        "properties": {
            "OBJECTID": 599,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 600,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22179782211185, 25.830592968990402]
        },
        "properties": {
            "OBJECTID": 600,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 601,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22150505591367, 25.830854047576281]
        },
        "properties": {
            "OBJECTID": 601,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 602,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22123576381949, 25.831054654848515]
        },
        "properties": {
            "OBJECTID": 602,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 603,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22092601662246, 25.831238504153703]
        },
        "properties": {
            "OBJECTID": 603,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 604,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22077749898244, 25.831323824634865]
        },
        "properties": {
            "OBJECTID": 604,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 605,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22068247931304, 25.831371328623959]
        },
        "properties": {
            "OBJECTID": 605,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 606,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21998142090592, 25.831639340982861]
        },
        "properties": {
            "OBJECTID": 606,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 607,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21989025932805, 25.831662968870944]
        },
        "properties": {
            "OBJECTID": 607,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 608,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21979467758234, 25.831682674815625]
        },
        "properties": {
            "OBJECTID": 608,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 609,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21940333309743, 25.831767675138167]
        },
        "properties": {
            "OBJECTID": 609,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 610,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21865738682902, 25.832125223100604]
        },
        "properties": {
            "OBJECTID": 610,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 611,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21860391763676, 25.832221681685326]
        },
        "properties": {
            "OBJECTID": 611,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 612,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21794244828544, 25.834947876643241]
        },
        "properties": {
            "OBJECTID": 612,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 613,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21751033123684, 25.835932921167455]
        },
        "properties": {
            "OBJECTID": 613,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 614,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21694634210007, 25.836471921741861]
        },
        "properties": {
            "OBJECTID": 614,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 615,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21643095412531, 25.83659641579203]
        },
        "properties": {
            "OBJECTID": 615,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 616,
        "geometry": {
            "type": "Point",
            "coordinates": [100.215448523031, 25.836522397990336]
        },
        "properties": {
            "OBJECTID": 616,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 617,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21462028969466, 25.836468373017055]
        },
        "properties": {
            "OBJECTID": 617,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 618,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21300430510468, 25.836507559176539]
        },
        "properties": {
            "OBJECTID": 618,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 619,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21235822405288, 25.836756887220645]
        },
        "properties": {
            "OBJECTID": 619,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 620,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21201342398041, 25.837016581749879]
        },
        "properties": {
            "OBJECTID": 620,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 621,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21164222700725, 25.837322541002493]
        },
        "properties": {
            "OBJECTID": 621,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 622,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21172258323071, 25.838103174122239]
        },
        "properties": {
            "OBJECTID": 622,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 623,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21209756275238, 25.838628457337506]
        },
        "properties": {
            "OBJECTID": 623,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 624,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21207515974089, 25.839297489987246]
        },
        "properties": {
            "OBJECTID": 624,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 625,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21133696203123, 25.839610243617926]
        },
        "properties": {
            "OBJECTID": 625,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 626,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21116214371801, 25.839659449124326]
        },
        "properties": {
            "OBJECTID": 626,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 627,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2110266176843, 25.83974540003021]
        },
        "properties": {
            "OBJECTID": 627,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 628,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21101923065305, 25.83978429121214]
        },
        "properties": {
            "OBJECTID": 628,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 629,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21102641443753, 25.83980508443716]
        },
        "properties": {
            "OBJECTID": 629,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 630,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21104258424793, 25.839824463028606]
        },
        "properties": {
            "OBJECTID": 630,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 631,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21106214899902, 25.839839391774547]
        },
        "properties": {
            "OBJECTID": 631,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 632,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21140354873484, 25.839878272164583]
        },
        "properties": {
            "OBJECTID": 632,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 633,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2123422017313, 25.839827898438784]
        },
        "properties": {
            "OBJECTID": 633,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 634,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21333974683193, 25.839789635883051]
        },
        "properties": {
            "OBJECTID": 634,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 635,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21335957508438, 25.839789179926754]
        },
        "properties": {
            "OBJECTID": 635,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 636,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21380955356813, 25.839809826562316]
        },
        "properties": {
            "OBJECTID": 636,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 637,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21410388638532, 25.839832737690756]
        },
        "properties": {
            "OBJECTID": 637,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 638,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21470007484862, 25.839879112131371]
        },
        "properties": {
            "OBJECTID": 638,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 639,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21516665211936, 25.839924037764206]
        },
        "properties": {
            "OBJECTID": 639,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 640,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21522596420692, 25.839936592299921]
        },
        "properties": {
            "OBJECTID": 640,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 641,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21535050861911, 25.839976243408955]
        },
        "properties": {
            "OBJECTID": 641,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 642,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2155075671206, 25.840045536172624]
        },
        "properties": {
            "OBJECTID": 642,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 643,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21564597907872, 25.840124134221469]
        },
        "properties": {
            "OBJECTID": 643,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 644,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21583289507208, 25.840267972688821]
        },
        "properties": {
            "OBJECTID": 644,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 645,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21597755911711, 25.840427533104219]
        },
        "properties": {
            "OBJECTID": 645,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 646,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21615036474759, 25.840659543802929]
        },
        "properties": {
            "OBJECTID": 646,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 647,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21696861381054, 25.842903031249932]
        },
        "properties": {
            "OBJECTID": 647,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 648,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21702912109714, 25.843046249185079]
        },
        "properties": {
            "OBJECTID": 648,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 649,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21709126155253, 25.843336425335792]
        },
        "properties": {
            "OBJECTID": 649,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 650,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21701602786743, 25.843828285247241]
        },
        "properties": {
            "OBJECTID": 650,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 651,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2169516427042, 25.843975926947508]
        },
        "properties": {
            "OBJECTID": 651,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 652,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2165501340815, 25.844862543371732]
        },
        "properties": {
            "OBJECTID": 652,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 653,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21632769486729, 25.845327446104818]
        },
        "properties": {
            "OBJECTID": 653,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 654,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21649099826101, 25.845444086375551]
        },
        "properties": {
            "OBJECTID": 654,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 655,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2175738593462, 25.845414408748013]
        },
        "properties": {
            "OBJECTID": 655,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 656,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21764425557797, 25.845420146422668]
        },
        "properties": {
            "OBJECTID": 656,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 657,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21782997187574, 25.845461520632739]
        },
        "properties": {
            "OBJECTID": 657,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 658,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21822120214682, 25.845590174946665]
        },
        "properties": {
            "OBJECTID": 658,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 659,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21835741076598, 25.84574065760529]
        },
        "properties": {
            "OBJECTID": 659,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 660,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21838862443553, 25.84587543090646]
        },
        "properties": {
            "OBJECTID": 660,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 661,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21842399657027, 25.846366741332076]
        },
        "properties": {
            "OBJECTID": 661,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 662,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21839301762373, 25.846521349180989]
        },
        "properties": {
            "OBJECTID": 662,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 663,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21830050256619, 25.846692321993089]
        },
        "properties": {
            "OBJECTID": 663,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 664,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21813748875417, 25.84687259469456]
        },
        "properties": {
            "OBJECTID": 664,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 665,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21745218017401, 25.847354650197019]
        },
        "properties": {
            "OBJECTID": 665,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 666,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21682126888652, 25.847783467632951]
        },
        "properties": {
            "OBJECTID": 666,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 667,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21664014272847, 25.847922501921971]
        },
        "properties": {
            "OBJECTID": 667,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 668,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21658276688123, 25.847991757813418]
        },
        "properties": {
            "OBJECTID": 668,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 669,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21657330691266, 25.848020653030744]
        },
        "properties": {
            "OBJECTID": 669,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 670,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21657185360823, 25.848029339582411]
        },
        "properties": {
            "OBJECTID": 670,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 671,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21657157301973, 25.848038124160155]
        },
        "properties": {
            "OBJECTID": 671,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 672,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21667426390621, 25.84817305034602]
        },
        "properties": {
            "OBJECTID": 672,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 673,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21936074750056, 25.850178644628784]
        },
        "properties": {
            "OBJECTID": 673,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 674,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2200924718901, 25.851086353752294]
        },
        "properties": {
            "OBJECTID": 674,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 675,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22088683316088, 25.85207841029046]
        },
        "properties": {
            "OBJECTID": 675,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 676,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22108052914314, 25.852367021620807]
        },
        "properties": {
            "OBJECTID": 676,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 677,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22127361718373, 25.853525503998867]
        },
        "properties": {
            "OBJECTID": 677,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 678,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22126153479206, 25.853551751612088]
        },
        "properties": {
            "OBJECTID": 678,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 679,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22060177505193, 25.854260860750173]
        },
        "properties": {
            "OBJECTID": 679,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 680,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22051620815654, 25.854345658725208]
        },
        "properties": {
            "OBJECTID": 680,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 681,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22030124500651, 25.854559797196941]
        },
        "properties": {
            "OBJECTID": 681,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 682,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22019318606675, 25.854648850763908]
        },
        "properties": {
            "OBJECTID": 682,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 683,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22013059595031, 25.854683443186389]
        },
        "properties": {
            "OBJECTID": 683,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 684,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2199662367525, 25.854759558207093]
        },
        "properties": {
            "OBJECTID": 684,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 685,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21988297931608, 25.854778226334133]
        },
        "properties": {
            "OBJECTID": 685,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 686,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21970335232788, 25.85477891071821]
        },
        "properties": {
            "OBJECTID": 686,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 687,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21931067055107, 25.854649600798496]
        },
        "properties": {
            "OBJECTID": 687,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 688,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21918081564218, 25.854589447844774]
        },
        "properties": {
            "OBJECTID": 688,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 689,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21867854877604, 25.854359635988487]
        },
        "properties": {
            "OBJECTID": 689,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 690,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21795567101748, 25.854047114382865]
        },
        "properties": {
            "OBJECTID": 690,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 691,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21789617996478, 25.854026983958192]
        },
        "properties": {
            "OBJECTID": 691,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 692,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21777638667095, 25.85399113428349]
        },
        "properties": {
            "OBJECTID": 692,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 693,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21749604730479, 25.853920391812665]
        },
        "properties": {
            "OBJECTID": 693,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 694,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21717712342559, 25.853866007110582]
        },
        "properties": {
            "OBJECTID": 694,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 695,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21658737141013, 25.853820594045203]
        },
        "properties": {
            "OBJECTID": 695,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 696,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21653599583959, 25.85385230414056]
        },
        "properties": {
            "OBJECTID": 696,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 697,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21648849994438, 25.853890915633315]
        },
        "properties": {
            "OBJECTID": 697,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 698,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21641913433564, 25.853964429814653]
        },
        "properties": {
            "OBJECTID": 698,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 699,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21581592666377, 25.854746712290989]
        },
        "properties": {
            "OBJECTID": 699,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 700,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21566142763282, 25.854941177193609]
        },
        "properties": {
            "OBJECTID": 700,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 701,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21550831535654, 25.855114157292576]
        },
        "properties": {
            "OBJECTID": 701,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 702,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21514905508491, 25.855463597866787]
        },
        "properties": {
            "OBJECTID": 702,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 703,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21463716727152, 25.855868139902782]
        },
        "properties": {
            "OBJECTID": 703,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 704,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21431902490315, 25.856702228724942]
        },
        "properties": {
            "OBJECTID": 704,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 705,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21430688495485, 25.857325359977438]
        },
        "properties": {
            "OBJECTID": 705,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 706,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21429560655707, 25.857412403559636]
        },
        "properties": {
            "OBJECTID": 706,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 707,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21428810441256, 25.857455238268699]
        },
        "properties": {
            "OBJECTID": 707,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 708,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21403879255627, 25.858068554320369]
        },
        "properties": {
            "OBJECTID": 708,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 709,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2140016028917, 25.8581550735978]
        },
        "properties": {
            "OBJECTID": 709,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 710,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21399966305404, 25.858160986640257]
        },
        "properties": {
            "OBJECTID": 710,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 711,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21394592586387, 25.858364719056567]
        },
        "properties": {
            "OBJECTID": 711,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 712,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21394563358416, 25.858366219125742]
        },
        "properties": {
            "OBJECTID": 712,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 713,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21394520460757, 25.858368961158703]
        },
        "properties": {
            "OBJECTID": 713,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 714,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21393395318944, 25.858454590107272]
        },
        "properties": {
            "OBJECTID": 714,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 715,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21392942870023, 25.858516662214242]
        },
        "properties": {
            "OBJECTID": 715,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 716,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21392939992194, 25.858517151445426]
        },
        "properties": {
            "OBJECTID": 716,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 717,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21393361414505, 25.858723225696338]
        },
        "properties": {
            "OBJECTID": 717,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 718,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21395735534776, 25.858935023232789]
        },
        "properties": {
            "OBJECTID": 718,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 719,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21397658195377, 25.859070257886117]
        },
        "properties": {
            "OBJECTID": 719,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 720,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21398672360846, 25.859111177039267]
        },
        "properties": {
            "OBJECTID": 720,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 721,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21402064333807, 25.859201889855399]
        },
        "properties": {
            "OBJECTID": 721,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 722,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21467535518354, 25.860187000030066]
        },
        "properties": {
            "OBJECTID": 722,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 723,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21467714663305, 25.860189625151122]
        },
        "properties": {
            "OBJECTID": 723,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 724,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21475721957006, 25.860297792009533]
        },
        "properties": {
            "OBJECTID": 724,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 725,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21481240466881, 25.860376230878387]
        },
        "properties": {
            "OBJECTID": 725,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 726,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21482635405306, 25.860395785736955]
        },
        "properties": {
            "OBJECTID": 726,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 727,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21487340118659, 25.860459348020527]
        },
        "properties": {
            "OBJECTID": 727,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 728,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21557859827374, 25.86103176830153]
        },
        "properties": {
            "OBJECTID": 728,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 729,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21631905508036, 25.861679115598747]
        },
        "properties": {
            "OBJECTID": 729,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 730,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21636626139389, 25.861732571301161]
        },
        "properties": {
            "OBJECTID": 730,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 731,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21646683527626, 25.861859467440524]
        },
        "properties": {
            "OBJECTID": 731,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 732,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21656331364608, 25.862025625282456]
        },
        "properties": {
            "OBJECTID": 732,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 733,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21659964176013, 25.862111777636471]
        },
        "properties": {
            "OBJECTID": 733,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 734,
        "geometry": {
            "type": "Point",
            "coordinates": [100.216713665903, 25.862845827672857]
        },
        "properties": {
            "OBJECTID": 734,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 735,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21672248735297, 25.863952805877318]
        },
        "properties": {
            "OBJECTID": 735,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 736,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21666839223258, 25.864160099608966]
        },
        "properties": {
            "OBJECTID": 736,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 737,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21658652155077, 25.864375823585419]
        },
        "properties": {
            "OBJECTID": 737,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 738,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2165356657884, 25.864506610191938]
        },
        "properties": {
            "OBJECTID": 738,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 739,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21645576642055, 25.864712140353049]
        },
        "properties": {
            "OBJECTID": 739,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 740,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21631200439555, 25.865083021664191]
        },
        "properties": {
            "OBJECTID": 740,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 741,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21619858099979, 25.865382904197702]
        },
        "properties": {
            "OBJECTID": 741,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 742,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21549437586481, 25.86693071927732]
        },
        "properties": {
            "OBJECTID": 742,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 743,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21533116869853, 25.867081035561398]
        },
        "properties": {
            "OBJECTID": 743,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 744,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21508048897346, 25.867279433199315]
        },
        "properties": {
            "OBJECTID": 744,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 745,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2130759963602, 25.867484067635644]
        },
        "properties": {
            "OBJECTID": 745,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 746,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21303343414576, 25.867733830052259]
        },
        "properties": {
            "OBJECTID": 746,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 747,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21285103095039, 25.868729965015916]
        },
        "properties": {
            "OBJECTID": 747,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 748,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21276405841462, 25.868948983209066]
        },
        "properties": {
            "OBJECTID": 748,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 749,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21267345801374, 25.869120176355125]
        },
        "properties": {
            "OBJECTID": 749,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 750,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21237915127688, 25.869535982399725]
        },
        "properties": {
            "OBJECTID": 750,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 751,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21196942914588, 25.871484920292914]
        },
        "properties": {
            "OBJECTID": 751,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 752,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21206140640885, 25.872380165712116]
        },
        "properties": {
            "OBJECTID": 752,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 753,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21198898310513, 25.872865104441075]
        },
        "properties": {
            "OBJECTID": 753,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 754,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21155766015795, 25.873696247983503]
        },
        "properties": {
            "OBJECTID": 754,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 755,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21134530324321, 25.873929275815499]
        },
        "properties": {
            "OBJECTID": 755,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 756,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21098860783803, 25.874469305214291]
        },
        "properties": {
            "OBJECTID": 756,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 757,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21075638130202, 25.874952311300149]
        },
        "properties": {
            "OBJECTID": 757,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 758,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21066264856245, 25.875121489065521]
        },
        "properties": {
            "OBJECTID": 758,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 759,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20969519567677, 25.875982368691439]
        },
        "properties": {
            "OBJECTID": 759,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 760,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20956169941297, 25.876099512582471]
        },
        "properties": {
            "OBJECTID": 760,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 761,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20940637030782, 25.876279080215454]
        },
        "properties": {
            "OBJECTID": 761,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 762,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20929559811344, 25.876454080191706]
        },
        "properties": {
            "OBJECTID": 762,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 763,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2091067863492, 25.877001501118457]
        },
        "properties": {
            "OBJECTID": 763,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 764,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20906117903024, 25.877178715225568]
        },
        "properties": {
            "OBJECTID": 764,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 765,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20893365966072, 25.877672588719065]
        },
        "properties": {
            "OBJECTID": 765,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 766,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20890115816201, 25.877798937171292]
        },
        "properties": {
            "OBJECTID": 766,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 767,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20885753744545, 25.877968873964392]
        },
        "properties": {
            "OBJECTID": 767,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 768,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20882079564325, 25.87811702378167]
        },
        "properties": {
            "OBJECTID": 768,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 769,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2088080936187, 25.878218839627777]
        },
        "properties": {
            "OBJECTID": 769,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 770,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2088048461668, 25.878300684229259]
        },
        "properties": {
            "OBJECTID": 770,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 771,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20880663042169, 25.878309270956152]
        },
        "properties": {
            "OBJECTID": 771,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 772,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20882833016339, 25.878382259034083]
        },
        "properties": {
            "OBJECTID": 772,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 773,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20901167315026, 25.878687880141626]
        },
        "properties": {
            "OBJECTID": 773,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 774,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2096773684159, 25.879073599367189]
        },
        "properties": {
            "OBJECTID": 774,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 775,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20976248205295, 25.879120849747494]
        },
        "properties": {
            "OBJECTID": 775,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 776,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20996598334352, 25.879241674563673]
        },
        "properties": {
            "OBJECTID": 776,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 777,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21034643973644, 25.879498094661415]
        },
        "properties": {
            "OBJECTID": 777,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 778,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21072459206636, 25.87977052628878]
        },
        "properties": {
            "OBJECTID": 778,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 779,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21089274370524, 25.879905153000436]
        },
        "properties": {
            "OBJECTID": 779,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 780,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21102724631044, 25.880023210602758]
        },
        "properties": {
            "OBJECTID": 780,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 781,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21124776097543, 25.880215999169081]
        },
        "properties": {
            "OBJECTID": 781,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 782,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21195315591348, 25.880653555317394]
        },
        "properties": {
            "OBJECTID": 782,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 783,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21224813983986, 25.880805493978414]
        },
        "properties": {
            "OBJECTID": 783,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 784,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21275345990432, 25.881066204741614]
        },
        "properties": {
            "OBJECTID": 784,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 785,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2129828265962, 25.881199552617318]
        },
        "properties": {
            "OBJECTID": 785,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 786,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21306762367198, 25.881263492616142]
        },
        "properties": {
            "OBJECTID": 786,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 787,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21314776405814, 25.8813379088175]
        },
        "properties": {
            "OBJECTID": 787,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 788,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21329864511642, 25.881515022200574]
        },
        "properties": {
            "OBJECTID": 788,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 789,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21336105267051, 25.881617524229398]
        },
        "properties": {
            "OBJECTID": 789,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 790,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21340596301485, 25.881703822273607]
        },
        "properties": {
            "OBJECTID": 790,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 791,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21351894664213, 25.882262142084244]
        },
        "properties": {
            "OBJECTID": 791,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 792,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21354937970017, 25.882547228072156]
        },
        "properties": {
            "OBJECTID": 792,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 793,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21359643402826, 25.882964135786267]
        },
        "properties": {
            "OBJECTID": 793,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 794,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21361146709557, 25.883194831676178]
        },
        "properties": {
            "OBJECTID": 794,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 795,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21359926779206, 25.883305335872592]
        },
        "properties": {
            "OBJECTID": 795,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 796,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21350431107516, 25.883510726638747]
        },
        "properties": {
            "OBJECTID": 796,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 797,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21331845088588, 25.883700440423013]
        },
        "properties": {
            "OBJECTID": 797,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 798,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21305720502608, 25.88392300014641]
        },
        "properties": {
            "OBJECTID": 798,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 799,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21305392519855, 25.883962330197448]
        },
        "properties": {
            "OBJECTID": 799,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 800,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21310157577716, 25.884167719164964]
        },
        "properties": {
            "OBJECTID": 800,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 801,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21313419418777, 25.884240057932402]
        },
        "properties": {
            "OBJECTID": 801,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 802,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2135070998728, 25.884472244898234]
        },
        "properties": {
            "OBJECTID": 802,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 803,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21356439118375, 25.884494103819861]
        },
        "properties": {
            "OBJECTID": 803,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 804,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21435906361995, 25.884808386298062]
        },
        "properties": {
            "OBJECTID": 804,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 805,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21486940459943, 25.885253728776604]
        },
        "properties": {
            "OBJECTID": 805,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 806,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21503085538973, 25.885575347924032]
        },
        "properties": {
            "OBJECTID": 806,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 807,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21519457427024, 25.885937913204202]
        },
        "properties": {
            "OBJECTID": 807,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 808,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21530707136429, 25.88618208183766]
        },
        "properties": {
            "OBJECTID": 808,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 809,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21561179135114, 25.886862797580079]
        },
        "properties": {
            "OBJECTID": 809,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 810,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21562694762559, 25.886927118891492]
        },
        "properties": {
            "OBJECTID": 810,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 811,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21566022523928, 25.887646209604952]
        },
        "properties": {
            "OBJECTID": 811,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 812,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21566039431178, 25.888000874340946]
        },
        "properties": {
            "OBJECTID": 812,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 813,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21566374878302, 25.88805969809664]
        },
        "properties": {
            "OBJECTID": 813,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 814,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21567128600111, 25.888131194199332]
        },
        "properties": {
            "OBJECTID": 814,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 815,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2156992288364, 25.888205039331126]
        },
        "properties": {
            "OBJECTID": 815,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 816,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21577225288723, 25.888292591929599]
        },
        "properties": {
            "OBJECTID": 816,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 817,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2158207713116, 25.888320309934386]
        },
        "properties": {
            "OBJECTID": 817,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 818,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21605048604118, 25.888382420712219]
        },
        "properties": {
            "OBJECTID": 818,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 819,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21608645802365, 25.888388587363465]
        },
        "properties": {
            "OBJECTID": 819,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 820,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21613598818539, 25.888396915085593]
        },
        "properties": {
            "OBJECTID": 820,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 821,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21693150958157, 25.888609498629535]
        },
        "properties": {
            "OBJECTID": 821,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 822,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21706921737052, 25.888679823814869]
        },
        "properties": {
            "OBJECTID": 822,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 823,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21720671471815, 25.88879739128663]
        },
        "properties": {
            "OBJECTID": 823,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 824,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21725762533919, 25.888853019751139]
        },
        "properties": {
            "OBJECTID": 824,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 825,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21784872633771, 25.889466632579058]
        },
        "properties": {
            "OBJECTID": 825,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 826,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21787772137981, 25.88958879288856]
        },
        "properties": {
            "OBJECTID": 826,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 827,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21793378241819, 25.8901739439724]
        },
        "properties": {
            "OBJECTID": 827,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 828,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21793510711956, 25.890277623213819]
        },
        "properties": {
            "OBJECTID": 828,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 829,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21793981237249, 25.890430577209429]
        },
        "properties": {
            "OBJECTID": 829,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 830,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21794616788139, 25.890519335798786]
        },
        "properties": {
            "OBJECTID": 830,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 831,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2179830490785, 25.890822172605226]
        },
        "properties": {
            "OBJECTID": 831,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 832,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21789396673324, 25.891562099711166]
        },
        "properties": {
            "OBJECTID": 832,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 833,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21775956395277, 25.891887782894912]
        },
        "properties": {
            "OBJECTID": 833,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 834,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21771280370297, 25.89198752490438]
        },
        "properties": {
            "OBJECTID": 834,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 835,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21762053685836, 25.892190051329862]
        },
        "properties": {
            "OBJECTID": 835,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 836,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21758747328334, 25.892263062790164]
        },
        "properties": {
            "OBJECTID": 836,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 837,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21747273058213, 25.892512866575601]
        },
        "properties": {
            "OBJECTID": 837,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 838,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21738688489688, 25.892698410203536]
        },
        "properties": {
            "OBJECTID": 838,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 839,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21735873431817, 25.892759622558572]
        },
        "properties": {
            "OBJECTID": 839,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 840,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21734165709188, 25.892794418227879]
        },
        "properties": {
            "OBJECTID": 840,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 841,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21649267639594, 25.894054220026305]
        },
        "properties": {
            "OBJECTID": 841,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 842,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21625335420902, 25.894269032989541]
        },
        "properties": {
            "OBJECTID": 842,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 843,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21620992055148, 25.894302447300163]
        },
        "properties": {
            "OBJECTID": 843,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 844,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21616476379285, 25.894333940658896]
        },
        "properties": {
            "OBJECTID": 844,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 845,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21611313641216, 25.894366771309478]
        },
        "properties": {
            "OBJECTID": 845,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 846,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21574482896074, 25.894532043319202]
        },
        "properties": {
            "OBJECTID": 846,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 847,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2152732199832, 25.894699158039771]
        },
        "properties": {
            "OBJECTID": 847,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 848,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21514880057674, 25.894815415199275]
        },
        "properties": {
            "OBJECTID": 848,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 849,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21509450400822, 25.894884714258183]
        },
        "properties": {
            "OBJECTID": 849,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 850,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21505578369749, 25.89493725804806]
        },
        "properties": {
            "OBJECTID": 850,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 851,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21499740690484, 25.895014579958968]
        },
        "properties": {
            "OBJECTID": 851,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 852,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21442562873983, 25.89590111274623]
        },
        "properties": {
            "OBJECTID": 852,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 853,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21413091730807, 25.896351922203507]
        },
        "properties": {
            "OBJECTID": 853,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 854,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21397201789438, 25.896705283821859]
        },
        "properties": {
            "OBJECTID": 854,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 855,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21395013109372, 25.896824127431614]
        },
        "properties": {
            "OBJECTID": 855,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 856,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21394248056112, 25.896918136263025]
        },
        "properties": {
            "OBJECTID": 856,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 857,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21394206237636, 25.896933939150017]
        },
        "properties": {
            "OBJECTID": 857,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 858,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21393115629786, 25.897685754394104]
        },
        "properties": {
            "OBJECTID": 858,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 859,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21389464921873, 25.897823954112255]
        },
        "properties": {
            "OBJECTID": 859,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 860,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2138533577463, 25.897923597196325]
        },
        "properties": {
            "OBJECTID": 860,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 861,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21382674410899, 25.897973210095643]
        },
        "properties": {
            "OBJECTID": 861,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 862,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21367797735684, 25.89817392888375]
        },
        "properties": {
            "OBJECTID": 862,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 863,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21363506620543, 25.898217217750471]
        },
        "properties": {
            "OBJECTID": 863,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 864,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21356141982375, 25.898285583313054]
        },
        "properties": {
            "OBJECTID": 864,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 865,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21343330600234, 25.898391268042644]
        },
        "properties": {
            "OBJECTID": 865,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 866,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21333943027054, 25.898467312916182]
        },
        "properties": {
            "OBJECTID": 866,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 867,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21333564232611, 25.898470381403058]
        },
        "properties": {
            "OBJECTID": 867,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 868,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21284990589851, 25.898857911863217]
        },
        "properties": {
            "OBJECTID": 868,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 869,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21241524916445, 25.899182067998083]
        },
        "properties": {
            "OBJECTID": 869,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 870,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21229544777674, 25.899263983645994]
        },
        "properties": {
            "OBJECTID": 870,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 871,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21191706881774, 25.899499031355276]
        },
        "properties": {
            "OBJECTID": 871,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 872,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21174029717702, 25.899600733886814]
        },
        "properties": {
            "OBJECTID": 872,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 873,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21157366539239, 25.899690240712118]
        },
        "properties": {
            "OBJECTID": 873,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 874,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21141140521223, 25.899771810120967]
        },
        "properties": {
            "OBJECTID": 874,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 875,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21089088660517, 25.900010300435042]
        },
        "properties": {
            "OBJECTID": 875,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 876,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2105876172248, 25.900160577148995]
        },
        "properties": {
            "OBJECTID": 876,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 877,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21048579958006, 25.900234716359137]
        },
        "properties": {
            "OBJECTID": 877,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 878,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21043418299121, 25.900277328036339]
        },
        "properties": {
            "OBJECTID": 878,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 879,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21028354115253, 25.900562765659174]
        },
        "properties": {
            "OBJECTID": 879,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 880,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21002931899795, 25.901690708859178]
        },
        "properties": {
            "OBJECTID": 880,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 881,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21000800596477, 25.901720442244653]
        },
        "properties": {
            "OBJECTID": 881,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 882,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20997630126533, 25.901791748590369]
        },
        "properties": {
            "OBJECTID": 882,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 883,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20845742007396, 25.902818687132765]
        },
        "properties": {
            "OBJECTID": 883,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 884,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20805005866333, 25.902988331646213]
        },
        "properties": {
            "OBJECTID": 884,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 885,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20711654978902, 25.903359198568182]
        },
        "properties": {
            "OBJECTID": 885,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 886,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20711646525274, 25.903359231843126]
        },
        "properties": {
            "OBJECTID": 886,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 887,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20455731396891, 25.904170198196141]
        },
        "properties": {
            "OBJECTID": 887,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 888,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20317003327045, 25.904464155996038]
        },
        "properties": {
            "OBJECTID": 888,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 889,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20293864670123, 25.904503190170146]
        },
        "properties": {
            "OBJECTID": 889,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 890,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20281737222399, 25.904514107040484]
        },
        "properties": {
            "OBJECTID": 890,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 891,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2025691791244, 25.90451393886724]
        },
        "properties": {
            "OBJECTID": 891,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 892,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20234816083899, 25.904502885299962]
        },
        "properties": {
            "OBJECTID": 892,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 893,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20142166397869, 25.904467880088646]
        },
        "properties": {
            "OBJECTID": 893,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 894,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20128438966293, 25.904483864638678]
        },
        "properties": {
            "OBJECTID": 894,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 895,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20113046709741, 25.904510632959386]
        },
        "properties": {
            "OBJECTID": 895,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 896,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20092776800215, 25.904548269586996]
        },
        "properties": {
            "OBJECTID": 896,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 897,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20063980418365, 25.904598138793119]
        },
        "properties": {
            "OBJECTID": 897,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 898,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20038442100429, 25.90463759744722]
        },
        "properties": {
            "OBJECTID": 898,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 899,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20026775015663, 25.904664164319797]
        },
        "properties": {
            "OBJECTID": 899,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 900,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20006556187622, 25.904738408750632]
        },
        "properties": {
            "OBJECTID": 900,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 901,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19928593329922, 25.905063028935672]
        },
        "properties": {
            "OBJECTID": 901,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 902,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19867715522662, 25.905200978642313]
        },
        "properties": {
            "OBJECTID": 902,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 903,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19862879688151, 25.905221182811374]
        },
        "properties": {
            "OBJECTID": 903,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 904,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19812853280558, 25.905510761812252]
        },
        "properties": {
            "OBJECTID": 904,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 905,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19711845565229, 25.906135331080691]
        },
        "properties": {
            "OBJECTID": 905,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 906,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19677082901063, 25.906517751593015]
        },
        "properties": {
            "OBJECTID": 906,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 907,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19667999118872, 25.906577233652456]
        },
        "properties": {
            "OBJECTID": 907,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 908,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19653365170672, 25.90666349392518]
        },
        "properties": {
            "OBJECTID": 908,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 909,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19625896457961, 25.90689027326755]
        },
        "properties": {
            "OBJECTID": 909,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 910,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19624406461196, 25.906922868295794]
        },
        "properties": {
            "OBJECTID": 910,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 911,
        "geometry": {
            "type": "Point",
            "coordinates": [100.196161621062, 25.907259351437972]
        },
        "properties": {
            "OBJECTID": 911,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 912,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19602948277446, 25.907513970193634]
        },
        "properties": {
            "OBJECTID": 912,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 913,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19608192224297, 25.908467498876007]
        },
        "properties": {
            "OBJECTID": 913,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 914,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19610016319206, 25.909318987781091]
        },
        "properties": {
            "OBJECTID": 914,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 915,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19592168823539, 25.909700467602534]
        },
        "properties": {
            "OBJECTID": 915,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 916,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19577600166122, 25.909834606881645]
        },
        "properties": {
            "OBJECTID": 916,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 917,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19466279475222, 25.910610559929466]
        },
        "properties": {
            "OBJECTID": 917,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 918,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19384557901031, 25.911267464322066]
        },
        "properties": {
            "OBJECTID": 918,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 919,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19383507402949, 25.9122610766874]
        },
        "properties": {
            "OBJECTID": 919,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 920,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19395927580001, 25.913451720620458]
        },
        "properties": {
            "OBJECTID": 920,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 921,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19399762199271, 25.913922486033982]
        },
        "properties": {
            "OBJECTID": 921,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 922,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19401050657967, 25.914057689211063]
        },
        "properties": {
            "OBJECTID": 922,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 923,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1940240251887, 25.914132178257489]
        },
        "properties": {
            "OBJECTID": 923,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 924,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19404305484318, 25.914253075019474]
        },
        "properties": {
            "OBJECTID": 924,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 925,
        "geometry": {
            "type": "Point",
            "coordinates": [100.194059960299, 25.914308627940898]
        },
        "properties": {
            "OBJECTID": 925,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 926,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19566525194989, 25.914804595056069]
        },
        "properties": {
            "OBJECTID": 926,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 927,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19575419130297, 25.914893116224391]
        },
        "properties": {
            "OBJECTID": 927,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 928,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19639885412386, 25.915945101785098]
        },
        "properties": {
            "OBJECTID": 928,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 929,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19654238502312, 25.916252065580409]
        },
        "properties": {
            "OBJECTID": 929,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 930,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19656642300208, 25.916353601737399]
        },
        "properties": {
            "OBJECTID": 930,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 931,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19613962544344, 25.917150541364435]
        },
        "properties": {
            "OBJECTID": 931,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 932,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19592696725579, 25.917310575722524]
        },
        "properties": {
            "OBJECTID": 932,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 933,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19529932770189, 25.917776232087476]
        },
        "properties": {
            "OBJECTID": 933,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 934,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19485440520668, 25.918186110700503]
        },
        "properties": {
            "OBJECTID": 934,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 935,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19406087221154, 25.919668855333441]
        },
        "properties": {
            "OBJECTID": 935,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 936,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19377071314796, 25.920218589316505]
        },
        "properties": {
            "OBJECTID": 936,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 937,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19350376198906, 25.920741497422227]
        },
        "properties": {
            "OBJECTID": 937,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 938,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19328909201806, 25.921121860285666]
        },
        "properties": {
            "OBJECTID": 938,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 939,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19309712543208, 25.921396994375868]
        },
        "properties": {
            "OBJECTID": 939,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 940,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19283355302611, 25.92165858737394]
        },
        "properties": {
            "OBJECTID": 940,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 941,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19178786361908, 25.922341469583216]
        },
        "properties": {
            "OBJECTID": 941,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 942,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18989534829177, 25.922972104778864]
        },
        "properties": {
            "OBJECTID": 942,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 943,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18813082987396, 25.92371799438996]
        },
        "properties": {
            "OBJECTID": 943,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 944,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18789260036328, 25.923818838968316]
        },
        "properties": {
            "OBJECTID": 944,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 945,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18739992656543, 25.924171437062228]
        },
        "properties": {
            "OBJECTID": 945,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 946,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18695530804109, 25.924889760653969]
        },
        "properties": {
            "OBJECTID": 946,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 947,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18694267616365, 25.924922520258178]
        },
        "properties": {
            "OBJECTID": 947,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 948,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18692060590132, 25.924983263167121]
        },
        "properties": {
            "OBJECTID": 948,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 949,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18689435289212, 25.925076405951415]
        },
        "properties": {
            "OBJECTID": 949,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 950,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18688663401099, 25.925111891400718]
        },
        "properties": {
            "OBJECTID": 950,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 951,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18687966876178, 25.925146821968326]
        },
        "properties": {
            "OBJECTID": 951,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 952,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18687364510271, 25.925178709230124]
        },
        "properties": {
            "OBJECTID": 952,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 953,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18686419142938, 25.925227672818892]
        },
        "properties": {
            "OBJECTID": 953,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 954,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18685745190993, 25.925372078758471]
        },
        "properties": {
            "OBJECTID": 954,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 955,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18685681878719, 25.92539440083101]
        },
        "properties": {
            "OBJECTID": 955,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 956,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18685696177943, 25.925401006351422]
        },
        "properties": {
            "OBJECTID": 956,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 957,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18686057525542, 25.925634237430188]
        },
        "properties": {
            "OBJECTID": 957,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 958,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18686547026527, 25.925830735699947]
        },
        "properties": {
            "OBJECTID": 958,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 959,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18686821589552, 25.926005512644338]
        },
        "properties": {
            "OBJECTID": 959,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 960,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18687293283966, 25.926104107118874]
        },
        "properties": {
            "OBJECTID": 960,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 961,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18712893025599, 25.928421228357081]
        },
        "properties": {
            "OBJECTID": 961,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 962,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18711919419553, 25.928552275766947]
        },
        "properties": {
            "OBJECTID": 962,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 963,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18708917572496, 25.928715068345753]
        },
        "properties": {
            "OBJECTID": 963,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 964,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18705936229981, 25.928822157816398]
        },
        "properties": {
            "OBJECTID": 964,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 965,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18649623111622, 25.930285242370076]
        },
        "properties": {
            "OBJECTID": 965,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 966,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18583976919007, 25.931408322935795]
        },
        "properties": {
            "OBJECTID": 966,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 967,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18548590215272, 25.9317654446196]
        },
        "properties": {
            "OBJECTID": 967,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 968,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18486450928975, 25.932231368982571]
        },
        "properties": {
            "OBJECTID": 968,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 969,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18436449972194, 25.932654898404905]
        },
        "properties": {
            "OBJECTID": 969,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 970,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18387321357801, 25.933096954760686]
        },
        "properties": {
            "OBJECTID": 970,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 971,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1831828301236, 25.933485850391719]
        },
        "properties": {
            "OBJECTID": 971,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 972,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1827736583777, 25.93364032873825]
        },
        "properties": {
            "OBJECTID": 972,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 973,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18258508673244, 25.933708574691025]
        },
        "properties": {
            "OBJECTID": 973,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 974,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18219210907864, 25.933853494143477]
        },
        "properties": {
            "OBJECTID": 974,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 975,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18192544660212, 25.93395236381059]
        },
        "properties": {
            "OBJECTID": 975,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 976,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18173378758428, 25.934019426255588]
        },
        "properties": {
            "OBJECTID": 976,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 977,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18103880409757, 25.934200626158031]
        },
        "properties": {
            "OBJECTID": 977,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 978,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18029560705668, 25.9343760380238]
        },
        "properties": {
            "OBJECTID": 978,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 979,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18002568813506, 25.934444702161329]
        },
        "properties": {
            "OBJECTID": 979,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 980,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17966765723668, 25.934530580222145]
        },
        "properties": {
            "OBJECTID": 980,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 981,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17953079211242, 25.934562617670736]
        },
        "properties": {
            "OBJECTID": 981,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 982,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1775092123803, 25.934905065116709]
        },
        "properties": {
            "OBJECTID": 982,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 983,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17723954167155, 25.934940991233816]
        },
        "properties": {
            "OBJECTID": 983,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 984,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17717853166397, 25.934949326150559]
        },
        "properties": {
            "OBJECTID": 984,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 985,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1770961034025, 25.934960586561886]
        },
        "properties": {
            "OBJECTID": 985,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 986,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17647577803478, 25.935041247655533]
        },
        "properties": {
            "OBJECTID": 986,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 987,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1761361076953, 25.935087635585944]
        },
        "properties": {
            "OBJECTID": 987,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 988,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17565706222371, 25.935150481109872]
        },
        "properties": {
            "OBJECTID": 988,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 989,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17499757677683, 25.935204319923457]
        },
        "properties": {
            "OBJECTID": 989,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 990,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17495455770671, 25.935206123963496]
        },
        "properties": {
            "OBJECTID": 990,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 991,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17442574285383, 25.935228087206497]
        },
        "properties": {
            "OBJECTID": 991,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 992,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17414582616902, 25.935234976013362]
        },
        "properties": {
            "OBJECTID": 992,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 993,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17296493898078, 25.93521495530598]
        },
        "properties": {
            "OBJECTID": 993,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 994,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17278493697535, 25.935212279822906]
        },
        "properties": {
            "OBJECTID": 994,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 995,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17237017774158, 25.935201023008858]
        },
        "properties": {
            "OBJECTID": 995,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 996,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17218576725941, 25.935196675686086]
        },
        "properties": {
            "OBJECTID": 996,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 997,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17151981208974, 25.935123702896647]
        },
        "properties": {
            "OBJECTID": 997,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 998,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17137355174805, 25.935093944330106]
        },
        "properties": {
            "OBJECTID": 998,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 999,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17041094980556, 25.93464351708468]
        },
        "properties": {
            "OBJECTID": 999,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1000,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17033554434994, 25.934520558177269]
        },
        "properties": {
            "OBJECTID": 1000,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1001,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1701315250499, 25.933842078149439]
        },
        "properties": {
            "OBJECTID": 1001,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1002,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1700008247783, 25.933363336648711]
        },
        "properties": {
            "OBJECTID": 1002,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1003,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16977334936064, 25.932800799017684]
        },
        "properties": {
            "OBJECTID": 1003,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1004,
        "geometry": {
            "type": "Point",
            "coordinates": [100.169663210289, 25.932672792215612]
        },
        "properties": {
            "OBJECTID": 1004,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1005,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16949650386061, 25.932515634788786]
        },
        "properties": {
            "OBJECTID": 1005,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1006,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1692543038431, 25.932305758204222]
        },
        "properties": {
            "OBJECTID": 1006,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1007,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1690066701218, 25.932092303217246]
        },
        "properties": {
            "OBJECTID": 1007,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1008,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16845098262678, 25.931911340735837]
        },
        "properties": {
            "OBJECTID": 1008,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1009,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16789842836982, 25.932599186303435]
        },
        "properties": {
            "OBJECTID": 1009,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1010,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16753470206493, 25.932926018820751]
        },
        "properties": {
            "OBJECTID": 1010,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1011,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16684732414473, 25.93391506802601]
        },
        "properties": {
            "OBJECTID": 1011,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1012,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16678166374282, 25.935649431276261]
        },
        "properties": {
            "OBJECTID": 1012,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1013,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16651748339518, 25.93594917441493]
        },
        "properties": {
            "OBJECTID": 1013,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1014,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16556383690158, 25.936689329049329]
        },
        "properties": {
            "OBJECTID": 1014,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1015,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16506204937411, 25.937079884828847]
        },
        "properties": {
            "OBJECTID": 1015,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1016,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16494844431531, 25.937177894744082]
        },
        "properties": {
            "OBJECTID": 1016,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1017,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16500906941309, 25.938443748979864]
        },
        "properties": {
            "OBJECTID": 1017,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1018,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16512608739907, 25.938644190776813]
        },
        "properties": {
            "OBJECTID": 1018,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1019,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16528763261823, 25.93892614712405]
        },
        "properties": {
            "OBJECTID": 1019,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1020,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16547505762787, 25.939249777155567]
        },
        "properties": {
            "OBJECTID": 1020,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1021,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16555925575511, 25.939398870361629]
        },
        "properties": {
            "OBJECTID": 1021,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1022,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16585019902755, 25.939764016696074]
        },
        "properties": {
            "OBJECTID": 1022,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1023,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16804647656369, 25.940573435315855]
        },
        "properties": {
            "OBJECTID": 1023,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1024,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16823117572824, 25.940847066638867]
        },
        "properties": {
            "OBJECTID": 1024,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1025,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16846897356436, 25.941409510740357]
        },
        "properties": {
            "OBJECTID": 1025,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1026,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16845144937497, 25.941614959962465]
        },
        "properties": {
            "OBJECTID": 1026,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1027,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16836333200223, 25.941922148588333]
        },
        "properties": {
            "OBJECTID": 1027,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1028,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16821360027751, 25.942521614181203]
        },
        "properties": {
            "OBJECTID": 1028,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1029,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16804793256608, 25.943377252558207]
        },
        "properties": {
            "OBJECTID": 1029,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1030,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16713721521035, 25.945701403796818]
        },
        "properties": {
            "OBJECTID": 1030,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1031,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1667326974561, 25.946256933011114]
        },
        "properties": {
            "OBJECTID": 1031,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1032,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16656309431141, 25.946478106879169]
        },
        "properties": {
            "OBJECTID": 1032,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1033,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16585450138422, 25.947198710252337]
        },
        "properties": {
            "OBJECTID": 1033,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1034,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16568942093011, 25.94727805473849]
        },
        "properties": {
            "OBJECTID": 1034,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1035,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16483457485583, 25.948015442158976]
        },
        "properties": {
            "OBJECTID": 1035,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1036,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16444520258409, 25.948472467730255]
        },
        "properties": {
            "OBJECTID": 1036,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1037,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16254391886685, 25.949495043661386]
        },
        "properties": {
            "OBJECTID": 1037,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1038,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16244747107402, 25.949535551824283]
        },
        "properties": {
            "OBJECTID": 1038,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1039,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16132064483196, 25.949728371866911]
        },
        "properties": {
            "OBJECTID": 1039,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1040,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15989328134992, 25.950818053410501]
        },
        "properties": {
            "OBJECTID": 1040,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1041,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1596148152716, 25.952021839136592]
        },
        "properties": {
            "OBJECTID": 1041,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1042,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15956500092409, 25.952130943987925]
        },
        "properties": {
            "OBJECTID": 1042,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1043,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15939934670257, 25.952379754022502]
        },
        "properties": {
            "OBJECTID": 1043,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1044,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15934500606721, 25.952450734813624]
        },
        "properties": {
            "OBJECTID": 1044,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1045,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15916099038748, 25.952617509590425]
        },
        "properties": {
            "OBJECTID": 1045,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1046,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15906454079595, 25.952676816282121]
        },
        "properties": {
            "OBJECTID": 1046,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1047,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15869652562424, 25.952875710345779]
        },
        "properties": {
            "OBJECTID": 1047,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1048,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15711344722786, 25.953534357624562]
        },
        "properties": {
            "OBJECTID": 1048,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1049,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15608263710408, 25.953965391889369]
        },
        "properties": {
            "OBJECTID": 1049,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1050,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15529641110055, 25.955108064185879]
        },
        "properties": {
            "OBJECTID": 1050,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1051,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15511443868047, 25.955411638436431]
        },
        "properties": {
            "OBJECTID": 1051,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1052,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15506856156492, 25.955480299876001]
        },
        "properties": {
            "OBJECTID": 1052,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1053,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1550167282395, 25.955544325310427]
        },
        "properties": {
            "OBJECTID": 1053,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1054,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15469909668604, 25.955894017694789]
        },
        "properties": {
            "OBJECTID": 1054,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1055,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15392148708702, 25.956460524034014]
        },
        "properties": {
            "OBJECTID": 1055,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1056,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15427759973153, 25.956678016077603]
        },
        "properties": {
            "OBJECTID": 1056,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1057,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15437003474938, 25.956761377835392]
        },
        "properties": {
            "OBJECTID": 1057,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1058,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15468815103742, 25.957171037013779]
        },
        "properties": {
            "OBJECTID": 1058,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1059,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15493233765733, 25.958087391320078]
        },
        "properties": {
            "OBJECTID": 1059,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1060,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15509384510494, 25.95894862078228]
        },
        "properties": {
            "OBJECTID": 1060,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1061,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15576688063709, 25.960190460421472]
        },
        "properties": {
            "OBJECTID": 1061,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1062,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15614098511844, 25.961786311888829]
        },
        "properties": {
            "OBJECTID": 1062,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1063,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15614319115542, 25.96181104504376]
        },
        "properties": {
            "OBJECTID": 1063,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1064,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15613967300754, 25.962296178026236]
        },
        "properties": {
            "OBJECTID": 1064,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1065,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15605641017527, 25.9629272251114]
        },
        "properties": {
            "OBJECTID": 1065,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1066,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15605622401557, 25.962928518336525]
        },
        "properties": {
            "OBJECTID": 1066,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1067,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15603090720066, 25.963044313244495]
        },
        "properties": {
            "OBJECTID": 1067,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1068,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15514019166659, 25.96536514418608]
        },
        "properties": {
            "OBJECTID": 1068,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1069,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15510601293221, 25.965408058934827]
        },
        "properties": {
            "OBJECTID": 1069,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1070,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15506463512486, 25.965453750790005]
        },
        "properties": {
            "OBJECTID": 1070,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1071,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15479050737605, 25.965689455903714]
        },
        "properties": {
            "OBJECTID": 1071,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1072,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15477253442498, 25.965704092370004]
        },
        "properties": {
            "OBJECTID": 1072,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1073,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15461360893096, 25.965807301266011]
        },
        "properties": {
            "OBJECTID": 1073,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1074,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15409641331888, 25.966035914326028]
        },
        "properties": {
            "OBJECTID": 1074,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1075,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15247749064372, 25.966595787265817]
        },
        "properties": {
            "OBJECTID": 1075,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1076,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15242681204779, 25.966610049614133]
        },
        "properties": {
            "OBJECTID": 1076,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1077,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15177385567898, 25.966755851301571]
        },
        "properties": {
            "OBJECTID": 1077,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1078,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15150486036106, 25.966769599237693]
        },
        "properties": {
            "OBJECTID": 1078,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1079,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15112271594063, 25.966759650937206]
        },
        "properties": {
            "OBJECTID": 1079,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1080,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15000357710346, 25.966701022334348]
        },
        "properties": {
            "OBJECTID": 1080,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1081,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14874393178707, 25.966616701000021]
        },
        "properties": {
            "OBJECTID": 1081,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1082,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14870315922349, 25.966611117109437]
        },
        "properties": {
            "OBJECTID": 1082,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1083,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14855716687981, 25.966588053096245]
        },
        "properties": {
            "OBJECTID": 1083,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1084,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14824263528942, 25.966492674597305]
        },
        "properties": {
            "OBJECTID": 1084,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1085,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14818864538972, 25.966470812977661]
        },
        "properties": {
            "OBJECTID": 1085,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1086,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14792358370647, 25.966352261647614]
        },
        "properties": {
            "OBJECTID": 1086,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1087,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14767376103532, 25.966206885339545]
        },
        "properties": {
            "OBJECTID": 1087,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1088,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1476232173377, 25.966169478938355]
        },
        "properties": {
            "OBJECTID": 1088,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1089,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14729689203801, 25.965845272441413]
        },
        "properties": {
            "OBJECTID": 1089,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1090,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14705767327308, 25.965465934805081]
        },
        "properties": {
            "OBJECTID": 1090,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1091,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14684150503149, 25.964904652627695]
        },
        "properties": {
            "OBJECTID": 1091,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1092,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14410226900458, 25.964169759926506]
        },
        "properties": {
            "OBJECTID": 1092,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1093,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14249447903705, 25.963742345432934]
        },
        "properties": {
            "OBJECTID": 1093,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1094,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14247485043404, 25.963736546604366]
        },
        "properties": {
            "OBJECTID": 1094,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1095,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14170995095355, 25.963397557950998]
        },
        "properties": {
            "OBJECTID": 1095,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1096,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1416061646928, 25.96334287377465]
        },
        "properties": {
            "OBJECTID": 1096,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1097,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13809631129578, 25.960875938973459]
        },
        "properties": {
            "OBJECTID": 1097,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1098,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13790951491222, 25.960736541358358]
        },
        "properties": {
            "OBJECTID": 1098,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1099,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13781708798825, 25.960653168808733]
        },
        "properties": {
            "OBJECTID": 1099,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1100,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13749000186215, 25.960231840027461]
        },
        "properties": {
            "OBJECTID": 1100,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1101,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13729528155204, 25.959803270804343]
        },
        "properties": {
            "OBJECTID": 1101,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1102,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13721903882765, 25.959253786832846]
        },
        "properties": {
            "OBJECTID": 1102,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1103,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13727584360538, 25.958810453439753]
        },
        "properties": {
            "OBJECTID": 1103,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1104,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13729060687609, 25.958752931902325]
        },
        "properties": {
            "OBJECTID": 1104,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1105,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1373904810859, 25.958489586125552]
        },
        "properties": {
            "OBJECTID": 1105,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1106,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13773058489863, 25.957981429599329]
        },
        "properties": {
            "OBJECTID": 1106,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1107,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13950980363461, 25.956507334645835]
        },
        "properties": {
            "OBJECTID": 1107,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1108,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13704342461443, 25.952857179815055]
        },
        "properties": {
            "OBJECTID": 1108,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1109,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13697826603419, 25.952959463308616]
        },
        "properties": {
            "OBJECTID": 1109,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1110,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13694027777166, 25.953007496998453]
        },
        "properties": {
            "OBJECTID": 1110,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1111,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13689889636703, 25.953053182558392]
        },
        "properties": {
            "OBJECTID": 1111,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1112,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13640712368988, 25.953330305049576]
        },
        "properties": {
            "OBJECTID": 1112,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1113,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13601853382835, 25.953353707207896]
        },
        "properties": {
            "OBJECTID": 1113,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1114,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13473991821269, 25.952696461972209]
        },
        "properties": {
            "OBJECTID": 1114,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1115,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13471289898115, 25.952642701399611]
        },
        "properties": {
            "OBJECTID": 1115,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1116,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13468982687408, 25.95258745874429]
        },
        "properties": {
            "OBJECTID": 1116,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1117,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13465590264786, 25.952473478668139]
        },
        "properties": {
            "OBJECTID": 1117,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1118,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13426989833721, 25.95183635126665]
        },
        "properties": {
            "OBJECTID": 1118,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1119,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13425500106752, 25.951778857608247]
        },
        "properties": {
            "OBJECTID": 1119,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1120,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1342323516418, 25.951659818845599]
        },
        "properties": {
            "OBJECTID": 1120,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1121,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13422367678135, 25.951542045429107]
        },
        "properties": {
            "OBJECTID": 1121,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1122,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13432614463591, 25.951142842668105]
        },
        "properties": {
            "OBJECTID": 1122,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1123,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13452871243021, 25.950891757348757]
        },
        "properties": {
            "OBJECTID": 1123,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1124,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13477289095619, 25.950628330632981]
        },
        "properties": {
            "OBJECTID": 1124,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1125,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13483633093199, 25.950565143366703]
        },
        "properties": {
            "OBJECTID": 1125,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1126,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13542584372777, 25.950147598033254]
        },
        "properties": {
            "OBJECTID": 1126,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1127,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13710756875935, 25.949386907379903]
        },
        "properties": {
            "OBJECTID": 1127,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1128,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13712666406434, 25.949308269760877]
        },
        "properties": {
            "OBJECTID": 1128,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1129,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13745184542637, 25.948515852824414]
        },
        "properties": {
            "OBJECTID": 1129,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1130,
        "geometry": {
            "type": "Point",
            "coordinates": [100.138202422304, 25.9473079311162]
        },
        "properties": {
            "OBJECTID": 1130,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1131,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13897195599105, 25.946128722062952]
        },
        "properties": {
            "OBJECTID": 1131,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1132,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13905667122845, 25.945998275400143]
        },
        "properties": {
            "OBJECTID": 1132,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1133,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13974773816761, 25.944926725987671]
        },
        "properties": {
            "OBJECTID": 1133,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1134,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14011360665768, 25.944325527401247]
        },
        "properties": {
            "OBJECTID": 1134,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1135,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14000698033794, 25.942617461226916]
        },
        "properties": {
            "OBJECTID": 1135,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1136,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13999498068387, 25.942583293284429]
        },
        "properties": {
            "OBJECTID": 1136,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1137,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13794115216251, 25.941242620850346]
        },
        "properties": {
            "OBJECTID": 1137,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1138,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13789151408218, 25.941210391846141]
        },
        "properties": {
            "OBJECTID": 1138,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1139,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13745594993219, 25.940873871831798]
        },
        "properties": {
            "OBJECTID": 1139,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1140,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13684967467282, 25.940253764999284]
        },
        "properties": {
            "OBJECTID": 1140,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1141,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13617983263322, 25.939146009780586]
        },
        "properties": {
            "OBJECTID": 1141,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1142,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13596031981297, 25.938739435276773]
        },
        "properties": {
            "OBJECTID": 1142,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1143,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13522365724606, 25.937391327248008]
        },
        "properties": {
            "OBJECTID": 1143,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1144,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13521959680702, 25.937383987880764]
        },
        "properties": {
            "OBJECTID": 1144,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1145,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13521860395548, 25.93738216675365]
        },
        "properties": {
            "OBJECTID": 1145,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1146,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13352939656448, 25.935086387326976]
        },
        "properties": {
            "OBJECTID": 1146,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1147,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13348658703643, 25.935036822091718]
        },
        "properties": {
            "OBJECTID": 1147,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1148,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13325860530045, 25.934751894384476]
        },
        "properties": {
            "OBJECTID": 1148,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1149,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13319321739311, 25.934649734098002]
        },
        "properties": {
            "OBJECTID": 1149,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1150,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13314100275511, 25.934535004886641]
        },
        "properties": {
            "OBJECTID": 1150,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1151,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13298577437405, 25.934108780196084]
        },
        "properties": {
            "OBJECTID": 1151,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1152,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13297087980231, 25.93405128833632]
        },
        "properties": {
            "OBJECTID": 1152,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1153,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13295370634853, 25.933934278444269]
        },
        "properties": {
            "OBJECTID": 1153,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1154,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13295151470072, 25.933869711617945]
        },
        "properties": {
            "OBJECTID": 1154,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1155,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13300750739063, 25.932358061875107]
        },
        "properties": {
            "OBJECTID": 1155,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1156,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13304867295795, 25.931771683216652]
        },
        "properties": {
            "OBJECTID": 1156,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1157,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13309903139526, 25.931281513131353]
        },
        "properties": {
            "OBJECTID": 1157,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1158,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13342422085117, 25.927726217890722]
        },
        "properties": {
            "OBJECTID": 1158,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1159,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13308561081237, 25.927713160633914]
        },
        "properties": {
            "OBJECTID": 1159,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1160,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13304762704644, 25.928045060830925]
        },
        "properties": {
            "OBJECTID": 1160,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1161,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13302271762439, 25.928382518838248]
        },
        "properties": {
            "OBJECTID": 1161,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1162,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1329374502032, 25.928806697571076]
        },
        "properties": {
            "OBJECTID": 1162,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1163,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13292268603317, 25.928864219108448]
        },
        "properties": {
            "OBJECTID": 1163,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1164,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13280384781939, 25.929253496951389]
        },
        "properties": {
            "OBJECTID": 1164,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1165,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13266639184059, 25.929599672986569]
        },
        "properties": {
            "OBJECTID": 1165,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1166,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13212909278349, 25.930954194980188]
        },
        "properties": {
            "OBJECTID": 1166,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1167,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13190721564553, 25.931411612655836]
        },
        "properties": {
            "OBJECTID": 1167,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1168,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13157855480654, 25.931888847792152]
        },
        "properties": {
            "OBJECTID": 1168,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1169,
        "geometry": {
            "type": "Point",
            "coordinates": [100.130789829587, 25.932714403846717]
        },
        "properties": {
            "OBJECTID": 1169,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1170,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12970366079537, 25.933726693332289]
        },
        "properties": {
            "OBJECTID": 1170,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1171,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1286243943004, 25.934865144211074]
        },
        "properties": {
            "OBJECTID": 1171,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1172,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12804396915391, 25.936642690208032]
        },
        "properties": {
            "OBJECTID": 1172,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1173,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12753643675723, 25.937615628958156]
        },
        "properties": {
            "OBJECTID": 1173,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1174,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12680977465146, 25.938240621807324]
        },
        "properties": {
            "OBJECTID": 1174,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1175,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12652017496617, 25.938381554565126]
        },
        "properties": {
            "OBJECTID": 1175,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1176,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12650819509719, 25.938385168940442]
        },
        "properties": {
            "OBJECTID": 1176,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1177,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12585452017004, 25.938571504871106]
        },
        "properties": {
            "OBJECTID": 1177,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1178,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12579009543663, 25.938581180676977]
        },
        "properties": {
            "OBJECTID": 1178,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1179,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12440994276471, 25.938626003787078]
        },
        "properties": {
            "OBJECTID": 1179,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1180,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12183352000102, 25.938627631559996]
        },
        "properties": {
            "OBJECTID": 1180,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1181,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12115180960836, 25.93874045780592]
        },
        "properties": {
            "OBJECTID": 1181,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1182,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12018940012075, 25.941177392122995]
        },
        "properties": {
            "OBJECTID": 1182,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1183,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12017935199555, 25.941214464875713]
        },
        "properties": {
            "OBJECTID": 1183,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1184,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12032309693342, 25.941309244426122]
        },
        "properties": {
            "OBJECTID": 1184,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1185,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12042918815655, 25.941402254110812]
        },
        "properties": {
            "OBJECTID": 1185,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1186,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12070138596027, 25.941563772350264]
        },
        "properties": {
            "OBJECTID": 1186,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1187,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12145952523696, 25.942077826630452]
        },
        "properties": {
            "OBJECTID": 1187,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1188,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12149761602222, 25.942125792871082]
        },
        "properties": {
            "OBJECTID": 1188,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1189,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12153215808269, 25.942175907592116]
        },
        "properties": {
            "OBJECTID": 1189,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1190,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12159000967125, 25.942281720924711]
        },
        "properties": {
            "OBJECTID": 1190,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1191,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12163209074845, 25.942393456293019]
        },
        "properties": {
            "OBJECTID": 1191,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1192,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12164698082358, 25.942450949951422]
        },
        "properties": {
            "OBJECTID": 1192,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1193,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12166634412654, 25.942626976054157]
        },
        "properties": {
            "OBJECTID": 1193,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1194,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12159073632347, 25.942972361585248]
        },
        "properties": {
            "OBJECTID": 1194,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1195,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12154636107579, 25.943178953845688]
        },
        "properties": {
            "OBJECTID": 1195,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1196,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12151950462152, 25.943299794849679]
        },
        "properties": {
            "OBJECTID": 1196,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1197,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12143117321011, 25.943592170742136]
        },
        "properties": {
            "OBJECTID": 1197,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1198,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12141378212038, 25.94364364343943]
        },
        "properties": {
            "OBJECTID": 1198,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1199,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12136393719601, 25.943752736599549]
        },
        "properties": {
            "OBJECTID": 1199,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1200,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12112716458705, 25.944032269273862]
        },
        "properties": {
            "OBJECTID": 1200,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1201,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12102366521003, 25.944104212339596]
        },
        "properties": {
            "OBJECTID": 1201,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1202,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12096500063433, 25.94413712662714]
        },
        "properties": {
            "OBJECTID": 1202,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1203,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12088186460642, 25.944180242824018]
        },
        "properties": {
            "OBJECTID": 1203,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1204,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12080532330708, 25.944231525764337]
        },
        "properties": {
            "OBJECTID": 1204,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1205,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1204111306692, 25.944430507961613]
        },
        "properties": {
            "OBJECTID": 1205,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1206,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12009036407909, 25.944478669355135]
        },
        "properties": {
            "OBJECTID": 1206,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1207,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11996008289157, 25.944471060191347]
        },
        "properties": {
            "OBJECTID": 1207,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1208,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11941437787129, 25.9442500059331]
        },
        "properties": {
            "OBJECTID": 1208,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1209,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11919875641752, 25.944062832733607]
        },
        "properties": {
            "OBJECTID": 1209,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1210,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1189368198784, 25.943907125913199]
        },
        "properties": {
            "OBJECTID": 1210,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1211,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1178649233276, 25.944017874725205]
        },
        "properties": {
            "OBJECTID": 1211,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1212,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11779992122945, 25.944012142446468]
        },
        "properties": {
            "OBJECTID": 1212,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1213,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11760933690141, 25.943972130709312]
        },
        "properties": {
            "OBJECTID": 1213,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1214,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11646566186084, 25.943396221958153]
        },
        "properties": {
            "OBJECTID": 1214,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1215,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11641795282634, 25.943355896357559]
        },
        "properties": {
            "OBJECTID": 1215,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1216,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11633178608309, 25.943267222304542]
        },
        "properties": {
            "OBJECTID": 1216,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1217,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11622831638374, 25.943117088582824]
        },
        "properties": {
            "OBJECTID": 1217,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1218,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11615684636143, 25.942041616327572]
        },
        "properties": {
            "OBJECTID": 1218,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1219,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11524482408942, 25.941218433583913]
        },
        "properties": {
            "OBJECTID": 1219,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1220,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11431137906698, 25.940351411588438]
        },
        "properties": {
            "OBJECTID": 1220,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1221,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11358142993907, 25.940132515703112]
        },
        "properties": {
            "OBJECTID": 1221,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1222,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11193031333335, 25.940129234976268]
        },
        "properties": {
            "OBJECTID": 1222,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1223,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11218349856966, 25.940708846236305]
        },
        "properties": {
            "OBJECTID": 1223,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1224,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11220284658418, 25.94088487413768]
        },
        "properties": {
            "OBJECTID": 1224,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1225,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11218384121139, 25.941060931716663]
        },
        "properties": {
            "OBJECTID": 1225,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1226,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11216906714878, 25.941118450556075]
        },
        "properties": {
            "OBJECTID": 1226,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1227,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11215016339941, 25.941174972946669]
        },
        "properties": {
            "OBJECTID": 1227,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1228,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11210031038109, 25.94128406340883]
        },
        "properties": {
            "OBJECTID": 1228,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1229,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11182423110347, 25.941609431829875]
        },
        "properties": {
            "OBJECTID": 1229,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1230,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11149709641393, 25.942101069608725]
        },
        "properties": {
            "OBJECTID": 1230,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1231,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11122465399467, 25.942301613928407]
        },
        "properties": {
            "OBJECTID": 1231,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1232,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11099108117349, 25.942405985647781]
        },
        "properties": {
            "OBJECTID": 1232,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1233,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11073558827684, 25.942452143351773]
        },
        "properties": {
            "OBJECTID": 1233,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1234,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11034945806108, 25.942406489268137]
        },
        "properties": {
            "OBJECTID": 1234,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1235,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11022880141809, 25.942361413448509]
        },
        "properties": {
            "OBJECTID": 1235,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1236,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10918142488293, 25.941617801820144]
        },
        "properties": {
            "OBJECTID": 1236,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1237,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10882557034387, 25.941329801129484]
        },
        "properties": {
            "OBJECTID": 1237,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1238,
        "geometry": {
            "type": "Point",
            "coordinates": [100.108770080375, 25.941298662103634]
        },
        "properties": {
            "OBJECTID": 1238,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1239,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10845008630253, 25.941064143195717]
        },
        "properties": {
            "OBJECTID": 1239,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1240,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10820961658061, 25.940783745373608]
        },
        "properties": {
            "OBJECTID": 1240,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1241,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10815355913951, 25.940705707602376]
        },
        "properties": {
            "OBJECTID": 1241,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1242,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10802161690418, 25.94031364186111]
        },
        "properties": {
            "OBJECTID": 1242,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1243,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10801942525632, 25.940254624751162]
        },
        "properties": {
            "OBJECTID": 1243,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1244,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1080384369244, 25.940078567172179]
        },
        "properties": {
            "OBJECTID": 1244,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1245,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10791889723936, 25.939895343795115]
        },
        "properties": {
            "OBJECTID": 1245,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1246,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10758495827923, 25.939232278147188]
        },
        "properties": {
            "OBJECTID": 1246,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1247,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10748340143789, 25.938621176226377]
        },
        "properties": {
            "OBJECTID": 1247,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1248,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1074939846597, 25.938562907352321]
        },
        "properties": {
            "OBJECTID": 1248,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1249,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10759310164036, 25.93802238422569]
        },
        "properties": {
            "OBJECTID": 1249,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1250,
        "geometry": {
            "type": "Point",
            "coordinates": [100.107633368785, 25.937721499847385]
        },
        "properties": {
            "OBJECTID": 1250,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1251,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10766157332301, 25.9376421382741]
        },
        "properties": {
            "OBJECTID": 1251,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1252,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10760402210798, 25.937605294848538]
        },
        "properties": {
            "OBJECTID": 1252,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1253,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10727369032861, 25.937413133109658]
        },
        "properties": {
            "OBJECTID": 1253,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1254,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10713078535753, 25.937292317286676]
        },
        "properties": {
            "OBJECTID": 1254,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1255,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10695885836469, 25.937042794989054]
        },
        "properties": {
            "OBJECTID": 1255,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1256,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10691679167667, 25.936931056023468]
        },
        "properties": {
            "OBJECTID": 1256,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1257,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10690190879609, 25.936873559667106]
        },
        "properties": {
            "OBJECTID": 1257,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1258,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10689121675631, 25.936815306980861]
        },
        "properties": {
            "OBJECTID": 1258,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1259,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10690158324155, 25.936521473287428]
        },
        "properties": {
            "OBJECTID": 1259,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1260,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1069582180474, 25.936352150731238]
        },
        "properties": {
            "OBJECTID": 1260,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1261,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10701585919463, 25.936246243869107]
        },
        "properties": {
            "OBJECTID": 1261,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1262,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10718728976167, 25.935978662285493]
        },
        "properties": {
            "OBJECTID": 1262,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1263,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10667149169603, 25.935483319299806]
        },
        "properties": {
            "OBJECTID": 1263,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1264,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10608550694064, 25.935379720997389]
        },
        "properties": {
            "OBJECTID": 1264,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1265,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10600673712128, 25.935472593085763]
        },
        "properties": {
            "OBJECTID": 1265,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1266,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10575557356094, 25.935659284248629]
        },
        "properties": {
            "OBJECTID": 1266,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1267,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10481520285936, 25.935687818837835]
        },
        "properties": {
            "OBJECTID": 1267,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1268,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10481288890378, 25.935686786416113]
        },
        "properties": {
            "OBJECTID": 1268,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1269,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10398648388917, 25.93535428906921]
        },
        "properties": {
            "OBJECTID": 1269,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1270,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10377069696017, 25.935330367102836]
        },
        "properties": {
            "OBJECTID": 1270,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1271,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10310148894263, 25.935392878978234]
        },
        "properties": {
            "OBJECTID": 1271,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1272,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10309441217743, 25.935422556605772]
        },
        "properties": {
            "OBJECTID": 1272,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1273,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10271482902618, 25.936786312838137]
        },
        "properties": {
            "OBJECTID": 1273,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1274,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10226307797871, 25.938090113068313]
        },
        "properties": {
            "OBJECTID": 1274,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1275,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10226145829972, 25.938104975264423]
        },
        "properties": {
            "OBJECTID": 1275,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1276,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10241878479911, 25.939189240191354]
        },
        "properties": {
            "OBJECTID": 1276,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1277,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10241243558551, 25.939248009088374]
        },
        "properties": {
            "OBJECTID": 1277,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1278,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10240184876636, 25.93930627796243]
        },
        "properties": {
            "OBJECTID": 1278,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1279,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10237779819687, 25.939393180351033]
        },
        "properties": {
            "OBJECTID": 1279,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1280,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10221660281405, 25.939858448208895]
        },
        "properties": {
            "OBJECTID": 1280,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1281,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10163841877807, 25.941119070188961]
        },
        "properties": {
            "OBJECTID": 1281,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1282,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10125291988641, 25.941937964065858]
        },
        "properties": {
            "OBJECTID": 1282,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1283,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10051237944282, 25.943484678375285]
        },
        "properties": {
            "OBJECTID": 1283,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1284,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10033516173837, 25.943792019885848]
        },
        "properties": {
            "OBJECTID": 1284,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1285,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10009610665008, 25.944140205905853]
        },
        "properties": {
            "OBJECTID": 1285,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1286,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09987927111155, 25.944306513934521]
        },
        "properties": {
            "OBJECTID": 1286,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1287,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09937820214236, 25.944502754098892]
        },
        "properties": {
            "OBJECTID": 1287,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1288,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09924793534401, 25.944510567408827]
        },
        "properties": {
            "OBJECTID": 1288,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1289,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09911765415649, 25.944502936661252]
        },
        "properties": {
            "OBJECTID": 1289,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1290,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09899256925172, 25.944480716212126]
        },
        "properties": {
            "OBJECTID": 1290,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1291,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09795263460649, 25.94410571330809]
        },
        "properties": {
            "OBJECTID": 1291,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1292,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09784899943185, 25.944033930321666]
        },
        "properties": {
            "OBJECTID": 1292,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1293,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09781623173376, 25.944006801372836]
        },
        "properties": {
            "OBJECTID": 1293,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1294,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09751026528659, 25.943692320144521]
        },
        "properties": {
            "OBJECTID": 1294,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1295,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09723591990183, 25.943104865896885]
        },
        "properties": {
            "OBJECTID": 1295,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1296,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09689154071208, 25.941732883565749]
        },
        "properties": {
            "OBJECTID": 1296,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1297,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09625836043506, 25.939342812920529]
        },
        "properties": {
            "OBJECTID": 1297,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1298,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09605173489973, 25.939064908019134]
        },
        "properties": {
            "OBJECTID": 1298,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1299,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09501731399791, 25.937961833771681]
        },
        "properties": {
            "OBJECTID": 1299,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1300,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09499177864774, 25.937933032083833]
        },
        "properties": {
            "OBJECTID": 1300,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1301,
        "geometry": {
            "type": "Point",
            "coordinates": [100.0949191700837, 25.937834935833678]
        },
        "properties": {
            "OBJECTID": 1301,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1302,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09488834222327, 25.937782880375664]
        },
        "properties": {
            "OBJECTID": 1302,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1303,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09460874929442, 25.936478811248207]
        },
        "properties": {
            "OBJECTID": 1303,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1304,
        "geometry": {
            "type": "Point",
            "coordinates": [100.0946663517708, 25.936229764691916]
        },
        "properties": {
            "OBJECTID": 1304,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1305,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09494759675425, 25.935530286393657]
        },
        "properties": {
            "OBJECTID": 1305,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1306,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09625999270457, 25.932842863907069]
        },
        "properties": {
            "OBJECTID": 1306,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1307,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09630650204349, 25.932785211068676]
        },
        "properties": {
            "OBJECTID": 1307,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1308,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09665752272656, 25.932386162091746]
        },
        "properties": {
            "OBJECTID": 1308,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1309,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09787346458876, 25.931111481909397]
        },
        "properties": {
            "OBJECTID": 1309,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1310,
        "geometry": {
            "type": "Point",
            "coordinates": [100.09825597503328, 25.930721245389236]
        },
        "properties": {
            "OBJECTID": 1310,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1311,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10027937319455, 25.928417926945826]
        },
        "properties": {
            "OBJECTID": 1311,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1312,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10065916139126, 25.928002255799527]
        },
        "properties": {
            "OBJECTID": 1312,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1313,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10105245110981, 25.927666410276686]
        },
        "properties": {
            "OBJECTID": 1313,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1314,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10340794372166, 25.925231165785704]
        },
        "properties": {
            "OBJECTID": 1314,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1315,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10514576216724, 25.922296559238873]
        },
        "properties": {
            "OBJECTID": 1315,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1316,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10532878679413, 25.922234112114609]
        },
        "properties": {
            "OBJECTID": 1316,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1317,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10539236886279, 25.922220654659554]
        },
        "properties": {
            "OBJECTID": 1317,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1318,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10545678730097, 25.922210988746201]
        },
        "properties": {
            "OBJECTID": 1318,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1319,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10552176601675, 25.922205153045411]
        },
        "properties": {
            "OBJECTID": 1319,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1320,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10595749564197, 25.921774136767056]
        },
        "properties": {
            "OBJECTID": 1320,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1321,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10606272081799, 25.921686532907245]
        },
        "properties": {
            "OBJECTID": 1321,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1322,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10672372432049, 25.921176795372844]
        },
        "properties": {
            "OBJECTID": 1322,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1323,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1071794188951, 25.920927559059635]
        },
        "properties": {
            "OBJECTID": 1323,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1324,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10722704069542, 25.92088715791607]
        },
        "properties": {
            "OBJECTID": 1324,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1325,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10810603266731, 25.920458008630249]
        },
        "properties": {
            "OBJECTID": 1325,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1326,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10817129287091, 25.920456027423768]
        },
        "properties": {
            "OBJECTID": 1326,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1327,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1088236592845, 25.920249284077272]
        },
        "properties": {
            "OBJECTID": 1327,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1328,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10888475202972, 25.920228436892899]
        },
        "properties": {
            "OBJECTID": 1328,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1329,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10907522034518, 25.920188122084198]
        },
        "properties": {
            "OBJECTID": 1329,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1330,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10909079210643, 25.92018367044011]
        },
        "properties": {
            "OBJECTID": 1330,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1331,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10913538858739, 25.920140537156101]
        },
        "properties": {
            "OBJECTID": 1331,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1332,
        "geometry": {
            "type": "Point",
            "coordinates": [100.10939947900283, 25.919969094897851]
        },
        "properties": {
            "OBJECTID": 1332,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1333,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11116055211784, 25.9195294604163]
        },
        "properties": {
            "OBJECTID": 1333,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1334,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11239255856981, 25.918848074678976]
        },
        "properties": {
            "OBJECTID": 1334,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1335,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11247192733765, 25.918754368019734]
        },
        "properties": {
            "OBJECTID": 1335,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1336,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11251652201992, 25.918711234735724]
        },
        "properties": {
            "OBJECTID": 1336,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1337,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11284005672331, 25.918515367790064]
        },
        "properties": {
            "OBJECTID": 1337,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1338,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11302719574854, 25.918463870811024]
        },
        "properties": {
            "OBJECTID": 1338,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1339,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11309161058944, 25.918454200401072]
        },
        "properties": {
            "OBJECTID": 1339,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1340,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11310278016924, 25.918392663390762]
        },
        "properties": {
            "OBJECTID": 1340,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1341,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11332136219119, 25.917785568848956]
        },
        "properties": {
            "OBJECTID": 1341,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1342,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11349278556366, 25.917535778553372]
        },
        "properties": {
            "OBJECTID": 1342,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1343,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1136347219649, 25.917413241427994]
        },
        "properties": {
            "OBJECTID": 1343,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1344,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11368597702625, 25.917242958395889]
        },
        "properties": {
            "OBJECTID": 1344,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1345,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11371412400769, 25.917121389840304]
        },
        "properties": {
            "OBJECTID": 1345,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1346,
        "geometry": {
            "type": "Point",
            "coordinates": [100.113786551808, 25.916895165379628]
        },
        "properties": {
            "OBJECTID": 1346,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1347,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11387618903495, 25.916667911195191]
        },
        "properties": {
            "OBJECTID": 1347,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1348,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11392494667899, 25.916516319672439]
        },
        "properties": {
            "OBJECTID": 1348,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1349,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11391633477109, 25.916467766174492]
        },
        "properties": {
            "OBJECTID": 1349,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1350,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11390768329301, 25.916349990060041]
        },
        "properties": {
            "OBJECTID": 1350,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1351,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11390976162625, 25.916290971151454]
        },
        "properties": {
            "OBJECTID": 1351,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1352,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11394145463447, 25.916116411843007]
        },
        "properties": {
            "OBJECTID": 1352,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1353,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11398330008927, 25.91600460632759]
        },
        "properties": {
            "OBJECTID": 1353,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1354,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11400642075978, 25.915939940575868]
        },
        "properties": {
            "OBJECTID": 1354,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1355,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1141537953614, 25.915390093278234]
        },
        "properties": {
            "OBJECTID": 1355,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1356,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11417674066411, 25.915334810153411]
        },
        "properties": {
            "OBJECTID": 1356,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1357,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11434816043931, 25.915085019857827]
        },
        "properties": {
            "OBJECTID": 1357,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1358,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11439275332299, 25.915041885674555]
        },
        "properties": {
            "OBJECTID": 1358,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1359,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11446274396053, 25.914791635825395]
        },
        "properties": {
            "OBJECTID": 1359,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1360,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11447463659528, 25.91475606763845]
        },
        "properties": {
            "OBJECTID": 1360,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1361,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11465131020987, 25.914384686304288]
        },
        "properties": {
            "OBJECTID": 1361,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1362,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11473723773338, 25.914271568677975]
        },
        "properties": {
            "OBJECTID": 1362,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1363,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11457275532848, 25.913911400091081]
        },
        "properties": {
            "OBJECTID": 1363,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1364,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11452332229356, 25.913695070870858]
        },
        "properties": {
            "OBJECTID": 1364,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1365,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11446407585646, 25.913587201688074]
        },
        "properties": {
            "OBJECTID": 1365,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1366,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1142322630086, 25.912902066677077]
        },
        "properties": {
            "OBJECTID": 1366,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1367,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11421325403848, 25.912845574863468]
        },
        "properties": {
            "OBJECTID": 1367,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1368,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11417902674077, 25.91261205240437]
        },
        "properties": {
            "OBJECTID": 1368,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1369,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11418110507401, 25.912553031697087]
        },
        "properties": {
            "OBJECTID": 1369,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1370,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11425285568481, 25.912027458900184]
        },
        "properties": {
            "OBJECTID": 1370,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1371,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11435443770722, 25.911215806364396]
        },
        "properties": {
            "OBJECTID": 1371,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1372,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11470056068237, 25.910204909029403]
        },
        "properties": {
            "OBJECTID": 1372,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1373,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11474515176741, 25.910161778443353]
        },
        "properties": {
            "OBJECTID": 1373,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1374,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11479276727243, 25.910121376400468]
        },
        "properties": {
            "OBJECTID": 1374,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1375,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11520913449402, 25.909730646152468]
        },
        "properties": {
            "OBJECTID": 1375,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1376,
        "geometry": {
            "type": "Point",
            "coordinates": [100.115255588075, 25.909629718836527]
        },
        "properties": {
            "OBJECTID": 1376,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1377,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11503533511268, 25.909068692965832]
        },
        "properties": {
            "OBJECTID": 1377,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1378,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11495452023496, 25.909047343959799]
        },
        "properties": {
            "OBJECTID": 1378,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1379,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1147762925919, 25.908974478189691]
        },
        "properties": {
            "OBJECTID": 1379,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1380,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11472081881078, 25.908943339163841]
        },
        "properties": {
            "OBJECTID": 1380,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1381,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11456951866842, 25.908831235972798]
        },
        "properties": {
            "OBJECTID": 1381,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1382,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11432990869838, 25.90848341148029]
        },
        "properties": {
            "OBJECTID": 1382,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1383,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11429816532814, 25.908397214160175]
        },
        "properties": {
            "OBJECTID": 1383,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1384,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11425119463701, 25.908193824385535]
        },
        "properties": {
            "OBJECTID": 1384,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1385,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11418688231885, 25.907485199982034]
        },
        "properties": {
            "OBJECTID": 1385,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1386,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11419373964941, 25.907317614016733]
        },
        "properties": {
            "OBJECTID": 1386,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1387,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11419580269421, 25.90730414217245]
        },
        "properties": {
            "OBJECTID": 1387,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1388,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11426101433443, 25.907002845904685]
        },
        "properties": {
            "OBJECTID": 1388,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1389,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11427206160641, 25.906974852707322]
        },
        "properties": {
            "OBJECTID": 1389,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1390,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11436354694018, 25.906754597047041]
        },
        "properties": {
            "OBJECTID": 1390,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1391,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11455193422444, 25.906420158063895]
        },
        "properties": {
            "OBJECTID": 1391,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1392,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11460272703431, 25.906356953710485]
        },
        "properties": {
            "OBJECTID": 1392,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1393,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11467456757731, 25.906271842771389]
        },
        "properties": {
            "OBJECTID": 1393,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1394,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11520108915897, 25.905860861589531]
        },
        "properties": {
            "OBJECTID": 1394,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1395,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11522650400002, 25.90584800668023]
        },
        "properties": {
            "OBJECTID": 1395,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1396,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11746294885609, 25.905148193834123]
        },
        "properties": {
            "OBJECTID": 1396,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1397,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11837443423281, 25.904557698079373]
        },
        "properties": {
            "OBJECTID": 1397,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1398,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1194300620478, 25.903835848245876]
        },
        "properties": {
            "OBJECTID": 1398,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1399,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11993032882168, 25.903491987964912]
        },
        "properties": {
            "OBJECTID": 1399,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1400,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12009486788389, 25.903105387403741]
        },
        "properties": {
            "OBJECTID": 1400,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1401,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11937555683653, 25.901835387293318]
        },
        "properties": {
            "OBJECTID": 1401,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1402,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11951324124317, 25.899671897240296]
        },
        "properties": {
            "OBJECTID": 1402,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1403,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11957590870128, 25.899570104776558]
        },
        "properties": {
            "OBJECTID": 1403,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1404,
        "geometry": {
            "type": "Point",
            "coordinates": [100.11973162631358, 25.899364317409379]
        },
        "properties": {
            "OBJECTID": 1404,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1405,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12016681184895, 25.898893206656226]
        },
        "properties": {
            "OBJECTID": 1405,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1406,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12017146314258, 25.898888208224264]
        },
        "properties": {
            "OBJECTID": 1406,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1407,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12057389177176, 25.898463513280547]
        },
        "properties": {
            "OBJECTID": 1407,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1408,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12227036298691, 25.897294948603019]
        },
        "properties": {
            "OBJECTID": 1408,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1409,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1222705131737, 25.897294845181023]
        },
        "properties": {
            "OBJECTID": 1409,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1410,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12258833808141, 25.897077562679442]
        },
        "properties": {
            "OBJECTID": 1410,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1411,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12287235567476, 25.896927783290607]
        },
        "properties": {
            "OBJECTID": 1411,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1412,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12302490407615, 25.896843594156621]
        },
        "properties": {
            "OBJECTID": 1412,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1413,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12296969739367, 25.896067420774898]
        },
        "properties": {
            "OBJECTID": 1413,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1414,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12290911906064, 25.895509897763645]
        },
        "properties": {
            "OBJECTID": 1414,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1415,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1229049551996, 25.89545344911744]
        },
        "properties": {
            "OBJECTID": 1415,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1416,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12289077199159, 25.895074690644492]
        },
        "properties": {
            "OBJECTID": 1416,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1417,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12289888927239, 25.894959008151773]
        },
        "properties": {
            "OBJECTID": 1417,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1418,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12297170198252, 25.894168073297976]
        },
        "properties": {
            "OBJECTID": 1418,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1419,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12300536180805, 25.89405403206797]
        },
        "properties": {
            "OBJECTID": 1419,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1420,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12304481506624, 25.893964615174923]
        },
        "properties": {
            "OBJECTID": 1420,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1421,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12307985714972, 25.893689001746111]
        },
        "properties": {
            "OBJECTID": 1421,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1422,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12308174122944, 25.893675136898082]
        },
        "properties": {
            "OBJECTID": 1422,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1423,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1231381206278, 25.893299554830662]
        },
        "properties": {
            "OBJECTID": 1423,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1424,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12324669487913, 25.892813339963709]
        },
        "properties": {
            "OBJECTID": 1424,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1425,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12343168992061, 25.892447109946204]
        },
        "properties": {
            "OBJECTID": 1425,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1426,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12361567142614, 25.89228039092734]
        },
        "properties": {
            "OBJECTID": 1426,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1427,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12372410358455, 25.8922147242302]
        },
        "properties": {
            "OBJECTID": 1427,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1428,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12378164940367, 25.892186841649448]
        },
        "properties": {
            "OBJECTID": 1428,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1429,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12397633643883, 25.892114972328159]
        },
        "properties": {
            "OBJECTID": 1429,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1430,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12401060870269, 25.892105035718885]
        },
        "properties": {
            "OBJECTID": 1430,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1431,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12404619217807, 25.891930442236173]
        },
        "properties": {
            "OBJECTID": 1431,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1432,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1241851572193, 25.891541446780423]
        },
        "properties": {
            "OBJECTID": 1432,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1433,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12436746868377, 25.891228735417883]
        },
        "properties": {
            "OBJECTID": 1433,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1434,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12437891975139, 25.891213680766782]
        },
        "properties": {
            "OBJECTID": 1434,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1435,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12472944490804, 25.890471848896766]
        },
        "properties": {
            "OBJECTID": 1435,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1436,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12481305667728, 25.890151717228036]
        },
        "properties": {
            "OBJECTID": 1436,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1437,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1248996002364, 25.890003153722603]
        },
        "properties": {
            "OBJECTID": 1437,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1438,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12508830318268, 25.889741647958715]
        },
        "properties": {
            "OBJECTID": 1438,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1439,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12584573559229, 25.888500109592428]
        },
        "properties": {
            "OBJECTID": 1439,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1440,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1267242743059, 25.886564874669261]
        },
        "properties": {
            "OBJECTID": 1440,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1441,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12647671522831, 25.88590488919931]
        },
        "properties": {
            "OBJECTID": 1441,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1442,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12647416744892, 25.885900881820305]
        },
        "properties": {
            "OBJECTID": 1442,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1443,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12610661992466, 25.885316100357784]
        },
        "properties": {
            "OBJECTID": 1443,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1444,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1260762111483, 25.885254808862385]
        },
        "properties": {
            "OBJECTID": 1444,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1445,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12589846284385, 25.884805823230181]
        },
        "properties": {
            "OBJECTID": 1445,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1446,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12586516814304, 25.884637230923545]
        },
        "properties": {
            "OBJECTID": 1446,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1447,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12558990275187, 25.882232264807215]
        },
        "properties": {
            "OBJECTID": 1447,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1448,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12564732266588, 25.88172149035455]
        },
        "properties": {
            "OBJECTID": 1448,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1449,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12570927336435, 25.88148578973744]
        },
        "properties": {
            "OBJECTID": 1449,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1450,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12573485997586, 25.881405259944813]
        },
        "properties": {
            "OBJECTID": 1450,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1451,
        "geometry": {
            "type": "Point",
            "coordinates": [100.125754159427, 25.881358052732025]
        },
        "properties": {
            "OBJECTID": 1451,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1452,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12592803704933, 25.880962392400534]
        },
        "properties": {
            "OBJECTID": 1452,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1453,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12599601140687, 25.880729035416721]
        },
        "properties": {
            "OBJECTID": 1453,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1454,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12610249743238, 25.880518225335891]
        },
        "properties": {
            "OBJECTID": 1454,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1455,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12650948832231, 25.879976164368543]
        },
        "properties": {
            "OBJECTID": 1455,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1456,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12660750813006, 25.879898256999013]
        },
        "properties": {
            "OBJECTID": 1456,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1457,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12670320319035, 25.879749968686156]
        },
        "properties": {
            "OBJECTID": 1457,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1458,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12675060195875, 25.879678766661812]
        },
        "properties": {
            "OBJECTID": 1458,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1459,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12708712377179, 25.879341161165712]
        },
        "properties": {
            "OBJECTID": 1459,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1460,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12719554423904, 25.879275491770557]
        },
        "properties": {
            "OBJECTID": 1460,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1461,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12725308106491, 25.879247608290541]
        },
        "properties": {
            "OBJECTID": 1461,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1462,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12740016788348, 25.879195015937285]
        },
        "properties": {
            "OBJECTID": 1462,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1463,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12749811304752, 25.878579944408955]
        },
        "properties": {
            "OBJECTID": 1463,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1464,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12769263011086, 25.877172355220068]
        },
        "properties": {
            "OBJECTID": 1464,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1465,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12769222811392, 25.877050648168904]
        },
        "properties": {
            "OBJECTID": 1465,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1466,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12833435484669, 25.875599643308931]
        },
        "properties": {
            "OBJECTID": 1466,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1467,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12833490523172, 25.875598779060454]
        },
        "properties": {
            "OBJECTID": 1467,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1468,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12855864576494, 25.87532937005443]
        },
        "properties": {
            "OBJECTID": 1468,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1469,
        "geometry": {
            "type": "Point",
            "coordinates": [100.12959818380921, 25.874409942764714]
        },
        "properties": {
            "OBJECTID": 1469,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1470,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1300990558268, 25.87398050569584]
        },
        "properties": {
            "OBJECTID": 1470,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1471,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13014947182074, 25.873943002167891]
        },
        "properties": {
            "OBJECTID": 1471,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1472,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13020249135195, 25.873908561730786]
        },
        "properties": {
            "OBJECTID": 1472,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1473,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13031541922129, 25.873849446594761]
        },
        "properties": {
            "OBJECTID": 1473,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1474,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13043591128832, 25.873804172024961]
        },
        "properties": {
            "OBJECTID": 1474,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1475,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13049835391598, 25.873786976987446]
        },
        "properties": {
            "OBJECTID": 1475,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1476,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13071379010938, 25.873743512752981]
        },
        "properties": {
            "OBJECTID": 1476,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1477,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13073014877739, 25.87374042538039]
        },
        "properties": {
            "OBJECTID": 1477,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1478,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13079453843727, 25.873730749574463]
        },
        "properties": {
            "OBJECTID": 1478,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1479,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13091851987383, 25.873722938962487]
        },
        "properties": {
            "OBJECTID": 1479,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1480,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1324411017755, 25.873720208620796]
        },
        "properties": {
            "OBJECTID": 1480,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1481,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1328543861211, 25.873722354403185]
        },
        "properties": {
            "OBJECTID": 1481,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1482,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13291286903376, 25.873724214201161]
        },
        "properties": {
            "OBJECTID": 1482,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1483,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13297783156173, 25.873729942882619]
        },
        "properties": {
            "OBJECTID": 1483,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1484,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1335221992901, 25.873833469239287]
        },
        "properties": {
            "OBJECTID": 1484,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1485,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13358167865158, 25.873857785108726]
        },
        "properties": {
            "OBJECTID": 1485,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1486,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13369473512398, 25.873916700595316]
        },
        "properties": {
            "OBJECTID": 1486,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1487,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1337983244332, 25.873988462897273]
        },
        "properties": {
            "OBJECTID": 1487,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1488,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13389067311613, 25.874071842641456]
        },
        "properties": {
            "OBJECTID": 1488,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1489,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13413885002797, 25.874666583196301]
        },
        "properties": {
            "OBJECTID": 1489,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1490,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13415971879607, 25.874749307334696]
        },
        "properties": {
            "OBJECTID": 1490,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1491,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13420117574373, 25.874794914653648]
        },
        "properties": {
            "OBJECTID": 1491,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1492,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13423924944186, 25.874842878196318]
        },
        "properties": {
            "OBJECTID": 1492,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1493,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13442999025193, 25.875367810675982]
        },
        "properties": {
            "OBJECTID": 1493,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1494,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13446081631366, 25.875419858939381]
        },
        "properties": {
            "OBJECTID": 1494,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1495,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13448781396153, 25.875473620411299]
        },
        "properties": {
            "OBJECTID": 1495,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1496,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13476211438012, 25.875249402338113]
        },
        "properties": {
            "OBJECTID": 1496,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1497,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13480347060374, 25.875203720375453]
        },
        "properties": {
            "OBJECTID": 1497,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1498,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13484804280301, 25.875160581695525]
        },
        "properties": {
            "OBJECTID": 1498,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1499,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13525686561189, 25.874470289971953]
        },
        "properties": {
            "OBJECTID": 1499,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1500,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13521697438392, 25.874436557301351]
        },
        "properties": {
            "OBJECTID": 1500,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1501,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13470878008621, 25.873707897801296]
        },
        "properties": {
            "OBJECTID": 1501,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1502,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13468178423699, 25.873654137228698]
        },
        "properties": {
            "OBJECTID": 1502,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1503,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13459236464598, 25.873381586890787]
        },
        "properties": {
            "OBJECTID": 1503,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1504,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13454769891723, 25.873338530049182]
        },
        "properties": {
            "OBJECTID": 1504,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1505,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13450624017088, 25.873292922730229]
        },
        "properties": {
            "OBJECTID": 1505,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1506,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1343527726624, 25.873033793874527]
        },
        "properties": {
            "OBJECTID": 1506,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1507,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13430818427537, 25.872861562011508]
        },
        "properties": {
            "OBJECTID": 1507,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1508,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13485533810342, 25.871499557658524]
        },
        "properties": {
            "OBJECTID": 1508,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1509,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13486415056013, 25.871489303588532]
        },
        "properties": {
            "OBJECTID": 1509,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1510,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13496089692791, 25.871280409063672]
        },
        "properties": {
            "OBJECTID": 1510,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1511,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13503327256751, 25.871182202196906]
        },
        "properties": {
            "OBJECTID": 1511,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1512,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13513424664825, 25.870883488782056]
        },
        "properties": {
            "OBJECTID": 1512,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1513,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13513100818955, 25.870570949189982]
        },
        "properties": {
            "OBJECTID": 1513,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1514,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13526206818995, 25.870178671208748]
        },
        "properties": {
            "OBJECTID": 1514,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1515,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13533444293023, 25.870080464341981]
        },
        "properties": {
            "OBJECTID": 1515,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1516,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13549078916867, 25.869876818260593]
        },
        "properties": {
            "OBJECTID": 1516,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1517,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1354329016072, 25.869780210388456]
        },
        "properties": {
            "OBJECTID": 1517,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1518,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13536794087781, 25.869774480807678]
        },
        "properties": {
            "OBJECTID": 1518,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1519,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13462613059153, 25.869565872267231]
        },
        "properties": {
            "OBJECTID": 1519,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1520,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13385277478579, 25.869080351676928]
        },
        "properties": {
            "OBJECTID": 1520,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1521,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13349454333854, 25.86842362535009]
        },
        "properties": {
            "OBJECTID": 1521,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1522,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13348384770148, 25.868365376261181]
        },
        "properties": {
            "OBJECTID": 1522,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1523,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13347518723015, 25.868247602844633]
        },
        "properties": {
            "OBJECTID": 1523,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1524,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13355073297998, 25.867902219112182]
        },
        "properties": {
            "OBJECTID": 1524,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1525,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1335776119173, 25.867848409976204]
        },
        "properties": {
            "OBJECTID": 1525,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1526,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13360832106724, 25.867796306854132]
        },
        "properties": {
            "OBJECTID": 1526,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1527,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13368069760617, 25.867698100886685]
        },
        "properties": {
            "OBJECTID": 1527,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1528,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13372205293047, 25.867652418924024]
        },
        "properties": {
            "OBJECTID": 1528,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1529,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13391764108633, 25.86749692794092]
        },
        "properties": {
            "OBJECTID": 1529,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1530,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13476678995545, 25.86712615274979]
        },
        "properties": {
            "OBJECTID": 1530,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1531,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13481135855739, 25.867083014969239]
        },
        "properties": {
            "OBJECTID": 1531,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1532,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13496237631267, 25.866970659968047]
        },
        "properties": {
            "OBJECTID": 1532,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1533,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13705392689729, 25.866535846751958]
        },
        "properties": {
            "OBJECTID": 1533,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1534,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13717450979584, 25.866580906383774]
        },
        "properties": {
            "OBJECTID": 1534,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1535,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13739114478557, 25.866711577877084]
        },
        "properties": {
            "OBJECTID": 1535,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1536,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13743634830888, 25.866719213121257]
        },
        "properties": {
            "OBJECTID": 1536,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1537,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13755277094367, 25.866712935853343]
        },
        "properties": {
            "OBJECTID": 1537,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1538,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13805159700365, 25.866833370363793]
        },
        "properties": {
            "OBJECTID": 1538,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1539,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13816014787261, 25.866898846404695]
        },
        "properties": {
            "OBJECTID": 1539,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1540,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13841703741639, 25.867163316334029]
        },
        "properties": {
            "OBJECTID": 1540,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1541,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13844786257886, 25.867215363698108]
        },
        "properties": {
            "OBJECTID": 1541,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1542,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13847486022667, 25.867269124270706]
        },
        "properties": {
            "OBJECTID": 1542,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1543,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13851721379842, 25.867304344420006]
        },
        "properties": {
            "OBJECTID": 1543,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1544,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13869539107947, 25.867377184109785]
        },
        "properties": {
            "OBJECTID": 1544,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1545,
        "geometry": {
            "type": "Point",
            "coordinates": [100.13901018796986, 25.867635890284134]
        },
        "properties": {
            "OBJECTID": 1545,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1546,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14069508670894, 25.868222411035504]
        },
        "properties": {
            "OBJECTID": 1546,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1547,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14079938738189, 25.868223553174516]
        },
        "properties": {
            "OBJECTID": 1547,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1548,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14099233332956, 25.8682521857898]
        },
        "properties": {
            "OBJECTID": 1548,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1549,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14111591906448, 25.868290010375745]
        },
        "properties": {
            "OBJECTID": 1549,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1550,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14176018618372, 25.868611596248229]
        },
        "properties": {
            "OBJECTID": 1550,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1551,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14221934944351, 25.868708092604436]
        },
        "properties": {
            "OBJECTID": 1551,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1552,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14245816711076, 25.868635145895325]
        },
        "properties": {
            "OBJECTID": 1552,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1553,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14252529610559, 25.868407501405102]
        },
        "properties": {
            "OBJECTID": 1553,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1554,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14255216964699, 25.868353689571165]
        },
        "properties": {
            "OBJECTID": 1554,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1555,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14258287699823, 25.868301584650453]
        },
        "properties": {
            "OBJECTID": 1555,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1556,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1426965971703, 25.868157691324427]
        },
        "properties": {
            "OBJECTID": 1556,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1557,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14289217363495, 25.86800218775079]
        },
        "properties": {
            "OBJECTID": 1557,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1558,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14306451161798, 25.86791863713546]
        },
        "properties": {
            "OBJECTID": 1558,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1559,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14312557198764, 25.86789778005857]
        },
        "properties": {
            "OBJECTID": 1559,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1560,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14317703838969, 25.867883249712236]
        },
        "properties": {
            "OBJECTID": 1560,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1561,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14315618760804, 25.867396368447714]
        },
        "properties": {
            "OBJECTID": 1561,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1562,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14316458188, 25.867278578843411]
        },
        "properties": {
            "OBJECTID": 1562,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1563,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14317514621609, 25.867220309969355]
        },
        "properties": {
            "OBJECTID": 1563,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1564,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14320877906198, 25.86710626514207]
        },
        "properties": {
            "OBJECTID": 1564,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1565,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14328928367354, 25.866945064363279]
        },
        "properties": {
            "OBJECTID": 1565,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1566,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14340300204697, 25.866801169238613]
        },
        "properties": {
            "OBJECTID": 1566,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1567,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14377091109873, 25.86656211325095]
        },
        "properties": {
            "OBJECTID": 1567,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1568,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14397591875371, 25.866507488429875]
        },
        "properties": {
            "OBJECTID": 1568,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1569,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14421220662808, 25.866468790602255]
        },
        "properties": {
            "OBJECTID": 1569,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1570,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14425862423616, 25.866462207564894]
        },
        "properties": {
            "OBJECTID": 1570,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1571,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14438879930373, 25.866454361879335]
        },
        "properties": {
            "OBJECTID": 1571,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1572,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14445403342694, 25.866456231569884]
        },
        "properties": {
            "OBJECTID": 1572,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1573,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14458339820533, 25.866471511950749]
        },
        "properties": {
            "OBJECTID": 1573,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1574,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14470945257921, 25.866501941411514]
        },
        "properties": {
            "OBJECTID": 1574,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1575,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14492037147801, 25.866593147955484]
        },
        "properties": {
            "OBJECTID": 1575,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1576,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14504668125937, 25.866677653650868]
        },
        "properties": {
            "OBJECTID": 1576,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1577,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14508461196533, 25.8667032006922]
        },
        "properties": {
            "OBJECTID": 1577,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1578,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14519190378337, 25.866638290324886]
        },
        "properties": {
            "OBJECTID": 1578,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1579,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14530884982361, 25.86658597136551]
        },
        "properties": {
            "OBJECTID": 1579,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1580,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14556027418735, 25.866524759909794]
        },
        "properties": {
            "OBJECTID": 1580,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1581,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14576687993764, 25.866519491681231]
        },
        "properties": {
            "OBJECTID": 1581,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1582,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14594058489013, 25.866531399604469]
        },
        "properties": {
            "OBJECTID": 1582,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1583,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14615545720864, 25.866309001759078]
        },
        "properties": {
            "OBJECTID": 1583,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1584,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14631426129421, 25.866205810849522]
        },
        "properties": {
            "OBJECTID": 1584,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1585,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14649226500609, 25.866132633014672]
        },
        "properties": {
            "OBJECTID": 1585,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1586,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14687803729163, 25.866086299043502]
        },
        "properties": {
            "OBJECTID": 1586,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1587,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14693618205922, 25.866091239918831]
        },
        "properties": {
            "OBJECTID": 1587,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1588,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14735854865813, 25.866007929422437]
        },
        "properties": {
            "OBJECTID": 1588,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1589,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14755310529165, 25.865990398038491]
        },
        "properties": {
            "OBJECTID": 1589,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1590,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14761833761622, 25.865992265930402]
        },
        "properties": {
            "OBJECTID": 1590,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1591,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1476606039538, 25.865972605851141]
        },
        "properties": {
            "OBJECTID": 1591,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1592,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14768747389786, 25.865918793117885]
        },
        "properties": {
            "OBJECTID": 1592,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1593,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14771817765183, 25.865866687297853]
        },
        "properties": {
            "OBJECTID": 1593,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1594,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14787645293603, 25.865679648996661]
        },
        "properties": {
            "OBJECTID": 1594,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1595,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14814036708435, 25.865508151879737]
        },
        "properties": {
            "OBJECTID": 1595,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1596,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14845120605838, 25.865422505844037]
        },
        "properties": {
            "OBJECTID": 1596,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1597,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1485161505999, 25.865416653955492]
        },
        "properties": {
            "OBJECTID": 1597,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1598,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14864661165188, 25.86541652355379]
        },
        "properties": {
            "OBJECTID": 1598,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1599,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14883999916668, 25.865445255094471]
        },
        "properties": {
            "OBJECTID": 1599,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1600,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14930099344616, 25.865562815371675]
        },
        "properties": {
            "OBJECTID": 1600,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1601,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14986338898427, 25.865473646691498]
        },
        "properties": {
            "OBJECTID": 1601,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1602,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14999131574666, 25.865450491846786]
        },
        "properties": {
            "OBJECTID": 1602,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1603,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15005626028824, 25.865444639058921]
        },
        "properties": {
            "OBJECTID": 1603,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1604,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15025168117023, 25.865450231043383]
        },
        "properties": {
            "OBJECTID": 1604,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1605,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15206108924792, 25.865886702609316]
        },
        "properties": {
            "OBJECTID": 1605,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1606,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15207906579633, 25.86589490622498]
        },
        "properties": {
            "OBJECTID": 1606,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1607,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15323992688116, 25.866523590791132]
        },
        "properties": {
            "OBJECTID": 1607,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1608,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15355012643727, 25.866554766689205]
        },
        "properties": {
            "OBJECTID": 1608,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1609,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15394331273376, 25.86656168067708]
        },
        "properties": {
            "OBJECTID": 1609,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1610,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15458155619393, 25.866476997815255]
        },
        "properties": {
            "OBJECTID": 1610,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1611,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15481977221475, 25.866472180147071]
        },
        "properties": {
            "OBJECTID": 1611,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1612,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15549743116156, 25.866088505979803]
        },
        "properties": {
            "OBJECTID": 1612,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1613,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1555504381023, 25.866054057448821]
        },
        "properties": {
            "OBJECTID": 1613,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1614,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15560582195116, 25.866022817698934]
        },
        "properties": {
            "OBJECTID": 1614,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1615,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15579923104974, 25.865838659926283]
        },
        "properties": {
            "OBJECTID": 1615,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1616,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15587429296431, 25.865622032131114]
        },
        "properties": {
            "OBJECTID": 1616,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1617,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15588893032992, 25.865591557704249]
        },
        "properties": {
            "OBJECTID": 1617,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1618,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15596404440521, 25.86544362912025]
        },
        "properties": {
            "OBJECTID": 1618,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1619,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15607752176027, 25.865274911807774]
        },
        "properties": {
            "OBJECTID": 1619,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1620,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1562124821204, 25.865125769139013]
        },
        "properties": {
            "OBJECTID": 1620,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1621,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1569011208901, 25.864359680754717]
        },
        "properties": {
            "OBJECTID": 1621,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1622,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15727301573713, 25.863945596012513]
        },
        "properties": {
            "OBJECTID": 1622,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1623,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15750952844206, 25.863479742696029]
        },
        "properties": {
            "OBJECTID": 1623,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1624,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15755149620463, 25.863218279200282]
        },
        "properties": {
            "OBJECTID": 1624,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1625,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15755717991999, 25.863157297071666]
        },
        "properties": {
            "OBJECTID": 1625,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1626,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15750724056676, 25.863003808878716]
        },
        "properties": {
            "OBJECTID": 1626,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1627,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1574878502841, 25.862827789970595]
        },
        "properties": {
            "OBJECTID": 1627,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1628,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1574899088323, 25.862768771961328]
        },
        "properties": {
            "OBJECTID": 1628,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1629,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15749436677169, 25.862723753698333]
        },
        "properties": {
            "OBJECTID": 1629,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1630,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15753684265121, 25.86239015018532]
        },
        "properties": {
            "OBJECTID": 1630,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1631,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15760008117888, 25.862061959691744]
        },
        "properties": {
            "OBJECTID": 1631,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1632,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15758290232912, 25.861944955195554]
        },
        "properties": {
            "OBJECTID": 1632,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1633,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15758068999691, 25.861885940783566]
        },
        "properties": {
            "OBJECTID": 1633,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1634,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15758694298307, 25.861653109903159]
        },
        "properties": {
            "OBJECTID": 1634,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1635,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15752221337948, 25.861517831182994]
        },
        "properties": {
            "OBJECTID": 1635,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1636,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15750319361757, 25.861461345664623]
        },
        "properties": {
            "OBJECTID": 1636,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1637,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15747759441552, 25.861345608313229]
        },
        "properties": {
            "OBJECTID": 1637,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1638,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15747097000934, 25.86116881778679]
        },
        "properties": {
            "OBJECTID": 1638,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1639,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15748784758614, 25.861051777317698]
        },
        "properties": {
            "OBJECTID": 1639,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1640,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15750645455927, 25.860924711206508]
        },
        "properties": {
            "OBJECTID": 1640,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1641,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15720329039959, 25.860559120606979]
        },
        "properties": {
            "OBJECTID": 1641,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1642,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15697324202154, 25.860108241002592]
        },
        "properties": {
            "OBJECTID": 1642,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1643,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15623720987941, 25.858917842584447]
        },
        "properties": {
            "OBJECTID": 1643,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1644,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15612522719755, 25.858756219124245]
        },
        "properties": {
            "OBJECTID": 1644,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1645,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1557501631396, 25.858022806707197]
        },
        "properties": {
            "OBJECTID": 1645,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1646,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1555652301513, 25.85677806765375]
        },
        "properties": {
            "OBJECTID": 1646,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1647,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1555196228324, 25.856300361272702]
        },
        "properties": {
            "OBJECTID": 1647,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1648,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1554530747996, 25.856165489046134]
        },
        "properties": {
            "OBJECTID": 1648,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1649,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15506994562156, 25.855866718074651]
        },
        "properties": {
            "OBJECTID": 1649,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1650,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15476792270152, 25.855418679428567]
        },
        "properties": {
            "OBJECTID": 1650,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1651,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15469397684564, 25.85523616111999]
        },
        "properties": {
            "OBJECTID": 1651,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1652,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15449907577181, 25.854890709938388]
        },
        "properties": {
            "OBJECTID": 1652,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1653,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1543045398227, 25.854908249416212]
        },
        "properties": {
            "OBJECTID": 1653,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1654,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15398391622483, 25.854860690568444]
        },
        "properties": {
            "OBJECTID": 1654,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1655,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15296292310046, 25.853660051570159]
        },
        "properties": {
            "OBJECTID": 1655,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1656,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15285891920382, 25.853362177222778]
        },
        "properties": {
            "OBJECTID": 1656,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1657,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15285244858171, 25.853303420016857]
        },
        "properties": {
            "OBJECTID": 1657,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1658,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1527872126598, 25.853094806080492]
        },
        "properties": {
            "OBJECTID": 1658,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1659,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1527678313704, 25.852918786272994]
        },
        "properties": {
            "OBJECTID": 1659,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1660,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15280152087354, 25.85268520625732]
        },
        "properties": {
            "OBJECTID": 1660,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1661,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15288406334889, 25.852495532942555]
        },
        "properties": {
            "OBJECTID": 1661,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1662,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15286529719577, 25.852430493972179]
        },
        "properties": {
            "OBJECTID": 1662,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1663,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15256144235673, 25.85169945216802]
        },
        "properties": {
            "OBJECTID": 1663,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1664,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15254242889006, 25.851642965750329]
        },
        "properties": {
            "OBJECTID": 1664,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1665,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1525275379156, 25.851585475689205]
        },
        "properties": {
            "OBJECTID": 1665,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1666,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15252137935823, 25.851554887947771]
        },
        "properties": {
            "OBJECTID": 1666,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1667,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15243595905235, 25.85115542977934]
        },
        "properties": {
            "OBJECTID": 1667,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1668,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15242465727215, 25.851151281206739]
        },
        "properties": {
            "OBJECTID": 1668,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1669,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15214856270609, 25.850996319924263]
        },
        "properties": {
            "OBJECTID": 1669,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1670,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15201476606865, 25.850867349948317]
        },
        "properties": {
            "OBJECTID": 1670,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1671,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15188434638554, 25.850663479036371]
        },
        "properties": {
            "OBJECTID": 1671,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1672,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15186129136555, 25.850608239079008]
        },
        "properties": {
            "OBJECTID": 1672,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1673,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15178288757028, 25.850132810680691]
        },
        "properties": {
            "OBJECTID": 1673,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1674,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15183399874013, 25.849794994743263]
        },
        "properties": {
            "OBJECTID": 1674,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1675,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15184951204543, 25.849724219896871]
        },
        "properties": {
            "OBJECTID": 1675,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1676,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15185568858919, 25.849547416779899]
        },
        "properties": {
            "OBJECTID": 1676,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1677,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15186624842869, 25.84948914700658]
        },
        "properties": {
            "OBJECTID": 1677,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1678,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15209404130701, 25.849069993685305]
        },
        "properties": {
            "OBJECTID": 1678,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1679,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15213859731847, 25.849026852307418]
        },
        "properties": {
            "OBJECTID": 1679,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1680,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15240246919865, 25.848855348895313]
        },
        "properties": {
            "OBJECTID": 1680,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1681,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15239949064403, 25.848327043058646]
        },
        "properties": {
            "OBJECTID": 1681,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1682,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15240781836616, 25.848220117264646]
        },
        "properties": {
            "OBJECTID": 1682,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1683,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15250177773493, 25.847938702309307]
        },
        "properties": {
            "OBJECTID": 1683,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1684,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15269071990087, 25.847699550993525]
        },
        "properties": {
            "OBJECTID": 1684,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1685,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1527741014437, 25.847632487649264]
        },
        "properties": {
            "OBJECTID": 1685,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1686,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1527134754466, 25.847145952623691]
        },
        "properties": {
            "OBJECTID": 1686,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1687,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15270703180414, 25.846998087891507]
        },
        "properties": {
            "OBJECTID": 1687,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1688,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15266820807142, 25.846949300569804]
        },
        "properties": {
            "OBJECTID": 1688,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1689,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15260286063358, 25.846847149276584]
        },
        "properties": {
            "OBJECTID": 1689,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1690,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1525528088651, 25.846738149645944]
        },
        "properties": {
            "OBJECTID": 1690,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1691,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15250820429026, 25.846565924977483]
        },
        "properties": {
            "OBJECTID": 1691,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1692,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15250173456747, 25.84650716777162]
        },
        "properties": {
            "OBJECTID": 1692,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1693,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15249952673184, 25.846448153359631]
        },
        "properties": {
            "OBJECTID": 1693,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1694,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15250717816377, 25.846335574427258]
        },
        "properties": {
            "OBJECTID": 1694,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1695,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15253303187399, 25.845828882896683]
        },
        "properties": {
            "OBJECTID": 1695,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1696,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15258338491532, 25.845479763380411]
        },
        "properties": {
            "OBJECTID": 1696,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1697,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15270314043767, 25.845262042908985]
        },
        "properties": {
            "OBJECTID": 1697,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1698,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15231686183375, 25.843875071577315]
        },
        "properties": {
            "OBJECTID": 1698,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1699,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15229726380772, 25.843816612047078]
        },
        "properties": {
            "OBJECTID": 1699,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1700,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15223655237503, 25.84361021943613]
        },
        "properties": {
            "OBJECTID": 1700,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1701,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15207592266574, 25.84325590633506]
        },
        "properties": {
            "OBJECTID": 1701,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1702,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15203447021469, 25.843210301714123]
        },
        "properties": {
            "OBJECTID": 1702,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1703,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15195248082239, 25.843178059220122]
        },
        "properties": {
            "OBJECTID": 1703,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1704,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15130904917334, 25.842557788711019]
        },
        "properties": {
            "OBJECTID": 1704,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1705,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1511395602426, 25.841760033398884]
        },
        "properties": {
            "OBJECTID": 1705,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1706,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15113927066085, 25.841524473076049]
        },
        "properties": {
            "OBJECTID": 1706,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1707,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15112390934098, 25.841259486935826]
        },
        "properties": {
            "OBJECTID": 1707,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1708,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1510585663998, 25.84115733294459]
        },
        "properties": {
            "OBJECTID": 1708,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1709,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15103156965125, 25.841103574170688]
        },
        "properties": {
            "OBJECTID": 1709,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1710,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15077566396576, 25.840459495909045]
        },
        "properties": {
            "OBJECTID": 1710,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1711,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15062662921565, 25.840151955648309]
        },
        "properties": {
            "OBJECTID": 1711,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1712,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15046051723914, 25.840058711240601]
        },
        "properties": {
            "OBJECTID": 1712,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1713,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15027624705124, 25.83989232676953]
        },
        "properties": {
            "OBJECTID": 1713,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1714,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14835826292358, 25.837225202880859]
        },
        "properties": {
            "OBJECTID": 1714,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1715,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14833648404158, 25.837207437673158]
        },
        "properties": {
            "OBJECTID": 1715,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1716,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14797671385435, 25.836905185426019]
        },
        "properties": {
            "OBJECTID": 1716,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1717,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14795081967463, 25.836882631328422]
        },
        "properties": {
            "OBJECTID": 1717,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1718,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14786471498468, 25.836793972563839]
        },
        "properties": {
            "OBJECTID": 1718,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1719,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14776825280268, 25.836672856367272]
        },
        "properties": {
            "OBJECTID": 1719,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1720,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14720054137194, 25.836381232308099]
        },
        "properties": {
            "OBJECTID": 1720,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1721,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14693187160867, 25.835950179157521]
        },
        "properties": {
            "OBJECTID": 1721,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1722,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14690882108528, 25.835894938300839]
        },
        "properties": {
            "OBJECTID": 1722,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1723,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14686473092263, 25.83576979314148]
        },
        "properties": {
            "OBJECTID": 1723,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1724,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14677309810008, 25.83557212665238]
        },
        "properties": {
            "OBJECTID": 1724,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1725,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14639072705046, 25.834900787241565]
        },
        "properties": {
            "OBJECTID": 1725,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1726,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14630159524251, 25.834026290980603]
        },
        "properties": {
            "OBJECTID": 1726,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1727,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1463205448573, 25.83385023160298]
        },
        "properties": {
            "OBJECTID": 1727,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1728,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14637751600964, 25.833680041200978]
        },
        "properties": {
            "OBJECTID": 1728,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1729,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14557153459873, 25.831738010101162]
        },
        "properties": {
            "OBJECTID": 1729,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1730,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14549227285016, 25.831440743695396]
        },
        "properties": {
            "OBJECTID": 1730,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1731,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14560485717845, 25.830851399972175]
        },
        "properties": {
            "OBJECTID": 1731,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1732,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1457078970019, 25.830701083688098]
        },
        "properties": {
            "OBJECTID": 1732,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1733,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14574923523907, 25.830655399027478]
        },
        "properties": {
            "OBJECTID": 1733,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1734,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14589175170306, 25.830534338588905]
        },
        "properties": {
            "OBJECTID": 1734,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1735,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14599164569796, 25.830473437399291]
        },
        "properties": {
            "OBJECTID": 1735,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1736,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14597390297331, 25.830306255229573]
        },
        "properties": {
            "OBJECTID": 1736,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1737,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14597596871602, 25.830247236320929]
        },
        "properties": {
            "OBJECTID": 1737,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1738,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14624212037762, 25.828783994385901]
        },
        "properties": {
            "OBJECTID": 1738,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1739,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14627281603771, 25.828731887666549]
        },
        "properties": {
            "OBJECTID": 1739,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1740,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1463864948409, 25.828587991642564]
        },
        "properties": {
            "OBJECTID": 1740,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1741,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14643104635576, 25.828544851163997]
        },
        "properties": {
            "OBJECTID": 1741,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1742,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14658200385645, 25.828432485370968]
        },
        "properties": {
            "OBJECTID": 1742,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1743,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1466440031183, 25.828311360181203]
        },
        "properties": {
            "OBJECTID": 1743,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1744,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14690352677638, 25.82780318027261]
        },
        "properties": {
            "OBJECTID": 1744,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1745,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14694807739193, 25.827760038894723]
        },
        "properties": {
            "OBJECTID": 1745,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1746,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14733235140693, 25.827543260013499]
        },
        "properties": {
            "OBJECTID": 1746,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1747,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14739476615557, 25.827526058680689]
        },
        "properties": {
            "OBJECTID": 1747,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1748,
        "geometry": {
            "type": "Point",
            "coordinates": [100.147587577205, 25.827497056444031]
        },
        "properties": {
            "OBJECTID": 1748,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1749,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1476909497776, 25.827495684977919]
        },
        "properties": {
            "OBJECTID": 1749,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1750,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14793525960459, 25.827213528981247]
        },
        "properties": {
            "OBJECTID": 1750,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1751,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14802114575934, 25.82712470294274]
        },
        "properties": {
            "OBJECTID": 1751,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1752,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14806871719759, 25.827084291007282]
        },
        "properties": {
            "OBJECTID": 1752,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1753,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14818585029684, 25.827004578698393]
        },
        "properties": {
            "OBJECTID": 1753,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1754,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14817450265122, 25.826894566431122]
        },
        "properties": {
            "OBJECTID": 1754,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1755,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14802291382648, 25.826725400356963]
        },
        "properties": {
            "OBJECTID": 1755,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1756,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14797730111161, 25.826658533064915]
        },
        "properties": {
            "OBJECTID": 1756,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1757,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14784318341623, 25.826207487085924]
        },
        "properties": {
            "OBJECTID": 1757,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1758,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14787069727493, 25.825995989923058]
        },
        "properties": {
            "OBJECTID": 1758,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1759,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14792545879294, 25.825789417447709]
        },
        "properties": {
            "OBJECTID": 1759,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1760,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14798020142524, 25.825490131164656]
        },
        "properties": {
            "OBJECTID": 1760,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1761,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14746158398498, 25.825303734979457]
        },
        "properties": {
            "OBJECTID": 1761,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1762,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1473485688814, 25.825244827586801]
        },
        "properties": {
            "OBJECTID": 1762,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1763,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1472954935922, 25.825210485175774]
        },
        "properties": {
            "OBJECTID": 1763,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1764,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1472450164444, 25.825173074277984]
        },
        "properties": {
            "OBJECTID": 1764,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1765,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14719734877866, 25.825132755872062]
        },
        "properties": {
            "OBJECTID": 1765,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1766,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14692393689023, 25.824670999866782]
        },
        "properties": {
            "OBJECTID": 1766,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1767,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14687918032996, 25.82424737061973]
        },
        "properties": {
            "OBJECTID": 1767,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1768,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14689606420205, 25.824130330150695]
        },
        "properties": {
            "OBJECTID": 1768,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1769,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1469100738409, 25.82407532581567]
        },
        "properties": {
            "OBJECTID": 1769,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1770,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14698667809279, 25.823774354203124]
        },
        "properties": {
            "OBJECTID": 1770,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1771,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14700249177162, 25.823670744209551]
        },
        "properties": {
            "OBJECTID": 1771,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1772,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14705902765206, 25.823501412660107]
        },
        "properties": {
            "OBJECTID": 1772,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1773,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14718835375959, 25.823249116853333]
        },
        "properties": {
            "OBJECTID": 1773,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1774,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14722274833122, 25.823198939179747]
        },
        "properties": {
            "OBJECTID": 1774,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1775,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14734657688308, 25.823062077652821]
        },
        "properties": {
            "OBJECTID": 1775,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1776,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14741985813998, 25.82289921222889]
        },
        "properties": {
            "OBJECTID": 1776,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1777,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14722127704039, 25.822406783046631]
        },
        "properties": {
            "OBJECTID": 1777,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1778,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14722054589151, 25.82239584639126]
        },
        "properties": {
            "OBJECTID": 1778,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1779,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14720835018528, 25.822169503220039]
        },
        "properties": {
            "OBJECTID": 1779,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1780,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14722389586615, 25.821995534766188]
        },
        "properties": {
            "OBJECTID": 1780,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1781,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14723611495481, 25.821925102561465]
        },
        "properties": {
            "OBJECTID": 1781,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1782,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14726973431084, 25.82181105773418]
        },
        "properties": {
            "OBJECTID": 1782,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1783,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14726088588122, 25.82164036101392]
        },
        "properties": {
            "OBJECTID": 1783,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1784,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14718003323202, 25.82147931491852]
        },
        "properties": {
            "OBJECTID": 1784,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1785,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14696254658429, 25.820817988559384]
        },
        "properties": {
            "OBJECTID": 1785,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1786,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14681687529861, 25.820514845983382]
        },
        "properties": {
            "OBJECTID": 1786,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1787,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14650179692046, 25.820265686112577]
        },
        "properties": {
            "OBJECTID": 1787,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1788,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14553684594875, 25.818835921440666]
        },
        "properties": {
            "OBJECTID": 1788,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1789,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14552615121096, 25.818777670553118]
        },
        "properties": {
            "OBJECTID": 1789,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1790,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14547846735746, 25.818549614173378]
        },
        "properties": {
            "OBJECTID": 1790,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1791,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14544458090273, 25.818435634996604]
        },
        "properties": {
            "OBJECTID": 1791,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1792,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14543388526567, 25.818377385008375]
        },
        "properties": {
            "OBJECTID": 1792,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1793,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14497059411769, 25.817882140948086]
        },
        "properties": {
            "OBJECTID": 1793,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1794,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14493253570799, 25.817834180103375]
        },
        "properties": {
            "OBJECTID": 1794,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1795,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14482671068419, 25.81767963700571]
        },
        "properties": {
            "OBJECTID": 1795,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1796,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1447632644132, 25.817567923221134]
        },
        "properties": {
            "OBJECTID": 1796,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1797,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14475625329851, 25.817555339007754]
        },
        "properties": {
            "OBJECTID": 1797,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1798,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14444902780048, 25.817217801860124]
        },
        "properties": {
            "OBJECTID": 1798,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1799,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1435218042879, 25.816236288074265]
        },
        "properties": {
            "OBJECTID": 1799,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1800,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14332342373706, 25.815981108141671]
        },
        "properties": {
            "OBJECTID": 1800,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1801,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14331040425179, 25.815963940983124]
        },
        "properties": {
            "OBJECTID": 1801,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1802,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1430733105849, 25.815590723233356]
        },
        "properties": {
            "OBJECTID": 1802,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1803,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14268538891963, 25.814979865928137]
        },
        "properties": {
            "OBJECTID": 1803,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1804,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14265840296298, 25.814926106254859]
        },
        "properties": {
            "OBJECTID": 1804,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1805,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14246096310302, 25.814421300602589]
        },
        "properties": {
            "OBJECTID": 1805,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1806,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14244967571204, 25.814386182976023]
        },
        "properties": {
            "OBJECTID": 1806,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1807,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14241750606311, 25.814093648802896]
        },
        "properties": {
            "OBJECTID": 1807,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1808,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14243153998365, 25.813978492413526]
        },
        "properties": {
            "OBJECTID": 1808,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1809,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14245604920734, 25.81385096674876]
        },
        "properties": {
            "OBJECTID": 1809,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1810,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14244819992456, 25.813844261403574]
        },
        "properties": {
            "OBJECTID": 1810,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1811,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1422087149603, 25.813496474682609]
        },
        "properties": {
            "OBJECTID": 1811,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1812,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14210425870465, 25.813178558943378]
        },
        "properties": {
            "OBJECTID": 1812,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1813,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14209885018187, 25.813168455959499]
        },
        "properties": {
            "OBJECTID": 1813,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1814,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14202588098965, 25.813081674979344]
        },
        "properties": {
            "OBJECTID": 1814,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1815,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14186751397466, 25.812876498251853]
        },
        "properties": {
            "OBJECTID": 1815,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1816,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14175901166914, 25.812811023110271]
        },
        "properties": {
            "OBJECTID": 1816,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1817,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14161623620038, 25.812690233367618]
        },
        "properties": {
            "OBJECTID": 1817,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1818,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14157479813849, 25.812644626048723]
        },
        "properties": {
            "OBJECTID": 1818,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1819,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14139621796113, 25.812312295975744]
        },
        "properties": {
            "OBJECTID": 1819,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1820,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14139022128177, 25.812292983934128]
        },
        "properties": {
            "OBJECTID": 1820,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1821,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14136731105265, 25.812191780526291]
        },
        "properties": {
            "OBJECTID": 1821,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1822,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14135817483998, 25.812118481282937]
        },
        "properties": {
            "OBJECTID": 1822,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1823,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14135630155215, 25.812035168987904]
        },
        "properties": {
            "OBJECTID": 1823,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1824,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14132471826122, 25.811885891420786]
        },
        "properties": {
            "OBJECTID": 1824,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1825,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14131260709121, 25.811853820697308]
        },
        "properties": {
            "OBJECTID": 1825,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1826,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14123136233758, 25.811624701318976]
        },
        "properties": {
            "OBJECTID": 1826,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1827,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14121791657362, 25.81158342873232]
        },
        "properties": {
            "OBJECTID": 1827,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1828,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14119233715667, 25.811467685085631]
        },
        "properties": {
            "OBJECTID": 1828,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1829,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14119486605028, 25.810992324136521]
        },
        "properties": {
            "OBJECTID": 1829,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1830,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14118562461692, 25.8109157072941]
        },
        "properties": {
            "OBJECTID": 1830,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1831,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1411801198667, 25.810842807349729]
        },
        "properties": {
            "OBJECTID": 1831,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1832,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14117509895169, 25.810679036308557]
        },
        "properties": {
            "OBJECTID": 1832,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1833,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14117512413276, 25.810628194035985]
        },
        "properties": {
            "OBJECTID": 1833,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1834,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14119647493743, 25.809445092716885]
        },
        "properties": {
            "OBJECTID": 1834,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1835,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14124608334015, 25.809225336180361]
        },
        "properties": {
            "OBJECTID": 1835,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1836,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14126899626723, 25.8091700467603]
        },
        "properties": {
            "OBJECTID": 1836,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1837,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14129585541946, 25.809116234027044]
        },
        "properties": {
            "OBJECTID": 1837,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1838,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14192403726457, 25.80793153910929]
        },
        "properties": {
            "OBJECTID": 1838,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1839,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1419498559012, 25.807626455796367]
        },
        "properties": {
            "OBJECTID": 1839,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1840,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14193085142773, 25.80756996668066]
        },
        "properties": {
            "OBJECTID": 1840,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1841,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14189880588526, 25.807395463130149]
        },
        "properties": {
            "OBJECTID": 1841,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1842,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14197843995311, 25.80684492605252]
        },
        "properties": {
            "OBJECTID": 1842,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1843,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14199794714762, 25.806771805774247]
        },
        "properties": {
            "OBJECTID": 1843,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1844,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14215441389513, 25.806132434564233]
        },
        "properties": {
            "OBJECTID": 1844,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1845,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14212016231568, 25.805898914803151]
        },
        "properties": {
            "OBJECTID": 1845,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1846,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14228247285791, 25.805294526321404]
        },
        "properties": {
            "OBJECTID": 1846,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1847,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14228026772025, 25.805235509211457]
        },
        "properties": {
            "OBJECTID": 1847,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1848,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14228865389833, 25.805117716909137]
        },
        "properties": {
            "OBJECTID": 1848,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1849,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14255069116155, 25.803999327207237]
        },
        "properties": {
            "OBJECTID": 1849,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1850,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1425570133955, 25.803940554712881]
        },
        "properties": {
            "OBJECTID": 1850,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1851,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14256756873834, 25.803882283140922]
        },
        "properties": {
            "OBJECTID": 1851,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1852,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14258231222396, 25.803824760704174]
        },
        "properties": {
            "OBJECTID": 1852,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1853,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14270088783576, 25.803506290982568]
        },
        "properties": {
            "OBJECTID": 1853,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1854,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14275004837612, 25.802813684404043]
        },
        "properties": {
            "OBJECTID": 1854,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1855,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14290708079722, 25.802179301733418]
        },
        "properties": {
            "OBJECTID": 1855,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1856,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14293867487999, 25.802074243831328]
        },
        "properties": {
            "OBJECTID": 1856,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1857,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14394689763128, 25.801371132271981]
        },
        "properties": {
            "OBJECTID": 1857,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1858,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14391990987599, 25.801317371699383]
        },
        "properties": {
            "OBJECTID": 1858,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1859,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14391906631192, 25.80062672024701]
        },
        "properties": {
            "OBJECTID": 1859,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1860,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14393004793345, 25.800603568100257]
        },
        "properties": {
            "OBJECTID": 1860,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1861,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14403357698802, 25.800247333147979]
        },
        "properties": {
            "OBJECTID": 1861,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1862,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14396865223159, 25.800241609862439]
        },
        "properties": {
            "OBJECTID": 1862,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1863,
        "geometry": {
            "type": "Point",
            "coordinates": [100.143717214378, 25.800180892134506]
        },
        "properties": {
            "OBJECTID": 1863,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1864,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14360020538521, 25.800128801602966]
        },
        "properties": {
            "OBJECTID": 1864,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1865,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1432694401326, 25.799848982044921]
        },
        "properties": {
            "OBJECTID": 1865,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1866,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14310950380064, 25.799465589365525]
        },
        "properties": {
            "OBJECTID": 1866,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1867,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14293660913728, 25.798374024641589]
        },
        "properties": {
            "OBJECTID": 1867,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1868,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14289202614617, 25.798201792778571]
        },
        "properties": {
            "OBJECTID": 1868,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1869,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14288541792774, 25.798024996856157]
        },
        "properties": {
            "OBJECTID": 1869,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1870,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14269987969573, 25.797272275994771]
        },
        "properties": {
            "OBJECTID": 1870,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1871,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14268918405867, 25.797214026006543]
        },
        "properties": {
            "OBJECTID": 1871,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1872,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14267659804665, 25.797122290661264]
        },
        "properties": {
            "OBJECTID": 1872,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1873,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1425163838241, 25.796773487706332]
        },
        "properties": {
            "OBJECTID": 1873,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1874,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14244875930285, 25.796546013187992]
        },
        "properties": {
            "OBJECTID": 1874,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1875,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14244229497598, 25.796487254183489]
        },
        "properties": {
            "OBJECTID": 1875,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1876,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14250375104734, 25.796053267444393]
        },
        "properties": {
            "OBJECTID": 1876,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1877,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14252666127646, 25.795997978024332]
        },
        "properties": {
            "OBJECTID": 1877,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1878,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14258420259893, 25.795892056773027]
        },
        "properties": {
            "OBJECTID": 1878,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1879,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14264329165462, 25.795378616729806]
        },
        "properties": {
            "OBJECTID": 1879,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1880,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14257808001446, 25.795009593418058]
        },
        "properties": {
            "OBJECTID": 1880,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1881,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14254498496314, 25.794933215795311]
        },
        "properties": {
            "OBJECTID": 1881,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1882,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14237403193607, 25.794289706804591]
        },
        "properties": {
            "OBJECTID": 1882,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1883,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14236669166957, 25.793947607396206]
        },
        "properties": {
            "OBJECTID": 1883,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1884,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14239831183266, 25.793773039993823]
        },
        "properties": {
            "OBJECTID": 1884,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1885,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14249762936214, 25.793555304233962]
        },
        "properties": {
            "OBJECTID": 1885,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1886,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14253201404125, 25.793505124761737]
        },
        "properties": {
            "OBJECTID": 1886,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1887,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14256995014313, 25.793457088373998]
        },
        "properties": {
            "OBJECTID": 1887,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1888,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14251570663464, 25.7932421468077]
        },
        "properties": {
            "OBJECTID": 1888,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1889,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14249042938985, 25.793041201390395]
        },
        "properties": {
            "OBJECTID": 1889,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1890,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14249249243466, 25.792982179783849]
        },
        "properties": {
            "OBJECTID": 1890,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1891,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14249881466861, 25.792923408188813]
        },
        "properties": {
            "OBJECTID": 1891,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1892,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14250937001145, 25.792865134818157]
        },
        "properties": {
            "OBJECTID": 1892,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1893,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14252411079912, 25.792807613280786]
        },
        "properties": {
            "OBJECTID": 1893,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1894,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14232066616586, 25.792046251733154]
        },
        "properties": {
            "OBJECTID": 1894,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1895,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14231332859731, 25.792017384394796]
        },
        "properties": {
            "OBJECTID": 1895,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1896,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14218205995417, 25.791904685852614]
        },
        "properties": {
            "OBJECTID": 1896,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1897,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1420372592122, 25.791708960100493]
        },
        "properties": {
            "OBJECTID": 1897,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1898,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14195334976733, 25.791485974997784]
        },
        "properties": {
            "OBJECTID": 1898,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1899,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14195153493546, 25.791477559142095]
        },
        "properties": {
            "OBJECTID": 1899,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1900,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14189212302313, 25.791106715602496]
        },
        "properties": {
            "OBJECTID": 1900,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1901,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14190145798597, 25.790729307911306]
        },
        "properties": {
            "OBJECTID": 1901,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1902,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14179236302721, 25.790464729163944]
        },
        "properties": {
            "OBJECTID": 1902,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1903,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1416475649832, 25.790269001613183]
        },
        "properties": {
            "OBJECTID": 1903,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1904,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14162057992587, 25.790215241939904]
        },
        "properties": {
            "OBJECTID": 1904,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1905,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14159753929505, 25.790159998385263]
        },
        "properties": {
            "OBJECTID": 1905,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1906,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14156365643765, 25.79004601740985]
        },
        "properties": {
            "OBJECTID": 1906,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1907,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14153231956095, 25.789742974658623]
        },
        "properties": {
            "OBJECTID": 1907,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1908,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14153142383623, 25.789636762026987]
        },
        "properties": {
            "OBJECTID": 1908,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1909,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14151441765631, 25.789601815271624]
        },
        "properties": {
            "OBJECTID": 1909,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1910,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14137528624053, 25.788469966315915]
        },
        "properties": {
            "OBJECTID": 1910,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1911,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14141945104689, 25.788297645420073]
        },
        "properties": {
            "OBJECTID": 1911,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1912,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14144236127601, 25.788242356899275]
        },
        "properties": {
            "OBJECTID": 1912,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1913,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14146781568718, 25.787832683331715]
        },
        "properties": {
            "OBJECTID": 1913,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1914,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1415422300899, 25.787477371983186]
        },
        "properties": {
            "OBJECTID": 1914,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1915,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14161086095254, 25.78731174384194]
        },
        "properties": {
            "OBJECTID": 1915,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1916,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14167592690256, 25.7872094567511]
        },
        "properties": {
            "OBJECTID": 1916,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1917,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14242364573471, 25.785958023144303]
        },
        "properties": {
            "OBJECTID": 1917,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1918,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1432336399206, 25.78443409405827]
        },
        "properties": {
            "OBJECTID": 1918,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1919,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14341299441423, 25.783828543652419]
        },
        "properties": {
            "OBJECTID": 1919,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1920,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14346275570165, 25.783719440599782]
        },
        "properties": {
            "OBJECTID": 1920,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1921,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14375672339412, 25.783294774434353]
        },
        "properties": {
            "OBJECTID": 1921,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1922,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14377905086258, 25.783264694809873]
        },
        "properties": {
            "OBJECTID": 1922,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1923,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14398065098629, 25.783004548820941]
        },
        "properties": {
            "OBJECTID": 1923,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1924,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14399252473527, 25.782983290646371]
        },
        "properties": {
            "OBJECTID": 1924,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1925,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14402641208932, 25.782829871701267]
        },
        "properties": {
            "OBJECTID": 1925,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1926,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14404074008814, 25.782774222552405]
        },
        "properties": {
            "OBJECTID": 1926,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1927,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14417556285201, 25.782421343869999]
        },
        "properties": {
            "OBJECTID": 1927,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1928,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14419370037905, 25.782378522650731]
        },
        "properties": {
            "OBJECTID": 1928,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1929,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14463835217828, 25.781839086804439]
        },
        "properties": {
            "OBJECTID": 1929,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1930,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14465051191166, 25.781830752787073]
        },
        "properties": {
            "OBJECTID": 1930,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1931,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14518389442122, 25.781469970862531]
        },
        "properties": {
            "OBJECTID": 1931,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1932,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14527901121744, 25.781301538635148]
        },
        "properties": {
            "OBJECTID": 1932,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1933,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14549385026106, 25.780328077378897]
        },
        "properties": {
            "OBJECTID": 1933,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1934,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14620586870592, 25.778544971773101]
        },
        "properties": {
            "OBJECTID": 1934,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1935,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1463395457335, 25.778277052943679]
        },
        "properties": {
            "OBJECTID": 1935,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1936,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14641879489153, 25.778183328298041]
        },
        "properties": {
            "OBJECTID": 1936,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1937,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14646332482266, 25.778140183322876]
        },
        "properties": {
            "OBJECTID": 1937,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1938,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14655083515299, 25.777823363857181]
        },
        "properties": {
            "OBJECTID": 1938,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1939,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14664240232509, 25.777585442813972]
        },
        "properties": {
            "OBJECTID": 1939,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1940,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14682683169298, 25.777220493431059]
        },
        "properties": {
            "OBJECTID": 1940,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1941,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14686815104437, 25.777174804273841]
        },
        "properties": {
            "OBJECTID": 1941,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1942,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14701060095854, 25.777053734842013]
        },
        "properties": {
            "OBJECTID": 1942,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1943,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14711891710442, 25.776988045661824]
        },
        "properties": {
            "OBJECTID": 1943,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1944,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14714310976677, 25.776975771714547]
        },
        "properties": {
            "OBJECTID": 1944,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1945,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14715445831166, 25.776960000303802]
        },
        "properties": {
            "OBJECTID": 1945,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1946,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14719238721898, 25.776911962117424]
        },
        "properties": {
            "OBJECTID": 1946,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1947,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14723735332126, 25.776860082027213]
        },
        "properties": {
            "OBJECTID": 1947,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1948,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14730966061239, 25.77676186256997]
        },
        "properties": {
            "OBJECTID": 1948,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1949,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14734299758135, 25.77672500025858]
        },
        "properties": {
            "OBJECTID": 1949,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1950,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14743296935609, 25.776346061921288]
        },
        "properties": {
            "OBJECTID": 1950,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1951,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14747890402828, 25.776179838428845]
        },
        "properties": {
            "OBJECTID": 1951,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1952,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14780234160486, 25.775515711580852]
        },
        "properties": {
            "OBJECTID": 1952,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1953,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14789657526677, 25.775284314219789]
        },
        "properties": {
            "OBJECTID": 1953,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1954,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14788780687684, 25.775178639382773]
        },
        "properties": {
            "OBJECTID": 1954,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1955,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14782282006718, 25.774912937382226]
        },
        "properties": {
            "OBJECTID": 1955,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1956,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1478218299136, 25.774897514908446]
        },
        "properties": {
            "OBJECTID": 1956,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1957,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14781590068333, 25.774671026047088]
        },
        "properties": {
            "OBJECTID": 1957,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1958,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14788927457045, 25.774384642337452]
        },
        "properties": {
            "OBJECTID": 1958,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1959,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1479161238301, 25.77433082690618]
        },
        "properties": {
            "OBJECTID": 1959,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1960,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14792063932612, 25.7743226205925]
        },
        "properties": {
            "OBJECTID": 1960,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1961,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14784908206957, 25.773488021854803]
        },
        "properties": {
            "OBJECTID": 1961,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1962,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14779215768198, 25.773318799123331]
        },
        "properties": {
            "OBJECTID": 1962,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1963,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1477814611456, 25.773260548235783]
        },
        "properties": {
            "OBJECTID": 1963,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1964,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14785643312797, 25.772613227918157]
        },
        "properties": {
            "OBJECTID": 1964,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1965,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1478793370618, 25.772557939397416]
        },
        "properties": {
            "OBJECTID": 1965,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1966,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14809500977697, 25.772264962757902]
        },
        "properties": {
            "OBJECTID": 1966,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1967,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14839485094166, 25.772040238366344]
        },
        "properties": {
            "OBJECTID": 1967,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1968,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14842553041393, 25.771988128949033]
        },
        "properties": {
            "OBJECTID": 1968,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1969,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14843683129482, 25.771971631785448]
        },
        "properties": {
            "OBJECTID": 1969,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1970,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14853886387755, 25.771646275954936]
        },
        "properties": {
            "OBJECTID": 1970,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1971,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1485963890122, 25.771540352005673]
        },
        "properties": {
            "OBJECTID": 1971,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1972,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14890541495299, 25.771240921831122]
        },
        "properties": {
            "OBJECTID": 1972,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1973,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14896075833241, 25.771209682081235]
        },
        "properties": {
            "OBJECTID": 1973,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1974,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14907761084311, 25.771157355927301]
        },
        "properties": {
            "OBJECTID": 1974,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1975,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14919187420571, 25.771050076699737]
        },
        "properties": {
            "OBJECTID": 1975,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1976,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14896813097454, 25.770993467974222]
        },
        "properties": {
            "OBJECTID": 1976,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1977,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1486922153735, 25.770838504893163]
        },
        "properties": {
            "OBJECTID": 1977,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1978,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14855850057438, 25.770709531319881]
        },
        "properties": {
            "OBJECTID": 1978,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1979,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14840720133134, 25.770484620768684]
        },
        "properties": {
            "OBJECTID": 1979,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1980,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14831485444705, 25.770328633359782]
        },
        "properties": {
            "OBJECTID": 1980,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1981,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14824265237655, 25.770064033028746]
        },
        "properties": {
            "OBJECTID": 1981,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1982,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14820883696831, 25.769724225092943]
        },
        "properties": {
            "OBJECTID": 1982,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1983,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14815721138626, 25.769632551800896]
        },
        "properties": {
            "OBJECTID": 1983,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1984,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1480977778902, 25.769608243125958]
        },
        "properties": {
            "OBJECTID": 1984,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1985,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14770954326087, 25.769300653402468]
        },
        "properties": {
            "OBJECTID": 1985,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1986,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14741800283861, 25.768876285811984]
        },
        "properties": {
            "OBJECTID": 1986,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1987,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14727711144957, 25.768585327251117]
        },
        "properties": {
            "OBJECTID": 1987,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1988,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14724984310584, 25.768482771262939]
        },
        "properties": {
            "OBJECTID": 1988,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1989,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1470589350219, 25.768330614066656]
        },
        "properties": {
            "OBJECTID": 1989,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1990,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14688717710158, 25.768081132238535]
        },
        "properties": {
            "OBJECTID": 1990,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1991,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14677029311457, 25.76762883530256]
        },
        "properties": {
            "OBJECTID": 1991,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1992,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14623883425486, 25.767128549642905]
        },
        "properties": {
            "OBJECTID": 1992,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1993,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14620078933501, 25.767080590596834]
        },
        "properties": {
            "OBJECTID": 1993,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1994,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14616628864337, 25.767030477674439]
        },
        "properties": {
            "OBJECTID": 1994,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1995,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14595149726387, 25.766474437645229]
        },
        "properties": {
            "OBJECTID": 1995,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1996,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14592726413201, 25.766387475901354]
        },
        "properties": {
            "OBJECTID": 1996,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1997,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14591501626506, 25.766318109393296]
        },
        "properties": {
            "OBJECTID": 1997,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1998,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14587314922653, 25.76599199543432]
        },
        "properties": {
            "OBJECTID": 1998,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 1999,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14591988429527, 25.765539516835304]
        },
        "properties": {
            "OBJECTID": 1999,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2000,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14600215517538, 25.765108204679962]
        },
        "properties": {
            "OBJECTID": 2000,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2001,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14608887230366, 25.764836466429813]
        },
        "properties": {
            "OBJECTID": 2001,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2002,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14611571886542, 25.764782651897917]
        },
        "properties": {
            "OBJECTID": 2002,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2003,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14614639563973, 25.764730543379926]
        },
        "properties": {
            "OBJECTID": 2003,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2004,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14651079193953, 25.764356137625725]
        },
        "properties": {
            "OBJECTID": 2004,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2005,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14669014643317, 25.763551354111769]
        },
        "properties": {
            "OBJECTID": 2005,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2006,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14706649921965, 25.763053962470451]
        },
        "properties": {
            "OBJECTID": 2006,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2007,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14709334578134, 25.763000147938556]
        },
        "properties": {
            "OBJECTID": 2007,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2008,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14778017511503, 25.761038404598764]
        },
        "properties": {
            "OBJECTID": 2008,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2009,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14787945667166, 25.760820666140887]
        },
        "properties": {
            "OBJECTID": 2009,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2010,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14799717343089, 25.760644408912412]
        },
        "properties": {
            "OBJECTID": 2010,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2011,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14807640819976, 25.760550683367399]
        },
        "properties": {
            "OBJECTID": 2011,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2012,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14808828284805, 25.760538612666892]
        },
        "properties": {
            "OBJECTID": 2012,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2013,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1481083269378, 25.760501739563665]
        },
        "properties": {
            "OBJECTID": 2013,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2014,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1482110546965, 25.760351816283332]
        },
        "properties": {
            "OBJECTID": 2014,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2015,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14831908305928, 25.760208569569897]
        },
        "properties": {
            "OBJECTID": 2015,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2016,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14835700567136, 25.760160533182102]
        },
        "properties": {
            "OBJECTID": 2016,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2017,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14844284236335, 25.760071699949037]
        },
        "properties": {
            "OBJECTID": 2017,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2018,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14901104122663, 25.759363402898771]
        },
        "properties": {
            "OBJECTID": 2018,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2019,
        "geometry": {
            "type": "Point",
            "coordinates": [100.14936201964156, 25.758918597315471]
        },
        "properties": {
            "OBJECTID": 2019,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2020,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15006046282014, 25.758486523434385]
        },
        "properties": {
            "OBJECTID": 2020,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2021,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15030364939395, 25.758126022098281]
        },
        "properties": {
            "OBJECTID": 2021,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2022,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15049254569448, 25.757680136429201]
        },
        "properties": {
            "OBJECTID": 2022,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2023,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15069821435117, 25.75745266640746]
        },
        "properties": {
            "OBJECTID": 2023,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2024,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15074857278847, 25.757415152087674]
        },
        "properties": {
            "OBJECTID": 2024,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2025,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15151595619648, 25.75689831260712]
        },
        "properties": {
            "OBJECTID": 2025,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2026,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1515418440809, 25.756852907635619]
        },
        "properties": {
            "OBJECTID": 2026,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2027,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15156070106553, 25.756796380748426]
        },
        "properties": {
            "OBJECTID": 2027,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2028,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15155643198381, 25.756244702431104]
        },
        "properties": {
            "OBJECTID": 2028,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2029,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15140331431155, 25.755962273939815]
        },
        "properties": {
            "OBJECTID": 2029,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2030,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15132923265804, 25.755676041316292]
        },
        "properties": {
            "OBJECTID": 2030,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2031,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15132585030779, 25.755548711703739]
        },
        "properties": {
            "OBJECTID": 2031,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2032,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15134547531352, 25.755073062072199]
        },
        "properties": {
            "OBJECTID": 2032,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2033,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15137906589126, 25.754959010949619]
        },
        "properties": {
            "OBJECTID": 2033,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2034,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15138067028175, 25.754945291791842]
        },
        "properties": {
            "OBJECTID": 2034,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2035,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15140174409527, 25.754542266912154]
        },
        "properties": {
            "OBJECTID": 2035,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2036,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1512225298959, 25.754355758311647]
        },
        "properties": {
            "OBJECTID": 2036,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2037,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15098311597808, 25.754007985080477]
        },
        "properties": {
            "OBJECTID": 2037,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2038,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15096411600121, 25.753951496864147]
        },
        "properties": {
            "OBJECTID": 2038,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2039,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15094923581864, 25.753894004105064]
        },
        "properties": {
            "OBJECTID": 2039,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2040,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15092986801903, 25.753717978002328]
        },
        "properties": {
            "OBJECTID": 2040,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2041,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15137391097727, 25.752575707702817]
        },
        "properties": {
            "OBJECTID": 2041,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2042,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15139877183589, 25.75252620811807]
        },
        "properties": {
            "OBJECTID": 2042,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2043,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15154304017921, 25.752330188287601]
        },
        "properties": {
            "OBJECTID": 2043,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2044,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15202719829665, 25.751518862205785]
        },
        "properties": {
            "OBJECTID": 2044,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2045,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15251858786257, 25.750332115035121]
        },
        "properties": {
            "OBJECTID": 2045,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2046,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15263218123022, 25.75018820462202]
        },
        "properties": {
            "OBJECTID": 2046,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2047,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15329811931281, 25.74966814017256]
        },
        "properties": {
            "OBJECTID": 2047,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2048,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15338394341433, 25.749579304241536]
        },
        "properties": {
            "OBJECTID": 2048,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2049,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15361588216729, 25.749274843259457]
        },
        "properties": {
            "OBJECTID": 2049,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2050,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15365763409261, 25.749163026052884]
        },
        "properties": {
            "OBJECTID": 2050,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2051,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15368447435907, 25.749109210621612]
        },
        "properties": {
            "OBJECTID": 2051,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2052,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15371514393883, 25.749057099405661]
        },
        "properties": {
            "OBJECTID": 2052,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2053,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15407500855486, 25.748561641306765]
        },
        "properties": {
            "OBJECTID": 2053,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2054,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15411463718084, 25.748511272976941]
        },
        "properties": {
            "OBJECTID": 2054,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2055,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15424799584838, 25.748382018815164]
        },
        "properties": {
            "OBJECTID": 2055,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2056,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15435130726706, 25.74831005236706]
        },
        "properties": {
            "OBJECTID": 2056,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2057,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1544641047347, 25.748250914747985]
        },
        "properties": {
            "OBJECTID": 2057,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2058,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15452346628501, 25.74822648196664]
        },
        "properties": {
            "OBJECTID": 2058,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2059,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15475257667015, 25.748149798574389]
        },
        "properties": {
            "OBJECTID": 2059,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2060,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1548055332488, 25.748115346446127]
        },
        "properties": {
            "OBJECTID": 2060,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2061,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15491832981712, 25.748056208827052]
        },
        "properties": {
            "OBJECTID": 2061,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2062,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15516454530757, 25.747980226006689]
        },
        "properties": {
            "OBJECTID": 2062,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2063,
        "geometry": {
            "type": "Point",
            "coordinates": [100.155511685416, 25.747973632177434]
        },
        "properties": {
            "OBJECTID": 2063,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2064,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15580420340132, 25.747644906587198]
        },
        "properties": {
            "OBJECTID": 2064,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2065,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15609783474736, 25.747373787070671]
        },
        "properties": {
            "OBJECTID": 2065,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2066,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15601283802209, 25.747101818594103]
        },
        "properties": {
            "OBJECTID": 2066,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2067,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15597517441483, 25.746857128353838]
        },
        "properties": {
            "OBJECTID": 2067,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2068,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15598354620374, 25.746739333353617]
        },
        "properties": {
            "OBJECTID": 2068,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2069,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15599409075475, 25.746681060882281]
        },
        "properties": {
            "OBJECTID": 2069,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2070,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15602767593657, 25.746567009759758]
        },
        "properties": {
            "OBJECTID": 2070,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2071,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15605056997788, 25.746511719440377]
        },
        "properties": {
            "OBJECTID": 2071,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2072,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15607741024434, 25.746457903109786]
        },
        "properties": {
            "OBJECTID": 2072,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2073,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15610807802545, 25.746405792793155]
        },
        "properties": {
            "OBJECTID": 2073,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2074,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15614244291947, 25.746355609723651]
        },
        "properties": {
            "OBJECTID": 2074,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2075,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15620941273426, 25.746274805637825]
        },
        "properties": {
            "OBJECTID": 2075,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2076,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15651504193568, 25.745952698158533]
        },
        "properties": {
            "OBJECTID": 2076,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2077,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1570926072381, 25.745460724033251]
        },
        "properties": {
            "OBJECTID": 2077,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2078,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15794584982115, 25.744469122552005]
        },
        "properties": {
            "OBJECTID": 2078,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2079,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15883892517633, 25.743319021855427]
        },
        "properties": {
            "OBJECTID": 2079,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2080,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1589088879349, 25.742947762829033]
        },
        "properties": {
            "OBJECTID": 2080,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2081,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15902286621235, 25.742672492941267]
        },
        "properties": {
            "OBJECTID": 2081,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2082,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15913644519082, 25.742528578031511]
        },
        "properties": {
            "OBJECTID": 2082,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2083,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1591809571355, 25.742485431257705]
        },
        "properties": {
            "OBJECTID": 2083,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2084,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15944458440009, 25.742313900865895]
        },
        "properties": {
            "OBJECTID": 2084,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2085,
        "geometry": {
            "type": "Point",
            "coordinates": [100.15989767813539, 25.742220716712779]
        },
        "properties": {
            "OBJECTID": 2085,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2086,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16008865996372, 25.74201319725131]
        },
        "properties": {
            "OBJECTID": 2086,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2087,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16032061400517, 25.741832035120353]
        },
        "properties": {
            "OBJECTID": 2087,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2088,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16061405919152, 25.741688365725565]
        },
        "properties": {
            "OBJECTID": 2088,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2089,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16120842203111, 25.74157489106841]
        },
        "properties": {
            "OBJECTID": 2089,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2090,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16178665463048, 25.741553747107787]
        },
        "properties": {
            "OBJECTID": 2090,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2091,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16208504698739, 25.741129374121385]
        },
        "properties": {
            "OBJECTID": 2091,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2092,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16223254839343, 25.74099697862772]
        },
        "properties": {
            "OBJECTID": 2092,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2093,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16236514713387, 25.74090072239045]
        },
        "properties": {
            "OBJECTID": 2093,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2094,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16277347711434, 25.740457951972928]
        },
        "properties": {
            "OBJECTID": 2094,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2095,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16301076413549, 25.740180609147842]
        },
        "properties": {
            "OBJECTID": 2095,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2096,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16320423618663, 25.740028190248836]
        },
        "properties": {
            "OBJECTID": 2096,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2097,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16421990622376, 25.739293450432456]
        },
        "properties": {
            "OBJECTID": 2097,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2098,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16422253674074, 25.739292464775474]
        },
        "properties": {
            "OBJECTID": 2098,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2099,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16501653468538, 25.739073894444743]
        },
        "properties": {
            "OBJECTID": 2099,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2100,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16534758502308, 25.73862799708445]
        },
        "properties": {
            "OBJECTID": 2100,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2101,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16532060266377, 25.73857424100845]
        },
        "properties": {
            "OBJECTID": 2101,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2102,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16529756113363, 25.738519001051088]
        },
        "properties": {
            "OBJECTID": 2102,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2103,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16524633934716, 25.738169977762311]
        },
        "properties": {
            "OBJECTID": 2103,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2104,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16531965118099, 25.737883586858118]
        },
        "properties": {
            "OBJECTID": 2104,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2105,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16537714843673, 25.737777656613616]
        },
        "properties": {
            "OBJECTID": 2105,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2106,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16541150613614, 25.737727472644792]
        },
        "properties": {
            "OBJECTID": 2106,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2107,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16542632156757, 25.737707898001133]
        },
        "properties": {
            "OBJECTID": 2107,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2108,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16594599930846, 25.737281792920442]
        },
        "properties": {
            "OBJECTID": 2108,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2109,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16606935481695, 25.737243712027748]
        },
        "properties": {
            "OBJECTID": 2109,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2110,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16647448141225, 25.737175011917316]
        },
        "properties": {
            "OBJECTID": 2110,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2111,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16700504184928, 25.737168430678594]
        },
        "properties": {
            "OBJECTID": 2111,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2112,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1671977854495, 25.737197027321031]
        },
        "properties": {
            "OBJECTID": 2112,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2113,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16726019929882, 25.737214097352762]
        },
        "properties": {
            "OBJECTID": 2113,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2114,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16732124617863, 25.73723482762523]
        },
        "properties": {
            "OBJECTID": 2114,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2115,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16765676075096, 25.737441113216846]
        },
        "properties": {
            "OBJECTID": 2115,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2116,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16799042002191, 25.737761465219478]
        },
        "properties": {
            "OBJECTID": 2116,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2117,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16886061662422, 25.73746243614255]
        },
        "properties": {
            "OBJECTID": 2117,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2118,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16988499479675, 25.737324591656602]
        },
        "properties": {
            "OBJECTID": 2118,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2119,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16988320784384, 25.737271723211506]
        },
        "properties": {
            "OBJECTID": 2119,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2120,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1699058096055, 25.737079446359417]
        },
        "properties": {
            "OBJECTID": 2120,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2121,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1698795431065, 25.737011948642589]
        },
        "properties": {
            "OBJECTID": 2121,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2122,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16983494842418, 25.736839723974128]
        },
        "properties": {
            "OBJECTID": 2122,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2123,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16982671333221, 25.736693487914181]
        },
        "properties": {
            "OBJECTID": 2123,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2124,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16956336395816, 25.736388496332097]
        },
        "properties": {
            "OBJECTID": 2124,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2125,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16952886416578, 25.736338389704997]
        },
        "properties": {
            "OBJECTID": 2125,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2126,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16944803130167, 25.736177354401434]
        },
        "properties": {
            "OBJECTID": 2126,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2127,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16940343661935, 25.736005127934334]
        },
        "properties": {
            "OBJECTID": 2127,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2128,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16939696509792, 25.735946368929831]
        },
        "properties": {
            "OBJECTID": 2128,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2129,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16944721201929, 25.735597232326484]
        },
        "properties": {
            "OBJECTID": 2129,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2130,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16952759072581, 25.735436008165323]
        },
        "properties": {
            "OBJECTID": 2130,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2131,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16966011212452, 25.73526538698809]
        },
        "properties": {
            "OBJECTID": 2131,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2132,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16970140809354, 25.735219693334216]
        },
        "properties": {
            "OBJECTID": 2132,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2133,
        "geometry": {
            "type": "Point",
            "coordinates": [100.16979344111445, 25.735136121135156]
        },
        "properties": {
            "OBJECTID": 2133,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2134,
        "geometry": {
            "type": "Point",
            "coordinates": [100.170423341564, 25.73474698178785]
        },
        "properties": {
            "OBJECTID": 2134,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2135,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17054367984701, 25.734701671245205]
        },
        "properties": {
            "OBJECTID": 2135,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2136,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17073382800385, 25.7346612799941]
        },
        "properties": {
            "OBJECTID": 2136,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2137,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17136536521951, 25.734612858696551]
        },
        "properties": {
            "OBJECTID": 2137,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2138,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17142883936953, 25.73459937516111]
        },
        "properties": {
            "OBJECTID": 2138,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2139,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17183572773672, 25.734602732330302]
        },
        "properties": {
            "OBJECTID": 2139,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2140,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17256648535505, 25.73419661018545]
        },
        "properties": {
            "OBJECTID": 2140,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2141,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17278111935326, 25.734131404840525]
        },
        "properties": {
            "OBJECTID": 2141,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2142,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17299974813994, 25.733820904011566]
        },
        "properties": {
            "OBJECTID": 2142,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2143,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17453808616381, 25.732262779104019]
        },
        "properties": {
            "OBJECTID": 2143,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2144,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17468045513897, 25.732141682692543]
        },
        "properties": {
            "OBJECTID": 2144,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2145,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17502886598942, 25.731985540600249]
        },
        "properties": {
            "OBJECTID": 2145,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2146,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17522151695948, 25.73195649519613]
        },
        "properties": {
            "OBJECTID": 2146,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2147,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17554457142489, 25.731984923665323]
        },
        "properties": {
            "OBJECTID": 2147,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2148,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17580808087831, 25.732087203561662]
        },
        "properties": {
            "OBJECTID": 2148,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2149,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17669958241987, 25.732310666204341]
        },
        "properties": {
            "OBJECTID": 2149,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2150,
        "geometry": {
            "type": "Point",
            "coordinates": [100.176764453217, 25.732304799027304]
        },
        "properties": {
            "OBJECTID": 2150,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2151,
        "geometry": {
            "type": "Point",
            "coordinates": [100.17825620186227, 25.732630992126644]
        },
        "properties": {
            "OBJECTID": 2151,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2152,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18076113032197, 25.731995746106861]
        },
        "properties": {
            "OBJECTID": 2152,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2153,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18340617135777, 25.731808906555841]
        },
        "properties": {
            "OBJECTID": 2153,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2154,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18389499156069, 25.731821881075007]
        },
        "properties": {
            "OBJECTID": 2154,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2155,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18511116724665, 25.731684089649093]
        },
        "properties": {
            "OBJECTID": 2155,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2156,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18578849164561, 25.731085097998914]
        },
        "properties": {
            "OBJECTID": 2156,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2157,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18594595843928, 25.730863567100016]
        },
        "properties": {
            "OBJECTID": 2157,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2158,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18597170423084, 25.730837913039295]
        },
        "properties": {
            "OBJECTID": 2158,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2159,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18601725759044, 25.730756531588725]
        },
        "properties": {
            "OBJECTID": 2159,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2160,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18602173171763, 25.730746539221457]
        },
        "properties": {
            "OBJECTID": 2160,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2161,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18624440025894, 25.730277780195422]
        },
        "properties": {
            "OBJECTID": 2161,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2162,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1862814793069, 25.730181177719203]
        },
        "properties": {
            "OBJECTID": 2162,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2163,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18630829888895, 25.730127357791389]
        },
        "properties": {
            "OBJECTID": 2163,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2164,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18654449503248, 25.729847728889638]
        },
        "properties": {
            "OBJECTID": 2164,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2165,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18659524377551, 25.729801866163257]
        },
        "properties": {
            "OBJECTID": 2165,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2166,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18652269276811, 25.729703815778521]
        },
        "properties": {
            "OBJECTID": 2166,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2167,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18649188109549, 25.729651779206279]
        },
        "properties": {
            "OBJECTID": 2167,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2168,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18639722655081, 25.729370575591645]
        },
        "properties": {
            "OBJECTID": 2168,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2169,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18638852111343, 25.729252805772433]
        },
        "properties": {
            "OBJECTID": 2169,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2170,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1864409389982, 25.728962683581017]
        },
        "properties": {
            "OBJECTID": 2170,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2171,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1864623671446, 25.728910585854919]
        },
        "properties": {
            "OBJECTID": 2171,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2172,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18661568806363, 25.72857027699672]
        },
        "properties": {
            "OBJECTID": 2172,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2173,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18672768243664, 25.728341995786536]
        },
        "properties": {
            "OBJECTID": 2173,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2174,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18700206919021, 25.7279978917893]
        },
        "properties": {
            "OBJECTID": 2174,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2175,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18725266168104, 25.727811071124108]
        },
        "properties": {
            "OBJECTID": 2175,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2176,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18749278786191, 25.727720616413364]
        },
        "properties": {
            "OBJECTID": 2176,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2177,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18755625391805, 25.727707125683366]
        },
        "properties": {
            "OBJECTID": 2177,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2178,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18781573620731, 25.727691385748926]
        },
        "properties": {
            "OBJECTID": 2178,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2179,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18812860585052, 25.72771577716145]
        },
        "properties": {
            "OBJECTID": 2179,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2180,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18813176337022, 25.727708757952882]
        },
        "properties": {
            "OBJECTID": 2180,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2181,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18818923184762, 25.727602822312463]
        },
        "properties": {
            "OBJECTID": 2181,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2182,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18828970770392, 25.727385459771199]
        },
        "properties": {
            "OBJECTID": 2182,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2183,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18833957241338, 25.727275892668388]
        },
        "properties": {
            "OBJECTID": 2183,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2184,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18840456641766, 25.727173589389679]
        },
        "properties": {
            "OBJECTID": 2184,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2185,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1884837463279, 25.727079843160311]
        },
        "properties": {
            "OBJECTID": 2185,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2186,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18853045081971, 25.727034688200376]
        },
        "properties": {
            "OBJECTID": 2186,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2187,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18851324768826, 25.727012185364117]
        },
        "properties": {
            "OBJECTID": 2187,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2188,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18847874429866, 25.726962085032255]
        },
        "properties": {
            "OBJECTID": 2188,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2189,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18837888088069, 25.726744581297453]
        },
        "properties": {
            "OBJECTID": 2189,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2190,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18837238058097, 25.72672152268018]
        },
        "properties": {
            "OBJECTID": 2190,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2191,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18833430598352, 25.726648093035124]
        },
        "properties": {
            "OBJECTID": 2191,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2192,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18826663919413, 25.726420642798473]
        },
        "properties": {
            "OBJECTID": 2192,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2193,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18875950544685, 25.724777412347748]
        },
        "properties": {
            "OBJECTID": 2193,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2194,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18885151508539, 25.724693830256115]
        },
        "properties": {
            "OBJECTID": 2194,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2195,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18937797461388, 25.724476937160944]
        },
        "properties": {
            "OBJECTID": 2195,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2196,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18970236277386, 25.724486135426844]
        },
        "properties": {
            "OBJECTID": 2196,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2197,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18976587109813, 25.724499459782294]
        },
        "properties": {
            "OBJECTID": 2197,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2198,
        "geometry": {
            "type": "Point",
            "coordinates": [100.18982827775289, 25.724516520820828]
        },
        "properties": {
            "OBJECTID": 2198,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2199,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1897987323257, 25.724260234722124]
        },
        "properties": {
            "OBJECTID": 2199,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2200,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19008112304545, 25.723316989787293]
        },
        "properties": {
            "OBJECTID": 2200,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2201,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19014257641885, 25.722776455868768]
        },
        "properties": {
            "OBJECTID": 2201,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2202,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19019899538739, 25.72260710543361]
        },
        "properties": {
            "OBJECTID": 2202,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2203,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19028603177503, 25.722394743122948]
        },
        "properties": {
            "OBJECTID": 2203,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2204,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1903362112472, 25.722045604720904]
        },
        "properties": {
            "OBJECTID": 2204,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2205,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19035908460415, 25.721990311703564]
        },
        "properties": {
            "OBJECTID": 2205,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2206,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19055217084605, 25.721626013429841]
        },
        "properties": {
            "OBJECTID": 2206,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2207,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19076689297776, 25.721377409339993]
        },
        "properties": {
            "OBJECTID": 2207,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2208,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19099770398094, 25.719943825247412]
        },
        "properties": {
            "OBJECTID": 2208,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2209,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19107891006371, 25.719732115844522]
        },
        "properties": {
            "OBJECTID": 2209,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2210,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19107811955962, 25.71960514775941]
        },
        "properties": {
            "OBJECTID": 2210,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2211,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19112848429216, 25.719374043577318]
        },
        "properties": {
            "OBJECTID": 2211,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2212,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19117817363389, 25.719264928833468]
        },
        "properties": {
            "OBJECTID": 2212,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2213,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1912810551766, 25.719114575677168]
        },
        "properties": {
            "OBJECTID": 2213,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2214,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19136682621814, 25.719025719961053]
        },
        "properties": {
            "OBJECTID": 2214,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2215,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19219823775853, 25.718236136788335]
        },
        "properties": {
            "OBJECTID": 2215,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2216,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19225116915618, 25.71820167386818]
        },
        "properties": {
            "OBJECTID": 2216,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2217,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19230647296541, 25.7181704188298]
        },
        "properties": {
            "OBJECTID": 2217,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2218,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19243007668678, 25.718115515218926]
        },
        "properties": {
            "OBJECTID": 2218,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2219,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19253122703458, 25.717565290206039]
        },
        "properties": {
            "OBJECTID": 2219,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2220,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19262886103314, 25.717108298808967]
        },
        "properties": {
            "OBJECTID": 2220,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2221,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19265173259146, 25.717053003992987]
        },
        "properties": {
            "OBJECTID": 2221,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2222,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19341525880719, 25.715687013844672]
        },
        "properties": {
            "OBJECTID": 2222,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2223,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19390441625586, 25.714871547284133]
        },
        "properties": {
            "OBJECTID": 2223,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2224,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19393875686819, 25.714821357919391]
        },
        "properties": {
            "OBJECTID": 2224,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2225,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19397664710465, 25.714773309840439]
        },
        "properties": {
            "OBJECTID": 2225,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2226,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1940179241879, 25.714727608992007]
        },
        "properties": {
            "OBJECTID": 2226,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2227,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19406241275021, 25.714684452325685]
        },
        "properties": {
            "OBJECTID": 2227,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2228,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19416024909629, 25.714606497292095]
        },
        "properties": {
            "OBJECTID": 2228,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2229,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19450857361187, 25.714450311133021]
        },
        "properties": {
            "OBJECTID": 2229,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2230,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19488674033084, 25.71391502026205]
        },
        "properties": {
            "OBJECTID": 2230,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2231,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19492107914454, 25.713864829997931]
        },
        "properties": {
            "OBJECTID": 2231,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2232,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19500599403148, 25.713750670956756]
        },
        "properties": {
            "OBJECTID": 2232,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2233,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19504084995532, 25.713706722887025]
        },
        "properties": {
            "OBJECTID": 2233,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2234,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19575911419184, 25.71296070467281]
        },
        "properties": {
            "OBJECTID": 2234,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2235,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19591939766212, 25.712789346051522]
        },
        "properties": {
            "OBJECTID": 2235,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2236,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19601844719364, 25.712594832585523]
        },
        "properties": {
            "OBJECTID": 2236,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2237,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19605940411827, 25.712516953094962]
        },
        "properties": {
            "OBJECTID": 2237,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2238,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19810381493915, 25.70959795608087]
        },
        "properties": {
            "OBJECTID": 2238,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2239,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1983946538902, 25.709208348186792]
        },
        "properties": {
            "OBJECTID": 2239,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2240,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19846338457762, 25.70901382123094]
        },
        "properties": {
            "OBJECTID": 2240,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2241,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19906528373593, 25.708139539008585]
        },
        "properties": {
            "OBJECTID": 2241,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2242,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19915498571407, 25.708005817014907]
        },
        "properties": {
            "OBJECTID": 2242,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2243,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1993329282721, 25.707761820151916]
        },
        "properties": {
            "OBJECTID": 2243,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2244,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19948123097407, 25.707621370330003]
        },
        "properties": {
            "OBJECTID": 2244,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2245,
        "geometry": {
            "type": "Point",
            "coordinates": [100.19967215344718, 25.707387266908881]
        },
        "properties": {
            "OBJECTID": 2245,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2246,
        "geometry": {
            "type": "Point",
            "coordinates": [100.1998552131476, 25.707228406166053]
        },
        "properties": {
            "OBJECTID": 2246,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2247,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20013899691725, 25.706872805235832]
        },
        "properties": {
            "OBJECTID": 2247,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2248,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2001425411454, 25.706869153088974]
        },
        "properties": {
            "OBJECTID": 2248,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2249,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20056599412538, 25.706061194068923]
        },
        "properties": {
            "OBJECTID": 2249,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2250,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20059280561355, 25.706007372342413]
        },
        "properties": {
            "OBJECTID": 2250,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2251,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20065778343002, 25.705905060070506]
        },
        "properties": {
            "OBJECTID": 2251,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2252,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20282436625155, 25.702493591606014]
        },
        "properties": {
            "OBJECTID": 2252,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2253,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20286884941794, 25.702450432241733]
        },
        "properties": {
            "OBJECTID": 2253,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2254,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20301959557793, 25.702338004395415]
        },
        "properties": {
            "OBJECTID": 2254,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2255,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20307489578988, 25.702306746659076]
        },
        "properties": {
            "OBJECTID": 2255,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2256,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20313233099233, 25.702278832602076]
        },
        "properties": {
            "OBJECTID": 2256,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2257,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20327849870381, 25.702224032413199]
        },
        "properties": {
            "OBJECTID": 2257,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2258,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2037603959256, 25.701295521071472]
        },
        "properties": {
            "OBJECTID": 2258,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2259,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20380166941158, 25.701249818424458]
        },
        "properties": {
            "OBJECTID": 2259,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2260,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20453945613104, 25.699092829570475]
        },
        "properties": {
            "OBJECTID": 2260,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2261,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20494047822058, 25.69855592081916]
        },
        "properties": {
            "OBJECTID": 2261,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2262,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20498798041103, 25.698515489997931]
        },
        "properties": {
            "OBJECTID": 2262,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2263,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20530568301092, 25.697346327271248]
        },
        "properties": {
            "OBJECTID": 2263,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2264,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20539544704229, 25.696911951125685]
        },
        "properties": {
            "OBJECTID": 2264,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2265,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20541831230531, 25.696856655410386]
        },
        "properties": {
            "OBJECTID": 2265,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2266,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20562644510443, 25.696556575025966]
        },
        "properties": {
            "OBJECTID": 2266,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2267,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20565782694723, 25.696411181630765]
        },
        "properties": {
            "OBJECTID": 2267,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2268,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20571357592087, 25.696232371226984]
        },
        "properties": {
            "OBJECTID": 2268,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2269,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20572843991562, 25.696188914187076]
        },
        "properties": {
            "OBJECTID": 2269,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2270,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20577676858306, 25.69600215107846]
        },
        "properties": {
            "OBJECTID": 2270,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2271,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20580199096918, 25.695886346277916]
        },
        "properties": {
            "OBJECTID": 2271,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2272,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20582081557825, 25.695829813095429]
        },
        "properties": {
            "OBJECTID": 2272,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2273,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20584368084127, 25.695774515581491]
        },
        "properties": {
            "OBJECTID": 2273,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2274,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20601460688863, 25.695524623662493]
        },
        "properties": {
            "OBJECTID": 2274,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2275,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20605908735706, 25.695481463398892]
        },
        "properties": {
            "OBJECTID": 2275,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2276,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20626511574255, 25.695337773319636]
        },
        "properties": {
            "OBJECTID": 2276,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2277,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20632254734772, 25.695309856564677]
        },
        "properties": {
            "OBJECTID": 2277,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2278,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2063818711265, 25.695285404897561]
        },
        "properties": {
            "OBJECTID": 2278,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2279,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2064428325707, 25.695264519941702]
        },
        "properties": {
            "OBJECTID": 2279,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2280,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20650829242379, 25.695246627929578]
        },
        "properties": {
            "OBJECTID": 2280,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2281,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20648922679646, 25.695190027297997]
        },
        "properties": {
            "OBJECTID": 2281,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2282,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20645712909328, 25.695015535438642]
        },
        "properties": {
            "OBJECTID": 2282,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2283,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20645693124243, 25.694897497621469]
        },
        "properties": {
            "OBJECTID": 2283,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2284,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20649524955616, 25.694651022226935]
        },
        "properties": {
            "OBJECTID": 2284,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2285,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20658661707876, 25.694304663629396]
        },
        "properties": {
            "OBJECTID": 2285,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2286,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20650223459052, 25.694043940275662]
        },
        "properties": {
            "OBJECTID": 2286,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2287,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20650060771692, 25.694032295853845]
        },
        "properties": {
            "OBJECTID": 2287,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2288,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20647460112201, 25.693825500346634]
        },
        "properties": {
            "OBJECTID": 2288,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2289,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20647440237184, 25.693707461630083]
        },
        "properties": {
            "OBJECTID": 2289,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2290,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20648412224449, 25.693629680165657]
        },
        "properties": {
            "OBJECTID": 2290,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2291,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20646763317478, 25.693614188444087]
        },
        "properties": {
            "OBJECTID": 2291,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2292,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20639313963176, 25.69361239969254]
        },
        "properties": {
            "OBJECTID": 2292,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2293,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20632827063326, 25.693606700688747]
        },
        "properties": {
            "OBJECTID": 2293,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2294,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20630472278481, 25.693603664577495]
        },
        "properties": {
            "OBJECTID": 2294,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2295,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2055472166308, 25.693109825258261]
        },
        "properties": {
            "OBJECTID": 2295,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2296,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20546327121303, 25.692886871631856]
        },
        "properties": {
            "OBJECTID": 2296,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2297,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20544881461115, 25.692799581635427]
        },
        "properties": {
            "OBJECTID": 2297,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2298,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2054164057426, 25.692506941342344]
        },
        "properties": {
            "OBJECTID": 2298,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2299,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20541366101173, 25.692477226842584]
        },
        "properties": {
            "OBJECTID": 2299,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2300,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20540831274349, 25.69191445358922]
        },
        "properties": {
            "OBJECTID": 2300,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2301,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20537185962371, 25.691667255139748]
        },
        "properties": {
            "OBJECTID": 2301,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2302,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20528581608761, 25.69157862785147]
        },
        "properties": {
            "OBJECTID": 2302,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2303,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20515547914209, 25.691374797409026]
        },
        "properties": {
            "OBJECTID": 2303,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2304,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20507909342547, 25.691029571957245]
        },
        "properties": {
            "OBJECTID": 2304,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2305,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20511317863037, 25.690258836775968]
        },
        "properties": {
            "OBJECTID": 2305,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2306,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20519494858809, 25.689756263241009]
        },
        "properties": {
            "OBJECTID": 2306,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2307,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20519857465456, 25.689727383312118]
        },
        "properties": {
            "OBJECTID": 2307,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2308,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20522379614135, 25.689611577612254]
        },
        "properties": {
            "OBJECTID": 2308,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2309,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20530400307734, 25.689147577798508]
        },
        "properties": {
            "OBJECTID": 2309,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2310,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20529674285046, 25.689083669275988]
        },
        "properties": {
            "OBJECTID": 2310,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2311,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20530880995369, 25.688677447306418]
        },
        "properties": {
            "OBJECTID": 2311,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2312,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20536431611038, 25.688482125349879]
        },
        "properties": {
            "OBJECTID": 2312,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2313,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20556467966628, 25.68798604761804]
        },
        "properties": {
            "OBJECTID": 2313,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2314,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20564163555309, 25.687627256792496]
        },
        "properties": {
            "OBJECTID": 2314,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2315,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20564401515924, 25.687528870960705]
        },
        "properties": {
            "OBJECTID": 2315,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2316,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20484487129386, 25.687135041648901]
        },
        "properties": {
            "OBJECTID": 2316,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2317,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20480683176993, 25.687087094294043]
        },
        "properties": {
            "OBJECTID": 2317,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2318,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20486892096397, 25.6857824172248]
        },
        "properties": {
            "OBJECTID": 2318,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2319,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20516976577215, 25.685443633616842]
        },
        "properties": {
            "OBJECTID": 2319,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2320,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20522268098199, 25.685409165300769]
        },
        "properties": {
            "OBJECTID": 2320,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2321,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20533539571198, 25.68534999170879]
        },
        "properties": {
            "OBJECTID": 2321,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2322,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20539471589348, 25.685325539142355]
        },
        "properties": {
            "OBJECTID": 2322,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2323,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20539910278643, 25.685323897879641]
        },
        "properties": {
            "OBJECTID": 2323,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2324,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20554857100979, 25.684975324251866]
        },
        "properties": {
            "OBJECTID": 2324,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2325,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20561654087072, 25.684794037115182]
        },
        "properties": {
            "OBJECTID": 2325,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2326,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20564782109011, 25.684593803960922]
        },
        "properties": {
            "OBJECTID": 2326,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2327,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20571195174523, 25.684369047193798]
        },
        "properties": {
            "OBJECTID": 2327,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2328,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2057804423136, 25.684203391173639]
        },
        "properties": {
            "OBJECTID": 2328,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2329,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20581107502113, 25.684151271863755]
        },
        "properties": {
            "OBJECTID": 2329,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2330,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20584540304299, 25.684101078002413]
        },
        "properties": {
            "OBJECTID": 2330,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2331,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20597127485451, 25.683955514635329]
        },
        "properties": {
            "OBJECTID": 2331,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2332,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20602094171318, 25.683846392696921]
        },
        "properties": {
            "OBJECTID": 2332,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2333,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20605802255977, 25.683784268429292]
        },
        "properties": {
            "OBJECTID": 2333,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2334,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20678263780854, 25.682847954569525]
        },
        "properties": {
            "OBJECTID": 2334,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2335,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20692128988566, 25.682743386797938]
        },
        "properties": {
            "OBJECTID": 2335,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2336,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20693722767101, 25.682571870795243]
        },
        "properties": {
            "OBJECTID": 2336,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2337,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20700778757941, 25.682278743968936]
        },
        "properties": {
            "OBJECTID": 2337,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2338,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20707604972, 25.682113859567039]
        },
        "properties": {
            "OBJECTID": 2338,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2339,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20713246329262, 25.682024041576312]
        },
        "properties": {
            "OBJECTID": 2339,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2340,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20724191078563, 25.681765844418237]
        },
        "properties": {
            "OBJECTID": 2340,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2341,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2074170420629, 25.68102839134724]
        },
        "properties": {
            "OBJECTID": 2341,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2342,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20737242130025, 25.680856174772714]
        },
        "properties": {
            "OBJECTID": 2342,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2343,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20737202379991, 25.680620604557305]
        },
        "properties": {
            "OBJECTID": 2343,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2344,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20750388959289, 25.68013369001784]
        },
        "properties": {
            "OBJECTID": 2344,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2345,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2075434138975, 25.679973756383788]
        },
        "properties": {
            "OBJECTID": 2345,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2346,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20777417363928, 25.679446992884436]
        },
        "properties": {
            "OBJECTID": 2346,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2347,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20781543723268, 25.679401289338102]
        },
        "properties": {
            "OBJECTID": 2347,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2348,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20818265020921, 25.679162066975834]
        },
        "properties": {
            "OBJECTID": 2348,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2349,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2083926607927, 25.678830148792372]
        },
        "properties": {
            "OBJECTID": 2349,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2350,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20842795918304, 25.678724026092993]
        },
        "properties": {
            "OBJECTID": 2350,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2351,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20798836696963, 25.678236303962308]
        },
        "properties": {
            "OBJECTID": 2351,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2352,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20788851614219, 25.678018802925465]
        },
        "properties": {
            "OBJECTID": 2352,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2353,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20785465396915, 25.677811500200619]
        },
        "properties": {
            "OBJECTID": 2353,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2354,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20772933973728, 25.676271385116365]
        },
        "properties": {
            "OBJECTID": 2354,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2355,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20782109756567, 25.676115246621407]
        },
        "properties": {
            "OBJECTID": 2355,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2356,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20797219985718, 25.675780175414786]
        },
        "properties": {
            "OBJECTID": 2356,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2357,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20800869434584, 25.675679969355144]
        },
        "properties": {
            "OBJECTID": 2357,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2358,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20801103168384, 25.675490877002403]
        },
        "properties": {
            "OBJECTID": 2358,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2359,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20798542618655, 25.675375143248289]
        },
        "properties": {
            "OBJECTID": 2359,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2360,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20798502778689, 25.675139573032936]
        },
        "properties": {
            "OBJECTID": 2360,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2361,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20806219771231, 25.674660595010494]
        },
        "properties": {
            "OBJECTID": 2361,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2362,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20808101602614, 25.674604060029424]
        },
        "properties": {
            "OBJECTID": 2362,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2363,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20810387769188, 25.674548762515428]
        },
        "properties": {
            "OBJECTID": 2363,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2364,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20832923520612, 25.674104456955149]
        },
        "properties": {
            "OBJECTID": 2364,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2365,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2084733245843, 25.673908386762662]
        },
        "properties": {
            "OBJECTID": 2365,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2366,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20862708167454, 25.673779780112795]
        },
        "properties": {
            "OBJECTID": 2366,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2367,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20871062059871, 25.673277576199212]
        },
        "properties": {
            "OBJECTID": 2367,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2368,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20884729326804, 25.671499810767727]
        },
        "properties": {
            "OBJECTID": 2368,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2369,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20884840932666, 25.671470932637476]
        },
        "properties": {
            "OBJECTID": 2369,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2370,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20883696815162, 25.670000840566388]
        },
        "properties": {
            "OBJECTID": 2370,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2371,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20886946335509, 25.669809829959718]
        },
        "properties": {
            "OBJECTID": 2371,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2372,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20891622270557, 25.669687791058777]
        },
        "properties": {
            "OBJECTID": 2372,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2373,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20885311368033, 25.669629104000023]
        },
        "properties": {
            "OBJECTID": 2373,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2374,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20873916957709, 25.669485470578081]
        },
        "properties": {
            "OBJECTID": 2374,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2375,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20865833761223, 25.66932445326097]
        },
        "properties": {
            "OBJECTID": 2375,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2376,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20859876472127, 25.669049144702342]
        },
        "properties": {
            "OBJECTID": 2376,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2377,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20860288181763, 25.66888700143403]
        },
        "properties": {
            "OBJECTID": 2377,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2378,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20862606633995, 25.668686163935376]
        },
        "properties": {
            "OBJECTID": 2378,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2379,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20875090123315, 25.667931270311954]
        },
        "properties": {
            "OBJECTID": 2379,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2380,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20878655665433, 25.667738348645912]
        },
        "properties": {
            "OBJECTID": 2380,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2381,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2087969582131, 25.667615742272744]
        },
        "properties": {
            "OBJECTID": 2381,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2382,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20866560593305, 25.667218791414143]
        },
        "properties": {
            "OBJECTID": 2382,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2383,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20871947532356, 25.66679343367008]
        },
        "properties": {
            "OBJECTID": 2383,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2384,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20877585202402, 25.666624073342348]
        },
        "properties": {
            "OBJECTID": 2384,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2385,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20915897490681, 25.666208928299454]
        },
        "properties": {
            "OBJECTID": 2385,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2386,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2092625066594, 25.666026008893255]
        },
        "properties": {
            "OBJECTID": 2386,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2387,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20948389816346, 25.665811126682229]
        },
        "properties": {
            "OBJECTID": 2387,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2388,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20975104807246, 25.665462816555817]
        },
        "properties": {
            "OBJECTID": 2388,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2389,
        "geometry": {
            "type": "Point",
            "coordinates": [100.20971927682325, 25.665239164156162]
        },
        "properties": {
            "OBJECTID": 2389,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2390,
        "geometry": {
            "type": "Point",
            "coordinates": [100.210384455878, 25.663731066735409]
        },
        "properties": {
            "OBJECTID": 2390,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2391,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21025900674783, 25.663397832843771]
        },
        "properties": {
            "OBJECTID": 2391,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2392,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21023686993567, 25.663077997051971]
        },
        "properties": {
            "OBJECTID": 2392,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2393,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21025425293146, 25.662854625240811]
        },
        "properties": {
            "OBJECTID": 2393,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2394,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2103571173871, 25.662529386322149]
        },
        "properties": {
            "OBJECTID": 2394,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2395,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21046222834923, 25.662003935833013]
        },
        "properties": {
            "OBJECTID": 2395,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2396,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21058481403799, 25.661715054706065]
        },
        "properties": {
            "OBJECTID": 2396,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2397,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21066695181844, 25.661590725231861]
        },
        "properties": {
            "OBJECTID": 2397,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2398,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21075165806263, 25.661433725186328]
        },
        "properties": {
            "OBJECTID": 2398,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2399,
        "geometry": {
            "type": "Point",
            "coordinates": [100.210752452164, 25.6614322565934]
        },
        "properties": {
            "OBJECTID": 2399,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2400,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2107592636292, 25.661419926888186]
        },
        "properties": {
            "OBJECTID": 2400,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2401,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21090062356501, 25.661171683426517]
        },
        "properties": {
            "OBJECTID": 2401,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2402,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21105953017326, 25.659707127581896]
        },
        "properties": {
            "OBJECTID": 2402,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2403,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21107239677377, 25.659639578603731]
        },
        "properties": {
            "OBJECTID": 2403,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2404,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21110340180059, 25.659523375403523]
        },
        "properties": {
            "OBJECTID": 2404,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2405,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21118312400205, 25.659293439440717]
        },
        "properties": {
            "OBJECTID": 2405,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2406,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21119700413851, 25.659258567329061]
        },
        "properties": {
            "OBJECTID": 2406,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2407,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21123958613805, 25.659158854097882]
        },
        "properties": {
            "OBJECTID": 2407,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2408,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21131082773258, 25.659014734142715]
        },
        "properties": {
            "OBJECTID": 2408,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2409,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2114316453542, 25.658818836620071]
        },
        "properties": {
            "OBJECTID": 2409,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2410,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21156051010951, 25.658294985125508]
        },
        "properties": {
            "OBJECTID": 2410,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2411,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21155722128879, 25.658011153691859]
        },
        "properties": {
            "OBJECTID": 2411,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2412,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21155926274986, 25.657987217336256]
        },
        "properties": {
            "OBJECTID": 2412,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2413,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2115839401468, 25.657740770720068]
        },
        "properties": {
            "OBJECTID": 2413,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2414,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21157918722975, 25.657533706315519]
        },
        "properties": {
            "OBJECTID": 2414,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2415,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21161023992067, 25.657193366880449]
        },
        "properties": {
            "OBJECTID": 2415,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2416,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21164792960832, 25.656997141105251]
        },
        "properties": {
            "OBJECTID": 2416,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2417,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21174172979704, 25.656149520184272]
        },
        "properties": {
            "OBJECTID": 2417,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2418,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21204134433265, 25.654947264204907]
        },
        "properties": {
            "OBJECTID": 2418,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2419,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21203022961146, 25.654705673028445]
        },
        "properties": {
            "OBJECTID": 2419,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2420,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21203557518169, 25.654653651744695]
        },
        "properties": {
            "OBJECTID": 2420,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2421,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21206748222858, 25.654437176834278]
        },
        "properties": {
            "OBJECTID": 2421,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2422,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21206977819776, 25.65442693715346]
        },
        "properties": {
            "OBJECTID": 2422,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2423,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21213868515224, 25.654128617641618]
        },
        "properties": {
            "OBJECTID": 2423,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2424,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2121503646477, 25.654061592068842]
        },
        "properties": {
            "OBJECTID": 2424,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2425,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21207703482736, 25.653994254431325]
        },
        "properties": {
            "OBJECTID": 2425,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2426,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21203562644308, 25.653948665098881]
        },
        "properties": {
            "OBJECTID": 2426,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2427,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21195650498879, 25.653851131824297]
        },
        "properties": {
            "OBJECTID": 2427,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2428,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21189847083781, 25.653770582246636]
        },
        "properties": {
            "OBJECTID": 2428,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2429,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21185557317619, 25.653695591378494]
        },
        "properties": {
            "OBJECTID": 2429,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2430,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21178638563327, 25.653559399846472]
        },
        "properties": {
            "OBJECTID": 2430,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2431,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21160433856943, 25.653311856956691]
        },
        "properties": {
            "OBJECTID": 2431,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2432,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2115773580087, 25.65325810717593]
        },
        "properties": {
            "OBJECTID": 2432,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2433,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21142132203641, 25.652767363323221]
        },
        "properties": {
            "OBJECTID": 2433,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2434,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21138902828108, 25.652474832747373]
        },
        "properties": {
            "OBJECTID": 2434,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2435,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21144971543208, 25.652166307728976]
        },
        "properties": {
            "OBJECTID": 2435,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2436,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21125026468803, 25.651394862083293]
        },
        "properties": {
            "OBJECTID": 2436,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2437,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21123955196379, 25.651336614792967]
        },
        "properties": {
            "OBJECTID": 2437,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2438,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21123306605324, 25.651277857587104]
        },
        "properties": {
            "OBJECTID": 2438,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2439,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21123083303655, 25.651218842275796]
        },
        "properties": {
            "OBJECTID": 2439,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2440,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21123914996684, 25.651101043678295]
        },
        "properties": {
            "OBJECTID": 2440,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2441,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21127956460026, 25.650866613803259]
        },
        "properties": {
            "OBJECTID": 2441,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2442,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21128624476444, 25.650832490826815]
        },
        "properties": {
            "OBJECTID": 2442,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2443,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21131975620187, 25.650718427113759]
        },
        "properties": {
            "OBJECTID": 2443,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2444,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21136940597336, 25.650609304276031]
        },
        "properties": {
            "OBJECTID": 2444,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2445,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21155826000575, 25.650369767151062]
        },
        "properties": {
            "OBJECTID": 2445,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2446,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21174361387676, 25.650201403272206]
        },
        "properties": {
            "OBJECTID": 2446,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2447,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21179076083502, 25.650161271925185]
        },
        "properties": {
            "OBJECTID": 2447,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2448,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21212687525514, 25.649984752094667]
        },
        "properties": {
            "OBJECTID": 2448,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2449,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21235276336938, 25.649941059432422]
        },
        "properties": {
            "OBJECTID": 2449,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2450,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21236524236213, 25.649729831166837]
        },
        "properties": {
            "OBJECTID": 2450,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2451,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21226568471366, 25.649584211142496]
        },
        "properties": {
            "OBJECTID": 2451,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2452,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21218176177894, 25.64936126021405]
        },
        "properties": {
            "OBJECTID": 2452,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2453,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21216456314414, 25.649244254818541]
        },
        "properties": {
            "OBJECTID": 2453,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2454,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21231911973166, 25.648529675104498]
        },
        "properties": {
            "OBJECTID": 2454,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2455,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21243289925894, 25.648254365646494]
        },
        "properties": {
            "OBJECTID": 2455,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2456,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21246721469026, 25.648204171785153]
        },
        "properties": {
            "OBJECTID": 2456,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2457,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21281842513025, 25.647815215000207]
        },
        "properties": {
            "OBJECTID": 2457,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2458,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21288182013996, 25.647774806662028]
        },
        "properties": {
            "OBJECTID": 2458,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2459,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21292255133471, 25.647288387649041]
        },
        "properties": {
            "OBJECTID": 2459,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2460,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21294048471566, 25.646794755173858]
        },
        "properties": {
            "OBJECTID": 2460,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2461,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21296003507763, 25.646277573051634]
        },
        "properties": {
            "OBJECTID": 2461,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2462,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21297284681947, 25.646167360235552]
        },
        "properties": {
            "OBJECTID": 2462,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2463,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21297869511073, 25.64609755305969]
        },
        "properties": {
            "OBJECTID": 2463,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2464,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2130101246176, 25.645842526911167]
        },
        "properties": {
            "OBJECTID": 2464,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2465,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21302747074122, 25.645723946802775]
        },
        "properties": {
            "OBJECTID": 2465,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2466,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21305530026194, 25.645587891967693]
        },
        "properties": {
            "OBJECTID": 2466,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2467,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21334594316079, 25.644000845762491]
        },
        "properties": {
            "OBJECTID": 2467,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2468,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21355092203748, 25.643563137628746]
        },
        "properties": {
            "OBJECTID": 2468,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2469,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21377765101784, 25.642840038636905]
        },
        "properties": {
            "OBJECTID": 2469,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2470,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21393195399656, 25.642554160346265]
        },
        "properties": {
            "OBJECTID": 2470,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2471,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21407599930797, 25.642358084757859]
        },
        "properties": {
            "OBJECTID": 2471,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2472,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21444149727733, 25.642119453250245]
        },
        "properties": {
            "OBJECTID": 2472,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2473,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21451790008109, 25.642007046987658]
        },
        "properties": {
            "OBJECTID": 2473,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2474,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21464497788349, 25.64175636006803]
        },
        "properties": {
            "OBJECTID": 2474,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2475,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21500775900091, 25.640957631689389]
        },
        "properties": {
            "OBJECTID": 2475,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2476,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21525367141987, 25.640473106648585]
        },
        "properties": {
            "OBJECTID": 2476,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2477,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21526171315759, 25.640458596986718]
        },
        "properties": {
            "OBJECTID": 2477,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2478,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21555313936597, 25.639944378130622]
        },
        "properties": {
            "OBJECTID": 2478,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2479,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21583950508915, 25.639495730643489]
        },
        "properties": {
            "OBJECTID": 2479,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2480,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21585287171274, 25.639480589657524]
        },
        "properties": {
            "OBJECTID": 2480,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2481,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21553271576227, 25.638650632320889]
        },
        "properties": {
            "OBJECTID": 2481,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2482,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21530305858937, 25.638485807274265]
        },
        "properties": {
            "OBJECTID": 2482,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2483,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21513779287488, 25.638427513219199]
        },
        "properties": {
            "OBJECTID": 2483,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2484,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21470888460738, 25.638168610093317]
        },
        "properties": {
            "OBJECTID": 2484,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2485,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21445790540804, 25.637822712847992]
        },
        "properties": {
            "OBJECTID": 2485,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2486,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21431348957594, 25.637411710981723]
        },
        "properties": {
            "OBJECTID": 2486,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2487,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21431125835795, 25.637349474298844]
        },
        "properties": {
            "OBJECTID": 2487,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2488,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21431362897084, 25.637138430394316]
        },
        "properties": {
            "OBJECTID": 2488,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2489,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21431991343331, 25.637079654302681]
        },
        "properties": {
            "OBJECTID": 2489,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2490,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21433948717765, 25.636983938557989]
        },
        "properties": {
            "OBJECTID": 2490,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2491,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21437556977679, 25.636848674226997]
        },
        "properties": {
            "OBJECTID": 2491,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2492,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21445810505759, 25.636603251938595]
        },
        "properties": {
            "OBJECTID": 2492,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2493,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21499593291605, 25.635892006011318]
        },
        "properties": {
            "OBJECTID": 2493,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2494,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21516494340744, 25.635346331567973]
        },
        "properties": {
            "OBJECTID": 2494,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2495,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2151955590279, 25.635294209560186]
        },
        "properties": {
            "OBJECTID": 2495,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2496,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21551460701357, 25.63482486327689]
        },
        "properties": {
            "OBJECTID": 2496,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2497,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21553310876601, 25.634798710991788]
        },
        "properties": {
            "OBJECTID": 2497,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2498,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21557096932486, 25.634750656617541]
        },
        "properties": {
            "OBJECTID": 2498,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2499,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21561221403249, 25.634704951272511]
        },
        "properties": {
            "OBJECTID": 2499,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2500,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21580732195042, 25.634549348773476]
        },
        "properties": {
            "OBJECTID": 2500,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2501,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2160402076895, 25.634444825967989]
        },
        "properties": {
            "OBJECTID": 2501,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2502,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21616592931423, 25.634414096133696]
        },
        "properties": {
            "OBJECTID": 2502,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2503,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21649004677829, 25.634404012035588]
        },
        "properties": {
            "OBJECTID": 2503,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2504,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21661779727356, 25.634426856614198]
        },
        "properties": {
            "OBJECTID": 2504,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2505,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21680054221133, 25.634488908036701]
        },
        "properties": {
            "OBJECTID": 2505,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2506,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21706478281351, 25.634578261977254]
        },
        "properties": {
            "OBJECTID": 2506,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2507,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21720029535743, 25.634492071851696]
        },
        "properties": {
            "OBJECTID": 2507,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2508,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21738092419037, 25.634194714614466]
        },
        "properties": {
            "OBJECTID": 2508,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2509,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21760148022418, 25.633834911152292]
        },
        "properties": {
            "OBJECTID": 2509,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2510,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21762827012861, 25.633781084029863]
        },
        "properties": {
            "OBJECTID": 2510,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2511,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21765888574902, 25.633728962022019]
        },
        "properties": {
            "OBJECTID": 2511,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2512,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2176964962963, 25.633407830307192]
        },
        "properties": {
            "OBJECTID": 2512,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2513,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21772169619936, 25.633292022808632]
        },
        "properties": {
            "OBJECTID": 2513,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2514,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21789292801628, 25.632975991149124]
        },
        "properties": {
            "OBJECTID": 2514,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2515,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21793417092533, 25.632930284005454]
        },
        "properties": {
            "OBJECTID": 2515,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2516,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21797862531338, 25.632887121043836]
        },
        "properties": {
            "OBJECTID": 2516,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2517,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21803769818126, 25.632837577392309]
        },
        "properties": {
            "OBJECTID": 2517,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2518,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2185468619482, 25.632553372740006]
        },
        "properties": {
            "OBJECTID": 2518,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2519,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21872993873575, 25.632447197879912]
        },
        "properties": {
            "OBJECTID": 2519,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2520,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2187892301389, 25.632422740816878]
        },
        "properties": {
            "OBJECTID": 2520,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2521,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21900151600721, 25.632317255736837]
        },
        "properties": {
            "OBJECTID": 2521,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2522,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21910195139401, 25.632120136035496]
        },
        "properties": {
            "OBJECTID": 2522,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2523,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21898286676594, 25.631769418423971]
        },
        "properties": {
            "OBJECTID": 2523,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2524,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2188989447306, 25.631546470193484]
        },
        "properties": {
            "OBJECTID": 2524,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2525,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21888174159915, 25.631429465697352]
        },
        "properties": {
            "OBJECTID": 2525,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2526,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21887950678388, 25.631370450386044]
        },
        "properties": {
            "OBJECTID": 2526,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2527,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21891525483522, 25.631103671896994]
        },
        "properties": {
            "OBJECTID": 2527,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2528,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21914679608784, 25.630662888981192]
        },
        "properties": {
            "OBJECTID": 2528,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2529,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21928162604627, 25.630520672890782]
        },
        "properties": {
            "OBJECTID": 2529,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2530,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21953854436839, 25.630115768428027]
        },
        "properties": {
            "OBJECTID": 2530,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2531,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21984921516918, 25.629891512583299]
        },
        "properties": {
            "OBJECTID": 2531,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2532,
        "geometry": {
            "type": "Point",
            "coordinates": [100.21985584766929, 25.629888060085932]
        },
        "properties": {
            "OBJECTID": 2532,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2533,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22011784716096, 25.629680792434669]
        },
        "properties": {
            "OBJECTID": 2533,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2534,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22017073359257, 25.629646318722678]
        },
        "properties": {
            "OBJECTID": 2534,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2535,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22046590817519, 25.629524556812896]
        },
        "properties": {
            "OBJECTID": 2535,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2536,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22047873610484, 25.629374763034946]
        },
        "properties": {
            "OBJECTID": 2536,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2537,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22051057929986, 25.629194705271459]
        },
        "properties": {
            "OBJECTID": 2537,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2538,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22067579555159, 25.628705465085204]
        },
        "properties": {
            "OBJECTID": 2538,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2539,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22072291912747, 25.628556067008958]
        },
        "properties": {
            "OBJECTID": 2539,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2540,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22079802241086, 25.628359688349008]
        },
        "properties": {
            "OBJECTID": 2540,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2541,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22084504706135, 25.62825747320386]
        },
        "properties": {
            "OBJECTID": 2541,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2542,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22091894525317, 25.628121749319348]
        },
        "properties": {
            "OBJECTID": 2542,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2543,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2209486831353, 25.62807123709797]
        },
        "properties": {
            "OBJECTID": 2543,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2544,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22098299137201, 25.628021041437989]
        },
        "properties": {
            "OBJECTID": 2544,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2545,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22098455439374, 25.628018918138594]
        },
        "properties": {
            "OBJECTID": 2545,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2546,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22115639055505, 25.627809266384588]
        },
        "properties": {
            "OBJECTID": 2546,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2547,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2211681419962, 25.627798022161016]
        },
        "properties": {
            "OBJECTID": 2547,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2548,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22143584678696, 25.62753801736568]
        },
        "properties": {
            "OBJECTID": 2548,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2549,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22185512511402, 25.627162577391061]
        },
        "properties": {
            "OBJECTID": 2549,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2550,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22465151665421, 25.625886716398895]
        },
        "properties": {
            "OBJECTID": 2550,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2551,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22493509268043, 25.625256347402569]
        },
        "properties": {
            "OBJECTID": 2551,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2552,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22495885726551, 25.625240855681]
        },
        "properties": {
            "OBJECTID": 2552,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2553,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2251031337027, 25.625185022171081]
        },
        "properties": {
            "OBJECTID": 2553,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2554,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22512796128643, 25.624861718593479]
        },
        "properties": {
            "OBJECTID": 2554,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2555,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22506363727712, 25.624776418796728]
        },
        "properties": {
            "OBJECTID": 2555,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2556,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22505901835905, 25.624768613580727]
        },
        "properties": {
            "OBJECTID": 2556,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2557,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22504351224836, 25.624708769993788]
        },
        "properties": {
            "OBJECTID": 2557,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2558,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22504475691005, 25.624691100114205]
        },
        "properties": {
            "OBJECTID": 2558,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2559,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22538603523742, 25.623432012377521]
        },
        "properties": {
            "OBJECTID": 2559,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2560,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22538593811061, 25.623379196992403]
        },
        "properties": {
            "OBJECTID": 2560,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2561,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22538814234895, 25.623370568896689]
        },
        "properties": {
            "OBJECTID": 2561,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2562,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22564424408665, 25.622592659823454]
        },
        "properties": {
            "OBJECTID": 2562,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2563,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22565072460134, 25.622579328273446]
        },
        "properties": {
            "OBJECTID": 2563,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2564,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22606682292559, 25.62193281734568]
        },
        "properties": {
            "OBJECTID": 2564,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2565,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22717893356099, 25.618949127600217]
        },
        "properties": {
            "OBJECTID": 2565,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2566,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22726769215035, 25.61874784763512]
        },
        "properties": {
            "OBJECTID": 2566,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2567,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2270648977269, 25.618149052037154]
        },
        "properties": {
            "OBJECTID": 2567,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2568,
        "geometry": {
            "type": "Point",
            "coordinates": [100.2271916292903, 25.61794284918318]
        },
        "properties": {
            "OBJECTID": 2568,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2569,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22754001855708, 25.617445579849687]
        },
        "properties": {
            "OBJECTID": 2569,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2570,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22792641497216, 25.616728279686413]
        },
        "properties": {
            "OBJECTID": 2570,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2571,
        "geometry": {
            "type": "Point",
            "coordinates": [100.22899201986587, 25.614193334554784]
        },
        "properties": {
            "OBJECTID": 2571,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2572,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23007753125313, 25.612731648446868]
        },
        "properties": {
            "OBJECTID": 2572,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2573,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23013565443694, 25.612494005294138]
        },
        "properties": {
            "OBJECTID": 2573,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2574,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23098798600682, 25.609381298807932]
        },
        "properties": {
            "OBJECTID": 2574,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2575,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23156454406853, 25.608963766065017]
        },
        "properties": {
            "OBJECTID": 2575,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2576,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23153113515383, 25.60833863292163]
        },
        "properties": {
            "OBJECTID": 2576,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2577,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23129564138083, 25.605750095390647]
        },
        "properties": {
            "OBJECTID": 2577,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2578,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23069585742792, 25.604879774681933]
        },
        "properties": {
            "OBJECTID": 2578,
            "Id": 0,
            "ORIG_FID": 0
        }
    }, {
        "type": "Feature",
        "id": 2579,
        "geometry": {
            "type": "Point",
            "coordinates": [100.23006652085331, 25.603042421970258]
        },
        "properties": {
            "OBJECTID": 2579,
            "Id": 0,
            "ORIG_FID": 0
        }
    }]
}
export const generateRedLine = () => {
    src.features[0].geometry.coordinates[1]
    let coordGroup = src.features.map(o => {
        return {
            coord: `${o.geometry.coordinates[0]},${o.geometry.coordinates[1]}`,
            coord_z: 50
        }
    });
    return {
        id: 'path-tripleline-red',
        coord_type: 0,
        cad_mapkey: '',
        type: 'arrow',
        color: 'ff0000',
        width: 10,
        points: coordGroup
    };
}