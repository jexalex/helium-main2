// const roadPoints = [{
//         "coord": "100.212257,25.641445",value:1
//     },
//     {
//         "coord": "100.212738,25.641447",value:1
//     },
//     {
//         "coord": "100.213234,25.641453",value:1
//     },
//     {
//         "coord": "100.213722,25.641443",value:2
//     },
//     {
//         "coord": "100.214203,25.64146",value:2
//     },
//     {
//         "coord": "100.214531,25.641464",value:2
//     },
//     {
//         "coord": "100.214806,25.641466",value:3
//     },
//     {
//         "coord": "100.215118,25.641476",value:3
//     },
//     {
//         "coord": "100.215446,25.64146",value:4
//     },
//     {
//         "coord": "100.21566,25.641443",value:4
//     },
//     {
//         "coord": "100.215981,25.641464",value:5
//     }
// ]

// export const generateRiverHeatMapData = () => {
//     return {
//         "id": "heatmap_id",
//         "coord_type": 0, 
//         "cad_mapkey": "", 
//         "heatmap_type": 1,
//         "leftupper_coord": "100.211044,25.641373", 
//         "leftlower_coord": "100.216415,25.641386",
//         "rightlower_coord": "100.216423,25.641541",
//         "rightupper_coord": "100.211037,25.641525",
//         "coord_z": 10, 
//         "brush_diameter": 500,
//         "heatpoint_minvalue": 1,
//         "heatpoint_maxvalue": 100, 
//         "data": [{
//             "coord": "134.55,34.577", //热力数据点的坐标 lng,lat
//             "value": 40 //热力数据点的热力值
//         }],
//         "data": roadPoints.map((o, i) => {
//             return {
//                 coord: o.coord,
//                 value:o.value
//             }
//         })
//     }
// }
export const generateRiverHeatMapData = () => {
return {
    "id":"river_path_1",
    "coord_type":0,                    //坐标类型(0:经纬度坐标, 1:cad坐标)
    "cad_mapkey":"",                   //CAD基准点Key值, 项目中约定
    "type":"arrow",                    //样式类型; 注①
    "color":"ff0000",                  //颜色(HEX颜色值)
    "width":10,                        //宽度(米)
    "points":[
        {
            "coord":"100.20826,25.676386",   //路径坐标点 lng,lat
            "coord_z":5              //高度(米)
        },
        {
            "coord":"100.208382,25.676397",
            "coord_z":5
        },
        {
            "coord":"100.208481,25.676428",
            "coord_z":5
        },
        {
            "coord":"100.208549,25.676451",
            "coord_z":5
        },
        {
            "coord":"100.208633,25.676447",
            "coord_z":5
        },
        {
            "coord":"100.20874,25.676466",
            "coord_z":5
        },
        {
            "coord":"100.20887,25.676493",
            "coord_z":5
        },
        {
            "coord":"100.208954,25.676472",
            "coord_z":5
        },
        {
            "coord":"100.20903,25.676437",
            "coord_z":5
        },
        {
            "coord":"100.209145,25.676426",
            "coord_z":5
        },
        {
            "coord":"100.209221,25.676407",
            "coord_z":5
        },
        {
            "coord":"100.20929,25.67639",
            "coord_z":5
        },
        {
            "coord":"100.209381,25.676399",
            "coord_z":5
        },
        {
            "coord":"100.209465,25.676405",
            "coord_z":5
        },
        {
            "coord":"100.209518,25.676411",
            "coord_z":5
        }
    ]
}
}