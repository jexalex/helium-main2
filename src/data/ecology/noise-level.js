export default [

  {
    id: "noise_poi_1",
    coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    coord_z: 1, //高度(米)
    always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    state: "state_1", //与marker之中images中的define_state对应
    marker: {
      size: "46,73", //marker大小(宽,高 单位:像素)
      images: [{
        define_state: "state_1", //marker图片组
        normal_url: "http://localhost:3000/html/images/unormal_noise_marker.png",
        activate_url: "http://localhost:3000/html/images/unormal_noise_marker.png",
      }, ],
    },
    label: {
      bg_image_url: "http://localhost:3000/html/images/label_bg.png",
      size: "113,26", //label大小(宽,高 单位:像素)
      offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
      text: "苏武庄广场", //label中的文本内容
      font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
      text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
      text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    },
    window: {
      url: "http://localhost:3000/html/noise_wether_page.html?checkPosition=苏武庄广场&degree=14℃&noise=80&pmTen=45&pm=56&humidity=76&wind=东北风2级&NAI=14201&pressure=0.7",
      //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
      size: "420,205", //window大小(宽,高 单位:像素)
      offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    },
    "coord": "100.220055,25.628769"
  },
  {
    id: "noise_poi_2",
    coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    coord_z: 1, //高度(米)
    always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    state: "state_1", //与marker之中images中的define_state对应
    marker: {
      size: "46,73", //marker大小(宽,高 单位:像素)
      images: [{
        define_state: "state_1", //marker图片组
        normal_url: "http://localhost:3000/html/images/noise_marker.png",
        activate_url: "http://localhost:3000/html/images/noise_marker_click.png",
      }, ],
    },
    label: {
      bg_image_url: "http://localhost:3000/html/images/label_bg.png",
      size: "113,26", //label大小(宽,高 单位:像素)
      offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
      text: "洱滨村广场", //label中的文本内容
      font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
      text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
      text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    },
    window: {
      url: "http://localhost:3000/html/noise_wether_page.html?checkPosition=洱滨村广场&degree=14℃&noise=52&pmTen=45&pm=56&humidity=76&wind=东北风2级&NAI=14201&pressure=0.7",
      //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
      size: "420,205", //window大小(宽,高 单位:像素)
      offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    },
    "coord": "100.219872,25.631357"
  },
  {
    id: "noise_poi_3",
    coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
    cad_mapkey: "", //CAD基准点的Key值, 项目中约定
    coord_z: 1, //高度(米)
    always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
    show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
    sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
    //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
    state: "state_1", //与marker之中images中的define_state对应
    marker: {
      size: "46,73", //marker大小(宽,高 单位:像素)
      images: [{
        define_state: "state_1", //marker图片组
        normal_url: "http://localhost:3000/html/images/noise_marker.png",
        activate_url: "http://localhost:3000/html/images/noise_marker_click.png",
      }, ],
    },
    label: {
      bg_image_url: "http://localhost:3000/html/images/label_bg.png",
      size: "113,26", //label大小(宽,高 单位:像素)
      offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
      text: "崇益庄广场", //label中的文本内容
      font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
      text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
      text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
    },
    window: {
      url: "http://localhost:3000/html/noise_wether_page.html?checkPosition=崇益庄广场&degree=14℃&noise=58&pmTen=45&pm=56&humidity=76&wind=东北风2级&NAI=14201&pressure=0.7",
      //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
      size: "420,205", //window大小(宽,高 单位:像素)
      offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
    },
    "coord": "100.214165,25.643192"
  },

  // {
  //     id: "noise_poi_9",
  //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
  //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
  //   coord_z: 1, //高度(米)
  //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
  //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
  //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
  //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
  //   state: "state_1", //与marker之中images中的define_state对应
  //   marker: {
  //     size: "46,73", //marker大小(宽,高 单位:像素)
  //     images: [
  //       {
  //         define_state: "state_1", //marker图片组
  //         normal_url:
  //           "http://localhost:3000/html/images/noise_marker.png",
  //         activate_url:
  //           "http://localhost:3000/html/images/noise_marker_click.png",
  //       },
  //     ],
  //   },
  //   label: {
  //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
  //     size: "113,26", //label大小(宽,高 单位:像素)
  //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //     text: "洱海廊道455M", //label中的文本内容
  //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
  //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
  //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
  //   },
  //   window: {
  //     url: "http://localhost:3000/html/noise_normal.html",
  //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
  //     size: "520,350", //window大小(宽,高 单位:像素)
  //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //   },
  //     "coord": "100.227097,25.621174"
  // },
  // {
  //     id: "noise_poi_10",
  //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
  //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
  //   coord_z: 1, //高度(米)
  //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
  //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
  //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
  //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
  //   state: "state_1", //与marker之中images中的define_state对应
  //   marker: {
  //     size: "46,73", //marker大小(宽,高 单位:像素)
  //     images: [
  //       {
  //         define_state: "state_1", //marker图片组
  //         normal_url:
  //           "http://localhost:3000/html/images/noise_marker.png",
  //         activate_url:
  //           "http://localhost:3000/html/images/noise_marker_click.png",
  //       },
  //     ],
  //   },
  //   label: {
  //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
  //     size: "113,26", //label大小(宽,高 单位:像素)
  //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //     text: "洱海廊道512M", //label中的文本内容
  //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
  //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
  //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
  //   },
  //   window: {
  //     url: "http://localhost:3000/html/noise_normal.html",
  //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
  //     size: "520,350", //window大小(宽,高 单位:像素)
  //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //   },
  //     "coord": "100.227135,25.622293"
  // },
  // {
  //     id: "noise_poi_11",
  //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
  //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
  //   coord_z: 1, //高度(米)
  //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
  //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
  //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
  //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
  //   state: "state_1", //与marker之中images中的define_state对应
  //   marker: {
  //     size: "46,73", //marker大小(宽,高 单位:像素)
  //     images: [
  //       {
  //         define_state: "state_1", //marker图片组
  //         normal_url:
  //           "http://localhost:3000/html/images/noise_marker.png",
  //         activate_url:
  //           "http://localhost:3000/html/images/noise_marker_click.png",
  //       },
  //     ],
  //   },
  //   label: {
  //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
  //     size: "113,26", //label大小(宽,高 单位:像素)
  //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //     text: "洱海廊道566M", //label中的文本内容
  //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
  //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
  //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
  //   },
  //   window: {
  //     url: "http://localhost:3000/html/noise_normal.html",
  //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
  //     size: "520,350", //window大小(宽,高 单位:像素)
  //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //   },
  //     "coord": "100.226349,25.623489"
  // },
  // {
  //     id: "noise_poi_12",
  //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
  //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
  //   coord_z: 1, //高度(米)
  //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
  //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
  //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
  //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
  //   state: "state_1", //与marker之中images中的define_state对应
  //   marker: {
  //     size: "46,73", //marker大小(宽,高 单位:像素)
  //     images: [
  //       {
  //         define_state: "state_1", //marker图片组
  //         normal_url:
  //           "http://localhost:3000/html/images/noise_marker.png",
  //         activate_url:
  //           "http://localhost:3000/html/images/noise_marker_click.png",
  //       },
  //     ],
  //   },
  //   label: {
  //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
  //     size: "113,26", //label大小(宽,高 单位:像素)
  //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //     text: "洱海廊道577M", //label中的文本内容
  //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
  //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
  //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
  //   },
  //   window: {
  //     url: "http://localhost:3000/html/noise_normal.html",
  //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
  //     size: "520,350", //window大小(宽,高 单位:像素)
  //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //   },
  //     "coord": "100.22728,25.623699"
  // },
  // {
  //     id: "noise_poi_13",
  //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
  //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
  //   coord_z: 1, //高度(米)
  //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
  //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
  //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
  //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
  //   state: "state_1", //与marker之中images中的define_state对应
  //   marker: {
  //     size: "46,73", //marker大小(宽,高 单位:像素)
  //     images: [
  //       {
  //         define_state: "state_1", //marker图片组
  //         normal_url:
  //           "http://localhost:3000/html/images/noise_marker.png",
  //         activate_url:
  //           "http://localhost:3000/html/images/noise_marker_click.png",
  //       },
  //     ],
  //   },
  //   label: {
  //     bg_image_url: "http://localhost:3000/html/images/label_bg.png",
  //     size: "113,26", //label大小(宽,高 单位:像素)
  //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //     text: "洱海廊道600M", //label中的文本内容
  //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
  //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
  //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
  //   },
  //   window: {
  //     url: "http://localhost:3000/html/noise_normal.html",
  //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
  //     size: "520,350", //window大小(宽,高 单位:像素)
  //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //   },
  //     "coord": "100.226601,25.625914"
  // },
  // {
  //     id: "noise_poi_14",
  //   coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
  //   cad_mapkey: "", //CAD基准点的Key值, 项目中约定
  //   coord_z: 1, //高度(米)
  //   always_show_label: true, //是否永远显示title, true:显示title(默认), false:不显示title
  //   show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
  //   sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
  //   //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
  //   state: "state_1", //与marker之中images中的define_state对应
  //   marker: {
  //     size: "46,73", //marker大小(宽,高 单位:像素)
  //     images: [
  //       {
  //         define_state: "state_1", //marker图片组
  //         normal_url:
  //           "http://localhost:3000/html/images/unormal_noise_marker.png",
  //         activate_url:
  //           "http://localhost:3000/html/images/unormal_noise_marker.png",
  //       },
  //     ],
  //   },
  //   label: {
  //     bg_image_url: "http://localhost:3000/html/images/unormal_label_bg.png",
  //     size: "113,26", //label大小(宽,高 单位:像素)
  //     offset: "11,63", //label左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //     text: "洱海廊道634M", //label中的文本内容
  //     font_color: "fafafaff", //label中文本颜色(HEXA颜色值)
  //     text_boxsize: "82,14", //label中文本框的大小(宽,高 单位:像素)
  //     text_offset: "25,7", //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
  //   },
  //   window: {
  //     url: "http://localhost:3000/html/noise_unormal.html",
  //     //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
  //     size: "520,350", //window大小(宽,高 单位:像素)
  //     offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
  //   },
  //     "coord": "100.22599,25.62604"
  // }
]