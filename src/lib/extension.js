import bus from '@/lib/bus'
const directiveHash = {
    remove: {
        'poi': 'RemovePOI',
        'path': 'RemovePath',
        'range': 'RemoveRange',
        'heatmap': 'RemoveHeatMap',
        'roadheatmap': 'RoadHeatMap',
        'migrationmap': 'MigrationMap'
    }
}
export const clearAllElement = (callback) => {
    window.cloudRender.SuperAPI('RemoveAllPOI');
    window.cloudRender.SuperAPI('RemoveAllPath');
    window.cloudRender.SuperAPI('RemoveAllRange');
    window.cloudRender.SuperAPI('RemoveAllHeatMap');
    window.cloudRender.SuperAPI("RemoveAllColumnHeatMap");
    window.cloudRender.SuperAPI("RemoveAllSpaceHeatMap")
    window.cloudRender.SuperAPI("RemoveAllRoadHeatMap")
    window.cloudRender.SuperAPI("RemoveAllMigrationMap")
    window.cloudRender.SuperAPI("RemoveAllStrategyMap")
    window.cloudRender.SuperAPI('RemoveAllEffect');
    if (callback) {
        callback();
    }
}

export const clearElement = (type, idList, callback) => {
    idList.forEach(item => {
        window.cloudRender.SuperAPI(directiveHash.remove[type], {
            id: item
        });
    });
    if (callback) {
        callback();
    }
}
export const doPOIMove = (poi, path) => {
    const scriptList = path.map(o => o.coord);
    let pathList = [];
    let headNode = null;
    headNode = scriptList.shift();
    window.cloudRender.SuperAPI('RemovePOI', {
        id: poi.id
    });
    window.cloudRender.SuperAPI('AddCustomPOI', poi);
    // updateCustomPOICoord容易导致poi创建重复，目前原因未知，因此先采用在POI迁徙的第一步，采用删除、再添加的方式，先做初始化。
    pathList.push(headNode);
    const autoTickFunction = () => {
        headNode = scriptList.shift();
        window.cloudRender.SuperAPI('UpdateCustomPOICoord', {
            id: poi.id,
            coord_type: 0,
            cad_mapkey: '',
            coord: headNode,
            coord_z: 0
        });
        pathList.push(headNode);
        // if (pathList.length === 2) {
        //     window.cloudRender.SuperAPI('AddPath', {
        //         "id": poi.id + '_move_path',
        //         "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
        //         "cad_mapkey": "", //CAD基准点Key值, 项目中约定
        //         "type": "dashed_dot", //样式类型; 注①
        //         "color": "00d3ff", //颜色(HEX颜色值)
        //         "width": 2, //宽度(米)
        //         "points": pathList.map(o => {
        //             return {
        //                 coord: o,
        //                 coord_z: 1
        //             }
        //         })
        //     });
        // } else if (pathList.length > 2) {
        //     window.cloudRender.SuperAPI('UpdatePathCoord', {
        //         "id": poi.id + '_move_path',
        //         "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
        //         "cad_mapkey": "", //CAD基准点Key值, 项目中约定
        //         "points": pathList.map(o => {
        //             return {
        //                 coord: o,
        //                 coord_z: 1
        //             }
        //         })
        //     })
        // }
    }

    const autoTick = setInterval(() => {
        if (scriptList.length === 0) {
            clearInterval(autoTick);
            // window.cloudRender.SuperAPI('RemovePath', {
            //     id: poi.id + '_move_path'
            // })
        } else {
            autoTickFunction();
        }
    }, 2000);
}

export const doPOIMigration = (poiStart, poiEnd, path) => {
    console.log('测试重复')
    const scriptList = path.map(o => o.coord);
    const pathList = [];
    let headNode = null;
    window.cloudRender.SuperAPI('AddMigrationMap', {
        "id": poiStart.id + '_' + poiEnd.id,
        "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
        "cad_mapkey": "", //CAD基准点Key值, 项目中约定
        "type": 2, //样式类型(1:波浪线型, 2:箭头型, 3:射线)
        "start_coord": poiStart.coord, //起点坐标 lng,lat
        "start_coord_z": 1, //起点高度(米)
        "targetdata": [{
            "target_coord": poiEnd.coord, //目标点坐标 lng,lat
            "target_coord_z": 1, //目标点高度(米)
            "mark_size": 30, //目标点标志直径(米)
            "mark_color": "ff0000", //目标点颜色(HEX颜色值)
            "line_width": 30, //连线宽度(米)(注:样式类型 type:3时, line_width参数为连线数量, 范围 5 ~ 100)
            "line_color": "ff0000", //连线颜色(HEX颜色值)
            "curvature": 0.6 //连线曲度调节(取值范围 -1 ~ 1, 0为默认曲度, 此值越小曲线越平, 反之曲线越陡峭)
        }]
    }, e => {
        headNode = scriptList.shift();
        window.cloudRender.SuperAPI('RemovePOI', {
            id: poiStart.id
        });
        window.cloudRender.SuperAPI('AddCustomPOI', poiStart);
        // updateCustomPOICoord容易导致poi创建重复，目前原因未知，因此先采用在POI迁徙的第一步，采用删除、再添加的方式，先做初始化。
        pathList.push(headNode);
    });
    window.cloudRender.SuperAPI('FocusMigrationMap', {
        id: poiStart.id + '_' + poiEnd.id,
        distance: 200
    });
    let autoTickFunction = () => {
        headNode = scriptList.shift();
        window.cloudRender.SuperAPI('UpdateCustomPOICoord', {
            id: poiStart.id,
            coord_type: 0,
            cad_mapkey: '',
            coord: headNode,
            coord_z: poiStart.coord_z
        });
        window.cloudRender.SuperAPI('UpdateMigrationMapCoord', {
            "id": poiStart.id + '_' + poiEnd.id,
            "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
            "cad_mapkey": "", //CAD基准点Key值, 项目中约定
            "start_coord": headNode, //起点坐标 lng,lat
            "start_coord_z": 1, //起点高度(米)
            "targetdata": [{
                "target_coord": poiEnd.coord, //目标点坐标
                "target_coord_z": 1 //目标点高度(米)
            }]
        })
        pathList.push(headNode);
        if (pathList.length === 2) {
            window.cloudRender.SuperAPI('AddPath', {
                "id": poiStart.id + '_' + poiEnd.id + '_path',
                "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
                "cad_mapkey": "", //CAD基准点Key值, 项目中约定
                "type": "arrow", //样式类型; 注①
                "color": "ff0000", //颜色(HEX颜色值)
                "width": 20, //宽度(米)
                "points": pathList.map(o => {
                    return {
                        coord: o,
                        coord_z: 10
                    }
                })
            });
        } else if (pathList.length > 2) {
            window.cloudRender.SuperAPI('UpdatePathCoord', {
                "id": poiStart.id + '_' + poiEnd.id + '_path',
                "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
                "cad_mapkey": "", //CAD基准点Key值, 项目中约定
                "points": pathList.map(o => {
                    return {
                        coord: o,
                        coord_z: 10
                    }
                })
            })
        }
    };
    let autoTick = setInterval(() => {
        if (scriptList.length === 0) {
            clearInterval(autoTick);
            window.cloudRender.SuperAPI('RemoveMigrationMap', {
                id: poiStart.id + '_' + poiEnd.id
            });
            window.cloudRender.SuperAPI('RemovePath', {
                id: poiStart.id + '_' + poiEnd.id + '_path'
            })
        } else {
            autoTickFunction();
        }
    }, 1000);
}

export const busEventHandle = (eventName, callback) => {
    bus.$off(eventName);
    if (callback) {
        bus.$on(eventName, callback);
    }
}

export const windowEventHandle = (posterOrigin, callback) => {
    let fn = (msg) => {
        if (msg.origin === posterOrigin) {
            callback(msg);
        }
    }
    window.removeEventListener('message', fn);
    window.addEventListener('message', fn);
}

export const getDistance = (lat1, lng1, lat2, lng2) => {
    let radLat1 = toRadians(lat1);
    let radLat2 = toRadians(lat2);
    let deltaLat = radLat1 - radLat2;
    let deltaLng = toRadians(lng1) - toRadians(lng2);
    let dis = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(deltaLng / 2), 2)));
    return dis * 6378.137 * 1000;

    function toRadians(d) {
        return d * Math.PI / 180;
    }
}

export const viewPOIDetail = (poiId, callback, routeName, routeParams) => {
    window.cloudRender.SuperAPI("FocusPOI", {
        id: poiId,
        distance: 200,
    });
    //如果routeName和routeParams不为空，跳转子路由
    if (routeName && routeParams) {
        this.$router.push({
            name: routeName,
            params: routeParams,
        });
    }
    callback();
}

export const matchNearbyPOI = (params = {
    mainPOIId: "",
    mainPOIList: [],
    matchedPOIList: [],
    matchKey: "",
    matchTypes: [],
    matchRadius: 0,
    matchData: [],
}) => {
    if (params.matchedPOIList.indexOf(params.mainPOIId) < 0) {
        //被点击的点不是匹配名单里的，说明点击的时【业务点】，可能需要匹配操作
        params.matchedPOIList = [];
        bus.$emit("remove-all-wfc");
        
        let clickedPOIData = params.mainPOIList.find((item) => {
            return params.mainPOIId === item.id;
        });

        if (clickedPOIData[params.matchKey]) {
            //判断是不是想要的数据类型
            if (
                typeof clickedPOIData.coord === "string" &&
                typeof params.matchRadius === "number" &&
                params.matchTypes instanceof Array &&
                params.matchData instanceof Array
            ) {
                //循环匹配要素出现的东西
                var copiedMatchData = JSON.parse(JSON.stringify(params.matchData));
                copiedMatchData.forEach((o, i) => {
                    o.window.url =
                        o.window.url +
                        `&schedulable=true&coord=${o.coord}&matchTo=${params.mainPOIId}`;
                    let centerPoint = {
                        lat: clickedPOIData.coord.split(",")[0],
                        lng: clickedPOIData.coord.split(",")[1],
                    };
                    let objectPoint = {
                        lat: o.coord.split(",")[0],
                        lng: o.coord.split(",")[1],
                    };
                    //赋值范围
                    let distance = getDistance(
                        centerPoint.lat,
                        centerPoint.lng,
                        objectPoint.lat,
                        objectPoint.lng
                    );
                    // 如果主点和该点的距离，小于匹配半径，并符合matchTypes中的类型，插入点
                    if (
                        distance <= params.matchRadius &&
                        params.matchTypes.indexOf(o.type) >= 0
                    ) {
                        window.cloudRender.SuperAPI("AddCustomPOI", o);
                        params.matchedPOIList.push(o.id);
                        localStorage.matchedPOIList = JSON.stringify(params.matchedPOIList);
                        bus.$emit("add-wfc", {
                            id: o.id,
                            url: o.window.url,
                        });
                    }
                });
                window.cloudRender.SuperAPI("FocusAllPOI");
            } else {
                console.error("参数有误");
            }
        }
    }
}

export const clearMatchedPOI = () => {
    let matchedPOIList = JSON.parse(localStorage.matchedPOIList);
    if (matchedPOIList instanceof Array) {
        matchedPOIList.forEach((o) => {
            console.log(o);
            window.cloudRender.SuperAPI("RemovePOI", {
                id: o
            });
        });
    }

}

export const timeShift = (offset) => {
    //单位为分钟
    let now = new Date()
    let newTime = new Date(now.getTime() + (offset * 60 * 1000))
    let obj = {
        year: newTime.getFullYear(),
        month: (newTime.getMonth() + 1 < 10) ? '0' + (newTime.getMonth() + 1) : newTime.getMonth() + 1,
        day: newTime.getDate() < 10 ? '0' + newTime.getDate() : newTime.getDate(),
        hour: newTime.getHours() < 10 ? '0' + newTime.getHours() : newTime.getHours(),
        minute: newTime.getMinutes() < 10 ? '0' + newTime.getMinutes() : newTime.getMinutes(),
        seconds: newTime.getSeconds() < 10 ? '0' + newTime.getSeconds() : newTime.getSeconds()
    }
    return `${obj.year}/${obj.month}/${obj.day} ${obj.hour}:${obj.minute}:${obj.seconds}`;
}