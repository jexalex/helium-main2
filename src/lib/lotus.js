import axios from "axios"
export class Camera {
  constructor (option) {
    //debugger
    this.data = {
      centerCoordinate: null,
      elevation: null,
      pitch: null,
      yaw: null
    };
    if (option) {
      this.data.centerCoordinate = option.centerCoord;
      this.data.elevation = option.elevation;
      this.data.pitch = option.pitch;
      this.data.yaw = option.yaw;
      cloudRender.SuperAPI('SetCameraInfo', {
        coord_type: 0,
        cad_mapkey: '',
        center_coord: option.centerCoord,
        arm_distance: option.elevation,
        yaw: option.yaw,
        pitch: option.pitch,
        fly: true,
      });
      cloudRender.SuperAPI('SetCameraRotate', {
        time: 200,
        direction: 'clockwise'
      })
    }
  }
  set params (value) {
    this.data.centerCoordinate = value.centerCoord;
    this.data.elevation = value.elevation;
    this.data.pitch = value.pitch;
    this.data.yaw = value.yaw;
    if (value !== null) {
      cloudRender.SuperAPI('SetCameraInfo', {
        coord_type: 0,
        cad_mapkey: '',
        center_coord: value.centerCoord,
        yaw: value.yaw,
        pitch: value.pitch,
        arm_distance: value.elevation,
        fly: true,
      });
      cloudRender.SuperAPI('SetCameraRotate', {
        time: 200,
        direction: 'clockwise'
      })
    }
  }

  get params () {
    let result = {};
    cloudRender.SuperAPI('GetCameraInfo', {
      coord_type: 0,
      cad_mapkey: ''
    }, e => {
      result = {
        centerCoord: e.args.center_coord,
        elevation: e.args.arm_distance,
        pitch: e.args.pitch,
        yaw: e.args.yaw,
        elevationMax: e.args.arm_distance_max,
        elevationMin: e.args.arm_distance_min,
        pitchMax: e.args.pitch_max,
        pitchMin: e.args.pitch_min,
        yawMax: e.args.yaw_max,
        yawMin: e.args.yaw_min,
        corners: {
          lt: e.args.left_top_coord,
          lb: e.args.left_bottom_coord,
          rt: e.args.right_top_coord,
          rb: e.args.right_bottom_coord
        }
      }
    });
    return result;
  }

  focusOn (eleType) {
    let hash = {
      'POI': 'FocusAllPOI',
      'path': 'FocusAllPath',
      'range': 'FocusAllRange',
      'heatmap': 'FocusAllHeatMap',
      'roadheatmap': 'FocusAllRoadHeatMap',
      'migrationmap': 'FocusAllMigrationMap'
    };
    cloudRender.SuperAPI(hash[eleType]);
    cloudRender.SuperAPI('SetCameraRotate', {
      time: 200,
      direction: 'clockwise'
    })
  }

  clear () {
    cloudRender.SuperAPI('RemoveAllPOI');
    cloudRender.SuperAPI('RemoveAllPath');
    cloudRender.SuperAPI('RemoveAllRange');
    cloudRender.SuperAPI('RemoveAllHeatMap');
    cloudRender.SuperAPI("RemoveAllColumnHeatMap");
    cloudRender.SuperAPI("RemoveAllSpaceHeatMap")
    cloudRender.SuperAPI("RemoveAllRoadHeatMap")
    cloudRender.SuperAPI("RemoveAllMigrationMap")
    cloudRender.SuperAPI("RemoveAllStrategyMap")
    cloudRender.SuperAPI('RemoveAllEffect');
  }
}

export class Cluster {
  constructor (clusterArray) {
    this.clusterArray = clusterArray.map(item => {
      return {
        id: item.id,
        coord_type: 0,
        cad_mapkey: '',
        coord: item.poiCoord,
        coord_z: item.elv ? item.elv : 0,
        always_show_label: false,
        show_label_range: '0,2000',
        sort_order: false,
        state: item.state,
        label: {
          bg_image_url: item.label ? "http://localhost:3000/html/images/label_bg.png" : "",
          size: item.label ? "113,26" : "0,0",                    //label大小(宽,高 单位:像素)
          offset: item.label ? "11,76" : "0,0",
          text: item.label,                  //label中的文本内容
          font_color: "fafafaff",            //label中文本颜色(HEXA颜色值)
          text_boxsize: "82,14",            //label中文本框的大小(宽,高 单位:像素)
          text_offset: "25,7"                 //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
        },
        marker: {
          size: item.label ? '46,85' : '46,73',
          images: [{
            define_state: 'state_1',
            normal_url: `http://localhost:3000/images/${item.type}_marker.png`,
            activate_url: `http://localhost:3000/images/${item.type}_marker_click.png`
          }, {
            define_state: 'state_2',
            normal_url: `http://localhost:3000/images/unormal_${item.type}_marker.png`,
            activate_url: `http://localhost:3000/images/${item.type}_marker_click.png`
          }]
        },
        window: {
          url: window.url,
          size: window.size,
          offset: '30,200'
        }
      }
    });
    if (this.clusterArray) {
      console.log(this.clusterArray)
      axios.post('http://api.wdp.51hitech.com/es/set-poi', {
        "user": "dlerstld",
        "password": "dlerstld123456",
        "project": "p5f5a12c6393ba",
        "poi_type": 2,
        "action": 1,
        "data": this.clusterArray
      }).then(res => {
        console.log(res)
        if (res.data.success) {
          window.cloudRender.SuperAPI("POICluster", {
            "data_url": "http://api.wdp.51hitech.com",       //聚合服务地址
            "type": "all",                              //聚合数据筛选POI点类型(部署点聚合服务时自定义, 如 building, alarm ...);
            //调用时 "type":"all" 全部类型
            "open": true,                                    //true:开启点聚合, false:关闭点聚合
            "split_num": 10,                                 //聚和区块级别(最小2X2最大10X10, 默认2 ~ 10);
            "use_custom_appearance": false                   //使用POI点聚合外观(true 使用; false 不使用); 注: 使用之前必须定义过POI点聚合外观;
          })
        }
      })
    }
  }

  set clusterReset (value) {
    axios.post("http://api.wdp.51hitech.com/es/update-poi", {
      "user": "dlerstld",
      "password": "dlerstld123456",
      "project": "p5f5a12c6393ba",
      "data": value.map(o => {
        return {
          "id": o.id,
          "field": {
            "coord": o.coord,
            "label": o.label ? o.label : "",
            "window": {
              "url": o.window.url ? o.window.url : "",
              "size": o.window.size ? o.window.size : "0,0",
              "offset": o.window.offset ? o.window.offset : "0,0"
            }
          }
        }
      })
    })
  }

  clusterAppend (value) {
    this.appendData = value.map(item => {
      return {
        id: item.id,
        coord_type: 0,
        cad_mapkey: '',
        coord: item.poiCoord,
        coord_z: item.elv,
        always_show_label: false,
        show_label_range: '0,2000',
        sort_order: false,
        state: item.state,
        label: {
          bg_image_url: item.label ? "http://localhost:3000/html/images/label_bg.png" : "",
          size: item.label ? "113,26" : "0,0",                    //label大小(宽,高 单位:像素)
          offset: item.label ? "11,76" : "0,0",
          text: item.label,                  //label中的文本内容
          font_color: "fafafaff",            //label中文本颜色(HEXA颜色值)
          text_boxsize: "82,14",            //label中文本框的大小(宽,高 单位:像素)
          text_offset: "25,7"                 //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
        },
        marker: {
          size: item.label ? '46,85' : '46,73',
          images: [{
            define_state: 'state_1',
            normal_url: `http://localhost:3000/images/${item.type}_marker.png`,
            activate_url: `http://localhost:3000/images/${item.type}_marker_click.png`
          }, {
            define_state: 'state_2',
            normal_url: `http://localhost:3000/images/unormal_${item.type}_marker.png`,
            activate_url: `http://localhost:3000/images/${item.type}_marker_click.png`
          }]
        },
        window: {
          url: window.url,
          size: window.size,
          offset: '30,200'
        }
      }
    });
    if (this.appendData) {
      axios.post('http://api.wdp.51hitech.com/es/set-poi', {
        "user": "dlerstld",
        "password": "dlerstld123456",
        "project": "p5f5a12c6393ba",
        "poi_type": 2,
        "action": 2,
        "data": this.appendData
      }).then(res => {
        console.log(res)
        if (res.success) {
          window.cloudRender.SuperAPI("POICluster", {
            "data_url": "http://api.wdp.51hitech.com",       //聚合服务地址
            "type": "all",                              //聚合数据筛选POI点类型(部署点聚合服务时自定义, 如 building, alarm ...);
            //调用时 "type":"all" 全部类型
            "open": true,                                    //true:开启点聚合, false:关闭点聚合
            "split_num": 10,                                 //聚和区块级别(最小2X2最大10X10, 默认2 ~ 10);
            "use_custom_appearance": false                   //使用POI点聚合外观(true 使用; false 不使用); 注: 使用之前必须定义过POI点聚合外观;
          })
        }
      })
    }
  }

  clusterRemove (value) {
    axios.post("http://api.wdp.51hitech.com/es/delete-poi", {
      "user": "dlerstld",
      "password": "dlerstld123456",
      "project": "p5f5a12c6393ba",
      "data": value.map(o => {
        return o.id
      })
    })
  }
}

export class POI {
  constructor (id, type, coord, elv = 0, state = 'state_1', window = {
    url: null,
    size: '0,0'
  }, label, code) {
    this.id = id;
    this.coordinate = coord;
    this.type = type;
    this.elevation = elv;
    this.phase = state;
    this.window = {
      url: window.url,
      size: window.size,
      offset: '30,200'
    };
    this.label = label;
    this.code = code
    setTimeout(() => {
      cloudRender.SuperAPI('AddCustomPOI', {
        id: this.id,
        coord_type: 0,
        cad_mapkey: '',
        coord: this.coordinate,
        coord_z: this.elevation,
        always_show_label: false,
        show_label_range: '0,2000',
        sort_order: false,
        state: this.phase,
        label: {
          bg_image_url: this.label ? "http://localhost:3000/html/images/label_bg.png" : "",
          size: this.label ? "113,26" : "0,0",                    //label大小(宽,高 单位:像素)
          offset: this.label ? "11,76" : "0,0",
          text: this.label,                  //label中的文本内容
          font_color: "fafafaff",            //label中文本颜色(HEXA颜色值)
          text_boxsize: "82,14",            //label中文本框的大小(宽,高 单位:像素)
          text_offset: "25,7"                 //label中文本框左上角相对于label左上角的margin偏移(x,y 单位:像素), 注: x为正向右, y为正向下
        },
        marker: {
          size: this.label ? '46,85' : '46,73',
          images: [{
            define_state: 'state_1',
            normal_url: `http://localhost:3000/images/${this.type}_marker.png`,
            activate_url: `http://localhost:3000/images/${this.type}_marker_click.png`
          }, {
            define_state: 'state_2',
            normal_url: `http://localhost:3000/images/unormal_${this.type}_marker.png`,
            activate_url: `http://localhost:3000/images/${this.type}_marker_click.png`
          }]
        },
        window: {
          url: window.url,
          size: window.size,
          offset: '30,200'
        }
      });
    }, 500)
  }

  set coord (value) {
    let $this = this;
    $this.coordinate = value;
    cloudRender.SuperAPI('UpdateCustomPOICoord', {
      id: $this.id,
      coord_type: 0,
      cad_mapkey: '',
      coord: value,
      coord_z: $this.elevation
    });
    return value;
  }

  set coord_z (value) {
    let $this = this;
    $this.elevation = value;
    cloudRender.SuperAPI('UpdateCustomPOICoord', {
      id: $this.id,
      coord_type: 0,
      cad_mapkey: '',
      coord: $this.coordinate,
      coord_z: value
    });
    return value;
  }

  set state (value) {
    let $this = this;
    $this.phase = value;
    cloudRender.SuperAPI('UpdateCustomPOIStyle', {
      id: $this.id,
      always_show_label: true,
      show_label_range: '0,2000',
      sort_order: false,
      state: value
    });
    return value;
  }

  set dialog (value = {
    url: '',
    size: '',
    offset: '30,200'
  }) {
    let $this = this;
    $this.window = value;
    cloudRender.SuperAPI('UpdateCustomPOIWindow', {
      url: value.url,
      size: value.size,
      offset: value.offset
    });
    return value;
  }

  focus (distance = 200) {
    cloudRender.SuperAPI('FocusPOI', {
      id: this.id,
      distance: distance
    })
    // cloudRender.SuperAPI('SetCameraRotate', {
    //     time: 200,
    //     direction: 'clockwise'
    // })
  }

  remove () {
    cloudRender.SuperAPI('RemovePOI', {
      id: this.id
    })
  }

  moveByPath (path) {
    if (path instanceof Array && path[0].coord) {
      const scriptList = path.map(o => o.coord);
      let pathList = [];
      let headNode = null;
      headNode = scriptList.shift();
      cloudRender.SuperAPI('UpdateCustomPOICoord', {
        id: this.id,
        coord_type: 0,
        cad_mapkey: '',
        coord: headNode,
        coord_z: this.elevation
      });
      // updateCustomPOICoord容易导致poi创建重复，目前原因未知，因此先采用在POI迁徙的第一步，采用删除、再添加的方式，先做初始化。
      pathList.push(headNode);
      const autoTickFunction = () => {
        headNode = scriptList.shift();
        cloudRender.SuperAPI('UpdateCustomPOICoord', {
          id: this.id,
          coord_type: 0,
          cad_mapkey: '',
          coord: headNode,
          coord_z: this.elevation
        });
        pathList.push(headNode);
      }

      const autoTick = setInterval(() => {
        if (scriptList.length === 0) {
          clearInterval(autoTick);
          // window.cloudRender.SuperAPI('RemovePath', {
          //     id: poi.id + '_move_path'
          // })
        } else {
          autoTickFunction();
        }
      }, 2000);
    }
  }

  migrateByPath (poiEnd, path) {
    const scriptList = path.map(o => o.coord);
    const pathList = [];
    let headNode = null;
    cloudRender.SuperAPI('AddMigrationMap', {
      "id": this.id + '_' + poiEnd.id,
      "coord_type": 0,
      "cad_mapkey": "",
      "type": 2,
      "start_coord": this.coordinate,
      "start_coord_z": 1,
      "targetdata": [{
        "target_coord": poiEnd.coordinate,
        "target_coord_z": 1,
        "mark_size": 5,
        "mark_color": "ff0000",
        "line_width": 5,
        "line_color": "ff0000",
        "curvature": 0.6
      }]
    }, e => {
      headNode = scriptList.shift();
      cloudRender.SuperAPI('UpdateCustomPOICoord', {
        id: this.id,
        coord_type: 0,
        cad_mapkey: '',
        coord: headNode,
        coord_z: this.elevation
      });
      pathList.push(headNode);
    });
    cloudRender.SuperAPI('FocusMigrationMap', {
      id: this.id + '_' + poiEnd.id,
      distance: 200
    });
    let autoTickFunction = () => {
      headNode = scriptList.shift();
      cloudRender.SuperAPI('UpdateCustomPOICoord', {
        id: this.id,
        coord_type: 0,
        cad_mapkey: '',
        coord: headNode,
        coord_z: this.elevation
      });
      cloudRender.SuperAPI('UpdateMigrationMapCoord', {
        "id": this.id + '_' + poiEnd.id,
        "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
        "cad_mapkey": "", //CAD基准点Key值, 项目中约定
        "start_coord": headNode, //起点坐标 lng,lat
        "start_coord_z": 1, //起点高度(米)
        "targetdata": [{
          "target_coord": poiEnd.coordinate, //目标点坐标
          "target_coord_z": 1 //目标点高度(米)
        }]
      })
      pathList.push(headNode);
      if (pathList.length === 2) {
        cloudRender.SuperAPI('AddPath', {
          "id": this.id + '_' + poiEnd.id + '_path',
          "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
          "cad_mapkey": "", //CAD基准点Key值, 项目中约定
          "type": "arrow", //样式类型; 注①
          "color": "ff0000", //颜色(HEX颜色值)
          "width": 5, //宽度(米)
          "points": pathList.map(o => {
            return {
              coord: o,
              coord_z: 10
            }
          })
        });
      } else if (pathList.length > 2) {
        cloudRender.SuperAPI('UpdatePathCoord', {
          "id": this.id + '_' + poiEnd.id + '_path',
          "coord_type": 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
          "cad_mapkey": "", //CAD基准点Key值, 项目中约定
          "points": pathList.map(o => {
            return {
              coord: o,
              coord_z: 10
            }
          })
        })
      }
    };
    let autoTick = setInterval(() => {
      if (scriptList.length === 0) {
        clearInterval(autoTick);
        cloudRender.SuperAPI('RemoveMigrationMap', {
          id: this.id + '_' + poiEnd.id
        });
        cloudRender.SuperAPI('RemovePath', {
          id: this.id + '_' + poiEnd.id + '_path'
        })
      } else {
        autoTickFunction();
      }
    }, 1000);
  }
}

export class Heatmap {
  constructor (id, area = {
    lt: '',
    lb: '',
    rt: '',
    rb: ''
  }, data = [{
    coord: '',
    value: 0
  }], valueRange = {
    min: 0,
    max: 100
  }) {
    this.area = area;
    this.id = id;
    this.data = data;
    this.valueRange = valueRange;
    let json = {
      id: this.id,
      coord_type: 0,
      cad_mapkey: '',
      heatmap_type: 1,
      leftupper_coord: this.area.lt,
      leftlower_coord: this.area.lb,
      rightlower_coord: this.area.rb,
      rightupper_coord: this.area.rt,
      coord_z: 1,
      brush_diameter: 200,
      heatpoint_minvalue: this.valueRange.min,
      heatpoint_maxvalue: this.valueRange.max,
      data: this.data
    }
    console.log('AddHeatMap', json);
    cloudRender.SuperAPI('AddHeatMap', json)
  }

  set heatData (value) {
    this.data = value;
    cloudRender.SuperAPI('UpdateHeatMapCoord', {
      id: this.id,
      coord_type: 0,
      cad_mapkey: '',
      coord_z: 1,
      data: this.data,
      clear_old_data: false
    });
    return this.data;
  }

  set range (value = {
    min: 0,
    max: 100
  }) {
    this.valueRange = value;
    cloudRender.SuperAPI('UpdateHeatMapStyle', {
      id: this.id,
      heatmap_type: 1,
      brush_diameter: 1200,
      heatpoint_minvalue: value.min,
      heatpoint_maxvalue: value.max
    })
    return this.valueRange;
  }

  focus (distance = 200) {
    cloudRender.SuperAPI('FocusHeatMap', {
      id: this.id,
      distance: distance
    })
  }

  remove () {
    cloudRender.SuperAPI('RemoveHeatMap', {
      id: this.id
    })
  }
}

export class Path {
  constructor (option) {
    this.prototype = {
      id: "path-id",
      coord_type: 0,
      cad_mapkey: "",
      shape: "solid",
      color: "#ff0000",
      width: 10,
      points: []
    }
    this.data = Object.assign(this.prototype, option);
    //读取上一次的渲染时间消耗
    let delay = 0;
    if (localStorage.lastRenderDuration) {
      delay = JSON.parse(localStorage.lastRenderDuration)
    }
    //let delay = JSON.parse(localStorage.lastRenderDuration)
    setTimeout(() => {
      cloudRender.SuperAPI('AddPath', {
        id: this.data.id,
        coord_type: this.data.coord_type,
        cad_mapkey: this.data.cad_mapkey,
        type: this.data.shape,
        color: this.data.color.replace('#', ''),
        width: this.data.width,
        points: this.data.points
      });
      console.dir({
        id: this.data.id,
        coord_type: this.data.coord_type,
        cad_mapkey: this.data.cad_mapkey,
        type: this.data.shape,
        color: this.data.color.replace('#', ''),
        width: this.data.width,
        points: this.data.points
      })
    }, delay)

    //写入本次路径渲染所需的预估时间，便于后续其他元素渲染时，作为一种延时参考
    //根据测算，路径每个点的渲染时间大概是3ms
    let timeCost = this.data.points.length * 3;
    localStorage.lastRenderDuration = JSON.stringify(timeCost);
    return this;
  }

  set shape (value) {
    this.data.shape = value;
    let jsondata = {
      id: this.data.id,
      type: value,
      color: this.data.color,
      width: this.data.width
    }
    cloudRender.SuperAPI("UpdatePathStyle", jsondata);
    return this;
  }
  get shape () {
    return this.data.shape
  }

  set color (value) {
    this.data.color = value;
    let jsondata = {
      id: this.data.id,
      type: this.data.shape,
      color: value,
      width: this.data.width
    }
    cloudRender.SuperAPI("UpdatePathStyle", jsondata);
    return this;
  }
  get color () {
    return this.data.color
  }

  set width (value) {
    this.data.width = value;
    let jsondata = {
      id: this.data.id,
      type: this.data.shape,
      color: this.data.color,
      width: value
    }
    cloudRender.SuperAPI("UpdatePathStyle", jsondata);
    return this;
  }
  get width () {
    return this.data.width;
  }

  set coords (value) {
    this.data.coords = value;
    let jsondata = {
      id: this.data.id,
      coord_type: this.data.coord_type,
      cad_mapkey: this.data.cad_mapkey,
      points: value
    }
    let delay = JSON.parse(localStorage.lastRenderDuration);
    setTimeout(() => {
      cloudRender.SuperAPI('UpdatePathCoord', jsondata);
    }, delay);
    let timeCost = value.length * 3;
    localStorage.lastRenderDuration = JSON.stringify(timeCost);
    return this;
  }
  get coords () {
    return this.data.coords;
  }

  focus (distance = 200) {
    cloudRender.SuperAPI('FocusPath', {
      id: this.id,
      distance: distance
    })
  }

  remove () {
    cloudRender.SuperAPI('RemovePath', {
      id: this.id
    })
  }
}