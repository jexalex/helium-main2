import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css';

Vue.use(ViewUI)

Vue.config.productionTip = false
require('@/assets/fonts/mfys.otf');

require('@/assets/css/base.css');
require('@/assets/css/base.font.css');
require('@/assets/css/top.css');
require('@/assets/css/bottom.css');
require('@/assets/css/container.css');
require('@/assets/css/component.css');
require('@/assets/css/dialog.css');


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
