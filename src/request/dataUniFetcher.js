import axios from 'axios';
import md5 from 'js-md5'
import qs from 'qs'
import {
  coordConvetor
} from "@/lib/coordConvetor";
let Base64 = require('js-base64').Base64;
const $URLHEAD = 'http://localhost:3080/server'

const convertCAJ_WGS = (coord) => {
  return coordConvetor.GCJ02_To_WGS84(parseFloat(coord.split(',')[0]), parseFloat(coord.split(',')[1]))
}

// const getVideoData = () =>{
//     let userName = "admin"
//     let accessCode = ""
//     let password = ""
//     let sign = ""
//     if(!localStorage.videoAccessToken){
//         axios.post('http://server-addr:8088/VIID/login/v2').then(res => {
//             if(res){
//                 accessCode = res.accessCode
//                 sign = md5(Base64(userName))+accessCode+md5(password)
//                 axios.post('http://server-addr:8088/VIID/login/v2',{
//                     "UserName": userName,
//                     "AccessCode": accessCode,
//                     "LoginSignature": sign
//                 }).then(v=>{
//                     if(v){
//                         localStorage.videoAccessToken = v.AccessToken
//                     }
//                 })
//             }
//         })
//     }
// }

const fetchDataJH = (url, params) => {
  let fetchedData = null;
  let now = new Date();
  let timeStamp = now.getTime();
  let JHURL = `http://139.155.88.154`;
  if (!localStorage.accessTokenJH || timeStamp > parseInt(localStorage.expiredTimeStampJH)) {

    axios.post(`${JHURL}/oauth/token`, {
      grant_type: 'client_credentials',
      client_id: '5c0e750b7a5cd42464a5099d',
      client_secret: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
    }).then(res => {
      console.dir(res.data);
      localStorage.accessTokenJH = JSON.stringify(res.data.access_token);
      localStorage.expiredTimeStampJH = timeStamp + 2 * 60 * 60 * 1000;
    })
    let accessToken = JSON.parse(localStorage.accessTokenJH);
    return new Promise(resolve => {
      axios.get(JHURL + url, {
        headers: {
          accessToken
        },
        params: params
      }).then(res => {
        fetchedData = res.data;
        resolve(res.data);
        console.dir(fetchedData);
      }).catch(e => {
        console.error(`${JHURL + url}请求数据出错，请求参数如下：`);
        console.dir(params);
      })
    });
  } else {
    let accessToken = JSON.parse(localStorage.accessTokenJH);
    return new Promise(resolve => {
      axios.get(JHURL + url, {
        headers: {
          "access_token": accessToken
        },
        params: params
      }).then(res => {
        fetchedData = res.data;
        resolve(res.data);
        // console.dir(fetchedData);
      }).catch(e => {
        console.error(`${JHURL + url}请求数据出错，请求参数如下：`);
        console.dir(params);
      })
    });
  }
}

const fetchDataZG = (url, params) => {
  let fetchedData = null;
  let now = new Date();
  let timeStamp = now.getTime();
  let ZGURL = `http://103.215.44.35:9979`;
  if (!localStorage.accessTokenZG || timeStamp > parseInt(localStorage.expiredTimeStampZG)) {
    let sign = md5(`appId=langshengzonghe&expiresTime=${timeStamp}&appSecret=872oiu/!qhs144564u1i2j3n4is`);
    axios.post(`${ZGURL}/api/v1/authentication/sso_auth/out_source_get_token`, qs.stringify({
      appId: "langshengzonghe",
      expiresTime: timeStamp,
      sign: sign
    })).then(res => {
      console.dir(res.data);
      localStorage.accessTokenZG = JSON.stringify(res.data.data);
      localStorage.expiredTimeStampZG = timeStamp + 2 * 60 * 60 * 1000;
      let accessToken = JSON.parse(localStorage.accessTokenZG);
      return new Promise(resolve => {
        axios.get(ZGURL + url, {
          headers: {
            accessToken
          },
          params: params
        }).then(res => {
          fetchedData = res.data;
          resolve(res.data);
          console.dir(fetchedData);
        }).catch(e => {
          console.error(`${ZGURL + url}请求数据出错，请求参数如下：`);
          console.dir(params);
        })
      });
    })
  } else {
    let accessToken = JSON.parse(localStorage.accessTokenZG);
    return new Promise(resolve => {
      axios.get(ZGURL + url, {
        headers: {
          accessToken
        },
        params: params
      }).then(res => {
        fetchedData = res.data;
        resolve(res.data);
        // console.dir(fetchedData);
      }).catch(e => {
        console.error(`${ZGURL + url}请求数据出错，请求参数如下：`);
        console.dir(params);
      })
    });
  }
}

const fetchDataHK = (url, params) => {
  let fetchedData = null;
  return new Promise(resolve => {
    axios.get('https://39.129.61.42:1443' + url, {
      params: params
    }).then(res => {
      fetchedData = res.data;
      resolve(res.data);
      // console.dir(fetchedData);
    }).catch(e => {
      console.error(`${'https://39.129.61.42:1443' + url}请求数据出错，请求参数如下：`);
      console.dir(params);
    })
  });
}

const fetchData = (url, params) => {
  let fetchedData = null;
  return new Promise(resolve => {
    axios.get($URLHEAD + url, {
      params: params
    }).then(res => {
      fetchedData = res.data;
      resolve(res.data);
      // console.dir(fetchedData);
    }).catch(e => {
      console.error(`${$URLHEAD + url}请求数据出错，请求参数如下：`);
      console.dir(params);
    })
  });
}

const getData = (url, params) => {
  let fetchedData = null;
  return new Promise(resolve => {
    axios.get('http://103.215.44.35:9979/api' + url, {
      params: params
    }).then(res => {
      fetchedData = res.data;
      resolve(res.data);
      console.dir(fetchedData);
    }).catch(e => {
      console.error(`${'http://103.215.44.35:9979/api' + url}请求数据出错，请求参数如下：`);
      console.dir(params);
    })
  });
}

const getJHData = (url, params) => {
  let fetchedData = null;
  return new Promise(resolve => {
    axios.get('http://139.155.88.154/tengyun' + url, {
      params: params
    }).then(res => {
      fetchedData = res.data;
      resolve(res.data);
      console.dir(fetchedData);
    }).catch(e => {
      console.error(`${'http://139.155.88.154/tengyun' + url}请求数据出错，请求参数如下：`);
      console.dir(params);
    })
  });
}

const uniFetcher = {
  Home: {
    getOrderList: async () => {
      let raw = await fetchDataZG('/api/v1/feedback_management/manager/out_query_feed_back_information')
      return raw.data
    },
    getSecurityStation: async () => {
      let raw = await fetchDataJH('/tengyun/api/security/securityInterface')
      console.log(raw)
    },
    getWheather: async () => {
      let raw = await fetchDataJH('/tengyun/api/bar/point/weather')
      return raw.data[0]
    },
    getContent: async () => {
      let raw = await fetchDataJH('/tengyun/api/erhai-gallery/count/device')
      return raw.data
    },
    getFacilityInfo: async () => {
      let raw = await fetchDataJH('/tengyun/api/erhai-gallery/device/countStatus')
      return raw.data
    }
  },
  ecology: {
    getWeatherPOIData: async () => {
      let raw = await fetchDataJH('/tengyun/api/bar/point/weather')
      console.log("noise", raw);
      let result = raw.data.map(o => {

        let info = `degree=${o.temperature_value}℃&noise=${o.noise_value}&pmTen=${o.pm10_value}&pm=${o.pm25_value}&humidity=${o.humidity_value}&wind=${o.avg_wind_speed}&NAI=${o.nai}&pressure=${o.pressure_value}`
        return {
          id: o.device_id,
          coord_type: 0, //坐标类型(0:经纬度坐标, 1:cad坐标)
          cad_mapkey: "", //CAD基准点的Key值, 项目中约定
          coord_z: 1, //高度(米)
          always_show_label: false, //是否永远显示title, true:显示title(默认), false:不显示title
          show_label_range: "0,2000", //POI点显示title的范围(单位:米, 范围最小、最大距离; 在此范围内显示, 超出范围隐藏; 注意:always_show_label属性优先于此属性)
          sort_order: false, //是否开启遮挡排序(根据POI点真实3D位置开启遮挡排序,
          //注: 只与其他开启排序的POI之间进行排序, 开启此排序会消耗性能(最多60个POI点同时开启排序))
          state: "state_1", //与marker之中images中的define_state对应
          marker: {
            size: "46,73", //marker大小(宽,高 单位:像素)
            images: [{
              define_state: "state_1", //marker图片组
              normal_url: "http://localhost:3000/html/images/unormal_noise_marker.png",
              activate_url: "http://localhost:3000/html/images/unormal_noise_marker.png",
            },],
          },

          window: {
            url: `http://localhost:3000/html/noise_wether_page.html?${info}`,
            //本地地址: "file:///D:/xxx/echarts.html",    D: 云渲染所在盘符
            size: "420,205", //window大小(宽,高 单位:像素)
            offset: "50,180", //window左上角相对于Marker的中心点(整个Marker的底部、中间位置)的偏移(x,y 单位:像素), 注: x为正向右, y为正向上
          },
          "coord": `${o.lng},${o.lat}`
        }
      })
      console.log(result);
      return result
    },
  },
  Operations: {
    getOperationsBasicInfo: async () => {
      let raw = await fetchDataJH('/tengyun/lbs/realFlow')
      return raw.data
    },
    getOpinionData: async () => {
      let raw = await fetchDataZG('/api/v1/feedback_management/manager/out_query_feed_back_information')
      return raw.data
    },
    getLBSData: async () => {
      let result = null;
      let raw = await fetchData('/operations/LBSData.json');
      return raw
    },
    getAllLBSData: async () => {
      let now = new Date()
      let date = new Date(now.getTime() - 24 * 60 * 60 * 1000)
      let month = ""
      if ((date.getMonth() + 1) < 10) {
        month = `0${date.getMonth() + 1}`
      } else {
        month = date.getMonth + 1
      }
      let days = ""
      if (date.getDate() < 10) {
        days = `0${date.getDate()}`
      } else {
        days = date.getDate()
      }
      let beforeDate = `${date.getFullYear()}-${month}-${days}`
      let raw = await fetchDataJH('/tengyun/lbs/portrayal', {
        date: beforeDate
      })
      return raw.data
    },
    getShuttleBus: async () => {
      let result = null
      let raw = await fetchData('/operations/shuttlePoi.json');
      return raw.map((o, i) => {
        let coord = convertCAJ_WGS(o.coord)
        return {
          id: o.id,
          type: 'shuttle',
          poiCoord: coord,
          status: o.status,
          state: o.status === "异常" ? 'state_2' : 'state_1',
        }
      })
    },
    getShuttleMove: async () => {
      let raw = await fetchDataJH('/tengyun/api/vehicles/track')
      let onlineList = []
      raw.data.forEach(item => {
        if (item.statusName === '在线' || item.statusName === '在线，ACC关闭') {
          onlineList.push(item)
        }
      })
      if (raw.code === 0) {
        return onlineList.map((o, i) => {
          return {
            id: o.vehicleCode,
            type: 'shuttle',
            poiCoord: o.lng + ',' + o.lat,
            status: o.statusName,
            state: o.statusName === '在线' ? 'state_1' : 'state_2',
            label: '',
            shuttleNumber: o.vehicleCode
          }
        })
      }
    },
    getShuttleInfo: async (shuttleNumber) => {
      let raw = await fetchDataJH('/tengyun/api/vehicles/info', {
        vehiclesCode: shuttleNumber
      })
      return raw.data
    },
    getBusDevice: async (shuttleNumber) => {
      let raw = await fetchDataJH('/tengyun/api/vehicles/info', {
        vehiclesCode: shuttleNumber
      })
      // debugger
      if (raw.code === 0) {
        console.log(raw)
        return raw.data[0].device_code
      }
    },
    // getShuttleStation: async (id) => {
    //     let raw = await fetchData('/operations/shuttleDetail.json')
    //     let node = raw.find(e => {
    //         return id === e.id
    //     });
    //     return node.station
    // },
    getShuttleStation: async () => {
      let raw = await fetchDataJH("/tengyun/api/vehicles/comprehensiveStatus")
      return raw.data[0].station
    },
    getShuttleCurrent: async (id) => {
      let raw = await fetchData('/operations/shuttleDetail.json')
      let node = raw.find(e => {
        return id === e.id
      })
      return {
        driver: node.driver,
        shuttleNumber: node.shuttleNumber,
        shuttleStatus: node.shuttleStatus,
        shuttleSpeed: node.shuttleSpeed,
        driverStatus: node.driverStatus
      }
    },
    getShuttleOverall: async () => {
      let raw = await fetchDataJH('/tengyun/api/vehicles/comprehensiveStatus')
      let node = raw.data[0]
      return {
        workPath: node.stationName,
        time: node.workTime,
        pathCount: node.pathCount
      }
    },
    getShuttleShift: async () => {
      let raw = await fetchDataJH('/tengyun/api/vehicles/driver')
      console.log(raw)
      return raw.data
    },
    getShuttleCamera: async (id) => {
      let raw = await fetchData('/operations/shuttleDetail.json')
      let node = raw.find(e => {
        return id === e.id
      });
      return node.shuttleCamera
    },
    getShuttleAllCamera: async (deviceCode) => {
      let raw = await fetchDataZG('/api/v1/vvm/vehicles_vessels/get_live_url', {
        deviceCode: deviceCode,
      })
      console.log(raw)
      return raw.data
    },
    getShuttlePath: async (id) => {
      let raw = await fetchData('/operations/shuttlePath.json')
      let node = raw.find(e => {
        return id === e.shuttleId
      });
      let result = node.pathGroup.map(item => {
        if (item.id.includes('plan')) {
          return {
            id: item.id,
            shape: 'arrow_dot',
            color: '#6dff82',
            width: 15,
            coord_type: 0,
            cad_mapkey: "",
            points: item.pathData.map(o => {
              return {
                coord: o.coord,
                coord_z: 10
              }
            })
          }
        } else {
          return {
            id: item.id,
            shape: 'dashed_dot',
            color: '#ff0000',
            width: 5,
            coord_type: 0,
            cad_mapkey: "",
            points: item.pathData.map(o => {
              return {
                coord: o.coord,
                coord_z: 10
              }
            })
          }
        }
      })
      return result
    },
    getShuttleActualPath: async (id) => {
      let raw = await fetchData('/operations/shuttleActualPath.json')
      let node = raw.find(e => {
        return id === e.shuttleId
      });
      return {
        id: node.id,
        shape: "dashed_dot",
        color: "#ff0000",
        width: 5,
        coord_type: 0,
        cad_mapkey: "",
        points: node.actualPath.map(item => {
          return {
            coord: item,
            coord_z: 10
          }
        })
      }
    },
    // getHeatMapData: async () => {
    //     let raw = await fetchData('/operations/heatMap.json')
    //     return {
    //         id: "heatmap_tourism",
    //         area:{
    //         lt: "100.21574,25.64917", //热力区域坐标(左上坐标 lng,lat)
    //         lb: "100.211324,25.647504", //热力区域坐标(左下坐标 lng,lat)
    //         rb: "100.222351,25.625396", //热力区域坐标(右下坐标 lng,lat)
    //         rt: "100.225583,25.625919", //热力区域坐标(右上坐标 lng,lat)
    //         },
    //         valueRange:{
    //             min: 1,
    //             max: 200
    //         },
    //         data: raw
    //     }
    // },
    getHeatMapData: async () => {
      let raw = await fetchDataJH('/tengyun/lbs/heatMap')
      console.log(raw)
      let rawList = []
      if (raw.data.length > 100) {
        for (let i = 0; i < raw.data.length; i += 100) {
          rawList.push(raw.data.splice(i, 100))
        }
      }
      // let convetorData = rawList.map((item,index)=>{
      //     return{
      //         coord: convertCAJ_WGS(item.coord),
      //         value: item.value
      //     }
      // })
      return rawList
    },
  },
  security: {
    getSecurityOverall: async () => {
      let raw = await fetchDataJH('/tengyun/api/security/deploymentSituation')
      return raw.data[0]
    },
    getEventSource: async () => {
      let raw = await fetchDataJH('/tengyun/api/security/eventStatistics', {
        type: 'month',
        date: '2020-12'
      })
      return raw.data
    },
    getSecurityEventTrend: async () => {
      let raw = await fetchDataJH('/tengyun/scene/trend')
      return raw.data
    },
    getSecurityEventSloveStatus: async () => {
      let raw = await fetchDataJH('/tengyun/api/security/dealCount', {
        startDate: '2020-11-01',
        endDate: '2021-12-31'
      })
      return raw.data
    },
    getSecurityEventTrack: async () => {
      let raw = await fetchDataJH('/tengyun/api/security/securityWorkOrder')
      return raw.data
    },
    getSecurityEvent: async () => {
      let raw = await fetchDataZG('/api/v1/feedback_management/manager/out_query_feed_back_information')
      return raw.data
    },
    getSecurityEventDetail: async () => {
      let raw = await fetchDataZG('/api/v1/feedback_management/manager/out_query_feed_back_information')
      return raw.data
    },
    getSecurityCamera: async (device_code) => {
      let raw = await fetchDataZG('/api/v1/video_monitoring/smart/get_preview_url', {
        cameraIndexCode: device_code
      })
      console.log(raw)
      return raw
    },
    // getSecurityCamera: async (device_code) => {
    //   let raw = await fetchDataHK('/api/video/v2/cameras/previewURLs', {
    //     ak: '28506251',
    //     secret: 'JcwIos7VuFwUEz2gZWtc',
    //     cameraIndexCode: device_code,
    //     streamType: 1,
    //     protocol: "hls",
    //     transmode: 1,
    //   })
    //   console.log(raw)
    //   return raw
    // },
    getSecurityAllCamera: async () => {
      let raw = await fetchDataJH('/tengyun/camera/cameraList')
      console.log(raw)
      return raw.data.map((item, index) => {
        return {
          id: 'security_camera_' + index,
          type: 'camera',
          state: item.status === 1 ? 'state_1' : 'state_2',
          poiCoord: item.longitude + ',' + item.latitude,
          code: item.code
        }
      })
    },
    getAllCamera: async () => {
      let raw = await fetchData('/camera.json')
      return raw.map(o => {
        return {
          id: o.deviceNumber,
          poiCoord: o.coord,
          type: 'camera',
          state: 'state_1'
        }
      })
    },
    getSecurityWorker: async () => {
      let raw = await fetchDataJH('/tengyun/api/patrol/patrolPersonnelLocation')
      console.log(raw)
      return raw.data.map((o, i) => {
        return {
          id: 'xj-' + o.userCode,
          type: 'worker',
          poiCoord: o.longitude + ',' + o.latitude,
          status: '正常',
          state: 'state_1',
          label: ''
        }
      })
    },
    getSecurityPlanePOI: async () => {
      //let raw = await fetchData('/security/securityPlanePoi.json')
      let raw = await fetchDataZG('/api/v1/feedback_management/uav_data/get_um_uva_data')
      console.log(raw)
      return raw.data.returnData.records.map((o, i) => {
        return {
          id: "security_plane_" + i,
          type: 'drone',
          poiCoord: o.lng + ',' + o.lat,
          //poiCoord: "100.20426146,25.70116786",
          //elv: o.attitude,
          elv: o.ultrasonic,
          state: 'state_1',
          window: {
            url: `http://localhost:3000/pages/easy.player.html?url=${o.flv}`,
            size: '855,495'
          },
          label: ''
        }
      })
    },
    getSecurityPlaneUpdate: async () => {
      let raw = await fetchData('/security/securityPlaneUpdate.json')
      return raw
    },
    getSecurityWorkerInfo: async (workerId) => {
      let raw = await fetchDataJH('/tengyun/api/patrol/patrolPersonnelInformation', {
        code: workerId
      })
      return raw.data[0]
    },
    startSecurityWorkerCamera: async (userId) => {
      let raw = await fetchDataZG('/api/v1/electronic_watchman/equipment/real_play_video', {
        userId: userId
      })
      return raw.success
    },
    getSecurityWorkerCamera: async (userId) => {
      let raw = await fetchDataZG('/api/v1/electronic_watchman/equipment/get_video_address', {
        userId: userId
      })
      return raw.data
    },
    getSecurityWorkerCheckList: async () => {
      let raw = await fetchDataJH('/tengyun/api/patrol/patrolClockInInformation')
      return raw.data
    },
    getSecurityWorkerHistory: async () => {
      let raw = await fetchDataJH('/tengyun/api/patrol/patrolHistory', {
        pageNo: 1,
        pageSize: 40
      })
      return raw.data.list
    },
    getSecurityWorkerPath: async (lineId) => {
      let raw = await fetchDataJH('/tengyun/api/patrol/patrolPointInformation', {
        lineId: lineId
      })
      let points = []
      raw.data.forEach((item, index) => {
        points.push({
          coord: item.longitude + ',' + item.latitude,
          coord_z: 5
        })
      })
      return {
        id: "worker_path",
        shape: 'arrow',
        color: "#2e8e0d",
        points: points
      }
    }
  },
  facilities: {
    getStatistics: async () => {
      let result = null;
      let raw = await fetchData('/facilities/general.json');
      result = {
        facilitiesTotalCount: raw.reduce((total, current) => {
          return total + current.totalCount;
        }, 0),
        facilitiesNormalCount: raw.reduce((total, current) => {
          return total + current.normalCount;
        }, 0),
        facilitiesFailedCount: raw.reduce((total, current) => {
          return total + current.failedCount;
        }, 0),
        facilitiesWorksheetCount: raw.reduce((total, current) => {
          return total + current.worksheet;
        }, 0),
        statData: raw
      }
      return result;
    },
    faultFacilityType: async () => {
      let raw = await fetchDataJH('/tengyun/api/erhai-gallery/device/faultCount');
      return raw.data
    },
    getClassified: async () => {
      let raw = await fetchData('/facilities/general.json');
      let result = raw.map(o => {
        return {
          name: o.typeName,
          value: o.totalCount
        }
      });
      return result;
    },
    getDamagedDistribution: async () => {
      let raw = await fetchData('/facilities/general.json');
      let result = raw.map(o => {
        return {
          name: o.typeName,
          value: o.failedCount
        }
      });
      return result;
    },
    getRepairedDistribution: async () => {
      let raw = await fetchData('/facilities/general.json');
      let result = raw.map(o => {
        return {
          name: o.typeName,
          value: o.repairedCount
        }
      });
      return result;
    },
    getWorkSheetCompletion: async () => {
      let raw = await fetchData('/facilities/general.json');
      return 100
      // let calc = {
      //     facilitiesWorksheetCompletedCount: raw.reduce((total, current) => {
      //         console.log(total)
      //         return total + current.worksheetCompleted;
      //     }, 0),
      //     facilitiesWorksheetCount: raw.reduce((total, current) => {
      //         return total + current.worksheet;
      //     }, 0),
      // }
      return parseInt((calc.facilitiesWorksheetCompletedCount / calc.facilitiesWorksheetCount) * 100);
    },
    getWorksheetTrend: async () => {
      let raw = await fetchData('/facilities/worksheet.trend.json');
      return raw;
    },
    getWorksheetSupervisor: async () => {
      let raw = await fetchData('/facilities/worksheet.json');
      let result = raw.map(o => {
        return {
          title: o.name,
          updateTime: o.updateTime,
          status: o.isFinished ? '已完成' : '未完成',
          supervisorCount: o.supervisorCount
        }
      });
      return result;
    }
  },
  facilitiesLamp: {
    getNormalRate: async () => {
      let raw = await fetchDataJH('/tengyun/api/bar/point/info');
      let totalCount = raw.data.normalTotal + raw.data.malfunctionTotal
      return {
        percentage: parseInt((raw.data.normalTotal / totalCount) * 100),
        data: [{
          name: '正常数',
          value: raw.data.normalTotal
        }, {
          name: '故障数',
          value: raw.data.malfunctionTotal
        }]
      }
    },
    getWorksheetCompletion: async () => {
      let raw = await fetchDataJH('/tengyun/api/bar/point/info');
      let totalCount = raw.data.accomplishTotal + raw.data.noAccomplishTotal
      return {
        percentage: parseInt((raw.data.accomplishTotal / totalCount) * 100),
        data: [{
          name: '已完成',
          value: raw.data.accomplishTotal
        }, {
          name: '未完成',
          value: raw.data.noAccomplishTotal
        }]
      }
    },
    getList: async () => {
      let raw = await fetchDataJH('/tengyun/api/bar/point/inventory');
      let result = raw.data.map((o, i) => {
        return {
          selected: false,
          deviceID: o.id,
          deviceNumber: "facilities_lamp_" + i,
          // deviceID: o.barCode,
          // deviceNumber: o.barCode,
          deviceArea: o.name,
          deviceStatus: o.status,
          deviceStatusTextColor: o.status === '在线' ? 'text-color-green' : 'text-color-orange',
          poiData: {
            id: o.id,
            //id: o.barCode,
            type: 'lamp',
            poiCoord: o.longitude + ',' + o.latitude,
            elv: 3,
            state: o.status === '在线' ? 'state_1' : 'state_2'
          }
        }
      });
      return result;
    },
    queryById: async (id) => {
      let raw = await fetchData('/facilities/lamp.json');
      let node = raw.find(e => {
        return id === e.deviceNumber;
      });
      node.poiData = {
        id: node.deviceNumber,
        type: 'lamp',
        poiCoord: node.coord,
        elv: 0,
        state: i === 6 ? 'state_2' : 'state_1'
      }
      return node;
    },
    getDetailById: async (id) => {
      let raw = await fetchDataJH('/tengyun/api/patrol/smartStickDetails', {
        barID: id
      });
      return raw.data[0]
      // let node = raw.find(e => {
      //     return id === e.deviceNumber;
      // });
      // delete node.worksheetHistory;
      // delete node.deviceArea;
      // let keys = Object.keys(node);
      // let values = Object.values(node);
      // let hash = {
      //     deviceID: '设备ID',
      //     deviceNumber: '设备编号',
      //     deviceType: '设备名称',
      //     coord: '经纬度',
      //     deviceStatus: '在线状态'
      // }

      // return values.map((o, i) => {
      //     return {
      //         name: hash[keys[i]],
      //         value: o,
      //         textColor: o === '有故障' ? 'text-color-orange' : 'text',
      //     }
      // })
    },
    getWorksheetHistoryById: async (id) => {
      let raw = await fetchData('/facilities/lamp.json');
      let node = raw.find(e => {
        return id === e.deviceNumber;
      });
      let result = [];
      if (node.worksheetHistory.length > 0) {
        result = node.worksheetHistory.map(o => {
          return {
            title: o.title,
            updateTime: o.updateTime,
            textColor: o.isFinished ? 'text-color-green' : 'text-color-orange',
            status: o.isFinished ? '已完成' : '未完成',
            supervisorCount: o.supervisorCount
          }
        });
      }
      return result;
    },
    getMatchedWorkerById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      let result = null;
      if (failNode) {
        let workerList = await fetchData('/worker.json');
        let workerNode = workerList.find(e => {
          return failNode.workerNumber === e.detail.workerNumber
        });
        result = {
          id: workerNode.detail.workerNumber,
          type: workerNode.type,
          poiCoord: workerNode.coord,
          elv: 1,
          state: null,
          window: {
            url: `http://localhost:3000/pages/facilities/worker.html?number=${workerNode.detail.workerNumber}&&name=${workerNode.detail.workerName}&group=${workerNode.detail.workerGroup}&tel=${workerNode.detail.workerTel}`,
            size: '290,185',
            offset: '30,200'
          }
        }
      }
      return result;
    },
    getCheckPathById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      return failNode.path;
    }
  },
  facilitiesToilet: {
    getToiletStatistics: async () => {
      let raw = await fetchData('/facilities/toilet.json');
      return {
        residentCount: raw.residentCount,
        averageEnterFlow: raw.averageEnterFlow,
        averageExitFlow: raw.averageExitFlow,
        ammonia: raw.ammonia,
        hydrothion: raw.hydrothion
      }
    },
    getMaleToiletPitUsing: async () => {
      let raw = await fetchData('/facilities/toilet.json');
      return [{
        name: '已使用',
        value: raw.current.malepit.used
      }, {
        name: '故障',
        value: raw.current.malepit.failed
      }, {
        name: '可用',
        value: raw.current.malepit.available
      }]
    },
    getUrinalFunnelUsing: async () => {
      let raw = await fetchData('/facilities/toilet.json');
      return [{
        name: '已使用',
        value: raw.current.urinalFunnel.used
      }, {
        name: '故障',
        value: raw.current.urinalFunnel.failed
      }, {
        name: '可用',
        value: raw.current.urinalFunnel.available
      }]
    },
    getFemaleToiletPitUsing: async () => {
      let raw = await fetchData('/facilities/toilet.json');
      return [{
        name: '已使用',
        value: raw.current.femalepit.used
      }, {
        name: '故障',
        value: raw.current.femalepit.failed
      }, {
        name: '可用',
        value: raw.current.femalepit.available
      }]
    },
    getPeopleFlowHistory: async () => {
      let raw = await fetchData('/facilities/toilet.json');
      let result = {
        '男厕': raw.flowHistory.map(o => {
          return {
            name: o.date,
            value: o.male
          }
        }),
        '女厕': raw.flowHistory.map(o => {
          return {
            name: o.date,
            value: o.female
          }
        })
      };
      return result;
    },
    getAirQualityHistory: async () => {
      let raw = await fetchData('/facilities/toilet.json');
      let result = {
        'NH3': raw.airQualityHistory.map(o => {
          return {
            name: o.date,
            value: o.NH3
          }
        }),
        'H2S': raw.airQualityHistory.map(o => {
          return {
            name: o.date,
            value: o.H2S
          }
        }),
        'PM2.5': raw.airQualityHistory.map(o => {
          return {
            name: o.date,
            value: o['PM2.5']
          }
        })
      };
      return result;
    },
    getList: async () => {
      let raw = await fetchData('/facilities/toilet.json');
      return raw.deviceList.map(o => {
        return {
          deviceID: o.deviceID,
          deviceNumber: o.deviceNumber,
          deviceArea: o.deviceArea,
          deviceStatus: o.deviceStatus,
          deviceStatusTextColor: o.deviceStatus === '正常' ? 'text-color-green' : 'text-color-orange',
          poiData: {
            id: o.deviceNumber,
            type: 'toilet',
            poiCoord: o.coord,
            elv: o.coord_z,
            state: 'state_1'
          }
        }
      })
    },
    queryById: async (id) => {
      let raw = await fetchData('/facilities/toilet.json');
      let node = raw.deviceList.find(e => {
        return id === e.deviceNumber;
      });
      node.poiData = {
        id: node.deviceNumber,
        type: 'toilet',
        poiCoord: node.coord,
        elv: 0,
        state: node.deviceStatus === 正常 ? 'state_1' : 'state_2'
      }
      return node;
    },
    getDetailBasicById: async (id) => {
      let raw = await fetchData('/facilities/toilet.json');
      let node = raw.deviceList.find(e => {
        return id === e.deviceNumber;
      });
      delete node.current;
      delete node.deviceArea
      delete node.warningHistory;
      let keys = Object.keys(node);
      let values = Object.values(node);
      let hash = {
        deviceID: '设备ID',
        deviceNumber: '设备编号',
        devH: '水深',
        deviceType: '设备类型',
        deviceName: '设备名称',
        coord: '经纬度',
        deviceStatus: '在线状态'
      }

      return values.map((o, i) => {
        return {
          name: hash[keys[i]],
          value: o,
          textColor: o === '有故障' ? 'text-color-orange' : 'text',
        }
      })
    },
    getDetailWarningHistoryById: async (id) => {
      let raw = await fetchData('/facilities/toilet.json');
      let result = raw.deviceList.find(e => {
        return id === e.deviceNumber
      })
      return result.warningHistory.map(o => {
        return {
          title: o.title,
          updateTime: o.updateTime,
          status: o.status,
          textColor: o.status === '已解决' ? 'text-color-green' : 'text-color-orange'
        }
      });
    },
    getDetailLoadById: async (id) => {
      let raw = await fetchData('/facilities/toilet.json');
      let result = raw.deviceList.find(e => {
        return id === e.deviceNumber
      })
      return {
        malepit: [{
          name: '已使用',
          value: result.current.malepit.used
        }, {
          name: '故障',
          value: result.current.malepit.failed
        }, {
          name: '可用',
          value: result.current.malepit.available
        }],
        urinalFunnel: [{
          name: '已使用',
          value: result.current.urinalFunnel.used
        }, {
          name: '故障',
          value: result.current.urinalFunnel.failed
        }, {
          name: '可用',
          value: result.current.urinalFunnel.available
        }],
        femalpit: [{
          name: '已使用',
          value: result.current.femalepit.used
        }, {
          name: '故障',
          value: result.current.femalepit.failed
        }, {
          name: '可用',
          value: result.current.femalepit.available
        }]
      };
    },
    getDetailAirDataById: async (id) => {
      let raw = await fetchData('/facilities/toilet.json');
      let result = raw.deviceList.find(e => {
        return id === e.deviceNumber
      })
      return result.current;
    },
    getMatchedWorkerById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      let result = null;
      if (failNode) {
        let workerList = await fetchData('/worker.json');
        let workerNode = workerList.find(e => {
          return failNode.workerNumber === e.detail.workerNumber
        });
        result = {
          id: workerNode.detail.workerNumber,
          type: workerNode.type,
          poiCoord: workerNode.coord,
          elv: 1,
          state: null,
          window: {
            url: `http://localhost:3000/pages/facilities/worker.html?number=${workerNode.detail.workerNumber}&name=${workerNode.detail.workerName}&group=${workerNode.detail.workerGroup}&tel=${workerNode.detail.workerTel}`,
            size: '290,185',
            offset: '30,200'
          }
        }
      }
      return result;
    },
    getCheckPathById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      return failNode.path;
    }
  },
  facilitiesCamera: {
    getNormalRate: async () => {
      let raw = await fetchDataJH('/tengyun/api/camera/cameraWorkOrder');
      console.log(raw)
      return {
        percentage: parseInt(raw.data[0].runProbability * 100),
        data: [{
          name: '正常数',
          value: raw.data[0].normalTotal
        }, {
          name: '故障数',
          value: raw.data[0].malfunctionTotal
        }]
      }
    },
    getWorksheetCompletion: async () => {
      let raw = await fetchDataJH('/tengyun/api/camera/cameraWorkOrder');
      return {
        percentage: parseInt(raw.data[1].accomplishProbability * 100),
        data: [{
          name: '已完成',
          value: raw.data[1].accomplishTotal
        }, {
          name: '未完成',
          value: raw.data[1].noAccomplishTotal
        }]
      }
    },
    getList: async () => {
      let raw = await fetchDataJH('/tengyun/camera/infos');
      let result = raw.data.map((o, i) => {
        return {
          selected: false,
          deviceID: o.camera_id,
          deviceNumber: o.camera_id,
          deviceCode: o.code,
          deviceArea: o.camera_name,
          deviceStatus: o.online_status === 1 ? '正常' : '异常',
          deviceStatusTextColor: o.online_status === 1 ? 'text-color-green' : 'text-color-orange',
          poiData: {
            id: o.code,
            type: 'camera',
            poiCoord: o.longitude + ',' + o.latitude,
            elv: 0,
            state: o.online_status === 1 ? 'state_1' : 'state_2'
          }
        }
      });
      return result;
    },
    queryById: async (id) => {
      let raw = await fetchData('/camera.json');
      let node = raw.find(e => {
        return id === e.deviceNumber;
      });
      node.poiData = {
        id: node.deviceNumber,
        type: 'camera',
        poiCoord: node.coord,
        elv: 0,
        state: node.deviceStatus === '正常' ? 'state_1' : 'state_2'
      }
      return node;
    },
    getDetailById: async () => {
      let raw = await fetchDataJH('/tengyun/camera/infos');
      return raw.data
      // delete node.worksheetHistory;
      // delete node.deviceArea;
      // delete node.deviceVideoURL;
      // let keys = Object.keys(node);
      // let values = Object.values(node);
      // let hash = {
      //     deviceID: '设备ID',
      //     deviceNumber: '设备编号',
      //     deviceType: '设备类型',
      //     deviceName: '设备名称',
      //     coord: '经纬度',
      //     deviceStatus: '在线状态'
      // }

      // return values.map((o, i) => {
      //     return {
      //         name: hash[keys[i]],
      //         value: o,
      //         textColor: o === '有故障' ? 'text-color-orange' : 'text',
      //     }
      // })
    },
    getPreviewURLById: async (id) => {
      let raw = await fetchData('/camera.json');
      let node = raw.find(e => {
        return id === e.deviceNumber
      });
      return node.deviceVideoURL;
    },
    getFacilitiesCamera: async (device_code) => {
      let raw = await fetchDataZG('/api/v1/video_monitoring/smart/get_preview_url', {
        cameraIndexCode: device_code
      })
      return raw
    },
    getWorksheetHistoryById: async (id) => {
      let raw = await fetchData('/camera.json');
      let node = raw.find(e => {
        return id === e.deviceNumber;
      });
      let result = [];
      if (node.worksheetHistory.length > 0) {
        result = node.worksheetHistory.map(o => {
          return {
            title: o.title,
            updateTime: o.updateTime,
            textColor: o.isFinished ? 'text-color-green' : 'text-color-orange',
            status: o.isFinished ? '已完成' : '未完成',
            supervisorCount: o.supervisorCount
          }
        });
      }
      return result;
    },
    getMatchedWorkerById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      let result = null;
      if (failNode) {
        let workerList = await fetchData('/worker.json');
        let workerNode = workerList.find(e => {
          return failNode.workerNumber === e.detail.workerNumber
        });
        result = {
          id: workerNode.detail.workerNumber,
          type: workerNode.type,
          poiCoord: workerNode.coord,
          elv: 1,
          state: null,
          window: {
            url: `http://localhost:3000/pages/facilities/worker.html?number=${workerNode.detail.workerNumber}&name=${workerNode.detail.workerName}&group=${workerNode.detail.workerGroup}&tel=${workerNode.detail.workerTel}`,
            size: '290,185',
            offset: '30,200'
          }
        }
      }
      return result;
    },
    getCheckPathById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      return failNode.path;
    }
  },
  facilitiesScreen: {
    getNormalRate: async () => {
      let raw = await fetchData('/facilities/general.json');
      let node = raw.find(e => {
        return e.type === 'camera'
      });
      let result = 0;
      if (node.totalCount !== 0) {
        result = parseInt((node.normalCount / node.totalCount) * 100);
      }
      return {
        percentage: result,
        data: [{
          name: '正常数',
          value: node.normalCount
        }, {
          name: '故障数',
          value: node.totalCount - node.normalCount
        }]
      };
    },
    getWorksheetCompletion: async () => {
      let raw = await fetchData('/facilities/general.json');
      let node = raw.find(e => {
        return e.type === 'camera'
      });
      let result = 0;
      if (node.worksheet !== 0) {
        result = parseInt((node.worksheetCompleted / node.worksheet) * 100);
      }
      return {
        percentage: result,
        data: [{
          name: '已完成',
          value: node.worksheetCompleted
        }, {
          name: '未完成',
          value: node.worksheet - node.worksheetCompleted
        }]
      };
    },
    getList: async () => {
      let raw = await fetchData('/facilities/screen.json');
      let result = raw.map((o, i) => {
        return {
          selected: false,
          deviceID: o.deviceID,
          deviceNumber: o.deviceNumber,
          deviceArea: o.deviceArea,
          deviceStatus: o.deviceStatus,
          deviceStatusTextColor: o.deviceStatus === '正常' ? 'text-color-green' : 'text-color-orange',
          poiData: {
            id: o.deviceNumber,
            type: 'touch',
            poiCoord: o.coord,
            elv: o.coord_z,
            state: o.deviceStatus === '正常' ? 'state_1' : 'state_2'
          }
        }
      });
      return result;
    },
    queryById: async (id) => {
      let raw = await fetchData('/facilities/screen.json');
      let node = raw.find(e => {
        return id === e.deviceNumber;
      });
      node.poiData = {
        id: node.deviceNumber,
        type: 'touch',
        poiCoord: node.coord,
        elv: 0,
        state: node.deviceStatus === '正常' ? 'state_1' : 'state_2'
      }
      return node;
    },
    getDetailById: async (id) => {
      let raw = await fetchData('/facilities/screen.json');
      let node = raw.find(e => {
        return id === e.deviceNumber;
      });
      delete node.worksheetHistory;
      delete node.deviceArea;
      delete node.deviceVideoURL;
      let keys = Object.keys(node);
      let values = Object.values(node);
      let hash = {
        deviceID: '设备ID',
        deviceNumber: '设备编号',
        deviceType: '设备类型',
        deviceName: '设备名称',
        coord: '经纬度',
        deviceStatus: '在线状态'
      }

      return values.map((o, i) => {
        return {
          name: hash[keys[i]],
          value: o,
          textColor: o === '有故障' ? 'text-color-orange' : 'text',
        }
      })
    },
    getPreviewURLById: async (id) => {
      let raw = await fetchData('/facilities/screen.json');
      let node = raw.find(e => {
        return id === e.deviceNumber
      });
      return node.deviceVideoURL;
    },
    getWorksheetHistoryById: async (id) => {
      let raw = await fetchData('/facilities/screen.json');
      let node = raw.find(e => {
        return id === e.deviceNumber;
      });
      let result = [];
      if (node.worksheetHistory.length > 0) {
        result = node.worksheetHistory.map(o => {
          return {
            title: o.title,
            updateTime: o.updateTime,
            textColor: o.isFinished ? 'text-color-green' : 'text-color-orange',
            status: o.isFinished ? '已完成' : '未完成',
            supervisorCount: o.supervisorCount
          }
        });
      }
      return result;
    },
    getMatchedWorkerById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      let result = null;
      if (failNode) {
        let workerList = await fetchData('/worker.json');
        let workerNode = workerList.find(e => {
          return failNode.workerNumber === e.detail.workerNumber
        });
        result = {
          id: workerNode.detail.workerNumber,
          type: workerNode.type,
          poiCoord: workerNode.coord,
          elv: 1,
          state: null,
          window: {
            url: `http://localhost:3000/pages/facilities/worker.html?number=${workerNode.detail.workerNumber}&name=${workerNode.detail.workerName}&group=${workerNode.detail.workerGroup}&tel=${workerNode.detail.workerTel}`,
            size: '290,185',
            offset: '30,200'
          }
        }
      }
      return result;
    },
    getCheckPathById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      return failNode.path;
    }
  },
  facilitiesWell: {
    // 获取井盖一周水位
    getWaterWeekAvg: async () => {
      let raw = await fetchDataJH('/tengyun/api/water/quality/weekAvg');
      return {
        '液位平均值': raw.data
      }
    },

    // 获取井盖总体水位占比
    getBilgewellProport: async () => {
      let raw = await fetchDataJH('/tengyun/api/water/quality/BilgewellProportion');
      return raw.data
    },

    // 获取井盖设施清单列表
    getBilgewellList: async () => {
      let raw = await fetchDataJH('/tengyun/api/water/quality/bilgewellList');
      // console.log(raw)
      return raw.data
    },

    // 获取井盖并渲染点位
    getBilgeWellPOI: async () => {
      let raw = await fetchDataJH('/tengyun/api/water/quality/bilgewellList')
      return raw.data.map(res => {
        return {
          id: res.id,
          type: 'well',
          poiCoord: res.longitude + ',' + res.latitude,
          elv: 0,
          state: res.flag === 1 ? 'state_1' : 'state_2',
        }
      })
    },

    // 获取井盖详情
    getBilgewellInfo: async (id) => {
      let raw = await fetchDataJH('/tengyun/api/water/quality/BilgewellInfo', {
        deviceId: id
      });
      return [{
        name: '设备编号：',
        value: raw.data.wellCode
      }, {
        name: '设备名称：',
        value: raw.data.name
      }, {
        name: '设备类型：',
        value: raw.data.type
      }, {
        name: '水深',
        value: raw.data.h
      }, {
        name: '经纬度：',
        value: raw.data.longitude + ',' + raw.data.latitude
      }, {
        name: '设备ID：',
        value: raw.data.id
      }, {
        name: '在线状态：',
        value: raw.data.flagStr
      }]
    },

    getGeneralWaterLevel: async () => {
      let raw = await fetchData('/facilities/well.json');
      return raw.general.averageLevel
    },
    getWellLevelDistribute: async () => {
      let raw = await fetchData('/facilities/well.json');
      return [{
        name: '溢出',
        value: raw.general.overflowed
      }, {
        name: '危险',
        value: raw.general.dangerous
      }, {
        name: '较高',
        value: raw.general.highlevel
      }, {
        name: '中等',
        value: raw.general.medium
      }, {
        name: '较低',
        value: raw.general.low
      }]

    },

    getList: async () => {
      let raw = await fetchData('/facilities/well.json');
      console.log(raw)
      let result = raw.deviceList.map((o, i) => {
        return {
          selected: false,
          deviceID: o.deviceID,
          deviceNumber: o.deviceNumber,
          deviceArea: o.deviceArea,
          deviceStatus: o.deviceStatus,
          deviceStatusTextColor: o.deviceStatus === '正常' ? 'text-color-green' : 'text-color-orange',
          poiData: {
            id: o.deviceNumber,
            type: 'well',
            poiCoord: o.coord,
            elv: o.coord_z,
            state: o.deviceStatus === '正常' ? 'state_1' : 'state_2'
          }
        }
      });
      return result;
    },
    queryById: async (id) => {
      let raw = await fetchData('/facilities/well.json');
      let node = raw.deviceList.find(e => {
        return id === e.deviceNumber;
      });
      node.poiData = {
        id: node.deviceNumber,
        type: 'well',
        poiCoord: node.coord,
        elv: 0,
        state: node.deviceStatus === '正常' ? 'state_1' : 'state_2'
      }
      return node;
    },
    getDetailById: async (id) => {
      let raw = await fetchData('/facilities/well.json');
      let node = raw.deviceList.find(e => {
        return id === e.deviceNumber;
      });
      delete node.worksheetHistory;
      delete node.deviceArea;
      let keys = Object.keys(node);
      let values = Object.values(node);
      let hash = {
        deviceID: '设备ID',
        deviceNumber: '设备编号',
        deviceType: '设备类型',
        deviceName: '设备名称',
        coord: '经纬度',
        deviceStatus: '在线状态'
      }

      return values.map((o, i) => {
        return {
          name: hash[keys[i]],
          value: o,
          textColor: o.includes('高液位') ? 'text-color-orange' : 'text',
        }
      })
    },
    // 井盖工单列表
    getWorksheetHistoryById: async (id) => {
      let raw = await fetchData('/facilities/well.json');
      let node = raw.deviceList.find(e => {
        return id === e.deviceNumber;
      });
      let result = [];
      if (node.worksheetHistory.length > 0) {
        result = node.worksheetHistory.map(o => {
          return {
            title: o.title,
            updateTime: o.updateTime,
            textColor: o.isFinished ? 'text-color-green' : 'text-color-orange',
            status: o.isFinished ? '已完成' : '未完成',
            supervisorCount: o.supervisorCount
          }
        });
      }
      return result;
    },
    getMatchedWorkerById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      let result = null;
      if (failNode) {
        let workerList = await fetchData('/worker.json');
        let workerNode = workerList.find(e => {
          return failNode.workerNumber === e.detail.workerNumber
        });
        result = {
          id: workerNode.detail.workerNumber,
          type: workerNode.type,
          poiCoord: workerNode.coord,
          elv: 1,
          state: null,
          window: {
            url: `http://localhost:3000/pages/facilities/worker.html?number=${workerNode.detail.workerNumber}&name=${workerNode.detail.workerName}&group=${workerNode.detail.workerGroup}&tel=${workerNode.detail.workerTel}`,
            size: '290,185',
            offset: '30,200'
          }
        }
      }
      return result;
    },
    getCheckPathById: async (id) => {
      let failList = await fetchData('/facilities/fail.json');
      let failNode = failList.find(e => {
        return id === e.deviceNumber
      });
      return failNode.path;
    }
  },
  science: {
    getPlant: async () => {
      let raw = await fetchData('/science/distribute.json');
      return raw.plant;
    },
    getAnimal: async () => {
      let raw = await fetchData('/science/distribute.json');
      return raw.animal;
    },
    getPOI: async () => {
      let raw = await fetchData('/science/poi.json');
      return raw.map((o, i) => {
        return {
          id: o.id,
          type: 'science',
          poiCoord: o.poiData.poiCoord,
          elv: 1,
          state: 'state_1',
        }
      })
    }
  }
}

export const uniFetcherFacilities = uniFetcher.facilities;
export const uniFetcherFacilitiesLamp = uniFetcher.facilitiesLamp;
export const uniFetcherFacilitiesToilet = uniFetcher.facilitiesToilet;
export const uniFetcherFacilitiesCamera = uniFetcher.facilitiesCamera;
export const uniFetcherFacilitiesScreen = uniFetcher.facilitiesScreen;
export const uniFetcherFacilitiesWell = uniFetcher.facilitiesWell;
export const uniFetcherScience = uniFetcher.science;
export const uniFetcherOperations = uniFetcher.Operations
export const uniFetcherSecurity = uniFetcher.security
export const uniFecherHome = uniFetcher.Home
export const uniFecherEcology = uniFetcher.ecology