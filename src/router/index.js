/*
 * @Author: your name
 * @Date: 2020-10-09 09:27:46
 * @LastEditTime: 2020-10-09 20:35:58
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \helium-main\src\router\index.js
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/login'
  },{
    path:'/login',
    name:'Login',
    component:()=>import('../views/Login')
  },{
    path:'/home',
    name:'home',
    component:()=>import('../views/Home')
  },{
    path:'/ecology',
    name:'ecology',
    component:()=>import('../views/Ecology')
  },{
    path:'/ecologyPage',
    name:'ecologyPage',
    component:()=>import('../views/Ecology_changed')
  },{
    path:'/operations',
    name:'operations',
    component:()=>import('../views/Operations')
  },{
    path:'/secruity',
    name:'secruity',
    component:()=>import('../views/Secruity')
  },{
    path: '/security/camera/:cameraid/:code',
    name: 'securityCameraDetail',
    component: ()=> import('../views/SecurityDetail/securityCameraDetail.vue')
  },{
    path:'/facilities',
    name:'facilities',
    component:()=>import('../views/facilities/Main')
  },{
    path:'/facilities/lamp',
    name:'facilities-lamp',
    component:()=>import('../views/facilities/LampList')
  },{
    path:'/facilities/lamp-detail/:id/:from',
    name:'facilities-lamp-detail',
    component:()=>import('../views/facilities/LampDetail')
  },{
    path:'/facilities/toilet',
    name:'facilities-toilet',
    component:()=>import('../views/facilities/ToiletList')
  },{
    path:'/facilities/toilet-detail/:id/:from',
    name:'facilities-toilet-detail',
    component:()=>import('../views/facilities/ToiletDetail')
  },{
    path:'/facilities/camera',
    name:'facilities-camera',
    component:()=>import('../views/facilities/CameraList')
  },{
    path:'/facilities/camera-detail/:id/:from',
    name:'facilities-camera-detail',
    component:()=>import('../views/facilities/CameraDetail')
  },{
    path:'/facilities/screen',
    name:'facilities-screen',
    component:()=>import('../views/facilities/ScreenList')
  },{
    path:'/facilities/screen-detail/:id/:from',
    name:'facilities-screen-detail',
    component:()=>import('../views/facilities/ScreenDetail')
  },{
    path:'/facilities/well',
    name:'facilities-well',
    component:()=>import('../views/facilities/WellList')
  },{
    path:'/facilities/well-detail/:id/:from',
    name:'facilities-well-detail',
    component:()=>import('../views/facilities/WellDetail')
  },{
    path:'/culture',
    name:'culture',
    component:()=>import('../views/Culture')
  },{
    path:'/science',
    name:'science',
    component:()=>import('../views/science/Main.vue')
  },{
    path: '/waterpointdetail/:waterpointid',
    name: 'water-point-detail',
    component:()=>import('../views/EcologyDetail/WaterPointDetail.vue')
  },{
    path: '/waterpointdetailchange/:waterpointid',
    name: 'water-point-detail-change',
    component:()=>import('../views/EcologyDetail/WaterPointDetail_Change.vue')
  },
  {
    path: '/parkingdetail/:parkingid',
    name: 'parking-detail',
    component: ()=>import('../views/OperationDetail/ParkingDetail.vue')
  },
  {
    path:'/uavdetail/:uavid',
    name:'uavdetail',
    component:()=>import('../views/OperationDetail/UAVDetail')
  },
  {
    path:'/shuttledetail/:shuttleid',
    name:'shuttle-detail',
    component:()=>import('../views/OperationDetail/ShuttleDetail')
  },{
    path: '/drawthreeline',
    name: 'threeLine',
    component:()=>import('../views/EcologyDetail/ThreeLineDetail')
  },{
    path:'/eventdetail/:eventid/:from',
    name:'eventdetail',
    component: ()=> import('../views/SecurityDetail/SecurityEventDetail.vue')
  },{
    path:'/workerdetail/:workerid',
    name:'workerdetail',
    component: ()=> import('../views/SecurityDetail/SecurityWorkerDetail.vue')
  },{
    path: '/sciencedetail/:scienceid',
    name: 'sciencedetail',
    component: ()=> import('../views/science/ScienceDetail.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
