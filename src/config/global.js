/*
 * @Description: 
 * @Author: yaozhu yang
 * @Date: 2021-11-17 11:42:54
 * @LastEditTime: 2021-11-17 14:52:14
 * @LastEditors: yaozhu yang
 */
import projectSetting from '@/config/developer/linqiankun.js'

export const URLHEAD = {
  attach: 'http://localhost:3000/',
  main: 'http://localhost:8080/'
}
export const CLOUDSETTING = projectSetting;